/* Automatically generated.  Do not edit */
/* See the mkopcodeh.awk script for details */
#define OP_MemMax                               1
#define OP_RowData                              2
#define OP_OpenTemp                             3
#define OP_AggInit                              4
#define OP_CreateIndex                          5
#define OP_Recno                                6
#define OP_Variable                             7
#define OP_MemStore                             8
#define OP_AggFunc                              9
#define OP_Clear                               10
#define OP_Last                                11
#define OP_KeyAsData                           12
#define OP_Add                                 83   /* same as TK_PLUS     */
#define OP_MoveGe                              13
#define OP_SortNext                            14
#define OP_RowKey                              15
#define OP_Divide                              86   /* same as TK_SLASH    */
#define OP_ResetCount                          16
#define OP_Delete                              17
#define OP_SortReset                           18
#define OP_OpenRead                            19
#define OP_Sort                                20
#define OP_VerifyCookie                        21
#define OP_Next                                22
#define OP_AggGet                              23
#define OP_ListReset                           24
#define OP_ListRead                            25
#define OP_Prev                                26
#define OP_PutStrKey                           27
#define OP_IdxGE                               28
#define OP_Not                                 66   /* same as TK_NOT      */
#define OP_Ge                                  77   /* same as TK_GE       */
#define OP_DropTable                           29
#define OP_MakeRecord                          30
#define OP_IdxIsNull                           31
#define OP_ReadCookie                          32
#define OP_DropIndex                           33
#define OP_IsNull                              70   /* same as TK_ISNULL   */
#define OP_MustBeInt                           34
#define OP_Callback                            35
#define OP_SortPut                             36
#define OP_IntegrityCk                         37
#define OP_MoveGt                              38
#define OP_MoveLe                              39
#define OP_CollSeq                             40
#define OP_HexBlob                            131   /* same as TK_BLOB     */
#define OP_Eq                                  73   /* same as TK_EQ       */
#define OP_String8                             92   /* same as TK_STRING   */
#define OP_PutIntKey                           41
#define OP_Found                               42
#define OP_If                                  43
#define OP_IdxPut                              44
#define OP_Multiply                            85   /* same as TK_STAR     */
#define OP_Dup                                 45
#define OP_ShiftRight                          82   /* same as TK_RSHIFT   */
#define OP_Goto                                46
#define OP_Function                            47
#define OP_Pop                                 48
#define OP_FullKey                             49
#define OP_Blob                                50
#define OP_MemIncr                             51
#define OP_BitNot                              91   /* same as TK_BITNOT   */
#define OP_IfMemPos                            52
#define OP_IdxGT                               53
#define OP_Gt                                  74   /* same as TK_GT       */
#define OP_Le                                  75   /* same as TK_LE       */
#define OP_AggFocus                            54
#define OP_IdxRecno                            55
#define OP_NullRow                             56
#define OP_Transaction                         57
#define OP_SetCookie                           58
#define OP_Negative                            89   /* same as TK_UMINUS   */
#define OP_And                                 65   /* same as TK_AND      */
#define OP_AggSet                              59
#define OP_ContextPush                         60
#define OP_DropTrigger                         61
#define OP_MoveLt                              62
#define OP_AutoCommit                          63
#define OP_Column                              67
#define OP_AbsValue                            68
#define OP_AddImm                              69
#define OP_Remainder                           87   /* same as TK_REM      */
#define OP_AggReset                            78
#define OP_ContextPop                          90
#define OP_IdxDelete                           93
#define OP_Ne                                  72   /* same as TK_NE       */
#define OP_Concat                              88   /* same as TK_CONCAT   */
#define OP_Return                              94
#define OP_Expire                              95
#define OP_Rewind                              96
#define OP_Statement                           97
#define OP_AggContextPush                      98
#define OP_BitOr                               80   /* same as TK_BITOR    */
#define OP_Integer                             99
#define OP_AggContextPop                      100
#define OP_Destroy                            101
#define OP_IdxLT                              102
#define OP_NewRecno                           103
#define OP_Lt                                  76   /* same as TK_LT       */
#define OP_Subtract                            84   /* same as TK_MINUS    */
#define OP_Vacuum                             104
#define OP_IfNot                              105
#define OP_Pull                               106
#define OP_ListRewind                         107
#define OP_ParseSchema                        108
#define OP_SetNumColumns                      109
#define OP_BitAnd                              79   /* same as TK_BITAND   */
#define OP_String                             110
#define OP_NotExists                          111
#define OP_Close                              112
#define OP_Halt                               113
#define OP_Noop                               114
#define OP_OpenPseudo                         115
#define OP_Or                                  64   /* same as TK_OR       */
#define OP_ShiftLeft                           81   /* same as TK_LSHIFT   */
#define OP_ListWrite                          116
#define OP_IsUnique                           117
#define OP_ForceInt                           118
#define OP_AggNext                            119
#define OP_OpenWrite                          120
#define OP_Gosub                              121
#define OP_Real                               130   /* same as TK_FLOAT    */
#define OP_Distinct                           122
#define OP_NotNull                             71   /* same as TK_NOTNULL  */
#define OP_MemLoad                            123
#define OP_NotFound                           124
#define OP_CreateTable                        125
#define OP_Push                               126

/* The following opcode values are never used */
#define OP_NotUsed_127                        127
#define OP_NotUsed_128                        128
#define OP_NotUsed_129                        129
