#ifndef DREADERSELX800_H
#define DREADERSELX800_H

#include <fx.h>

#include "cReadersManager.h"

#define READER_ELX800 2

class dReadersELX800 :public cReadersObject
{
    private:
    protected:
        cSerialLink *link;
        FXString filterA,filterB;
        
        virtual FXbool initialize(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout);
        virtual void deinitialize(void);
    
    public:
        dReadersELX800();
        virtual ~dReadersELX800();
        
        virtual FXString getManufacturer(void);
        virtual FXString getModel(void);
        virtual int getCode(void);
        virtual sReadersSettings getSettings(void);
        
        virtual FXString getFilterA(void);
        virtual FXString getFilterB(void);
        virtual void setFilterA(const FXString &prFilter);
        virtual void setFilterB(const FXString &prFilter);

        virtual double *acquire(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout);
};

#endif 
