#include <stdlib.h>

#include "engine.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "dReadersELX800.h"

FXbool dReadersELX800::initialize(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    char res[10];
    int count=0,flag=false;
    
    if(link!=NULL)
        deinitialize();
    
    link=new cSerialLink();

    if(!link->connect(prSerialDevice,prBaudRate,prBits,prParity,prStopBits,prFlow,prTimeout))
        return false;
    
    if(!link->setTimeouts(1))
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
        return false;
    }
    while(!flag)
    {
        if(link->output("X",1)==-1)
            return false;
        if(-1==(link->input(res,1)) || ++count>1)
        {
            oApplicationManager->endWaitCursor();
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
            return false;
        }
        if(res[0]==0x10)
            flag=true;
    }
    if(!link->setTimeouts(prTimeout))
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
        return false;
    }
    return true;
}

void dReadersELX800::deinitialize(void)
{
    if(link!=NULL)
        link->disconnect();
    delete link;
}

dReadersELX800::dReadersELX800()
{
    filterA="";
    filterB="";
    link=NULL;
}

dReadersELX800::~dReadersELX800()
{
    deinitialize();
    link=NULL;
}
    
FXString dReadersELX800::getManufacturer(void)
{
    return "Bio-Tek Instruments";
}

FXString dReadersELX800::getModel(void)
{
    return "ELx800";
}

int dReadersELX800::getCode(void)
{
    return READER_ELX800;
}

sReadersSettings dReadersELX800::getSettings(void)
{
    sReadersSettings ret;
    ret.baudRate=BAUD2400;
    ret.bits=BITS8;
    ret.parity=PARITYNONE;
    ret.stopBits=STOPBITS2;
    ret.flow=FLOWNONE;
    ret.timeout=90;
    return ret;
}

FXString dReadersELX800::getFilterA(void)
{
    return filterA;
}

FXString dReadersELX800::getFilterB(void)
{
    return filterB;
}

void dReadersELX800::setFilterA(const FXString &prFilter)
{
    filterA=prFilter;
}

void dReadersELX800::setFilterB(const FXString &prFilter)
{
    filterB=prFilter;
}

double *dReadersELX800::acquire(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    double *ret=(double *)malloc(96*sizeof(double));
    int min,fla=FXIntVal(filterA),flb=FXIntVal(filterB),filters[6];
    char res[10],digit;
    if(!ret)
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        {deinitialize();return NULL;}
    }
    memset(ret,0,sizeof(double)*96);
    if(link==NULL)    
        if(!initialize(prSerialDevice,prBaudRate,prBits,prParity,prStopBits,prFlow,prTimeout))
            {deinitialize();return NULL;}

    link->flush();
    if(!filterA.empty())
    {
        unsigned char *res2=(unsigned char *)malloc(1000);
        int i,flaa=0,flbb=0;
        
        if(!res2)
        {
            oApplicationManager->endWaitCursor();
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
            {deinitialize();return NULL;}
        }
    
        link->output("*",1,true);
        digit=0;
        i=0;
        while(digit!=0x1E)
        {
            if(link->input(&digit,1)==-1)
                {deinitialize();return NULL;}
            res2[i++]=digit;
        }
        res2[i]=0;
        if(link->input(&digit,1)==-1 || digit!='0')
            {deinitialize();return NULL;}
        if(link->input(&digit,1)==-1 || digit!='0')
            {deinitialize();return NULL;}
        if(link->input(&digit,1)==-1 || digit!='0')
            {deinitialize();return NULL;}
        if(link->input(&digit,1)==-1 || digit!=0x03)
            {deinitialize();return NULL;}
        link->flush();
        FXString *res4=new FXString((FXchar*)res2);
        i=min=0;
        int res5;
        memset(filters,0,sizeof(int)*6);
        while((min=res4->find("Filter: ",min)+8)>7)
        {
            res5=FXIntVal(res4->mid(min,3));
            if(res5>=300)
                filters[i++]=res5;
        }
        free(res2);
        if(fla!=0 && filters[0]!=0)
        {
            min=fla-filters[0];
            flaa=filters[0];
            
            for(int i=0;i<6 && filters[i]>0;i++)
            {
                if(abs(fla-filters[i])<min)
                {
                    min=abs(fla-filters[i]);
                    flaa=filters[i];
                }
            }
        }
        
        if(min>20)
        {
            oApplicationManager->endWaitCursor();
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),
                                FXStringFormat("%s %dnm",oLanguage->getText("str_serialerr_nofil").text(),flaa).text());
            {deinitialize();return NULL;}
        }

        flbb=0;
        if(flb!=0 && filters[0]!=0)
        {
            min=flb-filters[0];
            flbb=filters[0];
            
            for(int i=0;i<6 && filters[i]>0;i++)
            {
                if(abs(flb-filters[i])<min)
                {
                    min=abs(flb-filters[i]);
                    flbb=filters[i];
                }
            }
            if(min>20)
            {
                oApplicationManager->endWaitCursor();
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),
                                    FXStringFormat("%s %dnm",oLanguage->getText("str_serialerr_nofil").text(),flbb).text());
                {deinitialize();return NULL;}
            }
        }
     
        if(flaa>0)
        {
            unsigned char *cfg=(unsigned char *)malloc(170);
            if(!ret || !cfg)
            {
                oApplicationManager->endWaitCursor();
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                {deinitialize();return NULL;}
            }                                  
            memset(cfg,0,170);
            
            cfg[29]=flbb>0?0x40:0x00;
            
            FXString res5=FXStringVal(flaa);
            cfg[49]=res5.at(0);
            cfg[50]=res5.at(1);
            cfg[51]=res5.at(2);

            if(flb>0)
            {
                res5=FXStringVal(flbb);
                cfg[52]=res5.at(0);
                cfg[53]=res5.at(1);
                cfg[54]=res5.at(2);
            }
            cfg[0]=0;
            for(i=1;i<170;i++)
                cfg[0]+=cfg[i];
            
            link->output("V",1,true);
            link->output((char*)cfg,170);
            if(link->input(res,1)==-1 || res[0]!=0x1E)
                {deinitialize();return NULL;}
            if(link->input(res,1)==-1 || res[0]!='0')
                {deinitialize();return NULL;}
            if(link->input(res,1)==-1 || res[0]!='0')
                {deinitialize();return NULL;}
            if(link->input(res,1)==-1 || res[0]!='0')
                {deinitialize();return NULL;}
            if(link->input(res,1)==-1 || res[0]!=0x03)
                {deinitialize();return NULL;}
            link->flush();
            free(cfg);
        }
    }
    link->output("S",1,true);
    if(link->input(res,1)==-1 || res[0]!=0x1E)
        {deinitialize();return NULL;}
    if(link->input(res,1)==-1 || res[0]!='0')
        {deinitialize();return NULL;}
    if(link->input(res,1)==-1 || res[0]!='0')
        {deinitialize();return NULL;}
    if(link->input(res,1)==-1 || res[0]!='0')
        {deinitialize();return NULL;}
    if(link->input(res,1)==-1 || res[0]!=0x03)
        {deinitialize();return NULL;}
    if(link->input(res,1)==-1 || res[0]!=0x0D)
        {deinitialize();return NULL;}
    
    for(int j=0;j<8;j++)
    {
        for(int i=0;i<12;i++)
        {
            if(link->input(&digit,1)==-1 || digit==0x10)
                {deinitialize();return NULL;}
            for(int k=0;k<5;k++)
            {
                if(link->input(&digit,1)==-1 || digit==0x10)
                    {deinitialize();return NULL;}
                res[k]=digit;
            }
            res[5]=0;
            
            switch(res[0])
            {
                case '*':
                case 'O':
                case ' ':
                    res[0]='4';
                    res[1]=0;
                    break;
                case 'P':
                    res[0]='+';
                    break;
                case 'N':
                    res[0]='-';
                    break;
            }
            
            res[6]=res[5];res[5]=res[4];
            res[4]=res[3];res[3]=res[2];
            res[2]='.';
            ret[j*12+i]=FXDoubleVal(res);
        }
        if(link->input(res,1)==-1 || res[0]==0x0A)
            {deinitialize();return NULL;}
        if(link->input(res,1)==-1 || res[0]==0x0D)
            {deinitialize();return NULL;}
    }

    link->flush();
    link->output("X",1);
    link->flush();
    deinitialize();

    for(int yo=0;yo<96;yo++)
        if(ret[yo]==-0)ret[yo]=0;
    
    return ret;
}

