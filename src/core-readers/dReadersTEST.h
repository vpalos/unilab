#ifndef DREADERSTEST_H
#define DREADERSTEST_H

#include <fx.h>

#include "cReadersManager.h"

#define READER_TEST 0

class dReadersTEST :public cReadersObject
{
    private:
    protected:
    public:
        dReadersTEST();
        virtual ~dReadersTEST();
        
        virtual FXString getManufacturer(void);
        virtual FXString getModel(void);
        virtual int getCode(void);
        virtual sReadersSettings getSettings(void);
        
        virtual FXString getFilterA(void);
        virtual FXString getFilterB(void);
        virtual void setFilterA(const FXString &prFilter);
        virtual void setFilterB(const FXString &prFilter);

        virtual double *acquire(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout);
};

#endif 
