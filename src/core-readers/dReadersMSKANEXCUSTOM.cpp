#include <stdlib.h>
#include <strings.h>
#include "engine.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "dReadersMSKANEXCUSTOM.h"

FXbool dReadersMSKANEXCUSTOM::isError(void)
{
    char res[5];
    if(link->input(res,4)==-1 || strncmp(res,"OK\r\n",4))
    {
#ifdef WIN32
        Sleep(2);
#else
        sleep(2);
#endif
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
        return true;
    }
    return false;
}

FXbool dReadersMSKANEXCUSTOM::initialize(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    if(link!=NULL)
        deinitialize();
    link=new cSerialLink();
    if(!link->connect(prSerialDevice,prBaudRate,prBits,prParity,prStopBits,prFlow,prTimeout))
        return false;
    link->output("R\n",2);
    if(isError())
        return false;
    link->output("I\n",2);
    if(isError())
        return false;
    link->output("E0\n",3);
    if(isError())
        return false;
    return true;
}

void dReadersMSKANEXCUSTOM::deinitialize(void)
{
    if(link!=NULL)
    {
    	link->output("Q\n",2);
        if(isError())
            return;
        link->disconnect();
    }
    delete link;
}

dReadersMSKANEXCUSTOM::dReadersMSKANEXCUSTOM()
{
    filterA=(char*)NULL;
    filterB=(char*)NULL;
    link=NULL;
}

dReadersMSKANEXCUSTOM::~dReadersMSKANEXCUSTOM()
{
    deinitialize();
    link=NULL;
}
    
FXString dReadersMSKANEXCUSTOM::getManufacturer(void)
{
    return "Labsystems";
}

FXString dReadersMSKANEXCUSTOM::getModel(void)
{
    return "Multiskan EX/MS (Custom Filter Set)";
}

int dReadersMSKANEXCUSTOM::getCode(void)
{
    return READER_MSKANEXCUSTOM;
}

sReadersSettings dReadersMSKANEXCUSTOM::getSettings(void)
{
    sReadersSettings ret;
    ret.baudRate=BAUD9600;
    ret.bits=BITS8;
    ret.parity=PARITYNONE;
    ret.stopBits=STOPBITS1;
    ret.flow=FLOWNONE;
    ret.timeout=20;
    return ret;
}

FXString dReadersMSKANEXCUSTOM::getFilterA(void)
{
    return filterA;
}

FXString dReadersMSKANEXCUSTOM::getFilterB(void)
{
    return filterB;
}

void dReadersMSKANEXCUSTOM::setFilterA(const FXString &prFilter)
{
    filterA=prFilter;
}

void dReadersMSKANEXCUSTOM::setFilterB(const FXString &prFilter)
{
    filterB=prFilter;
}

double *dReadersMSKANEXCUSTOM::acquire(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    double *ret=(double *)malloc(96*sizeof(double));
    char *res=(char *)malloc(10*sizeof(char));
    
    if(!res || !ret)
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        {deinitialize();return NULL;}
    }
    memset(ret,0,sizeof(double)*96);
    if(link==NULL)    
        if(!initialize(prSerialDevice,prBaudRate,prBits,prParity,prStopBits,prFlow,prTimeout))
            {deinitialize();return NULL;}

    link->flush();
    
    int fla = 1, flb = FXIntVal(filterB);
    
    FXbool r=FXInputDialog::getInteger(fla,
                                       oWinMain,
                                       oLanguage->getText("str_question"),
                                       oLanguage->getText("str_question_filterA") + " " + filterA,
                                       NULL,
                                       1, 8);

    if(r == false)
        return false;
    
    FXString filters="F"+FXStringVal(fla)+"\n";
    if(fla>0)
    {
        link->output(filters.text(),filters.length());
        if(isError())
            return false;
    }
    
    link->output("P\n",2); 

    for(int j=0;j<8;j++)
    {
        for(int i=0;i<12;i++)
        {
            if(link->input(res,1)==-1 ||
               link->input(res+1,1)==-1 ||
               link->input(res+2,1)==-1 ||
               link->input(res+3,1)==-1 ||
               link->input(res+4,1)==-1 ||
               link->input(res+5,1)==-1)
            {
                oApplicationManager->endWaitCursor();
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
                break;
            }
            res[6]=0;
            ret[i+j*12]=FXFloatVal(res);
        }
        if(link->input(res,1)==-1 || res[0]!='\r')
            break;
        if(link->input(res,1)==-1 || res[0]!='\n')
            break;
    }

    if(flb)
    {
        flb = 1;
        r=FXInputDialog::getInteger(flb,
                                    oWinMain,
                                    oLanguage->getText("str_question"),
                                    oLanguage->getText("str_question_filterB") + " " + filterB,
                                    NULL,
                                    1, 8);
        if(r == false)
            return false;

        filters="F"+FXStringVal(flb)+"\n";
        link->output(filters.text(),filters.length());
        if(isError())
            return false;
        
        link->output("P\n",2); 
    
        for(int j=0;j<8;j++)
        {
            for(int i=0;i<12;i++)
            {
                if(link->input(res,1)==-1 ||
                   link->input(res+1,1)==-1 ||
                   link->input(res+2,1)==-1 ||
                   link->input(res+3,1)==-1 ||
                   link->input(res+4,1)==-1 ||
                   link->input(res+5,1)==-1)
                {
                    oApplicationManager->endWaitCursor();
                    FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
                    break;
                }
                res[6]=0;
                ret[i+j*12]-=FXFloatVal(res);
            }
            if(link->input(res,1)==-1 || res[0]!='\r')
                break;
            if(link->input(res,1)==-1 || res[0]!='\n')
                break;
        }
    }
    
    link->flush();
    deinitialize();
    free(res);
    for(int yo=0;yo<96;yo++)
        if(ret[yo]==-0)ret[yo]=0;
    
    return ret;
}

