#include <stdlib.h>

#include "engine.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "dReadersEMAX.h"

FXbool dReadersEMAX::initialize(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    char res[4];
    int res2;
    int count=0;
    
    if(link!=NULL)
        deinitialize();
    
    link=new cSerialLink();

    if(!link->connect(prSerialDevice,prBaudRate,prBits,prParity,prStopBits,prFlow,prTimeout))
        return false;
    if(link->output("\x00d",1)==-1)
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_write").text());
        return false;
    }
    if(!link->setTimeouts(3))
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
        return false;
    }
    res2=link->input(res,4);
    if(!link->setTimeouts(prTimeout))
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
        return false;
    }
    if(-1==res2 || count++>2 || res[0]==0)
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
        return false;
    }
    return true;
}

void dReadersEMAX::deinitialize(void)
{
    if(link!=NULL)
        link->disconnect();
    delete link;
}

dReadersEMAX::dReadersEMAX()
{
    filterA=(char*)NULL;
    filterB=(char*)NULL;
    link=NULL;
}

dReadersEMAX::~dReadersEMAX()
{
    deinitialize();
    link=NULL;
}
    
FXString dReadersEMAX::getManufacturer(void)
{
    return "Molecular Devices";
}

FXString dReadersEMAX::getModel(void)
{
    return "Emax";
}

int dReadersEMAX::getCode(void)
{
    return READER_EMAX;
}

sReadersSettings dReadersEMAX::getSettings(void)
{
    sReadersSettings ret;
    ret.baudRate=BAUD9600;
    ret.bits=BITS8;
    ret.parity=PARITYNONE;
    ret.stopBits=STOPBITS1;
    ret.flow=FLOWNONE;
    ret.timeout=40;
    return ret;
}

FXString dReadersEMAX::getFilterA(void)
{
    return filterA;
}

FXString dReadersEMAX::getFilterB(void)
{
    return filterB;
}

void dReadersEMAX::setFilterA(const FXString &prFilter)
{
    filterA=prFilter;
}

void dReadersEMAX::setFilterB(const FXString &prFilter)
{
    filterB=prFilter;
}

double *dReadersEMAX::acquire(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    double *ret=(double *)malloc(96*sizeof(double));
    char res[10];
    
    if(!ret)
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        {deinitialize();return NULL;}
    }
    memset(ret,0,sizeof(double)*96);
    if(link==NULL)    
        if(!initialize(prSerialDevice,prBaudRate,prBits,prParity,prStopBits,prFlow,prTimeout))
            {deinitialize();return NULL;}

    link->flush();
    
    int matrix[4][2]={{405,1},{450,2},{490,3},{650,4}};
    int min,choice,n=FXIntVal(filterA),fla,flb;
    
    min=abs(n-matrix[0][0]);
    choice=matrix[0][1];
    
    if(n>0)
        for(int i=0;i<4;i++)
        {
            if(abs(n-matrix[i][0])<min)
            {
                min=abs(n-matrix[i][0]);
                choice=matrix[i][1];
            }
        }
    if((min>20) && (n>0))
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),
                            FXStringFormat("%s %dnm",oLanguage->getText("str_serialerr_nofil").text(),n).text());
        {deinitialize();return NULL;}
    }
    fla=choice;

    n=FXIntVal(filterB);
    min=abs(n-matrix[0][0]);
    choice=0;
    
    if(n>0)
        for(int i=0;i<4;i++)
        {
            if(abs(n-matrix[i][0])<min)
            {
                min=abs(n-matrix[i][0]);
                choice=matrix[i][1];
            }
        }
    if((min>20) && (n>0))
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),
                            FXStringFormat("%s %dnm",oLanguage->getText("str_serialerr_nofil").text(),n).text());
        {deinitialize();return NULL;}
    }
    flb=choice;
    FXString filters=(flb>0)?"DWV"+FXStringVal(fla)+","+FXStringVal(flb):"SWV"+FXStringVal(fla);
    link->output("RMT",3,true); 
    if(fla>0)
        link->output(filters.text(),filters.length(),true);
    link->output("ASC",3,true);
    link->output("NEC",3,true); 
    link->output("RNE",3,true); 

    for(int i=0;i<96;i++)
    {
        if(link->input(res,i==95?6:7)==-1)
            break;
        
        res[6]=0;
        ret[i]=FXDoubleVal(res);
    }
    
    link->flush();
    
    link->output("SWV1",4,true);
    link->output("BIN",3,true);
    link->output("LCL",3,true);
    
    link->flush();
    deinitialize();
    
    for(int yo=0;yo<96;yo++)
        if(ret[yo]==-0)ret[yo]=0;
    
    return ret;
}

