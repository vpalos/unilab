#include <stdlib.h>
#include <strings.h>
#include "engine.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "dReadersMSKANEXA.h"

FXbool dReadersMSKANEXA::isError(void)
{
    char res[5];
    if(link->input(res,4)==-1 || strncmp(res,"OK\r\n",4))
    {
#ifdef WIN32
        Sleep(2);
#else
        sleep(2);
#endif
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
        return true;
    }
    return false;
}

FXbool dReadersMSKANEXA::initialize(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    if(link!=NULL)
        deinitialize();
    link=new cSerialLink();
    if(!link->connect(prSerialDevice,prBaudRate,prBits,prParity,prStopBits,prFlow,prTimeout))
        return false;
    link->output("R\n",2);
    if(isError())
        return false;
    link->output("I\n",2);
    if(isError())
        return false;
    link->output("E0\n",3);
    if(isError())
        return false;
    return true;
}

void dReadersMSKANEXA::deinitialize(void)
{
    if(link!=NULL)
    {
    	link->output("Q\n",2);
        if(isError())
            return;
        link->disconnect();
    }
    delete link;
}

dReadersMSKANEXA::dReadersMSKANEXA()
{
    filterA=(char*)NULL;
    filterB=(char*)NULL;
    link=NULL;
}

dReadersMSKANEXA::~dReadersMSKANEXA()
{
    deinitialize();
    link=NULL;
}
    
FXString dReadersMSKANEXA::getManufacturer(void)
{
    return "Labsystems";
}

FXString dReadersMSKANEXA::getModel(void)
{
    return "Multiskan EX/MS (Standard Filter Set A)";
}

int dReadersMSKANEXA::getCode(void)
{
    return READER_MSKANEXA;
}

sReadersSettings dReadersMSKANEXA::getSettings(void)
{
    sReadersSettings ret;
    ret.baudRate=BAUD9600;
    ret.bits=BITS8;
    ret.parity=PARITYNONE;
    ret.stopBits=STOPBITS1;
    ret.flow=FLOWNONE;
    ret.timeout=20;
    return ret;
}

FXString dReadersMSKANEXA::getFilterA(void)
{
    return filterA;
}

FXString dReadersMSKANEXA::getFilterB(void)
{
    return filterB;
}

void dReadersMSKANEXA::setFilterA(const FXString &prFilter)
{
    filterA=prFilter;
}

void dReadersMSKANEXA::setFilterB(const FXString &prFilter)
{
    filterB=prFilter;
}

double *dReadersMSKANEXA::acquire(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    double *ret=(double *)malloc(96*sizeof(double));
    char *res=(char *)malloc(10*sizeof(char));
    
    if(!res || !ret)
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        {deinitialize();return NULL;}
    }
    memset(ret,0,sizeof(double)*96);
    if(link==NULL)    
        if(!initialize(prSerialDevice,prBaudRate,prBits,prParity,prStopBits,prFlow,prTimeout))
            {deinitialize();return NULL;}

    link->flush();
    
    int matrix[4][2]={{405,1},{450,2},{490,3},{630,4}};
    int min,choice,n=FXIntVal(filterA),fla,flb;
    
    min=abs(n-matrix[0][0]);
    choice=matrix[0][1];
    
    if(n>0)
        for(int i=0;i<4;i++)
        {
            if(abs(n-matrix[i][0])<min)
            {
                min=abs(n-matrix[i][0]);
                choice=matrix[i][1];
            }
        }
    if((min>10) && (n>0))
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),
                            FXStringFormat("%s %dnm",oLanguage->getText("str_serialerr_nofil").text(),n).text());
        {deinitialize();return NULL;}
    }
    fla=choice;

    n=FXIntVal(filterB);
    min=abs(n-matrix[0][0]);
    choice=0;
    
    if(n>0)
        for(int i=0;i<4;i++)
        {
            if(abs(n-matrix[i][0])<min)
            {
                min=abs(n-matrix[i][0]);
                choice=matrix[i][1];
            }
        }
    if((min>10) && (n>0))
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),
                            FXStringFormat("%s %dnm",oLanguage->getText("str_serialerr_nofil").text(),n).text());
        {deinitialize();return NULL;}
    }
    flb=choice;
    
    
    FXString filters="F"+FXStringVal(fla)+"\n";
    if(fla>0)
    {
        link->output(filters.text(),filters.length());
        if(isError())
            return false;
    }
    
    link->output("P\n",2); 

    for(int j=0;j<8;j++)
    {
        for(int i=0;i<12;i++)
        {
            if(link->input(res,1)==-1 ||
               link->input(res+1,1)==-1 ||
               link->input(res+2,1)==-1 ||
               link->input(res+3,1)==-1 ||
               link->input(res+4,1)==-1 ||
               link->input(res+5,1)==-1)
            {
                oApplicationManager->endWaitCursor();
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
                break;
            }
            res[6]=0;
            ret[i+j*12]=FXFloatVal(res);
        }
        if(link->input(res,1)==-1 || res[0]!='\r')
            break;
        if(link->input(res,1)==-1 || res[0]!='\n')
            break;
    }

    if(flb)
    {
        filters="F"+FXStringVal(flb)+"\n";
        link->output(filters.text(),filters.length());
        if(isError())
            return false;
        
        link->output("P\n",2); 
    
        for(int j=0;j<8;j++)
        {
            for(int i=0;i<12;i++)
            {
                if(link->input(res,1)==-1 ||
                   link->input(res+1,1)==-1 ||
                   link->input(res+2,1)==-1 ||
                   link->input(res+3,1)==-1 ||
                   link->input(res+4,1)==-1 ||
                   link->input(res+5,1)==-1)
                {
                    oApplicationManager->endWaitCursor();
                    FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
                    break;
                }
                res[6]=0;
                ret[i+j*12]-=FXFloatVal(res);
            }
            if(link->input(res,1)==-1 || res[0]!='\r')
                break;
            if(link->input(res,1)==-1 || res[0]!='\n')
                break;
        }
    }
    
    link->flush();
    deinitialize();
    free(res);
    for(int yo=0;yo<96;yo++)
        if(ret[yo]==-0)ret[yo]=0;
    
    return ret;
}

