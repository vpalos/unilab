#include <stdlib.h>
#include <string.h>

#include "engine.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "dReadersTSUNRISE.h"

FXbool dReadersTSUNRISE::initialize(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    char res[100];
    
    if(link!=NULL)
        deinitialize();
    
    link=new cSerialLink();

    if(!link->connect(prSerialDevice,prBaudRate,prBits,prParity,prStopBits,prFlow,prTimeout))
        return false;
    
    if(!link->setTimeouts(5))
        return false;
    if(sendCommand("!",res)==-1)
        return false;
    if(strcmp(res,"+SU")!=0)
        return false;
    if(sendCommand("??",res)==-1)
        return false;
    if(strcmp(res,"S")!=0)
        return false;
    if(!link->setTimeouts(prTimeout))
        return false;
    return true;
}

void dReadersTSUNRISE::deinitialize(FXint prState)
{
    char res[10];
    
    if(prState)
    {
        if(prState==2)
        {
            if(sendCommand("??",res)==-1)
                return;
            if(sendCommand("TR0",res)==-1)
                return;
            do{
                if(sendCommand("??",res)==-1)
                    return;
            }while((strcmp(res,"B")==0));
            oApplicationManager->endWaitCursor();
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),"Remove the plate to continue...");
            oApplicationManager->beginWaitCursor();
            while(oApplicationManager->peekEvent())
                oApplicationManager->runOneEvent(false);

        }
        if(sendCommand("TR1",res)==-1)
            return;
        do{
            if(sendCommand("??",res)==-1)
                return;
        }while((strcmp(res,"B")==0));
    }
    if(link!=NULL)
        link->disconnect();
    delete link;
}

FXuchar dReadersTSUNRISE::makeChecksum(const FXString &prCommand)
{
    FXuchar ret=0;
    for(int i=0;i<prCommand.length();i++)
        ret^=prCommand.at(i);
    return ret;
}

int dReadersTSUNRISE::sendCommand(const FXString &prCommand,char * prBuffer,FXbool prSkipResults)
{
    FXString res;
    FXint plus=2;
    if(prCommand=="!")
        res=FXStringFormat("%c%s%c",0x02,prCommand.text(),0x0D);
    else
    {
        plus=4;
        res=FXStringFormat("%c%s%c",0x02,prCommand.text(),0x03);
        res=FXStringFormat("%s%c%c",res.text(),makeChecksum(res),0x0D);
    }
    char digit;
    if(link==NULL)
        return -1;
    link->flush();
    link->output(res.text(),plus+prCommand.length());
    
    if(link->input(&digit,1)==-1 || digit!=0x02)
        return -1;

    int i=-1,limit=0x03;
    if(prCommand=="!")
        limit=0x0D;
    while(digit!=limit)
    {
        i++;
        if(link->input(&digit,1)==-1)
            return -1;
        if(!prSkipResults)
            prBuffer[i]=digit;
    }
    prBuffer[i]=0;
    if(limit!=0x0D)
        while(digit!=0x0D)
            if(link->input(&digit,1)==-1)
                return -1;
    link->flush();
    return i;
}

dReadersTSUNRISE::dReadersTSUNRISE()
{
    filterA="";
    filterB="";
    link=NULL;
}

dReadersTSUNRISE::~dReadersTSUNRISE()
{
    deinitialize();
    link=NULL;
}
    
FXString dReadersTSUNRISE::getManufacturer(void)
{
    return "Tecan";
}

FXString dReadersTSUNRISE::getModel(void)
{
    return "Sunrise";
}

int dReadersTSUNRISE::getCode(void)
{
    return READER_TSUNRISE;
}

sReadersSettings dReadersTSUNRISE::getSettings(void)
{
    sReadersSettings ret;
    ret.baudRate=BAUD9600;
    ret.bits=BITS8;
    ret.parity=PARITYNONE;
    ret.stopBits=STOPBITS1;
    ret.flow=FLOWNONE;
    ret.timeout=50;
    return ret;
}

FXString dReadersTSUNRISE::getFilterA(void)
{
    return filterA;
}

FXString dReadersTSUNRISE::getFilterB(void)
{
    return filterB;
}

void dReadersTSUNRISE::setFilterA(const FXString &prFilter)
{
    filterA=prFilter;
}

void dReadersTSUNRISE::setFilterB(const FXString &prFilter)
{
    filterB=prFilter;
}

double *dReadersTSUNRISE::acquire(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    double *ret=(double *)malloc(96*sizeof(double));
    int flb=0;
    char *res=(char*)malloc(500);
    FXString s;
    
    if(!ret || !res)
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        {
            while(oApplicationManager->peekEvent())
                oApplicationManager->runOneEvent(false);
            deinitialize();
            return NULL;
        }
    }
    memset(ret,0,sizeof(double)*96);
    if(link==NULL)    
        if(!initialize(prSerialDevice,prBaudRate,prBits,prParity,prStopBits,prFlow,prTimeout))
        {
            oApplicationManager->endWaitCursor();
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
            while(oApplicationManager->peekEvent())
                oApplicationManager->runOneEvent(false);
            deinitialize();
            return NULL;
        }
    if(sendCommand("TR0",res)==-1)
        return NULL;
    do{
        if(sendCommand("??",res)==-1)
            return NULL;
    }while((strcmp(res,"B")==0));

    oApplicationManager->endWaitCursor();
    if(MBOX_CLICKED_CANCEL==FXMessageBox::warning(oWinMain,MBOX_OK_CANCEL,oLanguage->getText("str_warning").text(),"Load plate to continue..."))
    {
        while(oApplicationManager->peekEvent())
            oApplicationManager->runOneEvent(false);
        deinitialize(1);
        return NULL;
    }
    oApplicationManager->beginWaitCursor();
    while(oApplicationManager->peekEvent())
        oApplicationManager->runOneEvent(false);
    
    if(sendCommand("TR1",res)==-1)
        return NULL;
    do{
        if(sendCommand("??",res)==-1)
            return NULL;
    }while((strcmp(res,"B")==0));

    if(!filterA.empty())
    {
        int fla=FXIntVal(filterA),a[6],b,bb,choice;
        if(!filterB.empty())
            flb=FXIntVal(filterB);
        if(sendCommand("GW",res)==-1)
            return NULL;
        
        if(strcmp(res,"300-700")!=0)
        {
            int c=FXStringFormat("%s",res).contains('-');
            bool interval=true;
            
            if(c==0)
                interval = false;
            
            if(!interval)
            {
                c=FXStringFormat("%s",res).contains(' ');
                if(c==3)
                    sscanf(res,"%d %d %d %d",&a[0],&a[1],&a[2],&a[3]);
                else
                    sscanf(res,"%d %d %d %d %d %d",&a[0],&a[1],&a[2],&a[3],&a[4],&a[5]);
                
                b=abs(a[0]-fla);
                choice=a[0];
                for(int t=1;t<=c;t++)
                {
                    bb=abs(a[t]-fla);
                    if(b>bb)
                    {
                        b=bb;
                        choice=a[t];
                    }
                }
                if(b>20)
                {
                    oApplicationManager->endWaitCursor();
                    FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),
                                        FXStringFormat("%s %dnm",oLanguage->getText("str_serialerr_nofil").text(),fla).text());
                    {deinitialize();return NULL;}
                }
                fla=choice;
            }
            else
            {
                sscanf(res,"%d-%d",&a[0],&a[1]);
                if((fla < a[0]) || (fla > a[1]))
                {
                    oApplicationManager->endWaitCursor();
                    FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),
                                        FXStringFormat("%s %dnm",oLanguage->getText("str_serialerr_nofil").text(),fla).text());
                    {deinitialize();return NULL;}
                }
            }
            
            if(flb)
            {
                if(!interval)
                {
                    b=abs(a[0]-flb);
                    choice=a[0];
                    for(int t=1;t<=c;t++)
                    {
                        bb=abs(a[t]-flb);
                        if(b>bb)
                        {
                            b=bb;
                            choice=a[t];
                        }
                    }
                    if(b>20)
                    {
                        oApplicationManager->endWaitCursor();
                        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),
                                            FXStringFormat("%s %dnm",oLanguage->getText("str_serialerr_nofil").text(),flb).text());
                        {deinitialize();return NULL;}
                    }
                    flb=choice;
                }
                else
                {
                    if((flb < a[0]) || (flb > a[1]))
                    {
                        oApplicationManager->endWaitCursor();
                        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),
                                            FXStringFormat("%s %dnm",oLanguage->getText("str_serialerr_nofil").text(),flb).text());
                        {deinitialize();return NULL;}
                    }
                }
            }
        }
        
        if(sendCommand(FXStringFormat("SW%d",fla),res)==-1)
            return NULL;
        do{
            if(sendCommand("??",res)==-1)
                return NULL;
        }while(strcmp(res,"B")==0);
    }
    
    if(sendCommand("SM0",res)==-1)
        return NULL;
    if(strcmp(res,"+")!=0)
        return NULL;
    do{
        if(sendCommand("??",res)==-1)
            return NULL;
    }while((strcmp(res,"B")==0) || (strcmp(res,"M")==0));
    if(strcmp(res,"D")!=0)
        return NULL;

    if(sendCommand("DA",res)==-1)
        return NULL;
    
    FXString res2(res);
    for(int i=0;i<8;i++)
    {
        res2=res2.after(0x0D);
        for(int j=0;j<12;j++)
            *(ret+i*12+j)=FXFloatVal(res2.mid(j*5,5))/1000;
    }
    do{
        if(sendCommand("??",res)==-1)
            return NULL;
    }while((strcmp(res,"B")==0));
    

    if(flb)
    {
        if(sendCommand(FXStringFormat("SW%d",flb),res)==-1)
            return NULL;
        do{
            if(sendCommand("??",res)==-1)
                return NULL;
        }while(strcmp(res,"B")==0);
        
        if(sendCommand("SM0",res)==-1)
            return NULL;
        if(strcmp(res,"+")!=0)
            return NULL;
        do{
            if(sendCommand("??",res)==-1)
                return NULL;
        }while((strcmp(res,"B")==0) || (strcmp(res,"M")==0));
        if(strcmp(res,"D")!=0)
            return NULL;
    
        if(sendCommand("DA",res)==-1)
            return NULL;
        
        FXString res2(res);
        for(int i=0;i<8;i++)
        {
            res2=res2.after(0x0D);
            for(int j=0;j<12;j++)
                *(ret+i*12+j)-=FXFloatVal(res2.mid(j*5,5))/1000;
        }
        do{
            if(sendCommand("??",res)==-1)
                return NULL;
        }while((strcmp(res,"B")==0));
    }
    
    deinitialize(2);
    free(res);
    for(int yo=0;yo<96;yo++)
        if(ret[yo]==-0)ret[yo]=0;
    return ret;
}
