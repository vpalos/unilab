#ifndef DREADERSMSKANEXCUSTOM_H
#define DREADERSMSKANEXCUSTOM_H

#include <fx.h>

#include "cReadersManager.h"

#define READER_MSKANEXCUSTOM 10

class dReadersMSKANEXCUSTOM :public cReadersObject
{
    private:
    protected:
        cSerialLink *link;
        FXString filterA,filterB;
        
        virtual FXbool isError(void);
        
        virtual FXbool initialize(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout);
        virtual void deinitialize(void);
    
    public:
        dReadersMSKANEXCUSTOM();
        virtual ~dReadersMSKANEXCUSTOM();
        
        virtual FXString getManufacturer(void);
        virtual FXString getModel(void);
        virtual int getCode(void);
        virtual sReadersSettings getSettings(void);
        
        virtual FXString getFilterA(void);
        virtual FXString getFilterB(void);
        virtual void setFilterA(const FXString &prFilter);
        virtual void setFilterB(const FXString &prFilter);

        virtual double *acquire(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout);
};

#endif 
