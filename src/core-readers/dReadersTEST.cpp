#include <stdlib.h>

#include "cLanguage.h"
#include "cWinMain.h"
#include "engine.h"
#include "dReadersTEST.h"

dReadersTEST::dReadersTEST()
{
}

dReadersTEST::~dReadersTEST()
{
}
    
FXString dReadersTEST::getManufacturer(void)
{
    return "---";
}

FXString dReadersTEST::getModel(void)
{
    return "Test reader";
}

int dReadersTEST::getCode(void)
{
    return READER_TEST;
}

sReadersSettings dReadersTEST::getSettings(void)
{
    sReadersSettings ret;
    ret.baudRate=BAUD9600;
    ret.bits=BITS8;
    ret.parity=PARITYNONE;
    ret.stopBits=STOPBITS1;
    ret.flow=FLOWNONE;
    ret.timeout=0;
    return ret;
}

FXString dReadersTEST::getFilterA(void)
{
    return "";
}

FXString dReadersTEST::getFilterB(void)
{
    return "";
}

void dReadersTEST::setFilterA(const FXString &prFilter)
{
}

void dReadersTEST::setFilterB(const FXString &prFilter)
{
}

double *dReadersTEST::acquire(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    double *ret=(double *)malloc(96*sizeof(double));
    if(!ret)
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }
    memset(ret,0,sizeof(double)*96);
    for(int i=0;i<12;i++)
        ret[i]=-0.001;
    for(int i=12;i<24;i++)
        ret[i]=+4.001;
    for(int i=24;i<96;i++)
        ret[i]=(i-12)/12*0.5;
    return ret;
}

