#include <ctype.h>
#include <stdlib.h>

#include "engine.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "dReaders303PLUS.h"

FXbool dReaders303PLUS::initialize(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    if(link!=NULL)
        deinitialize();
    
    link=new cSerialLink();

    if(!link->connect(prSerialDevice,prBaudRate,prBits,prParity,prStopBits,prFlow,prTimeout))
        return false;
    
    return true;
}

void dReaders303PLUS::deinitialize(void)
{
    if(link!=NULL)
        link->disconnect();
    delete link;
}

dReaders303PLUS::dReaders303PLUS()
{
    filterA="";
    filterB="";
    link=NULL;
}

dReaders303PLUS::~dReaders303PLUS()
{
    deinitialize();
    link=NULL;
}
    
FXString dReaders303PLUS::getManufacturer(void)
{
    return "StatFax";
}

FXString dReaders303PLUS::getModel(void)
{
    return "303 Plus";
}

int dReaders303PLUS::getCode(void)
{
    return READER_303PLUS;
}

sReadersSettings dReaders303PLUS::getSettings(void)
{
    sReadersSettings ret;
    ret.baudRate=BAUD2400;
    ret.bits=BITS8;
    ret.parity=PARITYNONE;
    ret.stopBits=STOPBITS1;
    ret.flow=FLOWNONE;
    ret.timeout=60;
    return ret;
}

FXString dReaders303PLUS::getFilterA(void)
{
    return filterA;
}

FXString dReaders303PLUS::getFilterB(void)
{
    return filterB;
}

void dReaders303PLUS::setFilterA(const FXString &prFilter)
{
    filterA=prFilter;
}

void dReaders303PLUS::setFilterB(const FXString &prFilter)
{
    filterB=prFilter;
}

double *dReaders303PLUS::acquire(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    double *ret=(double *)malloc(96*sizeof(double));
    double d;
    char digit,*res=(char *)malloc(4096*sizeof(char));
    int i,k,l,strip12=false;
    
    if(!ret || !res)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        {deinitialize();return NULL;}
    }
    memset(ret,0,sizeof(double)*96);
    if(link==NULL)    
        if(!initialize(prSerialDevice,prBaudRate,prBits,prParity,prStopBits,prFlow,prTimeout))
            {deinitialize();return NULL;}

    link->flush();
    int fla=FXIntVal(filterA);
    int flb=FXIntVal(filterB);
    if(!filterA.empty())
    {
        if(fla<190 || fla>1000)
            filterA="";
        if(flb<190 || flb>1000)
            filterB="";
    }
    oApplicationManager->endWaitCursor();
    if(MBOX_CLICKED_CANCEL==FXMessageBox::warning(oWinMain,FX::MBOX_OK_CANCEL,"Preparing data...",("Preparing to read data...\n\n"+
        (filterA.empty()?"":"Please use "+(filterB.empty()?"single wavelength filtering of "+filterA:
        "double wavelength filtering of "+filterA+" and "+filterB)+".\n\n")+
        "Reading with the StatFax 303 Plus:\n\n"
        "1. Press the \"ABS (1/A)\" button on the reader.\n"+
        "2. Follow the instructions on the reader display to select filtering (until \"SET CARRIER\" message appears).\n"+
        "3. Press the \"Ok\" button below.\n"+
        "4. Press \"Enter\" button on the reader to start reading.\n"+
        "5. Repeat Step 4 for as many as 12 readings(for 8-well vertical strips) and\n    as many as 8 readings (for 12-well horizontal strips).\n"+
        "6. When done reading all the strips press the \"Clear\" button on the reader twice to end the sequence.\n\n"+
        "If you would like to cancel the operation please press \"Cancel\".").text()))
    {
        free(res);
        link->flush();
        deinitialize();
        return false;
    }
    oApplicationManager->beginWaitCursor();
    digit=0;
    for(i=0;i<4096 && digit!=0x1B;i++)
    {
        if(link->input(&digit,1)==-1)
            {deinitialize();return NULL;}
        if(digit==0)
            {deinitialize();return NULL;}
        res[i]=digit;
    }
    res[i]=0;
    FXString res3=FXStringFormat("%s",res).left(i);
    if((k=res3.find("Strip:",0))==-1)
        {deinitialize();return NULL;}
    if(isalpha(res3.at(k+6)))
        strip12=true;
    FXString res4;
    l=0;
    while(l<(strip12?8:12) && (k=res3.find("Carrier position ",k))!=-1)
    {
        k+=22;
        for(i=0;i<(strip12?12:8);i++)
        {
            res4=res3.mid(k+5,6);
            if(res4.empty())
                {deinitialize();return NULL;}
            if(res4.mid(0,1)==">")
                d=3.001;
            else if(res4.mid(0,1)=="<")
                d=-0.001;
            else d=(double)FXFloatVal(res4);
            *(ret+(strip12?l*12+i:i*12+l))=d;
            k+=22;
        }
        l++;
    }
    
    free(res);
    link->flush();
    deinitialize();
    for(int yo=0;yo<96;yo++)
        if(ret[yo]==-0)ret[yo]=0;
    return ret;
}

