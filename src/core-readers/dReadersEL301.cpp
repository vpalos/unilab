#include <stdlib.h>

#include "engine.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "dReadersEL301.h"

FXbool dReadersEL301::initialize(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    if(link!=NULL)
        deinitialize();
    
    link=new cSerialLink();

    if(!link->connect(prSerialDevice,prBaudRate,prBits,prParity,prStopBits,prFlow,prTimeout))
        return false;
    
    return true;
}

void dReadersEL301::deinitialize(void)
{
    if(link!=NULL)
        link->disconnect();
    delete link;
}

dReadersEL301::dReadersEL301()
{
    filterA="";
    filterB="";
    link=NULL;
}

dReadersEL301::~dReadersEL301()
{
    deinitialize();
    link=NULL;
}
    
FXString dReadersEL301::getManufacturer(void)
{
    return "Bio-Tek Instruments";
}

FXString dReadersEL301::getModel(void)
{
    return "EL301";
}

int dReadersEL301::getCode(void)
{
    return READER_EL301;
}

sReadersSettings dReadersEL301::getSettings(void)
{
    sReadersSettings ret;
    ret.baudRate=BAUD1200;
    ret.bits=BITS8;
    ret.parity=PARITYNONE;
    ret.stopBits=STOPBITS2;
    ret.flow=FLOWCRTSCTS;
    ret.timeout=60;
    return ret;
}

FXString dReadersEL301::getFilterA(void)
{
    return filterA;
}

FXString dReadersEL301::getFilterB(void)
{
    return filterB;
}

void dReadersEL301::setFilterA(const FXString &prFilter)
{
    filterA=prFilter;
}

void dReadersEL301::setFilterB(const FXString &prFilter)
{
    filterB=prFilter;
}

double *dReadersEL301::acquire(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    double *ret=(double *)malloc(96*sizeof(double));
    char digit,res[500];
    double res2[16];
    int i,k;
    
    if(!ret)
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        {deinitialize();return NULL;}
    }
    memset(ret,0,sizeof(double)*96);
    if(link==NULL)    
        if(!initialize(prSerialDevice,prBaudRate,prBits,prParity,prStopBits,prFlow,prTimeout))
            {deinitialize();return NULL;}

    link->flush();
    int fla=FXIntVal(filterA);
    int flb=FXIntVal(filterB);
    if(!filterA.empty())
    {
        if(fla<190 || fla>1000)
            filterA="";
        if(flb<190 || flb>1000)
            filterB="";
    }
    oApplicationManager->endWaitCursor();
    if(MBOX_CLICKED_CANCEL==FXMessageBox::warning(oWinMain,FX::MBOX_OK_CANCEL,"Preparing data...",("Preparing to read data...\n\n"+
        (filterA.empty()?"":"Please use "+(filterB.empty()?"single wavelength filtering of "+filterA:
        "double wavelength filtering of "+filterA+" and "+filterB)+".\n\n")+"Manually read the wells (you will repeat this step for each strip on the plate),\n"+
        "then click on the \"Ok\" button below and then press \"DATA OUT\" on the reader.\n\n"+
        "If you would like to cancel the operation please press \"Cancel\".").text()))
    {
        link->flush();
        deinitialize();
        return false;
    }
    oApplicationManager->beginWaitCursor();
    while(oApplicationManager->peekEvent())
        oApplicationManager->runOneEvent();
    #ifndef WIN32    
    for(i=0;i<1000;i++)
        oApplicationManager->runOneEvent();
    #endif    
    int push=0,remaining=100;
    
    while(remaining>0)
    {
        oApplicationManager->endWaitCursor();
        if((remaining<20) && (MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),
            "Choose \"Yes\" if you want to continue reading more strips for this plate\nor \"No\" is you want to finish the reading for this plate.")))
            break;
        oApplicationManager->beginWaitCursor();
        while(oApplicationManager->peekEvent())
            oApplicationManager->runOneEvent();
        #ifndef WIN32    
        for(i=0;i<1000;i++)
            oApplicationManager->runOneEvent();
        #endif    
            
        digit=0;
        for(i=0;i<500 && digit!=0x04;i++)
        {
            if(link->input(&digit,1)==-1)
                {deinitialize();return NULL;}
            if(digit==0)
                {deinitialize();return NULL;}
            res[i]=digit;
        }
        res[i]=0;
        FXString res4,res3=FXStringFormat("%s",res).left(i);
        k=res3.find(",A1 ,",0);
        i=0;
        while(i<16)
        {
            k=res3.find(",",k+1)+1;
            if(k==0)
                break;
            res4=res3.mid(k,6);
            if(res4.mid(1,5)=="+.+++")
                res2[i++]=30000.000;
            else if(res4.mid(1,5)=="-.---")
                res2[i++]=-30000.000;
            else
                res2[i++]=FXFloatVal(res4);
            k+=7;
            if((k=res3.find(",",k))==-1)
                break;
        }
        if(remaining>=20)
        {
            if(i==12)remaining=8;
            else if(i==16)remaining=6;
            else remaining=12;
        }
        switch(i)
        {
            case 12:
                for(i=0;i<12;i++)
                    *(ret+i+push*12)=(double)res2[i];
                break;
            case 16:
                for(i=0;i<8;i++)
                    *(ret+i*12+push*2)=(double)res2[i];
                for(i=0;i<8;i++)
                    *(ret+i*12+1+push*2)=(double)res2[i+8];
                break;
            default:
                for(i=0;i<8;i++)
                    *(ret+i*12+push)=(double)res2[i];
                break;
        }
        remaining--;
        push++;
    }
    
    link->flush();
    deinitialize();
    
    for(int yo=0;yo<96;yo++)
        if(ret[yo]==-0)ret[yo]=0;
    
    return ret;
}

