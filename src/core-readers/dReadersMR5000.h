#ifndef DREADERSMR5000_H
#define DREADERSMR5000_H

#include <fx.h>

#include "cReadersManager.h"

#define READER_MR5000 3

class dReadersMR5000 :public cReadersObject
{
    private:
    protected:
        cSerialLink *link;
        FXString filterA,filterB;
        
        virtual FXbool initialize(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout);
        virtual void deinitialize(void);
        
        virtual int sendCommand(const FXString &prCommand,int prCount,char *prBuffer,FXbool prSkipResults=false);
    
    public:
        dReadersMR5000();
        virtual ~dReadersMR5000();
        
        virtual FXString getManufacturer(void);
        virtual FXString getModel(void);
        virtual int getCode(void);
        virtual sReadersSettings getSettings(void);
        
        virtual FXString getFilterA(void);
        virtual FXString getFilterB(void);
        virtual void setFilterA(const FXString &prFilter);
        virtual void setFilterB(const FXString &prFilter);

        virtual double *acquire(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout);
};

#endif 
