#include <stdlib.h>

#include "engine.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "dReadersPR2100.h"

FXbool dReadersPR2100::initialize(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    char res[100];
    
    if(link!=NULL)
        deinitialize();
    
    link=new cSerialLink();

    if(!link->connect(prSerialDevice,prBaudRate,prBits,prParity,prStopBits,prFlow,prTimeout))
        return false;
    
    if(!link->setTimeouts(3))
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
        return false;
    }
    if(sendCommand("71",2,res)==-1)
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
        return false;
    }
    if(!link->setTimeouts(prTimeout))
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
        return false;
    }
    return true;
}

void dReadersPR2100::deinitialize(void)
{
    char res[10];
    sendCommand("70",2,res);
    if(link!=NULL)
        link->disconnect();
    delete link;
}

int dReadersPR2100::sendCommand(const FXString &prCommand,int prCount,char * prBuffer,FXbool prSkipResults)
{
    FXString res=FXStringFormat("%c%s%c",0x02,prCommand.text(),0x03);
    char digit;
    int i=-1;
    if(link==NULL)
        return -1;
    digit=0x15;
    link->flush();
    while(digit==0x15)
    {
        link->output(res.text(),2+prCount);
        if(link->input(&digit,1)==-1)
            return -1;
    }
    if(digit!=0x06)
        return -1;
    
    if(prSkipResults)
        return 0;
    if(link->input(&digit,1)==-1 || digit!=0x05)
        return -1;

    link->output("\x006",1);
    if(link->input(&digit,1)==-1 || digit!=0x02)
        return -1;
    while(digit!=0x03)
    {
        i++;
        if(link->input(&digit,1)==-1)
            return -1;
        prBuffer[i]=digit;
    }
    prBuffer[i]=0;
    link->output("\x006",1);
    
    if(prBuffer[0]==0x15)
        return -1;
    link->flush();
    return i;
}

dReadersPR2100::dReadersPR2100()
{
    filterA="";
    filterB="";
    link=NULL;
}

dReadersPR2100::~dReadersPR2100()
{
    deinitialize();
    link=NULL;
}
    
FXString dReadersPR2100::getManufacturer(void)
{
    return "Sanofi / Dynatech";
}

FXString dReadersPR2100::getModel(void)
{
    return "PR2100 / MR700 / MR7000";
}

int dReadersPR2100::getCode(void)
{
    return READER_PR2100;
}

sReadersSettings dReadersPR2100::getSettings(void)
{
    sReadersSettings ret;
    ret.baudRate=BAUD9600;
    ret.bits=BITS8;
    ret.parity=PARITYEVEN;
    ret.stopBits=STOPBITS1;
    ret.flow=FLOWCRTSCTS;
    ret.timeout=50;
    return ret;
}

FXString dReadersPR2100::getFilterA(void)
{
    return filterA;
}

FXString dReadersPR2100::getFilterB(void)
{
    return filterB;
}

void dReadersPR2100::setFilterA(const FXString &prFilter)
{
    filterA=prFilter;
}

void dReadersPR2100::setFilterB(const FXString &prFilter)
{
    filterB=prFilter;
}

double *dReadersPR2100::acquire(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    double *ret=(double *)malloc(96*sizeof(double));
    char digit,*res=(char*)malloc(256);;
    FXString s;
    float a[8];
    int i,k,l;
    
    if(!ret || !res)
    {
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        {deinitialize();return NULL;}
    }
    memset(ret,0,sizeof(double)*96);
    if(link==NULL)    
        if(!initialize(prSerialDevice,prBaudRate,prBits,prParity,prStopBits,prFlow,prTimeout))
            {deinitialize();return NULL;}

    if(!filterA.empty())
    {
        if(sendCommand("33",2,res,true)==-1)
            {deinitialize();return NULL;}
        
        if(link->input(&digit,1)==-1 || digit!=0x05)
            {deinitialize();return NULL;}
        link->output("\x006",1);
        digit=k=0;
        while((digit!=0x03) && (k<256))
        {
            if(link->input(&digit,1)==-1)
                {deinitialize();return NULL;}
            res[k++]=digit;
        }
        res[k]=0;
        link->output("\x006",1);
        if(link->input(&digit,1)==-1 || digit!=0x05)
            {deinitialize();return NULL;}
        link->output("\x006",1);
        if(link->input(&digit,1)==-1 || digit!=0x02)
            {deinitialize();return NULL;}
        if(link->input(&digit,1)==-1 || digit!=0x06)
            {deinitialize();return NULL;}
        if(link->input(&digit,1)==-1 || digit!=0x03)
            {deinitialize();return NULL;}
        link->output("\x006",1);

        int min=1000,choice=0,fla=FXIntVal(filterA),flb=FXIntVal(filterB),filters[5];
        
        sscanf(res,"%d %d %d %d %d",&filters[0],&filters[1],&filters[2],&filters[3],&filters[4]);
        
        if(fla>0)
        {
            min=fla-filters[0];
            choice=1;
            
            for(i=0;i<5 && filters[i]>0;i++)
            {
                if(abs(fla-filters[i])<min)
                {
                    min=abs(fla-filters[i]);
                    choice=i+1;
                }
            }
        }
        if(min>20)
        {
            oApplicationManager->endWaitCursor();
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),
                                FXStringFormat("%s %dnm",oLanguage->getText("str_serialerr_nofil").text(),filters[choice-1]).text());
            {deinitialize();return NULL;}
        }
        fla=choice;
        
        choice=0;
        if(flb>0)
        {
            min=flb-filters[0];
            choice=1;
            
            for(i=0;i<5 && filters[i]>0;i++)
            {
                if(abs(flb-filters[i])<min)
                {
                    min=abs(flb-filters[i]);
                    choice=i+1;
                }
            }
            if(min>20)
            {
                oApplicationManager->endWaitCursor();
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),
                                    FXStringFormat("%s %dnm",oLanguage->getText("str_serialerr_nofil").text(),filters[choice-1]).text());
                {deinitialize();return NULL;}
            }
        }
        flb=choice;
        
        if(fla>0)
        {
            if(sendCommand(FXStringFormat("32%d",flb>0?1:0).text(),3,res)==-1 || res[0]!=0x06)
                {deinitialize();return NULL;}
            if(sendCommand(FXStringFormat("30%d",fla).text(),3,res)==-1 || res[0]!=0x06)
                {deinitialize();return NULL;}
            if(flb>0)
            {
                if(sendCommand(FXStringFormat("31%d",flb).text(),3,res)==-1 || res[0]!=0x06)
                    {deinitialize();return NULL;}
            }
        }
    }
    
    if(sendCommand("20",2,res)==-1 || res[0]!=0x06)
        {deinitialize();return NULL;}
    if(sendCommand("40",2,res)==-1 || res[0]!=0x06)
        {deinitialize();return NULL;}
    if(sendCommand("50",2,res)==-1 || res[0]!=0x06)
        {deinitialize();return NULL;}
    
    if(sendCommand("11",2,res,true)==-1)
        {deinitialize();return NULL;}

    for(i=0;i<6;i++)
    {
        if(link->input(&digit,1)==-1 || digit!=0x05)
            break;
        link->output("\x006",1);
        digit=0;
        while(digit!=0x03)
            if(link->input(&digit,1)==-1)
                break;
        link->output("\x006",1);
    }
    for(i=0;i<12;i++)
    {
        if(link->input(&digit,1)==-1 || digit!=0x05)
            break;
        link->output("\x006",1);
        digit=0;
        k=0;
        while(digit!=0x03)
        {
            if(link->input(&digit,1)==-1)
                break;
            res[k++]=digit;
        }
        res[k]=0;
        link->output("\x006",1);
        s=FXString(res);
        s=s.substitute("BLANK","0.000");
        s=s.substitute("UNDER","0.000");
        s=s.substitute(" OVER","9.999");
        sscanf(s.text(),"%d %c %f\x00d%d %c %f\x00d%d %c %f\x00d%d %c %f\x00d%d %c %f\x00d%d %c %f\x00d%d %c %f\x00d%d %c %f",
               &l,&digit,&a[0],
               &l,&digit,&a[1],
               &l,&digit,&a[2],
               &l,&digit,&a[3],
               &l,&digit,&a[4],
               &l,&digit,&a[5],
               &l,&digit,&a[6],
               &l,&digit,&a[7]);
        for(k=0;k<8;k++)
            *(ret+k*12+i)=a[k];
    }
    if(link->input(&digit,1)==-1 || digit!=0x05)
        {deinitialize();return NULL;}
    link->output("\x006",1);
    if(link->input(&digit,1)==-1 || digit!=0x02)
        {deinitialize();return NULL;}
    if(link->input(&digit,1)==-1 || digit!=0x06)
        {deinitialize();return NULL;}
    if(link->input(&digit,1)==-1 || digit!=0x03)
        {deinitialize();return NULL;}
    link->output("\x006",1);
    if(sendCommand("16",2,res)==-1 || res[0]!=0x06)
        {deinitialize();return NULL;}
    deinitialize();
    free(res);
    for(int yo=0;yo<96;yo++)
        if(ret[yo]==-0)ret[yo]=0;
    return ret;
}

