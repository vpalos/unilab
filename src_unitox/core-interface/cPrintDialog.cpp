#include "cPrintDialog.h"

FXDEFMAP(cPrintDialog) mapPrintDialog[]=
{
    FXMAPFUNC(SEL_UPDATE,cPrintDialog::ID_PROPERTIES,cPrintDialog::onUpdate),
};

FXIMPLEMENT(cPrintDialog,FXPrintDialog,mapPrintDialog,ARRAYNUMBER(mapPrintDialog))

cPrintDialog::cPrintDialog()
{
}

cPrintDialog::cPrintDialog(FXWindow *owner, const FXString &title, FXuint opts, FXint x, FXint y, FXint w, FXint h):
    FXPrintDialog(owner,title,opts,x,y,w,h)
{
    orientportrait->getParent()->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_HIDE),NULL);
    printer.flags&=~PRINT_LANDSCAPE;
    media->getParent()->setLayoutHints(LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT);
    media->getParent()->setHeight(69);
    
    getApp()->reg().writeStringEntry("PAPER","0","[A4] 595.27559 841.88976 80 80 80 80");
    getApp()->reg().writeStringEntry("PAPER","1","[US Letter] 612 792 72 72 72 72");
    getApp()->reg().deleteEntry("PAPER","2");
    getApp()->reg().deleteEntry("PAPER","3");
    int md=getApp()->reg().readIntEntry("PRINTER","media",0);
    md=md>1?0:md;
    printer.mediasize=md;
    getApp()->reg().writeIntEntry("PRINTER","media",md);
    getApp()->reg().write();

    media->setNumVisible(2);
    media->clearItems();
    FXchar key[20],name[100];
    for(int i=0; ; i++)
    {
        sprintf(key,"%d",i);
        if(getApp()->reg().readFormatEntry("PAPER",key,"[%[^]]] %*f %*f %*f %*f %*f %*f",name)!=1) break;
        media->appendItem(name);
    }
}

long cPrintDialog::onUpdate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    prSender->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_HIDE),NULL);
    return 1;
}


