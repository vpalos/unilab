#include "engine.h"
#include "cWindowInstall.h"
#include "cLabel.h"

FXDEFMAP(cWindowInstall) mapWindowInstall[]=
{
    FXMAPFUNC(SEL_COMMAND,cWindowInstall::ID_BROWSE,cWindowInstall::onBrowse),
};

FXIMPLEMENT(cWindowInstall,FXDialogBox,mapWindowInstall,ARRAYNUMBER(mapWindowInstall))

cWindowInstall::cWindowInstall()
{
}

cWindowInstall::cWindowInstall(FXApp *a, const FXString &name, FXuint opts, FXint x, FXint y, FXint w, FXint h, FXint pl, FXint pr, FXint pt, FXint pb, FXint hs, FXint vs):
    FXDialogBox(a,name,opts,x,y,w,h,pl,pr,pt,pb,hs,vs)
{
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL);
    FXLabel *_l0;

    FXHorizontalFrame *_hframe8=new FXHorizontalFrame(_vframe0,0,0,0,0,0,0,0,0,0,0,0);
    FXFont *ft1=new FXFont(oApplicationManager,"Times New Roman",26,FXFont::Normal);
    FXFont *ft2=new FXFont(oApplicationManager,"Times New Roman",18,FXFont::Normal);
    _l0=new cLabel(_hframe8,"U",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,0,0,0,0);
        _l0->setFont(ft1);
        _l0->setTextColor(FXRGB(160,0,0));
    _l0=new cLabel(_hframe8,"n",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,2,3,0,2);
        _l0->setFont(ft2);
        _l0->setTextColor(FXRGB(160,0,0));
    _l0=new cLabel(_hframe8,"i",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,0,0,0,2);
        _l0->setFont(ft2);
        _l0->setTextColor(FXRGB(160,0,0));
    _l0=new cLabel(_hframe8,"V",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,0,0,0,0);
        _l0->setFont(ft1);
        _l0->setTextColor(FXRGB(160,0,0));
    _l0=new cLabel(_hframe8,"e",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,0,3,0,2);
        _l0->setFont(ft2);
        _l0->setTextColor(FXRGB(160,0,0));
    _l0=new cLabel(_hframe8,"t",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,0,0,0,2);
        _l0->setFont(ft2);
        _l0->setTextColor(FXRGB(160,0,0));

    new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_WIDTH|SEPARATOR_GROOVE,0,0,350);
    new FXLabel(_vframe0,"You must install UniTox software first.\nIt is recommended that you close all programs during this process!\n\nInstall destination:",NULL,LAYOUT_FILL_X);

    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
    tx=new FXTextField(_hframe0,15,NULL,0,LAYOUT_FILL_X|TEXTFIELD_READONLY|FRAME_SUNKEN);
    tx->setBackColor(FXRGB(242,240,240));
#ifdef WIN32
    if(FXStat::exists("C:\\Program Files") && FXStat::isDirectory("C:\\Program Files"))
        tx->setText("C:\\Program Files\\UniTox");
    else
        tx->setText("C:\\UniTox");
#else
    tx->setText("/usr/local/share/UniTox");
#endif
    new FXButton(_hframe0,"Browse",NULL,this,ID_BROWSE,FRAME_RAISED|LAYOUT_FIX_WIDTH,0,0,80);

    new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_WIDTH|SEPARATOR_GROOVE,0,0,350);
    desk=new FXCheckButton(_vframe0,"Create Desktop Shortcut");
    desk->setCheck(true);
    new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_WIDTH|SEPARATOR_GROOVE,0,0,350);
    FXHorizontalFrame *_hframe10=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
    new FXButton(_hframe10,"Cancel",NULL,this,FXDialogBox::ID_CANCEL,BUTTON_DEFAULT|FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH|LAYOUT_FIX_HEIGHT,0,0,80,30);
    new FXVerticalSeparator(_hframe10,LAYOUT_FIX_WIDTH|SEPARATOR_NONE,0,0,50);
    new FXButton(_hframe10,"Install >>",NULL,this,FXDialogBox::ID_ACCEPT,BUTTON_INITIAL|BUTTON_DEFAULT|FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH|LAYOUT_FIX_HEIGHT,0,0,80,30);
    FXString ret="";
}

cWindowInstall::~cWindowInstall()
{
}

long cWindowInstall::onBrowse(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!FXStat::exists(tx->getText().text()))
        return 1;
    FXDirDialog open((FXWindow*)this,"Open directory");
    open.setDirectory(FXStat::exists(tx->getText().text())?tx->getText():".");
    open.showFiles(FALSE);
    if(open.execute())
        tx->setText(open.getDirectory());
    return 1;
}


