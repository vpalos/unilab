#include "cWinMain.h"
#include "cColorsManager.h"
#include "cDCNativePrinter.h"
#include "cDCPrint.h"

#ifdef WIN32
class cPrinterVisual : public FXVisual {
  public:
  cPrinterVisual();
  void create() { xid=(void *)1; }
  void detach() { xid=(void *)0; }
  void destroy() { xid=(void *)0; }
  FXPixel getPixel(FXColor clr){ return RGB( FXREDVAL(clr), FXGREENVAL(clr), FXBLUEVAL(clr) ); }
  FXColor getColor(FXPixel pix){ return FXRGB( GetRValue(pix), GetGValue(pix), GetBValue(pix) ); }
  };

cPrinterVisual::cPrinterVisual():FXVisual() {
  depth=24;
  numred=numgreen=numblue=256;
  numcolors=256*256*256;
  type=(FXVisualType)VISUALTYPE_TRUE;
  xid=(void *)0;
  //hPalette = NULL;
  };

class cPrinterDrawable : public FXDrawable {
  protected:
  FXID dc;
  public:
  cPrinterDrawable(FXID gc);
  cPrinterDrawable();
  ~cPrinterDrawable();
  void SetDC(FXID gc);
  virtual FXID GetDC() const { return (FXID)dc; }
  virtual int ReleaseDC(FXID) const { return 0; }
  };

cPrinterDrawable::~cPrinterDrawable() {
  delete visual;
  }

cPrinterDrawable::cPrinterDrawable() {
  cPrinterDrawable(0);
  }

cPrinterDrawable::cPrinterDrawable(FXID gc) : FXDrawable() {
  dc = gc;
  visual=new cPrinterVisual();
  xid=(FXID)1;
  }

void cPrinterDrawable::SetDC(FXID gc) {
  dc = (HDC)gc;
  }
#endif

// Construct
cDCNativePrinter::cDCNativePrinter(FXApp *a):FXDC(a) {
#ifdef WIN32
  opaque = (FXObject *)NULL;
  dctype=TYPE_WIN32;
#else
  dctype=TYPE_PS;
#endif
  logpixelsx = 72;
  logpixelsy = 72;
  trsx=0;
  trsy=0;
  bw=false;
  scalex = 1;
  scaley = 1;
  bscale = 0;
  unitsx = 72;
  unitsy = 72;
  pdc = (FXDC *)NULL;
  pagecount=0;
  iscolor=false;
  }

// Destruct
cDCNativePrinter::~cDCNativePrinter(){
  }

// Generate print job prolog
FXbool cDCNativePrinter::beginPrint(FXPrinter& job){
    FXuint retvalue;
    this->iscolor=job.flags&PRINT_COLOR;
  pagecount=0;
  bw=(job.flags&PRINT_COLOR)?false:true;
#ifndef WIN32
  dctype=TYPE_PS;
  switch (dctype) {
    case TYPE_WIN32: return FALSE; //  TYPE_WIN32  on non WIN32 platform???? should not happen!!
    case TYPE_PS: {
      pdc = (FXDC *)new cDCPrint(getApp());
      retvalue = ((cDCPrint *)pdc)->beginPrint(job);
      logpixelsx = 72;
      logpixelsy = 72;
      scalex = 1;
      scaley = 1;
      } break;
    }
#else
  if (job.flags&PRINT_DEST_FILE) dctype = TYPE_PS;
  else dctype=TYPE_WIN32;
  switch (dctype) {
    case TYPE_WIN32: {
      devmode_handle=0;
      cPrinterDrawable *prn;
      // TODO: Paper size
      memset(&devmode,0,sizeof(devmode));
      devmode.dmFields = DM_ORIENTATION | DM_COLOR | DM_PAPERSIZE;
      if(job.mediasize==1)
          devmode.dmPaperSize=DMPAPER_LETTER;
      else
          devmode.dmPaperSize=DMPAPER_A4;
      devmode.dmSize = sizeof(devmode);
      devmode.dmOrientation = (job.flags&PRINT_LANDSCAPE) ? DMORIENT_LANDSCAPE : DMORIENT_PORTRAIT;
      devmode.dmColor = (job.flags&PRINT_COLOR) ? DMCOLOR_COLOR : DMCOLOR_MONOCHROME;

      char devicename[256];
      /* Note: Under Win 9x/ME   "WINSPOOL" should be replaced by NULL,
      **    but this seems to work anyway  */
      strcpy(devicename, job.name.text() );
      dc = CreateDC( "WINSPOOL", devicename, NULL,&devmode);
      if (dc==(HANDLE)NULL) return 0;

      // Initialize the members of a DOCINFO structure.
      memset( (void *)&di, 0, sizeof(di) );
      di.cbSize = sizeof(DOCINFO);
      di.lpszDocName = "Document";  // FIXME: API to support document name?
      di.lpszOutput = (LPTSTR) NULL;
      di.fwType = 0;

      /****** START DOCUMENT *******/
      if (::StartDoc(dc, &di)==SP_ERROR) {
        ::DeleteDC(dc);   dc=0;
        return 0;
        }

      SetMapMode( dc, MM_TEXT );

      /* get pixels per inch, usually 600x600 or 300x300 */
      logpixelsy = ::GetDeviceCaps( dc, LOGPIXELSY );
      logpixelsx = ::GetDeviceCaps( dc, LOGPIXELSX );
      setHorzUnitsInch( 72.0 );
      setVertUnitsInch( 72.0 );

      /* Create drawable */
      prn = new cPrinterDrawable( (FXID)dc );

      opaque= (FXObject *)prn;
      FXdouble dx,dy;
      dx=job.mediawidth;
      dy=job.mediaheight;
      if (job.flags&PRINT_LANDSCAPE) { FXdouble kk; kk=dx; dx=dy; dy=kk; }
      prn->resize( (FXint)(scalex * dx) , (FXint)(scaley * dy) );
      pdc= (FXDC *)new FXDCWindow(prn);  // Create a WIN32 FXDC from our drawable
      } break;
    case TYPE_PS: {
      pdc = (FXDC *)new cDCPrint(getApp());
      retvalue = ((cDCPrint *)pdc)->beginPrint(job);
      logpixelsx = 72;
      logpixelsy = 72;
      scalex = 1;
      scaley = 1;
      } break;
    }
#endif

  trsx=trsy=0;
  // Following two lines needed to get consistent behaviour under WIN32
  pdc->setForeground( FXRGB( 0,0,0) );
  pdc->setBackground( FXRGB( 255,255,255) );
  return TRUE;
  }

// Generate print job epilog
FXbool cDCNativePrinter::endPrint(){
#ifndef WIN32
  switch (dctype) {
    case TYPE_WIN32: return FALSE; // TYPE_WIN32 on non WIN32 platform??? should not happen!
    case TYPE_PS: {
      FXbool v;
      cDCPrint *pd;
      pd=(cDCPrint *)pdc;
      v=pd->endPrint();
      delete pd;
      pdc=(FXDC *)NULL;
      return v;
      }
    }
#else
  switch (dctype) {
    case TYPE_WIN32: {
      // End of Document
      if (dc!=0) {
        ::EndDoc(dc);
        FXDCWindow *pd;
        pd=(FXDCWindow *)pdc;
        delete pd;
        cPrinterDrawable *prn=(cPrinterDrawable *)opaque;
        delete prn;
        ::DeleteDC(dc);
        dc=0;
        opaque=(FXObject *)NULL;
        pdc=(FXDC*)NULL;
        }
     return TRUE;
     }
    case TYPE_PS: {
      FXbool v;
      cDCPrint *pd;
      pd=(cDCPrint *)pdc;
      v=pd->endPrint();
      delete pd;
      pdc=(FXDC *)NULL;
      return v;
      }
    }
#endif
  return FALSE;
  }

// Generate begin of page
FXbool cDCNativePrinter::beginPage(FXuint page){
    trsx=trsy=0;
#ifndef WIN32
  switch (dctype) {
    case TYPE_WIN32: return FALSE;
    case TYPE_PS: {
      cDCPrint *pd;
      pd=(cDCPrint *)pdc;
      return pd->beginPage(page);
      }
    }
#else
  switch (dctype) {
    case TYPE_WIN32: {
      if (::StartPage(dc)<=0) { endPrint(); return FALSE; }
      return 1;
      }
    case TYPE_PS: {
      cDCPrint *pd;
      pd=(cDCPrint *)pdc;
      return pd->beginPage(page);
      }
    }
#endif
  return FALSE;
  }

// Generate end of page
FXbool cDCNativePrinter::endPage(){
#ifdef WIN32
  switch (dctype) {
    case TYPE_WIN32:
      ::EndPage(dc);
      pagecount++;
      return TRUE;
#else
  switch (dctype) {
    case TYPE_WIN32: return FALSE;
#endif
    case TYPE_PS:
      cDCPrint *pd;
      pd=(cDCPrint *)pdc;
      return pd->endPage();
    }
  return FALSE;
  }

// Draw a point in the current pen color
void cDCNativePrinter::drawPoint(FXint x,FXint y){
  pdc->drawPoint( ScaleX(x+trsx), ScaleY(y+trsy) );
  }

// Draw points in the current pen color.
// Each point's position is relative to the drawable's origin (as usual).
void cDCNativePrinter::drawPoints(const FXPoint* points,FXuint npoints){
  FXPoint dst[128],*p;
  p=dst;
  if (npoints>128) p=(FXPoint *)malloc( sizeof(FXPoint)*npoints );
  scalePoints( p, (FXPoint *)points, npoints );
  pdc->drawPoints( dst, npoints );
  if (npoints>128) free(p);
  }

// Draw points in the current pen color. The first point's position is
// relative to the drawable's origin, but each subsequent point's position
// is relative to the previous point's position; each FXPoint defines
// the relative coordinates. Think LOGO.
void cDCNativePrinter::drawPointsRel(const FXPoint*points,FXuint npoints){
  FXPoint dst[128],*p;
  p=dst;
  if (npoints>128) p=(FXPoint *)malloc( sizeof(FXPoint)*npoints );
  scalePoints( p, (FXPoint *)points, npoints );
  pdc->drawPointsRel( dst, npoints );
  if (npoints>128) free(p);
  }

// Draw a line
void cDCNativePrinter::drawLine(FXint x1,FXint y1,FXint x2,FXint y2){
  pdc->drawLine( ScaleX(x1+trsx), ScaleY(y1+trsy), ScaleX(x2+trsx), ScaleY(y2+trsy) );
  }

// Draw multiple lines. All points are drawn connected.
// Each point is specified relative to Drawable's origin.
void cDCNativePrinter::drawLines(const FXPoint* points,FXuint npoints){
  FXPoint dst[128],*p;
  p=dst;
  if (npoints>128) p=(FXPoint *)malloc( sizeof(FXPoint)*npoints );
  scalePoints( p, (FXPoint *)points, npoints );
  pdc->drawLines( dst, npoints );
  if (npoints>128) free(p);
  }

// Draw multiple lines. All points are drawn connected.
// First point's coordinate is relative to drawable's origin, but
// subsequent points' coordinates are relative to previous point.
void cDCNativePrinter::drawLinesRel(const FXPoint* points,FXuint npoints){
  FXPoint dst[128],*p;
  p=dst;
  if (npoints>128) p=(FXPoint *)malloc( sizeof(FXPoint)*npoints );
  scalePoints( p, (FXPoint *)points, npoints );
  pdc->drawLinesRel( dst, npoints );
  if (npoints>128) free(p);
  }

// Draw unconnected line segments
void cDCNativePrinter::drawLineSegments(const FXSegment* segments,FXuint nsegments){
  FXSegment dst[128],*p;
  p=dst;
  if (nsegments>128) p=(FXSegment *)malloc( sizeof(FXSegment)*nsegments );
  scaleSegments( p, (FXSegment *)segments, nsegments );
  pdc->drawLineSegments( dst, nsegments );
  if (nsegments>128) free(p);
  }

// Draw unfilled rectangle
void cDCNativePrinter::drawRectangle(FXint x,FXint y,FXint w,FXint h){
  pdc->drawRectangle( ScaleX(x+trsx), ScaleY(y+trsy), ScaleX(w), ScaleY(h) );
  }

void cDCNativePrinter::drawRectangles(const FXRectangle* rectangles,FXuint nrectangles){
  FXRectangle dst[128],*p;
  p=dst;
  if (nrectangles>128) p=(FXRectangle *)malloc( sizeof(FXRectangle)*nrectangles );
  scaleRectangles( p, (FXRectangle *)rectangles, nrectangles );
  pdc->drawRectangles( dst, nrectangles );
  if (nrectangles>128) free(p);
  }

// Draw arc
void cDCNativePrinter::drawArc(FXint x,FXint y,FXint w,FXint h,FXint ang1,FXint ang2){
  pdc->drawArc( ScaleX(x+trsx), ScaleY(y+trsy), ScaleX(w), ScaleY(h), ang1, ang2);
  }

// Draw arcs
void cDCNativePrinter::drawArcs(const FXArc* arcs,FXuint narcs){
  FXArc dst[128],*p;
  p=dst;
  if (narcs>128) p=(FXArc *)malloc( sizeof(FXArc)*narcs );
  scaleArcs( p, (FXArc *)arcs, narcs );
  pdc->drawArcs( dst, narcs );
  if (narcs>128) free(p);
  }

// Filled rectangle
void cDCNativePrinter::fillRectangle(FXint x,FXint y,FXint w,FXint h){
  pdc->fillRectangle( ScaleX(x+trsx), ScaleY(y+trsy), ScaleX(w), ScaleY(h) );
  }

// Filled rectangles
void cDCNativePrinter::fillRectangles(const FXRectangle* rectangles,FXuint nrectangles){
  FXRectangle dst[128],*p;
  p=dst;
  if (nrectangles>128) p=(FXRectangle *)malloc( sizeof(FXRectangle)*nrectangles );
  scaleRectangles( p, (FXRectangle *)rectangles, nrectangles );
  pdc->fillRectangles( dst, nrectangles );
  if (nrectangles>128) free(p);
  }

// Fill arc
void cDCNativePrinter::fillArc(FXint x,FXint y,FXint w,FXint h,FXint ang1,FXint ang2){
  pdc->fillArc( ScaleX(x+trsx), ScaleY(y+trsy), ScaleX(w), ScaleY(h), ang1, ang2 );
  }


// Fill arcs
void cDCNativePrinter::fillArcs(const FXArc*arcs,FXuint  narcs){
  FXArc dst[128],*p;
  p=dst;
  if (narcs>128) p=(FXArc *)malloc( sizeof(FXArc)*narcs );
  scaleArcs( p, (FXArc *)arcs, narcs );
  pdc->fillArcs( dst, narcs );
  if (narcs>128) free(p);
  }

// Filled simple polygon
void cDCNativePrinter::fillPolygon(const FXPoint *points,FXuint npoints){
  FXPoint dst[128],*p;
  p=dst;
  if (npoints>128) p=(FXPoint *)malloc( sizeof(FXPoint)*npoints );
  scalePoints( p, (FXPoint *)points, npoints );
  pdc->fillPolygon( dst, npoints );
  if (npoints>128) free(p);
  }

// Fill concave polygon
void cDCNativePrinter::fillConcavePolygon(const FXPoint *points,FXuint npoints){
  FXPoint dst[128],*p;
  p=dst;
  if (npoints>128) p=(FXPoint *)malloc( sizeof(FXPoint)*npoints );
  scalePoints( p, (FXPoint *)points, npoints );
  pdc->fillConcavePolygon( dst, npoints );
  if (npoints>128) free(p);
  }


// Fill complex (self-intersecting) polygon
void cDCNativePrinter::fillComplexPolygon(const FXPoint *points,FXuint npoints){
  FXPoint dst[128],*p;
  p=dst;
  if (npoints>128) p=(FXPoint *)malloc( sizeof(FXPoint)*npoints );
  scalePoints( p, (FXPoint *)points, npoints );
  pdc->fillComplexPolygon( dst, npoints );
  if (npoints>128) free(p);
  }


// Filled simple polygon with relative points
void cDCNativePrinter::fillPolygonRel(const FXPoint *points,FXuint npoints){
  FXPoint dst[128],*p;
  p=dst;
  if (npoints>128) p=(FXPoint *)malloc( sizeof(FXPoint)*npoints );
  scalePoints( p, (FXPoint *)points, npoints );
  pdc->fillPolygonRel( dst, npoints );
  if (npoints>128) free(p);
  }


// Fill concave polygon
void cDCNativePrinter::fillConcavePolygonRel(const FXPoint *points,FXuint npoints){
  FXPoint dst[128],*p;
  p=dst;
  if (npoints>128) p=(FXPoint *)malloc( sizeof(FXPoint)*npoints );
  scalePoints( p, (FXPoint *)points, npoints );
  pdc->fillConcavePolygonRel( dst, npoints );
  if (npoints>128) free(p);
  }


// Fill complex (self-intersecting) polygon
void cDCNativePrinter::fillComplexPolygonRel(const FXPoint *points,FXuint npoints){
  FXPoint dst[128],*p;
  p=dst;
  if (npoints>128) p=(FXPoint *)malloc( sizeof(FXPoint)*npoints );
  scalePoints( p, (FXPoint *)points, npoints );
  pdc->fillComplexPolygonRel( dst, npoints );
  if (npoints>128) free(p);
  }

// Draw string (only foreground bits)
void cDCNativePrinter::drawText(FXint x,FXint y,const FXchar* string,FXuint len){
#ifdef WIN32
    FXFont *ft=NULL,*oldft;
    FXString name;
    if(dctype==TYPE_WIN32 && (logpixelsx>72))
    {
        oldft=pdc->getFont();
        if(oldft)
        {
            name=oldft->getName();
            double as=(double)oldft->getActualSize(),ns=scalex-(1.3*bscale);

            ft=new FXFont(getApp(),FXStringFormat("%s,%d%s",name.text(),(int)(as*ns),(oldft->getWeight()!=FXFont::Normal)?",bold":"").text());
            ft->create();
            setFont(ft);
        }
    }
    pdc->drawText( ScaleX(x+trsx), ScaleY(y+trsy), string, len );
    if(dctype==TYPE_WIN32 && (logpixelsx>72) && oldft)
    {
        setFont(oldft);
        if(ft)
            delete ft;
    }
#else
    pdc->drawText( ScaleX(x+trsx), ScaleY(y+trsy), string, len );
#endif
  }

// Draw string (both foreground and background bits)
void cDCNativePrinter::drawImageText(FXint x,FXint y,const FXchar *string,FXuint len){
  pdc->drawImageText( ScaleX(x+trsx), ScaleY(y+trsy), string, len);
  }

// Draw area from source
void cDCNativePrinter::drawArea(const FXDrawable* source, FXint sx, FXint sy, FXint sw, FXint sh, FXint dx, FXint dy) {
  pdc->drawArea( source, sx,sy,sw,sh, ScaleX(dx+trsx), ScaleY(dy+trsy) );
  }

// Draw image
void cDCNativePrinter::drawImage(const FXImage *img,FXint dx,FXint dy){
  pdc->drawImage( img, ScaleX(dx+trsx), ScaleY(dy+trsy) );
  }

// Draw bitmap
void cDCNativePrinter::drawBitmap(const FXBitmap* bitmap,FXint dx,FXint dy){
  pdc->drawBitmap( bitmap, ScaleX(dx+trsx), ScaleY(dy+trsy) );
  }


// Draw icon
void cDCNativePrinter::drawIcon(const FXIcon*icon,FXint dx,FXint dy){
  pdc->drawIcon( icon, ScaleX(dx+trsx), ScaleY(dy+trsy) );
  }


// Draw icon shaded
void cDCNativePrinter::drawIconShaded(const FXIcon*icon,FXint dx,FXint dy){
  pdc->drawIconShaded( icon, ScaleX(dx+trsx), ScaleY(dy+trsy) );
  }


// Draw icon sunken
void cDCNativePrinter::drawIconSunken(const FXIcon* icon,FXint dx,FXint dy){
  pdc->drawIconSunken( icon, ScaleX(dx+trsx), ScaleY(dy+trsy) );
  }


// Draw hashed box
void cDCNativePrinter::drawHashBox(FXint x, FXint y, FXint w, FXint h, FXint b){
  // FIXME: Scaling border by horizontal resolution, what when logpixelsx != logpixelsy ??
  pdc->drawHashBox( ScaleX(x+trsx), ScaleY(y+trsy), ScaleX(w), ScaleY(h), ScaleX(b));
  }

// Set foreground drawing color (brush)
void cDCNativePrinter::setForeground(FXColor prClr){
  /*if(bw)
  {
      unsigned long r,g,b;
      r=FXREDVAL(clr);
      g=FXGREENVAL(clr);
      b=FXBLUEVAL(clr);

      if(r>250 && g>250 && b>250)
          clr=FXRGB(255,255,255);
      else
          clr=FXRGB(0,0,0);
  }*/
  FXColor clr=prClr;
  if(bw)
  {
      unsigned long r,g,b,m;
      r=FXREDVAL(prClr);
      g=FXGREENVAL(prClr);
      b=FXBLUEVAL(prClr);
      m=(r/30+g/30+b/30)*10;
      if(r>250 && g>250 && b>250)
          clr=FXRGB(255,255,255);
      else
          clr=FXRGB(m,m,m);
  }
  pdc->setForeground(clr);
  }

// Set background drawing color (brush)
void cDCNativePrinter::setBackground(FXColor clr){
  /*if(bw)
  {
      unsigned long r,g,b;
      r=FXREDVAL(clr);
      g=FXGREENVAL(clr);
      b=FXBLUEVAL(clr);

      if(r>250 && g>250 && b>250)
          clr=FXRGB(255,255,255);
      else
          clr=FXRGB(0,0,0);
  }*/
  pdc->setBackground(clr);
  }

// Set dash pattern
void cDCNativePrinter::setDashes(FXuint dashoffset, const FXchar* dashpattern, FXuint dashlength) {
  pdc->setDashes( dashoffset, dashpattern, dashlength );
  }

// Set line width
void cDCNativePrinter::setLineWidth(FXuint linewidth){
// FIXME: Scaling by X resolution, what if Xdpi != Ydpi ????
  pdc->setLineWidth( ScaleX(linewidth) );
  }

// Set line cap style
void cDCNativePrinter::setLineCap(FXCapStyle capstyle){
  pdc->setLineCap( capstyle );
  }
// Set line join style
void cDCNativePrinter::setLineJoin(FXJoinStyle joinstyle){
  pdc->setLineJoin( joinstyle );
  }


// Set line style
void cDCNativePrinter::setLineStyle(FXLineStyle linestyle){
  pdc->setLineStyle( linestyle );
  }


// Set fill style
void cDCNativePrinter::setFillStyle(FXFillStyle fillstyle){
  pdc->setFillStyle( fillstyle );
  }


// Set fill rule
void cDCNativePrinter::setFillRule(FXFillRule fillrule){
  pdc->setFillRule( fillrule );
  }


// Set blit function
void cDCNativePrinter::setFunction(FXFunction func){
  pdc->setFunction( func );
  }


// Set tile image
void cDCNativePrinter::setTile(FXImage* image,FXint dx,FXint dy){
  pdc->setTile( image, dx, dy );  // TODO: Check if dx,dy should be scaled
  }


// Set stipple pattern
void cDCNativePrinter::setStipple(FXBitmap* bitmap,FXint dx,FXint dy){
  pdc->setStipple( bitmap, dx, dy ); // TODO: Check if dx,dy should be scaled
  }


// Set stipple pattern
void cDCNativePrinter::setStipple(FXStipplePattern pat,FXint dx,FXint dy){
  pdc->setStipple( pat, dx, dy ); // TODO: Check if dx,dy should be scaled
  }


// Set clip rectangle
void cDCNativePrinter::setClipRectangle(FXint x,FXint y,FXint w,FXint h){
  pdc->setClipRectangle( ScaleX(x+trsx), ScaleY(y+trsy), ScaleX(w), ScaleY(h) );
  }


// Set clip rectangle
void cDCNativePrinter::setClipRectangle(const FXRectangle& rectangle){
  pdc->setClipRectangle( ScaleX(rectangle.x+trsx), ScaleY(rectangle.y+trsy),
      ScaleX(rectangle.w), ScaleY(rectangle.h) );
  }


// Clear clipping
void cDCNativePrinter::clearClipRectangle(){
  pdc->clearClipRectangle();
  }


// Set clip mask
void cDCNativePrinter::setClipMask(FXBitmap* bitmap,FXint dx,FXint dy){
  pdc->setClipMask( bitmap, dx, dy );  // TODO: Check if dx,dy should be scaled
  }


// Clear clip mask
void cDCNativePrinter::clearClipMask(){
  pdc->clearClipMask();
  }



// Set font to draw text with
void cDCNativePrinter::setFont(FXFont *fnt){
  pdc->setFont( fnt );
  }

FXFont *cDCNativePrinter::getFont(void){
  return pdc->getFont();
  }


// Change clip-against-child windows mode
void cDCNativePrinter::clipChildren(FXbool yes){
   // Do nothing
  }

void cDCNativePrinter::scalePoints(FXPoint *dst, FXPoint *src, FXuint npoints ){
  for (;npoints>0;npoints--,dst++,src++) {
    dst->x = ScaleX( src->x+trsx );
    dst->y = ScaleY( src->y+trsy );
    }
  }

void cDCNativePrinter::scaleRectangles(FXRectangle *dst, FXRectangle *src, FXuint nrectangles ) {
  for (;nrectangles>0;nrectangles--,dst++,src++) {
    dst->x = ScaleX( src->x+trsx );
    dst->y = ScaleY( src->y+trsy );
    dst->w = ScaleX( src->w );
    dst->h = ScaleY( src->h );
    }
  }

void cDCNativePrinter::scaleSegments(FXSegment *dst, FXSegment *src, FXuint nsegments ) {
  for (;nsegments>0;nsegments--,dst++,src++) {
    dst->x1 = ScaleX( src->x1+trsx );
    dst->y1 = ScaleY( src->y1+trsy );
    dst->x2 = ScaleX( src->x2+trsx );
    dst->y2 = ScaleY( src->y2+trsy );
    }
  }

void cDCNativePrinter::scaleArcs(FXArc *dst, FXArc *src, FXuint narcs ) {
  for (;narcs>0;narcs--,dst++,src++) {
    dst->x = ScaleX( src->x+trsx );
    dst->y = ScaleY( src->y+trsy );
    dst->w = ScaleX( src->w );
    dst->h = ScaleY( src->h );
    dst->a = src->a;
    dst->b = src->b;
    }
  }

void cDCNativePrinter::setHorzUnitsInch(FXfloat sx) {
  scalex = logpixelsx / sx;
  unitsx = sx;
  }

void cDCNativePrinter::setVertUnitsInch(FXfloat sy) {
  bscale=72/sy;
  scaley = logpixelsy / sy;
  unitsx = sy;
  }


