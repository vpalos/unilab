#include "engine.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "fxkeys.h"
#include "cDateChooser.h"

static int	DaysSoFar[][13] =
{
    {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365},
    {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366}
};

static int MONTH_LENGTH[] = {31,28,31,30,31,30,31,31,30,31,30,31}; // 0-based
static int LEAP_MONTH_LENGTH[] = {31,29,31,30,31,30,31,31,30,31,30,31}; // 0-based

/* Constructors */

FXwDate::FXwDate()
{

    tm			*local_time;
    time_t timer	   = time(NULL);
    local_time = localtime(&timer);

    month = (short)(local_time->tm_mon + 1);
    day   = (short) local_time->tm_mday;
    year  = (short)(local_time->tm_year + 1900);
    hour = (short)(local_time->tm_hour);
    minute = (short)(local_time->tm_min);
    second= (short)(local_time->tm_sec);

    mdy_to_julian ();
};

FXwDate::FXwDate (const FXwDate &dt)
{
    month = dt.month;
    day   = dt.day;
    year  = dt.year;
    hour = dt.hour;
    minute = dt.minute;
    second= dt.second;

    mdy_to_julian();
}

FXwDate::FXwDate(const time_t time)
{

    tm	*local_time;

    local_time = localtime(&time);

    month = (short)(local_time->tm_mon + 1);
    day   = (short) local_time->tm_mday;
    year  = (short)(local_time->tm_year + 1900);
    hour = (short)(local_time->tm_hour);
    minute = (short)(local_time->tm_min);
    second= (short)(local_time->tm_sec);

    mdy_to_julian ();
};


FXwDate::FXwDate(int year, int month, int day, int hour, int minute, int second)
{
    setNew(year, month, day, hour, minute,second);
}

FXwDate::FXwDate(int quarter)
{
    setQuarter(quarter);
}
FXwDate::FXwDate(FXString datestring)
{
    setDate(datestring);
}

FXString FXwDate::getMonthFName(int i)
{
    tm	*local_time;
    time_t timer	   = time(NULL);
    local_time = localtime(&timer);
    local_time->tm_mon = i;
    char ret[25];
    strftime(ret,25,"%B",local_time);
    return FXString(ret);
}

FXString FXwDate::getWDayName(int i)
{
    tm	*local_time;
    time_t timer	   = time(NULL);
    local_time = localtime(&timer);
    local_time->tm_wday = i;
    char ret[25];
    strftime(ret,25,"%a",local_time);
    return FXString(ret);
}

bool FXwDate::isLeapYear()
{

    julian_to_mdy();
    int y = year;

    if(y%4==0 && y%100==0)
        return (y%400==0);
    else
        return (y%4==0);


};


bool FXwDate::checkStringFormat(FXString datestring)
{

    if(datestring.length()>10)
        return false;

    unsigned int day=0, month=0, year=0;

    if (3 != datestring.scan("%u.%u.%u", &day, &month, &year))
        return false;

    if(day > 31 || month>12 || year<999 || year>9999)
        return false;




    return true;

}


/* Set the Elements */
void FXwDate::setDate(FXString datestring)
{

    if(datestring.length()>10)
        return;

    unsigned int day=0, month=0, year=0;

    if (3 != datestring.scan("%u.%u.%u", &day, &month, &year))
        return;

    if(month>12 || year<999 || year>9999)
        return;


    setNew(year,month,day);

}

void FXwDate::setNew(int YY, int MM, int DD, int hh, int mm, int ss)
{

    month=MM;
    day=DD;
    year=YY;
    FXASSERT(month > 0);
    FXASSERT(month < 13);
    FXASSERT(day   > 0);
    FXASSERT(day   < 32);

    hour=hh;
    minute=mm;
    second=ss;
    mdy_to_julian();
}


void FXwDate::setSystime(time_t systime)
{
    tm*  local_time = localtime(&systime);

    if(!local_time) return;
    month = (short)(local_time->tm_mon + 1);
    day   = (short) local_time->tm_mday;
    year  = (short)(local_time->tm_year + 1900);
    hour = (short)(local_time->tm_hour);
    minute = (short)(local_time->tm_min);
    second= (short)(local_time->tm_sec);

    mdy_to_julian ();

}

void FXwDate::setYear(int YYYY)
{

    year=YYYY;
    mdy_to_julian ();


}

void FXwDate::setMonth(int MM)
{
    month=MM;
    if(month<1)
    {
        month=12;
        year--;
    }
    else if(month>12)
    {
        month=1;
        year++;
    }

    mdy_to_julian ();


}


void FXwDate::setDay(int DD)
{
    day=DD;
    mdy_to_julian ();

}

void FXwDate::setHour(int hh)
{

    hour=hh;
    mdy_to_julian ();



}
void FXwDate::setMinute(int mm)
{

    minute=mm;
    mdy_to_julian ();



}
void FXwDate::setSec(int ss)
{

    second=ss;
    mdy_to_julian ();



}

void FXwDate::setHMS(int hh, int mm, int ss)
{
    second=ss;
    minute=mm;
    hour=hh;
    mdy_to_julian ();

}

void FXwDate::setQuarter(int quarter)
{

    int dy=(quarter<1)?(quarter/4)-1:(quarter-1)/4;
    int yy = year+dy;

    quarter = (quarter-1)%4;
    quarter=(quarter<0)?quarter+5:quarter+1;

    switch(quarter)
    {
    case 1:
        setNew(yy,01,01,1,0);
        break;
    case 2:
        setNew(yy,04,01,1,0);

        break;
    case 3:
        setNew(yy,07,01,1,0);
        break;
    case 4:
        setNew(yy,10,01,1,0);
        break;
    }

    mdy_to_julian();
}

time_t FXwDate::getSystime()
{
    struct tm tmRep;
    time_t t;

    julian_to_mdy();

    int y=year;
    int m=month;
    int d=day;;

    if ( y < 1970 )
    {
        y = 70;
        m = 1;
        d = 1;
    }
    tmRep.tm_year = y - 1900 ;
    tmRep.tm_mon = m-1;
    tmRep.tm_mday = d;
    tmRep.tm_hour = hour;
    tmRep.tm_min = minute;
    tmRep.tm_sec = second;
    tmRep.tm_isdst = 0;

    t = mktime( &tmRep );
    return t;

};


int FXwDate::getYear()
{
    julian_to_mdy();
    return year;
};
int FXwDate::getMonth()
{
    julian_to_mdy();
    return month;

};
int FXwDate::getDay()
{
    julian_to_mdy();
    return day;
};

int FXwDate::getHour()
{
    julian_to_mdy();
    return hour;

};
int FXwDate::getMinute()
{
    julian_to_mdy();
    return minute;

};
int FXwDate::getSecond()
{
    julian_to_mdy();
    return second;
};
int FXwDate::getDayOfWeek() {

    julian_to_mdy();
    return day_of_week-1;

};

int FXwDate::getDaysOfMonth(int month)
{
    if(isLeapYear())
        return LEAP_MONTH_LENGTH[month-1];
    else
        return MONTH_LENGTH[month-1];
};


FXString FXwDate::getDayName()
{

    return getWDayName(this->getDayOfWeek());
}
FXString FXwDate::getShortDayName()
{

    return getWDayName(this->getDayOfWeek());
}
int FXwDate::getTTMM()
{
    int ttmmjj = 0;

    julian_to_mdy();

    ttmmjj = day* 100;
    ttmmjj += month;


    return ttmmjj;

}
int FXwDate::getTTMMJJ()
{
    int ttmmjj = 0;

    julian_to_mdy();

    ttmmjj = day * 10000;
    ttmmjj += month * 100;


    ttmmjj += year - (abs(year/100) * 100);

    return ttmmjj;

}

short FXwDate::getYY()
{
    julian_to_mdy();
    return year - (abs(year/100) * 100);
}

// Operators

void  FXwDate::operator= (FXwDate  const &param) {
    this->julian  = param.julian;
    julian_to_mdy();
}


FXwDate FXwDate::operator- (FXwDate param) {
    FXwDate temp;
    temp.julian =julian-param.julian;;
    julian_to_mdy();
    return (temp);
}
FXwDate FXwDate::operator++(int)  {
    day++;
    if(day<getDaysOfMonth(month)) {
        month++;
        if(month<12)
            year++;
        day=1;
    }
    mdy_to_julian();
    return *this;
}

FXwDate FXwDate::operator--(int)  {
    day--;
    if(day<1) {
        month--;
        if(month<1)
            year--;
        day=getDaysOfMonth(month);
    }
    mdy_to_julian();
    return *this;
}


FXwDate FXwDate::operator+ (FXwDate param) {
    FXwDate temp;
    temp.julian =julian+param.julian;;
    julian_to_mdy();
    return (temp);
}

bool FXwDate::operator > (FXwDate param)
{

    unsigned long  dif =param.julian-julian;
    return (dif<0)?true:false;
}
bool FXwDate::operator < (FXwDate param)
{
    unsigned long  dif =param.julian-julian;
    return (dif>0)?true:false;
}
bool FXwDate::operator >= (FXwDate param)
{

    unsigned long  dif =param.julian-julian;
    return (dif<=0)?true:false;
}
bool FXwDate::operator <= (FXwDate param)
{
    unsigned long dif=param.julian-julian;
    return (dif>=0)?true:false;
}

bool FXwDate::operator == (FXwDate param)
{
    if(year==param.year &&
        month==param.month &&
        day==param.day)
        return true;
    else
        return false;
}

int FXwDate::getQuarter()
{

    julian_to_mdy();

    FXwDate begin_q1(year,01,01,0,0);
    FXwDate begin_q2(year,04,01,0,0);
    FXwDate begin_q3(year,07,01,0,0);
    FXwDate begin_q4(year,10,01,0,0);


    if(*this>=begin_q1 && *this<begin_q2) {
        return 1;
    }
    else
        if(*this>=begin_q2 && *this<begin_q3) {
            return 2;
        }
        else
            if(*this>=begin_q3 && *this <begin_q4) {
                return 3;
            }
            else
                if(*this>=begin_q4) {
                    return 4;

                }

                return -1;

}
FXString FXwDate::text()
{
    FXString buffer;
    buffer.format("%02d.%02d.%02d",day,month,year);
    return buffer;
}
FXString FXwDate::quarterToShortText()
{

    julian_to_mdy();

    FXwDate begin_q1(year,01,01,0,0);
    FXwDate begin_q2(year,04,01,0,0);
    FXwDate begin_q3(year,07,01,0,0);
    FXwDate begin_q4(year,10,01,0,0);


    FXString buffer;

    if(*this>=begin_q1 && *this<begin_q2) {
        buffer.format("Q1/%d",year);
    }
    else
        if(*this>=begin_q2 && *this<begin_q3) {
            buffer.format("Q2/%d",year);
        }
        else
            if(*this>=begin_q3 && *this <begin_q4) {
                buffer.format("Q3/%d",year);
            }
            else
                if(*this>=begin_q4) {
                    buffer.format("Q4/%d",year);

                }


                return buffer;

}
FXString FXwDate::quarterToText()
{


    julian_to_mdy();

    FXwDate begin_q1(year,01,01,0,0);
    FXwDate begin_q2(year,04,01,0,0);
    FXwDate begin_q3(year,07,01,0,0);
    FXwDate begin_q4(year,10,01,0,0);


    FXString buffer;

    if(*this>=begin_q1 && *this<begin_q2) {
        buffer.format("1.quarter: 01.02.%d - 31.03.%d",year,year);
    }
    else
        if(*this>=begin_q2 && *this<begin_q3) {
            buffer.format("2.quarter: 01.04.%d - 30.06.%d",year,year);
        }
        else
            if(*this>=begin_q3 && *this <begin_q4) {
                buffer.format("3.quarter: 01.07.%d - 30.09.%d",year,year);
            }
            else
                if(*this>=begin_q4) {
                    buffer.format("4.quarter: 01.10.%d - 31.12.%d",year,year);

                }

                return buffer;

}

FXString  FXwDate::textWithDayName()
{
    FXString buffer;
    buffer.format("%s  %02d.%02d.%04d",getDayName().text(),day,month,year);
    return buffer;
}



FXString  FXwDate::textWidthoutYear()
{

    FXString buffer;
    buffer.format("%s  %02d.%02d",getDayName().text(),day,month);
    return buffer;
}

FXString  FXwDate::timeToText()
{

    FXString buffer;
    buffer.format("%02d:%02d",hour,minute);
    return buffer;

}
FXString FXwDate::monthYearToText()
{
    FXString buffer;
    buffer.format("%s %02d",getMonthFName(month-1).text(),year);
    return buffer;
}

FXString FXwDate::getMonthname()
{
    return getMonthFName(month-1);
}
FXString FXwDate::getShortDayName(FXint index)
{
    if(index>6) return getWDayName(0);
    return getWDayName(index);
}


FXString FXwDate::getDayName(FXint index)
{
    if(index>6) return getWDayName(0);
    return getWDayName(index);
}

FXString FXwDate::getMonthName(FXint index)
{
    if(index>11) return getMonthFName(11);
    return getMonthFName(index);
}



/*************************************************************
* Conversion routines
**************************************************************/

void FXwDate::julian_to_wday (void)
{
    day_of_week = (enum Wday) ((julian + 2) % 7 + 1);
}

/*************************************************************/


#define OCT5_1582		(2299160L)		/* "really" 15-Oct-1582  */
#define OCT14_1582		(2299169L)		/* "really"  4-Oct-1582  */
#define JAN1_1			(1721423L)

#define YEAR			(365)
#define FOUR_YEARS		(1461)
#define CENTURY 		(36524L)
#define FOUR_CENTURIES	(146097L)

void FXwDate::julian_to_mdy ()
{
    long	z,y;
    short 	m,d;
    int 	lp;

    z = julian+1;
    if (z >= OCT5_1582)
    {
        z -= JAN1_1;
        z  = z + (z/CENTURY)  - (z/FOUR_CENTURIES) -2;
        z += JAN1_1;

    }

    z = z - ((z-YEAR) / FOUR_YEARS);		/* Remove leap years before current year */
    y = z / YEAR;

    d = (short) (z - (y * YEAR));

    y = y - 4712;				/* our base year in 4713BC */
    if (y < 1)
        y--;

    lp = !(y & 3);				/* lp = 1 if this is a leap year. */

    if (d==0)
    {
        y--;
        d = (short) (YEAR + lp);
    }

    m  = (short) (d/30);		/* guess at month */

    while (DaysSoFar[lp][m] >=d)
        m--;					/* Correct guess. */

    d = (short) (d - DaysSoFar[lp][m]);

    day = d;

    month = (short) (m+1);

    year = (short) y;

    julian_to_wday ();
}

void FXwDate::mdy_to_julian (void)
{
    int		a;
    int		work_year=year;
    long	j;
    int 	lp;

    /* correct for negative year  (-1 = 1BC = year 0) */

    if (work_year < 0)
        work_year++;

    lp = !(work_year & 3);			/* lp = 1 if this is a leap year. */

    j =
        ((work_year-1) / 4)		+		/* Plus ALL leap years */
        DaysSoFar[lp][month-1]	+
        day					+
        (work_year * 365L)	+		/* Days in years */
        JAN1_1 			+
        -366;						/* adjustments */

    /* deal with Gregorian calendar */
    if (j >= OCT14_1582)
    {

        a = (int)(work_year/100);
        j = j+ 2 - a + a/4;			/* Skip days which didn't exist. */
    }

    julian = j;

    julian_to_wday ();
}

void FXwDate::validate()
{

}

FXDEFMAP(FXwDatePickerLabel) FXwDatePickerLabelMap[]={
        FXMAPFUNC(SEL_CLICKED,0,FXwDatePickerLabel::onClick),
        FXMAPFUNC(SEL_LEFTBUTTONPRESS,0,FXwDatePickerLabel::onLeftBtnPress),
        FXMAPFUNC(SEL_PAINT,0,FXwDatePickerLabel::onPaint)

};

// Object implementation
FXIMPLEMENT(FXwDatePickerLabel,FXLabel,FXwDatePickerLabelMap,ARRAYNUMBER(FXwDatePickerLabelMap))

    FXwDatePickerLabel::FXwDatePickerLabel(FXComposite* p,FXObject* tgt,FXSelector sel,const FXString& text,FXIcon* ic,FXuint opts,FXint x,FXint y,FXint w,FXint h,FXint pl,FXint pr,FXint pt,FXint pb) :
            FXLabel(p,text,ic,opts,x,y,w,h,pl,pr,pt,pb)
            {
                selector = sel;
                target = tgt;
                istoday = false;
                dayvalue=-1;
                todayColor=FXRGB(200,200,255);
            }

            long FXwDatePickerLabel::onClick(FXObject* obj,FXSelector sel,void* ptr)
            {
                return 1;
            }

            long FXwDatePickerLabel::onLeftBtnPress(FXObject*,FXSelector,void*ptr)
            {
                FXEvent *event=(FXEvent*)ptr;
                if((event->click_count>1) && target)
                        target->handle(this,FXSEL(SEL_DOUBLECLICKED,selector),0);
                if(target && target->handle(this,FXSEL(SEL_LEFTBUTTONPRESS,selector),0))
                    return 1;
                return 0;
            }

            long FXwDatePickerLabel::onPaint(FXObject* obj,FXSelector sel,void* ptr){

                if(istoday)
                {
                    this->setTextColor(FXRGB(255,150,150));
                }
                FXLabel::onPaint(obj,sel,ptr);
                return 1;
            }

            /*******************************************************************************/


            FXDEFMAP(cDateChooser) cDateChooserMap[]={
                FXMAPFUNC(SEL_LEFTBUTTONPRESS,cDateChooser::ID_LABEL,cDateChooser::onLabelSelected),
                FXMAPFUNC(SEL_DOUBLECLICKED,cDateChooser::ID_LABEL,cDateChooser::onLabelDBC),
                FXMAPFUNC(SEL_COMMAND,cDateChooser::ID_BUTTON_PREV,cDateChooser::onCmdPrev),
                FXMAPFUNC(SEL_COMMAND,cDateChooser::ID_BUTTON_TODAY,cDateChooser::onCmdToday),
                FXMAPFUNC(SEL_COMMAND,cDateChooser::ID_BUTTON_NEXT,cDateChooser::onCmdNext)
            };

            // Object implementation
            FXIMPLEMENT(cDateChooser,FXVerticalFrame,cDateChooserMap,ARRAYNUMBER(cDateChooserMap))



                /*******************************************************************************/


                // Init
                cDateChooser::cDateChooser(){
                selected=NULL;
            }



            cDateChooser::cDateChooser(FXComposite* p,FXwDatePickerHoliday *holi,FXObject* tgt,FXSelector sel,FXuint opts,FXint x,FXint y,FXint w,FXint h,FXint pl,FXint pr,FXint pt,FXint pb):
            FXVerticalFrame(p,opts,x,y,w,h,pl,pr,pt,pb){

                holiday = holi;
                selector = sel;
                target =tgt;
                selected =NULL;
                /* Default Colors */
                monthbkcolor = FXRGB(150,30,30);
                yearbkcolor = FXRGB(150,30,30);
                boderColorToday = FXRGB(150,30,30);
                selectColor = FXRGB(70,70,70);
                offdaycolor = FXRGB(180,0,0);
                holidaycolor = FXRGB(0,0,255);
                FXColor bcolor = FXRGB(241,240,240);
                FXColor bcolor2 = FXRGB(221,220,220);

                FXFont * f = getApp()->getNormalFont();
                FXint textheight = f->getTextHeight ("><");
                FXint textwidth = f->getTextWidth("30"); // possible max width

                /** Creating Components **/
                navframe = new FXHorizontalFrame(this,FRAME_SUNKEN|LAYOUT_FILL_X,0,0,textheight,0,0,0,0,0);
                navframe->setBackColor(yearbkcolor);
                prev= new FXArrowButton(navframe,this, ID_BUTTON_PREV,ARROW_REPEAT|FRAME_RAISED|LAYOUT_LEFT|ARROW_LEFT|LAYOUT_CENTER_Y|LAYOUT_FILL_Y|LAYOUT_FIX_WIDTH,0,0,15,15,1,1,2,2);
                FXHorizontalFrame *_hframe0=new FXHorizontalFrame(navframe,LAYOUT_CENTER_X|JUSTIFY_CENTER_X,0,0,0,0,0,0,0,0);
                _hframe0->setBackColor(yearbkcolor);
                monthname = new FXLabel(_hframe0,"Januar",NULL,FRAME_NONE);
                monthname->setBackColor(monthbkcolor);
                monthname->setTextColor(FXRGB(255,255,255));
                monthname->setFont(new FXFont(oApplicationManager,"Helvetica",8,FXFont::Bold));
                year = new FXLabel(_hframe0,"2004",NULL,FRAME_NONE);
                year->setBackColor(yearbkcolor);
                year->setTextColor(FXRGB(255,255,255));
                year->setFont(new FXFont(oApplicationManager,"Helvetica",8,FXFont::Bold));
                next = new FXArrowButton(navframe,this, ID_BUTTON_NEXT,ARROW_REPEAT|FRAME_RAISED| LAYOUT_CENTER_Y |LAYOUT_RIGHT|ARROW_RIGHT|LAYOUT_FILL_Y|LAYOUT_FIX_WIDTH,0,0,15,15,1,1,2,2);
                days =new FXMatrix(this,7,MATRIX_BY_COLUMNS| LAYOUT_FILL_COLUMN| LAYOUT_SIDE_TOP| LAYOUT_SIDE_RIGHT|LAYOUT_FILL_X|FRAME_SUNKEN,0,0,0,0,0,0,0,0,0,0);
                days->setBackColor(bcolor);


                FXLabel * l;
                l = new FXLabel(days,FXwDate::getShortDayName(1),NULL,FRAME_NONE|LAYOUT_FILL_X);
                l->setBackColor(bcolor2);
                l = new FXLabel(days,FXwDate::getShortDayName(2),NULL,FRAME_NONE|LAYOUT_FILL_X);
                l->setBackColor(bcolor2);
                l = new FXLabel(days,FXwDate::getShortDayName(3),NULL,FRAME_NONE|LAYOUT_FILL_X);
                l->setBackColor(bcolor2);
                l = new FXLabel(days,FXwDate::getShortDayName(4),NULL,FRAME_NONE|LAYOUT_FILL_X);
                l->setBackColor(bcolor2);
                l = new FXLabel(days,FXwDate::getShortDayName(5),NULL,FRAME_NONE|LAYOUT_FILL_X);
                l->setBackColor(bcolor2);
                l = new FXLabel(days,FXwDate::getShortDayName(6),NULL,FRAME_NONE|LAYOUT_FILL_X);
                l->setBackColor(bcolor2);
                l->setTextColor(offdaycolor);
                l = new FXLabel(days,FXwDate::getShortDayName(0),NULL,FRAME_NONE|LAYOUT_FILL_X);
                l->setBackColor(bcolor2);
                l->setTextColor(offdaycolor);

                FXint count  = 0;
                for(FXint i=0; i<42;i++)
                {
                    FXwDatePickerLabel *l = new FXwDatePickerLabel(days,this,ID_LABEL," ",NULL,FRAME_NONE|JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y|LAYOUT_FILL_Y|LAYOUT_CENTER_X|LAYOUT_CENTER_Y|LAYOUT_FIX_WIDTH|LAYOUT_FIX_HEIGHT,0,0,textwidth+6,textheight+6,0,0,0,0);
                    l->setBackColor(bcolor);
                    dayfields.append(l);
                    if(count == 5 || count==6) {
                        l->setTextColor(offdaycolor);
                    }
                    if(count==6) count = 0; else  count++;

                }

                selectedDate=actualDate;
                fill(actualDate);

            }

            cDateChooser::~cDateChooser()
            {
                if(holiday) delete holiday;
            }



            // Create window
            void cDateChooser::create(){
                FXVerticalFrame::create();
            }


            // Detach window
            void cDateChooser::detach(){
                FXVerticalFrame::detach();

            }
            void cDateChooser::setMonthBkColor(FXColor col)
            {
                monthbkcolor=col;
                navframe->setBackColor(monthbkcolor);
                monthname->setBackColor(monthbkcolor);
            }
            void cDateChooser::setYearBkColor(FXColor col)
            {
                yearbkcolor=col;
                navframe->setBackColor(yearbkcolor);
                year->setBackColor(yearbkcolor);
            }
            void cDateChooser::setSelectionColor(FXColor col)
            {
                selectColor=col;
            }
            void cDateChooser::setOffdayColor(FXColor col)
            {
                offdaycolor=col;
                FXint count = 0;
                for(FXint i=0; i<42;i++)
                {
                    if(count == 5 || count==6) {
                        ((FXLabel * )dayfields[i])->setTextColor(offdaycolor);
                    }
                    if(count==6) count = 0; else  count++;

                }
            }
            void cDateChooser::setHolidayColor(FXColor col)
            {
                holidaycolor=col;
            }


            long cDateChooser::onLabelDBC(FXObject* obj,FXSelector sel,void* ptr)
            {
                target->handle(this,FXSEL(SEL_COMMAND,FXDialogBox::ID_ACCEPT),NULL);
                return 1;
            }

            long cDateChooser::onLabelSelected(FXObject* obj,FXSelector sel,void* ptr)
            {
                int y;
                // selected öabel is not valid
                if(((FXwDatePickerLabel*)obj)->getDayValue()==-1) return 1;
                if(selected) {
                    selected->setBackColor(days->getBackColor());
                    y=selectedDate.getDayOfWeek();
                    if(y==0 || y==6)
                        selected->setTextColor(offdaycolor);
                    else
                        selected->setTextColor(FXRGB(0,0,0));
                }
                selected = (FXwDatePickerLabel *)obj;
                selected->setBackColor(selectColor);
                selected->setTextColor(FXRGB(255,255,255));
                // We have a date selected send a message to target
                //FXwDate d = getSelectedDate();
                selectedDate.setDay(selected->getDayValue());
                selectedDate.setMonth(actualDate.getMonth());
                selectedDate.setYear(actualDate.getYear());
                if(target) target->handle(this,FXSEL(SEL_LEFTBUTTONPRESS,selector),&selectedDate);

                return 1;

                //return 0;
            }
            long cDateChooser::onCmdPrev(FXObject* obj,FXSelector sel,void* ptr)
            {
                actualDate.setMonth(actualDate.getMonth()-1);
                fill(actualDate);
                if(target)  target->handle(this,FXSEL(SEL_CHANGED,selector),&actualDate);
                return 0;
            }
            long cDateChooser::onCmdToday(FXObject* obj,FXSelector sel,void* ptr)
            {

                actualDate = FXwDate();
                fill(actualDate);
                if(target) target->handle(this,FXSEL(SEL_CHANGED,selector),&actualDate);

                return 0;
            }
            long cDateChooser::onCmdNext(FXObject* obj,FXSelector sel,void* ptr)
            {
                actualDate.setMonth(actualDate.getMonth()+1);
                fill(actualDate);
                if(target) target->handle(this,FXSEL(SEL_CHANGED,selector),&actualDate);
                return 0;
            }

            void cDateChooser::reset()
            {
                actualDate = FXwDate();
                fill(actualDate);

            }
            void cDateChooser::fill(FXwDate &adate)
            {

                FXint y,i = 0;
                FXString buffer ="";
                FXwDate firstdayofmonth(adate.getYear(),adate.getMonth(),1);
                FXwDate today;
                FXwDate tempdate =firstdayofmonth;
                if (selected){
                    selected->setBackColor(days->getBackColor());
                    y=selectedDate.getDayOfWeek();
                    if(y==0 || y==6)
                        selected->setTextColor(offdaycolor);
                    else
                        selected->setTextColor(FXRGB(0,0,0));
                }
                selected=NULL;

                monthname->setText(adate.getMonthname());
                buffer.format("%04d",adate.getYear());
                year->setText(buffer);

                FXint dayindex = (firstdayofmonth.getDayOfWeek()==0)?6:firstdayofmonth.getDayOfWeek()-1;

                // Delete unused

                for(i=0; i<dayfields.no();i++) {
                    ((FXLabel * )dayfields[i])->setText(" ");
                    ((FXwDatePickerLabel * )dayfields[i])->setToday(false);
                    ((FXwDatePickerLabel * )dayfields[i])->setDayValue(-1);
                    ((FXwDatePickerLabel * )dayfields[i])->setTextColor(FXRGB(0,0,0));
                }


                FXwDate d=adate;
                for(i=dayindex; i<adate.getDaysOfMonth(adate.getMonth())+dayindex;i++) {
                    buffer.format("%d",i-dayindex+1);
                    ((FXLabel * )dayfields[i])->setText(buffer);
                    ((FXwDatePickerLabel * )dayfields[i])->setDayValue(i-dayindex+1);

                    d.setDay(i-dayindex+1);
                    if(today == d) {
                        ((FXwDatePickerLabel * )dayfields[i])->setToday();
                        ((FXwDatePickerLabel * )dayfields[i])->setTodayColor(boderColorToday);
                    }
                    if (selectedDate == d){
                        selected = (FXwDatePickerLabel * )dayfields[i];
                        selected->setBackColor(selectColor);
                        selected->setTextColor(FXRGB(255,255,255));
                        continue;
                    }
                    tempdate.setDay(i-dayindex+1);
                    if(tempdate.getDayOfWeek()==0 || tempdate.getDayOfWeek()==6) {
                        ((FXLabel * )dayfields[i])->setTextColor(offdaycolor);
                    }
                    else
                        ((FXLabel * )dayfields[i])->setTextColor(FXRGB(0,0,0));
                }
            }

            FXwDate  cDateChooser::getSelectedDate()
            {
                /*if(selected)
                {
                    actualDate.setDay(selected->getDayValue());
                }
                return actualDate;*/
                return selectedDate;

            }

            FXwDate  cDateChooser::setSelectedDate(FXwDate d)
            {
                actualDate=d;
                selectedDate=d;
                fill(actualDate);
                return selectedDate;
            }

FXString cDateChooser::selectDate(FXWindow *prOwner)
{
    FXDialogBox dlg(prOwner==NULL?oWinMain:prOwner,oLanguage->getText("str_dlg_dc_title"),DECOR_NONE,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe0=new FXVerticalFrame(&dlg,LAYOUT_FILL|FRAME_LINE,0,0,0,0,DEFAULT_PAD,DEFAULT_PAD,DEFAULT_PAD,DEFAULT_PAD);
    _vframe0->setBorderColor(FXRGB(150,30,30));
    cDateChooser *_dc=new cDateChooser(_vframe0,NULL,&dlg,0);

    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(_vframe0,LAYOUT_CENTER_X|JUSTIFY_CENTER_X,0,0,0,0,0,0,0,DEFAULT_PAD);
    new FXButton(_hframe0,oLanguage->getText("str_but_accept"),NULL,&dlg,FXDialogBox::ID_ACCEPT,BUTTON_DEFAULT|BUTTON_INITIAL|FRAME_RAISED|FRAME_THICK|LAYOUT_CENTER_X);
    new FXButton(_hframe0,oLanguage->getText("str_but_cancel"),NULL,&dlg,FXDialogBox::ID_CANCEL,BUTTON_DEFAULT|FRAME_RAISED|FRAME_THICK|LAYOUT_CENTER_X);

    if(!dlg.execute(PLACEMENT_CURSOR))
        return "";
    FXwDate ret=_dc->getSelectedDate();
    return FXStringFormat("%04d-%02d-%02d",ret.getYear(),ret.getMonth(),ret.getDay());
}
