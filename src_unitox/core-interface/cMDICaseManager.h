#ifndef CMDICASEMANAGER_H
#define CMDICASEMANAGER_H

#include <fx.h>
#include "cSortList.h"
#include "cMDIChild.h"
#include "cColorTable.h"
#include "cCaseManager.h"

class cMDICaseManager : public cMDIChild
{
    FXDECLARE(cMDICaseManager);

    private:
        FXTabBook *tbMain;
        FXTabItem *tiQuick;
        FXTabItem *tiDetailed;
        FXTabItem *tiList;
        FXButton *butSelall,*butDesel;

        FXGIFIcon *critIcon;
        FXGIFIcon *deadIcon;

        FXButton *proceed;
        FXLabel *nocWarn,*nocTitle;
        FXLabel *paramsTitle;
        FXTextField *coma,*comb;
        FXTextField *datea,*dateb;
        FXTextField *casea,*caseb;
        cSortList *assayList;
        FXIconList *criteriaList;
        cColorTable *params;
        cSortList *cases;

        FXMDIClient *client;
        FXPopup *popup;
        FXint selSource;

        void (*targetFunc)(FXMDIClient*,FXPopup*,cSortList&);

        void update(void);
    protected:
        cMDICaseManager();

        void makeSelection(int prSource);
        void drawByDict(void);
        void drawCases(sCaseObject *prCases,FXint prCount,FXbool prAddCols);

    public:
        cMDICaseManager(FXMDIClient *prP, const FXString &prName, FXIcon *prIc=NULL, FXPopup *prPup=NULL, FXuint prOpts=0, FXint prX=0, FXint prY=0, FXint prW=0, FXint prH=0);
        virtual ~cMDICaseManager();

        virtual void create();

        static void load(FXMDIClient *prP,FXPopup *prMenu);
        static void load(FXMDIClient *prP,FXPopup *prMenu,void (*prFunction)(FXMDIClient*,FXPopup*,cSortList&),FXString prForTitle="");

        static FXbool isLoaded(void);
        static void unload(void);
        static void unshow(void);
        static void cmdRefresh(void);
        static void cmdResearch(void);

        cSortList *getCases(void);

        enum
        {
            ID_CASEMANAGER=FXMDIChild::ID_LAST,
            ID_TBMAIN,

            CMD_BUT_OPEN,
            CMD_BUT_REMOVE,
            CMD_BUT_PRINT,
            CMD_BUT_CLOSE,
            CMD_BUT_QSELECT,
            CMD_BUT_DSELECT,
            CMD_BUT_PROCEED,

            ID_QCRIT,
            ID_DCRIT,
            ID_DCRITLIST,
            ID_CASELIST,
            ID_SELALL0,
            ID_SELALL1,
            ID_LSALL,
            ID_LDSEL,

            CMD_DC0,
            CMD_DC1,

            ID_LAST
        };

        long onCmdChangeCrit(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdProceed(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSelAll0(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSelAll1(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdQSelect(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdDSelect(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdDateChooser0(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdDateChooser1(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdOpen(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdRemove(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdCriteria(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdLSall(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdLDsel(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCritLooseFocus(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdCriteria(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdProceed(FXObject *prSender,FXSelector prSelector,void *prData);
        long onRszDCritList(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
