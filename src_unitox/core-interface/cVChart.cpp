#include "engine.h"
#include "cLanguage.h"
#include "cVChart.h"
#include "cColorsManager.h"
#include "cDCNativePrinter.h"
#include <math.h>

#define CVCHART_AXIS_MIN_TICK 2
#define CVCHART_AXIS_MAJ_TICK 5
#define CVCHART_BORDER_SPACING 10
#define CVCHART_SPACING 3
#define CVCHART_CLUSTER_SP 10
#define CVCHART_DOTT 7
#define CVCHART_DOTW 11

// Must be 0. Set to 1 ONLY for development purposes
#define __ONLY_BW 0

FXDEFMAP(cVChart) mapVChart[]=
{
    FXMAPFUNC(SEL_MOUSEWHEEL,0,cVChart::onEvtRedir),
    FXMAPFUNC(SEL_PAINT,0,cVChart::onPaint),
};

FXIMPLEMENT(cVChart,FXFrame,mapVChart,ARRAYNUMBER(mapVChart))

cVChart::cVChart()
{
}

FXbool cVChart::checkVal(double prValue) {
    double no=round(prValue*1000)/1000;
    FXString val=FXStringFormat("%.13g",no);
    if(prValue<=-9999 || prValue>=9999 || val.lower()=="inf" || val.lower()=="nan")
        return false;
    return true;
}

FXString cVChart::sampleVal(double prValue) {
    return FXStringFormat("%g",round(prValue*1000)/1000);
}

bool cVChart::compute(FXDC &prDC)
{
    dataCount=values->no();
    if(!dataCount)
        return false;

    for(int i = 0; i < dataCount; i++)
        if(!checkVal(((sVChartData*)values->data(i))->value))
            return false;

    sVChartData *lcd = (sVChartData*)values->data(values->last());
    sVChartData *fcd = (sVChartData*)values->data(values->first());

    if(fcd->value > lcd->value) {
        dataMin=floor(lcd->value > lcd->tvalue ? lcd->tvalue : lcd->value);
        dataMax=ceil(fcd->value > fcd->tvalue ? fcd->value : fcd->tvalue);
    }
    else {
        dataMax=floor(lcd->value > lcd->tvalue ? lcd->tvalue : lcd->value);
        dataMin=ceil(fcd->value > fcd->tvalue ? fcd->value : fcd->tvalue);
    }

    double delta = fabs(dataMax - dataMin);

    dataMin -= delta * 0.1;
    dataMax += delta * 0.1;

    unit=1;
    if(!dataMax)
        dataMax=1;
    for(int n=dataMax;n;n/=10,unit*=10);
    unit/=10;
    if(dataMax%unit)
        dataMax=dataMax-(dataMax%unit)+unit;
    if(dataMin%unit || dataMin==dataMax)
        dataMin-=dataMax%unit;
    if(fontValues)
        delete fontValues;
    if(fontTitle)
        delete fontTitle;
    if(fontAxis)
        delete fontAxis;

    int dw=(int)sqrt((width*height)/((double)dataCount*1.4));
    if(dw>100)dw=100;
    if(dw<40)dw=40;
    fontValues=new FXFont(oApplicationManager,"Helvetica",(int)(dw/11),FXFont::Normal);
    dw=(width-(CVCHART_BORDER_SPACING>>1))/10;
    if(dw>70)dw=70;
    if(dw<50)dw=50;
    fontTitle=new FXFont(oApplicationManager,"Helvetica",(int)(dw*2/10),FXFont::Bold);
    fontAxis=new FXFont(oApplicationManager,"Helvetica",(int)(dw*1.5/10),FXFont::Bold);
    fontAxis->create();
    fontValues->create();
    fontTitle->create();

    th=title.empty()?0:fontTitle->getTextHeight(title);
    vah=fontAxis->getTextHeight(vaxis);
    hah=fontAxis->getTextHeight(haxis);
    vw=fontValues->getTextWidth(FXStringVal(dataMax));
    vh=fontValues->getTextHeight(FXStringVal(dataMax));

    dataW=width-(border<<1)-(CVCHART_BORDER_SPACING*3)-vw-vah-CVCHART_AXIS_MAJ_TICK-CVCHART_SPACING;
    lh=0;
    dataX=border+(CVCHART_BORDER_SPACING<<1)+vw+vah+CVCHART_AXIS_MAJ_TICK+CVCHART_SPACING;
    dataY=border+(CVCHART_BORDER_SPACING<<1)+(CVCHART_SPACING*6)+th;
    dataH=height-(border<<1)-(CVCHART_BORDER_SPACING*3)-lh-vh-hah-th-CVCHART_AXIS_MAJ_TICK-(CVCHART_SPACING*6);

    unitPixels=(FXdouble)dataH/((FXdouble)(dataMax-dataMin)/(FXdouble)unit);
    /*elementPixels=unitPixels/unit;
    dataPixels=(FXdouble)dataW/(FXdouble)clusterCount;
    space=(FXdouble)dataPixels/CVCHART_CLUSTER_SP;
    valPixels=clusterPixels/(FXdouble)setCount;*/
    return true;
}

void cVChart::drawCanvas(FXDC &prDC)
{
    prDC.setForeground(canvasColor);
    prDC.fillRectangle(0,0,width,height);
}

void cVChart::drawData(FXDC &prDC)
{
    double dwidth;
    double doff;
    double x, y, yy;

    if(type == CHART_LOG) {
        dwidth = dataW / dataCount;
        doff = dwidth / 2;
    }
    else {
        x=((sVChartData*)values->data(values->last()))->xvalue * 1.1;
        dwidth = x / dataW;
        doff = dwidth / 2;
    }

    sVChartData *d;

    double x1,y1,x2,y2;
    x1=x2=y1=y2=0;

    prDC.setLineWidth(1);

    for(int i=0;i<dataCount;i++)
    {
        d = ((sVChartData*)values->find(FXStringFormat("%d",i).text()));

        if(type == CHART_LOG) {
            x = (double)dataX + (double)i * dwidth + doff;
        }
        else {
            x = d->xvalue;
            x = (double)dataX + (x / dwidth);
        }

        if(i == 0) {
            x1 = x;
            y1 = dataY+dataH+1 - (d->tvalue - dataMin) * unitPixels;
        }
        if(i == dataCount - 1) {
            x2 = x;
            y2 = dataY+dataH+1 - (d->tvalue - dataMin) * unitPixels;
        }

        y = dataY+dataH+1 - (d->value - dataMin) * unitPixels;
        yy = dataY+dataH+1 - (d->tvalue - dataMin) * unitPixels;

        if((i == 0) || (i == dataCount - 1)) {
            prDC.setForeground(trendColor);
            prDC.fillEllipse(
                (int)x - (CVCHART_DOTT / 2),
                (int)yy - (CVCHART_DOTT / 2),
                CVCHART_DOTT,CVCHART_DOTT);
        }

        prDC.setForeground(dataColor);
        prDC.fillEllipse(
            (int)x - (CVCHART_DOTW / 2),
            (int)y - (CVCHART_DOTW / 2),
            CVCHART_DOTW,CVCHART_DOTW);
    }

    prDC.setLineStyle(LINE_SOLID);
    prDC.setLineWidth(3);

    prDC.setForeground(trendColor);
    prDC.drawLine(x1,y1,x2,y2);

    prDC.setLineWidth(1);

}

void cVChart::drawAxes(FXDC &prDC)
{
    double r,g,b;

    r=FXREDVAL(canvasColor)-8;
    g=FXGREENVAL(canvasColor)-8;
    b=FXBLUEVAL(canvasColor)-8;
    prDC.setForeground(FXRGB(r,g,b));
    prDC.fillRectangle(dataX,dataY,dataW,dataH+1);

    prDC.setFont(fontValues);
    FXString s="0";
    FXint t=dataMin,w=fontValues->getTextWidth(s),h=(int)floor(vh/2.8);

    for(FXdouble i=(FXdouble)(dataY+dataH);i>=(FXdouble)dataY-1;i-=unitPixels,t+=unit)
    {
        s=FXStringFormat("%d",t);
        w=fontValues->getTextWidth(s);

        prDC.setForeground(borderColor);
        prDC.drawLine(dataX-1,(int)i,dataX-1-CVCHART_AXIS_MAJ_TICK,(int)i);

        prDC.setForeground(valuesColor);
        prDC.drawText(dataX-1-CVCHART_AXIS_MAJ_TICK-CVCHART_SPACING-w,(int)i+h,s.text(),s.length());

        {
            prDC.setLineStyle(LINE_ONOFF_DASH);
            prDC.setForeground(t == 0 ? borderColor : gridColor);
            prDC.drawLine(dataX,(int)i,dataX+dataW,(int)i);
            prDC.setLineStyle(LINE_SOLID);
        }
    }

    double dwidth;
    double doff;
    double x;

    if(type == CHART_LOG) {
        dwidth = dataW / dataCount;
        doff = dwidth / 2;
    }
    else {
        x=((sVChartData*)values->data(values->last()))->xvalue * 1.1;
        dwidth = x / dataW;
        doff = dwidth / 2;
    }

    sVChartData *d;
    for(int i=0;i<dataCount;i++)
    {
        d = ((sVChartData*)values->find(FXStringFormat("%d",i).text()));

        if(type == CHART_LOG) {
            x = (double)dataX + (double)i * dwidth + doff;
        }
        else {
            x = d->xvalue;
            x = (double)dataX + (x / dwidth);
        }

        prDC.setForeground(borderColor);
        prDC.drawLine((int)x,dataY+dataH+1,(int)x,dataY+dataH+1+CVCHART_AXIS_MAJ_TICK);

        prDC.setLineStyle(LINE_ONOFF_DASH);
        prDC.setForeground(gridColor);
        prDC.drawLine((int)x,dataY+dataH,(int)x,dataY);
        prDC.setLineStyle(LINE_SOLID);

        prDC.setForeground(valuesColor);
        s=d->name;
        prDC.drawText((int)x-(fontValues->getTextWidth(s)>>1),dataY+dataH+1+CVCHART_AXIS_MIN_TICK+vh,s.text(),s.length());
    }

    prDC.setLineWidth(1);
    prDC.setForeground(gridColor);
    prDC.drawLine(dataX+dataW,dataY,dataX+dataW,dataY+dataH);
    prDC.drawLine(dataX,dataY,dataX+dataW,dataY);

    prDC.setLineWidth(2);
    prDC.setForeground(borderColor);
    prDC.drawLine(dataX-1,dataY,dataX-1,dataY+dataH+1);
    prDC.drawLine(dataX-1,dataY+dataH+1,dataX+dataW+1,dataY+dataH+1);

    prDC.setForeground(borderColor);
    if(!title.empty())
    {
        prDC.setFont(fontTitle);
        prDC.drawText((width>>1)-(fontTitle->getTextWidth(title)>>1)+(int)(width*0.025),border+CVCHART_BORDER_SPACING+(CVCHART_SPACING*3)+(int)(th/1.2),title.text(),title.length());
    }

    if(!haxis.empty())
    {
        prDC.setFont(fontAxis);
        prDC.drawText(dataX+(dataW>>1)-(fontAxis->getTextWidth(haxis)>>1),dataY+dataH+CVCHART_AXIS_MAJ_TICK+(CVCHART_SPACING<<1)+vh+fontAxis->getTextHeight(haxis),haxis.text(),haxis.length());
    }

    if(!vaxis.empty())
    {
        prDC.setFont(fontAxis);
        int dvah=(int)(vah/1.1);
        int y=dataY+(dataH>>1)-((vaxis.length()*dvah)>>1)+dvah;
        for(int i=0;i<vaxis.length();i++,y+=dvah)
        {
            s=vaxis.mid(i,1);
            prDC.drawText(border+CVCHART_BORDER_SPACING+(vah>>1)-(fontAxis->getTextWidth(s)>>1),y,s.text(),1);
        }
    }
}

cVChart::cVChart(FXComposite *prP, FXuint prType, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH)
    :FXFrame(prP,prOpts,prX,prY,prW,prH,0,0,0,0)
{
    setTarget(prP);
    enable();
    type=prType;

    title="";
    haxis="";
    vaxis="";

    canvasColor=FXRGB(255,255,255);
    borderColor=FXRGB(0,0,0);
    gridColor=FXRGB(210,210,210);
    valuesColor=FXRGB(170,0,0);
    dataColor=FXRGB(0,0,0);
    trendColor=FXRGB(0,100,0);

    doubleBuffering=false;
    values=new FXDict();
    fontValues=NULL;
    fontTitle=NULL;
    fontAxis=NULL;

    isprint=false;
}

cVChart::~cVChart()
{
    delete values;
}

void cVChart::create()
{
    FXFrame::create();
}

FXString cVChart::getTitle(void)
{
    return title;
}

void cVChart::setTitle(FXString prText)
{
    title=prText;
}

void cVChart::appendHAxisTitle(FXString prText)
{
    haxis=haxis + prText;
}

void cVChart::setHAxisTitle(FXString prText)
{
    haxis=prText;
}

void cVChart::setVAxisTitle(FXString prText)
{
    vaxis=prText;
}

void cVChart::setType(FXuint prType)
{
    type=prType;
}

FXDict *cVChart::dataSet(void)
{
    return values;
}

FXbool cVChart::getDoubleBuffering(void)
{
    return doubleBuffering;
}

void cVChart::setDoubleBuffering(FXbool prFlag)
{
    doubleBuffering=prFlag;
}

long cVChart::onEvtRedir(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXObject *target=getTarget();
    if(!target)
        return 1;
    return target->tryHandle(prSender,prSelector,prData);
}

long cVChart::onPaint(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXEvent *event=(FXEvent*)prData;
    FXDC *rdc;

    if(prSender==NULL && prSelector==0)
    {
        isprint=true;
        rdc=(FXDC*)prData;
        rdc->drawRectangle(10,10,100,100);
        rdc->fillRectangle(40,40,60,60);
    }
    else
    {
        isprint=false;
        rdc=new FXDCWindow(this,event);
    }

    if(width<200 || height<120)
        return 1;

    if(!compute(*rdc))
    {
        drawCanvas(*rdc);
        FXFont *ft=new FXFont(oApplicationManager,"helvetica",7);
        ft->create();
        rdc->setFont(ft);
        rdc->setForeground(FXRGB(255,0,0));
        rdc->drawText(10,20,title.text(),title.length());
        FXString msg=oLanguage->getText("str_invalid_data");
        rdc->drawText(10,35,msg.text(),msg.length());
        delete ft;
        if(rdc  && (prSender!=NULL || prSelector!=0))
            delete rdc;
        return 1;
    }

    /*if(doubleBuffering)
    {
    FXBMPImage *picture=new FXBMPImage(getApp(),NULL,IMAGE_SHMI|IMAGE_SHMP,width,height);
    picture->create();
    FXDC dc(picture);

    drawCanvas(dc);
    drawAxes(dc);
    drawData(dc);
    drawLegend(dc);

    rdc.drawImage(picture,0,0);
    delete picture;
    }
    else*/
    //{
        rdc->clearClipRectangle();
        drawCanvas(*rdc);
        drawAxes(*rdc);
        drawData(*rdc);
    //}
    if(rdc  && (prSender!=NULL || prSelector!=0))
        delete rdc;
    return 1;
}
