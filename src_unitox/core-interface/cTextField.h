#ifndef CTEXTFIELD_H
#define CTEXTFIELD_H

#include <fx.h>

class cTextField : public FXTextField
{
    FXDECLARE(cTextField);
    
    private:
    protected:
        cTextField();
    
    public:
        cTextField(FXComposite *p, FXint ncols, FXObject *tgt=NULL, FXSelector sel=0, FXuint opts=0, FXint x=0, FXint y=0, FXint w=0, FXint h=0, FXint pl=3, FXint pr=3, FXint pt=2, FXint pb=2);
        ~cTextField();
        
        long onPaint(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
