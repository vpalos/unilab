#ifndef CVCHART_H
#define CVCHART_H

#include <fx.h>

typedef struct sVChartData
{
    FXString name;
    FXdouble value;
    FXdouble xvalue;
    FXdouble tvalue;
};

class cVChart : public FXFrame
{
    FXDECLARE(cVChart);

    private:
    protected:
        FXuint type;
        FXbool isprint;

        FXint dataMin,dataMax,unit;
        FXint vw,vh,lh,vah,hah,th,lvh;
        int tw[20],tn[20],rows;
        FXint dataX,dataY,dataW,dataH;
        FXint dataCount;
        FXdouble unitPixels;

        FXString title;
        FXString vaxis;
        FXString haxis;

        FXColor canvasColor;
        FXColor borderColor;
        FXColor gridColor;
        FXColor valuesColor;
        FXColor dataColor;
        FXColor trendColor;

        FXbool doubleBuffering;

        FXDict *values;

        FXFont *fontTitle;
        FXFont *fontAxis,*fontValues;

        cVChart();

        bool compute(FXDC &prDC);
        FXbool checkVal(double prValue);
        FXString sampleVal(double prValue);

        void drawCanvas(FXDC &prDC);
        void drawData(FXDC &prDC);
        void drawAxes(FXDC &prDC);

    public:
        enum
        {
            CHART_LOG=0x0000,
            CHART_LINEAR=0x1000
        };

        cVChart(FXComposite *prP, FXuint prType=CHART_LOG, FXuint prOpts=0, FXint prX=0, FXint prY=0, FXint prW=0, FXint prH=0);
        ~cVChart();

        virtual void create();

        void setType(FXuint prType=CHART_LOG);

        FXString getTitle(void);
        void setTitle(FXString prText);
        void appendHAxisTitle(FXString prText);
        void setHAxisTitle(FXString prText);
        void setVAxisTitle(FXString prText);

        FXDict* dataSet(void);

        FXbool getDoubleBuffering(void);
        void setDoubleBuffering(FXbool prFlag);

        long onPaint(FXObject *prSender,FXSelector prSelector,void *prData);
        long onEvtRedir(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
