#include "cHorizontalSeparator.h"

FXDEFMAP(cHorizontalSeparator) mapHorizontalSeparator[]=
{
    FXMAPFUNC(SEL_PAINT,0,cHorizontalSeparator::onPaint),
};

FXIMPLEMENT(cHorizontalSeparator,FXHorizontalSeparator,mapHorizontalSeparator,ARRAYNUMBER(mapHorizontalSeparator))

cHorizontalSeparator::cHorizontalSeparator()
{
}

cHorizontalSeparator::cHorizontalSeparator(FXComposite *p, FXuint opts, FXint x, FXint y, FXint w, FXint h, FXint pl, FXint pr, FXint pt, FXint pb):
    FXHorizontalSeparator(p,opts,x,y,w,h,pl,pr,pt,pb)
{
}

cHorizontalSeparator::~cHorizontalSeparator()
{
}

long cHorizontalSeparator::onPaint(FXObject *prSender,FXSelector prSelector,void *prData)
{
  FXDC *dc;
  FXEvent *ev = NULL;
  if(prSender==NULL && prSelector==0)
      dc=(FXDC*)prData;
  else
  {
      ev=(FXEvent*)prData;
      dc=new FXDCWindow(this,ev);
  }

  register FXint kk,ll;
  dc->clearClipRectangle();
  // Draw background
  dc->setForeground(backColor);
  if(prSender==NULL && prSelector==0)
      dc->fillRectangle(0,0,width,height);
  else
      dc->fillRectangle(ev->rect.x,ev->rect.y,ev->rect.w,ev->rect.h);

  // Draw frame
  drawFrame(*(FXDCWindow*)dc,0,0,width,height);

  // Horizonal orientation
  if((height-padbottom-padtop) < (width-padleft-padright)){
    kk=(options&(SEPARATOR_GROOVE|SEPARATOR_RIDGE)) ? 2 : 1;
    ll=border+padtop+(height-padbottom-padtop-(border<<1)-kk)/2;
    if(options&SEPARATOR_GROOVE){
      dc->setForeground(shadowColor);
      dc->fillRectangle(border+padleft,ll,width-padright-padleft-(border<<1),1);
      dc->setForeground(hiliteColor);
      dc->fillRectangle(border+padleft,ll+1,width-padright-padleft-(border<<1),1);
      }
    else if(options&SEPARATOR_RIDGE){
      dc->setForeground(hiliteColor);
      dc->fillRectangle(border+padleft,ll,width-padright-padleft-(border<<1),1);
      dc->setForeground(shadowColor);
      dc->fillRectangle(border+padleft,ll+1,width-padright-padleft-(border<<1),1);
      }
    else if(options&SEPARATOR_LINE){
      dc->setForeground(borderColor);
      dc->fillRectangle(border+padleft,ll,width-padright-padleft-(border<<1),1);
      }
    }

  // Vertical orientation
  else{
    kk=(options&(SEPARATOR_GROOVE|SEPARATOR_RIDGE)) ? 2 : 1;
    ll=border+padleft+(width-padleft-padright-(border<<1)-kk)/2;
    if(options&SEPARATOR_GROOVE){
      dc->setForeground(shadowColor);
      dc->fillRectangle(ll,padtop+border,1,height-padtop-padbottom-(border<<1));
      dc->setForeground(hiliteColor);
      dc->fillRectangle(ll+1,padtop+border,1,height-padtop-padbottom-(border<<1));
      }
    else if(options&SEPARATOR_RIDGE){
      dc->setForeground(hiliteColor);
      dc->fillRectangle(ll,padtop+border,1,height-padtop-padbottom-(border<<1));
      dc->setForeground(shadowColor);
      dc->fillRectangle(ll+1,padtop+border,1,height-padtop-padbottom-(border<<1));
      }
    else if(options&SEPARATOR_LINE){
      dc->setForeground(borderColor);
      dc->fillRectangle(ll,padtop+border,1,height-padtop-padbottom-(border<<1));
      }
    }

  if(dc  && (prSender!=NULL || prSelector!=0))
    delete dc;
  return 1;
}
