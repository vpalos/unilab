#include <time.h>
#include <ctype.h>
#include <stdlib.h>

#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cColorsManager.h"
#include "cFormulaRatio.h"
#include "cAssayManager.h"
#include "cCaseManager.h"
#include "cWinMain.h"
#include "cMDIAssay.h"
#include "cMDIAssayManager.h"
#include "cMDICaseManager.h"

FXDEFMAP(cMDIAssay) mapMDIAssay[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIAssay::ID_MDIASSAY,cMDIAssay::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYID,cMDIAssay::onCmdTfId),
    FXMAPFUNC(SEL_CHANGED,cMDIAssay::ID_ASYID,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYTITLE,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYFILTERA,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYFILTERB,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIAssay::ID_ASYCAL,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYCOUNT,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYUNIT,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYSC,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_REPLACED,cMDIAssay::ID_ASYBINS,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIAssay::ID_ASYCOMMENTS,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::CMD_CLOSE,cMDIAssay::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::CMD_SAVE,cMDIAssay::onCmdSave),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::CMD_PRINT,cMDIAssay::onCmdPrint),
};

FXIMPLEMENT(cMDIAssay,cMDIChild,mapMDIAssay,ARRAYNUMBER(mapMDIAssay))

cMDIAssay::cMDIAssay()
{
}

FXbool cMDIAssay::updateData(void)
{
    saved=false;

    FXdouble v;
    if(tfFilterA->getText().empty())
        tfFilterA->setText("410");
    if(tfFilterB->getText().empty())
        tfFilterB->setText("0");
    if(tfUnit->getText().empty())
        tfUnit->setText("ppm");

    int spc = spControls->getValue();
    int i = tbBins->getNumRows();
    if(spc > tbBins->getNumRows()) {
        tbBins->insertRows(i, 1, false);
        tbBins->setRowText(i,FXStringFormat("Std%d",i+1));
        tbBins->getRowHeader()->setItemJustify(i,JUSTIFY_CENTER_X);
        tbBins->setItemText(i,0,"0");
        tbBins->setItemJustify(i,0,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
        tbBins->setItemText(i,1,"0");
        tbBins->setItemJustify(i,1,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
    }
    else if(spc < tbBins->getNumRows()) {
        tbBins->removeRows(spc, tbBins->getNumRows()-spc);
    }

    bool b;
    FXStipplePattern st;
    if(cbSC->getCheck()) {
        b = true;
        st = STIPPLE_NONE;
    }
    else {
        b = false;
        st = STIPPLE_GRAY;
    }
    for(int i=0;i<tbBins->getNumRows();i++)
    {
        v=FXDoubleVal(tbBins->getItemText(i,0));
        if(v<0)v=0;
        if(v>1000)v=1000;
        tbBins->setItemText(i,0,FXStringVal(v));
        v=FXIntVal(tbBins->getItemText(i,1));
        if(v<0)v=0;
        if(v>100)v=100;
        if(i==0)v=100;
        tbBins->setItemText(i,1,b?FXStringVal(v):"n/a");
        tbBins->setCellEditable(i,1,i==0?false:b);
        tbBins->setItemStipple(i,1,i==0?STIPPLE_GRAY:st);
    }

    return false;
}

FXbool cMDIAssay::saveData(void)
{
    tbBins->acceptInput(true);

    if(updateData())
        return false;

    if(saved)
        return true;

    if(!name.empty() && cAssayManager::assaySolid(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_eraseasysolid").text());
        saved=true;
        return 1;
    }

    FXString oldname=name;
    name=tfId->getText();
    if(name!=oldname && cAssayManager::assayExists(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
        tfId->setText(cAssayManager::getNewID());
        setTitle(tfId->getText());
        updateData();
        return false;
    }
    this->setTitle(name);

    FXString calculation = lxCalculus->getItem(lxCalculus->getCurrentItem()).before('=').trim();
    FXString unit = tfUnit->getText().trim();
    FXString controls_defs="";

    FXdouble v1,v2,b1,b2;
    FXString s;
    for(int i=0;i<tbBins->getNumRows()-1;i++)
    {
        v1=FXFloatVal(tbBins->getItemText(i,0));
        v2=FXFloatVal(tbBins->getItemText(i+1,0));
        b1=FXFloatVal(tbBins->getItemText(i,1));
        b2=FXFloatVal(tbBins->getItemText(i+1,1));
        if(v1>v2 || b1<b2)
        {
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_asycontrols").text());
            return false;
        }
    }

    controls_defs=controls_defs+FXStringVal(cbSC->getCheck())+"\n";
    for(int i=0;i<tbBins->getNumRows();i++)
    {
        controls_defs=controls_defs+tbBins->getItemText(i,0)+"\t";
        controls_defs=controls_defs+tbBins->getItemText(i,1)+"\n";
    }

    if(oldname.empty())
        cAssayManager::addAssay(name,tfTitle->getText(),tfFilterA->getText(),tfFilterB->getText(),controls_defs,calculation,unit,comments->getText());
    else
        cAssayManager::setAssay(oldname,name,tfTitle->getText(),tfFilterA->getText(),tfFilterB->getText(),controls_defs,calculation,unit,comments->getText());

    saved=true;
    cMDIAssayManager::cmdRefresh();
    cMDICaseManager::cmdRefresh();
    cMDICaseManager::cmdResearch();
    return true;
}

cMDIAssay::cMDIAssay(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH)
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_MDIASSAY);
    saved=false;
    name="";

    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL,0,0,0,0);

    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
        FXVerticalFrame *_vframe1=new FXVerticalFrame(_hframe0,0,0,0,0,0,0,0,0,0);
            new FXLabel(_vframe1,oLanguage->getText("str_asymdi_id"),NULL,LAYOUT_CENTER_X);
            tfId=new FXTextField(_vframe1,15,this,ID_ASYID);
                tfId->setText(prName);
                tfId->setJustify(JUSTIFY_CENTER_X);
                tfId->setSelection(0,tfId->getText().length());
                tfId->setFocus();
        FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe0,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
            new FXLabel(_vframe2,oLanguage->getText("str_asymdi_title"));
            tfTitle=new FXTextField(_vframe2,0,this,ID_ASYTITLE,LAYOUT_FILL_X|TEXTFIELD_NORMAL);
                tfTitle->setText(oLanguage->getText("str_asymdi_asytitle"));
        FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe0,0,0,0,0,0,0,0,0,0);
            new FXLabel(_vframe3,oLanguage->getText("str_asymdi_filtera"));
            tfFilterA=new FXTextField(_vframe3,10,this,ID_ASYFILTERA,FRAME_SUNKEN|FRAME_THICK|TEXTFIELD_INTEGER);
                tfFilterA->setText("410");
                tfFilterA->setJustify(JUSTIFY_CENTER_X);
                tfFilterA->setBackColor(FXRGB(245,245,255));
                tfFilterA->setTextColor(FXRGB(255,0,0));
        FXVerticalFrame *_vframe4=new FXVerticalFrame(_hframe0,0,0,0,0,0,0,0,0,0);
            new FXLabel(_vframe4,oLanguage->getText("str_asymdi_filterb"));
            tfFilterB=new FXTextField(_vframe4,10,this,ID_ASYFILTERB,FRAME_SUNKEN|FRAME_THICK|TEXTFIELD_INTEGER);
                tfFilterB->setText("0");
                tfFilterB->setJustify(JUSTIFY_CENTER_X);
                tfFilterB->setBackColor(FXRGB(245,245,255));
                tfFilterB->setTextColor(FXRGB(255,0,0));

    int ch=0;
    sFormulaRatioObject *res=cFormulaRatio::listFormulaRatio();
    //new FXHorizontalSeparator(_vframe0,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT,0,0,3,3);
    FXGroupBox *_gbox0=new FXGroupBox(_vframe0,oLanguage->getText("str_asymdi_calculus"),GROUPBOX_TITLE_CENTER|LAYOUT_FILL_X|FRAME_GROOVE);
    _gbox0->setTextColor(FXRGB(150,150,150));
        FXHorizontalFrame *_hframe1=new FXHorizontalFrame(_gbox0,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
            FXVerticalFrame *_vframe5=new FXVerticalFrame(_hframe1,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                new FXLabel(_vframe5,oLanguage->getText("str_asymdi_cal"));
                lxCalculus=new FXListBox(_vframe5,this,ID_ASYCAL,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK);
                lxCalculus->setNumVisible(cFormulaRatio::getCount());
                ch=0;
                for(int i=0;i<cFormulaRatio::getCount();i++)
                {
                    if(res[i].flags&FORMULA_DEFAULT)
                        ch=i;
                    if(res[i].flags&FORMULA_PRICAL)
                        lxCalculus->appendItem(FXStringFormat("%s  =  %s",res[i].id->text(),res[i].title->text()));
                }
                lxCalculus->setCurrentItem(ch);

            FXMatrix *_matrix1=new FXMatrix(_hframe1,1,MATRIX_BY_COLUMNS|LAYOUT_RIGHT,0,0,0,0,0,0,0,0);
                new FXLabel(_matrix1,oLanguage->getText("str_asymdi_unit"),NULL,LAYOUT_CENTER_X);
                tfUnit=new FXTextField(_matrix1,10,this,ID_ASYUNIT,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK);
                tfUnit->setJustify(JUSTIFY_CENTER_X);
                tfUnit->setText("ppm");
                tfUnit->setBackColor(FXRGB(235,255,230));

    FXHorizontalFrame *_hframe2=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,5,0);
            FXHorizontalFrame *_hframe6=new FXHorizontalFrame(_hframe2,0,0,0,0,0,0,0,0,0);
                FXVerticalFrame *_vframe51=new FXVerticalFrame(_hframe6,0,0,0,0,0,0,0,0,0);
                    new FXLabel(_vframe51,oLanguage->getText("str_asymdi_ctlc"));
                    spControls=new FXSpinner(_vframe51,10,this,ID_ASYCOUNT,SPIN_NORMAL|LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK);
                    spControls->setValue(ASSAY_CTLCOUNT);
                    spControls->setRange(3, ASSAY_CTLCOUNT);
                    new FXHorizontalSeparator(_vframe51,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT,0,0,3,3);
                    cbSC=new FXCheckButton(_vframe51,oLanguage->getText("str_asymdi_ctlsct"),this,ID_ASYSC,CHECKBUTTON_NORMAL,0,0,0,0,0);
                    cbSC->setJustify(JUSTIFY_LEFT|JUSTIFY_TOP);
                    cbSC->setTextColor(FXRGB(160,90,40));
                FXVerticalFrame *_vframe52=new FXVerticalFrame(_hframe6,0,0,0,0,0,0,0,0,0);
                new FXLabel(_vframe52,oLanguage->getText("str_asymdi_ctl"));
                FXHorizontalFrame *_hframe61=new FXHorizontalFrame(_vframe52,FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
                tbBins=new cColorTable(_hframe61,this,ID_ASYBINS,TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT|LAYOUT_FIX_WIDTH,0,0,202);
                tbBins->setTableSize(spControls->getValue(),2);
                tbBins->setVisibleRows(6);
                for(int i=0;i<spControls->getValue();i++)
                {
                    tbBins->setRowText(i,FXStringFormat("Std%d",i+1));
                    tbBins->getRowHeader()->setItemJustify(i,JUSTIFY_CENTER_X);
                    tbBins->setItemText(i,0,"0");
                    tbBins->setItemJustify(i,0,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                    tbBins->setItemText(i,1,"0");
                    tbBins->setItemJustify(i,1,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                }
                tbBins->setRowHeaderWidth(47);
                tbBins->setColumnText(0,oLanguage->getText("str_asymdi_ctlcr"));
                tbBins->setColumnText(1,oLanguage->getText("str_asymdi_ctlb"));
                tbBins->getColumnHeader()->setItemJustify(0,JUSTIFY_CENTER_X);
                tbBins->getColumnHeader()->setItemJustify(1,JUSTIFY_CENTER_X);
                tbBins->setColumnWidth(0,95);
                tbBins->setColumnWidth(1,60);
                tbBins->setCellEditable(0,0,false);
                tbBins->setItemStipple(0,0,STIPPLE_GRAY);
                tbBins->setStippleColor(FXRGB(230,230,200));
                tbBins->showVertGrid(false);
                tbBins->setScrollStyle(HSCROLLING_OFF|HSCROLLER_NEVER|VSCROLLING_OFF|VSCROLLER_NEVER|SCROLLERS_TRACK);
                tbBins->setSelectable(false);
        FXVerticalFrame *_vframe10=new FXVerticalFrame(_hframe2,LAYOUT_RIGHT|LAYOUT_FILL,0,0,0,0,0,0,0,0);
            new FXLabel(_vframe10,oLanguage->getText("str_asymdi_comments"));
            FXVerticalFrame *_vframe8=new FXVerticalFrame(_vframe10,LAYOUT_RIGHT|LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
            comments=new FXText(_vframe8,this,ID_ASYCOMMENTS,TEXT_WORDWRAP|TEXT_NO_TABS|LAYOUT_FILL);

    FXHorizontalFrame *_hframe8=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,10,0);
        //new FXButton(_hframe8,oLanguage->getText("str_asymdi_print"),new FXGIFIcon(getApp(),data_settings_printer),this,CMD_PRINT,FRAME_RAISED|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH,0,0,100,0);
        new FXButton(_hframe8,oLanguage->getText("str_but_close"),NULL,this,CMD_CLOSE,FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_FIX_WIDTH|LAYOUT_RIGHT,0,0,100,0);
        new FXButton(_hframe8,oLanguage->getText("str_asymdi_save"),NULL,this,CMD_SAVE,FRAME_RAISED|BUTTON_DEFAULT|BUTTON_INITIAL|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_RIGHT|LAYOUT_FIX_WIDTH,0,0,100,0);

    free(res);
}

cMDIAssay::~cMDIAssay()
{
}

void cMDIAssay::create()
{
    cMDIChild::create();
    show();
    updateData();
}

FXbool cMDIAssay::canClose(void)
{
    tbBins->acceptInput(true);
    if(!saved)
    {
        setFocus();
        int res=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),oLanguage->getText("str_qsave").text());
        switch(res)
        {
            case MBOX_CLICKED_YES:
                if(!saveData())
                    return false;
                break;
            case MBOX_CLICKED_CANCEL:
                return false;
            default:
                break;
        }
    }
    return true;
}

FXbool cMDIAssay::loadAssay(const FXString &prId)
{
    if(!cAssayManager::assayExists(prId))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_openasy").text());
        return false;
    }
    if(!oWinMain->isWindow(prId))
        return oWinMain->raiseWindow(prId);

    cDataResult *res=cAssayManager::getAssay(prId);
    if(res==NULL)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_openasy").text());
        return false;
    }

    name=res->getCellString(0,0);
    tfId->setText(name);
    tfTitle->setText(res->getCellString(0,1));
    tfFilterA->setText(res->getCellString(0,3));
    tfFilterB->setText(res->getCellString(0,4));
    comments->setText(res->getCellString(0,9));

    FXString controls_defs=res->getCellString(0,5);
    int i=0;
    tbBins->removeRows(0,tbBins->getNumRows());

    i = FXIntVal(controls_defs.before('\n'));
    controls_defs=controls_defs.after('\n');
    cbSC->setCheck(i);

    i = 0;
    while(!controls_defs.empty())
    {
        tbBins->insertRows(i, 1, false);
        tbBins->setRowText(i,FXStringFormat("Std%d",i+1));
        tbBins->getRowHeader()->setItemJustify(i,JUSTIFY_CENTER_X);
        tbBins->setItemText(i,0,controls_defs.before('\t'));
        controls_defs=controls_defs.after('\t');
        tbBins->setItemJustify(i,0,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
        tbBins->setItemText(i,1,controls_defs.before('\n'));
        controls_defs=controls_defs.after('\n');
        tbBins->setItemJustify(i,1,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
        i++;
    }
    tbBins->setColumnText(0,oLanguage->getText("str_asymdi_ctlcr"));
    tbBins->setColumnText(1,oLanguage->getText("str_asymdi_ctlb"));
    tbBins->getColumnHeader()->setItemJustify(0,JUSTIFY_CENTER_X);
    tbBins->getColumnHeader()->setItemJustify(1,JUSTIFY_CENTER_X);
    tbBins->setColumnWidth(0,95);
    tbBins->setColumnWidth(1,60);
    tbBins->setCellEditable(0,0,false);
    tbBins->setItemStipple(0,0,STIPPLE_GRAY);
    tbBins->setStippleColor(FXRGB(230,230,200));
    spControls->setValue(i);

    FXString calculation=res->getCellString(0,6);
    int p=lxCalculus->findItem(calculation+"  =  ",-1,SEARCH_FORWARD|SEARCH_WRAP|SEARCH_PREFIX);
    if(p!=-1)
        lxCalculus->setCurrentItem(p);

    tfUnit->setText(res->getCellString(0,7));

    setTitle(name);
    updateData();
    saved=true;
    return true;
}

long cMDIAssay::onCmdTfId(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    if(!tfId->getText().empty())
    {
        if(!cAssayManager::assayExists(tfId->getText()))
            return 1;
        else
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
    }
    tfId->setText(getTitle());
    return 1;
}

long cMDIAssay::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(canClose())
    {
        close();
        return 1;
    }
    return 0;
}

long cMDIAssay::onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    return 1;
}

long cMDIAssay::onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData)
{
    saveData();
    if(saved)
        onCmdClose(NULL,0,NULL);
    return 1;
}

long cMDIAssay::onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData)
{


    // TODO


    return 1;
}
