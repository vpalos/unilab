#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cWinMain.h"
#include "cMDIReaderManager.h"

cMDIReaderManager *oMDIReaderManager=NULL;

FXDEFMAP(cMDIReaderManager) mapMDIReaderManager[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIReaderManager::ID_READERMANAGER,cMDIReaderManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIReaderManager::CMD_BUT_CLOSE,cMDIReaderManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIReaderManager::CMD_BUT_DEFAULT,cMDIReaderManager::onCmdDefault),
    FXMAPFUNC(SEL_COMMAND,cMDIReaderManager::ID_READER,cMDIReaderManager::onCmdDefault),
    FXMAPFUNC(SEL_COMMAND,cMDIReaderManager::ID_BAUD,cMDIReaderManager::onCmdApply),
    FXMAPFUNC(SEL_COMMAND,cMDIReaderManager::ID_DATA,cMDIReaderManager::onCmdApply),
    FXMAPFUNC(SEL_COMMAND,cMDIReaderManager::ID_PARITY,cMDIReaderManager::onCmdApply),
    FXMAPFUNC(SEL_COMMAND,cMDIReaderManager::ID_STOP,cMDIReaderManager::onCmdApply),
    FXMAPFUNC(SEL_COMMAND,cMDIReaderManager::ID_PORT,cMDIReaderManager::onCmdApply),
};

FXIMPLEMENT(cMDIReaderManager,cMDIChild,mapMDIReaderManager,ARRAYNUMBER(mapMDIReaderManager))

cMDIReaderManager::cMDIReaderManager()
{
}

cMDIReaderManager::cMDIReaderManager(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH) 
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_READERMANAGER);
    client=prP;
    popup=prPup;
    oMDIReaderManager=this;
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL);

    new FXLabel(_vframe0,oLanguage->getText("str_rdrman_port"));
    FXHorizontalFrame *_hframe4=new FXHorizontalFrame(_vframe0,FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
    portList=new FXListBox(_hframe4,this,ID_PORT,LIST_SINGLESELECT|LAYOUT_FILL_X);
    portList->setNumVisible(4);
    
    new FXLabel(_vframe0,oLanguage->getText("str_rdrman_readers"),NULL,0,0,0,0,0,0,0,10);
    FXHorizontalFrame *_hframe3a=new FXHorizontalFrame(_vframe0,FRAME_RAISED|LAYOUT_FILL_X);
    _hframe3a->setBackColor(FXRGB(200,200,202));
    FXHorizontalFrame *_hframe3=new FXHorizontalFrame(_hframe3a,FRAME_SUNKEN|LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
    rdrList=new FXListBox(_hframe3,this,ID_READER,LIST_SINGLESELECT|LAYOUT_FILL_X);
    rdrList->setTextColor(FXRGB(0,0,255));
    rdrList->setFocus();
    FXHorizontalFrame *_hframe1=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,10,0);
        FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe1,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
            new FXLabel(_vframe2,oLanguage->getText("str_rdrman_baud"));
            FXHorizontalFrame *_hframe5=new FXHorizontalFrame(_vframe2,FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
            baudList=new FXListBox(_hframe5,this,ID_BAUD,LIST_SINGLESELECT|LAYOUT_FILL_X);
            baudList->setNumVisible(15);
            new FXLabel(_vframe2,oLanguage->getText("str_rdrman_parity"));
            FXHorizontalFrame *_hframe6=new FXHorizontalFrame(_vframe2,FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
            parityList=new FXListBox(_hframe6,this,ID_PARITY,LIST_SINGLESELECT|LAYOUT_FILL_X);
            parityList->setNumVisible(3);
        FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe1,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
            new FXLabel(_vframe3,oLanguage->getText("str_rdrman_data"));
            FXHorizontalFrame *_hframe7=new FXHorizontalFrame(_vframe3,FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
            dataList=new FXListBox(_hframe7,this,ID_DATA,LIST_SINGLESELECT|LAYOUT_FILL_X);
            dataList->setNumVisible(4);
            new FXLabel(_vframe3,oLanguage->getText("str_rdrman_stop"));
            FXHorizontalFrame *_hframe8=new FXHorizontalFrame(_vframe3,FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
            stopList=new FXListBox(_hframe8,this,ID_STOP,LIST_SINGLESELECT|LAYOUT_FILL_X);
            stopList->setNumVisible(2);

    FXHorizontalFrame *_hframe2=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,10,0);
        custom=new FXLabel(_hframe2,"",NULL,LAYOUT_LEFT);
        custom->setTextColor(FXRGB(255,0,0));
        new FXButton(_hframe2,oLanguage->getText("str_rdrman_done"),NULL,this,CMD_BUT_CLOSE,BUTTON_INITIAL|BUTTON_DEFAULT|LAYOUT_RIGHT|FRAME_RAISED);
        new FXVerticalSeparator(_hframe2,LAYOUT_FILL_Y);
        new FXButton(_hframe2,oLanguage->getText("str_rdrman_default"),NULL,this,CMD_BUT_DEFAULT,LAYOUT_RIGHT|FRAME_RAISED);        

#ifdef WIN32
    portList->appendItem("COM1");
    portList->appendItem("COM2");
    portList->appendItem("COM3");
    portList->appendItem("COM4");
#else
    portList->appendItem("/dev/ttyS0");
    portList->appendItem("/dev/ttyS1");
    portList->appendItem("/dev/ttyS2");
    portList->appendItem("/dev/ttyS3");
#endif

    objList=cReadersManager::getReadersList();
    for(int i=0;i<cReadersManager::getReadersCount();i++)
        rdrList->appendItem(*(objList[i].label));

    rdrList->setNumVisible(rdrList->getNumItems()>15?15:rdrList->getNumItems());
    
    baudList->appendItem("600 bps");
    baudList->appendItem("1200 bps");
    baudList->appendItem("1800 bps");
    baudList->appendItem("2400 bps");
    baudList->appendItem("4800 bps");
    baudList->appendItem("9600 bps");
    baudList->appendItem("19200 bps");
    baudList->appendItem("38400 bps");
    baudList->appendItem("57600 bps");
    baudList->appendItem("115200 bps");

    parityList->appendItem("None");
    parityList->appendItem("Odd");
    parityList->appendItem("Even");

    dataList->appendItem("5 bits");
    dataList->appendItem("6 bits");
    dataList->appendItem("7 bits");
    dataList->appendItem("8 bits");

    stopList->appendItem("1 bit");
    stopList->appendItem("2 bits");
}

cMDIReaderManager::~cMDIReaderManager()
{
    cMDIReaderManager::unload();
}

void cMDIReaderManager::create()
{
    cMDIChild::create();
    show();
}

void cMDIReaderManager::load(FXMDIClient *prP,FXPopup *prMenu)
{
    if(oMDIReaderManager!=NULL)
    {
        oMDIReaderManager->setFocus();
        prP->setActiveChild(oMDIReaderManager);
        if(oMDIReaderManager->isMinimized())
            oMDIReaderManager->restore();
        return;
    }
    new cMDIReaderManager(prP,oLanguage->getText("str_rdrman_title"),new FXGIFIcon(oApplicationManager,data_settings_readers),prMenu,MDI_NORMAL|MDI_TRACKING,10,10,400,290);
    oMDIReaderManager->create();
    if(oApplicationManager->reg().existingEntry("readercomm","port"))
    {
        int pos;
        
        pos=oMDIReaderManager->portList->findItem(oApplicationManager->reg().readStringEntry("readercomm","port","---"));
        oMDIReaderManager->portList->setCurrentItem(pos<0?0:pos);
        
        pos=oMDIReaderManager->rdrList->findItem(oApplicationManager->reg().readStringEntry("readercomm","reader","---"));
        oMDIReaderManager->rdrList->setCurrentItem(pos<0?0:pos);

        pos=oMDIReaderManager->parityList->findItem(oApplicationManager->reg().readStringEntry("readercomm","parity","---"));
        oMDIReaderManager->parityList->setCurrentItem(pos<0?0:pos);

        pos=oMDIReaderManager->baudList->findItem(oApplicationManager->reg().readStringEntry("readercomm","baud","---"));
        oMDIReaderManager->baudList->setCurrentItem(pos<0?0:pos);

        pos=oMDIReaderManager->dataList->findItem(oApplicationManager->reg().readStringEntry("readercomm","data","---"));
        oMDIReaderManager->dataList->setCurrentItem(pos<0?0:pos);

        pos=oMDIReaderManager->stopList->findItem(oApplicationManager->reg().readStringEntry("readercomm","stop","---"));
        oMDIReaderManager->stopList->setCurrentItem(pos<0?0:pos);
        oMDIReaderManager->update();
    }
    else
    {
        oMDIReaderManager->onCmdDefault(NULL,0,NULL);
    }
    oMDIReaderManager->setFocus();
}

FXbool cMDIReaderManager::isLoaded(void)
{
    return (oMDIReaderManager!=NULL);
}

void cMDIReaderManager::unload(void)
{
    oMDIReaderManager=NULL;
}

void cMDIReaderManager::update(void)
{
    int j,i=rdrList->getCurrentItem(),p,d,s,b;
    
    j=0;
    switch(objList[i].settings.parity)
    {
        case PARITYNONE:
            j=0;
            break;
        case PARITYODD:
            j=1;
            break;
        case PARITYEVEN:
            j=2;
            break;
    }
    p=j;
    j=0;
    switch(objList[i].settings.stopBits)
    {
        case STOPBITS1:
            j=0;
            break;
        case STOPBITS2:
            j=1;
            break;
    }
    s=j;
    j=0;
    switch(objList[i].settings.bits)
    {
        case BITS5:
            j=0;
            break;
        case BITS6:
            j=1;
            break;
        case BITS7:
            j=2;
            break;
        case BITS8:
            j=3;
            break;
    }
    d=j;
    j=0;
    switch(objList[i].settings.baudRate)
    {
        case BAUD600:
            j=0;
            break;
        case BAUD1200:
            j=1;
            break;
        case BAUD1800:
            j=2;
            break;
        case BAUD2400:
            j=3;
            break;
        case BAUD4800:
            j=4;
            break;
        case BAUD9600:
            j=5;
            break;
        case BAUD19200:
            j=6;
            break;
        case BAUD38400:
            j=7;
            break;
        case BAUD57600:
            j=8;
            break;
        case BAUD115200:
            j=9;
            break;
    }
    b=j;
    if(p!=parityList->getCurrentItem() ||
       b!=baudList->getCurrentItem() ||
       d!=dataList->getCurrentItem() ||
       s!=stopList->getCurrentItem())
        custom->setText(oLanguage->getText("str_rdrman_custom"));
    else
        custom->setText("");
}

long cMDIReaderManager::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    onCmdApply(NULL,0,NULL);
    cMDIReaderManager::unload();
    close();
    return 1;
}

long cMDIReaderManager::onCmdDefault(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int j,i=rdrList->getCurrentItem();
    
    j=0;
    switch(objList[i].settings.parity)
    {
        case PARITYNONE:
            j=0;
            break;
        case PARITYODD:
            j=1;
            break;
        case PARITYEVEN:
            j=2;
            break;
    }
    parityList->setCurrentItem(j);
    j=0;
    switch(objList[i].settings.stopBits)
    {
        case STOPBITS1:
            j=0;
            break;
        case STOPBITS2:
            j=1;
            break;
    }
    stopList->setCurrentItem(j);
    j=0;
    switch(objList[i].settings.bits)
    {
        case BITS5:
            j=0;
            break;
        case BITS6:
            j=1;
            break;
        case BITS7:
            j=2;
            break;
        case BITS8:
            j=3;
            break;
    }
    dataList->setCurrentItem(j);
    j=0;
    switch(objList[i].settings.baudRate)
    {
        case BAUD600:
            j=0;
            break;
        case BAUD1200:
            j=1;
            break;
        case BAUD1800:
            j=2;
            break;
        case BAUD2400:
            j=3;
            break;
        case BAUD4800:
            j=4;
            break;
        case BAUD9600:
            j=5;
            break;
        case BAUD19200:
            j=6;
            break;
        case BAUD38400:
            j=7;
            break;
        case BAUD57600:
            j=8;
            break;
        case BAUD115200:
            j=9;
            break;
    }
    baudList->setCurrentItem(j);
    onCmdApply(NULL,0,NULL);
    return 1;
}

long cMDIReaderManager::onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData)
{
    update();
    oApplicationManager->reg().writeStringEntry("readercomm","port",portList->getItemText(portList->getCurrentItem()).text());
    oApplicationManager->reg().writeStringEntry("readercomm","parity",parityList->getItemText(parityList->getCurrentItem()).text());
    oApplicationManager->reg().writeStringEntry("readercomm","baud",baudList->getItemText(baudList->getCurrentItem()).text());
    oApplicationManager->reg().writeStringEntry("readercomm","data",dataList->getItemText(dataList->getCurrentItem()).text());
    oApplicationManager->reg().writeStringEntry("readercomm","stop",stopList->getItemText(stopList->getCurrentItem()).text());
    oApplicationManager->reg().writeStringEntry("readercomm","reader",rdrList->getItemText(rdrList->getCurrentItem()).text());
    oApplicationManager->reg().write();
    return 1;
}


