#include "cMDIChild.h"
#include "cWinMain.h"
#include <stdlib.h>
#define BORDERWIDTH 3
#define MDI_CASCADE_COUNT 10
#define MDI_CASCADE_SPACE 15

static int mdicnt=0;

FXDEFMAP(cMDIChild) mapMDIChild[]=
{
    FXMAPFUNC(SEL_PAINT,0,cMDIChild::onPaint),
    FXMAPFUNC(SEL_COMMAND,FXWindow::ID_MDI_CLOSE,cMDIChild::onClose),
    FXMAPFUNC(SEL_CLOSE,cWinMain::ID_MDI_CHILD,cMDIChild::onClose),
    FXMAPFUNC(SEL_CONFIGURE,0,cMDIChild::onUpdWindow),
};

FXIMPLEMENT(cMDIChild,FXMDIChild,mapMDIChild,ARRAYNUMBER(mapMDIChild))

cMDIChild::cMDIChild()
{
}

cMDIChild::cMDIChild(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH) 
    : FXMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    mdiClient=prP;
    mdiMenu=prPup;
    winWidth=prW;
    winHeight=prH;
    setTracking(false);
    oWinMain->addWindow(prName,this);
    this->setShadowColor(FXRGB(80,80,80));
    mdicnt++;
}

cMDIChild::~cMDIChild()
{
    oWinMain->removeWindow(this->getTitle());
}

void cMDIChild::create()
{
    FXMDIChild::create();
    move((mdicnt%MDI_CASCADE_COUNT)*MDI_CASCADE_SPACE,(mdicnt%MDI_CASCADE_COUNT)*MDI_CASCADE_SPACE);
}

FXbool cMDIChild::canClose(void)
{
    return true;
}

long cMDIChild::onPaint(FXObject*,FXSelector,void* ptr)
{
    FXEvent *ev=(FXEvent*)ptr;
    FXint xx,yy,th,titlespace,letters,dots,dotspace;
    FXint fh,mh,bh,bw,mw;
    
    // If box is shown, hide it temporarily
    if(mode&DRAG_INVERTED)
        drawRubberBox(newx,newy,neww,newh);
    {
        FXDCWindow dc(this,ev);
        
        // Draw MDIChild background
        dc.setForeground(baseColor);
        dc.fillRectangle(ev->rect.x,ev->rect.y,ev->rect.w,ev->rect.h);
        
        // Only draw stuff when not maximized
        if(!(options&MDI_MAXIMIZED))
        {
            // Compute sizes
            fh=font->getFontHeight();
            mw=windowbtn->getDefaultWidth();
            mh=windowbtn->getDefaultHeight();
            bw=deletebtn->getDefaultWidth();
            bh=deletebtn->getDefaultHeight();
            th=FXMAX3(fh,mh,bh)+(options&MDI_MINIMIZED?3:4);
            
            // Draw outer border
            
            dc.setForeground(hiliteColor);
            dc.fillRectangle(0,0,width-1,1);
            dc.fillRectangle(0,0,1,height-1);
            dc.setForeground(FXRGB(100,100,100));
            dc.fillRectangle(0,height-1,width,1);
            dc.fillRectangle(width-1,0,1,height);
            
            // Draw title background
            dc.setForeground(isActive() ? (hasFocus() ? titleBackColor : shadowColor) : backColor);
            dc.fillRectangle(BORDERWIDTH,BORDERWIDTH,width-BORDERWIDTH*2,th);
            
            // Draw title
            if(!title.empty())
            {
                xx=BORDERWIDTH+mw+2+4;
                yy=BORDERWIDTH+font->getFontAscent()+(th-fh)/2;
                
                // Compute space for title
                titlespace=width-mw-3*bw-(BORDERWIDTH<<1)-2-4-4-6-2;
                
                dots=0;
                letters=title.length();
                
                // Title too large for space
                if(font->getTextWidth(title.text(),letters)>titlespace)\
                {
                    dotspace=titlespace-font->getTextWidth("...",3);
                    while(letters>0 && font->getTextWidth(title.text(),letters)>dotspace)
                        letters--;
                    dots=3;
                    if(letters==0)
                    {
                        letters=1;
                        dots=0;
                    }
                }
                
                // Draw as much of the title as possible
                dc.setForeground(isActive() ? titleColor : borderColor);
                dc.setFont(font);
                dc.drawText(xx,yy,title.text(),letters);
                dc.drawText(xx+font->getTextWidth(title.text(),letters),yy,"...",dots);
            }
        }
    }
    if(mode&DRAG_INVERTED)
        drawRubberBox(newx,newy,neww,newh);
    
    return 1;
    }

long cMDIChild::onClose(FXObject *prSender,FXSelector,void* ptr)
{
    if(!canClose())
        return 0;
    if((prSender!=NULL) && (prSender==this))
        return 0;
    close();
    return 1;
}

long cMDIChild::onUpdWindow(FXObject *prSender,FXSelector,void* ptr)
{
    FXint w=getWidth(),h=getHeight();
    if((w<winWidth || h<winHeight) && !(options&MDI_MINIMIZED) && !(options&MDI_MAXIMIZED))
        resize(w<winWidth?winWidth:w,h<winHeight?winHeight:h);
    return 1;
}

