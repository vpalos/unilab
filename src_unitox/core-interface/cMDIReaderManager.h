#ifndef CMDIREADERMANAGER_H
#define CMDIREADERMANAGER_H

#include <fx.h>

#include "cSortList.h" 
#include "cMDIChild.h"
#include "cReadersManager.h"

class cMDIReaderManager : public cMDIChild
{
    FXDECLARE(cMDIReaderManager);
    
    private:
        FXMDIClient *client; 
        FXPopup *popup;
        FXLabel *custom;
        FXListBox *rdrList;
        FXListBox *portList;
        FXListBox *baudList;
        FXListBox *dataList;
        FXListBox *parityList;
        FXListBox *stopList;
        sReadersObject *objList;
        
        void update(void);
    protected:
        cMDIReaderManager();
        
    public:
        cMDIReaderManager(FXMDIClient *prP, const FXString &prName, FXIcon *prIc=NULL, FXPopup *prPup=NULL, FXuint prOpts=0, FXint prX=0, FXint prY=0, FXint prW=0, FXint prH=0);
        virtual ~cMDIReaderManager();
        
        virtual void create();
        
        static void load(FXMDIClient *prP,FXPopup *prMenu);
        static FXbool isLoaded(void);
        static void unload(void);
        
        enum
        {
            ID_READERMANAGER=FXMDIChild::ID_LAST,
            
            ID_READER,
            ID_BAUD,
            ID_PARITY,
            ID_DATA,
            ID_STOP,
            ID_PORT,
            
            CMD_BUT_CLOSE,
            CMD_BUT_DEFAULT,
            
            ID_LAST
        };

        long onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdDefault(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif

