#ifndef CPRINTDIALOG_H
#define CPRINTDIALOG_H

#include <fx.h>

class cPrintDialog : public FXPrintDialog
{
    FXDECLARE(cPrintDialog);
    
    private:
    protected:
        cPrintDialog();
    
    public:
    
        cPrintDialog(FXWindow *owner, const FXString &title, FXuint opts=0, FXint x=0, FXint y=0, FXint w=0, FXint h=0);
        
        long onUpdate(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
