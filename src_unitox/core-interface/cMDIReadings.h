#ifndef CMDIREADINGS_H
#define CMDIREADINGS_H

#include <fx.h>
#include "ee.h"
#include "cDataResult.h"
#include "cMDIChild.h"
#include "cColorTable.h"
#include "cSortList.h"
#include "cMDITemplate.h"
#include "cVChart.h"

typedef struct sCDLGO
{
    FXString *id;
    FXString *id2;
    FXString *asy;
    cDataResult *res;
};

typedef struct sCaseEntry
{
    FXString *old_id;
    FXString *id;
    FXString *reading_oid;
    FXint replicates;
    FXint factor;
    FXint startCell;
    FXint startPlate;
    FXint plateSpanCount;
    double *data;

    double *controlsData;
    FXint controlsCount;

    FXString *tpl;
    FXString *controls_defs;
    FXString *readingDate;
    FXString *technician;
    FXString *lot;
    FXString *expirationDate;
    FXString *commodity;
    FXString *comments;
    FXString *assay;

    int parentSet;
};

typedef struct sCasesSet
{
    sCaseEntry *cases;
    int casesCount;
    int platesCount;

    cDataResult *assayRes;
    cDataResult *templateRes;

    FXString *cal;

    FXString *unit;
    FXString *assay;
    FXString *assaytitle;
    FXint controls_replicates;

    int controlsLayout[50];
    int controlsPerPlate;

    bool isSc;
    FXdouble asyControls[50];
    int asyControlsBBO[50];
    int asyControlsCount;
};

class cLocInfo
{
    private:
    protected:
    public:
        int caseIndex;
        int sampleLoc;
        int plate;
        bool isValue;
        double value;
        int set;

        cLocInfo(int prCaseIndex=-1,int prSampleLoc=-1,int prPlate=-1,int prSet=-1);
        ~cLocInfo();

        void setValue(double prValue);
};

class cMDIReadings : public cMDIChild
{
    FXDECLARE(cMDIReadings);

    private:
    protected:
        FXbool saved,newdata;

        FXTabBook *tbMain;
        FXTabItem *tiPrimary;
        FXTabItem *tiStats;
        FXTabItem *tiGraph;
        FXButton *lbInv;
        FXButton *butSelall,*butDesel;

        cColorTable *plateTable1;
        cColorTable *caseList1;
        FXGroupBox *_gb0;
        FXLabel *lbPlates;
        FXLabel *assayId;
        FXLabel *assayTpl;
        FXLabel *assayTech;

        FXMenuPane *edit;
        FXMenuButton *mbEdit;

        cColorTable *plateTable2;
        cColorTable *caseList2;
        FXGroupBox *_gb10;
        FXLabel *lbPlates2;
        FXCheckButton *showStats;

        FXButton *mbReports;

        FXString invalidPlatesDetails;
        FXScrollWindow *chartWin;
        FXVerticalFrame *chartFrame;
        cVChart **charts;

        sCasesSet *casesSets;
        int readingsWindow;
        int selectedCase;
        int tSetsCount;
        int tCasesCount;
        int tPlatesCount;

        cMDIReadings();

        void drawPlateTables(void);
        bool plate1HasValue(int prIndex);
        void locateCaseLists(int prIndex);
        void locatePlateTable1(int prIndex);
        void locatePlateTable2(int prIndex);
        void selectCase(int prIndex,int prPlate=-1);

        void updateData(void);
        FXbool saveData(void);

    public:
        static void sortControls(int *prData,int prLength,int prDesc=false);

        static FXString sampleKey(int prIndex);
        static FXString sampleVal(double prValue);
        static FXString calculVal(double prValue);
        static FXString calculVal1(double prValue);
        static FXString displayPosition(int prPosition);
        static int platePosition(int prPosition);
        static bool isControlPosition(int prPosition,int *prControls,int prControlsCount,bool prAlelisa);
        static void appendControls(double *prDest,double *prSrc,int *prControls,int prControlsCount,bool prAlelisa);

        cMDIReadings(FXMDIClient *prP,const FXString &prName,FXIcon *prIc=NULL,FXPopup *prPup=NULL,FXuint prOpts=0,FXint prX=0,FXint prY=0,FXint prW=0,FXint prH=0);
        virtual ~cMDIReadings();

        virtual void create();

        virtual FXbool canClose(void);
        virtual FXbool newReading(cDataResult *prResAssay,cDataResult *prResTemplate,int prPlateCount,double **prPlateData,FXString prLot="",FXString prExpirationDate="");
        virtual FXbool loadReadings(cSortList &prCases);

        enum
        {
            ID_MDIREADINGS=FXMDIChild::ID_LAST,
            ID_TBMAIN,

            ID_COPY,
            ID_PASTE,
            ID_SELALL,

            ID_PLATETABLE1,
            ID_CASELIST1,
            ID_PLATETABLE2,
            ID_CASELIST2,
            ID_STATSTABLE,
            ID_SHOWSTATS,
            ID_SHOWRULES,

            ID_REP_CA,

            CMD_TEMPLATE,
            CMD_REREAD,
            CMD_CASEEDIT,
            CMD_CLOSE,
            CMD_SAVE,

            ID_LAST
        };

        long onCopyPlateTable(FXObject *prSender,FXSelector prSelector,void *prData);
        long onPastePlateTable(FXObject *prSender,FXSelector prSelector,void *prData);
        long onSelallPlateTable(FXObject *prSender,FXSelector prSelector,void *prData);

        long onRszPlateTable1(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdSelPlateTable1(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdPlateTable1(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdSelPlateTable2(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdPlateTable2(FXObject *prSender,FXSelector prSelector,void *prData);

        long onRszCaseList1(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSelCaseList1(FXObject *prSender,FXSelector prSelector,void *prData);
        long onRszCaseList2(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSelCaseList2(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdCaseList1(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdCaseList2(FXObject *prSender,FXSelector prSelector,void *prData);

        long onCmdCalcSel(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData);

        long onCmdShowRules(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdReread(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdTemplate(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdReport(FXObject *prSender,FXSelector prSelector,void *prData);

        long onEvtRedir(FXObject *prSender,FXSelector prSelector,void *prData);
        long onRefresh(FXObject *prSender,FXSelector prSelector,void *prData);
        long onRefreshAsy(FXObject *prSender,FXSelector prSelector,void *prData);
        long onRefreshTpl(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
