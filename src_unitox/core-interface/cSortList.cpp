#include "cSortList.h"

FXDEFMAP(cSortList) mapSortList[]=
{
    FXMAPFUNC(SEL_COMMAND,cSortList::CMD_SORT,cSortList::onCmdSort),
};

FXIMPLEMENT(cSortList,FXIconList,mapSortList,ARRAYNUMBER(mapSortList))

cSortList::cSortList()
{
}

cSortList::cSortList(FXComposite *p,FXObject *tgt,FXSelector sel,FXuint opts,FXint x,FXint y,FXint w,FXint h)
    : FXIconList(p,tgt,sel,opts,x,y,w,h)
{
    sortColumn=0;
    sortDirection=FALSE;
    this->getHeader()->setTarget(this);
    this->getHeader()->setSelector(CMD_SORT);
    this->setSortFunc(FXIconList::ascendingCase);
    this->setScrollStyle(HSCROLLING_ON|VSCROLLING_ON|VSCROLLER_ALWAYS);
}

cSortList::~cSortList()
{
}

void cSortList::create()
{
    FXIconList::create();
    this->getHeader()->setArrowDir(sortColumn,sortDirection);
}

void cSortList::sortItems(void)
{
    register FXIconItem *v,*c=0;
    register FXbool exch=FALSE;
    register FXint i,j,h;
    FXIconItem *resA,*resB;
    if(sortfunc)
    {
        if(0<=current)
            c=items[current];
        h=1;j=1;
        while(h)
        {
            h=0;
            for(i=0;i<items.no()-j;i++)
            {
                resA=new FXIconItem(items[i]->getText().after('\t',sortColumn));
                resB=new FXIconItem(items[i+1]->getText().after('\t',sortColumn));
                if(sortfunc(resA,resB)>0)
                {
                    v=items[i];
                    items[i]=items[i+1];
                    items[i+1]=v;
                    h=1;
                    exch=true;
                }
                delete resA;
                delete resB;
            }
            j++;
        }
        if(0<=current)
        {
            for(i=0; i<items.no(); i++)
            {
                if(items[i]==c)
                {
                    current=i;
                    break;
                }
            }
        }
        if(exch) recalc();
    }
}

long cSortList::onCmdSort(FXObject *prSender,FXSelector prSelector,void *prPtr)
{
    int i,res=this->getHeader()->getArrowDir((FXint)prPtr);
    sortColumn=(FXint)prPtr;
    if(res==MAYBE)
    {
        for(i=0;i<this->getHeader()->getNumItems();i++)
            this->getHeader()->setArrowDir(i,MAYBE);
        this->getHeader()->setArrowDir((FXint)prPtr,FALSE);
        this->setSortFunc(FXIconList::ascendingCase);
    }
    else
    {
        this->getHeader()->setArrowDir((FXint)prPtr,res==FALSE?TRUE:FALSE);
        this->setSortFunc(res==FALSE?FXIconList::descendingCase:FXIconList::ascendingCase);
    }
    this->sortItems();
    return 1;
}
