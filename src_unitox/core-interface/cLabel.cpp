#include "cLabel.h"

FXDEFMAP(cLabel) mapLabel[]=
{
    FXMAPFUNC(SEL_PAINT,0,cLabel::onPaint),
};

FXIMPLEMENT(cLabel,FXLabel,mapLabel,ARRAYNUMBER(mapLabel))

cLabel::cLabel()
{
}

cLabel::cLabel(FXComposite *p, const FXString &text, FXIcon *ic, FXuint opts, FXint x, FXint y, FXint w, FXint h, FXint pl, FXint pr, FXint pt, FXint pb):
    FXLabel(p,text,ic,opts,x,y,w,h,pl,pr,pt,pb)
{
}

cLabel::~cLabel()
{
}

long cLabel::onPaint(FXObject *prSender,FXSelector prSelector,void *prData)
{
  FXDC *dc;
  
  if(prSender==NULL && prSelector==0)
      dc=(FXDC*)prData;
  else
  {
      FXEvent *ev=(FXEvent*)prData;
      dc=new FXDCWindow(this,ev);
  }
  dc->clearClipRectangle();
  FXint tw=0,th=0,iw=0,ih=0,tx,ty,ix,iy;
  dc->setForeground(backColor);
  dc->fillRectangle(0,0,width,height);
  if(!label.empty()){
    tw=labelWidth(label);
    th=labelHeight(label);
    }
  if(icon){
    iw=icon->getWidth();
    ih=icon->getHeight();
    }
  just_x(tx,ix,tw,iw);
  just_y(ty,iy,th,ih);
  if(icon){
    if(isEnabled())
      dc->drawIcon(icon,ix,iy);
    else
      dc->drawIconSunken(icon,ix,iy);
    }
  if(!label.empty()){
    dc->setFont(font);
    if(isEnabled()){
      dc->setForeground(textColor);
      drawLabel(*(FXDCWindow*)dc,label,hotoff,tx,ty,tw,th);
      }
    else{
      dc->setForeground(hiliteColor);
      drawLabel(*(FXDCWindow*)dc,label,hotoff,tx+1,ty+1,tw,th);
      dc->setForeground(shadowColor);
      drawLabel(*(FXDCWindow*)dc,label,hotoff,tx,ty,tw,th);
      }
    }
  drawFrame(*(FXDCWindow*)dc,0,0,width,height);
  if(dc  && (prSender!=NULL || prSelector!=0))
    delete dc;
  return 1;
}


