#include "engine.h"
#include "graphics.h"
#include "cLanguage.h"
#include "cDLGReportCA.h"

FXDEFMAP(cDLGReportCA) mapDLGReportCA[]=
{
    FXMAPFUNC(SEL_UPDATE,cDLGReportCA::ID_UPDATE,cDLGReportCA::onCmdUpdate),
    FXMAPFUNC(SEL_UPDATE,cDLGReportCA::CMD_BUT_APPLY,cDLGReportCA::onUpdApply),
    FXMAPFUNC(SEL_COMMAND,cDLGReportCA::CMD_BUT_APPLY,cDLGReportCA::onCmdApply),
};

FXIMPLEMENT(cDLGReportCA,FXDialogBox,mapDLGReportCA,ARRAYNUMBER(mapDLGReportCA))

cDLGReportCA::cDLGReportCA(FXWindow *prOwner) :
    FXDialogBox(prOwner,oLanguage->getText("str_report_ca")+"...",DECOR_TITLE|DECOR_BORDER)
{
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL);
        bUseInfo=new FXCheckButton(_vframe0,oLanguage->getText("str_report_showInfo"),this,ID_UPDATE);
        bUseInfo->setTextColor(FXRGB(0,0,200));
        bUseInfo->setCheck(true);
        new FXHorizontalSeparator(_vframe0);
        bUseGraphs=new FXCheckButton(_vframe0,oLanguage->getText("str_report_showGraph"),this,ID_UPDATE);
        bUseGraphs->setCheck(true);
        bUseGraphs->setTextColor(FXRGB(0,0,200));
        new FXHorizontalSeparator(_vframe0);
        bUseData=new FXCheckButton(_vframe0,oLanguage->getText("str_report_showData"),this,ID_UPDATE);
        bUseData->setTextColor(FXRGB(0,0,200));
        bUseData->setCheck(true);
        new FXHorizontalSeparator(_vframe0);

        FXHorizontalFrame *_hframe100=new FXHorizontalFrame(_vframe0,LAYOUT_CENTER_X);
        butCancel=new FXButton(_hframe100,oLanguage->getText("str_but_cancel"),NULL,this,ID_CANCEL,BUTTON_DEFAULT|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
        butAccept=new FXButton(_hframe100,oLanguage->getText("str_but_ok"),NULL,this,CMD_BUT_APPLY,BUTTON_DEFAULT|BUTTON_INITIAL|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
}

cDLGReportCA::~cDLGReportCA()
{
}

long cDLGReportCA::onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData)
{
    handle(NULL,FXSEL(SEL_COMMAND,ID_ACCEPT),NULL);
    return 1;
}

long cDLGReportCA::onUpdApply(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(bUseGraphs->getCheck() || bUseInfo->getCheck() || bUseData->getCheck())
        butAccept->enable();
    else
        butAccept->disable();
    return 1;
}

long cDLGReportCA::onCmdUpdate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    return 1;
}
