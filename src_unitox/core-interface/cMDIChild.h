#ifndef CMDICHILD_H
#define CMDICHILD_H

#include <fx.h>

class cMDIChild : public FXMDIChild
{
    FXDECLARE(cMDIChild);
    
    private:
        FXint winWidth,winHeight;
    
    protected:
        FXMDIClient *mdiClient;
        FXPopup *mdiMenu;

        cMDIChild();
    
    public:
        cMDIChild(FXMDIClient *prP, const FXString &prName, FXIcon *prIc=NULL, FXPopup *prPup=NULL, FXuint prOpts=0, FXint prX=0, FXint prY=0, FXint prW=0, FXint prH=0);
        virtual ~cMDIChild();
        
        virtual void create();
        
        virtual FXbool canClose(void);
        
        long onPaint(FXObject*,FXSelector,void* ptr);
        long onClose(FXObject*,FXSelector,void* ptr);
        long onUpdWindow(FXObject*,FXSelector,void* ptr);
};

#endif

