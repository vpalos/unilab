#include <time.h>
#include <ctype.h>
#include <stdlib.h>

#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cDateChooser.h"
#include "cColorsManager.h"
#include "cTemplateManager.h"
#include "cCaseManager.h"
#include "cReadersManager.h"
#include "cReadingsManager.h"
#include "cWinMain.h"
#include "cDLGChooseAT.h"
#include "cMDITemplate.h"
#include "cMDITemplateManager.h"

FXDEFMAP(cMDITemplate) mapMDITemplate[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDITemplate::ID_MDITEMPLATE,cMDITemplate::onCmdClose),
    FXMAPFUNC(SEL_CONFIGURE,cMDITemplate::ID_PLATETABLE,cMDITemplate::onRszPlateTable),
    FXMAPFUNC(SEL_SELECTED,cMDITemplate::ID_PLATETABLE,cMDITemplate::onUpdSelPlateTable),
    FXMAPFUNC(SEL_UPDATE,cMDITemplate::ID_PLATETABLE,cMDITemplate::onUpdPlateTable),
    FXMAPFUNC(SEL_CLICKED,cMDITemplate::ID_PLATETABLE,cMDITemplate::onDbcPlateTable),
    FXMAPFUNC(SEL_CONFIGURE,cMDITemplate::ID_CASELIST,cMDITemplate::onRszCaseList),
    FXMAPFUNC(SEL_UPDATE,cMDITemplate::ID_CASELIST,cMDITemplate::onUpdCaseList),
    FXMAPFUNC(SEL_SELECTED,cMDITemplate::ID_CASELIST,cMDITemplate::onUpdSelCaseList),
    FXMAPFUNC(SEL_REPLACED,cMDITemplate::ID_CASELIST,cMDITemplate::onCmdCaseList),
    FXMAPFUNC(SEL_CHANGED,cMDITemplate::ID_TPLNAME,cMDITemplate::onChgTfName),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::ID_TPLNAME,cMDITemplate::onCmdTfName),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::ID_CONTROLSTGT,cMDITemplate::onCmdUpdateControls),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::ID_ALELISATGT,cMDITemplate::onCmdUpdateAlelisa),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::ID_SUBCTGT,cMDITemplate::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_CLOSE,cMDITemplate::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_SAVE,cMDITemplate::onCmdSave),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_READ,cMDITemplate::onCmdRead),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_FILL,cMDITemplate::onCmdFill),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_PRINT,cMDITemplate::onCmdPrint),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_CASENEW,cMDITemplate::onCmdCaseNew),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_CASEDEL,cMDITemplate::onCmdCaseDel),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_CASEUP,cMDITemplate::onCmdCaseUp),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_CASEDN,cMDITemplate::onCmdCaseDown),
};

FXIMPLEMENT(cMDITemplate,cMDIChild,mapMDITemplate,ARRAYNUMBER(mapMDITemplate))

cMDITemplate::cMDITemplate()
{
}

FXString cMDITemplate::sampleKey(int prIndex)
{
    FXString ret=FXStringFormat("%d",1 + prIndex);
    return ret;
}

int cMDITemplate::platePositionRow(int prIndex)
{
    return prIndex%8;
}

int cMDITemplate::platePositionColumn(int prIndex)
{
    return prIndex/8;
}

int cMDITemplate::platePosition(int prRow,int prColumn)
{
    return ((prColumn==-1)?prRow:prColumn*8+(prRow%8));
}

bool cMDITemplate::isControl(int prRow,int prColumn)
{
    int i,pos=platePosition(prRow,prColumn);
    int pos2=pos - (((pos%96) && (alelisaChoice == 2))?1:0);
    if(pos==0)
        pos=1000;
    if(pos2==0)
        pos2=1000;
    for(i=0;i<ASSAY_CTLCOUNT;i++)
        if(controls[i]==pos || controls[i]==pos2)
            return true;
    return false;
}

void cMDITemplate::placeControl(int prRow,int prColumn)
{
    if(controlsCount==ASSAY_CTLCOUNT)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_maxtplcontrols").text());
        return;
    }
    int i,pos=platePosition(prRow,prColumn);
    clearControl(pos);
    for(i=0;i<ASSAY_CTLCOUNT;i++)
        if(controls[i]==-100)
        {
            controls[i]=(pos==0?1000:pos);
            controlsCount++;
            break;
        }
}

void cMDITemplate::clearControl(int prRow,int prColumn)
{
    int i,pos=platePosition(prRow,prColumn);
    if(pos==0)
        pos=1000;
    for(i=0;i<ASSAY_CTLCOUNT;i++)
        if(controls[i]==pos)
        {
            for(; i < controlsCount - 1; i++)
                controls[i] = controls[i + 1];
            controls[--controlsCount]=-100;
            return;
        }
}

void cMDITemplate::clearAllControls(void)
{
    for(int i=0;i<ASSAY_CTLCOUNT;i++)
        controls[i]=-100;
    controlsCount=0;
}

void cMDITemplate::drawCaseList(void)
{
    for(int j=0;j<caseList->getNumRows();j++)
    {
        caseList->setCellColor(j,0,cColorsManager::getColor(j,-30));
        for(int i=1;i<caseList->getNumColumns();i++)
        {
            caseList->setItemBorders(j,i,0);
            caseList->setCellColor(j,i,cColorsManager::getColor(j));
        }
    }
}

void cMDITemplate::updateData(void)
{
    saved=false;
    plateTable->showHorzGrid(false);
    plateTable->showVertGrid(true);

    if(controlsChoice==3)
    {
        spStd->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        butNeg->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        butNone->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    else
    {
        spStd->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        butNeg->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        butNone->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }

    int r,c,rr,cc,v,i,j;
    int counter,stock=0,plates;
    for(i=0;i<caseList->getNumRows();i++)
        stock+=1+FXIntVal(caseList->getItemText(i,3));
    counter=controlsCount;
    if(alelisaChoice==2)
    {
        counter*=2;
        stock*=2;
    }
    plates=stock/(96-counter)+(stock%(96-counter)>0?1:0);
    if(plates==0)
        plates=1;
    lbPlates->setText(FXStringFormat(" - %d %s [%d %s]",plates,oLanguage->getText("str_tplmdi_plates").text(),(96-counter)-((stock-1)%(96-counter)+1),oLanguage->getText("str_tplmdi_cellsrem").text()));

    spStd->setText(FXStringFormat("%s%d", oLanguage->getText("str_ctl").text(), controlsCount + (controlsCount==ASSAY_CTLCOUNT?0:1)));
    if(controlsCount!=ASSAY_CTLCOUNT && controlsChoice==3) {
        spStd->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        butNeg->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    else {
        spStd->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        butNeg->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }

    plateTable->removeRows(0,plateTable->getNumRows());
    plateTable->insertRows(0,plates*8);
    for(i=0;i<plateTable->getNumRows();i++)
        for(j=0;j<12;j++)
            plateTable->setItemJustify(i,j,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
    for(i=1;i<plateTable->getNumRows();i++)
        for(j=0;j<12;j++)
        {
            plateTable->setItemText(i,j,(char*)NULL);
            plateTable->setCellColor(i,j,FXRGB(255,255,255));
            plateTable->setItemStipple(i,j,STIPPLE_NONE);
            plateTable->setItemBorders(i,j,i%8==0?FXTableItem::TBORDER:0);
        }
    int row=plateTable->getNumRows()-1;
    for(j=0;j<12;j++)
        plateTable->setItemBorders(row,j,FXTableItem::BBORDER);
    for(j=0;j<plateTable->getNumRows();j++)
    {
        plateTable->getRowHeader()->setItemJustify(j,JUSTIFY_CENTER_X);
        plateTable->getRowHeader()->setItemText(j,FXStringFormat("P%d:%c",j/8+1,j%8+'A'));
    }
    for(i=0;i<plateTable->getNumRows();i++)
        plateTable->getRowHeader()->setItemPressed(i,false);
    for(i=0;i<12;i++)
        plateTable->getColumnHeader()->setItemPressed(i,false);
    FXbool alel=false;

    alel=alelisaChoice==2;

    int m,mm;
    for(i=0;i<ASSAY_CTLCOUNT;i++)
        if(controls[i]!=-100)
        {
            v=controls[i]%1000;
            r=platePositionRow(v);
            c=platePositionColumn(v);
            rr=platePositionRow(v + 1);
            cc=platePositionColumn(v + 1);
            for(j=0;j<plates;j++)
            {
                plateTable->setCellColor(r+8*j,c,FXRGB(255,90,90));
                plateTable->setItemText(r+8*j,c,FXStringFormat("%s%d", oLanguage->getText("str_ctl").text(), (i+1)));
                m=r+8*j;
                plateTable->setItemBorders(m,c,FXTableItem::LBORDER|(((c<11 && isControl(m,c+1)))?0:FXTableItem::RBORDER)|FXTableItem::TBORDER|(((m%8!=7 && !isControl(m+1,c) && !alel) || m==row)?FXTableItem::BBORDER:0));
                if(alel)
                {
                    plateTable->setItemStipple(rr+8*j,cc,STIPPLE_GRAY);
                    plateTable->setCellColor(rr+8*j,cc,FXRGB(255,90,90));
                    plateTable->setItemText(rr+8*j,cc,FXStringFormat("%s%d'", oLanguage->getText("str_ctl").text(), (i+1)));
                    mm=rr+8*j;
                    plateTable->setItemBorders(mm,cc,FXTableItem::LBORDER|((cc<11 && isControl(mm,cc+1))?0:FXTableItem::RBORDER)|FXTableItem::TBORDER|(((mm%8!=7 && !isControl(mm+1,cc)) || mm==row)?FXTableItem::BBORDER:0));
                }
            }
        }
    counter=0;
    int p;
    char replicates[10];
    for(i=0;i<caseList->getNumRows();i++)
    {
        v=0;
        if(caseList->getItemText(i,1).empty())
            continue;
        p=0;
        stock=1;
        while(stock>0)
        {
            replicates[0]=0;
            for(j=0;j<FXIntVal(caseList->getItemText(i,3))+1;j++)
            {
                r=platePositionRow(counter%96);
                c=platePositionColumn(counter%96);
                while(!plateTable->getItemText(r+8*(counter/96),c).empty())
                {
                    counter++;
                    r=platePositionRow(counter%96);
                    c=platePositionColumn(counter%96);
                }
                rr=platePositionRow((counter+1)%96);
                cc=platePositionColumn((counter+1)%96);
                if(j==0 && v==0)
                    caseList->setItemText(i,0,FXStringFormat("(%s) P%d:%c%d",sampleKey(i).text(),(counter/96)+1,'A'+r,c+1));
                plateTable->setCellColor(r+8*(counter/96),c,cColorsManager::getColor(i,-j*10));
                plateTable->setItemText(r+8*(counter/96),c,FXStringFormat("%s%s",sampleKey(i).text(),replicates));
                v=1;
                counter++;
                replicates[j]='\'';
                replicates[j+1]=0;
            }
            stock--;
            p++;
        }
    }
    onRszPlateTable(NULL,0,NULL);
    onRszCaseList(NULL,0,NULL);
}

FXbool cMDITemplate::saveData(void)
{
    caseList->acceptInput(true);
    plateTable->acceptInput(true);
    if(saved)
        return true;

    if(caseList->getNumRows()==0)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_nocase").text());
        return false;
    }

    FXbool noctl=false;
    if(controlsCount<=0)
        noctl=true;

    if(noctl)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_nocontrols").text());
        return false;
    }

    FXString oldname=name;
    name=tfName->getText();
    if(name!=oldname && cTemplateManager::templateExists(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
        tfName->setText(cTemplateManager::getNewID());
        setTitle(tfName->getText());
        updateData();
        return false;
    }
    this->setTitle(name);

    FXString controls_defs=FXStringVal(controlsChoice)+"\n";
    FXString cases_defs=FXStringVal(caseList->getNumRows())+"\n";

    for(int i=0;i<ASSAY_CTLCOUNT;i++)
        if(controls[i]!=-100)
            controls_defs=controls_defs+FXStringFormat("%d\t",controls[i]);
    controls_defs=controls_defs+"-100";

    for(int i=0;i<caseList->getNumRows();i++)
    {
        if(caseList->getItemText(i,1).empty())
            continue;
        for(int j=1;j<caseList->getNumColumns();j++)
            cases_defs=cases_defs+caseList->getItemText(i,j)+"\t";
        cases_defs=cases_defs+"\n";
    }
    if(oldname.empty())
        cTemplateManager::addTemplate(name,lbDate->getText(),alelisaChoice,controls_defs,cases_defs);
    else
        cTemplateManager::setTemplate(oldname,name,lbDate->getText(),alelisaChoice,controls_defs,cases_defs);
    saved=true;
    cMDITemplateManager::cmdRefresh();
    return true;
}

cMDITemplate::cMDITemplate(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH)
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH),
    controlsTgt(controlsChoice,this,ID_CONTROLSTGT),
    subcTgt(subcChoice,this,ID_SUBCTGT),
    alelisaTgt(alelisaChoice,this,ID_ALELISATGT)
{
    setSelector(ID_MDITEMPLATE);
    saved=false;
    name="";
    clearAllControls();
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL);

        FXHorizontalFrame *_hframe0=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
            FXMatrix *_matrix0=new FXMatrix(_hframe0,3,MATRIX_BY_ROWS|PACK_UNIFORM_HEIGHT|LAYOUT_FILL_X,0,0,0,0,0,0,7,7);
            new FXLabel(_matrix0,oLanguage->getText("str_tplmdi_name"));
            new FXLabel(_matrix0,oLanguage->getText("str_tplmdi_date"));
            new FXLabel(_matrix0,oLanguage->getText("str_tplmdi_platel"));
            tfName=new FXTextField(_matrix0,15,this,ID_TPLNAME);
            tfName->setText(prName);
            tfName->setSelection(0,tfName->getText().length());
            tfName->setFocus();
            lbDate=new FXLabel(_matrix0,"");
            lbDate->setTextColor(FXRGB(0,130,0));
                FXchar ftime[100];
                time_t curtime;
                struct tm *loctime;
                curtime=time(NULL);
                loctime=localtime(&curtime);
                strftime(ftime,100,"%Y-%m-%d",loctime);
                lbDate->setText(ftime);
            lbPlates=new FXLabel(_matrix0,oLanguage->getText("str_tplmdi_platel"));

            alelisaChoice=1;
            controlsChoice=1;
            oldControlsChoice=0;
            subcChoice=1;
            controls[0]=-340;
            FXGroupBox *_group1=new FXGroupBox(_hframe0,oLanguage->getText("str_tplmdi_controls"),GROUPBOX_TITLE_LEFT|FRAME_GROOVE|LAYOUT_RIGHT);
                FXMatrix *_matrix1=new FXMatrix(_group1,2,MATRIX_BY_ROWS|PACK_UNIFORM_HEIGHT,0,0,0,0,0,0,0,0);
                new FXRadioButton(_matrix1,oLanguage->getText("str_tplmdi_c5a"),&controlsTgt,FXDataTarget::ID_OPTION+1);
                new FXRadioButton(_matrix1,oLanguage->getText("str_tplmdi_c5d"),&controlsTgt,FXDataTarget::ID_OPTION+2);
                new FXRadioButton(_matrix1,oLanguage->getText("str_tplmdi_ccustom"),&controlsTgt,FXDataTarget::ID_OPTION+3);
                FXHorizontalFrame *_hframe11=new FXHorizontalFrame(_matrix1,0,0,0,0,0,0,0,0,0);
                butNeg=new FXRadioButton(_hframe11,oLanguage->getText("str_tplmdi_cstd"),&subcTgt,FXDataTarget::ID_OPTION+1,LAYOUT_CENTER_Y|RADIOBUTTON_NORMAL,0,0,0,0,DEFAULT_PAD,0);
                spStd=new FXLabel(_hframe11,FXStringFormat("%s%d", oLanguage->getText("str_ctl").text(), 1),NULL,LAYOUT_CENTER_Y, 0,0,0,0,0);
                spStd->setTextColor(FXRGB(230,50,00));
                //spStd=new FXSpinner(_hframe11,5,this,ID_STDS,SPIN_NORMAL|FRAME_SUNKEN|FRAME_THICK|LAYOUT_CENTER_Y);
                //spStd->setValue(1);
                //spStd->setRange(1, ASSAY_CTLCOUNT);
                butNone=new FXRadioButton(_hframe11,oLanguage->getText("str_tplmdi_cnone"),&subcTgt,FXDataTarget::ID_OPTION+2,LAYOUT_CENTER_Y|RADIOBUTTON_NORMAL);
                FXRadioButton *l = new FXRadioButton(_matrix1,oLanguage->getText("str_tplmdi_alelisa1"),&alelisaTgt,FXDataTarget::ID_OPTION+1);
                l->setTextColor(FXRGB(50,180,0));
                l = new FXRadioButton(_matrix1,oLanguage->getText("str_tplmdi_alelisa2"),&alelisaTgt,FXDataTarget::ID_OPTION+2);
                l->setTextColor(FXRGB(50,180,0));

        FXHorizontalFrame *_hframe1=new FXHorizontalFrame(_vframe0,LAYOUT_FILL,0,0,0,0,0,0,0,0);
        FXVerticalFrame *_vframe1=new FXVerticalFrame(_hframe1,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
            plateTable=new cColorTable(_vframe1,this,ID_PLATETABLE,TABLE_READONLY|TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT|LAYOUT_FILL,0,0,0,181);
            plateTable->setGridColor(FXRGB(120,120,120));
            plateTable->setDefColumnWidth(0);
            plateTable->setTableSize(8,12);
            plateTable->setSelTextColor(0);
            plateTable->setCellBorderWidth(2);
            plateTable->setBorderColor(FXRGB(120,120,120));
            plateTable->setStippleColor(FXRGB(250,250,240));
            plateTable->setScrollStyle(HSCROLLING_OFF|HSCROLLER_NEVER|VSCROLLING_ON|VSCROLLER_ALWAYS|SCROLLERS_TRACK);
            for(int j=0;j<12;j++) {
                plateTable->getColumnHeader()->setItemJustify(j,JUSTIFY_CENTER_X);
                plateTable->getColumnHeader()->setItemText(j,FXStringFormat("%d",j+1));
            }
        FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe1,LAYOUT_RIGHT|LAYOUT_FILL_Y,0,0,0,0,0,0,0,0,0,0);
            new FXButton(_vframe2,oLanguage->getText("str_tplmdi_save"),new FXGIFIcon(getApp(),data_new_template),this,CMD_SAVE,FRAME_RAISED|BUTTON_DEFAULT|BUTTON_INITIAL|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH,0,0,100,0);
            new FXHorizontalSeparator(_vframe2,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT,0,0,0,10);
            new FXButton(_vframe2,oLanguage->getText("str_tplmdi_read"),new FXGIFIcon(getApp(),data_start_reading),this,CMD_READ,FRAME_RAISED|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH,0,0,100,0);
            new FXButton(_vframe2,oLanguage->getText("str_tplmdi_fill"),NULL,this,CMD_FILL,FRAME_RAISED|LAYOUT_FILL_X);
            new FXHorizontalSeparator(_vframe2,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT,0,0,0,10);
            new FXButton(_vframe2,oLanguage->getText("str_but_close"),NULL,this,CMD_CLOSE,FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,100,0);
            new FXHorizontalSeparator(_vframe2,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT|LAYOUT_BOTTOM,0,0,0,10);

        FXHorizontalFrame *_hframe91=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
        FXGroupBox *_group3=new FXGroupBox(_hframe91,oLanguage->getText("str_tplmdi_createsp"),GROUPBOX_TITLE_LEFT|FRAME_GROOVE|LAYOUT_FILL_X);
            FXHorizontalFrame *_hframe9=new FXHorizontalFrame(_group3,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
                new FXLabel(_hframe9,oLanguage->getText("str_tplmdi_lcreate") + " ", NULL, LAYOUT_CENTER_Y);
                spSps=new FXSpinner(_hframe9,5,this,ID_NCNT,SPIN_NORMAL|FRAME_SUNKEN|FRAME_THICK);
                spSps->setValue(1);
                spSps->setRange(1, 100);
                new FXLabel(_hframe9," " + oLanguage->getText("str_tplmdi_lsamples") + " ", NULL, LAYOUT_CENTER_Y);
                cbNCom=new FXComboBox(_hframe9,12,this,ID_NCOM,FRAME_SUNKEN|FRAME_THICK);
                FXString s, coms = oLanguage->getText("str_tplmdi_coms");
                cbNCom->appendItem("---");
                while(!coms.empty()) {
                    s = coms.before('\t');
                    cbNCom->appendItem(s);
                    coms = coms.after('\t');
                }
                cbNCom->setNumVisible(5);
                new FXLabel(_hframe9," " + oLanguage->getText("str_tplmdi_las") + " ", NULL, LAYOUT_CENTER_Y);
                lxNRep=new FXListBox(_hframe9,this,ID_NREP,FRAME_SUNKEN|FRAME_THICK);
                lxNRep->appendItem(oLanguage->getText("str_tplmdi_alelisa1"));
                lxNRep->appendItem(oLanguage->getText("str_tplmdi_alelisa2"));
                new FXLabel(_hframe9," ...", NULL, LAYOUT_CENTER_Y);
        new FXButton(_hframe91,oLanguage->getText("str_tplmdi_casenew"),NULL,this,CMD_CASENEW,FRAME_RAISED|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH|LAYOUT_FIX_HEIGHT|LAYOUT_BOTTOM,0,0,100,43);

        caselb=new FXLabel(_vframe0,oLanguage->getText("str_tplmdi_casesl"));
        FXHorizontalFrame *_hframe2=new FXHorizontalFrame(_vframe0,LAYOUT_FILL,0,0,0,0,0,0,0,0);
        FXHorizontalFrame *_hframe3=new FXHorizontalFrame(_hframe2,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
        caseList=new cColorTable(_hframe3,this,ID_CASELIST,TABLE_NO_COLSELECT|LAYOUT_FILL);
        caseList->setTipText(oLanguage->getText("str_tplmdi_casetip"));
        caseList->setTableSize(0,5);
        caseList->setRowHeaderWidth(0);
        caseList->setColumnText(0,oLanguage->getText("str_tplmdi_caseloc"));
        caseList->setColumnWidth(0,70);
        caseList->setColumnText(1,oLanguage->getText("str_tplmdi_casenam"));
        caseList->setColumnWidth(1,283);
        caseList->setColumnText(2,oLanguage->getText("str_tplmdi_casecom"));
        caseList->setColumnWidth(2,108);
        caseList->setColumnText(3,oLanguage->getText("str_tplmdi_caserep"));
        caseList->setColumnWidth(3,40);
        caseList->setColumnText(4,oLanguage->getText("str_tplmdi_casefct"));
        caseList->setColumnWidth(4,34);
        caseList->getColumnHeader()->setItemJustify(0,JUSTIFY_CENTER_X);
        caseList->getColumnHeader()->setItemJustify(3,JUSTIFY_CENTER_X);
        caseList->getColumnHeader()->setItemJustify(4,JUSTIFY_CENTER_X);
        caseList->setGridColor(FXRGB(255,255,255));
        caseList->setCellBorderWidth(1);
        caseList->setColumnHeaderHeight(32);
        caseList->showHorzGrid(true);
        caseList->showVertGrid(false);
        caseList->setSelTextColor(0);
        caseList->setCellBorderColor(FXRGB(255,0,0));
        caseList->setSelBackColor(FXRGB(180,180,180));
        caseList->setScrollStyle(HSCROLLING_ON|VSCROLLER_ALWAYS|VSCROLLING_ON);
        FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe2,LAYOUT_FILL_Y|LAYOUT_RIGHT,0,0,0,0,0,0,0,0,0,0);
            new FXButton(_vframe3,oLanguage->getText("str_tplmdi_casedel"),NULL,this,CMD_CASEDEL,FRAME_RAISED|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH,0,0,100,0);
            new FXArrowButton(_vframe3,this,CMD_CASEDN,ARROW_DOWN|ARROW_REPEAT|FRAME_RAISED|LAYOUT_FIX_WIDTH|LAYOUT_FIX_HEIGHT|LAYOUT_BOTTOM,0,0,100,20);
            new FXArrowButton(_vframe3,this,CMD_CASEUP,ARROW_UP|ARROW_REPEAT|FRAME_RAISED|LAYOUT_FIX_WIDTH|LAYOUT_FIX_HEIGHT|LAYOUT_BOTTOM,0,0,100,20);
}

cMDITemplate::~cMDITemplate()
{
    getApp()->removeTimeout(this,EVT_LBDATE);
}

void cMDITemplate::create()
{
    cMDIChild::create();
    show();
    onCmdUpdateControls(NULL,0,NULL);
    updateData();
}

FXbool cMDITemplate::canClose(void)
{
    caseList->acceptInput(true);
    plateTable->acceptInput(true);
    if(!saved)
    {
        setFocus();
        int res=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),oLanguage->getText("str_qsave").text());
        switch(res)
        {
            case MBOX_CLICKED_YES:
                if(!saveData())
                    return false;
                break;
            case MBOX_CLICKED_CANCEL:
                return false;
            default:
                break;
        }
    }
    return true;
}

FXbool cMDITemplate::loadTemplate(const FXString &prTitle)
{
    if(!cTemplateManager::templateExists(prTitle))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_opentpl").text());
        return false;
    }
    cDataResult *res=cTemplateManager::getTemplate(prTitle);
    if(res==NULL)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_opentpl").text());
        return false;
    }
    name=res->getCellString(0,0);
    lbDate->setText(res->getCellString(0,1));
    alelisaChoice=res->getCellInt(0,2);
    FXString controls_defs=res->getCellString(0,3);
    FXString cases_defs=res->getCellString(0,4);
    setTitle(name);
    oldControlsChoice=controlsChoice=FXIntVal(controls_defs.before('\n'));
    clearAllControls();
    int k;
    switch(controlsChoice)
    {
    case 3:
        controls_defs=controls_defs.after('\n');
        controlsCount=0;
        k=0;
        do{
            k=FXIntVal(controls_defs.before('\t'));
            controls_defs=controls_defs.after('\t');
            if(k!=0 && k!=-100)
                controls[controlsCount++]=k;
        }while(k!=0 && k!=-100);
        break;
    case 2:
        placeControl(alelisaChoice==1?4:8);
        placeControl(alelisaChoice==1?3:6);
        placeControl(alelisaChoice==1?2:4);
        placeControl(alelisaChoice==1?1:2);
        placeControl(alelisaChoice==1?0:0);
        break;
    default:
        placeControl(alelisaChoice==1?0:0);
        placeControl(alelisaChoice==1?1:2);
        placeControl(alelisaChoice==1?2:4);
        placeControl(alelisaChoice==1?3:6);
        placeControl(alelisaChoice==1?4:8);
        break;
    }

    caseList->removeRows(0,caseList->getNumRows());
    int limita=FXIntVal(cases_defs.before('\n'));
    for(int i=0;i<limita;i++)
    {
        cases_defs=cases_defs.after('\n');
        caseList->insertRows(i);
        caseList->layout();
        for(int j=0;j<caseList->getNumColumns();j++)
        {
            caseList->setItemJustify(i,j,JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
            caseList->setCellColor(i,j,cColorsManager::getColor(i));
        }
        caseList->setCellColor(i,0,cColorsManager::getColor(i,-30));
        caseList->setCellEditable(i,0,false);
        caseList->setItemJustify(i,1,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
        caseList->setItemJustify(i,2,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);

        caseList->setItemText(i,0,"");
        caseList->setItemText(i,1,cases_defs.before('\t'));
        cases_defs=cases_defs.after('\t');
        caseList->setItemText(i,2,cases_defs.before('\t'));
        cases_defs=cases_defs.after('\t');
        caseList->setItemText(i,3,cases_defs.before('\t'));
        cases_defs=cases_defs.after('\t');
        caseList->setItemText(i,4,cases_defs.before('\t'));

    }
    updateData();
    saved=true;
    return true;
}

long cMDITemplate::onRszPlateTable(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int tw=plateTable->getWidth()-plateTable->verticalScrollBar()->getWidth();
    int cw=tw/13;
    int hw=tw-(cw*13)+cw;
    if(cw<30)
        cw=hw=30;
    plateTable->setRowHeaderWidth(hw);
    for(tw=0;tw<12;tw++)
        plateTable->setColumnWidth(tw,cw);
    return 1;
}

long cMDITemplate::onUpdPlateTable(FXObject *prSender,FXSelector prSelector,void *prData)
{
    return 1;
}

long cMDITemplate::onUpdSelPlateTable(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int sr,ssr=plateTable->getSelStartRow(),ser=plateTable->getSelEndRow();
    if(ssr<0 || ser<0)
        return 1;
    sr=ssr;
    if(ssr!=ser)
        sr=(ssr==plateTable->getAnchorRow()?ser:ssr);
    int sc,ssc=plateTable->getSelStartColumn(),sec=plateTable->getSelEndColumn();
    if(ssc<0 || sec<0)
        return 1;
    sc=ssc;

    if(ssc!=sec)
        sc=(ssc==plateTable->getAnchorColumn()?sec:ssc);
    for(int i=0;i<plateTable->getNumRows();i++)
        plateTable->getRowHeader()->setItemPressed(i,false);
    for(int i=0;i<12;i++)
        plateTable->getColumnHeader()->setItemPressed(i,false);
    plateTable->getRowHeader()->setItemPressed(sr,true);
    plateTable->getColumnHeader()->setItemPressed(sc,true);
    plateTable->killSelection();
    return 1;
}

long cMDITemplate::onUpdCaseList(FXObject *prSender,FXSelector prSelector,void *prData)
{
    caselb->setText(oLanguage->getText("str_tplmdi_casesl")+
                    (caseList->getNumRows()>0?FXStringFormat(" - %d %s",
                     caseList->getNumRows(),oLanguage->getText("str_tplmdi_caseslsx").text()):""));
    return 1;
}

long cMDITemplate::onRszCaseList(FXObject *prSender,FXSelector prSelector,void *prData)
{
    //int tw=caseList->getWidth()-caseList->verticalScrollBar()->getWidth();
    //caseList->setColumnWidth(4,tw-490);
    return 1;
}

long cMDITemplate::onUpdSelCaseList(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int sr,ssr=caseList->getSelStartRow(),ser=caseList->getSelEndRow();
    if(ssr<0 || ser<0)
        return 1;
    sr=ssr;
    if(ssr!=ser)
        sr=(ssr==caseList->getAnchorRow()?ser:ssr);
    drawCaseList();
    for(int i=1;i<caseList->getNumColumns();i++)
    {
        caseList->setCellColor(sr,i,cColorsManager::getColor(sr,-30));
        caseList->setItemBorders(sr,i,FXTableItem::TBORDER|FXTableItem::BBORDER);
    }
    caseList->setItemBorders(sr,1,FXTableItem::LBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    caseList->setItemBorders(sr,caseList->getNumColumns()-1,FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    caseList->killSelection();
    return 1;
}

long cMDITemplate::onChgTfName(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    return 1;
}

long cMDITemplate::onCmdTfName(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!tfName->getText().empty())
    {
        if(!cTemplateManager::templateExists(tfName->getText()))
        {
            updateData();
            return 1;
        }
        else
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
    }
    if(name.empty() && cTemplateManager::templateExists(getTitle()))
    {
        tfName->setText(cTemplateManager::getNewID());
        setTitle(tfName->getText());
    }
    else
        tfName->setText(getTitle());
    updateData();
    return 1;
}

long cMDITemplate::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(canClose())
    {
        close();
        return 1;
    }
    return 0;
}

long cMDITemplate::onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    return 1;
}

long cMDITemplate::onCmdUpdateControls(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(controlsChoice==oldControlsChoice)
        return 1;
    if(controls[0]!=-340 && controlsCount)
        if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_resetcontrols").text()))
        {
            controlsChoice=oldControlsChoice;
            return 1;
        }
    oldControlsChoice=controlsChoice;
    switch(controlsChoice)
    {
        case 1:
            clearAllControls();
            placeControl(alelisaChoice==1?0:0);
            placeControl(alelisaChoice==1?1:2);
            placeControl(alelisaChoice==1?2:4);
            placeControl(alelisaChoice==1?3:6);
            placeControl(alelisaChoice==1?4:8);
            break;
        case 2:
            clearAllControls();
            placeControl(alelisaChoice==1?4:8);
            placeControl(alelisaChoice==1?3:6);
            placeControl(alelisaChoice==1?2:4);
            placeControl(alelisaChoice==1?1:2);
            placeControl(alelisaChoice==1?0:0);
            break;
        default:
            clearAllControls();
            break;
    }
    updateData();
    return 1;
}

long cMDITemplate::onCmdUpdateAlelisa(FXObject *prSender,FXSelector prSelector,void *prData)
{
    switch(controlsChoice)
    {
        case 1:
            clearAllControls();
            placeControl(alelisaChoice==1?0:0);
            placeControl(alelisaChoice==1?1:2);
            placeControl(alelisaChoice==1?2:4);
            placeControl(alelisaChoice==1?3:6);
            placeControl(alelisaChoice==1?4:8);
            break;
        case 2:
            clearAllControls();
            placeControl(alelisaChoice==1?4:8);
            placeControl(alelisaChoice==1?3:6);
            placeControl(alelisaChoice==1?2:4);
            placeControl(alelisaChoice==1?1:2);
            placeControl(alelisaChoice==1?0:0);
            break;
        default:
            if(alelisaChoice==2 && controlsCount) {
                if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_resetcontrolsalelisa").text())) {
                    alelisaChoice=1;
                    return 1;
                }
                clearAllControls();
            }
            break;
    }
    updateData();
    return 1;
}

long cMDITemplate::onDbcPlateTable(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(controlsChoice!=3)
        return 1;
    switch(subcChoice)
    {
        case 1:
            if(alelisaChoice==2) {
                if(isControl(plateTable->getCurrentRow(),plateTable->getCurrentColumn()))
                    return 1;
                int r = plateTable->getCurrentRow();
                if(isControl(r + (r==7?-7:1),plateTable->getCurrentColumn() + (r==7?1:0)))
                    return 1;
                if(plateTable->getCurrentRow() == 7 && plateTable->getCurrentColumn()==11)
                    return 1;
            }
            placeControl(plateTable->getCurrentRow(),plateTable->getCurrentColumn());
            break;
        default:
            clearControl(plateTable->getCurrentRow(),plateTable->getCurrentColumn());
            break;
    }
    plateTable->killSelection();
    updateData();
    return 1;
}

long cMDITemplate::onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData)
{
    saveData();
    if(saved)
        onCmdClose(NULL,0,NULL);
    return 1;
}

long cMDITemplate::onCmdRead(FXObject *prSender,FXSelector prSelector,void *prData)
{
    caseList->acceptInput(true);
    plateTable->acceptInput(true);
    if(!saved)
    {
        setFocus();
        int res=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),oLanguage->getText("str_qsave").text());
        switch(res)
        {
            case MBOX_CLICKED_YES:
                if(!saveData())
                    return 1;
                break;
            case MBOX_CLICKED_CANCEL:
                return 1;
            default:
                break;
        }
    }
    cDLGChooseAT dlg(this,CHOOSEAT_ASSAY);
    dlg.setTemplate(name);
    if(!dlg.execute(PLACEMENT_OWNER))
        return 1;
    cReadingsManager::makeReading(mdiClient,mdiMenu,dlg.getAssay(),dlg.getTemplate(),false,dlg.getLot(),dlg.getExpDate());
    return 1;
}

long cMDITemplate::onCmdFill(FXObject *prSender,FXSelector prSelector,void *prData)
{
    caseList->acceptInput(true);
    plateTable->acceptInput(true);
    if(!saved)
    {
        setFocus();
        int res=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),oLanguage->getText("str_qsave").text());
        switch(res)
        {
            case MBOX_CLICKED_YES:
                if(!saveData())
                    return 1;
                break;
            case MBOX_CLICKED_CANCEL:
                return 1;
            default:
                break;
        }
    }
    cDLGChooseAT dlg(this,CHOOSEAT_ASSAY);
    dlg.setTemplate(name);
    if(!dlg.execute(PLACEMENT_OWNER))
        return 1;
    cReadingsManager::makeReading(mdiClient,mdiMenu,dlg.getAssay(),dlg.getTemplate(),true,dlg.getLot(),dlg.getExpDate());
    return 1;
}

long cMDITemplate::onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData)
{


    // TODO


    return 1;
}

long cMDITemplate::onCmdCaseList(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(caseList->getNumRows()<=0)
        return 1;
    int uret,r=*((int*)prData),c=*((int*)prData+1);
    FXString s1,s2,s=caseList->getItemText(r,c),err="";
    switch(c)
    {
        case 1:
            if(s.empty())
            {
                caseList->setItemText(r,c,cCaseManager::getNewID());
                break;
            }
            for(int i=0;i<caseList->getNumRows();i++)
                if(i!=r && caseList->getItemText(i,1)==s)
                {
                    err="str_invalid_duplid";
                    break;
                }
            for(int i=0;i<s.length();i++)
                if(!isalnum(s.at(i)) && s.at(i)!=' ' && s.at(i)!='#' && s.at(i)!='-' && s.at(i)!='/')
                {
                    err="str_invalid_numlet";
                    break;
                }
            break;
        case 2:
            if(s.empty())
            {
                caseList->setItemText(r,c,"---");
                break;
            }
            for(int i=0;i<s.length();i++)
                if(!isalnum(s.at(i)) && s.at(i)!=' ' && s.at(i)!='#' && s.at(i)!='-' && s.at(i)!='/')
                {
                    err="str_invalid_numlet";
                    break;
                }
            break;
        case 3:
            if(s.empty())
            {
                caseList->setItemText(r,c,"0");
                break;
            }
            for(int i=0;i<s.length();i++)
                if(!isdigit(s.at(i)))
                {
                    err="str_invalid_pnum01";
                    break;
                }
            uret=(FXuint)FXIntVal(s);
            if(uret>1)
                uret=1;
            caseList->setItemText(r,c,FXStringFormat("%d",uret));
            break;
        case 4:
            if(s.empty())
            {
                caseList->setItemText(r,c,"1");
                break;
            }
            for(int i=0;i<s.length();i++)
                if(!isdigit(s.at(i)))
                {
                    err="str_invalid_pnum";
                    break;
                }
            if(err.empty() && FXIntVal(s)==0)
                err="str_invalid_nzero";
            uret=(FXuint)FXIntVal(s);
            if(uret>4)
                uret=4;
            caseList->setItemText(r,c,FXStringFormat("%d",uret));
            break;
    }

    if(!err.empty())
    {
        caseList->setItemText(r,c,"");
        caseList->startInput(r,c);
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),
                              (oLanguage->getText("str_invalid")+"\n"+
                               oLanguage->getText(err)).text());
        return 1;
    }

    updateData();
    return 1;
}

long cMDITemplate::onCmdCaseNew(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int allow=true,r,r2;
    caseList->acceptInput(true);
    if(caseList->getNumRows()>=1000)
        return 1;
    for(int i=0;i<caseList->getNumRows();i++)
        if(caseList->getItemText(i,1).empty())
        {
            allow=false;
            caseList->startInput(i,1);
        }
    if(allow==true)
    {
        r=caseList->getNumRows();
        caseList->insertRows(r, spSps->getValue());
        r2=caseList->getNumRows();
        caseList->layout();
        for(; r < r2; r++) {
            for(int i=0;i<caseList->getNumColumns();i++) {
                caseList->setItemJustify(r,i,JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
                caseList->setCellColor(r,i,cColorsManager::getColor(r));
            }
            caseList->setCellColor(r,0,cColorsManager::getColor(r,-30));
            caseList->setCellEditable(r,0,false);
            caseList->setItemJustify(r,1,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
            caseList->setItemJustify(r,2,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
            caseList->setItemText(r,1,cCaseManager::getNewID());
            caseList->setItemText(r,2,cbNCom->getText());
            caseList->setItemText(r,3,lxNRep->getCurrentItem()?"1":"0");
            caseList->setItemText(r,4,"1");
        }
        r = r2 - spSps->getValue();
        caseList->setCurrentItem(r,1);
        caseList->makePositionVisible(r,1);
        caseList->startInput(r,1);
        updateData();
    }
    return 1;
}

long cMDITemplate::onCmdCaseDel(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int sr=caseList->getCurrentRow();
    if(sr==-1)
        return 1;
    if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_erasecs").text()))
        return 1;
    caseList->removeRows(sr,1);
    drawCaseList();
    updateData();
    return 1;
}

long cMDITemplate::onCmdCaseUp(FXObject *prSender,FXSelector prSelector,void *prData)
{
    caseList->acceptInput();
    int er,sr=caseList->getCurrentRow();
    if(sr<1)
        return 1;
    er=sr-1;
    FXString v;
    for(int i=1;i<caseList->getNumColumns();i++)
    {
        v=caseList->getItemText(er,i);
        caseList->setItemText(er,i,caseList->getItemText(sr,i));
        caseList->setItemText(sr,i,v);
    }
    drawCaseList();
    updateData();
    caseList->setCurrentItem(er,caseList->getCurrentColumn());
    caseList->selectRow(er);
    onUpdSelCaseList(NULL,0,NULL);
    return 1;
}

long cMDITemplate::onCmdCaseDown(FXObject *prSender,FXSelector prSelector,void *prData)
{
    caseList->acceptInput();
    int er,sr=caseList->getCurrentRow();
    if(sr<0 || (sr>=caseList->getNumRows()-1))
        return 1;
    er=sr+1;
    FXString v;
    for(int i=1;i<caseList->getNumColumns();i++)
    {
        v=caseList->getItemText(er,i);
        caseList->setItemText(er,i,caseList->getItemText(sr,i));
        caseList->setItemText(sr,i,v);
    }
    drawCaseList();
    updateData();
    caseList->setCurrentItem(er,caseList->getCurrentColumn());
    caseList->selectRow(er);
    onUpdSelCaseList(NULL,0,NULL);
    return 1;
}
