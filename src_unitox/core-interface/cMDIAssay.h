#ifndef CMDIASSAY_H
#define CMDIASSAY_H

#include <fx.h>

#include "ee.h"
#include "cMDIChild.h"
#include "cColorTable.h"

#define ASSAY_CTLCOUNT 6

class cMDIAssay : public cMDIChild
{
    FXDECLARE(cMDIAssay);

    private:
        FXTextField *tfId;
        FXTextField *tfTitle;
        FXTextField *tfFilterA;
        FXTextField *tfFilterB;
        FXCheckButton *cbSC;
        FXSpinner *spControls;
        FXListBox *lxCalculus;
        FXTextField *tfUnit;
        cColorTable *tbBins;
        FXText *comments;

        FXbool saved;
        FXString name;

    protected:
        cMDIAssay();

        FXbool updateData(void);
        FXbool saveData(void);

    public:
        cMDIAssay(FXMDIClient *prP,const FXString &prName,FXIcon *prIc=NULL,FXPopup *prPup=NULL,FXuint prOpts=0,FXint prX=0,FXint prY=0,FXint prW=0,FXint prH=0);
        virtual ~cMDIAssay();

        virtual void create();

        virtual FXbool canClose(void);
        virtual FXbool loadAssay(const FXString &prId);

        enum
        {
            ID_MDIASSAY=FXMDIChild::ID_LAST,
            ID_ASYID,
            ID_ASYTITLE,
            ID_ASYFILTERA,
            ID_ASYFILTERB,
            ID_ASYCAL,
            ID_ASYCOUNT,
            ID_ASYUNIT,
            ID_ASYBINS,
            ID_ASYSC,
            ID_ASYCOMMENTS,

            CMD_SAVE,
            CMD_PRINT,
            CMD_CLOSE,

            ID_LAST
        };

        long onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdTfId(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
