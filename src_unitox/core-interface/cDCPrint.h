#ifndef CDCPRINT_H
#define CDCPRINT_H

#include <fx.h>

class cDCPrint : public FXDCPrint
{
    private:
    protected:
    public:
        cDCPrint(FXApp *a);

        FXbool beginPrint(FXPrinter& job);
        void drawText(FXint x,FXint y,const FXchar* string,FXuint len);
};

#endif
