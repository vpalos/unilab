#ifndef CMDIREPORT_H
#define CMDIREPORT_H

#include <fx.h>
#include "cMDIChild.h"
#include "cVChart.h"
#include "cColorTable.h"
#include "cDCNativePrinter.h"
#include "cText.h"
#include "cLabel.h"
#include "cTextField.h"
#include "cHorizontalSeparator.h"

typedef struct sPageObject
{
    cLabel *lb;
    FXPacker *container;
    FXComposite *contents;
    FXTextField *headerA;
    FXText *headerB;
    cHorizontalSeparator *hs,*fs;
};

class cMDIReport : public cMDIChild
{
    FXDECLARE(cMDIReport);

    private:
        FXbool saved;
        FXString name;

        FXDict *pages;
        FXDict *charts;
        FXSwitcher *switcher;
        int counter,currentPage;
        FXScrollWindow *scrollWindow;
        cText *comment;

        FXButton *butPrev;
        FXButton *butNext;
        cLabel *lbPage;

        FXint pcSize,pcPos,pcPage,pcWidth;

        FXbool seps,isCom;
        FXString titleCon;
        FXint titleConOpts;

    protected:
        cMDIReport();

        void printObject(cDCNativePrinter *prDC,FXWindow *prObj,unsigned int prFlags,int prX,int prY);
        void printPage(cDCNativePrinter *prDC,int prPage,FXPrinter &prPrinter,int prSx,int prSy);
        FXbool saveData(void);

    public:
        cMDIReport(FXMDIClient *prP,const FXString &prName,FXIcon *prIc=NULL,FXPopup *prPup=NULL,FXuint prOpts=0,FXint prX=0,FXint prY=0,FXint prW=0,FXint prH=0);
        virtual ~cMDIReport();

        virtual void create();
        FXbool canClose(void);

        FXComposite *addPage(FXbool prNoCreate=false);
        FXComposite *getPageContents(int prPage=-1);
        void addTitle(FXString prText,FXint prOpts=0,FXbool prContinuum=false);
        void cancelTitleContinuum(void){titleCon="";};
        void addError(FXString prText,FXint prOpts=0);
        void addSuccess(FXString prText,FXint prOpts=0);
        void addSubTitle(FXString prText,FXint prOpts=0);
        void addText(FXString prText,FXint prOpts=JUSTIFY_LEFT,FXbool prIndent=false);
        void addTextMono(FXString prText,FXint prOpts=JUSTIFY_LEFT,FXbool prIndent=false,FXbool prSmall=false);
        void addTextBold(FXString prText,FXint prOpts=JUSTIFY_LEFT,FXbool prIndent=false);
        void addTextBoldMono(FXString prText,FXint prOpts=JUSTIFY_LEFT,FXbool prIndent=false,FXbool prSmall=false);
        void addChart(cVChart *prObj);
        void addDoubleCharts(cVChart *prObj1,cVChart *prObj2);
        void addTable(cColorTable *prObj,FXbool prIndent=false);
        void addPlateTable(cColorTable *prObj,FXbool prIndent=false);
        void addDetailTable(cColorTable *prObj,FXbool prIndent=false);
        void addComments(void);

        FXint getTextWidth(FXString prText);

        FXint getPageSize(void);
        FXbool fitsOnPage(FXint prSize);
        int getCurrentPage(void);
        int getWidth(FXbool prIndent=true){return (pcWidth-(prIndent?40:0));};
        void setPage(int prPage);
        void nextPage(void);

        void setTitle(FXString prText);
        void setHeader(FXString prText);

        enum
        {
            ID_MDIREPORT=FXMDIChild::ID_LAST,
            ID_REDIR,
            ID_COMMENT,
            ID_HA,
            ID_HB,
            ID_SEP,

            CMD_NEXT,
            CMD_PREV,
            CMD_SAVE,
            CMD_PRINT,
            CMD_CLOSE,

            ID_LAST
        };

        long onEvtRedir(FXObject *prSender,FXSelector prSelector,void *prData);
        long onEvtKRedir(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdHA(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdHB(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSeparator(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdNav(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdNext(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdPrev(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
