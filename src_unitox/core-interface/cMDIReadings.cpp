#include <time.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include <fxkeys.h>
#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cColorsManager.h"
#include "cFormulaRatio.h"
#include "cReadingsManager.h"
#include "cCaseManager.h"
#include "cSortList.h"
#include "cCaseAnalysisReport.h"
#include "cTemplateManager.h"
#include "cMDIReadings.h"
#include "cMDICaseManager.h"
#include "cWinMain.h"

cLocInfo::cLocInfo(int prCaseIndex,int prSampleLoc,int prPlate,int prSet)
{
    this->caseIndex=prCaseIndex;
    this->sampleLoc=prSampleLoc;
    this->plate=prPlate;
    this->isValue=false;
    this->set=prSet;
}

cLocInfo::~cLocInfo()
{
}

void cLocInfo::setValue(double prValue)
{
    isValue=true;
    value=prValue;
}

FXDEFMAP(cMDIReadings) mapMDIReadings[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIReadings::ID_MDIREADINGS,cMDIReadings::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::CMD_CLOSE,cMDIReadings::onCmdClose),

    FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_COPY,cMDIReadings::onCopyPlateTable),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_PASTE,cMDIReadings::onPastePlateTable),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_SELALL,cMDIReadings::onSelallPlateTable),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_SHOWRULES,cMDIReadings::onCmdShowRules),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::CMD_SAVE,cMDIReadings::onCmdSave),

    FXMAPFUNC(SEL_CONFIGURE,cMDIReadings::ID_PLATETABLE1,cMDIReadings::onRszPlateTable1),
    FXMAPFUNC(SEL_SELECTED,cMDIReadings::ID_PLATETABLE1,cMDIReadings::onUpdSelPlateTable1),
    //FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_PLATETABLE1,cMDIReadings::onCmdPlateTable1),
    FXMAPFUNC(SEL_REPLACED,cMDIReadings::ID_PLATETABLE1,cMDIReadings::onCmdPlateTable1),

    FXMAPFUNC(SEL_SELECTED,cMDIReadings::ID_PLATETABLE2,cMDIReadings::onUpdSelPlateTable2),
    //FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_PLATETABLE2,cMDIReadings::onCmdPlateTable2),
    FXMAPFUNC(SEL_REPLACED,cMDIReadings::ID_PLATETABLE2,cMDIReadings::onCmdPlateTable2),

    FXMAPFUNC(SEL_CONFIGURE,cMDIReadings::ID_CASELIST1,cMDIReadings::onRszCaseList1),
    FXMAPFUNC(SEL_SELECTED,cMDIReadings::ID_CASELIST1,cMDIReadings::onCmdSelCaseList1),
    FXMAPFUNC(SEL_CONFIGURE,cMDIReadings::ID_CASELIST2,cMDIReadings::onRszCaseList2),
    FXMAPFUNC(SEL_SELECTED,cMDIReadings::ID_CASELIST2,cMDIReadings::onCmdSelCaseList2),

    FXMAPFUNC(SEL_REPLACED,cMDIReadings::ID_CASELIST1,cMDIReadings::onCmdCaseList1),
    FXMAPFUNC(SEL_REPLACED,cMDIReadings::ID_CASELIST2,cMDIReadings::onCmdCaseList2),

    FXMAPFUNC(SEL_COMMAND,cMDIReadings::CMD_REREAD,cMDIReadings::onCmdReread),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::CMD_TEMPLATE,cMDIReadings::onCmdTemplate),

    FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_REP_CA,cMDIReadings::onCmdReport),

    //FXMAPFUNC(SEL_KEYPRESS,0,cMDIReadings::onEvtRedir),
    //FXMAPFUNC(SEL_KEYRELEASE,0,cMDIReadings::onEvtRedir),
};

FXIMPLEMENT(cMDIReadings,cMDIChild,mapMDIReadings,ARRAYNUMBER(mapMDIReadings))

cMDIReadings::cMDIReadings()
{
}

void cMDIReadings::sortControls(int *prData,int prLength,int prDesc)
{
    /*int parcurgeri=1,a,b;
    bool schimbat=true;
    while(schimbat)
    {
        schimbat=false;
        for(int i=0;i<prLength-parcurgeri;i++)
        {
            a=abs(prData[i])%1000;
            b=abs(prData[i+1])%1000;
            if((a>b && !prDesc) || (a<b && prDesc))
            {
                a=prData[i+1];
                prData[i+1]=prData[i];
                prData[i]=a;
                schimbat=true;
            }
        }
        parcurgeri++;
    }*/
}

FXString cMDIReadings::sampleKey(int prIndex)
{
    FXString ret=FXStringFormat("%d",1 + prIndex);
    return ret;
}

FXString cMDIReadings::sampleVal(double prValue)
{
    return prValue>4.000?oLanguage->getText("str_rdgmdi_over"):FXStringFormat("%g",round(prValue*1000)/1000);
}

FXString cMDIReadings::calculVal(double prValue)
{
    double no=round(prValue*1000)/1000;
    FXString val=FXStringFormat("%.13g",no);
    if(prValue==-9999 || val.lower()=="inf" || val.lower()=="nan" || no>999999999.999)
        return oLanguage->getText("str_invalid");
    return val;
}

FXString cMDIReadings::calculVal1(double prValue)
{
    double no=round(prValue*10)/10;
    FXString val=FXStringFormat("%.13g",no);
    if(prValue==-9999 || val.lower()=="inf" || val.lower()=="nan" || no>999999999.999)
        return oLanguage->getText("str_invalid");
    return val;
}

FXString cMDIReadings::displayPosition(int prPosition)
{
    int pos=platePosition(prPosition);
    return FXStringFormat("%c%d",'A'+pos/12,pos%12+1);
}

int cMDIReadings::platePosition(int prPosition)
{
    return prPosition/8+12*(prPosition%8);
}

bool cMDIReadings::isControlPosition(int prPosition,int *prControls,int prControlsCount,bool prAlelisa)
{
    int pos2 = prPosition - (prAlelisa?1:0);
    for(int i=0;i<prControlsCount;i++) {
        if(prControls[i]==prPosition || prControls[i]==pos2)
            return true;
        if(prControls[i]==1000 && (prPosition==0 || pos2==0))
            return true;
    }
    return false;
}

void cMDIReadings::appendControls(double *prDest,double *prSrc,int *prControls,int prControlsCount,bool prAlelisa)
{
    int j,k,controli=0;
    double val;

    for(int i=0;i<prControlsCount;i++) {

        j=prControls[i]==1000?0:prControls[i];
        val=prSrc[platePosition(j)];
        prDest[controli++]=val<0?0.000:val;

        if(prAlelisa && ((j%96)!=95)) {
            k = platePosition(j + 1);
            val=prSrc[k];
            prDest[controli++]=val<0?0.000:val;
        }
    }
}

void cMDIReadings::drawPlateTables(void)
{
    if(!tPlatesCount)
        return;
    int platesgi=0;
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        for(int platesi=0;platesi<casesSets[setsi].platesCount;platesi++,platesgi++)
        {
            for(int i=platesgi*8;i<(platesgi+1)*8;i++)
                for(int j=0;j<12;j++)
                {
                    plateTable1->setItemJustify(i,j,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                    plateTable1->setItemText(i,j,(char*)NULL);
                    plateTable1->setItemData(i,j,NULL);
                    plateTable1->setCellEditable(i,j,false);
                    plateTable1->setCellColor(i,j,FXRGB(255,255,255));
                    plateTable1->setItemBorders(i,j,(i>0 && i%8==0?FXTableItem::TBORDER:0)|
                                                FXTableItem::RBORDER|((i+1)%8==0?FXTableItem::BBORDER:0));
                }
        }
    }

    bool p2_filled=plateTable2->getNumRows()>0?true:false;
    int nc = 5;//6;
    if(p2_filled && (nc!=plateTable2->getNumColumns()))
    {
        if(nc>plateTable2->getNumColumns())
            plateTable2->insertColumns(1,nc-plateTable2->getNumColumns());
        else
            plateTable2->removeColumns(1,plateTable2->getNumColumns()-nc);
        plateTable2->layout();
        for(int i=0;i<plateTable2->getNumRows();i++)
        {
            for(int j=0;j<nc;j++)
            {
                plateTable2->setItemJustify(i,j,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                plateTable2->setCellEditable(i,j,j>0?false:true);
            }
            plateTable2->getRowHeader()->setItemJustify(i,JUSTIFY_CENTER_X);
        }
    }

    bool firstDraw = charts == NULL;

    if(!p2_filled)
    {
        plateTable2->setTableSize(plateTable2->getNumRows(),nc);
        plateTable2->layout();

    }

    if(firstDraw) {
        charts=(cVChart**)malloc(tPlatesCount*sizeof(cVChart*));
        if(!charts)
        {
            tPlatesCount=0;
            tSetsCount=0;
            tCasesCount=0;
            saved=true;
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
            return;
        }
    }

    for(int i=0;i<plateTable2->getNumRows();i++)
    {
        for(int j=0;j<plateTable2->getNumColumns();j++)
            plateTable2->setItemText(i,j,(char*)NULL);
        ((cLocInfo*)plateTable2->getItemData(i,0))->isValue=false;
    }
    for(int i=0;i<nc;i++)
        plateTable2->getColumnHeader()->setItemJustify(i,JUSTIFY_CENTER_X);


    plateTable2->getColumnHeader()->setItemText(0,oLanguage->getText("str_rdgmdi_od"));
    plateTable2->setColumnWidth(0,90);
    plateTable2->getColumnHeader()->setItemText(1,oLanguage->getText("str_rdgmdi_conc"));
    plateTable2->getColumnHeader()->setItemText(2,oLanguage->getText("str_rdgmdi_bbo"));
    plateTable2->setColumnWidth(2,87);
    plateTable2->getColumnHeader()->setItemText(3,oLanguage->getText("str_rdgmdi_calc"));
    plateTable2->getColumnHeader()->setItemText(4,oLanguage->getText("str_rdgmdi_cv"));
    plateTable2->setColumnWidth(4,84);
    //plateTable2->getColumnHeader()->setItemText(5,oLanguage->getText("str_rdgmdi_result"));
    //plateTable2->setColumnWidth(5,70);

    for(int j=0;j<12;j++)
    {
        plateTable1->setItemBorders(platesgi*8-1,j,plateTable1->getItemBorders(platesgi*8-1,j)|FXTableItem::BBORDER);
        plateTable1->getColumnHeader()->setItemJustify(j,JUSTIFY_CENTER_X);
        plateTable1->getColumnHeader()->setItemText(j,FXStringFormat("%d",j+1));
    }
    for(int j=0;j<plateTable1->getNumRows();j++)
    {
        plateTable1->getRowHeader()->setItemJustify(j,JUSTIFY_CENTER_X);
        plateTable1->getRowHeader()->setItemText(j,FXStringFormat("P%d:%c",j/8+1,j%8+'A'));
    }

    char replicates[10];
    char creplicates[10];
    int k,l,offset,m;
    int platesi=0;
    int platesci=0;
    int datai=0;
    int samplei=0;
    int casesgi=0;
    int controlsi=0;
    int casepi=0;
    int p2_datai=0;
    int deltasi=0;
    int replicatesi=0;
    int creplicatesi=0;
    sCResult cresult;
    sCResult scresult;
    sSResult sresult;


    double *ctData=NULL,n,val;
    FXDict *cv;
    FXString invalidPlates="";
    invalidPlatesDetails="";
    int dp,rp,pd=0,diff=0,spi=-1,cplatei,splatei;
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        spi=-1;
        splatei=-1;
        diff=0;
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++,casesgi++)
        {
            cplatei=0;
            samplei=0;
            for(int g=spi+1;g<casesSets[setsi].cases[casei].startPlate;g++,diff++,splatei++);
            spi=casesSets[setsi].cases[casei].startPlate;
            while(samplei<1)
            {
                deltasi=0;
                replicatesi=0;
                replicates[0]=0;
                while(replicatesi<=casesSets[setsi].cases[casei].replicates)
                {
                    platesi=datai/96;
                    if(platesci==platesi)
                    {
                        splatei++;
                        if(samplei)cplatei++;
                        ctData=casesSets[setsi].cases[casei].controlsData+cplatei*casesSets[setsi].controlsPerPlate*casesSets[setsi].controls_replicates;

                        if(p2_datai>0)
                            for(int xi=0;xi<nc;xi++)
                                plateTable2->setItemBorders(p2_datai-1,xi,plateTable2->getItemBorders(p2_datai-1,xi)|FXTableItem::BBORDER);


                        // Create chart
                        if(firstDraw) {
                            charts[platesi]=new cVChart(chartFrame,
                                cFormulaRatio::calculateChartType(casesSets[setsi].cal),
                                LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT|LAYOUT_TOP,0,0,600,400);
                            charts[platesi]->setTitle(oLanguage->getText("str_rdgmdi_plate")+FXStringFormat(" %d",platesi+1)+" - "+(*casesSets[setsi].assay) + " (" +
                                *casesSets[setsi].cal + ") - " + casesSets[setsi].templateRes->getCellString(0,0));
                            charts[platesi]->setVAxisTitle(cFormulaRatio::calculateChartVAxis(casesSets[setsi].cal));
                            charts[platesi]->create();
                            charts[platesi]->recalc();
                        }
                        charts[platesi]->setHAxisTitle(oLanguage->getText("str_asymdi_ctlcr") + " (" + *casesSets[setsi].unit + ") - " +
                            oLanguage->getText("str_rdgmdi_corr") + ": ");
                        cv = charts[platesi]->dataSet();

                        cv->clear();
                        FXbool err=false;
                        FXdouble last_ctl = 1000, this_ctl = 0;
                        controlsi=0;
                        for(int i=0;i<casesSets[setsi].controlsPerPlate;i++)
                        {
                            creplicatesi=0;
                            creplicates[0]=0;
                            while(creplicatesi<casesSets[setsi].controls_replicates)
                            {
                                m=platePosition((abs(casesSets[setsi].controlsLayout[i])==1000?0:abs(casesSets[setsi].controlsLayout[i])) + creplicatesi);
                                l=m%12;
                                k=platesi*8+m/12;
                                plateTable1->setItemText(k,l,sampleVal(ctData[controlsi]));
                                plateTable1->setItemData(k,l,new cLocInfo(casesSets[setsi].controlsLayout[i]>=0?-2:-3,controlsi,splatei,setsi));
                                plateTable1->setCellColor(k,l,FXRGB(255,90+creplicatesi*50,90+creplicatesi*50));
                                plateTable1->setItemBorders(k,l,FXTableItem::LBORDER|FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
                                plateTable1->setCellEditable(k,l,true);
                                if(k>0)plateTable1->setItemBorders(k-1,l,plateTable1->getItemBorders(k-1,l)|FXTableItem::BBORDER);
                                if(l>0)plateTable1->setItemBorders(k,l-1,plateTable1->getItemBorders(k,l-1)|FXTableItem::RBORDER);
                                if((k+1)<plateTable1->getNumRows())plateTable1->setItemBorders(k+1,l,plateTable1->getItemBorders(k+1,l)|FXTableItem::TBORDER);
                                if(l<11)plateTable1->setItemBorders(k,l+1,plateTable1->getItemBorders(k,l+1)|FXTableItem::LBORDER);

                                if(!p2_filled)
                                {
                                    plateTable2->insertRows(p2_datai);
                                    plateTable2->getRowHeader()->setItemJustify(p2_datai,JUSTIFY_CENTER_X);
                                    plateTable2->getRowHeader()->setItemText(p2_datai,FXStringFormat("P%d: Std%d%s",platesi+1,i+1,creplicates));
                                    for(int xi=0;xi<nc;xi++)
                                    {
                                        plateTable2->setItemJustify(p2_datai,xi,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                                        plateTable2->setItemStipple(p2_datai,xi,STIPPLE_NONE);
                                        plateTable2->setCellEditable(p2_datai,xi,xi>0?false:true);
                                    }
                                    plateTable2->setItemBorders(p2_datai,0,FXTableItem::LBORDER|FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
                                    plateTable2->setCellColor(p2_datai,0,FXRGB(255,90+creplicatesi*50,90+creplicatesi*50));
                                }
                                plateTable2->setItemText(p2_datai,0,calculVal(ctData[controlsi]));
                                plateTable2->setItemData(p2_datai,0,new cLocInfo(casesSets[setsi].controlsLayout[i]>=0?-2:-3,controlsi,splatei,setsi));
                                if(creplicatesi) {
                                    for(int xi=1;xi<nc;xi++) {
                                        plateTable2->setItemText(p2_datai,xi,creplicates);
                                    }
                                }
                                else {
                                    cresult = cFormulaRatio::calculateControl(
                                        casesSets[setsi].cal,
                                        i,
                                        ctData,
                                        casesSets[setsi].controlsLayout,
                                        casesSets[setsi].controlsPerPlate,
                                        casesSets[setsi].controls_replicates==2,
                                        casesSets[setsi].isSc,
                                        casesSets[setsi].asyControls,
                                        casesSets[setsi].asyControlsBBO,
                                        casesSets[setsi].asyControlsCount);

                                    this_ctl = cFormulaRatio::cOD(i,
                                        ctData,
                                        casesSets[setsi].controls_replicates==2);
                                    if(!err) {
                                        err = this_ctl > last_ctl;
                                    }
                                    last_ctl = this_ctl;

                                    bool linear = cFormulaRatio::calculateChartType(casesSets[setsi].cal) == cVChart::CHART_LINEAR;
                                    int delta = linear ? 0 : 1;

                                    // Add control to chart with trend point
                                    if((i > 0) || ((i == 0) && linear)) {
                                        sVChartData *cval=new sVChartData;
                                        cval->value=cresult.y;
                                        cval->xvalue=cresult.ippu;
                                        cval->tvalue=cresult.ty;
                                        cval->name=calculVal(cresult.ippu);
                                        cv->insert(FXStringFormat("%d",i - delta).text(),cval);
                                        if(i == 1) {
                                            charts[platesi]->appendHAxisTitle(calculVal(cresult.corr));
                                        }
                                    }
                                    else if(casesSets[setsi].isSc) {
                                        for(int sc = delta; sc < casesSets[setsi].asyControlsCount; sc++) {
                                            scresult = cFormulaRatio::calculateControl(
                                                casesSets[setsi].cal,
                                                sc,
                                                ctData,
                                                casesSets[setsi].controlsLayout,
                                                casesSets[setsi].controlsPerPlate,
                                                casesSets[setsi].controls_replicates==2,
                                                casesSets[setsi].isSc,
                                                casesSets[setsi].asyControls,
                                                casesSets[setsi].asyControlsBBO,
                                                casesSets[setsi].asyControlsCount);

                                            sVChartData *cval=new sVChartData;
                                            cval->value=scresult.y;
                                            cval->xvalue=scresult.ippu;
                                            cval->tvalue=scresult.ty;
                                            cval->name=calculVal(scresult.ippu);
                                            cv->insert(FXStringFormat("%d",sc - delta).text(),cval);
                                            if(sc == 1) {
                                                charts[platesi]->appendHAxisTitle(calculVal(scresult.corr));
                                            }
                                        }
                                        charts[platesi]->appendHAxisTitle(" - " + oLanguage->getText("str_asymdi_ctlsc"));
                                    }

                                    plateTable2->setItemText(p2_datai,1,FXStringFormat("%s %s", calculVal(cresult.ippu).text(), casesSets[setsi].unit->text()));
                                    plateTable2->setItemText(p2_datai,2,calculVal1(cresult.bbo) + "%");
                                    plateTable2->setItemText(p2_datai,3,(cresult.cppu < 0) ? "-" : FXStringFormat("%s %s", calculVal(cresult.cppu).text(), casesSets[setsi].unit->text()));
                                    plateTable2->setItemText(p2_datai,4,(cresult.cppu < 0) ? "-" : calculVal1(cresult.var) + "%");
                                    //plateTable2->setItemText(p2_datai,5,"---");
                                }
                                p2_datai++;
                                controlsi++;

                                creplicates[creplicatesi]='\'';
                                creplicates[++creplicatesi]=0;
                            }
                        }

                        chartFrame->recalc();
                        chartWin->recalc();

                        platesci++;

                        if(err)
                        {
                            invalidPlates=invalidPlates+"\n"+oLanguage->getText("str_rdgmdi_plate")+" "+FXStringVal(platesci);
                            invalidPlatesDetails=invalidPlatesDetails+oLanguage->getText("str_rdgmdi_plate")+FXStringFormat(" %d [",platesci)+
                                                 *casesSets[setsi].assay+" - "+(*casesSets[setsi].assaytitle)+"]:\n           "+
                                                 oLanguage->getText("str_reading_invbbo")+"\n";
                        }
                    }

                    casepi=datai%96;
                    dp=datai/96;
                    rp=pd+casesSets[setsi].cases[casei].startPlate-diff;
                    if(!isControlPosition(casepi,casesSets[setsi].controlsLayout,
                                          casesSets[setsi].controlsPerPlate,casesSets[setsi].controls_replicates==2) &&
                                          ((casepi>=casesSets[setsi].cases[casei].startCell && dp==rp) || (dp>rp)))
                    {
                        m=platePosition(datai%96);
                        l=m%12;
                        k=(datai/96)*8+m/12;
                        offset=(casesSets[setsi].cases[casei].replicates+1)*samplei+replicatesi;
                        plateTable1->setItemText(k,l,sampleVal(casesSets[setsi].cases[casei].data[offset]));
                        plateTable1->setItemData(k,l,new cLocInfo(casesgi,offset));
                        plateTable1->setCellEditable(k,l,true);
                        plateTable1->setCellColor(k,l,casesgi==selectedCase?FXRGB(180,180,255):cColorsManager::getColor(casei,-replicatesi*10));
                        if(!p2_filled)
                        {
                            plateTable2->insertRows(p2_datai);
                            plateTable2->getRowHeader()->setItemJustify(p2_datai,JUSTIFY_CENTER_X);
                            plateTable2->getRowHeader()->setItemText(p2_datai,FXStringFormat("(%s) P%d:%c%d",sampleKey(casei).text(),platesi+1,'A'+m/12,l+1));
                            for(int xi=0;xi<nc;xi++)
                            {
                                plateTable2->setItemJustify(p2_datai,xi,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                                plateTable2->setItemStipple(p2_datai,xi,STIPPLE_NONE);
                                plateTable2->setCellEditable(p2_datai,xi,xi>0?false:true);
                            }
                            plateTable2->setItemData(p2_datai,0,new cLocInfo(casesgi,offset,platesi));
                        }
                        plateTable2->setItemText(p2_datai,0,calculVal(casesSets[setsi].cases[casei].data[offset]));
                        if(replicatesi==0)
                        {
                            val=n=0;
                            for(int xi=0;xi<=casesSets[setsi].cases[casei].replicates;xi++,n++)
                                val+=casesSets[setsi].cases[casei].data[(casesSets[setsi].cases[casei].replicates+1)*samplei+xi];
                            ((cLocInfo*)plateTable2->getItemData(p2_datai,0))->setValue(val/n);
                        }
                        else
                            for(int xi=1;xi<nc;xi++)
                                plateTable2->setItemText(p2_datai,xi,replicates);
                        plateTable2->setCellColor(p2_datai,0,casesgi==selectedCase?FXRGB(180,180,255):cColorsManager::getColor(casei,-replicatesi*10));
                        for(int xi=1;xi<nc;xi++)
                            plateTable2->setCellColor(p2_datai,xi,FXRGB(247,247,247));
                        deltasi=1;

                        if(((cLocInfo*)plateTable2->getItemData(p2_datai,0))->isValue)
                        {
                            double val = ((cLocInfo*)plateTable2->getItemData(p2_datai,0))->value;
                            sresult = cFormulaRatio::calculateSample(
                                casesSets[setsi].cal,
                                val,
                                casesSets[setsi].cases[casei].replicates,
                                casesSets[setsi].cases[casei].factor,
                                ctData,
                                casesSets[setsi].controlsLayout,
                                casesSets[setsi].controlsPerPlate,
                                casesSets[setsi].controls_replicates==2,
                                casesSets[setsi].isSc,
                                casesSets[setsi].asyControls,
                                casesSets[setsi].asyControlsBBO,
                                casesSets[setsi].asyControlsCount);

                            plateTable2->setItemText(p2_datai,1,"---");
                            plateTable2->setItemText(p2_datai,2,calculVal1(sresult.bbo) + "%");
                            plateTable2->setItemText(p2_datai,3,(sresult.result == 1) ? oLanguage->getText("str_rdgmdi_over") : FXStringFormat("%s %s", calculVal(sresult.cppu).text(), casesSets[setsi].unit->text()));
                            plateTable2->setItemText(p2_datai,4,"---");

                            /*switch(sresult.result)
                            {
                                case -1:
                                    plateTable2->setCellTextColor(p2_datai,5,FXRGB(200,200,200));
                                    plateTable2->useCellTextColor(p2_datai,5,true);
                                    plateTable2->setItemText(p2_datai,5,oLanguage->getText("str_rdgmdi_neg"));
                                    break;
                                case 1:
                                    plateTable2->setCellTextColor(p2_datai,5,FXRGB(255,0,0));
                                    plateTable2->useCellTextColor(p2_datai,5,true);
                                    plateTable2->setItemText(p2_datai,5,oLanguage->getText("str_rdgmdi_pos"));
                                    break;
                                case 100:
                                    plateTable2->setCellTextColor(p2_datai,5,FXRGB(255,0,0));
                                    plateTable2->useCellTextColor(p2_datai,5,true);
                                    plateTable2->setItemText(p2_datai,5,oLanguage->getText("str_rdgmdi_over"));
                                    break;
                                default:
                                    plateTable2->useCellTextColor(p2_datai,5,false);
                                    plateTable2->setItemText(p2_datai,5,oLanguage->getText("str_invalid"));
                                    break;
                            }*/
                        }

                        p2_datai+=deltasi;
                        replicates[replicatesi]='\'';
                        replicates[replicatesi+1]=0;
                        replicatesi++;
                    }
                    datai++;
                }
                samplei+=deltasi;
            }
        }
        pd+=casesSets[setsi].platesCount;
        int mdi=datai%96;
        if(mdi)
            datai+=96-mdi;
    }



    if(invalidPlates.empty())
    {
        lbInv->setText((char*)NULL);
        lbInv->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        lbInv->setTipText((char*)NULL);
        lbInv->hide();
    }
    else
    {
        lbInv->show();
        lbInv->setText(oLanguage->getText("str_rdgmdi_invplt")+""+invalidPlates);
        lbInv->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        lbInv->setTipText(oLanguage->getText("str_rdgmdi_rhint"));
    }
}

bool cMDIReadings::plate1HasValue(int prIndex) {
    return (plateTable1->getItemText(prIndex % 8, prIndex / 8).empty())?false:true;
}

void cMDIReadings::locateCaseLists(int prIndex)
{
    if(prIndex<0)
        return;
    caseList1->makePositionVisible(prIndex,0);
    caseList2->makePositionVisible(prIndex,0);
}

void cMDIReadings::locatePlateTable1(int prIndex)
{
    if(prIndex<0)
        return;
    for(int i=0;i<plateTable1->getNumRows();i++)
        for(int j=0;j<plateTable1->getNumColumns();j++)
        {
            if(!plateTable1->getItemData(i,j))
                continue;
            if((((cLocInfo*)plateTable1->getItemData(i,j))->caseIndex)==prIndex)
            {
                plateTable1->makePositionVisible(i,j);
                return;
            }
        }
}

void cMDIReadings::locatePlateTable2(int prIndex)
{
    if(prIndex<0)
        return;

    for(int i=0;i<plateTable2->getNumRows();i++)
    {
        if(!plateTable2->getItemData(i,0))
            continue;
        if((((cLocInfo*)plateTable2->getItemData(i,0))->caseIndex)==prIndex)
        {
            plateTable2->makePositionVisible(i,0);
            return;
        }
    }
}

void cMDIReadings::selectCase(int prIndex,int prPlate)
{
    selectedCase=prIndex;
    caseList1->killSelection();
    caseList2->killSelection();

    drawPlateTables();

    if(prIndex==-1)
    {
        assayId->setText(FXStringFormat("%s\n",oLanguage->getText("str_rdgmdi_nosel").text()));
        assayTpl->setText(FXStringFormat("%s\n",oLanguage->getText("str_rdgmdi_nosel").text()));
        assayTech->setText(oLanguage->getText("str_rdgmdi_nosel"));
        this->setTitle(oLanguage->getText("str_reading"));
        return;
    }

    sCaseEntry *found=NULL;
    if(prIndex<0)
    {
        if(prPlate<0)
            return;

        int platesi=0;
        for(int i=0;i<tSetsCount;i++)
        {
            platesi+=casesSets[i].platesCount;
            if(platesi>prPlate)
            {
                found=&casesSets[i].cases[0];
                break;
            }
        }
        if(found==NULL)
            return;
    }

    sCaseEntry *selCase=prIndex<0?found:(sCaseEntry*)caseList1->getItemData(prIndex,0);

    if(prIndex>=0)
    {
        caseList1->selectRow(prIndex);
        caseList2->selectRow(prIndex);
    }

    if(prIndex<0)
    {
        assayId->setText(FXStringFormat("%s\n",prIndex==-2?oLanguage->getText("str_rdgmdi_posc").text():oLanguage->getText("str_rdgmdi_negc").text()));
        this->setTitle(oLanguage->getText("str_reading"));
    }
    else
    {
        this->setTitle(oLanguage->getText("str_reading")+" ("+caseList1->getItemText(prIndex,1)+")");
        assayId->setText(FXStringFormat("%s \"%s\", %s %s,\n%s \"%s\"",
                         oLanguage->getText("str_rdgmdi_caseid").text(),caseList1->getItemText(prIndex,1).text(),
                         oLanguage->getText("str_rdgmdi_factor").text(),FXStringFormat("%d",selCase->factor).text(),
                         oLanguage->getText("str_rdgmdi_comm").text(),caseList1->getItemText(prIndex,2).text()));
    }

    assayTpl->setText(FXStringFormat("%s \"%s\"\n%s \"%s\", %s \"%s\", %s \"%s\"",
                      oLanguage->getText("str_tplmdi_title").text(),selCase->tpl->text(),
                      oLanguage->getText("str_rdgmdi_usedagainst").text(),selCase->assay->text(),
                      oLanguage->getText("str_rdgmdi_assayLot").lower().text(),selCase->lot->text(),
                      oLanguage->getText("str_rdgmdi_assayExp").lower().text(),selCase->expirationDate->text()));
    assayTech->setText(oWinMain->getTechnician());
}

void cMDIReadings::updateData()
{
    saved=false;
}

FXbool cMDIReadings::saveData(void)
{
    plateTable1->acceptInput(true);
    plateTable2->acceptInput(true);
    if(saved)
        return true;

    sCasesSet *s;
    sCaseEntry *c;

    FXchar ftime[100];
    time_t curtime;
    struct tm *loctime;
    curtime=time(NULL);
    loctime=localtime(&curtime);
    strftime(ftime,100,"%Y-%m-%d",loctime);
    FXString nowDate(ftime);
    FXString data_defs,controls_defs;


    curtime=time(NULL);
    loctime=localtime(&curtime);
    strftime(ftime,100,"%s",loctime);
    FXString *sroid=new FXString(ftime);
    srand(curtime);
    FXString *nroid=new FXString();

    int cci=0,p=0,d,od;
    saved=true;
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        *nroid = *sroid+FXStringVal(rand());
        d=od=0;
        s=&(casesSets[setsi]);
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++,cci++)
        {
            c=&(s->cases[casei]);
            data_defs="";
            for(int i=0;i<c->controlsCount;i++)
                data_defs=data_defs+FXStringVal(casesSets[setsi].cases[casei].controlsData[i])+" ";
            data_defs=data_defs+"\n";

            for(int i=0;i<(c->replicates+1);i++)
                data_defs=data_defs+FXStringVal(casesSets[setsi].cases[casei].data[i])+" ";

            if(newdata)
            {
                if(cCaseManager::caseExists(*c->id,*c->assay))
                {
                    int result=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),
                                                      (*c->id+"\n\n"+oLanguage->getText("str_cseman_duplicate")).text());
                    int ct=1;
                    FXString newId;
                    switch(result)
                    {
                        case MBOX_CLICKED_YES:
                            cCaseManager::setCase(*c->id,*c->assay,*c->id,*c->reading_oid,nowDate,*c->technician,*c->assay,
                                      *c->lot,*c->expirationDate,*c->commodity,c->replicates,c->factor,
                                      *c->tpl,c->startPlate,c->startCell,
                                      c->plateSpanCount,*c->controls_defs,data_defs,*c->comments);
                            continue;
                            break;
                        case MBOX_CLICKED_NO:
                            while(cCaseManager::caseExists(newId=*c->id+"_"+FXStringVal(ct++),*c->assay));
                            *c->id=newId;
                            if(c->old_id)delete c->old_id;
                            c->old_id=new FXString(newId);
                            caseList1->setItemText(cci,1,*c->id);
                            caseList2->setItemText(cci,1,*c->id);
                            selectCase(selectedCase);

                            break;
                        default:
                            saved=false;
                            continue;
                            break;
                    }
                }

                cCaseManager::addCase(*c->id,*c->reading_oid,*c->readingDate,*c->technician,*c->assay,
                                      *c->lot,*c->expirationDate,*c->commodity,c->replicates,c->factor,
                                      *c->tpl,c->startPlate,c->startCell,
                                      c->plateSpanCount,*c->controls_defs,data_defs,*c->comments);
            }
            else {
                cDataResult *res=oDataLink->execute("SELECT data_defs FROM t_gncases WHERE id='"+*c->old_id+"' AND assay_oid='"+*c->assay+"';");
                FXString od(res->getCellString(0,0).before('\n'));
                FXString nd(data_defs.before('\n'));

                if(od != nd) {
                    *c->reading_oid = *nroid;
                }

                cCaseManager::setCase(*c->old_id,*c->assay,*c->id,*c->reading_oid,*c->readingDate,*c->technician,*c->assay,
                                      *c->lot,*c->expirationDate,*c->commodity,c->replicates,c->factor,
                                      *c->tpl,c->startPlate,c->startCell,
                                      c->plateSpanCount,*c->controls_defs,data_defs,*c->comments);
                if(c->old_id)delete c->old_id;
                c->old_id=new FXString(*c->id);
            }

            d=casesSets[setsi].cases[casei].startPlate;
            if(d!=od)
            {
                p++;
                od=d;
            }
            p+=casesSets[setsi].cases[casei].plateSpanCount-1;
        }
    }
    if(saved)
        newdata=false;
    cMDICaseManager::cmdResearch();
    return true;
}

cMDIReadings::cMDIReadings(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH)
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_MDIREADINGS);
    saved=false;
    newdata=true;
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(this,LAYOUT_FILL);
    tbMain=new FXTabBook(_hframe0,this,ID_TBMAIN,PACK_UNIFORM_WIDTH|PACK_UNIFORM_HEIGHT|LAYOUT_FILL,0,0,0,0,0,0,0,0);
    tiPrimary=new FXTabItem(tbMain,oLanguage->getText("str_rdgmdi_primary"));
        FXVerticalFrame *_vframe0=new FXVerticalFrame(tbMain,FRAME_RAISED|FRAME_THICK|LAYOUT_FILL);
        FXHorizontalFrame *_hframe200=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
        new FXLabel(_hframe200,oLanguage->getText("str_rdgmdi_primary_od"));
        lbPlates=new FXLabel(_hframe200,(char*)NULL);
        FXVerticalFrame *_vframe01=new FXVerticalFrame(_vframe0,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
            plateTable1=new cColorTable(_vframe01,this,ID_PLATETABLE1,TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT|LAYOUT_FILL,0,0,0,181);
            plateTable1->setTableSize(8,12);
            plateTable1->setGridColor(FXRGB(120,120,120));
            plateTable1->setRowHeaderWidth(42);
            plateTable1->setDefColumnWidth(39);
            plateTable1->setCellBorderWidth(1);
            plateTable1->setSelBackColor(FXRGB(100,100,140));
            plateTable1->setSelTextColor(0);
            plateTable1->setGridColor(FXRGB(120,120,120));
            plateTable1->setBorderColor(FXRGB(120,120,120));
            plateTable1->setStippleColor(FXRGB(250,250,240));
            plateTable1->setScrollStyle(HSCROLLING_OFF|HSCROLLER_NEVER|VSCROLLING_ON|VSCROLLER_ALWAYS|SCROLLERS_TRACK);
            plateTable1->showVertGrid(false);
            plateTable1->showHorzGrid(false);

            edit=new FXMenuPane(this);
            new FXMenuCommand(edit,oLanguage->getText("str_rdgmdi_edit1"),NULL,plateTable1,FXTable::ID_SELECT_ALL);
            new FXMenuSeparator(edit);
            new FXMenuCommand(edit,oLanguage->getText("str_rdgmdi_edit2"),NULL,this,ID_COPY);
            new FXMenuCommand(edit,oLanguage->getText("str_rdgmdi_edit3"),NULL,this,ID_PASTE);
            mbEdit=new FXMenuButton(_hframe200,oLanguage->getText("str_rdgmdi_mbedit"),NULL,edit,ICON_AFTER_TEXT|LAYOUT_RIGHT|MENUBUTTON_DOWN|FRAME_RAISED);
            mbEdit->setTextColor(FXRGB(180,0,0));

            setAccelTable(new FXAccelTable());
            getAccelTable()->addAccel(MKUINT(KEY_A,CONTROLMASK),this,FXSEL(SEL_COMMAND,ID_SELALL));
            getAccelTable()->addAccel(MKUINT(KEY_a,CONTROLMASK),this,FXSEL(SEL_COMMAND,ID_SELALL));
            getAccelTable()->addAccel(MKUINT(KEY_C,CONTROLMASK),this,FXSEL(SEL_COMMAND,ID_COPY));
            getAccelTable()->addAccel(MKUINT(KEY_c,CONTROLMASK),this,FXSEL(SEL_COMMAND,ID_COPY));
            getAccelTable()->addAccel(MKUINT(KEY_V,CONTROLMASK),this,FXSEL(SEL_COMMAND,ID_PASTE));
            getAccelTable()->addAccel(MKUINT(KEY_v,CONTROLMASK),this,FXSEL(SEL_COMMAND,ID_PASTE));

        _gb0=new FXGroupBox(_vframe0,oLanguage->getText("str_rdgmdi_caseinfo"),GROUPBOX_TITLE_LEFT|LAYOUT_FILL_X|FRAME_GROOVE);
            FXVerticalFrame *_vframe02=new FXVerticalFrame(_gb0,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
            caseList1=new cColorTable(_vframe02,this,ID_CASELIST1,TABLE_NO_COLSELECT|LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT,0,0,0,100);
            caseList1->setTableSize(0,6);
            caseList1->setRowHeaderWidth(0);
            caseList1->setColumnText(0,oLanguage->getText("str_rdgmdi_cllayout"));
            caseList1->setColumnWidth(0,80);
            caseList1->setColumnText(1,oLanguage->getText("str_rdgmdi_clid"));
            caseList1->setColumnWidth(1,110);
            caseList1->setColumnText(2,oLanguage->getText("str_rdgmdi_comm"));
            caseList1->setColumnWidth(2,80);
            caseList1->setColumnText(3,oLanguage->getText("str_rdgmdi_clasy"));
            caseList1->setColumnWidth(3,80);
            caseList1->setColumnText(4,oLanguage->getText("str_rdgmdi_clrepl"));
            caseList1->setColumnWidth(4,70);
            caseList1->setColumnText(5,oLanguage->getText("str_rdgmdi_dil"));
            caseList1->setColumnWidth(5,50);
            caseList1->getColumnHeader()->setItemJustify(0,JUSTIFY_CENTER_X);
            caseList1->getColumnHeader()->setItemJustify(4,JUSTIFY_CENTER_X);
            caseList1->getColumnHeader()->setItemJustify(5,JUSTIFY_CENTER_X);
            caseList1->setGridColor(FXRGB(255,255,255));
            caseList1->setCellBorderWidth(1);
            caseList1->setColumnHeaderHeight(22);
            caseList1->showHorzGrid(true);
            caseList1->showVertGrid(false);
            caseList1->setSelTextColor(0);
            caseList1->setCellBorderColor(FXRGB(255,0,0));
            caseList1->setSelBackColor(FXRGB(180,180,255));
            caseList1->setScrollStyle(HSCROLLING_ON|VSCROLLER_ALWAYS|VSCROLLING_ON);

        FXGroupBox *_gb1=new FXGroupBox(_vframe0,oLanguage->getText("str_rdgmdi_assayinfo"),GROUPBOX_TITLE_LEFT|LAYOUT_FILL_X|FRAME_GROOVE);
            FXVerticalFrame *_vframe03=new FXVerticalFrame(_gb1,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,5);
                FXHorizontalFrame *_hframe01=new FXHorizontalFrame(_vframe03,LAYOUT_FILL,0,0,0,0,0,0,0,0);
                    new FXLabel(_hframe01,oLanguage->getText("str_rdgmdi_caseInfo")+":",NULL,LAYOUT_FIX_WIDTH|JUSTIFY_LEFT,0,0,100);
                    assayId=new FXLabel(_hframe01,oLanguage->getText("str_rdgmdi_nosel")+"\n",NULL,JUSTIFY_LEFT);
                FXHorizontalFrame *_hframe02=new FXHorizontalFrame(_vframe03,LAYOUT_FILL,0,0,0,0,0,0,0,0);
                    new FXLabel(_hframe02,oLanguage->getText("str_rdgmdi_tplInfo")+":",NULL,LAYOUT_FIX_WIDTH|JUSTIFY_LEFT,0,0,100);
                    assayTpl=new FXLabel(_hframe02,oLanguage->getText("str_rdgmdi_nosel")+"\n",NULL,JUSTIFY_LEFT);
                FXHorizontalFrame *_hframe03=new FXHorizontalFrame(_vframe03,LAYOUT_FILL,0,0,0,0,0,0,0,0);
                    new FXLabel(_hframe03,oLanguage->getText("str_rdgmdi_assayTech")+":",NULL,LAYOUT_FIX_WIDTH|JUSTIFY_LEFT,0,0,100);
                    assayTech=new FXLabel(_hframe03,oLanguage->getText("str_rdgmdi_nosel"),NULL,JUSTIFY_LEFT);

                    assayId->setTextColor(FXRGB(50,50,180));
                    assayTpl->setTextColor(FXRGB(50,50,180));
                    assayTech->setTextColor(FXRGB(50,50,180));

    tiStats=new FXTabItem(tbMain,oLanguage->getText("str_rdgmdi_stats"));
        FXVerticalFrame *_vframe1=new FXVerticalFrame(tbMain,FRAME_RAISED|FRAME_THICK|LAYOUT_FILL);
        FXHorizontalFrame *_hframe100=new FXHorizontalFrame(_vframe1,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,5,0);
        new FXLabel(_hframe100,oLanguage->getText("str_rdgmdi_primary_st"));
        lbPlates2=new FXLabel(_hframe100,(char*)NULL);

        FXVerticalFrame *_vframe11=new FXVerticalFrame(_vframe1,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
            plateTable2=new cColorTable(_vframe11,this,ID_PLATETABLE2,TABLE_COL_SIZABLE|TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT|LAYOUT_FILL,0,0,0,181);
            plateTable2->setGridColor(FXRGB(230,230,230));
            plateTable2->setTableSize(8,12);
            plateTable2->setDefColumnWidth(87);
            plateTable2->setColumnHeaderHeight(22);
            plateTable2->setRowHeaderWidth(81);
            plateTable2->setSelBackColor(FXRGB(100,100,140));
            plateTable2->setCellBorderWidth(1);
            plateTable2->setSelTextColor(0);
            plateTable2->setStippleColor(FXRGB(250,250,240));
            plateTable2->setScrollStyle(HSCROLLING_ON|VSCROLLING_ON|VSCROLLER_ALWAYS|SCROLLERS_TRACK);
        FXVerticalFrame *statsFrame=new FXVerticalFrame(_vframe1,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
        _gb10=new FXGroupBox(statsFrame,oLanguage->getText("str_rdgmdi_caseinfo"),GROUPBOX_TITLE_LEFT|LAYOUT_FILL_X|FRAME_GROOVE);
            FXVerticalFrame *_vframe12=new FXVerticalFrame(_gb10,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
            caseList2=new cColorTable(_vframe12,this,ID_CASELIST2,TABLE_NO_COLSELECT|LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT,0,0,0,210);
            caseList2->setTableSize(0,6);
            caseList2->setRowHeaderWidth(0);
            caseList2->setColumnText(0,oLanguage->getText("str_rdgmdi_cllayout"));
            caseList2->setColumnWidth(0,80);
            caseList2->setColumnText(1,oLanguage->getText("str_rdgmdi_clid"));
            caseList2->setColumnWidth(1,110);
            caseList2->setColumnText(2,oLanguage->getText("str_rdgmdi_comm"));
            caseList2->setColumnWidth(2,80);
            caseList2->setColumnText(3,oLanguage->getText("str_rdgmdi_clasy"));
            caseList2->setColumnWidth(3,80);
            caseList2->setColumnText(4,oLanguage->getText("str_rdgmdi_clrepl"));
            caseList2->setColumnWidth(4,70);
            caseList2->setColumnText(5,oLanguage->getText("str_rdgmdi_dil"));
            caseList2->setColumnWidth(5,50);
            caseList2->getColumnHeader()->setItemJustify(0,JUSTIFY_CENTER_X);
            caseList2->getColumnHeader()->setItemJustify(4,JUSTIFY_CENTER_X);
            caseList2->getColumnHeader()->setItemJustify(5,JUSTIFY_CENTER_X);
            caseList2->setGridColor(FXRGB(255,255,255));
            caseList2->setCellBorderWidth(1);
            caseList2->setColumnHeaderHeight(22);
            caseList2->showHorzGrid(true);
            caseList2->showVertGrid(false);
            caseList2->setSelTextColor(0);
            caseList2->setCellBorderColor(FXRGB(255,0,0));
            caseList2->setSelBackColor(FXRGB(180,180,255));
            caseList2->setScrollStyle(HSCROLLING_ON|VSCROLLER_ALWAYS|VSCROLLING_ON);

    tiGraph=new FXTabItem(tbMain,oLanguage->getText("str_rdgmdi_graph"));
        FXVerticalFrame *_vframe2=new FXVerticalFrame(tbMain,FRAME_RAISED|FRAME_THICK|LAYOUT_FILL);
            FXHorizontalFrame *_hframe22=new FXHorizontalFrame(_vframe2,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                new FXLabel(_hframe22,oLanguage->getText("str_rdgmdi_primary_gp"));
            FXVerticalFrame *_vframe22=new FXVerticalFrame(_vframe2,FRAME_SUNKEN|LAYOUT_FILL,0,0,0,0,0,0,0,0);
            chartWin=new FXScrollWindow(_vframe22,LAYOUT_FILL);
            chartFrame=new FXVerticalFrame(chartWin,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,1);
            chartFrame->setBackColor(FXRGB(190,190,190));

    FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe0,LAYOUT_FILL_Y|LAYOUT_RIGHT,0,0,0,0,5,0,0,0);
        new FXHorizontalSeparator(_vframe3,LAYOUT_FIX_HEIGHT,0,0,0,16);
        lbInv=new FXButton(_vframe3,(char*)NULL,NULL,this,ID_SHOWRULES,FRAME_RAISED|LAYOUT_FILL_X);
        lbInv->setBackColor(FXRGB(255,255,0));
        lbInv->setTextColor(FXRGB(255,0,0));
        lbInv->setJustify(JUSTIFY_CENTER_X);
        lbInv->setFont(new FXFont(getApp(),"Helvetica",9,FXFont::Bold));
        new FXButton(_vframe3,oLanguage->getText("str_but_close"),NULL,this,CMD_CLOSE,FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,80,0);
        new FXHorizontalSeparator(_vframe3,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT|LAYOUT_BOTTOM,0,0,0,10);

        mbReports=new FXButton(_vframe3,oLanguage->getText("str_rdgmdi_report").before('\t'),NULL,this,ID_REP_CA,FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,80,0);

        new FXButton(_vframe3,oLanguage->getText("str_rdgmdi_template"),NULL,this,CMD_TEMPLATE,FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,80,0);
        new FXHorizontalSeparator(_vframe3,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT|LAYOUT_BOTTOM,0,0,0,10);
        new FXButton(_vframe3,oLanguage->getText("str_rdgmdi_reread"),NULL,this,CMD_REREAD,FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,80,0);
        new FXButton(_vframe3,oLanguage->getText("str_rdgmdi_save"),NULL,this,CMD_SAVE,BUTTON_INITIAL|BUTTON_DEFAULT|FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,80,0);
    charts=NULL;
    tCasesCount=0;
    readingsWindow=oWinMain->getReadings()->no();
    oWinMain->getReadings()->insert(FXStringFormat("%c%c",readingsWindow/256+1,readingsWindow%256+1).text(),this);
}

cMDIReadings::~cMDIReadings()
{
    oWinMain->getReadings()->remove(FXStringFormat("%c%c",readingsWindow/256+1,readingsWindow%256+1).text());
    if(casesSets)
    {
        for(int i=0;i<tSetsCount;i++)
        {
            if(casesSets[i].assayRes)
                delete casesSets[i].assayRes;
            if(casesSets[i].templateRes)
                delete casesSets[i].templateRes;
            for(int j=0;j<casesSets[i].casesCount;j++)
            {
                free(casesSets[i].cases[j].data);
                free(casesSets[i].cases[j].controlsData);
            }
            free(casesSets[i].cases);
        }
        free(casesSets);
        for(int i=0;i<plateTable1->getNumRows();i++)
            for(int j=0;j<plateTable1->getNumColumns();j++)
                if(plateTable1->getItemData(i,j)!=NULL)
                    delete (cLocInfo*)plateTable1->getItemData(i,j);
    }
    for(int i=0;i<tSetsCount;i++)
        delete charts[i];
    if(charts)
    {
        free(charts);
        delete edit;
        delete mbEdit;
    }
}

void cMDIReadings::create()
{
    hide();
    cMDIChild::create();
}

FXbool cMDIReadings::canClose(void)
{
    plateTable1->acceptInput(true);
    plateTable2->acceptInput(true);
    if(!saved)
    {
        setFocus();
        int res=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),oLanguage->getText("str_qsave").text());
        switch(res)
        {
            case MBOX_CLICKED_YES:
                if(!saveData())
                    return false;
                break;
            case MBOX_CLICKED_CANCEL:
                return false;
            default:
                break;
        }
    }
    return true;
}

FXbool cMDIReadings::newReading(cDataResult *prResAssay,cDataResult *prResTemplate,int prPlateCount,double **prPlateData,FXString prLot, FXString prExpirationDate)
{
    if(!prResAssay || !prResTemplate || !prPlateCount || !prPlateData)
        return false;

    casesSets=(sCasesSet*)malloc(sizeof(sCasesSet));
    if(!casesSets)
    {
        tPlatesCount=0;
        tSetsCount=0;
        tCasesCount=0;
        saved=true;
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return false;
    }

    tSetsCount=1;
    tPlatesCount=casesSets[0].platesCount=prPlateCount;
    FXString __cases_defs=prResTemplate->getCellString(0,4);
    tCasesCount=casesSets[0].casesCount=FXIntVal(__cases_defs.before('\n'));

    casesSets[0].cases=(sCaseEntry*)malloc(tCasesCount*sizeof(sCaseEntry));
    if(!casesSets[0].cases)
    {
        tPlatesCount=0;
        tSetsCount=0;
        tCasesCount=0;
        saved=true;
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return false;
    }

    FXchar ftime[100];
    time_t curtime;
    struct tm *loctime;
    curtime=time(NULL);
    loctime=localtime(&curtime);
    strftime(ftime,100,"%Y-%m-%d",loctime);

    casesSets[0].templateRes=prResTemplate;
    casesSets[0].controls_replicates=prResTemplate->getCellInt(0,2);

    casesSets[0].assayRes=prResAssay;
    FXString *stime=new FXString(ftime);
    FXString *stech=new FXString(oWinMain->getTechnician());
    FXString *slot=new FXString(prLot);
    FXString *sexp=new FXString(prExpirationDate);
    FXString *scomments=new FXString();
    FXString *sassay=new FXString(prResAssay->getCellString(0,0));
    FXString *stpl=new FXString(prResTemplate->getCellString(0,0));
    FXString *scdefs=new FXString(prResTemplate->getCellString(0,3));
    curtime=time(NULL);
    loctime=localtime(&curtime);
    strftime(ftime,100,"%s",loctime);
    FXString *sroid=new FXString(ftime);
    srand(curtime);
    *sroid=*sroid+FXStringVal(rand());

    casesSets[0].cal=new FXString(prResAssay->getCellString(0,6));
    casesSets[0].unit=new FXString(prResAssay->getCellString(0,7));

    casesSets[0].assay=sassay;
    casesSets[0].assaytitle=new FXString(prResAssay->getCellString(0,1));

    for(int i=0;i<casesSets[0].casesCount;i++)
    {
        casesSets[0].cases[i].parentSet=0;
        __cases_defs=__cases_defs.after('\n');

        casesSets[0].cases[i].id=new FXString(__cases_defs.before('\t'));
        __cases_defs=__cases_defs.after('\t');
        casesSets[0].cases[i].old_id=new FXString(*casesSets[0].cases[i].id);

        casesSets[0].cases[i].commodity=new FXString(__cases_defs.before('\t'));
        __cases_defs=__cases_defs.after('\t');

        casesSets[0].cases[i].readingDate=stime;
        casesSets[0].cases[i].technician=stech;
        casesSets[0].cases[i].lot=slot;
        casesSets[0].cases[i].reading_oid=sroid;
        casesSets[0].cases[i].tpl=stpl;
        casesSets[0].cases[i].controls_defs=scdefs;
        casesSets[0].cases[i].expirationDate=sexp;
        casesSets[0].cases[i].comments=scomments;
        casesSets[0].cases[i].assay=sassay;

        casesSets[0].cases[i].replicates=FXIntVal(__cases_defs.before('\t'));
        __cases_defs=__cases_defs.after('\t');

        casesSets[0].cases[i].factor=FXIntVal(__cases_defs.before('\t'));
        __cases_defs=__cases_defs.after('\t');
    }

    if(charts)
        free(charts);

    FXString __controls_defs(*scdefs);
    __controls_defs=__controls_defs.after('\n');
    casesSets[0].controlsPerPlate=0;
    int value;
    do{
        value=FXIntVal(__controls_defs.before('\t'));
        __controls_defs=__controls_defs.after('\t');
        if(value!=0 && value!=-100) {
            casesSets[0].controlsLayout[casesSets[0].controlsPerPlate++]=value;
        }
        else
            break;
    }while(true);

    FXString __asy_controls_defs=prResAssay->getCellString(0,5);
    value=0;
    value = FXIntVal(__asy_controls_defs.before('\n'));
    __asy_controls_defs=__asy_controls_defs.after('\n');

    casesSets[0].isSc = value;

    for(value = 0; value < 50; value++) {
        casesSets[0].asyControls[value]=-1;
        casesSets[0].asyControlsBBO[value]=-1;
    }

    value = 0;
    while(!__asy_controls_defs.empty())
    {
        casesSets[0].asyControls[value]=FXDoubleVal(__asy_controls_defs.before('\t'));
        __asy_controls_defs=__asy_controls_defs.after('\t');

        casesSets[0].asyControlsBBO[value]=FXIntVal(__asy_controls_defs.before('\n'));
        __asy_controls_defs=__asy_controls_defs.after('\n');
        value++;
    }
    casesSets[0].asyControlsCount = value;

    this->sortControls(casesSets[0].controlsLayout,casesSets[0].controlsPerPlate);

    int casei=0;
    int platei=0;
    int stocki=96*prPlateCount;
    int datai=0;
    int casepi=0;
    int samplei=0;
    bool newp=true;
    double var;

    while(stocki>0)
    {
        if(datai>=96)
        {
            newp=true;
            datai=0;
            platei++;
        }

        if(!isControlPosition(datai,casesSets[0].controlsLayout,casesSets[0].controlsPerPlate, casesSets[0].controls_replicates==2))
        {
            if(samplei==0)
            {
                casesSets[0].cases[casei].data=(double*)malloc((casesSets[0].cases[casei].replicates+1)*sizeof(double));

                if(!casesSets[0].cases[casei].data)
                {
                    tPlatesCount=0;
                    tSetsCount=0;
                    tCasesCount=0;
                    saved=true;
                    FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                    return false;
                }
                casesSets[0].cases[casei].startCell=datai;
                casesSets[0].cases[casei].startPlate=platei;
            }
            if(samplei==0 || newp)
            {
                casepi++;
                casesSets[0].cases[casei].plateSpanCount=casepi;
                casesSets[0].cases[casei].controlsCount=casepi*casesSets[0].controlsPerPlate*casesSets[0].controls_replicates;
                casesSets[0].cases[casei].controlsData=(double*)realloc(samplei==0?NULL:casesSets[0].cases[casei].controlsData,
                                                                        casesSets[0].cases[casei].controlsCount*sizeof(double));
                if(!casesSets[0].cases[casei].controlsData)
                {
                    tPlatesCount=0;
                    tSetsCount=0;
                    tCasesCount=0;
                    saved=true;
                    FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                    return false;
                }

                appendControls(casesSets[0].cases[casei].controlsData+(casepi-1)*casesSets[0].controlsPerPlate*casesSets[0].controls_replicates,
                               prPlateData[platei],
                               casesSets[0].controlsLayout,
                               casesSets[0].controlsPerPlate,
                               casesSets[0].controls_replicates==2);
            }

            var=prPlateData[platei][platePosition(datai)];
            casesSets[0].cases[casei].data[samplei]=var<0?0.000:var;
            samplei++;
            if(samplei>=(casesSets[0].cases[casei].replicates+1))
            {
                samplei=0;
                casepi=0;
                casei++;
            }
            if(casei>=tCasesCount)
                break;

            newp=false;
        }
        stocki--;
        datai++;
    }

    caseList1->removeRows(0,caseList1->getNumRows());
    caseList1->insertRows(0,tCasesCount);
    caseList1->layout();
    caseList2->removeRows(0,caseList2->getNumRows());
    caseList2->insertRows(0,tCasesCount);
    caseList2->layout();

    for(int i=0;i<tCasesCount;i++)
    {
        for(int j=0;j<caseList1->getNumColumns();j++)
        {
            caseList1->setItemJustify(i,j,JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
            caseList1->setCellColor(i,j,cColorsManager::getColor(i,-30));
            caseList1->setCellEditable(i,j,false);
            caseList2->setItemJustify(i,j,JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
            caseList2->setCellColor(i,j,cColorsManager::getColor(i,-30));
            caseList2->setCellEditable(i,j,false);
        }
        caseList1->setCellEditable(i,1,true);
        caseList2->setCellEditable(i,1,true);
        caseList1->setCellEditable(i,2,true);
        caseList2->setCellEditable(i,2,true);

        caseList1->setCellColor(i,1,cColorsManager::getColor(i));
        caseList1->setCellColor(i,2,cColorsManager::getColor(i));
        caseList1->setItemJustify(i,1,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
        caseList1->setItemJustify(i,2,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
        caseList1->setItemJustify(i,3,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
        caseList2->setCellColor(i,1,cColorsManager::getColor(i));
        caseList2->setCellColor(i,2,cColorsManager::getColor(i));
        caseList2->setItemJustify(i,1,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
        caseList2->setItemJustify(i,2,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
        caseList2->setItemJustify(i,3,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);

        caseList1->setItemData(i,0,&casesSets[0].cases[i]);
        caseList2->setItemData(i,0,&casesSets[0].cases[i]);

        caseList1->setItemText(i,0,FXStringFormat("(%s) P%d:",sampleKey(i).text(),casesSets[0].cases[i].startPlate+1)+
                               displayPosition(casesSets[0].cases[i].startCell));
        caseList2->setItemText(i,0,caseList1->getItemText(i,0));

        caseList1->setItemText(i,1,*casesSets[0].cases[i].id);
        caseList2->setItemText(i,1,*casesSets[0].cases[i].id);

        caseList1->setItemText(i,2,*casesSets[0].cases[i].commodity);
        caseList2->setItemText(i,2,*casesSets[0].cases[i].commodity);

        caseList1->setItemText(i,3,*casesSets[0].assay);
        caseList2->setItemText(i,3,*casesSets[0].assay);

        caseList1->setItemText(i,4,FXStringFormat("%d",casesSets[0].cases[i].replicates));
        caseList2->setItemText(i,4,FXStringFormat("%d",casesSets[0].cases[i].replicates));

        caseList1->setItemText(i,5,FXStringFormat("%d",casesSets[0].cases[i].factor));
        caseList2->setItemText(i,5,FXStringFormat("%d",casesSets[0].cases[i].factor));
    }

    lbPlates->setText(FXStringFormat(" - %d %s",tPlatesCount,oLanguage->getText("str_rdgmdi_plates").text()));
    lbPlates2->setText(lbPlates->getText());
    _gb0->setText(FXStringFormat("%s - %d %s",oLanguage->getText("str_rdgmdi_caseinfo").text(),tCasesCount,oLanguage->getText("str_rdgmdi_caseslsx").text()));
    _gb10->setText(FXStringFormat("%s - %d %s",oLanguage->getText("str_rdgmdi_caseinfo").text(),tCasesCount,oLanguage->getText("str_rdgmdi_caseslsx").text()));

    plateTable1->setTableSize(8*tPlatesCount,12);
    plateTable1->layout();
    plateTable2->setTableSize(0,0);
    selectCase(0);
    updateData();
    onRszPlateTable1(NULL,0,NULL);
    onRszCaseList1(NULL,0,NULL);
    onRszCaseList2(NULL,0,NULL);
    tiPrimary->setFocus();
    newdata=true;
    show();
    return true;
}

FXbool cMDIReadings::loadReadings(cSortList &prCases)
{
    FXString criteria="";
    int j=0;
    for(int i=0;i<prCases.getNumItems();i++)
        if(prCases.isItemSelected(i))
        {
            if(j>0)
                criteria=criteria+" OR ";
            criteria=criteria+"(assay_oid='"+prCases.getItemText(i).before('\t')+"' AND "+"id='"+prCases.getItemText(i).after('\t').before('\t')+"')";
            j++;
        }
    if(!j)
        return false;
    cDataResult *resr=oDataLink->execute("SELECT DISTINCT reading_oid FROM t_gncases WHERE "+criteria+" ORDER BY assay_oid ASC;");
    tSetsCount=resr->getRowCount();
    if(!tSetsCount)
        return false;
    cDataResult *resc;
    casesSets=(sCasesSet*)malloc(tSetsCount*sizeof(sCasesSet));
    if(!casesSets)
    {
        tPlatesCount=0;
        tSetsCount=0;
        tCasesCount=0;
        saved=true;
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return false;
    }
    tCasesCount=0;
    tPlatesCount=0;
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        FXString __reading_oid(resr->getCellString(setsi,0));
        resc=oDataLink->execute("SELECT * FROM t_gncases WHERE ("+criteria+") AND reading_oid='"+__reading_oid+"' ORDER BY (startPlate*100+startCell) ASC;");
        casesSets[setsi].casesCount=resc->getRowCount();
        tCasesCount+=casesSets[setsi].casesCount;
        casesSets[setsi].cases=(sCaseEntry*)malloc(casesSets[setsi].casesCount*sizeof(sCaseEntry));
        if(!casesSets[setsi].cases)
        {
            tPlatesCount=0;
            tSetsCount=0;
            tCasesCount=0;
            saved=true;
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
            return false;
        }

        FXString __assay(resc->getCellString(0,4));
        FXString __template(resc->getCellString(0,10));
        casesSets[setsi].assayRes=oDataLink->execute("SELECT * FROM t_assays WHERE id='"+__assay+"';");
        casesSets[setsi].templateRes=oDataLink->execute("SELECT * FROM t_templates WHERE id='"+__template+"';");
        casesSets[setsi].controls_replicates=casesSets[setsi].templateRes->getCellInt(0,2);

        casesSets[setsi].cal=new FXString(casesSets[setsi].assayRes->getCellString(0,6));
        casesSets[setsi].unit=new FXString(casesSets[setsi].assayRes->getCellString(0,7));
        casesSets[setsi].assay=new FXString(__assay);
        casesSets[setsi].assaytitle=new FXString(casesSets[setsi].assayRes->getCellString(0,1));

        FXString __lot(resc->getCellString(0,5));
        FXString __expDate(resc->getCellString(0,6));

        FXString __controls_defs(resc->getCellString(0,14));
        __controls_defs=__controls_defs.after('\n');
        casesSets[setsi].controlsPerPlate=0;
        int value;
        do{
            value=FXIntVal(__controls_defs.before('\t'));
            __controls_defs=__controls_defs.after('\t');
            if(value!=0 && value!=-100)
                casesSets[setsi].controlsLayout[casesSets[setsi].controlsPerPlate++]=value;
            else
                break;
        }while(true);

        FXString __asy_controls_defs=casesSets[setsi].assayRes->getCellString(0,5);
        value=0;
        value = FXIntVal(__asy_controls_defs.before('\n'));
        __asy_controls_defs=__asy_controls_defs.after('\n');

        casesSets[setsi].isSc = value;

        for(value = 0; value < 50; value++) {
            casesSets[setsi].asyControls[value]=-1;
            casesSets[setsi].asyControlsBBO[value]=-1;
        }

        value = 0;
        while(!__asy_controls_defs.empty())
        {
            casesSets[setsi].asyControls[value]=FXDoubleVal(__asy_controls_defs.before('\t'));
            __asy_controls_defs=__asy_controls_defs.after('\t');

            casesSets[setsi].asyControlsBBO[value]=FXIntVal(__asy_controls_defs.before('\n'));
            __asy_controls_defs=__asy_controls_defs.after('\n');
            value++;
        }
        casesSets[setsi].asyControlsCount = value;

        this->sortControls(casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate);

        casesSets[setsi].platesCount=0;
        FXDict *pl=new FXDict();
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++)
        {
            casesSets[setsi].cases[casei].startCell=resc->getCellInt(casei,12);
            casesSets[setsi].cases[casei].startPlate=resc->getCellInt(casei,11);
            casesSets[setsi].cases[casei].plateSpanCount=resc->getCellInt(casei,13);

            int ps=casesSets[setsi].cases[casei].startPlate,pe=ps+casesSets[setsi].cases[casei].plateSpanCount-1;
            FXString key;
            for(int i=ps;i<=pe;i++)
                pl->insert(FXStringFormat("%c%c",i/256+1,i%256+1).text(),NULL);

            casesSets[setsi].cases[casei].id=new FXString(resc->getCellString(casei,0));
            casesSets[setsi].cases[casei].old_id=new FXString(*casesSets[setsi].cases[casei].id);
            casesSets[setsi].cases[casei].reading_oid=new FXString(__reading_oid);
            casesSets[setsi].cases[casei].replicates=resc->getCellInt(casei,8);
            casesSets[setsi].cases[casei].factor=resc->getCellInt(casei,9);

            casesSets[setsi].cases[casei].tpl=new FXString(__template);
            casesSets[setsi].cases[casei].controls_defs=new FXString(resc->getCellString(casei,14));
            casesSets[setsi].cases[casei].readingDate=new FXString(resc->getCellString(casei,2));
            casesSets[setsi].cases[casei].technician=new FXString(resc->getCellString(casei,3));
            casesSets[setsi].cases[casei].lot=new FXString(__lot);
            casesSets[setsi].cases[casei].expirationDate=new FXString(__expDate);
            casesSets[setsi].cases[casei].commodity=new FXString(resc->getCellString(casei,7));
            casesSets[setsi].cases[casei].comments=new FXString(resc->getCellString(casei,16));
            casesSets[setsi].cases[casei].assay=casesSets[setsi].assay;

            casesSets[setsi].cases[casei].parentSet=setsi;

            FXString __data(resc->getCellString(casei,15));
            FXString __controls=__data.before('\n');
            __data=__data.after('\n');

            casesSets[setsi].cases[casei].controlsCount=casesSets[setsi].cases[casei].plateSpanCount*casesSets[setsi].controls_replicates*casesSets[setsi].controlsPerPlate;
            casesSets[setsi].cases[casei].controlsData=(double*)malloc(casesSets[setsi].cases[casei].controlsCount*sizeof(double));
            if(!casesSets[setsi].cases[casei].controlsData)
            {
                tPlatesCount=0;
                tSetsCount=0;
                tCasesCount=0;
                saved=true;
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                return false;
            }
            for(int i=0;i<casesSets[setsi].cases[casei].controlsCount;i++)
            {
                casesSets[setsi].cases[casei].controlsData[i]=FXFloatVal(__controls.before(' '));
                __controls=__controls.after(' ');
            }

            int ct=casesSets[setsi].cases[casei].replicates+1;
            casesSets[setsi].cases[casei].data=(double*)malloc(ct*sizeof(double));
            if(!casesSets[setsi].cases[casei].data)
            {
                tPlatesCount=0;
                tSetsCount=0;
                tCasesCount=0;
                saved=true;
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                return false;
            }
            for(int i=0;i<ct;i++)
            {
                casesSets[setsi].cases[casei].data[i]=FXFloatVal(__data.before(' '));
                __data=__data.after(' ');
            }
        }
        casesSets[setsi].platesCount=pl->no();
        delete pl;
        tPlatesCount+=casesSets[setsi].platesCount;
        resc->free();
    }
    resr->free();
    caseList1->removeRows(0,caseList1->getNumRows());
    caseList1->insertRows(0,tCasesCount);
    caseList1->layout();
    caseList2->removeRows(0,caseList2->getNumRows());
    caseList2->insertRows(0,tCasesCount);
    caseList2->layout();

    if(charts)
        free(charts);

    int i=0;
    int p=0;
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        int d=0,od=0;
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++,i++)
        {
            for(int j=0;j<caseList1->getNumColumns();j++)
            {
                caseList1->setItemJustify(i,j,JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
                caseList1->setCellColor(i,j,cColorsManager::getColor(i,-30));
                caseList1->setCellEditable(i,j,false);
                caseList2->setItemJustify(i,j,JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
                caseList2->setCellColor(i,j,cColorsManager::getColor(i,-30));
                caseList2->setCellEditable(i,j,false);
            }
            caseList1->setCellEditable(i,1,true);
            caseList2->setCellEditable(i,1,true);
            caseList1->setCellEditable(i,2,true);
            caseList2->setCellEditable(i,2,true);

            caseList1->setCellColor(i,1,cColorsManager::getColor(i));
            caseList1->setCellColor(i,2,cColorsManager::getColor(i));
            caseList1->setItemJustify(i,1,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
            caseList1->setItemJustify(i,2,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
            caseList1->setItemJustify(i,3,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
            caseList2->setCellColor(i,1,cColorsManager::getColor(i));
            caseList2->setCellColor(i,2,cColorsManager::getColor(i));
            caseList2->setItemJustify(i,1,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
            caseList2->setItemJustify(i,2,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
            caseList2->setItemJustify(i,3,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);

            caseList1->setItemData(i,0,&casesSets[setsi].cases[casei]);
            caseList2->setItemData(i,0,&casesSets[setsi].cases[casei]);

            caseList1->setItemText(i,1,*casesSets[setsi].cases[casei].id);
            caseList2->setItemText(i,1,*casesSets[setsi].cases[casei].id);

            caseList1->setItemText(i,2,*casesSets[setsi].cases[casei].commodity);
            caseList2->setItemText(i,2,*casesSets[setsi].cases[casei].commodity);

            caseList1->setItemText(i,3,*casesSets[setsi].assay);
            caseList2->setItemText(i,3,*casesSets[setsi].assay);

            caseList1->setItemText(i,4,FXStringFormat("%d",casesSets[setsi].cases[casei].replicates));
            caseList2->setItemText(i,4,FXStringFormat("%d",casesSets[setsi].cases[casei].replicates));

            caseList1->setItemText(i,5,FXStringFormat("%d",casesSets[setsi].cases[casei].factor));
            caseList2->setItemText(i,5,FXStringFormat("%d",casesSets[setsi].cases[casei].factor));

            d=casesSets[setsi].cases[casei].startPlate;
            if(d!=od)
            {
                p++;
                od=d;
            }
            caseList1->setItemText(i,0,FXStringFormat("(%s) P%d:",sampleKey(i).text(),p+1)+
                                   displayPosition(casesSets[setsi].cases[casei].startCell));
            caseList2->setItemText(i,0,caseList1->getItemText(i,0));
            p+=casesSets[setsi].cases[casei].plateSpanCount-1;
        }
        p++;
    }

    lbPlates->setText(FXStringFormat(" - %d %s",tPlatesCount,oLanguage->getText("str_rdgmdi_plates").text()));
    lbPlates2->setText(lbPlates->getText());
    _gb0->setText(FXStringFormat("%s - %d %s",oLanguage->getText("str_rdgmdi_caseinfo").text(),tCasesCount,oLanguage->getText("str_rdgmdi_caseslsx").text()));
    _gb10->setText(FXStringFormat("%s - %d %s",oLanguage->getText("str_rdgmdi_caseinfo").text(),tCasesCount,oLanguage->getText("str_rdgmdi_caseslsx").text()));

    plateTable1->setTableSize(8*tPlatesCount,12);

    plateTable1->layout();
    plateTable2->setTableSize(0,0);
    newdata=false;
    saved=true;
    selectCase(0);
    onRszPlateTable1(NULL,0,NULL);
    onRszCaseList1(NULL,0,NULL);
    onRszCaseList2(NULL,0,NULL);
    tiPrimary->setFocus();
    show();

    return true;
}

long cMDIReadings::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{

    if(canClose())
    {
        close();
        return 1;
    }
    return 0;
}

long cMDIReadings::onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    return 1;
}

long cMDIReadings::onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData)
{
    saveData();
    return 1;
}

long cMDIReadings::onCopyPlateTable(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(tbMain->getCurrent()==0)
    {
        plateTable1->handle(this,FXSEL(SEL_COMMAND,FXTable::ID_COPY_SEL),NULL);
    }
    else if(tbMain->getCurrent()==1)
    {
        plateTable2->handle(this,FXSEL(SEL_COMMAND,FXTable::ID_COPY_SEL),NULL);
    }
    return 1;
}

long cMDIReadings::onPastePlateTable(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(tbMain->getCurrent()==0)
    {
        plateTable1->handle(this,FXSEL(SEL_COMMAND,FXTable::ID_PASTE_SEL),NULL);
        onCmdPlateTable1(NULL,0,NULL);
    }
    else if(tbMain->getCurrent()==1)
    {
        plateTable2->handle(this,FXSEL(SEL_COMMAND,FXTable::ID_PASTE_SEL),NULL);
        onCmdPlateTable2(NULL,0,NULL);
    }
    return 1;
}

long cMDIReadings::onSelallPlateTable(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(tbMain->getCurrent()==0)
    {
        plateTable1->handle(this,FXSEL(SEL_COMMAND,FXTable::ID_SELECT_ALL),NULL);
    }
    else if(tbMain->getCurrent()==1)
    {
        plateTable2->handle(this,FXSEL(SEL_COMMAND,FXTable::ID_SELECT_ALL),NULL);
    }
    return 1;
}

long cMDIReadings::onRszPlateTable1(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int tw=plateTable1->getWidth()-plateTable1->verticalScrollBar()->getWidth();
    int cw=tw/13;
    int hw=tw-(cw*13)+cw;
    if(cw<30)
        cw=hw=30;
    plateTable1->setRowHeaderWidth(hw);
    for(tw=0;tw<12;tw++)
        plateTable1->setColumnWidth(tw,cw);
    return 1;
}

long cMDIReadings::onUpdSelPlateTable1(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int r=*((int*)prData),c=*((int*)prData+1);
    int sr,ssr=plateTable1->getSelStartRow(),ser=plateTable1->getSelEndRow();
    if(ssr<0 || ser<0)
        return 1;
    sr=ssr;
    if(ssr!=ser)
        sr=(ssr==plateTable1->getAnchorRow()?ser:ssr);
    int sc,ssc=plateTable1->getSelStartColumn(),sec=plateTable1->getSelEndColumn();
    if(ssc<0 || sec<0)
        return 1;
    sc=ssc;
    if(ssc!=sec)
        sc=(ssc==plateTable1->getAnchorColumn()?sec:ssc);
    for(int i=0;i<plateTable1->getNumRows();i++)
        plateTable1->getRowHeader()->setItemPressed(i,false);
    for(int i=0;i<12;i++)
        plateTable1->getColumnHeader()->setItemPressed(i,false);
    plateTable1->getRowHeader()->setItemPressed(sr,true);
    plateTable1->getColumnHeader()->setItemPressed(sc,true);

    if(r!=sr || c!=sc)
        return 1;

    if(!plateTable1->getItemData(sr,sc))
    {
        selectCase(-1);
        return 1;
    }

    selectCase(((cLocInfo*)plateTable1->getItemData(sr,sc))->caseIndex,sr/8);
    locateCaseLists(((cLocInfo*)plateTable1->getItemData(sr,sc))->caseIndex);
    locatePlateTable2(((cLocInfo*)plateTable1->getItemData(sr,sc))->caseIndex);
    return 1;
}

long cMDIReadings::onUpdSelPlateTable2(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int r=*((int*)prData),c=*((int*)prData+1);
    int sr,ssr=plateTable2->getSelStartRow(),ser=plateTable2->getSelEndRow();
    if(ssr<0 || ser<0)
        return 1;
    sr=ssr;
    if(ssr!=ser)
        sr=(ssr==plateTable2->getAnchorRow()?ser:ssr);
    int sc,ssc=plateTable2->getSelStartColumn(),sec=plateTable2->getSelEndColumn();
    if(ssc<0 || sec<0)
        return 1;
    sc=ssc;
    if(ssc!=sec)
        sc=(ssc==plateTable2->getAnchorColumn()?sec:ssc);

    if((r!=sr) || (c!=sc))
        return 1;

    if(!plateTable2->getItemData(sr,0))
    {
        selectCase(-1);
        return 1;
    }
    selectCase(((cLocInfo*)plateTable2->getItemData(sr,0))->caseIndex,sr/8);
    locateCaseLists(((cLocInfo*)plateTable2->getItemData(sr,0))->caseIndex);
    locatePlateTable1(((cLocInfo*)plateTable2->getItemData(sr,0))->caseIndex);
    return 1;
}

long cMDIReadings::onRszCaseList1(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int tw=caseList1->getWidth()-caseList1->verticalScrollBar()->getWidth();
    caseList1->setColumnWidth(5,tw-420);
    return 1;
}

long cMDIReadings::onCmdSelCaseList1(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int sr,ssr=caseList1->getSelStartRow(),ser=caseList1->getSelEndRow();
    if(ssr<0 || ser<0)
        return 1;
    sr=ssr;
    if(ssr!=ser)
        sr=(ssr==caseList1->getAnchorRow()?ser:ssr);
    for(int j=0;j<caseList1->getNumRows();j++)
        for(int i=1;i<caseList1->getNumColumns();i++)
        {
            caseList1->setItemBorders(j,i,0);
            caseList2->setItemBorders(j,i,0);
        }
    for(int i=1;i<caseList1->getNumColumns();i++)
    {
        //caseList1->setCellColor(sr,i,cColorsManager::getColor(sr));
        caseList1->setItemBorders(sr,i,FXTableItem::TBORDER|FXTableItem::BBORDER);
        //caseList2->setCellColor(sr,i,cColorsManager::getColor(sr));
        caseList2->setItemBorders(sr,i,FXTableItem::TBORDER|FXTableItem::BBORDER);
    }
    caseList1->setItemBorders(sr,1,FXTableItem::LBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    caseList1->setItemBorders(sr,caseList1->getNumColumns()-1,FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    caseList2->setItemBorders(sr,1,FXTableItem::LBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    caseList2->setItemBorders(sr,caseList1->getNumColumns()-1,FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    selectCase(sr);
    locateCaseLists(sr);
    locatePlateTable1(sr);
    locatePlateTable2(sr);
    return 1;
}

long cMDIReadings::onRszCaseList2(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int tw=caseList2->getWidth()-caseList2->verticalScrollBar()->getWidth();
    caseList2->setColumnWidth(5,tw-420);
    return 1;
}

long cMDIReadings::onCmdSelCaseList2(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int sr,ssr=caseList2->getSelStartRow(),ser=caseList2->getSelEndRow();
    if(ssr<0 || ser<0)
        return 1;
    sr=ssr;
    if(ssr!=ser)
        sr=(ssr==caseList2->getAnchorRow()?ser:ssr);
    for(int j=0;j<caseList1->getNumRows();j++)
        for(int i=1;i<caseList1->getNumColumns();i++)
        {
            caseList1->setItemBorders(j,i,0);
            caseList2->setItemBorders(j,i,0);
        }
    for(int i=1;i<caseList1->getNumColumns();i++)
    {
        //caseList1->setCellColor(sr,i,cColorsManager::getColor(sr));
        caseList1->setItemBorders(sr,i,FXTableItem::TBORDER|FXTableItem::BBORDER);
        //caseList2->setCellColor(sr,i,cColorsManager::getColor(sr));
        caseList2->setItemBorders(sr,i,FXTableItem::TBORDER|FXTableItem::BBORDER);
    }
    caseList1->setItemBorders(sr,1,FXTableItem::LBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    caseList1->setItemBorders(sr,caseList1->getNumColumns()-1,FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    caseList2->setItemBorders(sr,1,FXTableItem::LBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    caseList2->setItemBorders(sr,caseList1->getNumColumns()-1,FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    selectCase(sr);
    locateCaseLists(sr);
    locatePlateTable1(sr);
    locatePlateTable2(sr);
    return 1;
}

long cMDIReadings::onCmdCaseList1(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(caseList1->getNumRows()<=0)
        return 1;
    int r=*((int*)prData),c=*((int*)prData+1);
    sCaseEntry *cas=NULL;
    FXString s1,s2,s=caseList1->getItemText(r,c),err="";
    switch(c)
    {
        case 1:
            if(s.empty())
            {
                caseList1->setItemText(r,c,cCaseManager::getNewID());
                break;
            }
            for(int i=0;i<caseList1->getNumRows();i++)
                if(i!=r && caseList1->getItemText(i,1)==s)
                {
                    err="str_invalid_duplid";
                    break;
                }
            for(int i=0;i<s.length();i++)
                if(!isalnum(s.at(i)) && s.at(i)!=' ' && s.at(i)!='#' && s.at(i)!='-' && s.at(i)!='/')
                {
                    err="str_invalid_numlet";
                    break;
                }
            cas=(sCaseEntry*)caseList1->getItemData(r,0);
            if(cas->id) delete cas->id;
            cas->id = new FXString(caseList1->getItemText(r,c));
            break;
        case 2:
            if(s.empty())
            {
                caseList1->setItemText(r,c,"---");
                break;
            }
            for(int i=0;i<s.length();i++)
                if(!isalnum(s.at(i)) && s.at(i)!=' ' && s.at(i)!='#' && s.at(i)!='-' && s.at(i)!='/')
                {
                    err="str_invalid_numlet";
                    break;
                }
            cas=(sCaseEntry*)caseList1->getItemData(r,0);
            if(cas->commodity) delete cas->commodity;
            cas->commodity = new FXString(caseList1->getItemText(r,c));
            break;
    }

    if(!err.empty())
    {
        caseList1->setItemText(r,c,"");
        caseList1->startInput(r,c);
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),
                              (oLanguage->getText("str_invalid")+"\n"+
                               oLanguage->getText(err)).text());
        return 1;
    }

    caseList2->setItemText(r,c,caseList1->getItemText(r,c));

    updateData();
    return 1;
}

long cMDIReadings::onCmdCaseList2(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(caseList2->getNumRows()<=0)
        return 1;
    int r=*((int*)prData),c=*((int*)prData+1);
    FXString s1,s2,s=caseList2->getItemText(r,c),err="";
    switch(c)
    {
        case 1:
            if(s.empty())
            {
                caseList2->setItemText(r,c,cCaseManager::getNewID());
                break;
            }
            for(int i=0;i<caseList2->getNumRows();i++)
                if(i!=r && caseList2->getItemText(i,1)==s)
                {
                    err="str_invalid_duplid";
                    break;
                }
            for(int i=0;i<s.length();i++)
                if(!isalnum(s.at(i)) && s.at(i)!=' ' && s.at(i)!='#' && s.at(i)!='-' && s.at(i)!='/')
                {
                    err="str_invalid_numlet";
                    break;
                }
            break;
        case 2:
            if(s.empty())
            {
                caseList2->setItemText(r,c,"---");
                break;
            }
            for(int i=0;i<s.length();i++)
                if(!isalnum(s.at(i)) && s.at(i)!=' ' && s.at(i)!='#' && s.at(i)!='-' && s.at(i)!='/')
                {
                    err="str_invalid_numlet";
                    break;
                }
            break;
    }

    if(!err.empty())
    {
        caseList2->setItemText(r,c,"");
        caseList2->startInput(r,c);
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),
                              (oLanguage->getText("str_invalid")+"\n"+
                               oLanguage->getText(err)).text());
        return 1;
    }

    caseList1->setItemText(r,c,caseList2->getItemText(r,c));

    updateData();
    return 1;
}

long cMDIReadings::onCmdPlateTable1(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    int r,c,rs,cs,re,ce;
    if(prData)
    {
        rs=re=*(int*)prData;
        cs=ce=*((int*)prData+1);
    }
    else
    {
        rs=FXMIN(plateTable1->getSelStartRow(),plateTable1->getSelEndRow());
        cs=FXMIN(plateTable1->getSelStartColumn(),plateTable1->getSelEndColumn());
        re=FXMAX(plateTable1->getSelStartRow(),plateTable1->getSelEndRow());
        ce=FXMAX(plateTable1->getSelStartColumn(),plateTable1->getSelEndColumn());
    }
    if(rs==-1 || cs==-1 || re==-1 || ce==-1)
    {
        rs=cs=0;
        re=plateTable1->getNumRows()-1;
        ce=plateTable1->getNumColumns()-1;
    }
    for(r=rs;r<=re;r++)
        for(c=cs;c<=ce;c++)
        {
            if(!plateTable1->getItemData(r,c))
                continue;
            FXString s=plateTable1->getItemText(r,c);
			if(s.empty())
				{
					 FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),
										  oLanguage->getText("str_invalid").text());
					 drawPlateTables();
					 return 1;
				}

            if(s.lower()=="under" || s=="-.---")
                s="0.000";
            if(s.lower()=="over" || s=="+.+++")
                s="4.001";

            for(int k=0;k<s.length();k++)
                if(s.at(k)!='-' &&
                   s.at(k)!='+' &&
                   s.at(k)!='.' &&
                   !isdigit(s.at(k)))
                        {
                             FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),
                                                  oLanguage->getText("str_invalid").text());
							 drawPlateTables();
                             return 1;
                        }
		}
    for(r=rs;r<=re;r++)
        for(c=cs;c<=ce;c++)
        {
            if(!plateTable1->getItemData(r,c))
                continue;
            cLocInfo *cell=(cLocInfo*)plateTable1->getItemData(r,c);
            int index=cell->caseIndex;
            sCasesSet *set=NULL;
            sCaseEntry *cas=NULL;
            int platel=cell->plate;

            if(index==-1)
                continue;
            else if(index<0)
            {
                set=&casesSets[cell->set];

                if(set->casesCount==0)
                    continue;
            }
            else
            {
                cas=(sCaseEntry*)caseList1->getItemData(index,0);
                set=&casesSets[cas->parentSet];
            }

            FXString s=plateTable1->getItemText(r,c);

            if(s.lower()=="under" || s=="-.---")
                s="0.000";
            if(s.lower()=="over" || s=="+.+++")
                s="4.001";

            double value=FXDoubleVal(s);
            value=value<0?0.000:value;
            if(index<0)
            {
                for(int casei=0;casei<set->casesCount;casei++)
                {
                    if((platel>=set->cases[casei].startPlate) && (platel<=(set->cases[casei].startPlate+set->cases[casei].plateSpanCount-1)))
                        set->cases[casei].controlsData[(platel-set->cases[casei].startPlate)*set->controlsPerPlate*set->controls_replicates+cell->sampleLoc]=value;
                }
            }
            else
                cas->data[cell->sampleLoc]=value;
        }
    drawPlateTables();
    return 1;
}

long cMDIReadings::onCmdPlateTable2(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    int r,c,rs,cs,re,ce;
    if(prData)
    {
        rs=re=*(int*)prData;
        cs=ce=*((int*)prData+1);
    }
    else
    {
        rs=FXMIN(plateTable2->getSelStartRow(),plateTable2->getSelEndRow());
        cs=FXMIN(plateTable2->getSelStartColumn(),plateTable2->getSelEndColumn());
        re=FXMAX(plateTable2->getSelStartRow(),plateTable2->getSelEndRow());
        ce=FXMAX(plateTable2->getSelStartColumn(),plateTable2->getSelEndColumn());
    }
   for(r=rs;r<=re;r++)
        for(c=cs;c<=ce;c++)
        {
            if(!plateTable2->getItemData(r,c))
                continue;
            FXString s=plateTable2->getItemText(r,c);

			if(s.empty())
				{
					 FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),
										  oLanguage->getText("str_invalid").text());
					 drawPlateTables();
					 return 1;
				}

            if(s.lower()=="under" || s=="-.---")
                s="0.000";
            if(s.lower()=="over" || s=="+.+++")
                s="4.001";

            for(int k=0;k<s.length();k++)
                if(s.at(k)!='-' &&
                   s.at(k)!='+' &&
                   s.at(k)!='.' &&
                   !isdigit(s.at(k)))
                        {
                             FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),
                                                  oLanguage->getText("str_invalid").text());
							 drawPlateTables();
                             return 1;
                        }
		}
    for(r=rs;r<=re;r++)
        for(c=cs;c<=cs;c++)
        {
            if(!plateTable2->getItemData(r,c))
                continue;
            cLocInfo *cell=(cLocInfo*)plateTable2->getItemData(r,c);
            int index=cell->caseIndex;
            sCasesSet *set=NULL;
            sCaseEntry *cas=NULL;
            int platel=cell->plate;

            if(index==-1)
                continue;
            else if(index<0)
            {
                set=&casesSets[cell->set];

                if(set->casesCount==0)
                    continue;
            }
            else
            {
                cas=(sCaseEntry*)caseList1->getItemData(index,0);
                set=&casesSets[cas->parentSet];
            }

            FXString s=plateTable2->getItemText(r,c);

            if(s.lower()=="under" || s=="-.---")
                s="0.000";
            if(s.lower()=="over" || s=="+.+++")
                s="4.001";

            double value=FXFloatVal(s);
            value=value<0?0.000:value;
            if(index<0)
            {
                for(int casei=0;casei<set->casesCount;casei++)
                {
                    if((platel>=set->cases[casei].startPlate) && (platel<=(set->cases[casei].startPlate+set->cases[casei].plateSpanCount-1)))
                        set->cases[casei].controlsData[(platel-set->cases[casei].startPlate)*set->controlsPerPlate*set->controls_replicates+cell->sampleLoc]=value;
                }
            }
            else
                cas->data[cell->sampleLoc]=value;
        }
    drawPlateTables();
    return 1;
}

long cMDIReadings::onCmdReread(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!tPlatesCount)
        return 1;

    if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_rerdw").text()))
        return 1;

    double **plates;

    /*int ows=0;
    if(oWinMain->isMinimized())
        ows=1;
    else if(oWinMain->isMaximized())
        ows=2;
    oWinMain->minimize();*/

    int platesi=0;
    for(int i=0;i<tSetsCount;i++)
    {
        plates=cReadingsManager::remakeReading(casesSets[i].assayRes->getCellString(0,3),casesSets[i].assayRes->getCellString(0,4),casesSets[i].platesCount,platesi,tPlatesCount);
        if(!plates)
            break;

        for(int k=0;k<casesSets[i].platesCount;k++,platesi++)
        {
            for(int j=0;j<96;j++)
                plateTable1->setItemText(platesi*8+j/12,j%12,FXStringVal(plates[k][j]));
            free(plates[k]);
        }
    }
    /*switch(ows)
    {
        case 1:
            break;
        case 2:
            oWinMain->maximize();
            break;
        default:
            oWinMain->restore();
            break;
    }*/
    onCmdPlateTable1(NULL,0,NULL);
    return 1;
}

long cMDIReadings::onCmdTemplate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    plateTable1->acceptInput(true);
    plateTable2->acceptInput(true);
    if(selectedCase<0)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_nocasesel").text());
        return 1;
    }
    FXString st=*((sCaseEntry*)caseList1->getItemData(selectedCase,0))->tpl;

    if(!cTemplateManager::templateExists(st))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_rdgmdi_opentpl").text());
        return false;
    }

    if(oWinMain->isWindow(st))
        oWinMain->raiseWindow(st);
    else
        cTemplateManager::editTemplate(mdiClient,mdiMenu,st);
    return 1;
}

long cMDIReadings::onRefreshAsy(FXObject *prSender,FXSelector prSelector,void *prData)
{
    sCDLGO *data=(sCDLGO*)prData;
    FXString prId=*data->id;
    FXString prId2=*data->id2;
    FXString prAsy=*data->asy;
    int ci=0;
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        *casesSets[setsi].assay=prId2;
        *casesSets[setsi].assaytitle=prAsy;
        if(casesSets[setsi].assayRes)
            delete casesSets[setsi].assayRes;
        casesSets[setsi].assayRes=data->res;
        //FXString calculation=casesSets[setsi].assayRes->getCellString(0,6);

        for(int casei=0;casei<casesSets[setsi].casesCount;casei++,ci++)
        {
            sCaseEntry *c=&casesSets[setsi].cases[casei];
            if(prId==*c->assay)
            {
                *c->assay=prId2;
                caseList1->setItemText(ci,4,*c->assay);
                caseList2->setItemText(ci,4,*c->assay);
            }

            int cci=0,p=0,d,od,pp=0;
            for(int setsi=0;setsi<tSetsCount;setsi++)
            {
                d=od=0;
                for(int casei=0;casei<casesSets[setsi].casesCount;casei++,cci++)
                {
                    if(cci==selectedCase)
                        pp=p;
                    d=casesSets[setsi].cases[casei].startPlate;
                    if(d!=od)
                    {
                        p++;
                        od=d;
                    }
                    p+=casesSets[setsi].cases[casei].plateSpanCount-1;
                }
            }
        }
    }
    selectCase(selectedCase);
    return 1;
}

long cMDIReadings::onRefreshTpl(FXObject *prSender,FXSelector prSelector,void *prData)
{
    sCDLGO *data=(sCDLGO*)prData;
    FXString prId=*data->id;
    FXString prId2=*data->id2;
    int ci=0;
    for(int setsi=0;setsi<tSetsCount;setsi++)
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++,ci++)
        {
            sCaseEntry *c=&casesSets[setsi].cases[casei];
            if(prId==*c->tpl)
            {
                *c->tpl=prId2;
                selectCase(selectedCase);
            }
        }
    return 1;
}

long cMDIReadings::onRefresh(FXObject *prSender,FXSelector prSelector,void *prData)
{
    sCDLGO *data=(sCDLGO*)prData;
    if(newdata)
        return 1;
    FXString prId=*data->id;
    FXString prId2=*data->id2;
    FXString prAsy=*data->asy;
    FXString id,asy;
    FXint ci=0;
    for(int setsi=0;setsi<tSetsCount;setsi++)
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++,ci++)
        {
            sCaseEntry *c=&casesSets[setsi].cases[casei];
            id=*c->id;
            asy=*c->assay;

            if(prId!=id || prAsy!=asy)
                continue;

            *c->id=prId2;
            *c->commodity=data->res->getCellString(0,7);
            *c->comments=data->res->getCellString(0,16);

            caseList1->setItemText(ci,1,*c->id);
            caseList2->setItemText(ci,1,*c->id);
            selectCase(selectedCase);

            int cci=0,p=0,d,od,pp=0;
            for(int setsi=0;setsi<tSetsCount;setsi++)
            {
                d=od=0;
                for(int casei=0;casei<casesSets[setsi].casesCount;casei++,cci++)
                {
                    if(cci==selectedCase)
                        pp=p;
                    d=casesSets[setsi].cases[casei].startPlate;
                    if(d!=od)
                    {
                        p++;
                        od=d;
                    }
                    p+=casesSets[setsi].cases[casei].plateSpanCount-1;
                }
            }
            charts[ci]->setTitle(oLanguage->getText("str_rdgmdi_plate")+FXStringFormat(" %d",pp+1)+" - "+(*c->id)+" - "+(*c->assay));
            charts[ci]->hide();
            charts[ci]->show();
        }
    return 1;
}

long cMDIReadings::onCmdReport(FXObject *prSender,FXSelector prSelector,void *prData)
{
    plateTable1->acceptInput(true);
    plateTable2->acceptInput(true);
    if(!saved)
    {
        if(MBOX_CLICKED_YES==FXMessageBox::warning(oWinMain,MBOX_YES_NO,oLanguage->getText("str_warning").text(),oLanguage->getText("str_rdgmdi_mustsaverep").text()))
            saveData();
        else
            return 1;
    }
    if(!saved)
        return 1;

    cSortList *cases=new cSortList(this);
    for(int i=0;i<caseList1->getNumRows();i++)
    {
        cases->appendItem(caseList1->getItemText(i,3)+"\t"+caseList1->getItemText(i,1));
        cases->selectItem(i);
    }
    switch(prSelector)
    {
        case FXSEL(SEL_COMMAND,ID_REP_CA):
            cCaseAnalysisReport::openCases(mdiClient,mdiMenu,*cases);
            break;
        default:
            break;
    }
    delete cases;
    return 1;
}

long cMDIReadings::onCmdShowRules(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXDialogBox msg(oWinMain,oLanguage->getText("str_rdgmdi_rules"),DECOR_TITLE|DECOR_BORDER);
    FXVerticalFrame *vf=new FXVerticalFrame(&msg,LAYOUT_CENTER_X|JUSTIFY_CENTER_X);
        FXVerticalFrame *vf2=new FXVerticalFrame(vf,FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
        FXText *tf=new FXText(vf2,NULL,0,LAYOUT_FIX_WIDTH|LAYOUT_FIX_HEIGHT|TEXT_READONLY,0,0,300,200);
        tf->setText(invalidPlatesDetails);
    new FXButton(vf,oLanguage->getText("str_but_close"),NULL,&msg,FXDialogBox::ID_ACCEPT,BUTTON_NORMAL|LAYOUT_CENTER_X);
    msg.create();
    msg.show(PLACEMENT_OWNER);
    msg.setFocus();
    msg.raise();
    getApp()->runModalWhileShown(&msg);
    return 1;
}

long cMDIReadings::onEvtRedir(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXEvent *event=(FXEvent*)prData;

    switch(event->code)
    {
        case KEY_Page_Up:
        case KEY_Page_Down:
        case KEY_KP_Page_Up:
        case KEY_KP_Page_Down:
            if(tiPrimary->hasFocus() || tiStats->hasFocus())
                return 1;
            if(tbMain->getCurrent()==2)
                return chartWin->tryHandle(prSender,prSelector,prData);
            break;
        default:
            break;
    }
    return cMDIChild::handle(prSender,prSelector,prData);
}
