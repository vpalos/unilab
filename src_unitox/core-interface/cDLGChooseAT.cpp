#include "graphics.h"
#include "cLanguage.h"
#include "cDateChooser.h"
#include "cDLGChooseAT.h"
#include "cAssayManager.h"
#include "cTemplateManager.h"

FXDEFMAP(cDLGChooseAT) mapDLGChooseAT[]=
{
    FXMAPFUNC(SEL_DOUBLECLICKED,cDLGChooseAT::ID_DATETF,cDLGChooseAT::onCmdDateTf),
    FXMAPFUNC(SEL_COMMAND,cDLGChooseAT::ID_BUTDATETF,cDLGChooseAT::onCmdDateTf),
    FXMAPFUNC(SEL_DOUBLECLICKED,cDLGChooseAT::ID_ASSAYLIST,cDLGChooseAT::onDbcList),
    FXMAPFUNC(SEL_DOUBLECLICKED,cDLGChooseAT::ID_TEMPLATELIST,cDLGChooseAT::onDbcList),
    FXMAPFUNC(SEL_UPDATE,cDLGChooseAT::ID_DLGCHOOSEAT,cDLGChooseAT::onUpdWindow),
    FXMAPFUNC(SEL_COMMAND,cDLGChooseAT::CMD_BUT_PROCEED,cDLGChooseAT::onCmdProceed),
};

FXIMPLEMENT(cDLGChooseAT,FXDialogBox,mapDLGChooseAT,ARRAYNUMBER(mapDLGChooseAT))

cDLGChooseAT::cDLGChooseAT(FXWindow *prOwner,FXint prFlags) :
    FXDialogBox(prOwner,oLanguage->getText("str_dlg_chooseat_title"),DECOR_TITLE|DECOR_BORDER)
{
    setTarget(this);
    setSelector(ID_DLGCHOOSEAT);
    asy="";
    tpl="";
    lot="";
    expdate="";
    setChoices(prFlags);
    
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL|LAYOUT_FIX_WIDTH|LAYOUT_FIX_HEIGHT,0,0,415,250,0,0,0,0);
        lbStatus=new FXLabel(_vframe0,"",NULL,LAYOUT_FILL_X|JUSTIFY_CENTER_X);
            lbStatus->setText(oLanguage->getText("str_dlg_chooseat_none"));
        gbMain=new FXGroupBox(_vframe0,"",GROUPBOX_TITLE_CENTER|FRAME_GROOVE|LAYOUT_FILL);
        gbMain->setTextColor(FXRGB(200,0,0));
            swMain=new FXSwitcher(gbMain,LAYOUT_FILL,0,0,0,0,0,0,0,0);
                    s1Frame=new FXVerticalFrame(swMain,LAYOUT_FILL|JUSTIFY_CENTER_X,0,0,0,0,0,0,0,0);
                        FXHorizontalFrame *_hframe2=new FXHorizontalFrame(s1Frame,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0,0,0);
                        asyList=new cSortList(_hframe2,this,ID_ASSAYLIST,ICONLIST_SINGLESELECT|ICONLIST_DETAILED|LAYOUT_FILL);
                        asyList->getHeader()->setHeaderStyle(HEADER_HORIZONTAL|HEADER_BUTTON);
                        asyList->appendHeader(oLanguage->getText("str_asyman_asynameh"),NULL,120);
                        asyList->appendHeader(oLanguage->getText("str_asyman_asytitleh"),NULL,264);
                        sAssayObject *res=cAssayManager::listAssays();
                        if(res!=NULL)
                        {
                            for(int i=0;i<cAssayManager::getAssayCount();i++)
                                asyList->appendItem(*(res[i].id)+"\t"+(res[i].solid>0?"* ":"")+*(res[i].title));
                            asyList->sortItems();
                            free(res);
                        }
                        if(cAssayManager::getAssayCount()>0)
                            asyList->setCurrentItem(0);
                    FXHorizontalFrame *_hframe1=new FXHorizontalFrame(s1Frame,LAYOUT_CENTER_X|JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y,0,0,0,0,0,0,0,0,0);
                    new FXLabel(_hframe1,oLanguage->getText("str_dlg_chooseat_lot"),NULL,LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
                    tfLot=new FXTextField(_hframe1,13,NULL,0,TEXTFIELD_NORMAL|LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
                    new FXVerticalSeparator(_hframe1,LAYOUT_FIX_WIDTH|LAYOUT_CENTER_X|LAYOUT_CENTER_Y,0,0,10);
                    new FXLabel(_hframe1,oLanguage->getText("str_dlg_chooseat_expdate"),NULL,LABEL_NORMAL|LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
                    new FXButton(_hframe1,(char*)NULL,new FXGIFIcon(getApp(),data_calendar),this,ID_BUTDATETF,FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
                    tfDate=new FXTextField(_hframe1,10,this,ID_DATETF,TEXTFIELD_NORMAL|LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
                    
                s2Frame=new FXHorizontalFrame(swMain,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
                    tplList=new cSortList(s2Frame,this,ID_TEMPLATELIST,ICONLIST_SINGLESELECT|ICONLIST_DETAILED|LAYOUT_FILL);
                    tplList->getHeader()->setHeaderStyle(HEADER_HORIZONTAL|HEADER_BUTTON);
                    tplList->appendHeader(oLanguage->getText("str_tplman_tplnameh"),NULL,264);
                    tplList->appendHeader(oLanguage->getText("str_tplman_tpldateh"),NULL,120);
                    tplList->setFocus();
                    sTemplateObject *res2=cTemplateManager::listTemplates();
                    if(res2!=NULL)
                    {
                        tplList->clearItems();
                        for(int i=0;i<cTemplateManager::getTemplateCount();i++)
                            tplList->appendItem(*(res2[i].title)+"\t"+*(res2[i].date));
                        tplList->sortItems();
                        free(res2);
                    }
                    if(cTemplateManager::getTemplateCount()>0)
                        tplList->setCurrentItem(0);
                    
            FXHorizontalFrame *_hframe0=new FXHorizontalFrame(_vframe0,LAYOUT_CENTER_X);
            new FXButton(_hframe0,oLanguage->getText("str_but_cancel"),NULL,this,ID_CANCEL,FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
            new FXVerticalFrame(_hframe0,LAYOUT_FILL_Y|LAYOUT_FIX_WIDTH,0,0,0,0,0,0,10,0);
            btProceed=new FXButton(_hframe0,"",NULL,this,CMD_BUT_PROCEED,BUTTON_DEFAULT|BUTTON_INITIAL|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
}

cDLGChooseAT::~cDLGChooseAT()
{
}

void cDLGChooseAT::setChoices(FXint prFlags)
{
    flags=prFlags;
    switch(flags)
    {
        case CHOOSEAT_TEMPLATE:
            state=1;
            break;
        default:
            state=0;
            break;
    }
}
        
void cDLGChooseAT::setAssay(const FXString &prAssay)
{
    asy=prAssay;
}

void cDLGChooseAT::setTemplate(const FXString &prTemplate)
{
    tpl=prTemplate;
}

FXString cDLGChooseAT::getAssay(void)
{
    return asy;
}

FXString cDLGChooseAT::getTemplate(void)
{
    return tpl;
}

FXString cDLGChooseAT::getLot(void)
{
    return lot;
}

FXString cDLGChooseAT::getExpDate(void)
{
    return expdate;
}

long cDLGChooseAT::onCmdProceed(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(state==0)
    {
        asy=asyList->getItemText(asyList->getAnchorItem()).before('\t');
        if(flags==CHOOSEAT_ASSAY)
            state++;
    }
    if(state==1 && flags==CHOOSEAT_TEMPLATE)
        tpl=tplList->getItemText(tplList->getAnchorItem()).before('\t');
    state++;
    if(state==2 && flags!=CHOOSEAT_ASSAY)
        tpl=tplList->getItemText(tplList->getAnchorItem()).before('\t');
    expdate=tfDate->getText();
    lot=tfLot->getText();
    if(state>1)
        handle(NULL,FXSEL(SEL_COMMAND,ID_ACCEPT),NULL);
    return 1;
}

long cDLGChooseAT::onUpdWindow(FXObject *prSender,FXSelector prSelector,void *prData)
{
    switch(state)
    {
        case 0:
            swMain->setCurrent(0);
            if(flags==CHOOSEAT_ASSAY && !tpl.empty())
                lbStatus->setText(oLanguage->getText("str_dlg_chooseat_step2stt")+" "+tpl+", "+oLanguage->getText("str_dlg_chooseat_none"));
            else
                lbStatus->setText(oLanguage->getText("str_dlg_chooseat_none"));
            break;
        case 1:
            swMain->setCurrent(1);
            if(!asy.empty())
                lbStatus->setText(oLanguage->getText("str_dlg_chooseat_step1stt")+" "+asy+", "+oLanguage->getText("str_dlg_chooseat_none"));
            else
                lbStatus->setText(oLanguage->getText("str_dlg_chooseat_none"));
            break;
    }
    
    FXString gb="",gt="";
    if(flags==CHOOSEAT_BOTH)
    {
        gt=oLanguage->getText("str_dlg_chooseat_step"+FXStringFormat("%s",(state==0?"1":"2")))+": ";
        gb=oLanguage->getText("str_dlg_chooseat_"+FXStringFormat("%s",(state==0?"step2pro":"proceed")));
    }
    else
        gb=oLanguage->getText("str_dlg_chooseat_proceed");
    gt=gt+oLanguage->getText("str_dlg_chooseat_step"+FXStringFormat("%s",(state==0?"1":"2"))+"txt");
    gbMain->setText(gt);
    btProceed->setText(gb+" >>");
    p=true;
    switch(state)
    {
        case 0:
            if(asyList->getAnchorItem()==-1)
                p=false;
            break;
        case 1:
            if(tplList->getAnchorItem()==-1)
                p=false;
            break;
    }
    if(p)
        btProceed->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    else
        btProceed->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    return 1;
}

long cDLGChooseAT::onCmdDateTf(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXString res=cDateChooser::selectDate(this);
    if(!res.empty())
        tfDate->setText(res);
    return 1;
}

long cDLGChooseAT::onDbcList(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(p)
        handle(this,FXSEL(SEL_COMMAND,CMD_BUT_PROCEED),NULL);
    return 1;
}


