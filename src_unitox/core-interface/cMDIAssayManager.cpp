#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cAssayManager.h"
#include "cWinMain.h"
#include "cMDIDatabaseManager.h"
#include "cMDIAssayManager.h"

cMDIAssayManager *oMDIAssayManager=NULL;

FXDEFMAP(cMDIAssayManager) mapMDIAssayManager[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIAssayManager::ID_ASSAYMANAGER,cMDIAssayManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIAssayManager::CMD_BUT_CLOSE,cMDIAssayManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIAssayManager::CMD_BUT_OPEN,cMDIAssayManager::onCmdOpen),
    FXMAPFUNC(SEL_COMMAND,cMDIAssayManager::CMD_BUT_NEW,cMDIAssayManager::onCmdNew),
    FXMAPFUNC(SEL_COMMAND,cMDIAssayManager::CMD_BUT_DUPLICATE,cMDIAssayManager::onCmdDuplicate),
    FXMAPFUNC(SEL_DOUBLECLICKED,cMDIAssayManager::ID_ASSAYLIST,cMDIAssayManager::onCmdOpen),
    FXMAPFUNC(SEL_COMMAND,cMDIAssayManager::CMD_BUT_REMOVE,cMDIAssayManager::onCmdRemove),

};

FXIMPLEMENT(cMDIAssayManager,cMDIChild,mapMDIAssayManager,ARRAYNUMBER(mapMDIAssayManager))

cMDIAssayManager::cMDIAssayManager()
{
}

cMDIAssayManager::cMDIAssayManager(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH)
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_ASSAYMANAGER);
    client=prP;
    popup=prPup;

    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(this,LAYOUT_FILL);
    FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe0,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
    new FXLabel(_vframe2,oLanguage->getText("str_asyman_asylist_title"));
    FXHorizontalFrame *_hframe1=new FXHorizontalFrame(_vframe2,LAYOUT_FILL,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe4=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|LAYOUT_RIGHT,0,0,0,0,0,0,0,0,0,0);

    asyList=new cSortList(_vframe3,this,ID_ASSAYLIST,ICONLIST_SINGLESELECT|ICONLIST_DETAILED|LAYOUT_FILL);
    asyList->appendHeader(oLanguage->getText("str_asyman_asynameh"),NULL,120);
    asyList->appendHeader(oLanguage->getText("str_asyman_asytitleh"),NULL,264);
    oMDIAssayManager=this;
    this->update();
    if(asyList->getNumItems()>0)
    {
        asyList->selectItem(0);
        asyList->setCurrentItem(0);
    }
    asyList->setFocus();

    new FXButton(_vframe4,oLanguage->getText("str_asyman_new"),NULL,this,CMD_BUT_NEW,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_FIX_WIDTH,0,0,88,0);
    new FXHorizontalSeparator(_vframe4,LAYOUT_TOP|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_asyman_open"),NULL,this,CMD_BUT_OPEN,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X);
    new FXButton(_vframe4,oLanguage->getText("str_asyman_duplicate"),NULL,this,CMD_BUT_DUPLICATE,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X);
    new FXButton(_vframe4,oLanguage->getText("str_but_close"),NULL,this,CMD_BUT_CLOSE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
    new FXHorizontalSeparator(_vframe4,LAYOUT_BOTTOM|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_asyman_remove"),NULL,this,CMD_BUT_REMOVE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
}

cMDIAssayManager::~cMDIAssayManager()
{
    cMDIAssayManager::unload();
}

void cMDIAssayManager::create()
{
    cMDIChild::create();
    show();
}

void cMDIAssayManager::load(FXMDIClient *prP,FXPopup *prMenu)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(prP,prMenu);
        return;
    }

    if(oMDIAssayManager!=NULL)
    {
        oMDIAssayManager->setFocus();
        prP->setActiveChild(oMDIAssayManager);
        if(oMDIAssayManager->isMinimized())
            oMDIAssayManager->restore();
        return;
    }
    new cMDIAssayManager(prP,oLanguage->getText("str_asyman_title")+" (Mycotoxyn)",new FXGIFIcon(oApplicationManager,data_inputs),prMenu,MDI_NORMAL|MDI_TRACKING,10,10,515,300);
    oMDIAssayManager->create();
    oMDIAssayManager->setFocus();

}

FXbool cMDIAssayManager::isLoaded(void)
{
    return (oMDIAssayManager!=NULL);
}

void cMDIAssayManager::unload(void)
{
    oMDIAssayManager=NULL;
}

void cMDIAssayManager::update(void)
{
    if(!isLoaded())
        return;
    sAssayObject *res=cAssayManager::listAssays();
    if(res==NULL)
        return;
    asyList->clearItems();
    for(int i=0;i<cAssayManager::getAssayCount();i++)
    asyList->appendItem(*(res[i].id)+"\t"+(res[i].solid>0?"* ":"")+*(res[i].title));
    asyList->sortItems();
    free(res);
}

void cMDIAssayManager::cmdRefresh(void)
{
    if(!isLoaded())
        return;
    oMDIAssayManager->update();
}

long cMDIAssayManager::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDIAssayManager::unload();
    close();
    return 1;
}

long cMDIAssayManager::onCmdNew(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cAssayManager::newAssay(client,popup);
    return 1;
}

long cMDIAssayManager::onCmdOpen(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int db=-1,i;
    for(i=0;i<asyList->getNumItems();i++)
        if(asyList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        db=asyList->getCurrentItem();
    if(db==-1)
        return 1;
    FXString st=asyList->getItemText(db).before('\t');
    if(oWinMain->isWindow(st))
        oWinMain->raiseWindow(st);
    else
        cAssayManager::editAssay(client,popup,st);
    return 1;
}

long cMDIAssayManager::onCmdDuplicate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int db=-1,i,j;
    for(i=0;i<asyList->getNumItems();i++)
        if(asyList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        return 1;
    FXString res2=cAssayManager::duplicateAssay(asyList->getItemText(db).before('\t').text());
    this->update();
    if(res2.empty())
        return 1;
    for(j=0;j<asyList->getNumItems();j++)
        if(res2==asyList->getItemText(j).before('\t'))
        {
            asyList->selectItem(j);
            break;
        }
    return 1;
}

long cMDIAssayManager::onCmdRemove(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i,db=-1;
    for(i=0;i<asyList->getNumItems();i++)
        if(asyList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        return 1;
    if(cAssayManager::assaySolid(asyList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_eraseasysolid").text());
        return 1;
    }
    if(cAssayManager::assayNeeded(asyList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasedataneeded").text());
        return 1;
    }
    if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_eraseasy").text()))
        return 1;
    cMDIChild *win=oWinMain->getWindow(asyList->getItemText(db).before('\t'));
    if(win!=NULL)
        win->close();
    cAssayManager::removeAssay(asyList->getItemText(db).before('\t'));
    this->update();
    if(db>=asyList->getNumItems())
        db=asyList->getNumItems()-1;
    if(db>=0)
        asyList->selectItem(db);
    return 1;
}
