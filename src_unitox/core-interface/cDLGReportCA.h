#ifndef CDLGREPORTCA_H
#define CDLGREPORTCA_H

#include <fx.h>

class cDLGReportCA : public FXDialogBox
{
    FXDECLARE(cDLGReportCA);
    private:
        FXButton *butAccept;
        FXButton *butCancel;

    protected:

        cDLGReportCA(){}

    public:
        FXuint choice;

        FXCheckButton *bUseGraphs;
        FXCheckButton *bUseData;
        FXCheckButton *bUseInfo;

        cDLGReportCA(FXWindow *prOwner);
        virtual ~cDLGReportCA();

        enum
        {
            ID_THIS=FXDialogBox::ID_LAST,
            ID_UPDATE,

            CMD_BUT_APPLY,
            ID_LAST
        };

        long onCmdUpdate(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdBslchk(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdApply(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
