#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDatabaseManager.h"
#include "cWinMain.h"
#include "cMDIDatabaseManager.h"

cMDIDatabaseManager *oMDIDatabaseManager=NULL;

FXDEFMAP(cMDIDatabaseManager) mapMDIDatabaseManager[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIDatabaseManager::ID_DATABASEMANAGER,cMDIDatabaseManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIDatabaseManager::CMD_BUT_CLOSE,cMDIDatabaseManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIDatabaseManager::CMD_BUT_OPEN,cMDIDatabaseManager::onCmdOpen),
    FXMAPFUNC(SEL_DOUBLECLICKED,cMDIDatabaseManager::ID_DATABASELIST,cMDIDatabaseManager::onCmdOpen),
    FXMAPFUNC(SEL_COMMAND,cMDIDatabaseManager::CMD_BUT_NEW,cMDIDatabaseManager::onCmdNew),
    FXMAPFUNC(SEL_COMMAND,cMDIDatabaseManager::CMD_BUT_EDIT,cMDIDatabaseManager::onCmdEdit),
    FXMAPFUNC(SEL_COMMAND,cMDIDatabaseManager::CMD_BUT_DUPLICATE,cMDIDatabaseManager::onCmdDuplicate),
    FXMAPFUNC(SEL_COMMAND,cMDIDatabaseManager::CMD_BUT_REMOVE,cMDIDatabaseManager::onCmdRemove),

};

FXIMPLEMENT(cMDIDatabaseManager,cMDIChild,mapMDIDatabaseManager,ARRAYNUMBER(mapMDIDatabaseManager))

cMDIDatabaseManager::cMDIDatabaseManager()
{
}

cMDIDatabaseManager::cMDIDatabaseManager(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH)
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_DATABASEMANAGER);
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(this,LAYOUT_FILL);
    FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe0,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
    new FXLabel(_vframe2,oLanguage->getText("str_dbman_dblist_title"));
    FXHorizontalFrame *_hframe1=new FXHorizontalFrame(_vframe2,LAYOUT_FILL,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe4=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|LAYOUT_RIGHT,0,0,0,0,0,0,0,0,0,0);

    dbList=new cSortList(_vframe3,this,ID_DATABASELIST,ICONLIST_SINGLESELECT|ICONLIST_DETAILED|LAYOUT_FILL);
    dbList->appendHeader(oLanguage->getText("str_dbman_dbnameh"),NULL,229);
    dbList->appendHeader(oLanguage->getText("str_dbman_dbownerh"),NULL,150);

    this->onCmdSpecies(NULL,0,NULL);

    if(dbList->getNumItems()>0)
    {
        dbList->selectItem(0);
        dbList->setCurrentItem(0);
    }
    dbList->setFocus();

    new FXButton(_vframe4,oLanguage->getText("str_dbman_open"),new FXGIFIcon(getApp(),data_open_db),this,CMD_BUT_OPEN,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FIX_WIDTH,0,0,93);
    new FXHorizontalSeparator(_vframe4,LAYOUT_TOP|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_but_close"),NULL,this,CMD_BUT_CLOSE,LAYOUT_TOP|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);

    new FXButton(_vframe4,oLanguage->getText("str_dbman_remove"),NULL,this,CMD_BUT_REMOVE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
    new FXHorizontalSeparator(_vframe4,LAYOUT_BOTTOM|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_dbman_duplicate"),NULL,this,CMD_BUT_DUPLICATE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
    new FXButton(_vframe4,oLanguage->getText("str_dbman_edit"),NULL,this,CMD_BUT_EDIT,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
    new FXHorizontalSeparator(_vframe4,LAYOUT_BOTTOM|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_dbman_new"),new FXGIFIcon(getApp(),data_new_db),this,CMD_BUT_NEW,LAYOUT_BOTTOM|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
}

cMDIDatabaseManager::~cMDIDatabaseManager()
{
    cMDIDatabaseManager::unload();
}

void cMDIDatabaseManager::create()
{
    cMDIChild::create();
    show();
}

void cMDIDatabaseManager::load(FXMDIClient *prP,FXPopup *prMenu)
{
    if(oMDIDatabaseManager!=NULL)
    {
        oMDIDatabaseManager->setFocus();
        prP->setActiveChild(oMDIDatabaseManager);
        if(oMDIDatabaseManager->isMinimized())
            oMDIDatabaseManager->restore();
        return;
    }
    oMDIDatabaseManager=new cMDIDatabaseManager(prP,oLanguage->getText("str_dbman_title"),new FXGIFIcon(oApplicationManager,data_open_db),prMenu,MDI_NORMAL|MDI_TRACKING,10,10,515,350);
    oMDIDatabaseManager->create();
    oMDIDatabaseManager->setFocus();
}

FXbool cMDIDatabaseManager::isLoaded(void)
{
    return (oMDIDatabaseManager!=NULL);
}

void cMDIDatabaseManager::unload(void)
{
    oMDIDatabaseManager=NULL;
}

void cMDIDatabaseManager::cmdNew(void)
{
    if(!isLoaded())
        return;
    oMDIDatabaseManager->onCmdNew(NULL,0,NULL);
}

void cMDIDatabaseManager::update(void)
{
    if(!isLoaded())
        return;
    oMDIDatabaseManager->onCmdSpecies(NULL,0,NULL);
}

void cMDIDatabaseManager::cmdRefresh(void)
{
    if(!isLoaded())
        return;
    oMDIDatabaseManager->update();
}

long cMDIDatabaseManager::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDIDatabaseManager::unload();
    close();
    return 1;
}

long cMDIDatabaseManager::onCmdSpecies(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i;
    sDatabaseObject *dbo=cDatabaseManager::listDatabases();
    if(dbo==NULL)
        return 1;
    dbList->clearItems();
    int dbc=cDatabaseManager::getDatabaseCount();
    for(i=0;i<dbc;i++)
        if(dbo[i].title!=NULL)
            dbList->appendItem(*(dbo[i].title)+"\t"+*(dbo[i].owner));
    free(dbo);
    dbList->sortItems();
    return 1;
}

long cMDIDatabaseManager::onCmdOpen(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i;
    int db=-1;
    for(i=0;i<dbList->getNumItems();i++)
        if(dbList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        db=dbList->getCurrentItem();
    if(db==-1)
        return 1;
    dbList->selectItem(db);
    FXString res2;
    sDatabaseObject *res=cDatabaseManager::listDatabases();
    if(res==NULL)
        return 1;
    for(i=0;i<cDatabaseManager::getDatabaseCount();i++)
        if(*(res[i].title)==dbList->getItemText(db).before('\t'))
        {
            oDataLink->openDesignated(*(res[i].filename),false,this);
            break;
        }
    free(res);
    close();
    return 1;
}

long cMDIDatabaseManager::onCmdNew(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i,j;
    FXString res=cDatabaseManager::newDatabase();
    if(!res.empty())
    {
        this->onCmdSpecies(NULL,0,NULL);
        sDatabaseObject *res2=cDatabaseManager::listDatabases();
        if(res2==NULL)
            return 1;
        for(i=0;i<cDatabaseManager::getDatabaseCount();i++)
            if(res==*(res2[i].filename))
            {
            for(j=0;j<dbList->getNumItems();j++)
                if(*(res2[i].title)==dbList->getItemText(j).before('\t'))
                {
                    dbList->selectItem(j);
                    break;
                }
                break;
            }
        free(res2);
        oDataLink->openDesignated(res,false,this);
        close();
    }
    return 1;
}

long cMDIDatabaseManager::onCmdEdit(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i,j;
    int db=-1;
    for(i=0;i<dbList->getNumItems();i++)
        if(dbList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        return 1;
    FXString res2;
    sDatabaseObject *res=cDatabaseManager::listDatabases();
    if(res==NULL)
        return 1;
    for(i=0;i<cDatabaseManager::getDatabaseCount();i++)
        if(*(res[i].title)==dbList->getItemText(db).before('\t'))
        {
            res2=cDatabaseManager::editDatabase(*(res[i].filename));
            this->onCmdSpecies(NULL,0,NULL);
            break;
        }
    free(res);
    if(res2.empty())
        return 1;
    res=cDatabaseManager::listDatabases();
    if(res==NULL)
        return 1;
    for(i=0;i<cDatabaseManager::getDatabaseCount();i++)
        if(res2==*(res[i].filename))
        {
        for(j=0;j<dbList->getNumItems();j++)
            if(*(res[i].title)==dbList->getItemText(j).before('\t'))
            {
                dbList->selectItem(j);
                break;
            }
        break;
        }
    free(res);
    return 1;
}

long cMDIDatabaseManager::onCmdDuplicate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i,j;
    int db=-1;
    for(i=0;i<dbList->getNumItems();i++)
        if(dbList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        return 1;
    sDatabaseObject *res=cDatabaseManager::listDatabases();
    FXString res2="";
    if(res==NULL)
        return 1;
    for(i=0;i<cDatabaseManager::getDatabaseCount();i++)
    {
        if(strcasecmp(res[i].title->text(),dbList->getItemText(db).before('\t').text())==0)
        {
            res2=cDatabaseManager::duplicateDatabase(*(res[i].filename));
            this->onCmdSpecies(NULL,0,NULL);
            break;
        }
    }
    free(res);
    if(res2.empty())
        return 1;
    res=cDatabaseManager::listDatabases();
    if(res==NULL)
        return 1;
    for(i=0;i<cDatabaseManager::getDatabaseCount();i++)
        if(res2==*(res[i].filename))
        {
        for(j=0;j<dbList->getNumItems();j++)
            if(*(res[i].title)==dbList->getItemText(j).before('\t'))
            {
                dbList->selectItem(j);
                break;
            }
        break;
        }
    free(res);
    return 1;
}

long cMDIDatabaseManager::onCmdRemove(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i;
    int db=-1;
    for(i=0;i<dbList->getNumItems();i++)
        if(dbList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        return 1;
    if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_erasedb").text()))
        return 1;
    sDatabaseObject *res=cDatabaseManager::listDatabases();
    if(res==NULL)
        return 1;
    for(i=0;i<cDatabaseManager::getDatabaseCount();i++)
    {
        if(strcasecmp(res[i].title->text(),dbList->getItemText(db).before('\t').text())==0)
        {
            if(*(res[i].filename)==oDataLink->getFilename())
            {
                FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_closedb").text());
                return 1;
            }
            cDatabaseManager::removeDatabase(*(res[i].filename));
            this->onCmdSpecies(NULL,0,NULL);
            break;
        }
    }
    if(db>=dbList->getNumItems())
        db=dbList->getNumItems()-1;
    if(db>=0)
        dbList->selectItem(db);
    free(res);
    return 1;
}
