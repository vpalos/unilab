#ifndef CHORIZONTALSEPARATOR_H
#define CHORIZONTALSEPARATOR_H

#include <fx.h>

class cHorizontalSeparator : public FXHorizontalSeparator
{
    FXDECLARE(cHorizontalSeparator);
    
    private:
    protected:
        cHorizontalSeparator();
    
    public:
        cHorizontalSeparator(FXComposite *p, FXuint opts=SEPARATOR_GROOVE|LAYOUT_FILL_X, FXint x=0, FXint y=0, FXint w=0, FXint h=0, FXint pl=1, FXint pr=1, FXint pt=0, FXint pb=0);
        ~cHorizontalSeparator();
        
        long onPaint(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
