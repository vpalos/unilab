#include "engine.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cMDIAssay.h"
#include "cMDIReadings.h"
#include "cAssayManager.h"
#include "cFormulaRatio.h"

FXbool cAssayManager::assaySolid(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_assays WHERE id='"+ress.trim().substitute("'","")+"' AND solid='1';");
    return (res->getRowCount()>0) || (prId=="---");
}

FXbool cAssayManager::assayExists(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_assays WHERE id='"+ress.trim().substitute("'","")+"';");
    return (res->getRowCount()>0) || (prId=="---");
}

FXbool cAssayManager::assayNeeded(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXbool ret=false;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_gncases WHERE assay_oid='"+prId+"';");
    if(res->getRowCount()>0)
        ret=true;
    return ret;
}

FXString cAssayManager::getNewID(void)
{
    static int asyNr=oApplicationManager->reg().readIntEntry("oids","assays",1);
    FXString res=oLanguage->getText("str_asymdi_asytitle");
    FXString ret=res+FXStringFormat(" #%d",++asyNr);
    while(assayExists(ret))
        ret=res+FXStringFormat(" #%d",++asyNr);
    oApplicationManager->reg().writeIntEntry("oids","assays",asyNr);
    return ret;
}

int cAssayManager::getAssayCount(void)
{
    if(!oDataLink->isOpened())
        return 0;
    cDataResult *res=oDataLink->execute("SELECT COUNT(id) FROM t_assays;");
    return res->getCellInt(0,0);
}

sAssayObject *cAssayManager::listAssays()
{
    if(!oDataLink->isOpened())
        return NULL;

    sAssayObject *ret=(sAssayObject*)malloc(getAssayCount()*sizeof(sAssayObject));
    if(!ret)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }
    cDataResult *res=oDataLink->execute(("SELECT oid,id,title,solid FROM t_assays;"));
    for(int i=0;i<res->getRowCount();i++)
    {
        ret[i].oid=res->getCellInt(i,0);
        ret[i].id=new FXString(res->getCellString(i,1));
        ret[i].title=new FXString(res->getCellString(i,2));
        ret[i].solid=res->getCellInt(i,3);
    }
    return ret;
}

cDataResult *cAssayManager::getAssay(const FXString &prId)
{
    FXString res=prId;
    return oDataLink->execute("SELECT * FROM t_assays WHERE id='"+res.trim().substitute("'","")+"';");
}

cDataResult *cAssayManager::getAssay(int prOid)
{
    return oDataLink->execute("SELECT * FROM t_assays WHERE oid='"+FXStringVal(prOid)+"';");
}

int cAssayManager::getAssayOid(const FXString &prId)
{
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT oid FROM t_assays WHERE id='"+ress.trim().substitute("'","")+"';");
    int ret=res==NULL?-1:res->getCellInt(0,0);
    return ret;
}

FXString cAssayManager::newAssay(FXMDIClient *prP, FXPopup *prPup)
{
    FXString ret=getNewID();
    cMDIAssay *asyWin=new cMDIAssay(prP,ret,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,589,370);
    asyWin->create();
    asyWin->setFocus();
    return ret;
}

FXString cAssayManager::editAssay(FXMDIClient *prP, FXPopup *prPup,const FXString &prId)
{
    cMDIAssay *asyWin=new cMDIAssay(prP,prId,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,589,370);
    asyWin->create();
    asyWin->loadAssay(prId);
    asyWin->setFocus();
    return prId;
}

FXString cAssayManager::duplicateAssay(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return "";
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT * FROM t_assays WHERE id='"+ress.trim().substitute("'","")+"';");
    if(res->getRowCount()<=0)
        return "";
    int i=1;
    FXString ret=res->getCellString(0,0);
    while(assayExists(ret))
        ret=res->getCellString(0,0)+FXStringFormat(" #%d",i++);
    FXString res2="INSERT INTO t_assays VALUES('"+ret.trim().substitute("'","")+"'";
    for(i=1;i<res->getFieldCount();i++)
        res2=res2+",'"+(i==2?"0":res->getCellString(0,i).trim().substitute("'",""))+"'";
    res2=res2+");";
    res=oDataLink->execute(res2);
    return ret;
}

FXbool cAssayManager::addAssay(FXString prId,FXString prTitle,FXString prFilterA,FXString prFilterB,FXString prControls_defs,FXString prCalculation,FXString prUnit,FXString prComments)
{
    return addAssay(prId,"0",prTitle,prFilterA,prFilterB,prControls_defs,prCalculation,prUnit,prComments);
}

FXbool cAssayManager::addAssay(FXString prId,FXString prSolid,FXString prTitle,FXString prFilterA,FXString prFilterB,FXString prControls_defs,FXString prCalculation,FXString prUnit,FXString prComments)
{
    return addAssay(oDataLink,prId,prSolid,prTitle,prFilterA,prFilterB,prControls_defs,prCalculation,prUnit,prComments);
}

FXbool cAssayManager::addAssay(cDataLink *prLink,FXString prId,FXString prSolid,FXString prTitle,FXString prFilterA,FXString prFilterB,FXString prControls_defs,FXString prCalculation,FXString prUnit,FXString prComments)
{
    if(!prLink->isOpened())
        return false;
    prLink->execute("INSERT INTO t_assays VALUES('"+
                                                    prId.trim().substitute("'","")+"','"+
                                                    prTitle.trim().substitute("'","")+"','"+
                                                    prSolid.trim().substitute("'","")+"','"+
                                                    prFilterA.trim().substitute("'","")+"','"+
                                                    prFilterB.trim().substitute("'","")+"','"+
                                                    prControls_defs.trim().substitute("'","")+"','"+
                                                    prCalculation.trim().substitute("'","")+"','"+
                                                    prUnit.trim().substitute("'","")+"','"+
                                                    prComments.trim().substitute("'","")+"');");
    return (prLink->getAffectedRowCount()==1);
}

FXbool cAssayManager::setAssay(FXString prOldId,FXString prId,FXString prTitle,FXString prFilterA,FXString prFilterB,FXString prControls_defs,FXString prCalculation,FXString prUnit,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    bool are = false;
    for(int i=oWinMain->getReadings()->first();i<=oWinMain->getReadings()->last();i=oWinMain->getReadings()->next(i)) {
        are = true;
    }
    if(are) {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_closereadings").text());
        return false;
    }
    /*cDataResult *res_=oDataLink->execute("SELECT id FROM t_gncases WHERE assay_oid='"+prOldId+"';");
    cDataResult *res0=getAssay(prOldId);
    if((res_->getRowCount()>0) && (!cFormulaRatio::testChangeCompatible(prCalculation,
                                            res0->getCellString(0,6),
                                            prRules_defs,
                                            res0->getCellString(0,10))))
    {
        delete res_;
        delete res0;
        return false;
    }
    delete res_;
    delete res0;*/
    oDataLink->execute("UPDATE t_assays SET id='"+prId.trim().substitute("'","")+
                       "', title='"+prTitle.trim().substitute("'","")+
                       "', solid='0"+
                       "', filterA='"+prFilterA.trim().substitute("'","")+
                       "', filterB='"+prFilterB.trim().substitute("'","")+
                       "', controls_defs='"+prControls_defs.trim().substitute("'","")+
                       "', calculation='"+prCalculation.trim().substitute("'","")+
                       "', unit='"+prUnit.trim().substitute("'","")+
                       "', comments='"+prComments.trim().substitute("'","")+
                       "' WHERE id='"+prOldId+"' AND solid='0';");
    int ret=oDataLink->getAffectedRowCount();
    oDataLink->execute("UPDATE t_gncases SET assay_oid='"+prId.trim().substitute("'","")+"' WHERE assay_oid='"+prOldId+"';");

    sCDLGO *dt=new sCDLGO;
    dt->id=new FXString(prOldId);
    dt->id2=new FXString(prId);
    dt->asy=new FXString(prTitle);
    dt->res=getAssay(prId);
    delete dt->id;
    delete dt->id2;
    delete dt->asy;
    //delete dt;
    return (ret==1);
}

FXbool cAssayManager::removeAssay(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;

    return (oDataLink->execute("DELETE FROM t_assays WHERE id='"+ress.trim().substitute("'","")+"' AND solid='0';")!=NULL);
}
