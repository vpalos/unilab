#include <stdio.h>
#include <strings.h>
#include <locale.h>
#include "engine.h"
#include "cLanguage.h"

cLanguage *oLanguage;

cLanguage::cLanguage()
{
    help=new FXSettings();
    dictionary=new FXStringDict();
    loaded=false;
}

cLanguage::~cLanguage()
{
}

FXbool cLanguage::loadDesignated(bool prChoose)
{
    FXString lang;
    if(!prChoose)
        lang=oApplicationManager->reg().readStringEntry("settings","language","");
    if(prChoose || lang.empty())
    {
        FXString *list;
        FXString langs="";
        int p=1,i,fno=FXDir::listFiles(list,"./core-packages/","l*",FXDir::NoFiles|FXDir::NoParent);
        bool re=true;
        while(re)
        {
            re=false;
            for(i=0;i<fno-p;i++)
            {
                if(strcmp(list[i].text(),list[i+1].text())>0)
                {
                    FXString s=list[i];
                    list[i]=list[i+1];
                    list[i+1]=s;
                    re=true;
                }
            }
            p++;
        }

        if(fno)
        {
            int j=0;
            for(i=0;i<fno;i++,j++)
            {
                if(!FXStat::exists("./core-packages/"+list[i]+"/help.lang")||
                   !FXStat::exists("./core-packages/"+list[i]+"/strings.lang"))
                {
                    list[i]="";
                    continue;
                }
                langs=langs+(langs.empty()?"":"\n");
                langs=langs+list[i];
            }
            langs=langs+"\n";
            if(!j)
                return loadDesignated("English");
            int cnt;
            if(j==1)
            {
                j--;
                while(list[j].empty() && (j<fno)){j++;}
                return loadDesignated(list[j].after('l'));
            }
            if(!APP_CREATED)
            {
                APP_CREATED=true;
                oApplicationManager->create();
            }
            if((cnt=FXChoiceBox::ask(oApplicationManager,0,"Languages","Please choose a language.\nClick \"Cancel\" for english default.",NULL,langs))==-1)
            {
                if(prChoose)
                    return saveDesignated("English");
                else
                    return loadDesignated("English");
            }
            while(list[cnt].empty() && (cnt<fno)){cnt++;}
            if(cnt>=fno)
            {
                if(prChoose)
                    return saveDesignated("English");
                else
                    return loadDesignated("English");
            }
            if(prChoose)
                return saveDesignated(list[cnt].after('l'));
            else
                return loadDesignated(list[cnt].after('l'));
        }
    }
    return loadDesignated(lang);
}

FXbool cLanguage::loadDesignated(const FXString &prLanguage)
{
    FILE *fp;
    FXString key,buf,language,local1,lhelp;

    size_t n=1024;
    FXchar *buffer=new FXchar[1024];
    if(!buffer)
        fatalError("Could not allocate memory for reading the dictionary: 1024 bytes");

    language=PACKAGES_PATH+"l";
    storedLanguage=prLanguage;
    language+=storedLanguage;
    if(!FXStat::isDirectory(language))
        fatalError("Specified language not found: "+storedLanguage);
    language+=PATHSEP;
    local1=language;
    lhelp=language;
    local1+="strings.lang";
    lhelp+="help.lang";
    if(!help->parseFile(lhelp,false))
    {
        fatalError("Could not parse help file:\n\n"+lhelp);
    }
    if(loaded)
        dictionary->clear();
    if(!(fp=fopen(local1.text(),"r")))
        fatalError("Could not open language files:\n\n"+local1+"\n"+local1);
    while(!feof(fp))
    {
        if(fgets(buffer,n,fp))
        {
            buf=buffer;
            key=buf.before('=').trim();
            if(key.length()>0)
                dictionary->insert(key.text(),buf.after('=').trim().text());
        }
        else
            break;
    }
    fclose(fp);
    delete buffer;
    loaded=true;
    oApplicationManager->reg().readStringEntry("settings","language",language.text());
    saveDesignated(prLanguage);
    return true;
}

FXbool cLanguage::saveDesignated(void)
{
    if(!loaded)
        loadDesignated();
    oApplicationManager->reg().writeStringEntry("settings","language",storedLanguage.text());
    oApplicationManager->reg().write();
    return true;
}

FXbool cLanguage::saveDesignated(const FXString &prLanguage)
{
    storedLanguage=prLanguage;
    oApplicationManager->reg().writeStringEntry("settings","language",prLanguage.text());
    oApplicationManager->reg().write();
    return true;
}

FXString cLanguage::getLanguage(void)
{
    return storedLanguage;
}

FXString cLanguage::getText(const FXString &prKey)
{
    FXString ret;
    if(!loaded)
        loadDesignated();
    ret=dictionary->find(prKey.text());
    return ret.substitute("\\\\","\\").substitute("\\n","\n").substitute("\\t","\t");
}

int cLanguage::canQuit(void)
{
    return 1;
}

void cLanguage::saveSettings(void)
{
    this->saveDesignated();
}


