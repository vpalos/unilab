#ifndef CDATABASEMANAGER_H
#define CDATABASEMANAGER_H

#include <fx.h>

typedef struct sDatabaseObject
{
    FXString *filename;
    FXString *title;
    FXString *owner;
    FXString *comments;
};

class cDatabaseManager
{
    private:
    protected:
    public:
        static int getDatabaseCount(void);

        static sDatabaseObject *listDatabases(void);

        static FXbool databaseExists(const FXString &prTitle);

        static FXString newDatabase();
        static FXString editDatabase(const FXString &prFilename);
        static FXString duplicateDatabase(const FXString &prFilename);

        static FXbool removeDatabase(const FXString &prFilename);
};

#endif
