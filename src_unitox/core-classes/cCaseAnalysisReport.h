#ifndef CCASEANALYSISREPORT_H
#define CCASEANALYSISREPORT_H

#include <fx.h>
#include "cDLGReportCA.h"
#include "cMDIReadings.h"
#include "cMDIReport.h"
#include "cSortList.h"

class cCaseAnalysisReport
{
    private:
        cDLGReportCA *dlg;
        cMDIReport *repWin;
        cColorTable *plateTable2;
        cColorTable *statsTable;

        FXbool useGraphs;
        FXbool useData;
        FXbool useInfo;

        sCasesSet *casesSets;
        int tSetsCount;
        int tCasesCount;
        int tPlatesCount;

    protected:
        FXbool loadReadings(cSortList &prCases);
        FXbool processReadings(void);
        FXbool askPrefs(void);

    public:
        static void openCases(FXMDIClient *prP, FXPopup *prPup,cSortList &prCases);
};

#endif
