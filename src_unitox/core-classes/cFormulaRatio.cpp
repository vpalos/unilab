#include <fx.h>
#include <strings.h>
#include <math.h>
#include "engine.h"
#include "cVChart.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cFormulaRatio.h"

#define FORMULA_COUNT 2

int cFormulaRatio::getCount(void)
{
    return FORMULA_COUNT;
}

FXString cFormulaRatio::formulaTitle(FXString *prFormula) {
    FXString ret="";

    sFormulaRatioObject *res=cFormulaRatio::listFormulaRatio();

    for(int i = 0; i < getCount(); i++) {
        if(*res[i].id == *prFormula) {
            ret = FXStringFormat("%s - %s", res[i].id->text(), res[i].title->text());
            delete res;
            return ret;
        }
    }
    delete res;
    return ret;
}

sFormulaRatioObject *cFormulaRatio::listFormulaRatio(void)
{
    sFormulaRatioObject *ret=(sFormulaRatioObject *)malloc(FORMULA_COUNT*sizeof(sFormulaRatioObject));
    if(!ret)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }

    int i=0;
    ret[i].id=new FXString("Logit/Log");
    ret[i].title=new FXString(oLanguage->getText("str_fratio_logit_title"));
    ret[i].flags=FORMULA_ALL|FORMULA_DEFAULT;

    i++;
    ret[i].id=new FXString("Linear Reg.");
    ret[i].title=new FXString(oLanguage->getText("str_fratio_linreg_title"));
    ret[i].flags=FORMULA_ALL;

    return ret;
}

FXdouble cFormulaRatio::cOD(FXint prIndex, FXdouble *prControlsData, FXint prCReplicates) {
    if(prCReplicates) {
        return (prControlsData[prIndex * 2] + prControlsData[prIndex * 2 + 1]) / 2.0;
    }
    else {
        return prControlsData[prIndex];
    }
}

FXuint cFormulaRatio::calculateChartType(FXString *prFormula) {
    if(*prFormula=="Logit/Log") {
        return cVChart::CHART_LOG;
    }
    else if(*prFormula=="Linear Reg.") {
        return cVChart::CHART_LINEAR;
    }
    return cVChart::CHART_LOG;
}

FXString cFormulaRatio::calculateChartVAxis(FXString *prFormula) {
    if(*prFormula=="Logit/Log") {
        return oLanguage->getText("str_fratio_logit_vaxis");
    }
    else if(*prFormula=="Linear Reg.") {
        return oLanguage->getText("str_fratio_linreg_vaxis");
    }
    return "---";
}

sCResult cFormulaRatio::calculateControl(FXString *prFormula, FXint prIndex,
    FXdouble *prControlsData, FXint *prControlsLayout, FXint prControlsCount, FXint prCReplicates,
    FXbool prAsySC, FXdouble *prControlsAsy, FXint *prControlsAsyBBO, FXint prControlsAsyCount) {
    sCResult ret={0,0,0,0};

    if(((prControlsAsyCount != prControlsCount) && !prAsySC) ||
       (prAsySC && prControlsCount != 1))
        return ret;

    if(*prFormula=="Logit/Log") {
        // FIRST OD
        FXdouble first_od = cOD(0, prControlsData, prCReplicates);

        // OD
        ret.od = (prAsySC && prIndex) ? (prControlsAsyBBO[prIndex] * first_od) / 100 : cOD(prIndex, prControlsData, prCReplicates);

        // IPPU
        ret.ippu = prControlsAsy[prIndex];

        // BBO
        if(prAsySC) {
            ret.bbo = prIndex == 0 ? 100 : prControlsAsyBBO[prIndex];
        }
        else {
            ret.bbo = prIndex == 0 ? 100 : (ret.od / first_od * 100) ;
        }

        // Linear Reg. Sums
        FXdouble sx = 0;
        FXdouble sy = 0;
        FXdouble sxy = 0;
        FXdouble sx2 = 0;
        FXdouble sy2 = 0;

        double v, t;
        for(int i = 1; i < prControlsAsyCount; i++) {
            v = log(prControlsAsy[i]);
            sx += v;
            sx2 += v * v;

            if(prAsySC) {
                t = prControlsAsyBBO[i];
            }
            else {
                t = cOD(i, prControlsData, prCReplicates) / first_od * 100;
            }
            t = log(t / (100 - t));
            sy += t;
            sy2 += t * t;

            sxy += v * t;
        }

        FXdouble s2x = sx * sx;
        FXdouble s2y = sy * sy;

        // Control Count (without 0)
        FXdouble count = prControlsAsyCount - 1;

        // Linear Reg. Slope/Intercept/Correlation Coefficient
        FXdouble lr_slope = (count * sxy - sx * sy)/(count * sx2 - s2x);
        FXdouble lr_intercept = (sy - lr_slope * sx) / count;
        FXdouble lr_correlation = fabs((count * sxy - sx * sy)/sqrt((count * sx2 - s2x) * (count * sy2 - s2y)));

        if(prIndex == 0) {
            ret.cppu = 0;
            ret.var = 0;
            return ret;
        }
        FXdouble logit = log(ret.bbo/(100.0 - ret.bbo));
        ret.y = logit;

        ret.cppu = exp((logit - lr_intercept) / lr_slope);
        ret.corr = lr_correlation;
        ret.ty = lr_slope*log(ret.ippu) + lr_intercept;

        v = ret.cppu;
        t = ret.ippu;
        t = fabs(t - v);
        v = fabs(ret.ippu);
        ret.var = t / v * 100;

        if(ret.cppu < 0.0)
            ret.var = +0.0;
    }
    else if(*prFormula=="Linear Reg.") {
        // FIRST OD
        FXdouble first_od = cOD(0, prControlsData, prCReplicates);

        // OD
        ret.od = (prAsySC && prIndex) ? (prControlsAsyBBO[prIndex] * first_od) / 100 : cOD(prIndex, prControlsData, prCReplicates);

        // IPPU
        ret.ippu = prControlsAsy[prIndex];

        // BBO
        if(prAsySC) {
            ret.bbo = prIndex == 0 ? 100 : prControlsAsyBBO[prIndex];
        }
        else {
            ret.bbo = prIndex == 0 ? 100 : (ret.od / first_od * 100) ;
        }

        // Linear Reg. Sums
        FXdouble sx = 0;
        FXdouble sy = 0;
        FXdouble sxy = 0;
        FXdouble sx2 = 0;
        FXdouble sy2 = 0;

        double v, t;
        for(int i = 0; i < prControlsAsyCount; i++) {
            v = prControlsAsy[i];
            sx += v;
            sx2 += v * v;

            if(prAsySC) {
                t = (prControlsAsyBBO[i] * first_od) / 100;
            }
            else {
                t = cOD(i, prControlsData, prCReplicates);
            }

            sy += t;
            sy2 += t * t;

            sxy += v * t;
        }

        FXdouble s2x = sx * sx;
        FXdouble s2y = sy * sy;

        // Control Count (without 0)
        FXdouble count = prControlsAsyCount;

        // Linear Reg. Slope/Intercept/Correlation Coefficient
        FXdouble lr_slope = (count * sxy - sx * sy)/(count * sx2 - s2x);
        FXdouble lr_intercept = (sy - lr_slope * sx) / count;
        FXdouble lr_correlation = fabs((count * sxy - sx * sy)/sqrt((count * sx2 - s2x) * (count * sy2 - s2y)));

        FXdouble y = ret.od;
        ret.y = y;

        ret.cppu = (y - lr_intercept) / lr_slope;
        ret.corr = lr_correlation;
        ret.ty = lr_slope*ret.ippu + lr_intercept;



        v = ret.cppu;
        t = ret.ippu;
        t = fabs(t - v);
        v = fabs(ret.ippu);
        ret.var = t / v * 100;

        if(ret.cppu < 0.0)
            ret.var = +0.0;
    }

    return ret;
}

sSResult cFormulaRatio::calculateSample(FXString *prFormula, FXdouble prValue, FXint prReplicates, FXint prFactor,
    FXdouble *prControlsData, FXint *prControlsLayout, FXint prControlsCount, FXint prCReplicates,
    FXbool prAsySC, FXdouble *prControlsAsy, FXint *prControlsAsyBBO, FXint prControlsAsyCount) {
    sSResult ret={0,0,0,0};

    if(((prControlsAsyCount != prControlsCount) && !prAsySC) ||
       (prAsySC && prControlsCount != 1))
        return ret;

    if(*prFormula=="Logit/Log") {
        // FIRST OD
        FXdouble first_od = cOD(0, prControlsData, prCReplicates);

        // MIN CPPU (FOR NEG)
        //FXdouble min_cppu = prControlsAsy[1];

        // MAX CPPU
        FXdouble max_cppu = prControlsAsy[prControlsAsyCount - 1];
        FXdouble max_logit = 0;

        // OD
        ret.od = prValue;

        // BBO
        ret.bbo = ret.od / first_od * 100 ;

        // Linear Reg. Sums
        FXdouble sx = 0;
        FXdouble sy = 0;
        FXdouble sxy = 0;
        FXdouble sx2 = 0;
        FXdouble sy2 = 0;

        double v, t;
        for(int i = 1; i < prControlsAsyCount; i++) {
            v = log(prControlsAsy[i]);
            sx += v;
            sx2 += v * v;

            if(prAsySC) {
                t = prControlsAsyBBO[i];
            }
            else {
                t = cOD(i, prControlsData, prCReplicates) / first_od * 100;
            }
            t = log(t / (100 - t));
            sy += t;
            sy2 += t * t;

            sxy += v * t;
            if(i == prControlsAsyCount - 1) {
                max_logit = t;
            }
        }

        FXdouble s2x = sx * sx;
        FXdouble s2y = sy * sy;

        // Control Count (without 0)
        FXdouble count = prControlsAsyCount - 1;

        // Linear Reg. Slope/Intercept/Correlation Coefficient
        FXdouble lr_slope = (count * sxy - sx * sy)/(count * sx2 - s2x);
        FXdouble lr_intercept = (sy - lr_slope * sx) / count;
        FXdouble lr_correlation = fabs((count * sxy - sx * sy)/sqrt((count * sx2 - s2x) * (count * sy2 - s2y)));

        if(max_logit) {
            max_cppu = exp((max_logit - lr_intercept) / lr_slope);
        }

        FXdouble logit = log(ret.bbo/(100.0 - ret.bbo));

        ret.cppu = exp((logit - lr_intercept) / lr_slope);

        ret.cppu *= prFactor;

        ret.result = 0;

        if(ret.cppu > max_cppu)
            ret.result = 1;

        if(ret.cppu < 0.0)
            ret.result = +0.0;
    }
    else if(*prFormula=="Linear Reg.") {
        // FIRST OD
        FXdouble first_od = cOD(0, prControlsData, prCReplicates);

        // MIN CPPU (FOR NEG)
        //FXdouble min_cppu = prControlsAsy[1];

        // MAX CPPU
        FXdouble max_cppu = prControlsAsy[prControlsAsyCount - 1];
        FXdouble max_y = 0;

        // OD
        ret.od = prValue;

        // BBO
        ret.bbo = ret.od / first_od * 100 ;

        // Linear Reg. Sums
        FXdouble sx = 0;
        FXdouble sy = 0;
        FXdouble sxy = 0;
        FXdouble sx2 = 0;
        FXdouble sy2 = 0;

        double v, t;
        for(int i = 0; i < prControlsAsyCount; i++) {
            v = prControlsAsy[i];
            sx += v;
            sx2 += v * v;

            if(prAsySC) {
                t = (prControlsAsyBBO[i] * first_od) / 100;
            }
            else {
                t = cOD(i, prControlsData, prCReplicates);
            }

            sy += t;
            sy2 += t * t;

            sxy += v * t;
            if(i == prControlsAsyCount - 1) {
                max_y = t;
            }
        }

        FXdouble s2x = sx * sx;
        FXdouble s2y = sy * sy;

        // Control Count (without 0)
        FXdouble count = prControlsAsyCount;

        // Linear Reg. Slope/Intercept/Correlation Coefficient
        FXdouble lr_slope = (count * sxy - sx * sy)/(count * sx2 - s2x);
        FXdouble lr_intercept = (sy - lr_slope * sx) / count;
        FXdouble lr_correlation = fabs((count * sxy - sx * sy)/sqrt((count * sx2 - s2x) * (count * sy2 - s2y)));
        //fxmessage("sx=%g, sx2=%g, s2x=%g, sy=%g, sy2=%g, s2y=%g, sxy=%g, lr_slope = %g, lr_intercept = %g \n", sx, sx2, s2x, sy, sy2, s2y, sxy, lr_slope, lr_intercept);

        if(max_y) {
            max_cppu = (max_y - lr_intercept) / lr_slope;
        }

        FXdouble y = ret.od;

        ret.cppu = (y - lr_intercept) / lr_slope;

        ret.cppu *= prFactor;

        ret.result = 0;

        if(ret.cppu > max_cppu)
            ret.result = 1;

        if(ret.cppu < 0.0)
            ret.result = +0.0;
    }

    return ret;
}

FXbool cFormulaRatio::testCompatible(cDataResult *prAssayRes,cDataResult *prTemplateRes)
{
    FXbool compatible=true,ast=false,tpt=false;
    FXint k, controlsCount, act;

    FXString acontrols_defs = prAssayRes->getCellString(0,5);
    ast = FXIntVal(acontrols_defs.before('\n'));
    acontrols_defs=acontrols_defs.after('\n');
    act = 0;
    while(!acontrols_defs.empty())
    {
        acontrols_defs=acontrols_defs.after('\n');
        act++;
    }

    FXString controls_defs=prTemplateRes->getCellString(0,3);
    FXint controlsChoice=FXIntVal(controls_defs.before('\n'));
    switch(controlsChoice)
    {
    case 3:
        controls_defs=controls_defs.after('\n');
        controlsCount=0;
        do{
            k=FXIntVal(controls_defs.before('\t'));
            controls_defs=controls_defs.after('\t');
            if(k!=0 && k!=-100)
                controlsCount++;
        }while(k!=0 && k!=-100);
        tpt = controlsCount==1;
        break;
    default:
        controlsCount = 5;
        tpt = false;
        break;
    }

    if((ast && !tpt) || (!ast && tpt)) {
        compatible = false;
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_reading_incompat").text());
    }

    if(!ast && (act != controlsCount)) {
        compatible = false;
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_reading_incompatctl").text());
    }

    return compatible;
}

/*FXbool cFormulaRatio::testChangeCompatible(FXString prCal_defs,FXString prOldCal_defs,FXString prRul_defs,FXString prOldRul_defs)
{
    int ast=0,as2t=0;
    if(prCal_defs.find("2xELISA S/P")!=-1 ||
       prCal_defs.find("Raw S/NHC")!=-1)
        ast=1;
    else if(prRul_defs.find('h')!=-1)
        ast=1;
    if(prOldCal_defs.find("2xELISA S/P")!=-1 ||
       prOldCal_defs.find("Raw S/NHC")!=-1)
        as2t=1;
    else if(prOldRul_defs.find('h')!=-1)
        as2t=1;
    if(ast!=as2t)
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nochassay").text());
    return (ast==as2t);
}*/
