#ifndef CREADINGSMANAGER_H
#define CREADINGSMANAGER_H

#include <fx.h>
#include "cSortList.h"

typedef struct sReadingsObject
{
    FXString *id;
    FXString *date;
    FXString *assay;
    FXString *population;
};

class cReadingsManager
{
    private:
    protected:
    public:
        static FXbool makeReading(FXMDIClient *prP, FXPopup *prPup,FXString prAssay,FXString prTemplate,FXbool prManual=false,FXString prLot="",FXString prExp="");
        static double **remakeReading(const FXString &prFilterA,const FXString &prFilterB,FXint prPlateCount,FXint prShownPlateStart,FXint prShownPlateCount);
        static void openReading(FXMDIClient *prP, FXPopup *prPup,cSortList &prCases);
};

#endif
