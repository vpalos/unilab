#include <fx.h>
#include <strings.h>
#include <math.h>
#include "engine.h"
#include "cCaseSelector.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cFormulaRatio.h"
#include "cDataResult.h"
#include "cDataLink.h"

int cCaseSelector::getCriteriaCount(void)
{
    return 11;
}

sCriteriaObject *cCaseSelector::listCriteria(void)
{
    sCriteriaObject *ret;

    ret=(sCriteriaObject*)malloc(getCriteriaCount()*sizeof(sCriteriaObject));
    if(!ret)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }

    FXString attrs="id,commodity,assay_oid,template_oid,readingDate,replicates,factor,lot,expirationDate,technician,comments";

    for(int i=0;i<11;i++)
    {
        ret[i].name=new FXString(oLanguage->getText(FXStringFormat("str_csmdi_c%d",i)));
        ret[i].attribute=new FXString(attrs.before(','));
        attrs=attrs.after(',');
        ret[i].flag=CRITERIA_CASE;
        ret[i].type=CRITERIA_TEXT;
        ret[i].values1=NULL;
        ret[i].values2=NULL;
    }
    ret[4].type=CRITERIA_DATE;
    ret[8].type=CRITERIA_DATE;
    ret[5].type=CRITERIA_NUMBER;
    ret[6].type=CRITERIA_NUMBER;

    return ret;
}

sCaseObject *cCaseSelector::makeQuickSelection(FXString prCasea,FXString prCaseb,FXString prComma,FXString prCommb,FXString prDatea,FXString prDateb,FXIconList *prAssays,FXint *prCount)
{
    sCaseObject *ret;
    FXString criteria="";
    prCasea=prCasea.trim().substitute("'","");
    prCaseb=prCaseb.trim().substitute("'","");
    prComma=prComma.trim().substitute("'","");
    prCommb=prCommb.trim().substitute("'","");
    prDatea=prDatea.trim().substitute("'","");
    prDateb=prDateb.trim().substitute("'","");
    if(prCasea.empty() && !prCaseb.empty())
        prCasea=prCaseb;
    if(!prCasea.empty())
    {
        criteria="lower(id)";
        criteria=criteria+(prCaseb.empty()?" LIKE ":">=")+(prCaseb.empty()?"'%":"'")+prCasea.lower()+(prCaseb.empty()?"%'":"'")+(prCaseb.empty()?"":" AND lower(id)<='"+prCaseb.lower()+"'");
    }
    if(prComma.empty() && !prCommb.empty())
        prComma=prCommb;
    if(!prComma.empty())
    {
        criteria="lower(commodity)";
        criteria=criteria+(prCommb.empty()?" LIKE ":">=")+(prCommb.empty()?"'%":"'")+prComma.lower()+(prCommb.empty()?"%'":"'")+(prCommb.empty()?"":" AND lower(commodity)<='"+prCommb.lower()+"'");
    }
    if(prDatea.empty() && !prDateb.empty())
        prDatea=prDateb;
    if(!prDatea.empty())
        criteria=criteria+(criteria.empty()?"":" AND ")+"readingDate"+(prDateb.empty()?" LIKE ":">=")+(prDateb.empty()?"'%":"'")+prDatea+(prDateb.empty()?"%'":"'")+(prDateb.empty()?"":" AND readingDate<='"+prDateb+"'");
    int selcount=0;
    for(int i=0;i<prAssays->getNumItems();i++)
        if(prAssays->isItemSelected(i))
            selcount++;
    if(selcount)
    {
        criteria=criteria+(criteria.empty()?"":" AND ")+"(";
        int ti=0;
        for(int i=0;i<prAssays->getNumItems();i++)
            if(prAssays->isItemSelected(i))
                criteria=criteria+((ti++)>0?" OR ":"")+"assay_oid='"+prAssays->getItemText(i).before('\t')+"'";
        criteria=criteria+")";
    }
    ret=cCaseManager::listCases(criteria,prCount);
    return ret;
}

sCaseObject *cCaseSelector::makeAllSelection(FXint *prCount)
{
    sCaseObject *ret;
    ret=cCaseManager::listCases("",prCount);
    return ret;
}

sCaseObject *cCaseSelector::makeSelection(FXIconList *prCriteriaList,FXint *prCount)
{
    sCaseObject *ret;
    sCriteriaObject *co;

    FXString sc="",ssc="";
    int sct=0,ssct=0;

    for(int i=0;i<prCriteriaList->getNumItems();i++)
    {
        co=(sCriteriaObject*)prCriteriaList->getItemData(i);
        if(!co || !co->values1)
            continue;
        ssc=(co->flag==CRITERIA_CASE)?"":"lower(population_oid) in (SELECT lower(id) FROM t_gnpopulations WHERE ";
        ssct=0;
        for(int j=0;j<co->values1->no();j++)
        {
            bool isd=false;
            if((co->values2) && co->values2->data(j) && (FXString(co->values2->data(j)).trim().substitute("'","").length()>0))
                isd=true;

            bool isn=co->type==CRITERIA_NUMBER;

            ssc=ssc+(j>0?" OR (":"(");
            ssct=1;
            if(isn)
                ssc=ssc+*co->attribute+(isd?">=":"=");
            else
                ssc=ssc+"lower("+*co->attribute+(isd?")>=":") LIKE ");
            ssc=ssc+(isn?"'":"'%")+FXString(co->values1->data(j)).trim().lower().substitute("'","")+(isn?"'":"%'");
            if(isd)
            {
                ssc=ssc+" AND ";
                if(isn)
                    ssc=ssc+*co->attribute+"<=";
                else
                    ssc=ssc+"lower("+*co->attribute+")<=";
                ssc=ssc+"'"+FXString(isn?FXStringVal(FXIntVal(co->values2->data(j))):co->values2->data(j)).trim().lower().substitute("'","")+"'";
            }
            ssc=ssc+")";
        }
        if(co->flag!=CRITERIA_CASE)
            ssc=ssc+")";
        sc=sc+(sct?" AND (":"(")+ssc+")";
        sct=1;
    }

    ret=cCaseManager::listCases(sc,prCount);

    return ret;
}
