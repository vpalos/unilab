#ifndef CCASEEXPORT_H
#define CCASEEXPORT_H

#include <fx.h>
#include "cSortList.h"

class cCaseExport
{
    private:
    protected:
        static FXString getFileName(void);
        static FXbool processData(cSortList &prCases,FXString prFile);
        
    public:
        static void openCases(FXMDIClient *prP, FXPopup *prPup,cSortList &prCases);
};

#endif
