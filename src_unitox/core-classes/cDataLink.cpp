#include <stdio.h>
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cWinMain.h"
#include "cAssayManager.h"
#include "cMDICaseManager.h"

cDataLink *oDataLink;

cDataLink::cDataLink()
{
    link=NULL;
    opened=false;
    dbFile="";
    dbName="";
}

cDataLink::~cDataLink()
{
}

void cDataLink::vacuumDatabase(void)
{
    if(!opened)
        return;

    execute("VACUUM;");
}

void cDataLink::executeSqlBase(void)
{
    if(!opened)
        return;

    execute("\
            CREATE TABLE t_properties(property,value);\
            \
            CREATE TABLE t_templates(id,date,controls_replicates,controls_defs,cases_defs);\
            \
            CREATE TABLE t_assays(id,title,solid,filterA,filterB,controls_defs,calculation,unit,comments);\
            \
            CREATE TABLE t_gncases(id,reading_oid,readingDate,technician,assay_oid,\
                                   lot,expirationDate,commodity,replicates,factor,\
                                   template_oid,startPlate,startCell,plateCount,controls_defs,data_defs,comments);\
            \
            INSERT INTO t_properties VALUES('database_version','1.0');\
            \
            CREATE TABLE t_reserved0(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved1(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved2(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved3(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved4(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved5(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved6(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved7(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved8(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved9(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            \
            ",false);
}

void cDataLink::ensureKitUpgrade(FXbool prSilent)
{
    if(!opened)
        return;
    FXDict *dict=new FXDict();

    sDefAssay *def;

    def=new sDefAssay;
    def->id="DON.aft";
    def->title="Deoxynivalenol";
    def->solid="1";
    def->filterA="450";
    def->filterB="0";
    def->controls_defs="0\n0\t100\n0.333\t75.9\n1\t54.4\n3\t26.7\n6\t17.8\n";
    def->calculation="Logit/Log";
    def->unit="ppm";
    def->comments="";
    dict->insert(FXStringFormat("%d",dict->no()).text(),def);

    bool first=true;
    for(int i=0;i<dict->no();i++)
    {
        def=(sDefAssay*)dict->find(FXStringFormat("%d",i).text());
        if(!def)
            continue;
        if(!cAssayManager::assayExists(def->id))
        {
            if(first)
            {
                if(!prSilent)
                {
                    if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_upgdb").text()))
                        break;
                }
                first=false;
            }
            cAssayManager::addAssay(def->id,def->solid,def->title,def->filterA,def->filterB,def->controls_defs,def->calculation,def->unit,def->comments);
        }
    }

    delete dict;
}

FXbool cDataLink::ensureUpgrade(void)
{
    if(getProperty("database_version")=="0.1")
    {
        setProperty("database_version","1.0");
    }
    return true;
}

FXbool cDataLink::open(const FXString &prDBFile,FXbool prCreate,FXObject *prSender,FXbool prNoInv)
{
    if(prDBFile.empty())
        return true;
    if(opened)
    {
        if(dbFile==prDBFile)
            return true;
        if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_changedb").text()))
            return false;
        oWinMain->closeAllWindows(prSender);
        if(!close())
            return false;
    }

    FXbool dbexists=true;
    sqlite3 *olink=link;

    if(!FXStat::exists(prDBFile))
    {
        if(!prCreate)
        {
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_opendberror").text());
            return false;
        }
        dbexists=false;
    }

    if(dbexists && (FXStat::isFile(prDBFile)) && (!FXStat::isWritable(prDBFile)))
    {
        if(prSender || (!prNoInv))
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_dbro").text());
        link=olink;
        return false;
    }

    if(sqlite3_open(prDBFile.text(),&link)!=SQLITE_OK)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),"%s (%s)\n\n%s",sqlite3_errmsg(link),prDBFile.text(),
                              dbexists?oLanguage->getText("str_opendberror").text():oLanguage->getText("str_newdberror").text());
        sqlite3_close(link);
        link=olink;
        return false;
    }

    opened=true;
    if(dbexists && "myco"!=this->getProperty("database_species").lower())
    {
        if(!prNoInv)
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),"%s\n\n%s",prDBFile.text(),
                                  oLanguage->getText("str_invdberror").text());

        sqlite3_close(link);
        link=olink;
        opened=false;
        return false;
    }

    if(olink)
        sqlite3_close(olink);

    dbFile=prDBFile;
    if(!dbexists)
        executeSqlBase();
    dbName=this->getProperty("database_name");
    cMDICaseManager::cmdRefresh();
    cMDICaseManager::cmdResearch();
    return true;
}

FXbool cDataLink::openDesignated(void)
{
    FXString ret=oApplicationManager->reg().readStringEntry("settings","recent_db","");
    if(ret.empty())
        return true;
    return openDesignated(DATABASES_PATH+FXPath::name(ret),false);
}

FXbool cDataLink::openDesignated(const FXString &prDBFile,FXbool prCreate,FXObject *prSender)
{
    FXbool ret=open(prDBFile,prCreate,prSender);
    if(!ret)
        return false;

    FXString rules_defs=getProperty("database_version");
    FXuint version=0,v1=0,v2=0,v3=0;

    v1=FXIntVal(rules_defs.before('.'));
    rules_defs=rules_defs.after('.');
    v2=FXIntVal(rules_defs.before('.'));
    rules_defs=rules_defs.after('.');
    v3=FXIntVal(rules_defs);
    version=FXIntVal(FXStringFormat("1%03d%03d%03d",v1,v2,v3));
    if(version>DBS_VERSION_NO)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_dbnew_version").text());
        close();
        saveDesignated("");
        return false;
    }

    if(version<DBS_VERSION_NO)
    {
        if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_upgrade_db").text()) || !ensureUpgrade())
        {
            //FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_dbnew_version").text());
            close();
            saveDesignated("");
            return false;
        }
    }

    if(ret==true)
    {
        saveDesignated();
        vacuumDatabase();
    }
    else
        if(!opened)
            saveDesignated("");
    if(dbName=="Demo Database")
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_demodatabase").text());
    ensureKitUpgrade();
    return ret;
}

FXbool cDataLink::saveDesignated(void)
{
    if(opened)
        return saveDesignated(dbFile);
    return false;
}

FXbool cDataLink::saveDesignated(const FXString &prDBFile)
{
    oApplicationManager->reg().writeStringEntry("settings","recent_db",FXPath::name(prDBFile).text());
    oApplicationManager->reg().write();
    return true;
}

FXbool cDataLink::close(void)
{
    if(!opened)
        return false;
    if(SQLITE_OK!=sqlite3_close(link))
        return false;
    opened=false;
    link=NULL;
    dbName="";
    return true;
}

FXbool cDataLink::closeDesignated(void)
{
    if(!opened)
        return false;
    if(SQLITE_OK!=sqlite3_close(link))
        return false;
    opened=false;
    dbName="";
    link=NULL;
    saveDesignated("");
    return true;
}

FXbool cDataLink::isOpened(void)
{
    return opened;
}

FXString cDataLink::getFilename(void)
{
    return dbFile;
}

FXString cDataLink::getName(void)
{
    return dbName;
}

FXString cDataLink::getProperty(FXString prProperty)
{
    if(!opened)
        return "";
    cDataResult *res=execute("SELECT value FROM t_properties WHERE property='"+prProperty+"';");
    if(res->getRowCount()==0)
        return "";
    return res->getCellString(0,0);
}

FXbool cDataLink::setProperty(FXString prProperty,FXString prValue)
{
    if(!opened)
        return false;
    cDataResult *res=execute("SELECT value FROM t_properties WHERE property='"+prProperty.trim().substitute("'","")+"';");
    if(res->getRowCount()==0)
        res=execute("INSERT INTO t_properties VALUES('"+prProperty.trim().substitute("'","")+"','"+prValue.trim().substitute("'","")+"');");
    else
        res=execute("UPDATE t_properties SET value='"+prValue.trim().substitute("'","")+"' WHERE property='"+prProperty.trim().substitute("'","")+"';");
    if(!res || getAffectedRowCount()!=1)
        return false;
    return true;
}

FXString cDataLink::getInfo(void)
{
    if(!opened)
        return "";
    return sqlite3_libversion();
}

FXString cDataLink::getLastError(void)
{
    if(!opened)
        return "";
    return sqlite3_errmsg(link);
}

int cDataLink::getLastErrorCode(void)
{
    if(!opened)
        return SQLITE_OK;
    return sqlite3_errcode(link);
}

int cDataLink::getLastId(void)
{
    if(!opened)
        return -1;
    return sqlite3_last_insert_rowid(link);
}

int cDataLink::getAffectedRowCount(void)
{
    return sqlite3_changes(link);
}

FXbool cDataLink::tableExists(const FXString &prTable)
{
    if(!opened)
        return false;
    cDataResult *res=execute("SELECT oid FROM sqlite_master WHERE type='table' AND name='"+prTable+"';");
    if(res->getRowCount()>0)
        return true;
    return false;
}

cDataResult *cDataLink::execute(const FXString &prQuery,FXbool prAsk)
{
    if(!opened)
        return NULL;
    char **res,*errmsg;
    int nrows,ncols;

    //fxmessage("\nSQL: %s\n\n",prQuery.text());

    while(sqlite3_get_table(link,prQuery.text(),&res,&nrows,&ncols,&errmsg)!=SQLITE_OK)
        if(!prAsk || MBOX_CLICKED_NO==FXMessageBox::error(oWinMain,MBOX_YES_NO,oLanguage->getText("str_error").text(),"[%s]\n\n%s",
                                                             errmsg,oLanguage->getText("str_sqlerror").text()))
            return NULL;
    cDataResult *ret=new cDataResult(res,nrows,ncols);
    return ret;
}
