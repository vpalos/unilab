#ifndef CFORMULARATIO_H
#define CFORMULARATIO_H

#include <fx.h>
#include "cDataResult.h"

#define FORMULA_PRICAL 1
#define FORMULA_SECCAL 2
#define FORMULA_TITERS 4

#define FORMULA_DEFAULT 128

#define FORMULA_ALL 7

typedef struct sFormulaRatioObject
{
    FXString *id;
    FXString *title;
    int flags;
};

typedef struct sCResult
{
    FXdouble od;
    FXdouble ippu;
    FXdouble bbo;
    FXdouble cppu;
    FXdouble var;
    FXdouble y,ty;
    FXdouble corr;
};

typedef struct sSResult
{
    FXdouble od;
    FXdouble bbo;
    FXdouble cppu;
    FXint result; // 0 good value, 1 OVER, -1 DO NOT SHOW (neg)
};

class cFormulaRatio
{
    private:
    protected:
    public:
        static int getCount(void);

        static FXString formulaTitle(FXString *prFormula);
        static sFormulaRatioObject *listFormulaRatio(void);

        static FXdouble cOD(FXint prIndex, FXdouble *prControlsData, FXint prCReplicates);

        static FXuint calculateChartType(FXString *prFormula);
        static FXString calculateChartVAxis(FXString *prFormula);

        static sCResult calculateControl(FXString *prFormula, FXint prIndex,
            FXdouble *prControlsData, FXint *prControlsLayout, FXint prControlsCount, FXint prCReplicates,
            FXbool prAsySC, FXdouble *prControlsAsy, FXint *prControlsAsyBBO, FXint prControlsAsyCount);

        static sSResult calculateSample(FXString *prFormula, FXdouble prValue, FXint prReplicates, FXint prFactor,
            FXdouble *prControlsData, FXint *prControlsLayout, FXint prControlsCount, FXint prCReplicates,
            FXbool prAsySC, FXdouble *prControlsAsy, FXint *prControlsAsyBBO, FXint prControlsAsyCount);

        static FXbool testCompatible(cDataResult *prAssayRes,cDataResult *prTemplateRes);
        //static FXbool testChangeCompatible(FXString prCal_defs,FXString prOldCal_defs,FXString prRul_defs,FXString prOldRul_defs);
};

#endif
