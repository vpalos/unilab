#include <strings.h>
#include <time.h>
#include "engine.h"
#include "graphics.h"
#include "cWinMain.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cCaseExport.h"
#include "cCaseManager.h"
#include "cMDIDatabaseManager.h"

FXString cCaseExport::getFileName(void)
{
    FXString ret,dir=oApplicationManager->reg().readStringEntry("settings","exportdir",(FXSystem::getHomeDirectory()+PATHSEP).text());
    bool found=false;
    while(!found)
    {
        ret=FXFileDialog::getSaveFilename(oWinMain,oLanguage->getText("str_export_dlgtitle"),dir,oLanguage->getText("str_export_dlgpatt1")+" (*)\n"+oLanguage->getText("str_export_dlgpatt2")+" (*.utmyco)",1);
        if(!ret.empty())
        {
            dir=FXPath::directory(ret)+PATHSEP;
            oApplicationManager->reg().writeStringEntry("settings","exportdir",dir.text());
        }
        if(ret=="")
            return ret;
        if(FXPath::extension(ret).lower()!=("utmyco"))
            ret=ret+".utmyco";
        if(FXStat::exists(ret))
        {
            if(MBOX_CLICKED_YES==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_error").text(),oLanguage->getText("str_fileovr").text()))
                found=true;
        }
        else
            found=true;
    }
    return ret;
}

FXbool cCaseExport::processData(cSortList &prCases,FXString prFile)
{
    if(prFile.empty())
        return true;

    FXStringDict *assays=new FXStringDict();
    FXStringDict *tpls=new FXStringDict();

    FXStringDict *cids=new FXStringDict();
    FXStringDict *casy=new FXStringDict();

    int cc=0;

    int j=0;
    for(int i=0;i<prCases.getNumItems();i++)
        if(prCases.isItemSelected(i))
        {
            cDataResult *cse;
            cse=cCaseManager::getCase(prCases.getItemText(i).after('\t').before('\t'),prCases.getItemText(i).before('\t'));
            if(!cse || !cse->getRowCount())continue;

            cids->insert(FXStringFormat("%d",cc).text(),cse->getCellString(0,0).text());
            casy->insert(FXStringFormat("%d",cc++).text(),cse->getCellString(0,4).text());

            assays->insert(cse->getCellString(0,4).text(),"-");
            tpls->insert(cse->getCellString(0,10).text(),"-");

            delete cse;

            j++;
        }
    if(!j)
        return false;

    cDataResult *rr;
    cDataLink *link=new cDataLink();
    if(FXStat::exists(prFile))
        FXFile::remove(prFile.text());
    if(!link->open(prFile,true))
        return false;
    for(int i=0;i<cids->no();i++)
    {
        rr=oDataLink->execute("SELECT * FROM t_gncases WHERE id='"+
                              FXStringFormat("%s",cids->find(FXStringFormat("%d",i).text()))+"' AND assay_oid=='"+
                              FXStringFormat("%s",casy->find(FXStringFormat("%d",i).text()))+"';");
        if(rr && rr->getRowCount())
        {
            link->execute(FXStringFormat("INSERT INTO t_gncases VALUES('%s','%s','%s','%s','%s','%s','%s','%s',%s,%s,'%s',%s,%s,%s,'%s','%s','%s');",
                                    rr->getCellString(0,0).text(),
                                    rr->getCellString(0,1).text(),
                                    rr->getCellString(0,2).text(),
                                    rr->getCellString(0,3).text(),
                                    rr->getCellString(0,4).text(),
                                    rr->getCellString(0,5).text(),
                                    rr->getCellString(0,6).text(),
                                    rr->getCellString(0,7).text(),
                                    rr->getCellString(0,8).text(),
                                    rr->getCellString(0,9).text(),
                                    rr->getCellString(0,10).text(),
                                    rr->getCellString(0,11).text(),
                                    rr->getCellString(0,12).text(),
                                    rr->getCellString(0,13).text(),
                                    rr->getCellString(0,14).text(),
                                    rr->getCellString(0,15).text(),
                                    rr->getCellString(0,16).text()
                                    ).text());
        }
        delete rr;
    }

    rr=oDataLink->execute("SELECT * FROM t_assays;");
    for(int i=0;i<rr->getRowCount();i++)
    {
        if(assays->find(rr->getCellString(i,0).text()))
        {
            link->execute(FXStringFormat("INSERT INTO t_assays VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s');",
                                    rr->getCellString(i,0).text(),
                                    rr->getCellString(i,1).text(),
                                    rr->getCellString(i,2).text(),
                                    rr->getCellString(i,3).text(),
                                    rr->getCellString(i,4).text(),
                                    rr->getCellString(i,5).text(),
                                    rr->getCellString(i,6).text(),
                                    rr->getCellString(i,7).text(),
                                    rr->getCellString(i,8).text()
                                    ).text());
        }
    }
    delete rr;

    rr=oDataLink->execute("SELECT * FROM t_templates;");
    for(int i=0;i<rr->getRowCount();i++)
    {
        if(tpls->find(rr->getCellString(i,0).text()))
        {
            link->execute(FXStringFormat("INSERT INTO t_templates VALUES('%s','%s',%s,'%s','%s');",
                                    rr->getCellString(i,0).text(),
                                    rr->getCellString(i,1).text(),
                                    rr->getCellString(i,2).text(),
                                    rr->getCellString(i,3).text(),
                                    rr->getCellString(i,4).text()
                                    ).text());
        }
    }
    delete rr;

    FXchar ftime[100];
    time_t curtime;
    struct tm *loctime;
    curtime=time(NULL);
    loctime=localtime(&curtime);
    strftime(ftime,100,"%Y-%m-%d %H:%M:%S",loctime);
    FXString nowDate(ftime);
    link->setProperty("database_name",oLanguage->getText("str_export_dbname")+FXStringFormat(" \"%s\"",oDataLink->getName().text())+" - "+nowDate);
    link->setProperty("database_species","myco");
    link->setProperty("database_comments","");
    link->setProperty("database_owner",FXPath::title(prFile));

    link->close();

    delete assays;
    delete tpls;
    delete cids;
    delete casy;

    FXMessageBox::information(oWinMain,MBOX_OK,oLanguage->getText("str_information").text(),oLanguage->getText("str_success").text());
    return true;
}

void cCaseExport::openCases(FXMDIClient *prP, FXPopup *prPup,cSortList &prCases)
{
    if(MBOX_CLICKED_NO==FXMessageBox::warning(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_export_confirm").text()))
        return;
    if(!processData(prCases,getFileName()))
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_exporterror").text());
    else
        cMDIDatabaseManager::cmdRefresh();
}
