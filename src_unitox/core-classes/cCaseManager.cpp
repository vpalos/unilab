#include <ctype.h>
#include <stdio.h>
#include <strings.h>

#include "engine.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cDataLink.h"
#include "cCaseManager.h"
#include "cMDITemplate.h"

FXbool cCaseManager::caseExists(const FXString &prTitle,const FXString &prAssay)
{
    if(!oDataLink->isOpened())
        return false;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_gncases WHERE id='"+prTitle+"' AND assay_oid='"+prAssay+"';");
    return res->getRowCount()>0;
}

FXString cCaseManager::getNewID()
{
    return getNewID("");
}

FXString cCaseManager::getNewID(FXString prAssay)
{
    static int csNr=oApplicationManager->reg().readIntEntry("oids","cases",1);
    FXString res=oLanguage->getText("str_csmdi_title");
    FXString ret;
    cDataResult *res2;
    bool found;
    do
    {
        found=true;
        ret=res+FXStringFormat(" #%d",++csNr);
        res2=oDataLink->execute("SELECT id FROM t_gncases WHERE id='"+ret+(prAssay.empty()?"":"' AND assay_oid='"+prAssay)+"';");
        if(res2->getRowCount()>0)
            found=false;
    }while(!found);
    oApplicationManager->reg().writeIntEntry("oids","cases",csNr);
    return ret;
}

int cCaseManager::getCaseCount(const FXString &prCriteria)
{
    if(!oDataLink->isOpened())
        return 0;
    cDataResult *res=oDataLink->execute("SELECT COUNT(id) FROM t_gncases"+(prCriteria.empty()?"":" WHERE "+prCriteria)+";");
    return res->getCellInt(0,0);
}

sCaseObject *cCaseManager::listCases(const FXString &prCriteria,FXint *prCount)
{
    if(!oDataLink->isOpened())
        return NULL;

    *prCount=0;
    cDataResult *res=oDataLink->execute("SELECT id,readingDate,assay_oid,commodity,template_oid FROM t_gncases"+(prCriteria.empty()?"":" WHERE "+prCriteria)+";");
    if(!res->getRowCount())
        return NULL;
    *prCount=res->getRowCount();
    sCaseObject *ret=(sCaseObject*)malloc(*prCount*sizeof(sCaseObject));
    if(!ret)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }
    for(int i=0;i<res->getRowCount();i++)
    {
        ret[i].id=new FXString(res->getCellString(i,0));
        ret[i].readingDate=new FXString(res->getCellString(i,1));
        ret[i].assay_oid=new FXString(res->getCellString(i,2));
        ret[i].commodity=new FXString(res->getCellString(i,3));
        ret[i].tpl=new FXString(res->getCellString(i,4));
    }
    return ret;
}

/*FXString cCaseManager::editCase(FXMDIClient *prP, FXPopup *prPup,const FXString &prId)
{
    cMDICase *cseWin=new cMDICase(prP,prId,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,678,507);
    cseWin->create();
    cseWin->loadCase(prId);
    cseWin->setFocus();
    return prId;
}*/

FXbool cCaseManager::addCase(FXString prId,FXString prReading_oid,FXString prReadingDate,
    FXString prTechnician,FXString prAssay_oid,FXString prLot,FXString prExpirationDate,
    FXString prCommodity,FXint prReplicates,FXint prFactor,FXString prTemplate,FXint startPlate,
    FXint startCell,FXint plateCount,FXString prControls_defs,FXString prData_defs,FXString prComments)
{
    return addCase(oDataLink,prId,prReading_oid,prReadingDate,prTechnician,prAssay_oid,prLot,prExpirationDate,
        prCommodity,prReplicates,prFactor,prTemplate,startPlate,
        startCell,plateCount,prControls_defs,prData_defs,prComments);
}

FXbool cCaseManager::addCase(cDataLink *prLink,FXString prId,FXString prReading_oid,FXString prReadingDate,
    FXString prTechnician,FXString prAssay_oid,FXString prLot,FXString prExpirationDate,
    FXString prCommodity,FXint prReplicates,FXint prFactor,FXString prTemplate,FXint startPlate,
    FXint startCell,FXint plateCount,FXString prControls_defs,FXString prData_defs,FXString prComments)
{
    if(!prLink->isOpened())
        return false;
    prLink->execute("INSERT INTO t_gncases VALUES('"+
                                                    prId.trim().substitute("'","")+"','"+
                                                    prReading_oid.trim().substitute("'","")+"','"+
                                                    prReadingDate.trim().substitute("'","")+"','"+
                                                    prTechnician.trim().substitute("'","")+"','"+
                                                    prAssay_oid.trim().substitute("'","")+"','"+
                                                    prLot.trim().substitute("'","")+"','"+
                                                    prExpirationDate.trim().substitute("'","")+"','"+
                                                    prCommodity.trim().substitute("'","")+"','"+
                                                    FXStringVal(prReplicates)+"','"+
                                                    FXStringVal(prFactor)+"','"+
                                                    prTemplate.trim().substitute("'","")+"','"+
                                                    FXStringVal(startPlate)+"','"+
                                                    FXStringVal(startCell)+"','"+
                                                    FXStringVal(plateCount)+"','"+
                                                    prControls_defs.trim().substitute("'","")+"','"+
                                                    prData_defs.trim().substitute("'","")+"','"+
                                                    prComments.trim().substitute("'","")+"');");
    return (prLink->getAffectedRowCount()==1);
}

FXbool cCaseManager::setCase(FXString prOldId,FXString prOldAssay,FXString prId,
    FXString prCommodity,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("UPDATE t_gncases SET id='"+prId.trim().substitute("'","")+
                       "', commodity='"+prCommodity.trim().substitute("'","")+
                       "', comments='"+prComments.trim().substitute("'","")+
                       "' WHERE id='"+prOldId+"' AND assay_oid='"+prOldAssay+"';");

    return (oDataLink->getAffectedRowCount()==1);
}

FXbool cCaseManager::setCase(FXString prOldId,FXString prOldAssay,FXString prId,FXString prReading_oid,FXString prReadingDate,
    FXString prTechnician,FXString prAssay_oid,FXString prLot,FXString prExpirationDate,
    FXString prCommodity,FXint prReplicates,FXint prFactor,FXString prTemplate,FXint startPlate,
    FXint startCell,FXint plateCount,FXString prControls_defs,FXString prData_defs,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("UPDATE t_gncases SET id='"+prId.trim().substitute("'","")+
                       "', reading_oid='"+prReading_oid.trim().substitute("'","")+
                       "', readingDate='"+prReadingDate.trim().substitute("'","")+
                       "', technician='"+prTechnician.trim().substitute("'","")+
                       "', assay_oid='"+prAssay_oid.trim().substitute("'","")+
                       "', lot='"+prLot.trim().substitute("'","")+
                       "', expirationDate='"+prExpirationDate.trim().substitute("'","")+
                       "', commodity='"+prCommodity.trim().substitute("'","")+
                       "', replicates='"+FXStringVal(prReplicates)+
                       "', factor='"+FXStringVal(prFactor)+
                       "', template_oid='"+prTemplate.trim().substitute("'","")+
                       "', startPlate='"+FXStringVal(startPlate)+
                       "', startCell='"+FXStringVal(startCell)+
                       "', plateCount='"+FXStringVal(plateCount)+
                       "', controls_defs='"+prControls_defs.trim().substitute("'","")+
                       "', data_defs='"+prData_defs.trim().substitute("'","")+
                       "', comments='"+prComments.trim().substitute("'","")+
                       "' WHERE id='"+prOldId+"' AND assay_oid='"+prOldAssay+"';");

    return (oDataLink->getAffectedRowCount()==1);
}

cDataResult *cCaseManager::getCase(FXString prId,FXString prAssay)
{
    return oDataLink->execute("SELECT * FROM t_gncases WHERE id='"+prId+"' AND assay_oid='"+prAssay+"';");
}

FXbool cCaseManager::removeCase(const FXString &prId,const FXString &prAssay)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *ret;
    ret=oDataLink->execute("DELETE FROM t_gncases WHERE id='"+prId+"' AND assay_oid='"+prAssay+"';");
    return (ret!=NULL);
}
