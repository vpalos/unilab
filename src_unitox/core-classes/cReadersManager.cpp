#include <fx.h>
#include <strings.h>
#include <stdlib.h>

#include "../index.h"
#include "engine.h"
#include "cWinMain.h"
#include "cLanguage.h"
#include "cReadersManager.h"
#include "dReadersTEST.h"
#include "dReadersEMAX.h"
#include "dReadersELX800.h"
#include "dReadersMR5000.h"
#include "dReadersTSUNRISE.h"
#include "dReadersEL301.h"
#include "dReaders303PLUS.h"
#include "dReadersMSKANEXA.h"
#include "dReadersMSKANEXB.h"
#include "dReadersMSKANEXCUSTOM.h"
#include "dReadersPR2100.h"

cReadersObject::~cReadersObject()
{
}

double *cReadersObject::acquire(const FXString &prSerialDevice)
{
    return acquire(prSerialDevice,this->getSettings());
}
    
double *cReadersObject::acquire(const FXString &prSerialDevice,sReadersSettings prSettings)
{
    return acquire(prSerialDevice,prSettings.baudRate,prSettings.bits,prSettings.parity,prSettings.stopBits,prSettings.flow,prSettings.timeout);
}
    
int cReadersManager::getReadersCount(void)
{
    return 10;  // valoarea este 11 cu tot cu "(---) Test Reader"
}

cReadersObject *cReadersManager::getReadersObject(int prCode)
{
    cReadersObject *ret;
    switch(prCode)
    {
        //case READER_TEST:
        //    ret=new dReadersTEST();
        //    break;
        case READER_EMAX:
            ret=new dReadersEMAX();
            break;
        case READER_ELX800:
            ret=new dReadersELX800();
            break;
        case READER_MR5000:
            ret=new dReadersMR5000();
            break;
        case READER_MSKANEXA:
            ret=new dReadersMSKANEXA();
            break;
        case READER_MSKANEXB:
            ret=new dReadersMSKANEXB();
            break;
        case READER_MSKANEXCUSTOM:
            ret=new dReadersMSKANEXCUSTOM();
            break;
        case READER_TSUNRISE:
            ret=new dReadersTSUNRISE();
            break;
        case READER_EL301:
            ret=new dReadersEL301();
            break;
        case READER_303PLUS:
            ret=new dReaders303PLUS();
            break;
        case READER_PR2100:
            ret=new dReadersPR2100();
            break;
        default:
            ret=NULL;
    }
    return ret;
}

sReadersObject *cReadersManager::getReadersList(void)
{
    sReadersObject *ret=(sReadersObject*)malloc(getReadersCount()*sizeof(sReadersObject));
    cReadersObject *t;
    if(!ret)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }
    int i=-1;

    // (---) Test Reader
    //i++;
    //t=new dReadersTEST();
    //ret[i].label=new FXString("("+t->getManufacturer()+") "+t->getModel());
    //ret[i].code=t->getCode();
    //ret[i].settings=t->getSettings();
    //delete t;
    

    // (Molecular Devices) Emax
    i++;
    t=new dReadersEMAX();
    ret[i].label=new FXString("("+t->getManufacturer()+") "+t->getModel());
    ret[i].code=t->getCode();
    ret[i].settings=t->getSettings();
    delete t;
    
    // (Bio-Tek Instruments) ELx800
    i++;
    t=new dReadersELX800();
    ret[i].label=new FXString("("+t->getManufacturer()+") "+t->getModel());
    ret[i].code=t->getCode();
    ret[i].settings=t->getSettings();
    delete t;
    
    // (Bio-Tek Instruments) EL301
    i++;
    t=new dReadersEL301();
    ret[i].label=new FXString("("+t->getManufacturer()+") "+t->getModel());
    ret[i].code=t->getCode();
    ret[i].settings=t->getSettings();
    delete t;
    
    // (Labsystems) Multiskan EX (Standard Filter Set A)
    i++;
    t=new dReadersMSKANEXA();
    ret[i].label=new FXString("("+t->getManufacturer()+") "+t->getModel());
    ret[i].code=t->getCode();
    ret[i].settings=t->getSettings();
    delete t;
    
    // (Labsystems) Multiskan EX (Standard Filter Set B)
    i++;
    t=new dReadersMSKANEXB();
    ret[i].label=new FXString("("+t->getManufacturer()+") "+t->getModel());
    ret[i].code=t->getCode();
    ret[i].settings=t->getSettings();
    delete t;
    
    // (Labsystems) Multiskan EX (Custom Filter Set)
    i++;
    t=new dReadersMSKANEXCUSTOM();
    ret[i].label=new FXString("("+t->getManufacturer()+") "+t->getModel());
    ret[i].code=t->getCode();
    ret[i].settings=t->getSettings();
    delete t;
    
    // (Dynatech) MR5000
    i++;
    t=new dReadersMR5000();
    ret[i].label=new FXString("("+t->getManufacturer()+") "+t->getModel());
    ret[i].code=t->getCode();
    ret[i].settings=t->getSettings();
    delete t;
    
    // (Tecan) Sunrise
    i++;
    t=new dReadersTSUNRISE();
    ret[i].label=new FXString("("+t->getManufacturer()+") "+t->getModel());
    ret[i].code=t->getCode();
    ret[i].settings=t->getSettings();
    delete t;
    
    // (StatFax) 303 Plus
    i++;
    t=new dReaders303PLUS();
    ret[i].label=new FXString("("+t->getManufacturer()+") "+t->getModel());
    ret[i].code=t->getCode();
    ret[i].settings=t->getSettings();
    delete t;
    
    // (Dynatech) PR2100
    i++;
    t=new dReadersPR2100();
    ret[i].label=new FXString("("+t->getManufacturer()+") "+t->getModel());
    ret[i].code=t->getCode();
    ret[i].settings=t->getSettings();
    delete t;
    
    return ret;
}

double *cReadersManager::acquire(const FXString &prFilterA,const FXString &prFilterB,int prPlate,int prPlateCount)
{
    sReadersObject *ret=cReadersManager::getReadersList();
    int i,pos=0;
    FXString data=oApplicationManager->reg().readStringEntry("readercomm","reader","");
    for(i=0;i<cReadersManager::getReadersCount();i++)
        if(*(ret[i].label)==data)
            pos=i;
    cReadersObject *obj=cReadersManager::getReadersObject(ret[pos].code);
    if(ret==NULL || obj==NULL)
        return NULL;
    
    int baud,bits,parity,stop; 
    data=oApplicationManager->reg().readStringEntry("readercomm","baud","");
    switch(FXIntVal(data))
    {
        case 600:
            baud=BAUD600;
            break;
        case 1200:
            baud=BAUD1200;
            break;
        case 1800:
            baud=BAUD1800;
            break;
        case 2400:
            baud=BAUD2400;
            break;
        case 4800:
            baud=BAUD4800;
            break;
        case 9600:
            baud=BAUD9600;
            break;
        case 19200:
            baud=BAUD19200;
            break;
        case 38400:
            baud=BAUD38400;
            break;
        case 57600:
            baud=BAUD57600;
            break;
        case 115200:
            baud=BAUD115200;
            break;
        default:
            baud=ret[pos].settings.baudRate;
            break;
    }
    
    data=oApplicationManager->reg().readStringEntry("readercomm","data","");
    switch(FXIntVal(data))
    {
        case 5:
            bits=BITS5;
           break;
        case 6:
            bits=BITS6;
           break;
        case 7:
            bits=BITS7;
           break;
        case 8:
            bits=BITS8;
           break;
        default:
            bits=ret[pos].settings.bits;
            break;
    }
    
    data=oApplicationManager->reg().readStringEntry("readercomm","parity","");
    if(data=="None")
        parity=PARITYNONE;
    else if(data=="Odd")
        parity=PARITYODD;
    else if(data=="Even")
        parity=PARITYEVEN;
    else
        parity=ret[pos].settings.parity;
    
    data=oApplicationManager->reg().readStringEntry("readercomm","stop","");
    switch(FXIntVal(data))
    {
        case 1:
            stop=STOPBITS1;
           break;
        case 2:
            stop=STOPBITS2;
           break;
        default:
            stop=ret[pos].settings.stopBits;
            break;
    }
    
    data=oApplicationManager->reg().readStringEntry("readercomm","port","");
    if(!prFilterA.empty() && FXIntVal(prFilterA)!=0)
    {
        obj->setFilterA(prFilterA);
        if(!prFilterB.empty() && FXIntVal(prFilterB)!=0)
            obj->setFilterB(prFilterB);
        else
            obj->setFilterB("");
    }
    else
    {
        obj->setFilterA("");
        obj->setFilterB("");
    }
    while(oApplicationManager->peekEvent())
        oApplicationManager->runOneEvent(false);
    if(MBOX_CLICKED_CANCEL==FXMessageBox::information(oWinMain,MBOX_OK_CANCEL,FXStringFormat("%s %s",APP_TITLE,oLanguage->getText("str_reading_title").text()).text(),
                                                      ((prPlate==0?oLanguage->getText("str_reading_message")+oLanguage->getText("str_reading_plate").trim():oLanguage->getText("str_reading_plate").trim())+
                                                       FXStringFormat(" %d / %d...",prPlate+1,prPlateCount)).text()))
        return NULL;
    FXDialogBox msg(oWinMain,oLanguage->getText("str_reading_title"),DECOR_TITLE|DECOR_BORDER);
    new FXLabel(&msg,oLanguage->getText("str_reading_process")+FXStringFormat(" %d / %d ",prPlate+1,prPlateCount)+"\n\n"+
                oLanguage->getText("str_reading_using")+FXStringFormat(" %s %s",obj->getManufacturer().text(),obj->getModel().text())+"\n\n"+
                oLanguage->getText("str_reading_pwait"),NULL,LAYOUT_FILL|JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
    msg.create();
    msg.show(PLACEMENT_SCREEN);
    msg.setFocus();
    msg.raise();
    oApplicationManager->beginWaitCursor();
    while(oApplicationManager->peekEvent())
        oApplicationManager->runOneEvent(false);
    double *ret2=obj->acquire(data,baud,bits,parity,stop,ret[pos].settings.flow,ret[pos].settings.timeout);
    oApplicationManager->endWaitCursor();
    msg.hide();
    return ret2;
}

