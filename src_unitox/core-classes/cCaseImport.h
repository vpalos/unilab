#ifndef CCASEIMPORT_H
#define CCASEIMPORT_H

#include <fx.h>
#include "cSortList.h"

class cCaseImport
{
    private:
    protected:
        static FXString getFileName(void);
        static FXbool processData(FXString prFile);
        
    public:
        enum
        {
            ID_UPDATE,
            ID_LAST
        };
    
        static void doImport(void);
};

#endif
