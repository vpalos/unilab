#include <stdlib.h>

#include "engine.h"
#include "cSerialLink.h"
#include "cLanguage.h"
#include "cWinMain.h"

#ifdef WIN32

cSerialLink::cSerialLink()
{
    opened=0;
}

cSerialLink::~cSerialLink()
{
    disconnect();
}

FXbool cSerialLink::connect(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    if(isConnected())
        return false;
    handle=CreateFile(prSerialDevice.text(),GENERIC_READ|GENERIC_WRITE,0,NULL,OPEN_EXISTING,0,NULL); 
    if(handle==INVALID_HANDLE_VALUE)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_opencom").text());
        CloseHandle(handle);
        return false;
    }
    
    DCB ds;
    BYTE p;
    switch(prParity) 
    {
        case PARITYODD: 
            p=ODDPARITY; 
            break;
        case PARITYEVEN: 
            p=EVENPARITY; 
            break;
        default: 
            p=NOPARITY; 
            break;
    }
    
    opened=1;
	if(!SetupComm(handle, 1024, 1024))
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),(oLanguage->getText("str_serialerr_setpcom")+" : "+FXStringFormat("%d",(int)GetLastError())).text());
        disconnect();
        return false;
    }

	if(GetCommState(handle, &ds))
	{
		ds.BaudRate = prBaudRate;
		ds.ByteSize = prBits;
		ds.Parity = p;
		ds.StopBits = prStopBits==1?ONESTOPBIT:TWOSTOPBITS;
		ds.fBinary = TRUE;
		ds.fParity = TRUE;
        if(prFlow==FLOWCRTSCTS)
        {
            ds.fRtsControl=RTS_CONTROL_ENABLE;
            ds.fDtrControl=DTR_CONTROL_ENABLE;
        }
	}
	else
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),(oLanguage->getText("str_serialerr_setpcom")+" : "+FXStringFormat("%d",(int)GetLastError())).text());
        disconnect();
        return false;
    }
    if(!SetCommState(handle,&ds))
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),(oLanguage->getText("str_serialerr_setpcom")+" : "+FXStringFormat("%d",(int)GetLastError())).text());
        disconnect();
        return false;
    }

    if(!setTimeouts(prTimeout))
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_settout").text());
        disconnect();
        return false;
    }
    return true;
}

FXbool cSerialLink::disconnect(void)
{
    if(!isConnected())
        return false;
    CloseHandle(handle);
    opened=0;
    return true;
}

FXbool cSerialLink::isConnected(void)
{
    return (opened==1?true:false);
}

FXbool cSerialLink::setTimeouts(FXint prTimeout)
{
    if(!isConnected())
        return false;
    COMMTIMEOUTS ct;
	if(GetCommTimeouts(handle, &ct))
	{
		ct.ReadIntervalTimeout = 10*prTimeout;
		ct.ReadTotalTimeoutConstant = prTimeout*1000;
		ct.ReadTotalTimeoutMultiplier = 0;
		ct.WriteTotalTimeoutConstant = prTimeout*1000;
		ct.WriteTotalTimeoutMultiplier = 0;
	}
	else
        return false;

    if(!SetCommTimeouts(handle,&ct))
        return false;
    return true;
}

void cSerialLink::flush(void)
{
    if(!isConnected())
        return;
    FlushFileBuffers(handle);
    PurgeComm(handle,PURGE_TXCLEAR|PURGE_RXCLEAR);
}

void cSerialLink::sendBreak(int prMs)
{
    if(!isConnected())
        return;
    SetCommBreak(handle);
    Sleep(prMs);
    ClearCommBreak(handle);
}

int cSerialLink::input(char *prBuffer,int prCount)
{
    if(!isConnected())
        return -1;
    
    int ret;
    memset(prBuffer,0,prCount);
    DWORD dummy;
    if((ret=ReadFile(handle,(void*)prBuffer,prCount,&dummy,NULL))==0)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
        return -1;
    }
    return (ret==0?-1:ret);
}

int cSerialLink::output(const char *prBuffer,int prCount,int prGetEcho)
{
    char back;
    DWORD dummy;
    if(!isConnected())
        return -1;
    for(int i=0;i<prCount;i++)
    {
        if(WriteFile(handle,(void*)(prBuffer+i),1,&dummy,NULL)==0)
        {
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_write").text());
            disconnect();
            return -1;
        }
        if(prGetEcho && (ReadFile(handle,(void*)(&back),1,&dummy,NULL)==0))
        {
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_write").text());
            disconnect();
            return -1;
        }
        Sleep(1);
    }
    return prCount;
}

#else

cSerialLink::cSerialLink()
{
    handle=-1;
}

cSerialLink::~cSerialLink()
{
    disconnect();
}

FXbool cSerialLink::connect(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout)
{
    if(isConnected())
        return false;
    handle=open(prSerialDevice.text(),O_RDWR|O_NOCTTY); 
    if(handle==-1)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_opencom").text());
        return false;
    }
    if(tcgetattr(handle,&oldtio)!=0)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_setpcom").text());
        return false;
    }
    memset(&newtio,0,sizeof(newtio));
    newtio.c_cflag = prBaudRate | prFlow | prBits | prParity | prStopBits | CLOCAL | CREAD;
    newtio.c_iflag = IGNPAR;
    newtio.c_oflag = 0;
    newtio.c_lflag = 0;
    newtio.c_cc[VTIME]=prTimeout;
    newtio.c_cc[VMIN]=1;
    if(tcflush(handle,TCIOFLUSH)!=0)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_setpcom").text());
        disconnect();
        return false;
    }
    if(tcsetattr(handle,TCSANOW,&newtio)!=0)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_setpcom").text());
        disconnect();
        return false;
    }
    return true;
}

FXbool cSerialLink::disconnect(void)
{
    if(!isConnected())
        return false;
    tcsetattr(handle,TCSANOW,&oldtio);
    close(handle);
    handle=-1;
    return true;
}

FXbool cSerialLink::isConnected(void)
{
    return (handle!=-1);
}

FXbool cSerialLink::setTimeouts(FXint prTimeout)
{
    return true;
}

void cSerialLink::flush(void)
{
    if(!isConnected())
        return;
    tcdrain(handle);
    tcflush(handle,TCIOFLUSH);
}

void cSerialLink::sendBreak(int prMs)
{
}

int cSerialLink::input(char *prBuffer,int prCount)
{
    int ret;
    if(!isConnected())
        return -1;
    bzero(prBuffer,prCount);
    if((ret=read(handle,prBuffer,prCount))==-1)
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_readcommon").text());
    //fxmessage("IN: %c --- (0x%x)\n",prBuffer[0],prBuffer[0]);
    //fxmessage("IN: < --- (0x%x)\n",prBuffer[0],prBuffer[0]);
    //for(int i=0;i<ret;i++)fxmessage("%c",prBuffer[i]);fxmessage("\n");
    //fxmessage("%d,%d\n",ret,prCount);
    return ret;
}

int cSerialLink::output(const char *prBuffer,int prCount,int prGetEcho)
{
    char back;
    if(!isConnected())
        return -1;
    for(int i=0;i<prCount;i++)
    {
        if(write(handle,prBuffer+i,1)==-1)
        {
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_write").text());
            return -1;
        }
        if(prGetEcho && (read(handle,&back,1)==-1))
        {
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_serialerr_write").text());
            return -1;
        }
        usleep(10);
        //fxmessage("OUT:%c --- (0x%x)\n",prBuffer[i],prBuffer[i]);
        //fxmessage("OUT:> --- (0x%x)\n",prBuffer[i],prBuffer[i]);
    }
    return prCount;
}

#endif
