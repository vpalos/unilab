#include "cColorsManager.h"
#include "engine.h"
#include <math.h>

#define COLORS_COUNT 10

int cColorsManager::getColorsCount(void)
{
    return COLORS_COUNT;
}

/*FXStipplePattern cColorsManager::color2Stipple(FXColor prColor)
{
    unsigned long r,g,b,col;
    r=FXREDVAL(prColor);
    g=FXGREENVAL(prColor);
    b=FXBLUEVAL(prColor);
    col=(r*1000+g*10+b);
    //col=col?col/24:0;
    col%=7;

    switch(col)
    {
    case 1:
        return STIPPLE_HORZ;
        break;
    case 2:
        return STIPPLE_VERT;
        break;
    case 3:
        return STIPPLE_CROSS;
        break;
    case 4:
        return STIPPLE_DIAG;
        break;
    case 5:
        return STIPPLE_REVDIAG;
        break;
    case 6:
        return STIPPLE_CROSSDIAG;
        break;
    default:
        return (FXStipplePattern)0;
        break;
}*/

FXColor cColorsManager::getColor(FXint prIndex,FXint prOffset)
{
    switch(prIndex%COLORS_COUNT)
    {
        case 0:
            return FXRGB(255,230+prOffset,230+prOffset);
        case 1:
            return FXRGB(255,255,200+prOffset);
        case 2:
            return FXRGB(230+prOffset,220+prOffset,245+prOffset);
        case 3:
            return FXRGB(225+prOffset,255,220+prOffset);
        case 4:
            return FXRGB(220+prOffset,235+prOffset,255);
        case 5:
            return FXRGB(255,240+prOffset,255);
        case 6:
            return FXRGB(240+prOffset,250+prOffset,180);
        case 7:
            return FXRGB(255,255,240+prOffset);
        case 8:
            return FXRGB(255,230+prOffset,200+prOffset);
        case 9:
            return FXRGB(240+prOffset,255,255);
    }
    return FXRGB(255,255,255);
}

FXColor cColorsManager::generateDeterministicColor(FXint prMin, FXint prMax)
{
    int re=rand(),ge=rand(),be=rand();
    srand(re);
    double p=(double)(prMax-prMin)/(double)RAND_MAX;
    return FXRGB(prMin+(int)((double)re*p),
                 prMin+(int)((double)ge*p),
                 prMin+(int)((double)be*p));
}

void cColorsManager::generateDeterministicSeed(FXint prSeed)
{
    srand(prSeed);
}
