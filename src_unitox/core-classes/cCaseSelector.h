#ifndef CCASESELECTOR_H
#define CCASESELECTOR_H

#include <fx.h>
#include "cCaseManager.h"

#define CRITERIA_NUMBER 0
#define CRITERIA_TEXT 1
#define CRITERIA_DATE 2

#define CRITERIA_CASE 0
#define CRITERIA_POPULATION 1

typedef struct sCriteriaObject
{
    FXString *name;
    int type;
    int flag;
    FXString *attribute;
    FXStringDict *values1;
    FXStringDict *values2;
};

class cCaseSelector
{
    private:
    protected:
    public:

    static int getCriteriaCount(void);
    static sCriteriaObject *listCriteria(void);

    static sCaseObject *makeQuickSelection(FXString prCasea,FXString prCaseb,FXString prComma,FXString prCommb,FXString prDatea,FXString prDateb,FXIconList *prAssays,FXint *prCount);
    static sCaseObject *makeAllSelection(FXint *prCount);
    static sCaseObject *makeSelection(FXIconList *prCriteriaList,FXint *prCount);
};

#endif
