#ifndef CREADERS_H
#define CREADERS_H

#include "cSerialLink.h"

typedef struct sReadersSettings
{
    int baudRate;
    int bits;
    int parity;
    int stopBits;
    int flow;
    int timeout;
};

typedef struct sReadersObject
{
    FXString *label;
    int code;
    sReadersSettings settings;
};

class cReadersObject
{
    private:
    protected:
    public:
    virtual ~cReadersObject();
    
    virtual FXString getManufacturer(void) = 0;
    virtual FXString getModel(void) = 0;
    virtual int getCode(void) = 0;
    virtual sReadersSettings getSettings(void) = 0;
    
    virtual FXString getFilterA(void) = 0;
    virtual FXString getFilterB(void) = 0;
    virtual void setFilterA(const FXString &prFilter) = 0;
    virtual void setFilterB(const FXString &prFilter) = 0;
    
    virtual double *acquire(const FXString &prSerialDevice);
    virtual double *acquire(const FXString &prSerialDevice,sReadersSettings prSettings);
    virtual double *acquire(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout) = 0;
};

class cReadersManager
{
    private:
    protected:
    public:
        static int getReadersCount(void);

        static cReadersObject *getReadersObject(int prCode);
        static sReadersObject *getReadersList(void);
        
        static double *acquire(const FXString &prFilterA,const FXString &prFilterB,int prPlate=0,int prPlateCount=1);
};

#endif
