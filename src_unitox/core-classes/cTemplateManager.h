#ifndef CTEMPLATEMANAGER_H
#define CTEMPLATEMANAGER_H

#include <fx.h>

#include "cMDIAssay.h"
#include "cDataResult.h"

typedef struct sTemplateObject
{
    FXString *title;
    FXString *date;
};

class cTemplateManager
{
    private:
    protected:
    public:
        static FXbool templateExists(const FXString &prTitle);
        static FXString getNewID(void);
        static int getTemplateCount(void);

        static sTemplateObject *listTemplates(void);
        static cDataResult *getTemplate(FXString prId);

        static FXString newTemplate(FXMDIClient *prP, FXPopup *prPup);
        static FXString editTemplate(FXMDIClient *prP, FXPopup *prPup,const FXString &prTemplate);
        static FXString duplicateTemplate(const FXString &prTemplate);

        static FXbool addTemplate(FXString prId,FXString prDate,FXint prAlelisa,FXString prControlsDefs,FXString prCaseDefs);
        static FXbool setTemplate(FXString prOldId,FXString prId,FXString prDate,FXint prAlelisa,FXString prControlsDefs,FXString prCaseDefs);
        static FXbool removeTemplate(const FXString &prTemplate);

        static FXint getWellCount(const FXString &prTemplate);
        static FXint getControlCount(const FXString &prTemplate);
        static FXint getPlateCount(const FXString &prTemplate);
};

#endif
