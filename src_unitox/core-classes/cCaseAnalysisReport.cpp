#include <strings.h>
#include "engine.h"
#include "graphics.h"
#include "cWinMain.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cAssayManager.h"
#include "cTemplateManager.h"
#include "cFormulaRatio.h"
#include "cColorsManager.h"
#include "cVChart.h"
#include "cCaseAnalysisReport.h"

cCaseAnalysisReport *oCaseAnalysisReport;

FXbool cCaseAnalysisReport::loadReadings(cSortList &prCases)
{
    FXDialogBox msg(oWinMain,oLanguage->getText("str_report_wtitle"),DECOR_TITLE|DECOR_BORDER);
    new FXLabel(&msg,oLanguage->getText("str_report_ltext"),NULL,LAYOUT_FILL|JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
    msg.create();
    msg.show(PLACEMENT_OWNER);
    msg.setFocus();
    msg.raise();
    oApplicationManager->beginWaitCursor();
    while(oApplicationManager->peekEvent())
        oApplicationManager->runOneEvent(false);

    FXString criteria="";
    int j=0;
    for(int i=0;i<prCases.getNumItems();i++)
        if(prCases.isItemSelected(i))
        {
            if(j>0)
                criteria=criteria+" OR ";
            criteria=criteria+"(assay_oid='"+prCases.getItemText(i).before('\t')+"' AND "+"id='"+prCases.getItemText(i).after('\t').before('\t')+"')";
            j++;
        }
    if(!j)
        return false;
    cDataResult *resr=oDataLink->execute("SELECT DISTINCT reading_oid FROM t_gncases WHERE "+criteria+" ORDER BY assay_oid ASC;");
    tSetsCount=resr->getRowCount();

    if(!tSetsCount)
        return false;
    cDataResult *resc;
    casesSets=(sCasesSet*)malloc(tSetsCount*sizeof(sCasesSet));
    if(!casesSets)
    {
        tPlatesCount=0;
        tSetsCount=0;
        tCasesCount=0;
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return false;
    }
    tCasesCount=0;
    tPlatesCount=0;
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        FXString __reading_oid(resr->getCellString(setsi,0));
        resc=oDataLink->execute("SELECT * FROM t_gncases WHERE ("+criteria+") AND reading_oid='"+__reading_oid+"' ORDER BY (startPlate*100+startCell) ASC;");
        casesSets[setsi].casesCount=resc->getRowCount();
        tCasesCount+=casesSets[setsi].casesCount;
        casesSets[setsi].cases=(sCaseEntry*)malloc(casesSets[setsi].casesCount*sizeof(sCaseEntry));
        if(!casesSets[setsi].cases)
        {
            tPlatesCount=0;
            tSetsCount=0;
            tCasesCount=0;
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
            return false;
        }

        FXString __assay(resc->getCellString(0,4));
        FXString __template(resc->getCellString(0,10));
        casesSets[setsi].assayRes=oDataLink->execute("SELECT * FROM t_assays WHERE id='"+__assay+"';");
        casesSets[setsi].templateRes=oDataLink->execute("SELECT * FROM t_templates WHERE id='"+__template+"';");
        casesSets[setsi].controls_replicates=casesSets[setsi].templateRes->getCellInt(0,2);

        casesSets[setsi].cal=new FXString(casesSets[setsi].assayRes->getCellString(0,6));
        casesSets[setsi].unit=new FXString(casesSets[setsi].assayRes->getCellString(0,7));
        casesSets[setsi].assay=new FXString(__assay);
        casesSets[setsi].assaytitle=new FXString(casesSets[setsi].assayRes->getCellString(0,1));

        FXString __lot(resc->getCellString(0,5));
        FXString __expDate(resc->getCellString(0,6));

        //FXString calculation=casesSets[setsi].assayRes->getCellString(0,6);

        FXString __controls_defs(resc->getCellString(0,14));
        __controls_defs=__controls_defs.after('\n');
        casesSets[setsi].controlsPerPlate=0;
        int value;
        do{
            value=FXIntVal(__controls_defs.before('\t'));
            __controls_defs=__controls_defs.after('\t');
            if(value!=0 && value!=-100)
                casesSets[setsi].controlsLayout[casesSets[setsi].controlsPerPlate++]=value;
            else
                break;
        }while(true);

        FXString __asy_controls_defs=casesSets[setsi].assayRes->getCellString(0,5);
        value=0;
        value = FXIntVal(__asy_controls_defs.before('\n'));
        __asy_controls_defs=__asy_controls_defs.after('\n');

        casesSets[setsi].isSc = value;

        for(value = 0; value < 50; value++) {
            casesSets[setsi].asyControls[value]=-1;
            casesSets[setsi].asyControlsBBO[value]=-1;
        }

        value = 0;
        while(!__asy_controls_defs.empty())
        {
            casesSets[setsi].asyControls[value]=FXDoubleVal(__asy_controls_defs.before('\t'));
            __asy_controls_defs=__asy_controls_defs.after('\t');

            casesSets[setsi].asyControlsBBO[value]=FXIntVal(__asy_controls_defs.before('\n'));
            __asy_controls_defs=__asy_controls_defs.after('\n');
            value++;
        }
        casesSets[setsi].asyControlsCount = value;

        cMDIReadings::sortControls(casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate);

        casesSets[setsi].platesCount=0;
        FXDict *pl=new FXDict();
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++)
        {
            casesSets[setsi].cases[casei].startCell=resc->getCellInt(casei,12);
            casesSets[setsi].cases[casei].startPlate=resc->getCellInt(casei,11);
            casesSets[setsi].cases[casei].plateSpanCount=resc->getCellInt(casei,13);

            int ps=casesSets[setsi].cases[casei].startPlate,pe=ps+casesSets[setsi].cases[casei].plateSpanCount-1;
            FXString key;
            for(int i=ps;i<=pe;i++)
                pl->insert(FXStringFormat("%c%c",i/256+1,i%256+1).text(),NULL);

            casesSets[setsi].cases[casei].id=new FXString(resc->getCellString(casei,0));
            casesSets[setsi].cases[casei].old_id=new FXString(*casesSets[setsi].cases[casei].id);
            casesSets[setsi].cases[casei].reading_oid=new FXString(__reading_oid);
            casesSets[setsi].cases[casei].replicates=resc->getCellInt(casei,8);
            casesSets[setsi].cases[casei].factor=resc->getCellInt(casei,9);

            casesSets[setsi].cases[casei].tpl=new FXString(__template);
            casesSets[setsi].cases[casei].controls_defs=new FXString(resc->getCellString(casei,14));
            casesSets[setsi].cases[casei].readingDate=new FXString(resc->getCellString(casei,2));
            casesSets[setsi].cases[casei].technician=new FXString(resc->getCellString(casei,3));
            casesSets[setsi].cases[casei].lot=new FXString(__lot);
            casesSets[setsi].cases[casei].expirationDate=new FXString(__expDate);
            casesSets[setsi].cases[casei].commodity=new FXString(resc->getCellString(casei,7));
            casesSets[setsi].cases[casei].comments=new FXString(resc->getCellString(casei,16));
            casesSets[setsi].cases[casei].assay=casesSets[setsi].assay;

            casesSets[setsi].cases[casei].parentSet=setsi;

            FXString __data(resc->getCellString(casei,15));
            FXString __controls=__data.before('\n');
            __data=__data.after('\n');

            casesSets[setsi].cases[casei].controlsCount=casesSets[setsi].cases[casei].plateSpanCount*casesSets[setsi].controls_replicates*casesSets[setsi].controlsPerPlate;
            casesSets[setsi].cases[casei].controlsData=(double*)malloc(casesSets[setsi].cases[casei].controlsCount*sizeof(double));
            if(!casesSets[setsi].cases[casei].controlsData)
            {
                tPlatesCount=0;
                tSetsCount=0;
                tCasesCount=0;
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                return false;
            }
            for(int i=0;i<casesSets[setsi].cases[casei].controlsCount;i++)
            {
                casesSets[setsi].cases[casei].controlsData[i]=FXFloatVal(__controls.before(' '));
                __controls=__controls.after(' ');
            }

            int ct=casesSets[setsi].cases[casei].replicates+1;
            casesSets[setsi].cases[casei].data=(double*)malloc(ct*sizeof(double));
            if(!casesSets[setsi].cases[casei].data)
            {
                tPlatesCount=0;
                tSetsCount=0;
                tCasesCount=0;
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                return false;
            }
            for(int i=0;i<ct;i++)
            {
                casesSets[setsi].cases[casei].data[i]=FXFloatVal(__data.before(' '));
                __data=__data.after(' ');
            }
        }
        casesSets[setsi].platesCount=pl->no();
        delete pl;
        tPlatesCount+=casesSets[setsi].platesCount;
        resc->free();
    }
    resr->free();

    oApplicationManager->endWaitCursor();
    msg.hide();

    return true;
}

FXbool cCaseAnalysisReport::processReadings(void)
{
    if(!tPlatesCount)
        return false;

    FXDialogBox msg(oWinMain,oLanguage->getText("str_report_wtitle"),DECOR_TITLE|DECOR_BORDER);
    new FXLabel(&msg,oLanguage->getText("str_report_wtext"),NULL,LAYOUT_FILL|JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
    msg.create();
    msg.show(PLACEMENT_OWNER);
    msg.setFocus();
    msg.raise();
    oApplicationManager->beginWaitCursor();
    while(oApplicationManager->peekEvent())
        oApplicationManager->runOneEvent(false);

    char replicates[10];
    char creplicates[10];
    int k,l,offset,m;
    int platesi=0;
    int platesci=0;
    int datai=0;
    int samplei=0;
    int casesgi=0;
    int controlsi=0;
    int casepi=0;
    int p2_datai=0;
    int deltasi=0;
    int replicatesi=0;
    int creplicatesi=0;
    sCResult cresult;
    sCResult scresult;
    sSResult sresult;
    cVChart *chart = NULL;

    int nc = 6;

    double *ctData=NULL,n,val;
    FXDict *cv;
    FXbool err=false;
    bool p2_filled = false;
    FXString invalidPlates="";
    int dp,rp,pd=0,diff=0,spi=-1,cplatei,splatei;
//    cColorTable *plateTable1 = NULL;
    cColorTable *plateTable2 = NULL;
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        spi=-1;
        splatei=-1;
        diff=0;
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++,casesgi++)
        {
            cplatei=0;
            samplei=0;
            for(int g=spi+1;g<casesSets[setsi].cases[casei].startPlate;g++,diff++,splatei++);
            spi=casesSets[setsi].cases[casei].startPlate;
            while(samplei<1)
            {
                deltasi=0;
                replicatesi=0;
                replicates[0]=0;
                while(replicatesi<=casesSets[setsi].cases[casei].replicates)
                {
                    platesi=datai/96;
                    if(platesci==platesi)
                    {
                        splatei++;
                        if(samplei)cplatei++;
                        ctData=casesSets[setsi].cases[casei].controlsData+cplatei*casesSets[setsi].controlsPerPlate*casesSets[setsi].controls_replicates;

                        if(p2_datai>0)
                            for(int xi=0;xi<nc;xi++)
                                plateTable2->setItemBorders(p2_datai-1,xi,plateTable2->getItemBorders(p2_datai-1,xi)|FXTableItem::BBORDER);

                        /*plateTable1=new cColorTable(repWin);
                        plateTable1->setTableSize(9,13);
                        plateTable1->create();

                        for(int i=0;i<8;i++)
                            for(int j=0;j<12;j++)
                            {
                                plateTable1->setItemJustify(i,j,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                                plateTable1->setItemText(i,j,(char*)NULL);
                                plateTable1->setItemData(i,j,NULL);
                                plateTable1->setCellEditable(i,j,false);
                                plateTable1->setCellColor(i,j,FXRGB(255,255,255));
                                plateTable1->setItemBorders(i,j,(i>0 && i%8==0?FXTableItem::TBORDER:0)|
                                                            FXTableItem::RBORDER|((i+1)%8==0?FXTableItem::BBORDER:0));
                            }

                        for(int j=0;j<12;j++)
                        {
                            plateTable1->getColumnHeader()->setItemJustify(j,JUSTIFY_CENTER_X);
                            plateTable1->getColumnHeader()->setItemText(j,FXStringFormat("%d",j+1));
                        }
                        for(int j=0;j<plateTable1->getNumRows();j++)
                        {
                            plateTable1->getRowHeader()->setItemJustify(j,JUSTIFY_CENTER_X);
                            plateTable1->getRowHeader()->setItemText(j,FXStringFormat("P%d:%c",j/8+1,j%8+'A'));
                        }*/


                        plateTable2=new cColorTable(repWin);
                        plateTable2->setTableSize(0,0);
                        plateTable2->create();

                        p2_filled=plateTable2->getNumRows()>0?true:false;
                        if(p2_filled && (nc!=plateTable2->getNumColumns()))
                        {
                            if(nc>plateTable2->getNumColumns())
                                plateTable2->insertColumns(1,nc-plateTable2->getNumColumns());
                            else
                                plateTable2->removeColumns(1,plateTable2->getNumColumns()-nc);
                            plateTable2->layout();
                            for(int i=0;i<plateTable2->getNumRows();i++)
                            {
                                for(int j=0;j<nc;j++)
                                {
                                    plateTable2->setItemJustify(i,j,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                                    plateTable2->setCellEditable(i,j,j>0?false:true);
                                }
                                plateTable2->getRowHeader()->setItemJustify(i,JUSTIFY_CENTER_X);
                            }
                        }

                        if(!p2_filled)
                        {
                            plateTable2->setTableSize(plateTable2->getNumRows(),nc);
                            plateTable2->layout();

                        }

                        for(int i=0;i<plateTable2->getNumRows();i++)
                        {
                            for(int j=0;j<plateTable2->getNumColumns();j++)
                                plateTable2->setItemText(i,j,(char*)NULL);
                            ((cLocInfo*)plateTable2->getItemData(i,0))->isValue=false;
                        }
                        for(int i=0;i<nc;i++)
                            plateTable2->getColumnHeader()->setItemJustify(i,JUSTIFY_CENTER_X);

                        plateTable2->getColumnHeader()->setItemText(0,oLanguage->getText("str_rdgmdi_od"));
                        plateTable2->setColumnWidth(0,90);
                        plateTable2->getColumnHeader()->setItemText(1,oLanguage->getText("str_rdgmdi_conc"));
                        plateTable2->getColumnHeader()->setItemText(2,oLanguage->getText("str_rdgmdi_bbo"));
                        plateTable2->setColumnWidth(2,87);
                        plateTable2->getColumnHeader()->setItemText(3,oLanguage->getText("str_rdgmdi_calc"));
                        plateTable2->getColumnHeader()->setItemText(4,oLanguage->getText("str_rdgmdi_cv"));
                        plateTable2->setColumnWidth(4,84);
                        plateTable2->getColumnHeader()->setItemText(5,oLanguage->getText("str_asymdi_factors"));
                        plateTable2->setColumnWidth(5,70);

                        // Create chart
                        chart=new cVChart(repWin,
                            cFormulaRatio::calculateChartType(casesSets[setsi].cal),
                            LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT,0,0,0,350);
                        chart->setTitle(oLanguage->getText("str_rdgmdi_plate")+FXStringFormat(" %d",platesi+1)+" - "+(*casesSets[setsi].assay) + " (" +
                            *casesSets[setsi].cal + ") - " + casesSets[setsi].templateRes->getCellString(0,0));
                        chart->setVAxisTitle(cFormulaRatio::calculateChartVAxis(casesSets[setsi].cal));
                        chart->setHAxisTitle(oLanguage->getText("str_asymdi_ctlcr") + " (" + *casesSets[setsi].unit + ") - " +
                            oLanguage->getText("str_rdgmdi_corr") + ": ");
                        chart->create();
                        chart->recalc();
                        cv = chart->dataSet();

                        cv->clear();
                        err=false;
                        FXdouble last_ctl = 1000, this_ctl = 0;
                        controlsi=0;
                        for(int i=0;i<casesSets[setsi].controlsPerPlate;i++)
                        {
                            creplicatesi=0;
                            creplicates[0]=0;
                            while(creplicatesi<casesSets[setsi].controls_replicates)
                            {
                                m=cMDIReadings::platePosition((abs(casesSets[setsi].controlsLayout[i])==1000?0:abs(casesSets[setsi].controlsLayout[i])) + creplicatesi);
                                l=m%12;
                                k=platesi*8+m/12;
                                /*plateTable1->setItemText(k,l,cMDIReadings::sampleVal(ctData[controlsi]));
                                plateTable1->setItemData(k,l,new cLocInfo(casesSets[setsi].controlsLayout[i]>=0?-2:-3,controlsi,splatei,setsi));
                                plateTable1->setCellColor(k,l,FXRGB(255,90+creplicatesi*50,90+creplicatesi*50));
                                plateTable1->setItemBorders(k,l,FXTableItem::LBORDER|FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
                                plateTable1->setCellEditable(k,l,true);
                                if(k>0)plateTable1->setItemBorders(k-1,l,plateTable1->getItemBorders(k-1,l)|FXTableItem::BBORDER);
                                if(l>0)plateTable1->setItemBorders(k,l-1,plateTable1->getItemBorders(k,l-1)|FXTableItem::RBORDER);
                                if((k+1)<plateTable1->getNumRows())plateTable1->setItemBorders(k+1,l,plateTable1->getItemBorders(k+1,l)|FXTableItem::TBORDER);
                                if(l<11)plateTable1->setItemBorders(k,l+1,plateTable1->getItemBorders(k,l+1)|FXTableItem::LBORDER);*/

                                if(!p2_filled)
                                {
                                    plateTable2->insertRows(p2_datai);
                                    plateTable2->getRowHeader()->setItemJustify(p2_datai,JUSTIFY_CENTER_X);
                                    plateTable2->getRowHeader()->setItemText(p2_datai,FXStringFormat("P%d: Std%d%s",platesi+1,i+1,creplicates));
                                    for(int xi=0;xi<nc;xi++)
                                    {
                                        plateTable2->setItemJustify(p2_datai,xi,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                                        plateTable2->setItemStipple(p2_datai,xi,STIPPLE_NONE);
                                        plateTable2->setCellEditable(p2_datai,xi,xi>0?false:true);
                                    }
                                    plateTable2->setItemBorders(p2_datai,0,FXTableItem::LBORDER|FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
                                    plateTable2->setCellColor(p2_datai,0,FXRGB(255,90+creplicatesi*50,90+creplicatesi*50));
                                }
                                plateTable2->setItemText(p2_datai,0,cMDIReadings::calculVal(ctData[controlsi]));
                                plateTable2->setItemData(p2_datai,0,new cLocInfo(casesSets[setsi].controlsLayout[i]>=0?-2:-3,controlsi,splatei,setsi));
                                if(creplicatesi) {
                                    for(int xi=1;xi<nc;xi++) {
                                        plateTable2->setItemText(p2_datai,xi,creplicates);
                                    }
                                }
                                else {
                                    cresult = cFormulaRatio::calculateControl(
                                        casesSets[setsi].cal,
                                        i,
                                        ctData,
                                        casesSets[setsi].controlsLayout,
                                        casesSets[setsi].controlsPerPlate,
                                        casesSets[setsi].controls_replicates==2,
                                        casesSets[setsi].isSc,
                                        casesSets[setsi].asyControls,
                                        casesSets[setsi].asyControlsBBO,
                                        casesSets[setsi].asyControlsCount);

                                    this_ctl = cFormulaRatio::cOD(i,
                                        ctData,
                                        casesSets[setsi].controls_replicates==2);
                                    if(!err) {
                                        err = this_ctl > last_ctl;
                                    }
                                    last_ctl = this_ctl;

                                    bool linear = cFormulaRatio::calculateChartType(casesSets[setsi].cal) == cVChart::CHART_LINEAR;
                                    int delta = linear ? 0 : 1;

                                    // Add control to chart with trend point
                                    if((i > 0) || ((i == 0) && linear)) {
                                        sVChartData *cval=new sVChartData;
                                        cval->value=cresult.y;
                                        cval->xvalue=cresult.ippu;
                                        cval->tvalue=cresult.ty;
                                        cval->name=cMDIReadings::calculVal(cresult.ippu);
                                        cv->insert(FXStringFormat("%d",i - delta).text(),cval);
                                        if(i == 1) {
                                            chart->appendHAxisTitle(cMDIReadings::calculVal(cresult.corr));
                                        }
                                    }
                                    else if(casesSets[setsi].isSc) {
                                        for(int sc = delta; sc < casesSets[setsi].asyControlsCount; sc++) {
                                            scresult = cFormulaRatio::calculateControl(
                                                casesSets[setsi].cal,
                                                sc,
                                                ctData,
                                                casesSets[setsi].controlsLayout,
                                                casesSets[setsi].controlsPerPlate,
                                                casesSets[setsi].controls_replicates==2,
                                                casesSets[setsi].isSc,
                                                casesSets[setsi].asyControls,
                                                casesSets[setsi].asyControlsBBO,
                                                casesSets[setsi].asyControlsCount);

                                            sVChartData *cval=new sVChartData;
                                            cval->value=scresult.y;
                                            cval->xvalue=scresult.ippu;
                                            cval->tvalue=scresult.ty;
                                            cval->name=cMDIReadings::calculVal(scresult.ippu);
                                            cv->insert(FXStringFormat("%d",sc - delta).text(),cval);
                                            if(sc == 1) {
                                                chart->appendHAxisTitle(cMDIReadings::calculVal(scresult.corr));
                                            }
                                        }
                                        chart->appendHAxisTitle(" - " + oLanguage->getText("str_asymdi_ctlsc"));
                                    }

                                    plateTable2->setItemText(p2_datai,1,FXStringFormat("%s %s", cMDIReadings::calculVal(cresult.ippu).text(), casesSets[setsi].unit->text()));
                                    plateTable2->setItemText(p2_datai,2,cMDIReadings::calculVal1(cresult.bbo) + "%");
                                    plateTable2->setItemText(p2_datai,3,(cresult.cppu < 0) ? "-" : FXStringFormat("%s %s", cMDIReadings::calculVal(cresult.cppu).text(), casesSets[setsi].unit->text()));
                                    plateTable2->setItemText(p2_datai,4,(cresult.cppu < 0) ? "-" : cMDIReadings::calculVal1(cresult.var) + "%");
                                    plateTable2->setItemText(p2_datai,5,"1");
                                }
                                p2_datai++;
                                controlsi++;

                                creplicates[creplicatesi]='\'';
                                creplicates[++creplicatesi]=0;
                            }
                        }

                        platesci++;
                        plateTable2->insertRows(p2_datai++);

                    }

                    casepi=datai%96;
                    dp=datai/96;
                    rp=pd+casesSets[setsi].cases[casei].startPlate-diff;
                    if(!cMDIReadings::isControlPosition(casepi,casesSets[setsi].controlsLayout,
                                          casesSets[setsi].controlsPerPlate,casesSets[setsi].controls_replicates==2) &&
                                          ((casepi>=casesSets[setsi].cases[casei].startCell && dp==rp) || (dp>rp)))
                    {

                        m=cMDIReadings::platePosition(datai%96);
                        l=m%12;
                        k=(datai/96)*8+m/12;
                        offset=(casesSets[setsi].cases[casei].replicates+1)*samplei+replicatesi;
                        /*plateTable1->setItemText(k,l,cMDIReadings::sampleVal(casesSets[setsi].cases[casei].data[offset]));
                        plateTable1->setItemData(k,l,new cLocInfo(casesgi,offset));
                        plateTable1->setCellEditable(k,l,true);
                        plateTable1->setCellColor(k,l,cColorsManager::getColor(casei,-replicatesi*10));*/
                        if(!p2_filled)
                        {
                            plateTable2->insertRows(p2_datai);
                            plateTable2->getRowHeader()->setItemJustify(p2_datai,JUSTIFY_CENTER_X);
                            plateTable2->getRowHeader()->setItemText(p2_datai,FXStringFormat("(%s) P%d:%c%d - %s",cMDIReadings::sampleKey(casei).text(),platesi+1,'A'+m/12,l+1, casesSets[setsi].cases[casei].id->text()));
                            for(int xi=0;xi<nc;xi++)
                            {
                                plateTable2->setItemJustify(p2_datai,xi,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                                plateTable2->setItemStipple(p2_datai,xi,STIPPLE_NONE);
                                plateTable2->setCellEditable(p2_datai,xi,xi>0?false:true);
                            }
                            plateTable2->setItemData(p2_datai,0,new cLocInfo(casesgi,offset,platesi));
                        }
                        plateTable2->setItemText(p2_datai,0,cMDIReadings::calculVal(casesSets[setsi].cases[casei].data[offset]));
                        if(replicatesi==0)
                        {
                            val=n=0;
                            for(int xi=0;xi<=casesSets[setsi].cases[casei].replicates;xi++,n++)
                                val+=casesSets[setsi].cases[casei].data[(casesSets[setsi].cases[casei].replicates+1)*samplei+xi];
                            ((cLocInfo*)plateTable2->getItemData(p2_datai,0))->setValue(val/n);
                        }
                        else
                            for(int xi=1;xi<nc;xi++)
                                plateTable2->setItemText(p2_datai,xi,replicates);
                        plateTable2->setCellColor(p2_datai,0,cColorsManager::getColor(casei,-replicatesi*10));
                        for(int xi=1;xi<nc;xi++)
                            plateTable2->setCellColor(p2_datai,xi,FXRGB(247,247,247));
                        deltasi=1;

                        if(((cLocInfo*)plateTable2->getItemData(p2_datai,0))->isValue)
                        {
                            double val = ((cLocInfo*)plateTable2->getItemData(p2_datai,0))->value;
                            sresult = cFormulaRatio::calculateSample(
                                casesSets[setsi].cal,
                                val,
                                casesSets[setsi].cases[casei].replicates,
                                casesSets[setsi].cases[casei].factor,
                                ctData,
                                casesSets[setsi].controlsLayout,
                                casesSets[setsi].controlsPerPlate,
                                casesSets[setsi].controls_replicates==2,
                                casesSets[setsi].isSc,
                                casesSets[setsi].asyControls,
                                casesSets[setsi].asyControlsBBO,
                                casesSets[setsi].asyControlsCount);

                            plateTable2->setItemText(p2_datai,1,"---");
                            plateTable2->setItemText(p2_datai,2,cMDIReadings::calculVal1(sresult.bbo) + "%");
                            plateTable2->setItemText(p2_datai,3,(sresult.result == 1) ? oLanguage->getText("str_rdgmdi_over") : FXStringFormat("%s %s", cMDIReadings::calculVal(sresult.cppu).text(), casesSets[setsi].unit->text()));
                            plateTable2->setItemText(p2_datai,4,"---");
                            plateTable2->setItemText(p2_datai,5,cMDIReadings::calculVal(casesSets[setsi].cases[casei].factor));

                            /*switch(sresult.result)
                            {
                                case -1:
                                    plateTable2->setCellTextColor(p2_datai,5,FXRGB(200,200,200));
                                    plateTable2->useCellTextColor(p2_datai,5,true);
                                    plateTable2->setItemText(p2_datai,5,oLanguage->getText("str_rdgmdi_neg"));
                                    break;
                                case 1:
                                    plateTable2->setCellTextColor(p2_datai,5,FXRGB(255,0,0));
                                    plateTable2->useCellTextColor(p2_datai,5,true);
                                    plateTable2->setItemText(p2_datai,5,oLanguage->getText("str_rdgmdi_pos"));
                                    break;
                                case 100:
                                    plateTable2->setCellTextColor(p2_datai,5,FXRGB(255,0,0));
                                    plateTable2->useCellTextColor(p2_datai,5,true);
                                    plateTable2->setItemText(p2_datai,5,oLanguage->getText("str_rdgmdi_over"));
                                    break;
                                default:
                                    plateTable2->useCellTextColor(p2_datai,5,false);
                                    plateTable2->setItemText(p2_datai,5,oLanguage->getText("str_invalid"));
                                    break;
                            }*/
                        }

                        p2_datai+=deltasi;
                        replicates[replicatesi]='\'';
                        replicates[replicatesi+1]=0;
                        replicatesi++;
                    }
                    datai++;
                    platesi=datai/96;
                    if((platesci==platesi) || ((casei==(casesSets[setsi].casesCount-1) && (replicatesi>casesSets[setsi].cases[casei].replicates))))
                    {
                        /*if(plateTable1) {
                            delete plateTable1;
                            plateTable1 = NULL;
                        }*/

                        repWin->addTitle(chart->getTitle(),0,true);
                        if(useInfo)
                        {
                            repWin->addSubTitle(oLanguage->getText("str_report_kitInfo"));
                            repWin->addText(FXStringFormat("%s - %s, %s: %s, %s: %s,\n%s%s: \"%s\", %s: \"%s\"",
                                                           casesSets[setsi].cases[casei].assay->text(),
                                                           casesSets[setsi].assayRes->getCellString(0,1).text(),
                                                           oLanguage->getText("str_asymdi_cal").text(),cFormulaRatio::formulaTitle(casesSets[setsi].cal).text(),
                                                           oLanguage->getText("str_asymdi_unit").text(),casesSets[setsi].unit->text(),
                                                           casesSets[setsi].isSc?FXStringFormat("%s, ", oLanguage->getText("str_asymdi_ctlsct").substitute('\n',' ').text()).text():"",
                                                           oLanguage->getText("str_rdgmdi_assayLot").text(),casesSets[setsi].cases[casei].lot->text(),
                                                           oLanguage->getText("str_rdgmdi_assayExp").text(),casesSets[setsi].cases[casei].expirationDate->text()),
                                                           JUSTIFY_LEFT,true);

                            repWin->addSubTitle(oLanguage->getText("str_report_caseInfo"));
                            repWin->addText(FXStringFormat("%s: %d", oLanguage->getText("str_asymdi_ctlc").text(), casesSets[setsi].asyControlsCount),JUSTIFY_LEFT,true);

                            for(int r = 0; r < casesSets[setsi].asyControlsCount; r++) {
                                repWin->addText(FXStringFormat("Std%d - %s: %s%s", r, oLanguage->getText("str_asymdi_ctlcr").text(),
                                                cMDIReadings::calculVal(casesSets[setsi].asyControls[r]).text(),
                                                    (casesSets[setsi].isSc?FXStringFormat(" - %s: %s%c",
                                                        oLanguage->getText("str_asymdi_ctlb").text(),
                                                        cMDIReadings::calculVal1(casesSets[setsi].asyControlsBBO[r]).text(),
                                                        '%'
                                                    ).text():"")
                                                )
                                                           ,JUSTIFY_LEFT,true);
                            }

                            if(err) {
                                repWin->addError(oLanguage->getText("str_reading_invbbo"));
                            }
                        }

                        if(useGraphs)
                        repWin->addChart(chart);
                        else
                        delete chart;

                        if(useData)
                        {
                            plateTable2->insertRows(0, 1);
                            for(int i=0;i<nc;i++) {
                                plateTable2->setItemJustify(0, i,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                                plateTable2->setItemText(0, i, plateTable2->getColumnHeader()->getItemText(i));
                            }

                            plateTable2->insertColumns(0, 1);
                            for(int i=0;i<plateTable2->getNumRows();i++) {
                                plateTable2->setItemJustify(i, 0,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                                plateTable2->setItemText(i, 0, plateTable2->getRowHeader()->getItemText(i));
                            }

                            repWin->addSubTitle(oLanguage->getText("str_report_caseData"));
                            repWin->addTable(plateTable2,true);
                        }
                        else
                        {
                            delete plateTable2;
                        }

                        repWin->addComments();
                        repWin->cancelTitleContinuum();
                        if((platesi<tPlatesCount-1))
                            repWin->nextPage();

                        p2_datai = 0;
                    }
                }
                samplei+=deltasi;
            }
        }
        pd+=casesSets[setsi].platesCount;
        int mdi=datai%96;
        if(mdi)
            datai+=96-mdi;
    }

    oApplicationManager->endWaitCursor();
    msg.hide();

    return true;
}

FXbool cCaseAnalysisReport::askPrefs(void)
{
    dlg=new cDLGReportCA(oWinMain);
    if(!dlg->execute(PLACEMENT_OWNER))
        return false;

    useGraphs=dlg->bUseGraphs->getCheck();
    useData=dlg->bUseData->getCheck();
    useInfo=dlg->bUseInfo->getCheck();

    return true;
}

void cCaseAnalysisReport::openCases(FXMDIClient *prP, FXPopup *prPup,cSortList &prCases)
{
    oCaseAnalysisReport=new cCaseAnalysisReport();

    oCaseAnalysisReport->repWin=new cMDIReport(prP,oLanguage->getText("str_report_ca"),new FXGIFIcon(oApplicationManager,data_reports_ca),prPup,MDI_NORMAL,0,0,400,300);
    oCaseAnalysisReport->repWin->setWidth(770);
    oCaseAnalysisReport->repWin->setHeight(520);
    //oCaseAnalysisReport->repWin->maximize();
    oCaseAnalysisReport->repWin->hide();
    oCaseAnalysisReport->repWin->create();
    oCaseAnalysisReport->repWin->setFocus();
    oCaseAnalysisReport->repWin->setTitle(oLanguage->getText("str_report_ca"));
    oCaseAnalysisReport->repWin->hide();

    oCaseAnalysisReport->useGraphs=true;
    oCaseAnalysisReport->useData=true;
    oCaseAnalysisReport->useInfo=true;

    if(!oCaseAnalysisReport->loadReadings(prCases) || !oCaseAnalysisReport->askPrefs() || !oCaseAnalysisReport->processReadings())
    {
        delete oCaseAnalysisReport->repWin;
        return;
    }

    oCaseAnalysisReport->repWin->recalc();
    oCaseAnalysisReport->repWin->show();
}




























































































































































































































































































//x
