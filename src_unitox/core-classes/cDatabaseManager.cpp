#include <ctype.h>
#include <stdio.h>
#include <strings.h>
#include "engine.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cDataLink.h"
#include "cDatabaseManager.h"

int cDatabaseManager::getDatabaseCount(void)
{
    FXString *fileList;
    return FXDir::listFiles(fileList,DATABASES_PATH,"*.utmyco",FXDir::CaseFold);
}

sDatabaseObject *cDatabaseManager::listDatabases(void)
{
    FXString *fileList;
    int res=FXDir::listFiles(fileList,DATABASES_PATH,"*.utmyco",FXDir::CaseFold);
    cDataLink *link=new cDataLink();
    sDatabaseObject *ret=(sDatabaseObject *)malloc(res*sizeof(sDatabaseObject));
    if(!ret)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }
    for(int i=0;i<res;i++)
    {
        ret[i].filename=new FXString(DATABASES_PATH+fileList[i]);

        if(link->open(ret[i].filename->text(),false,NULL,true))
        {
            ret[i].title=new FXString(link->getProperty("database_name"));
            ret[i].owner=new FXString(link->getProperty("database_owner"));
            ret[i].comments=new FXString(link->getProperty("database_comments"));
            link->close();
        }
        else
        {
            ret[i].title=new FXString("(inaccessible database)");
            ret[i].owner=new FXString("");
            ret[i].comments=new FXString("");
        }
    }

    return ret;
}

FXbool cDatabaseManager::databaseExists(const FXString &prTitle)
{
    sDatabaseObject *res=listDatabases();
    if(res==NULL)
        return false;
    for(int i=0;i<getDatabaseCount();i++)
        if(strcasecmp(res[i].title->text(),prTitle.text())==0)
        {
            free(res);
            return true;
        }
    free(res);
    return false;
}

FXString cDatabaseManager::newDatabase(void)
{
    int i;
    FXString filename="";
    FXString name;
    FXString owner;
    FXString comments;

    FXDataTarget nameTgt(name);
    FXDataTarget ownerTgt(owner);
    FXDataTarget commentsTgt(comments);

    FXDialogBox dlg(oWinMain,oLanguage->getText("str_dlg_newdb"),DECOR_TITLE|DECOR_BORDER);
    FXVerticalFrame *_vframe0=new FXVerticalFrame(&dlg,LAYOUT_FILL);
    FXLabel *_l0=new FXLabel(_vframe0,oLanguage->getText("str_dlg_newdb_name"));
    _l0->setTextColor(FXRGB(200,0,0));
    FXTextField *retName=new FXTextField(_vframe0,25,&nameTgt,FXDataTarget::ID_VALUE,TEXTFIELD_NORMAL|LAYOUT_FILL_X);

    new FXLabel(_vframe0,oLanguage->getText("str_dlg_newdb_owner"));
    new FXTextField(_vframe0,25,&ownerTgt,FXDataTarget::ID_VALUE,TEXTFIELD_NORMAL|LAYOUT_FILL_X);
    new FXLabel(_vframe0,oLanguage->getText("str_dlg_newdb_comments"));
    FXVerticalFrame *_vframe1=new FXVerticalFrame(_vframe0,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0,0,0);
    new FXText(_vframe1,&commentsTgt,FXDataTarget::ID_VALUE,TEXT_WORDWRAP|LAYOUT_FILL_X);
    new FXHorizontalSeparator(_vframe0,SEPARATOR_GROOVE|LAYOUT_FIX_WIDTH,0,0,220,0);
    new FXLabel(_vframe0,oLanguage->getText("str_dlg_newdb_explain"),NULL,LAYOUT_FILL_X|JUSTIFY_CENTER_Y);
    new FXHorizontalSeparator(_vframe0);
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(_vframe0,JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y|LAYOUT_CENTER_X);
    new FXButton(_hframe0,oLanguage->getText("str_but_accept"),NULL,&dlg,FXDialogBox::ID_ACCEPT,BUTTON_DEFAULT|BUTTON_INITIAL|FRAME_RAISED|FRAME_THICK|LAYOUT_CENTER_X);
    new FXButton(_hframe0,oLanguage->getText("str_but_cancel"),NULL,&dlg,FXDialogBox::ID_CANCEL,BUTTON_DEFAULT|FRAME_RAISED|FRAME_THICK|LAYOUT_CENTER_X);
    retName->setFocus();
    int exit=false;
    while(!exit)
    {
        exit=true;
        if(!dlg.execute(PLACEMENT_OWNER))
            return "";
        if(retName->getText().empty())
        {
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_dlg_newdb_incomplete").text());
            exit=false;
        }
        if(cDatabaseManager::databaseExists(retName->getText()))
        {
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_dlg_newdb_exists").text());
            exit=false;
        }
    }
    filename="";
    for(i=0;i<retName->getText().length();i++)
        if(isalnum(retName->getText().at(i)))
            filename=filename+retName->getText().at(i);
    i=1;
    FXString file=DATABASES_PATH+filename+".utmyco";
    while(FXStat::exists(file))
        {
            file=DATABASES_PATH+filename+"_"+FXStringFormat("%d",i)+".utmyco";
            i++;
        }
    cDataLink *olink=oDataLink,*link=new cDataLink();
    if(link->open(file,true))
    {
        link->setProperty("database_species","myco");
        link->setProperty("database_name",name);
        link->setProperty("database_owner",owner);
        link->setProperty("database_comments",comments);
        oDataLink=link;
        oDataLink->ensureKitUpgrade(true);
        oDataLink=olink;
        link->close();
    }
    return file;
}

FXString cDatabaseManager::editDatabase(const FXString &prFilename)
{
    int i=1,opn=false;
    cDataLink *link=new cDataLink();
    if(oDataLink->isOpened() && oDataLink->getFilename()==prFilename)
    {
        link=oDataLink;
        opn=true;
    }
    else
        if(!link->open(prFilename,false))
            return "";
    FXString title=link->getProperty("database_name");
    FXString owner=link->getProperty("database_owner");
    FXString comments=link->getProperty("database_comments");
    FXString ititle=title;

    FXDataTarget nameTgt(title);
    FXDataTarget ownerTgt(owner);
    FXDataTarget commentsTgt(comments);

    FXDialogBox dlg(oWinMain,oLanguage->getText("str_dlg_editdb"),DECOR_TITLE|DECOR_BORDER);
    FXVerticalFrame *_vframe0=new FXVerticalFrame(&dlg,LAYOUT_FILL);
    FXLabel *_l0=new FXLabel(_vframe0,oLanguage->getText("str_dlg_newdb_name"));
    _l0->setTextColor(FXRGB(200,0,0));
    FXTextField *retName=new FXTextField(_vframe0,25,&nameTgt,FXDataTarget::ID_VALUE,TEXTFIELD_NORMAL|LAYOUT_FILL_X);
    new FXLabel(_vframe0,oLanguage->getText("str_dlg_newdb_owner"));
    new FXTextField(_vframe0,25,&ownerTgt,FXDataTarget::ID_VALUE,TEXTFIELD_NORMAL|LAYOUT_FILL_X);
    new FXLabel(_vframe0,oLanguage->getText("str_dlg_newdb_comments"));
    FXVerticalFrame *_vframe1=new FXVerticalFrame(_vframe0,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0,0,0);
    new FXText(_vframe1,&commentsTgt,FXDataTarget::ID_VALUE,TEXT_WORDWRAP|LAYOUT_FILL_X);
    new FXHorizontalSeparator(_vframe0,SEPARATOR_GROOVE|LAYOUT_FIX_WIDTH,0,0,220,0);
    new FXLabel(_vframe0,oLanguage->getText("str_dlg_newdb_explain"),NULL,LAYOUT_FILL_X|JUSTIFY_CENTER_Y);
    new FXHorizontalSeparator(_vframe0);
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(_vframe0,JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y|LAYOUT_CENTER_X);
    new FXButton(_hframe0,oLanguage->getText("str_but_accept"),NULL,&dlg,FXDialogBox::ID_ACCEPT,BUTTON_DEFAULT|BUTTON_INITIAL|FRAME_RAISED|FRAME_THICK|LAYOUT_CENTER_X);
    new FXButton(_hframe0,oLanguage->getText("str_but_cancel"),NULL,&dlg,FXDialogBox::ID_CANCEL,BUTTON_DEFAULT|FRAME_RAISED|FRAME_THICK|LAYOUT_CENTER_X);
    retName->setText(title);
    retName->setFocus();
    retName->setCursorPos(retName->getText().length());
    int exit=false;
    while(!exit)
    {
        exit=true;
        if(!dlg.execute(PLACEMENT_OWNER))
            return "";
        if(retName->getText().empty())
        {
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_dlg_newdb_incomplete").text());
            exit=false;
        }
        if(cDatabaseManager::databaseExists(retName->getText()) && (ititle!=title))
        {
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_dlg_newdb_exists").text());
            exit=false;
        }
    }
    link->setProperty("database_name",title);
    link->setProperty("database_owner",owner);
    link->setProperty("database_comments",comments);
    if(!link->close())
        return "";

    FXString filename="";
    for(i=0;i<retName->getText().length();i++)
        if(isalnum(retName->getText().at(i)))
            filename=filename+retName->getText().at(i);
    i=1;
    FXString file=DATABASES_PATH+filename+".utmyco";
    while(FXStat::exists(file))
        {
            file=DATABASES_PATH+filename+"_"+FXStringFormat("%d",i)+".utmyco";
            i++;
        }
    if(!FXFile::moveFiles(prFilename,file,false))
        return "";
    if(opn)
        oDataLink->open(file,true);
    return file;
}

FXString cDatabaseManager::duplicateDatabase(const FXString &prFilename)
{
    cDataLink *link=new cDataLink();
    int i=1;
    FXString title;
    if(!link->open(prFilename,false))
        return "";
    title=link->getProperty("database_name");
    link->close();
    FXString add="";
    while(cDatabaseManager::databaseExists(title+add))
        add=FXStringFormat(" (%d)",i++);
    title=title+add;
    FXString filename="";
    for(i=0;i<title.length();i++)
        if(isalnum(title.at(i)))
            filename=filename+title.at(i);
    i=1;
    FXString file=FXPath::title(filename)+".utmyco";
    while(FXStat::exists(file))
            file=FXPath::title(filename)+"_"+FXStringFormat("%d",i++)+".utmyco";
    file=DATABASES_PATH+file;
    if(!FXFile::copyFiles(prFilename,file,false))
        return "";
    if(link->open(file,false))
        link->setProperty("database_name",title);
    link->close();
    return file;
}

FXbool cDatabaseManager::removeDatabase(const FXString &prFilename)
{
    if(oDataLink->isOpened() && oDataLink->getFilename()==prFilename)
        if(!oDataLink->closeDesignated())
            return false;
    return FXFile::remove(prFilename);
}
