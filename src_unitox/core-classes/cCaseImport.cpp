#include <strings.h>

#include "engine.h"
#include "graphics.h"
#include "cWinMain.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cCaseImport.h"
#include "cCaseManager.h"
#include "cAssayManager.h"
#include "cMDIDatabaseManager.h"
#include "cMDICaseManager.h"
#include "cTemplateManager.h"
#include "cMDITemplateManager.h"
#include "cMDIAssayManager.h"

FXString cCaseImport::getFileName(void)
{
    FXString ret,dir=oApplicationManager->reg().readStringEntry("settings","importdir",(FXSystem::getHomeDirectory()+PATHSEP).text());
    ret=FXFileDialog::getOpenFilename(oWinMain,oLanguage->getText("str_import_dlgtitle"),dir,oLanguage->getText("str_export_dlgpatt1")+" (*)\n"+oLanguage->getText("str_export_dlgpatt2")+" (*.utmyco)",1);
    if(!ret.empty())
    {
        dir=FXPath::directory(ret)+PATHSEP;
        oApplicationManager->reg().writeStringEntry("settings","importdir",dir.text());
    }
    if((!ret.empty()) && (MBOX_CLICKED_NO==FXMessageBox::warning(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_import_confirm").text())))
        return "";
    return ret;
}

FXbool cCaseImport::processData(FXString prFile)
{
    if(prFile.empty())
        return true;

    if(!FXStat::exists(prFile))
        return false;

    cDataLink *link=new cDataLink();
    if(!link->open(prFile,false))
        return false;

    cDataResult *r,*rr;
    FXString calculations_defs,rules_defs;

    rules_defs=link->getProperty("database_version");
    FXuint version=0,v1=0,v2=0,v3=0;

    v1=FXIntVal(rules_defs.before('.'));
    rules_defs=rules_defs.after('.');
    v2=FXIntVal(rules_defs.before('.'));
    rules_defs=rules_defs.after('.');
    v3=FXIntVal(rules_defs);
    version=FXIntVal(FXStringFormat("1%03d%03d%03d",v1,v2,v3));

    if(version>1001000000)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_dbnew_version").text());
        return false;
    }

    FXStringDict *roids=new FXStringDict();
    r=link->execute("SELECT * FROM t_gncases;");
    for(int i=0;i<r->getRowCount();i++)
    {
        FXString newId=r->getCellString(i,0);
        FXString newRd=r->getCellString(i,1);
        int ct=0;
        if(!roids->find(newRd.text()))
        {
            bool same=false;
            rr=oDataLink->execute("SELECT id FROM t_gncases WHERE reading_oid='"+newRd+"';");
            if(rr->getRowCount())
                same=true;
            delete rr;
            while(same)
            {
                same=false;
                newRd=r->getCellString(i,1)+FXStringVal(ct++);
                rr=oDataLink->execute("SELECT id FROM t_gncases WHERE reading_oid='"+newRd+"';");
                if(rr->getRowCount())
                    same=true;
                delete rr;
            }
            roids->insert(r->getCellString(i,1).text(),newRd.text());
        }
        if(cCaseManager::caseExists(newId,r->getCellString(i,4)))
        {
            int result=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),
                                              (r->getCellString(i,0)+"\n\n"+oLanguage->getText("str_cseman_duplicate")).text());
            switch(result)
            {
                case MBOX_CLICKED_YES:
                    cCaseManager::setCase(r->getCellString(i,0),r->getCellString(i,4),
                                          r->getCellString(i,0),
                                          r->getCellString(i,1),
                                          r->getCellString(i,2),
                                          r->getCellString(i,3),
                                          r->getCellString(i,4),
                                          r->getCellString(i,5),
                                          r->getCellString(i,6),
                                          r->getCellString(i,7),
                                          FXIntVal(r->getCellString(i,8)),
                                          FXIntVal(r->getCellString(i,9)),
                                          r->getCellString(i,10),
                                          FXIntVal(r->getCellString(i,11)),
                                          FXIntVal(r->getCellString(i,12)),
                                          FXIntVal(r->getCellString(i,13)),
                                          r->getCellString(i,14),
                                          r->getCellString(i,15),
                                          r->getCellString(i,16));
                    continue;
                    break;
                case MBOX_CLICKED_NO:
                    ct=1;
                    while(cCaseManager::caseExists(newId=r->getCellString(i,0)+"_"+FXStringVal(ct++),r->getCellString(i,4)));
                    break;
                default:
                    continue;
                    break;
            }
        }
        cCaseManager::addCase(newId,roids->find(r->getCellString(i,1).text()),
                              r->getCellString(i,2),
                              r->getCellString(i,3),
                              r->getCellString(i,4),
                              r->getCellString(i,5),
                              r->getCellString(i,6),
                              r->getCellString(i,7),
                              FXIntVal(r->getCellString(i,8)),
                              FXIntVal(r->getCellString(i,9)),
                              r->getCellString(i,10),
                              FXIntVal(r->getCellString(i,11)),
                              FXIntVal(r->getCellString(i,12)),
                              FXIntVal(r->getCellString(i,13)),
                              r->getCellString(i,14),
                              r->getCellString(i,15),
                              r->getCellString(i,16));
    }
    delete r;
    delete roids;

    r=link->execute("SELECT * FROM t_assays;");
    for(int i=0;i<r->getRowCount();i++)
    {
        if(!cAssayManager::assayExists(r->getCellString(i,0)))
        {
            oDataLink->execute(FXStringFormat("INSERT INTO t_assays VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s');",
                                              r->getCellString(i,0).text(),
                                              r->getCellString(i,1).text(),
                                              r->getCellString(i,2).text(),
                                              r->getCellString(i,3).text(),
                                              r->getCellString(i,4).text(),
                                              r->getCellString(i,5).text(),
                                              r->getCellString(i,6).text(),
                                              r->getCellString(i,7).text(),
                                              r->getCellString(i,8).text()
                                              ).text());
        }
    }
    delete r;

    r=link->execute("SELECT * FROM t_templates;");
    for(int i=0;i<r->getRowCount();i++)
    {
        if(!cTemplateManager::templateExists(r->getCellString(i,0)))
        {
            oDataLink->execute(FXStringFormat("INSERT INTO t_templates VALUES('%s','%s',%s,'%s','%s');",
                                              r->getCellString(i,0).text(),
                                              r->getCellString(i,1).text(),
                                              r->getCellString(i,2).text(),
                                              r->getCellString(i,3).text(),
                                              r->getCellString(i,4).text()
                                              ).text());
        }
    }
    delete r;

    cMDITemplateManager::cmdRefresh();
    cMDIAssayManager::cmdRefresh();

    cMDICaseManager::cmdRefresh();
    cMDICaseManager::cmdResearch();
    FXMessageBox::information(oWinMain,MBOX_OK,oLanguage->getText("str_information").text(),oLanguage->getText("str_success").text());
    return true;
}

void cCaseImport::doImport(void)
{
    if(!processData(getFileName()))
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_importerror").text());
}
