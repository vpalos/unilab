#ifndef CASSAYMANAGER_H
#define CASSAYMANAGER_H

#include <fx.h>

#include "cDataLink.h"
#include "cDataResult.h"

typedef struct sAssayObject
{
    FXint oid;
    FXString *id;
    FXString *title;
    FXint solid;
};

class cAssayManager
{
    private:
    protected:
    public:
        static FXbool assaySolid(const FXString &prId);
        static FXbool assayExists(const FXString &prId);
        static FXbool assayNeeded(const FXString &prId);
        static FXString getNewID(void);
        static int getAssayCount(void);

        static sAssayObject *listAssays(void);
        static cDataResult *getAssay(const FXString &prId);
        static cDataResult *getAssay(int prOid);
        static int getAssayOid(const FXString &prId);

        static FXString newAssay(FXMDIClient *prP, FXPopup *prPup);
        static FXString editAssay(FXMDIClient *prP, FXPopup *prPup,const FXString &prId);
        static FXString duplicateAssay(const FXString &prId);

        static FXbool addAssay(cDataLink *prLink,FXString prId,FXString prSolid,FXString prTitle,FXString prFilterA,FXString prFilterB,FXString prControls_defs,FXString prCalculation,FXString prUnit,FXString prComments);
        static FXbool addAssay(FXString prId,FXString prSolid,FXString prTitle,FXString prFilterA,FXString prFilterB,FXString prControls_defs,FXString prCalculation,FXString prUnit,FXString prComments);
        static FXbool addAssay(FXString prId,FXString prTitle,FXString prFilterA,FXString prFilterB,FXString prControls_defs,FXString prCalculation,FXString prUnit,FXString prComments);
        static FXbool setAssay(FXString prOldId,FXString prId,FXString prTitle,FXString prFilterA,FXString prFilterB,FXString prControls_defs,FXString prCalculation,FXString prUnit,FXString prComments);
        static FXbool removeAssay(const FXString &prId);
};

#endif
