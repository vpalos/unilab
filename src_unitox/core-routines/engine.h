#ifndef ENGINE_H
#define ENGINE_H

#include <fx.h>
#include "../index.h"

extern FXString APP_PATH;
extern FXString PACKAGES_PATH;
extern FXString DATABASES_PATH;
extern FXApp *oApplicationManager;
extern FXbool APP_CREATED;
extern FXbool APP_ITERATE;

int initiateCore(int prArgc,char **prArgv);

void fatalError(const FXString &prMessage);

int mainCanQuit(void);
void mainSaveSettings(void);
void quitProgram(void);

FXString val2key(FXString prVal);
FXString key2val(FXString prKey);

bool securityCheck(void);
FXString getLegal(void);

bool installSoftware(void);

#endif
