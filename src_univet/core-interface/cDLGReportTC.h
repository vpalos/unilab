#ifndef CDLGREPORTTC_H
#define CDLGREPORTTC_H

#include <fx.h>

class cDLGReportTC : public FXDialogBox
{
    FXDECLARE(cDLGReportTC);
    private:
        FXButton *butAccept;
        FXButton *butCancel;
        
    protected:
        FXDataTarget stype;
        FXRadioButton *bAssay;
        FXRadioButton *bCase;

        cDLGReportTC(){}
        
    public:
        FXuint schoice;
        FXCheckButton *bcAm;
        
        cDLGReportTC(FXWindow *prOwner);
        virtual ~cDLGReportTC();
        
        enum
        {
            ID_THIS=FXDialogBox::ID_LAST,
            ID_UPDATE,
            ID_FIELD,
            
            CMD_BUT_APPLY,
            ID_LAST
        };

        long onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
