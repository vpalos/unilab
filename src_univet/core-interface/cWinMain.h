#ifndef CWINMAIN_H
#define CWINMAIN_H

#include <fx.h>
#include "cMDIChild.h"
#include "cSplashWindow.h"

class cWinMain : public FXMainWindow
{
    FXDECLARE(cWinMain);

    private:
        FXString tech;
        FXDict *windows;
        FXMenuBar *menuBar;
        FXDict *readings;
        FXDict *vaccines;
        FXMenuPane *windowMenuPane;
        FXStatusBar *statusBar;
            FXLabel *statusTimer;
            FXLabel *statusTech;
        FXSplitter *mainSplitter;
            FXVerticalFrame *shutterContainer;
                FXToggleButton *toolToggleButton;
                FXShutter *shutter;
            FXLabel *mdiTitle;
            FXLabel *mdiSubtitle;
            FXMDIMenu *mdiMenu;
            FXMDIClient *mdiClient;
        cSplashWindow *sw;
        FXTreeList *helpTree;
        FXText *helpText;
        FXIcon *_img_helpdir;

    protected:
        bool constructHelpBranch(FXString prKey);

        cWinMain();

    public:
        cWinMain(FXApp *prApp,const FXString &prCaption);
        virtual ~cWinMain();

        virtual void create();

        int canQuit(void);
        void saveSettings(void);

        void inputTechnician(void);
        FXString getTechnician(void);

        int countWindows(void);
        FXbool isWindow(const FXString &prTitle);
        void addWindow(const FXString &prTitle,cMDIChild *prWindow);
        void removeWindow(const FXString &prTitle);
        cMDIChild *getWindow(const FXString &prTitle);
        FXbool raiseWindow(const FXString &prTitle);
        FXbool closeAllWindows(FXObject *prSender);

        FXDict *getReadings(void){return readings;};
        FXDict *getVaccines(void){return vaccines;};

        enum
        {
            EVT_STATUSTIMER=FXMainWindow::ID_LAST,
            CMD_QUITPROGRAM,

            ID_WINMAIN,
            ID_SHUTTER,
            ID_MDI_CLIENT,
            ID_MDI_CHILD,
            ID_MDI_TITLE,

            CMD_MFILE_STARTRD,
            CMD_MFILE_STARTMN,
            CMD_MFILE_BROWSERD,
            CMD_MFILE_NEWTPL,
            CMD_MFILE_TPLMANAGER,
            CMD_MFILE_DBPROPS,

            CMD_MDATABASE_NEWDB,
            CMD_MDATABASE_CLOSEDB,
            CMD_MDATABASE_DBMANAGER,
            CMD_MDATABASE_IMPORT,
            CMD_MDATABASE_EXPORT,
            //CMD_MDATABASE_CONVERT,

            CMD_MINPUT_VETS,
            CMD_MINPUT_OWNERS,
            CMD_MINPUT_GROWERS,
            CMD_MINPUT_VACCINE,
            CMD_MINPUT_BASELINE,
            CMD_MINPUT_KITS,
            CMD_MINPUT_BREEDS,

            CMD_HELP_TREE,
            CMD_HELP_LINK,

            CMD_MREPORT_CAN,
            CMD_MREPORT_MCAN,
            CMD_MREPORT_SBS,
            CMD_MREPORT_CCOMP,
            CMD_MREPORT_VACCHIST,
            CMD_MREPORT_VACCDATE,
            CMD_MREPORT_TOTCNT,
            CMD_MREPORT_TITBKD,
            CMD_MREPORT_BROWSE,

            CMD_MSETTINGS_SIDEBAR,
            CMD_MSETTINGS_OPTIONS,
            CMD_MSETTINGS_READER,
            CMD_MSETTINGS_PRINTER,
            CMD_MSETTINGS_LANG,
            CMD_MSETTINGS_TECH,

            CMD_MHELP_CONTENTS,
            CMD_MHELP_ABOUT,
            CMD_MHELP_QA,

            ID_LAST
        };

        long onRszWindow(FXObject *prSender,FXSelector prSelector,void *prData);
        long onEvtStatusTimer(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdQuitProgram(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdToolToggle(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdShutterSelect(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdMdiTitle(FXObject *prSender,FXSelector prSelector,void *prData);

        long onCmdMFileStartRead(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMFileStartManual(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMFileBrowseCases(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMFileTemplateNew(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMFileTemplateManager(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMFileDatabaseProperties(FXObject *prSender,FXSelector prSelector,void *prData);

        long onCmdMDatabaseNew(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMDatabaseManager(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMDatabaseExport(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMDatabaseImport(FXObject *prSender,FXSelector prSelector,void *prData);

        long onCmdMInputVeterinarianManager(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMInputOwnerManager(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMInputGrowerManager(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMInputVaccineManager(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMInputAssayManager(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMInputBreedManager(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMInputBaselineManager(FXObject *prSender,FXSelector prSelector,void *prData);

        long onCmdMReportsCA(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMReportsCC(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMReportsCM(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMReportsCS(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMReportsVH(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMReportsVD(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMReportsTC(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMReportsTB(FXObject *prSender,FXSelector prSelector,void *prData);

        long onCmdMSettingsGeneral(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMSettingsReader(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMSettingsPrinter(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMSettingsTech(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMSettingsLang(FXObject *prSender,FXSelector prSelector,void *prData);

        long onCmdHelpTree(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdHelpLink(FXObject *prSender,FXSelector prSelector,void *prData);

        long onSplashHide(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMHelpContents(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMHelpAbout(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdMHelpQA(FXObject *prSender,FXSelector prSelector,void *prData);
};

extern cWinMain *oWinMain;

#endif

