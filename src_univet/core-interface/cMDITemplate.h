#ifndef CMDITEMPLATE_H
#define CMDITEMPLATE_H

#include <fx.h>

#include "cMDIChild.h"
#include "cColorTable.h"

#define TEMPLATE_MAX_CONTROLS 50

class cMDITemplate : public cMDIChild
{
    FXDECLARE(cMDITemplate);
    
    private:
        FXTextField *tfName;
        FXLabel *lbDate;
        FXRadioButton *butPos;
        FXRadioButton *butNeg;
        FXRadioButton *butNone;
        FXDataTarget orientationTgt;
        FXDataTarget controlsTgt;
        FXDataTarget subcTgt;
        FXDataTarget alelisaTgt;
        FXuint orientationChoice;
        FXuint controlsChoice;
        FXuint oldControlsChoice;
        FXuint subcChoice;
        FXuint alelisaChoice;
        cColorTable *plateTable;
        cColorTable *caseList;
        FXbool saved;
        FXLabel *caselb;
        int controls[TEMPLATE_MAX_CONTROLS];
        int controlsCount;
        FXString name;
        FXLabel *lbPlates;
    
    protected:
        cMDITemplate();
        
        FXString sampleKey(int prIndex);
        
        int platePosition(int prRow,int prColumn=-1);
        int platePositionRow(int prIndex);
        int platePositionColumn(int prIndex);

        bool isControl(int prRow,int prColumn=-1);
        void placePositive(int prRow,int prColumn=-1);
        void placeNegative(int prRow,int prColumn=-1);
        void clearControl(int prRow,int prColumn=-1);
        void clearAllControls(void);
        
        void showPpDate(int prRow,int prColumn);
        void hidePpDate(void);
        
        void updateData(void);
        FXbool saveData(void);

        void drawCaseList(void);
        
    public:
        cMDITemplate(FXMDIClient *prP, const FXString &prName, FXIcon *prIc=NULL, FXPopup *prPup=NULL, FXuint prOpts=0, FXint prX=0, FXint prY=0, FXint prW=0, FXint prH=0);
        virtual ~cMDITemplate();
        
        virtual void create();
        
        virtual FXbool canClose(void);
        virtual FXbool loadTemplate(const FXString &prTitle);
        
        enum
        {
            EVT_LBDATE,
            ID_MDITEMPLATE=FXMDIChild::ID_LAST,
            ID_PLATETABLE,
            ID_CASELIST,
            ID_CASELIST_VALUE,
            ID_TPLNAME,
            
            ID_ORIENTATIONTGT,
            ID_CONTROLSTGT,
            ID_SUBCTGT,
            ID_ALELISATGT,
            
            CMD_SAVE,
            CMD_READ,
            CMD_FILL,
            CMD_PRINT,
            CMD_CLOSE,
            CMD_CASENEW,
            CMD_CASEDEL,
            CMD_CASEUP,
            CMD_CASEDN,
            
            ID_LAST
        };

        long onRszPlateTable(FXObject *prSender,FXSelector prSelector,void *prData);
        long onDbcPlateTable(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdPlateTable(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdSelPlateTable(FXObject *prSender,FXSelector prSelector,void *prData);

        long onRszCaseList(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdCaseList(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdSelCaseList(FXObject *prSender,FXSelector prSelector,void *prData);

        long onCmdPpDate(FXObject *prSender,FXSelector prSelector,void *prData);
        long onChgTfName(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdTfName(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdUpdateOrientation(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdUpdateControls(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdUpdateAlelisa(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdRead(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdFill(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData);

        long onCmdCaseList(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdCaseNew(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdCaseDel(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdCaseUp(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdCaseDown(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif

