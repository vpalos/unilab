#ifndef CMDIGROWER_H
#define CMDIGROWER_H

#include <fx.h>

#include "cMDIChild.h"
#include "cColorTable.h"

class cMDIGrower : public cMDIChild
{
    FXDECLARE(cMDIGrower);
    
    private:
        FXTextField *tfId;
        FXTextField *tfLab;
        FXText *tfAdd;
        FXTextField *tfCity;
        FXTextField *tfState;
        FXTextField *tfZip;
        FXTextField *tfPhone;
        FXTextField *tfFax;
        FXTextField *tfEmail;
        FXText *comments;

        FXbool saved;
        FXString name;
    
    protected:
        cMDIGrower();
        
        void updateData(void);
        FXbool saveData(void);
        
    public:
        cMDIGrower(FXMDIClient *prP,const FXString &prName,FXIcon *prIc=NULL,FXPopup *prPup=NULL,FXuint prOpts=0,FXint prX=0,FXint prY=0,FXint prW=0,FXint prH=0);
        virtual ~cMDIGrower();
        
        virtual void create();
        
        virtual FXbool canClose(void);
        virtual FXbool loadGrower(const FXString &prId);
        
        enum
        {
            ID_MDIGROWER=FXMDIChild::ID_LAST,
            ID_GRWID,
            ID_GRWLAB,
            ID_GRWADD,
            ID_GRWCITY,
            ID_GRWSTATE,
            ID_GRWZIP,
            ID_GRWPHONE,
            ID_GRWFAX,
            ID_GRWEMAIL,
            ID_GRWCOMMENTS,
            
            CMD_SAVE,
            CMD_PRINT,
            CMD_CLOSE,
            
            ID_LAST
        };

        long onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdTfId(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif

