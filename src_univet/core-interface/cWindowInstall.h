#ifndef CWINDOWINSTALL_H
#define CWINDOWINSTALL_H

#include <fx.h>

class cWindowInstall : public FXDialogBox
{
    FXDECLARE(cWindowInstall);
    
    private:
        FXTextField *tx;
        FXCheckButton *desk;
    
    protected:
        cWindowInstall();
    
    public:
        enum
        {
            ID_BROWSE=FXDialogBox::ID_LAST
        };
        
        cWindowInstall(FXApp *a, const FXString &name, FXuint opts=DECOR_TITLE|DECOR_BORDER, FXint x=0, FXint y=0, FXint w=0, FXint h=0, FXint pl=10, FXint pr=10, FXint pt=10, FXint pb=10, FXint hs=4, FXint vs=4);
        ~cWindowInstall();
        
        FXString getPath(void){return tx->getText();}
        FXbool getDesk(void){return desk->getCheck();}
        
        long onBrowse(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
