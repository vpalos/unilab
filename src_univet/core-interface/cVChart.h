#ifndef CVCHART_H
#define CVCHART_H

#include <fx.h>

typedef struct sVChartData
{
    FXString name;
    FXdouble value;
    FXColor color;
    FXString sValue;
};

class cVChart : public FXFrame
{
    FXDECLARE(cVChart);

    private:
    protected:
        FXuint type;
        FXbool isprint;

        FXString title;
        FXString vaxis;
        FXString haxis;

        FXColor canvasColor;
        FXColor borderColor;
        FXColor gridColor;
        FXColor valuesColor;
        FXColor dataColor;

        FXbool doubleBuffering;
        FXbool showValues;
        FXbool showVGrid;
        FXbool showLegend;

        FXDict *values;
        FXStringDict *legend;

        FXFont *fontValues;
        FXFont *fontTitle,*fontLegend;
        FXFont *fontAxis,*fontVAxis;

        FXint unit;
        FXdouble unitPixels;
        FXdouble elementPixels;
        FXdouble dataPixels;
        FXint clusterCount;
        FXint dataMin,dataMax;
        FXint vw,vh,lh,vah,hah,th,lvh;
        FXint setCount;
        int tw[20],tn[20],rows;

        FXint dataX,dataY,dataW,dataH;
        FXint dataCount;
        FXdouble space,clusterPixels,valPixels;

        cVChart();

        bool compute(FXDC &prDC);
        FXString sampleVal(double prValue);

        void fillRectPattern(FXDC &prDC,int x,int y,int w,int h,FXColor prColor);

        void drawCanvas(FXDC &prDC);
        void drawData(FXDC &prDC);
        void drawAxes(FXDC &prDC);

    public:
        enum
        {
            CHART_POINT=0x0000,
            CHART_LINE=0x1000,
            CHART_BAR=0x2000
        };

        cVChart(FXComposite *prP, FXuint prType=CHART_BAR, FXuint prOpts=0, FXint prX=0, FXint prY=0, FXint prW=0, FXint prH=0);
        ~cVChart();

        virtual void create();

        FXuint getType(void);
        void setType(FXuint prType=CHART_BAR);

        FXDict *dataSet(void);
        FXStringDict *legendStrings(void);

        FXString getTitle(void);
        FXString getHAxisTitle(void);
        FXString getVAxisTitle(void);
        void setTitle(FXString prText);
        void setHAxisTitle(FXString prText);
        void setVAxisTitle(FXString prText);

        FXColor getCanvasColor(void);
        FXColor getAxesColor(void);
        FXColor getGridColor(void);
        FXColor getDataColor(void);
        FXColor getValuesColor(void);
        void setCanvasColor(FXColor prColor);
        void setAxesColor(FXColor prColor);
        void setGridColor(FXColor prColor);
        void setDataColor(FXColor prColor);
        void setValuesColor(FXColor prColor);

        FXbool getDoubleBuffering(void);
        FXbool getShowValues(void);
        FXbool getShowGrid(void);
        FXbool getShowLegend(void);
        void setDoubleBuffering(FXbool prFlag);
        void setShowValues(FXbool prFlag);
        void setShowGrid(FXbool prFlag);
        void setShowLegend(FXbool prFlag);

        FXint getClusterCount(void);
        void setClusterCount(FXint prCount);

        long onPaint(FXObject *prSender,FXSelector prSelector,void *prData);
        long onEvtRedir(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
