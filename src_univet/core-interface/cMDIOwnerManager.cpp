#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cOwnerManager.h"
#include "cWinMain.h"
#include "cMDIDatabaseManager.h"
#include "cMDIOwnerManager.h"

cMDIOwnerManager *oMDIOwnerManager=NULL;

FXDEFMAP(cMDIOwnerManager) mapMDIOwnerManager[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIOwnerManager::ID_OWNERMANAGER,cMDIOwnerManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIOwnerManager::CMD_BUT_CLOSE,cMDIOwnerManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIOwnerManager::CMD_BUT_OPEN,cMDIOwnerManager::onCmdOpen),
    FXMAPFUNC(SEL_COMMAND,cMDIOwnerManager::CMD_BUT_NEW,cMDIOwnerManager::onCmdNew),
    FXMAPFUNC(SEL_COMMAND,cMDIOwnerManager::CMD_BUT_DUPLICATE,cMDIOwnerManager::onCmdDuplicate),
    FXMAPFUNC(SEL_DOUBLECLICKED,cMDIOwnerManager::ID_OWNERLIST,cMDIOwnerManager::onCmdOpen),
    FXMAPFUNC(SEL_COMMAND,cMDIOwnerManager::CMD_BUT_REMOVE,cMDIOwnerManager::onCmdRemove),
    
};

FXIMPLEMENT(cMDIOwnerManager,cMDIChild,mapMDIOwnerManager,ARRAYNUMBER(mapMDIOwnerManager))

cMDIOwnerManager::cMDIOwnerManager()
{
}

cMDIOwnerManager::cMDIOwnerManager(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH) 
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_OWNERMANAGER);
    client=prP;
    popup=prPup;
    
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(this,LAYOUT_FILL);
    FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe0,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
    new FXLabel(_vframe2,oLanguage->getText("str_ownman_ownlist_title"));
    FXHorizontalFrame *_hframe1=new FXHorizontalFrame(_vframe2,LAYOUT_FILL,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe4=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|LAYOUT_RIGHT,0,0,0,0,0,0,0,0,0,0);
    
    ownList=new cSortList(_vframe3,this,ID_OWNERLIST,ICONLIST_SINGLESELECT|ICONLIST_DETAILED|LAYOUT_FILL);
    ownList->appendHeader(oLanguage->getText("str_ownman_ownnameh"),NULL,120);
    ownList->appendHeader(oLanguage->getText("str_ownman_owntitleh"),NULL,264);
    oMDIOwnerManager=this;
    this->update();
    if(ownList->getNumItems()>0)
    {
        ownList->selectItem(0);
        ownList->setCurrentItem(0);
    }
    ownList->setFocus();

    new FXButton(_vframe4,oLanguage->getText("str_ownman_new"),NULL,this,CMD_BUT_NEW,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_FIX_WIDTH,0,0,88,0);
    new FXHorizontalSeparator(_vframe4,LAYOUT_TOP|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_ownman_open"),NULL,this,CMD_BUT_OPEN,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X);
    new FXButton(_vframe4,oLanguage->getText("str_ownman_duplicate"),NULL,this,CMD_BUT_DUPLICATE,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X);
    new FXButton(_vframe4,oLanguage->getText("str_but_close"),NULL,this,CMD_BUT_CLOSE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
    new FXHorizontalSeparator(_vframe4,LAYOUT_BOTTOM|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_ownman_remove"),NULL,this,CMD_BUT_REMOVE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
}

cMDIOwnerManager::~cMDIOwnerManager()
{
    cMDIOwnerManager::unload();
}

void cMDIOwnerManager::create()
{
    cMDIChild::create();
    show();
}

void cMDIOwnerManager::load(FXMDIClient *prP,FXPopup *prMenu)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(prP,prMenu);
        return;
    }
    
    if(oMDIOwnerManager!=NULL)
    {
        oMDIOwnerManager->setFocus();
        prP->setActiveChild(oMDIOwnerManager);
        if(oMDIOwnerManager->isMinimized())
            oMDIOwnerManager->restore();
        return;
    }
    new cMDIOwnerManager(prP,oLanguage->getText("str_ownman_title"),new FXGIFIcon(oApplicationManager,data_inputs),prMenu,MDI_NORMAL|MDI_TRACKING,10,10,515,200);
    oMDIOwnerManager->create();
    oMDIOwnerManager->setFocus();

}

FXbool cMDIOwnerManager::isLoaded(void)
{
    return (oMDIOwnerManager!=NULL);
}

void cMDIOwnerManager::unload(void)
{
    oMDIOwnerManager=NULL;
}

void cMDIOwnerManager::update(void)
{
    if(!isLoaded())
        return;
    sOwnerObject *res=cOwnerManager::listOwners();
    if(res==NULL)
        return;
    ownList->clearItems();
    for(int i=0;i<cOwnerManager::getOwnerCount();i++)
    ownList->appendItem(*(res[i].id)+"\t"+(res[i].solid>0?"* ":"")+*(res[i].title));
    ownList->sortItems();
    free(res);
}

void cMDIOwnerManager::cmdRefresh(void)
{
    if(!isLoaded())
        return;
    oMDIOwnerManager->update();
}

long cMDIOwnerManager::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDIOwnerManager::unload();
    close();
    return 1;
}

long cMDIOwnerManager::onCmdNew(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cOwnerManager::newOwner(client,popup);
    return 1;
}

long cMDIOwnerManager::onCmdOpen(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int db=-1,i;
    for(i=0;i<ownList->getNumItems();i++)
        if(ownList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        db=ownList->getCurrentItem();
    if(db==-1)
        return 1;
    if(cOwnerManager::ownerSolid(ownList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_eraseownsolid").text());
        return 1;
    }
    FXString st=ownList->getItemText(db).before('\t');
    if(oWinMain->isWindow(st))
        oWinMain->raiseWindow(st);
    else
        cOwnerManager::editOwner(client,popup,st);
    return 1;
}

long cMDIOwnerManager::onCmdDuplicate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int db=-1,i,j;
    for(i=0;i<ownList->getNumItems();i++)
        if(ownList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        return 1;
    FXString res2=cOwnerManager::duplicateOwner(ownList->getItemText(db).before('\t').text());
    this->update();
    if(res2.empty())
        return 1;
    for(j=0;j<ownList->getNumItems();j++)
        if(res2==ownList->getItemText(j).before('\t'))
        {
            ownList->selectItem(j);
            break;
        }
    return 1;
}

long cMDIOwnerManager::onCmdRemove(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i,db=-1;
    for(i=0;i<ownList->getNumItems();i++)
        if(ownList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        return 1;
    if(cOwnerManager::ownerSolid(ownList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_eraseownsolid").text());
        return 1;
    }
    if(cOwnerManager::ownerNeeded(ownList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasedataneeded").text());
        return 1;
    }
    if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_eraseown").text()))
        return 1;
    cMDIChild *win=oWinMain->getWindow(ownList->getItemText(db).before('\t'));
    if(win!=NULL)
        win->close();
    cOwnerManager::removeOwner(ownList->getItemText(db).before('\t'));
    this->update();
    if(db>=ownList->getNumItems())
        db=ownList->getNumItems()-1;
    if(db>=0)
        ownList->selectItem(db);
    return 1;
}


