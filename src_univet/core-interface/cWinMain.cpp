#include <time.h>
#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDatabaseManager.h"
#include "cBaselineManager.h"
#include "cReadersManager.h"
#include "cReadingsManager.h"
#include "cTemplateManager.h"
#include "cCaseAnalysisReport.h"
#include "cCaseCompareReport.h"
#include "cCaseCombineReport.h"
#include "cCaseSbsReport.h"
#include "cTotalCountsReport.h"
#include "cTiterBreakdownReport.h"
#include "cVaccineHistoryReport.h"
#include "cVaccineDateReport.h"
#include "cCaseExport.h"
#include "cCaseImport.h"
#include "cDLGChooseAT.h"
#include "cDLGCasePopEdit.h"
#include "cMDIAssayManager.h"
#include "cMDIVaccineManager.h"
#include "cMDICaseManager.h"
#include "cMDIDatabaseManager.h"
#include "cMDIBaselineManager.h"
#include "cMDITemplateManager.h"
#include "cMDIVeterinarianManager.h"
#include "cMDIBreedManager.h"
#include "cMDIOwnerManager.h"
#include "cMDIGrowerManager.h"
#include "cMDIReaderManager.h"
#include "cMDIReport.h"
#include "cWinMain.h"
#include <FXRGBIcon.h>
#include <FXXPMIcon.h>

cWinMain *oWinMain;

#define WINMAIN_MIN_W 630
#define WINMAIN_MIN_H 450

FXDEFMAP(cWinMain) mapWinMain[]=
{
    FXMAPFUNC(SEL_UPDATE,0,cWinMain::onRszWindow),
    FXMAPFUNC(SEL_TIMEOUT,cWinMain::EVT_STATUSTIMER,cWinMain::onEvtStatusTimer),

    FXMAPFUNC(SEL_LEFTBUTTONPRESS,0,cWinMain::onSplashHide),

    FXMAPFUNC(SEL_CLOSE,cWinMain::ID_WINMAIN,cWinMain::onCmdQuitProgram),
    FXMAPFUNC(SEL_FOCUSIN,cWinMain::ID_SHUTTER,cWinMain::onCmdShutterSelect),

    FXMAPFUNC(SEL_SIGNAL,cWinMain::CMD_QUITPROGRAM,cWinMain::onCmdQuitProgram),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_QUITPROGRAM,cWinMain::onCmdQuitProgram),

    FXMAPFUNC(SEL_CHANGED,cWinMain::ID_MDI_CLIENT,cWinMain::onUpdMdiTitle),
    FXMAPFUNC(SEL_UPDATE,cWinMain::ID_MDI_TITLE,cWinMain::onUpdMdiTitle),

    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_HELP_TREE,cWinMain::onCmdHelpTree),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_HELP_LINK,cWinMain::onCmdHelpLink),

    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MFILE_STARTRD,cWinMain::onCmdMFileStartRead),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MFILE_STARTMN,cWinMain::onCmdMFileStartManual),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MFILE_BROWSERD,cWinMain::onCmdMFileBrowseCases),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MFILE_NEWTPL,cWinMain::onCmdMFileTemplateNew),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MFILE_TPLMANAGER,cWinMain::onCmdMFileTemplateManager),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MFILE_DBPROPS,cWinMain::onCmdMFileDatabaseProperties),

    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MDATABASE_NEWDB,cWinMain::onCmdMDatabaseNew),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MDATABASE_DBMANAGER,cWinMain::onCmdMDatabaseManager),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MDATABASE_IMPORT,cWinMain::onCmdMDatabaseImport),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MDATABASE_EXPORT,cWinMain::onCmdMDatabaseExport),

    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MINPUT_VETS,cWinMain::onCmdMInputVeterinarianManager),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MINPUT_OWNERS,cWinMain::onCmdMInputOwnerManager),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MINPUT_GROWERS,cWinMain::onCmdMInputGrowerManager),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MINPUT_VACCINE,cWinMain::onCmdMInputVaccineManager),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MINPUT_KITS,cWinMain::onCmdMInputAssayManager),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MINPUT_BREEDS,cWinMain::onCmdMInputBreedManager),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MINPUT_BASELINE,cWinMain::onCmdMInputBaselineManager),

    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MREPORT_CAN,cWinMain::onCmdMReportsCA),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MREPORT_MCAN,cWinMain::onCmdMReportsCM),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MREPORT_SBS,cWinMain::onCmdMReportsCS),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MREPORT_CCOMP,cWinMain::onCmdMReportsCC),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MREPORT_VACCHIST,cWinMain::onCmdMReportsVH),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MREPORT_VACCDATE,cWinMain::onCmdMReportsVD),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MREPORT_TOTCNT,cWinMain::onCmdMReportsTC),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MREPORT_TITBKD,cWinMain::onCmdMReportsTB),

    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MSETTINGS_SIDEBAR,cWinMain::onCmdToolToggle),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MSETTINGS_OPTIONS,cWinMain::onCmdMSettingsGeneral),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MSETTINGS_READER,cWinMain::onCmdMSettingsReader),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MSETTINGS_PRINTER,cWinMain::onCmdMSettingsPrinter),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MSETTINGS_TECH,cWinMain::onCmdMSettingsTech),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MSETTINGS_LANG,cWinMain::onCmdMSettingsLang),

    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MHELP_CONTENTS,cWinMain::onCmdMHelpContents),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MHELP_ABOUT,cWinMain::onCmdMHelpAbout),
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_MHELP_QA,cWinMain::onCmdMHelpQA),
};

FXIMPLEMENT(cWinMain,FXMainWindow,mapWinMain,ARRAYNUMBER(mapWinMain))

cWinMain::cWinMain()
{
}

bool cWinMain::constructHelpBranch(FXString prKey)
{
    int ct=1;
    FXString prefix=prKey.mid(1,prKey.length()-1).substitute("_",".");
    if((!prefix.empty()) && (prefix[prefix.length()-1]=='.'))
        prefix=prefix.mid(0,prKey.length()-1);
    FXTreeItem *father=helpTree->findItem(prefix.rbefore('.')+" ",NULL,SEARCH_FORWARD|SEARCH_PREFIX);
    while(oLanguage->getHelp()->existingEntry("INDEX",FXStringFormat("%s%d_",prKey.text(),ct).text()))
    {
        FXString key=FXStringFormat("%s%d_",prKey.text(),ct);
        FXString index=oLanguage->getHelp()->readStringEntry("INDEX",key.text(),"...");
        FXString *text=new FXString(oLanguage->getHelp()->readStringEntry("TEXT",FXStringFormat("t%s%d_",prKey.mid(1,prKey.length()-1).text(),ct).text(),"*"));

        bool hc=false;
        if(oLanguage->getHelp()->existingEntry("INDEX",FXStringFormat("%s1_",key.text()).text()))
            hc=true;

        helpTree->appendItem(father,
                             FXStringFormat("%s%d %s",prefix.text(),ct,index.text()),
                             NULL,NULL,
                             (*text=="*")?NULL:text);

        constructHelpBranch(key);

        ct++;
    }
    return true;
}

cWinMain::cWinMain(FXApp *prApp,const FXString &prCaption)
    : FXMainWindow(prApp,prCaption,NULL,NULL,DECOR_ALL,0,0,WINMAIN_MIN_W,WINMAIN_MIN_H,3,2,0,0)
{
    // Icons
    FXIcon *_img_toolmbopen=new FXGIFIcon(getApp(),data_toolmbopen);
    FXIcon *_img_toolmbclose=new FXGIFIcon(getApp(),data_toolmbclose);
    _img_helpdir=NULL;//new FXGIFIcon(getApp(),data_help_entry);

    // Main window settings
    setIcon(new FXGIFIcon(getApp(),data_appicon32));
    setMiniIcon(new FXGIFIcon(getApp(),data_appicon16));
    setTarget(this);
    setSelector(ID_WINMAIN);
    new FXToolTip(getApp());
    //getApp()->setTooltipPause(700);

    // Main menu bar
    menuBar=new FXMenuBar(this,LAYOUT_SIDE_TOP|LAYOUT_FILL_X);
    FXMenuPane *_menuPane;
        // File menu
        _menuPane=new FXMenuPane(this);
        new FXMenuTitle(menuBar,oLanguage->getText("str_mfile"),NULL,_menuPane);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mfile_1"),new FXGIFIcon(getApp(),data_browse_readings),this,CMD_MFILE_BROWSERD);
            new FXMenuSeparator(_menuPane);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mfile_0"),new FXGIFIcon(getApp(),data_start_reading),this,CMD_MFILE_STARTRD);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mfile_7"),new FXGIFIcon(getApp(),data_start_manual),this,CMD_MFILE_STARTMN);
            new FXMenuSeparator(_menuPane);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mfile_2"),new FXGIFIcon(getApp(),data_new_template),this,CMD_MFILE_NEWTPL);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mfile_3"),new FXGIFIcon(getApp(),data_browse_templates),this,CMD_MFILE_TPLMANAGER);
            new FXMenuSeparator(_menuPane);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mfile_4"),new FXGIFIcon(getApp(),data_properties_db),this,CMD_MFILE_DBPROPS);
            new FXMenuSeparator(_menuPane);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mfile_6"),NULL,this,CMD_QUITPROGRAM);

        // Database menu
        _menuPane=new FXMenuPane(this);
        new FXMenuTitle(menuBar,oLanguage->getText("str_mdatabase"),NULL,_menuPane);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mdatabase_0"),new FXGIFIcon(getApp(),data_new_db),this,CMD_MDATABASE_NEWDB);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mdatabase_1"),new FXGIFIcon(getApp(),data_open_db),this,CMD_MDATABASE_DBMANAGER);
            new FXMenuSeparator(_menuPane);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mdatabase_3"),new FXGIFIcon(getApp(),data_import),this,CMD_MDATABASE_IMPORT);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mdatabase_4"),new FXGIFIcon(getApp(),data_export),this,CMD_MDATABASE_EXPORT);
            //new FXMenuCommand(_menuPane,oLanguage->getText("str_mdatabase_5"),new FXGIFIcon(getApp(),data_convert_db),this,CMD_MDATABASE_CONVERT);

        // Input menu
        _menuPane=new FXMenuPane(this);
        new FXMenuTitle(menuBar,oLanguage->getText("str_minput"),NULL,_menuPane);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_minput_5"),new FXGIFIcon(getApp(),data_inputs),this,CMD_MINPUT_KITS);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_minput_3"),new FXGIFIcon(getApp(),data_inputs),this,CMD_MINPUT_VACCINE);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_minput_4"),new FXGIFIcon(getApp(),data_inputs),this,CMD_MINPUT_BASELINE);
            new FXMenuSeparator(_menuPane);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_minput_0"),new FXGIFIcon(getApp(),data_inputs),this,CMD_MINPUT_VETS);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_minput_1"),new FXGIFIcon(getApp(),data_inputs),this,CMD_MINPUT_OWNERS);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_minput_2"),new FXGIFIcon(getApp(),data_inputs),this,CMD_MINPUT_GROWERS);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_minput_8"),new FXGIFIcon(getApp(),data_inputs),this,CMD_MINPUT_BREEDS);

        // Report menu
        _menuPane=new FXMenuPane(this);
        new FXMenuTitle(menuBar,oLanguage->getText("str_mreport"),NULL,_menuPane);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mreport_0"),new FXGIFIcon(getApp(),data_reports_ca),this,CMD_MREPORT_CAN);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mreport_2"),new FXGIFIcon(getApp(),data_reports_cca),this,CMD_MREPORT_CCOMP);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mreport_6"),new FXGIFIcon(getApp(),data_reports_pot),this,CMD_MREPORT_SBS);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mreport_1"),new FXGIFIcon(getApp(),data_reports_mca),this,CMD_MREPORT_MCAN);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mreport_7"),new FXGIFIcon(getApp(),data_reports_vacd),this,CMD_MREPORT_TOTCNT);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mreport_8"),new FXGIFIcon(getApp(),data_reports_vacd),this,CMD_MREPORT_TITBKD);
            new FXMenuSeparator(_menuPane);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mreport_3"),new FXGIFIcon(getApp(),data_reports_vacc),this,CMD_MREPORT_VACCHIST);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_mreport_4"),new FXGIFIcon(getApp(),data_reports_vacd),this,CMD_MREPORT_VACCDATE);

        // Settings menu
        _menuPane=new FXMenuPane(this);
        new FXMenuTitle(menuBar,oLanguage->getText("str_msettings"),NULL,_menuPane);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_msettings_0"),NULL,this,CMD_MSETTINGS_SIDEBAR);
            new FXMenuSeparator(_menuPane);
            //new FXMenuCommand(_menuPane,oLanguage->getText("str_msettings_1"),new FXGIFIcon(getApp(),data_settings_general),this,CMD_MSETTINGS_OPTIONS);
            //new FXMenuSeparator(_menuPane);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_msettings_2"),new FXGIFIcon(getApp(),data_settings_readers),this,CMD_MSETTINGS_READER);
            //new FXMenuCommand(_menuPane,oLanguage->getText("str_msettings_3"),new FXGIFIcon(getApp(),data_settings_printer),this,CMD_MSETTINGS_PRINTER);
            //new FXMenuSeparator(_menuPane);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_msettings_5"),NULL,this,CMD_MSETTINGS_LANG);
            new FXMenuCommand(_menuPane,oLanguage->getText("str_msettings_4"),NULL,this,CMD_MSETTINGS_TECH);

    // Status bar
    statusBar=new FXStatusBar(this,LAYOUT_SIDE_BOTTOM|LAYOUT_FILL_X|LAYOUT_BOTTOM,0,0,0,0,0,0,2,2);
    statusTimer=new FXLabel(statusBar,"",NULL,LAYOUT_RIGHT|LAYOUT_CENTER_Y);
    statusTimer->setTextColor(FXRGB(150,0,0));
    statusTech=new FXLabel(statusBar,"---",NULL,LAYOUT_RIGHT|LAYOUT_CENTER_Y|FRAME_GROOVE);

    // Content splitter
    mainSplitter=new FXSplitter(this,SPLITTER_HORIZONTAL|LAYOUT_FILL);

    // Instrument toolkit (left side content)
    shutterContainer=new FXVerticalFrame(mainSplitter,LAYOUT_FILL_Y|LAYOUT_FIX_WIDTH,0,0,0,0,0,0,0,0,0,0);
        FXSeparator *shutterSeparator=new FXSeparator(shutterContainer,SEPARATOR_LINE|LAYOUT_FILL_X);
        shutterSeparator->setBorderColor(FXRGB(180,180,180));

        // Instrument title
        FXHorizontalFrame *shutterUpperContainer=new FXHorizontalFrame(shutterContainer,LAYOUT_FILL_X|LAYOUT_TOP,0,0,0,0,0,0,0,0,0,0);
        FXButton *toolCloseButton=new FXButton(shutterUpperContainer,(char*)NULL,new FXGIFIcon(getApp(),data_toolclose),this,CMD_MSETTINGS_SIDEBAR,BUTTON_TOOLBAR|LAYOUT_RIGHT|FRAME_RAISED|LAYOUT_CENTER_Y);
        toolCloseButton->setTipText(oLanguage->getText("str_tooltogglebuttonclose"));
        FXLabel *toolboxLabel=new FXLabel(shutterUpperContainer,oLanguage->getText("str_tooltitle"),NULL,LAYOUT_FILL_X|JUSTIFY_LEFT|LAYOUT_CENTER_Y,0,0,0,0,0,0,4,3);
        toolboxLabel->setTextColor(FXRGB(140,140,140));

        // Shutter
        shutter=new FXShutter(shutterContainer,this,ID_SHUTTER,FRAME_SUNKEN|LAYOUT_FILL,0,0,200,0,0,0,0,0,0,0);

        // Instruments
        FXShutterItem *shutterItem;
            // Quick tools
            shutterItem=new FXShutterItem(shutter,oLanguage->getText("str_toolquick"),NULL,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,1);
            shutterItem->getButton()->setFrameStyle(FRAME_RAISED);
            shutterItem->getButton()->setFont(new FXFont(getApp(),"Helvetica",8,FXFont::Bold));
            shutterItem->getButton()->setPadLeft(5);
            shutterItem->getButton()->setPadTop(2);
            shutterItem->getButton()->setPadBottom(2);
            shutterItem->getButton()->setTextColor(FXRGB(150,0,0));
            shutterItem->getButton()->setJustify(JUSTIFY_LEFT);
                FXButton *_button;
                _button=new FXButton(shutterItem->getContent(),oLanguage->getText("str_mfile_0").substitute("&",""),new FXGIFIcon(getApp(),data_start_reading),this,CMD_MFILE_STARTRD,LAYOUT_FILL_X|FRAME_RAISED|ICON_ABOVE_TEXT|BUTTON_TOOLBAR|LAYOUT_FIX_HEIGHT,0,0,0,55);
                _button->setBackColor(FXRGB(213,210,210));
                _button->setFont(new FXFont(getApp(),"Helvetica",11,FXFont::Bold));
                _button=new FXButton(shutterItem->getContent(),oLanguage->getText("str_mfile_7").substitute("&",""),new FXGIFIcon(getApp(),data_start_manual),this,CMD_MFILE_STARTMN,LAYOUT_FILL_X|FRAME_RAISED|ICON_ABOVE_TEXT|BUTTON_TOOLBAR|LAYOUT_FIX_HEIGHT,0,0,0,55);
                _button->setBackColor(FXRGB(213,210,210));
                _button->setFont(new FXFont(getApp(),"Helvetica",11,FXFont::Bold));
                new FXSeparator(shutterItem->getContent(),SEPARATOR_NONE|LAYOUT_FILL_X);
                _button=new FXButton(shutterItem->getContent(),oLanguage->getText("str_mfile_1").substitute("&",""),new FXGIFIcon(getApp(),data_browse_readings),this,CMD_MFILE_BROWSERD,LAYOUT_FILL_X|FRAME_RAISED|ICON_ABOVE_TEXT|BUTTON_TOOLBAR|LAYOUT_FIX_HEIGHT,0,0,0,55);
                _button->setBackColor(FXRGB(223,220,220));
                _button->setFont(new FXFont(getApp(),"Helvetica",11,FXFont::Bold));
                _button=new FXButton(shutterItem->getContent(),oLanguage->getText("str_mfile_2").substitute("&",""),new FXGIFIcon(getApp(),data_new_template),this,CMD_MFILE_NEWTPL,LAYOUT_FILL_X|FRAME_RAISED|ICON_ABOVE_TEXT|BUTTON_TOOLBAR|LAYOUT_FIX_HEIGHT,0,0,0,55);
                _button->setBackColor(FXRGB(223,220,220));
                _button->setFont(new FXFont(getApp(),"Helvetica",11,FXFont::Bold));
                _button=new FXButton(shutterItem->getContent(),oLanguage->getText("str_mfile_3").substitute("&",""),new FXGIFIcon(getApp(),data_browse_templates),this,CMD_MFILE_TPLMANAGER,LAYOUT_FILL_X|FRAME_RAISED|ICON_ABOVE_TEXT|BUTTON_TOOLBAR|LAYOUT_FIX_HEIGHT,0,0,0,55);
                _button->setBackColor(FXRGB(223,220,220));
                _button->setFont(new FXFont(getApp(),"Helvetica",11,FXFont::Bold));
                new FXSeparator(shutterItem->getContent(),SEPARATOR_NONE|LAYOUT_FILL_X);
                _button=new FXButton(shutterItem->getContent(),oLanguage->getText("str_mdatabase_1").substitute("&",""),new FXGIFIcon(getApp(),data_open_db),this,CMD_MDATABASE_DBMANAGER,LAYOUT_FILL_X|FRAME_RAISED|ICON_ABOVE_TEXT|BUTTON_TOOLBAR|LAYOUT_FIX_HEIGHT,0,0,0,55);
                _button->setBackColor(FXRGB(213,210,210));
                _button->setFont(new FXFont(getApp(),"Helvetica",11,FXFont::Bold));
                _button=new FXButton(shutterItem->getContent(),oLanguage->getText("str_minput_3").substitute("&",""),new FXGIFIcon(getApp(),data_inputs),this,CMD_MINPUT_VACCINE,LAYOUT_FILL_X|FRAME_RAISED|ICON_ABOVE_TEXT|BUTTON_TOOLBAR|LAYOUT_FIX_HEIGHT,0,0,0,55);
                _button->setBackColor(FXRGB(213,210,210));
                _button->setFont(new FXFont(getApp(),"Helvetica",11,FXFont::Bold));
                new FXSeparator(shutterItem->getContent(),SEPARATOR_NONE|LAYOUT_FILL_X);
                _button=new FXButton(shutterItem->getContent(),oLanguage->getText("str_minput_5").substitute("&",""),new FXGIFIcon(getApp(),data_inputs),this,CMD_MINPUT_KITS,LAYOUT_FILL_X|FRAME_RAISED|ICON_ABOVE_TEXT|BUTTON_TOOLBAR|LAYOUT_FIX_HEIGHT,0,0,0,55);
                _button->setBackColor(FXRGB(223,220,220));
                _button->setFont(new FXFont(getApp(),"Helvetica",11,FXFont::Bold));
                /*_button=new FXButton(shutterItem->getContent(),oLanguage->getText("str_minput_1").substitute("&",""),new FXGIFIcon(getApp(),data_inputs),this,CMD_MINPUT_OWNERS,LAYOUT_FILL_X|FRAME_RAISED|ICON_ABOVE_TEXT|BUTTON_TOOLBAR|LAYOUT_FIX_HEIGHT,0,0,0,55);
                _button->setBackColor(FXRGB(223,220,220));
                _button->setFont(new FXFont(getApp(),"Helvetica",11,FXFont::Bold));
                _button=new FXButton(shutterItem->getContent(),oLanguage->getText("str_minput_2").substitute("&",""),new FXGIFIcon(getApp(),data_inputs),this,CMD_MINPUT_GROWERS,LAYOUT_FILL_X|FRAME_RAISED|ICON_ABOVE_TEXT|BUTTON_TOOLBAR|LAYOUT_FIX_HEIGHT,0,0,0,55);
                _button->setBackColor(FXRGB(223,220,220));
                _button->setFont(new FXFont(getApp(),"Helvetica",11,FXFont::Bold));
                _button=new FXButton(shutterItem->getContent(),oLanguage->getText("str_minput_8").substitute("&",""),new FXGIFIcon(getApp(),data_inputs),this,CMD_MINPUT_BREEDS,LAYOUT_FILL_X|FRAME_RAISED|ICON_ABOVE_TEXT|BUTTON_TOOLBAR|LAYOUT_FIX_HEIGHT,0,0,0,55);
                _button->setBackColor(FXRGB(223,220,220));
                _button->setFont(new FXFont(getApp(),"Helvetica",11,FXFont::Bold));*/
                new FXSeparator(shutterItem->getContent(),SEPARATOR_NONE|LAYOUT_FILL_X);
                _button=new FXButton(shutterItem->getContent(),oLanguage->getText("str_mfile_6").substitute("&",""),NULL,this,CMD_QUITPROGRAM,LAYOUT_FILL_X|FRAME_RAISED|ICON_ABOVE_TEXT|BUTTON_TOOLBAR|LAYOUT_FIX_HEIGHT,0,0,0,55);
                _button->setBackColor(FXRGB(183,180,180));
                _button->setFont(new FXFont(getApp(),"Helvetica",11,FXFont::Bold));
                new FXSeparator(shutterItem->getContent(),SEPARATOR_NONE|LAYOUT_FILL_X);
                new FXFrame(shutterItem->getContent(),LAYOUT_FILL);

            /* Template manager
            shutterItem=new FXShutterItem(shutter,oLanguage->getText("str_tooltemplates"),NULL,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
            shutterItem->getButton()->setFrameStyle(FRAME_RAISED);
            shutterItem->getButton()->setFont(new FXFont(getApp(),"Helvetica",8,FONTWEIGHT_BOLD));
            shutterItem->getButton()->setPadLeft(5);
            shutterItem->getButton()->setPadTop(2);
            shutterItem->getButton()->setPadBottom(2);
            shutterItem->getButton()->setTextColor(FXRGB(0,120,0));
            shutterItem->getButton()->setJustify(JUSTIFY_LEFT);

            // Case readings browser
            shutterItem=new FXShutterItem(shutter,oLanguage->getText("str_toolreadings"),NULL,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
            shutterItem->getButton()->setFrameStyle(FRAME_RAISED);
            shutterItem->getButton()->setFont(new FXFont(getApp(),"Helvetica",8,FONTWEIGHT_BOLD));
            shutterItem->getButton()->setPadLeft(5);
            shutterItem->getButton()->setPadTop(2);
            shutterItem->getButton()->setPadBottom(2);
            shutterItem->getButton()->setTextColor(FXRGB(0,120,0));
            shutterItem->getButton()->setJustify(JUSTIFY_LEFT);

            // Reports browser
            shutterItem=new FXShutterItem(shutter,oLanguage->getText("str_toolreports"),NULL,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
            shutterItem->getButton()->setFrameStyle(FRAME_RAISED);
            shutterItem->getButton()->setFont(new FXFont(getApp(),"Helvetica",8,FONTWEIGHT_BOLD));
            shutterItem->getButton()->setPadLeft(5);
            shutterItem->getButton()->setPadTop(2);
            shutterItem->getButton()->setPadBottom(2);
            shutterItem->getButton()->setTextColor(FXRGB(0,120,0));
            shutterItem->getButton()->setJustify(JUSTIFY_LEFT);*/

            // Help contents
            shutterItem=new FXShutterItem(shutter,oLanguage->getText("str_toolhelp"),NULL,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
            shutterItem->getButton()->setFrameStyle(FRAME_RAISED);
            shutterItem->getButton()->setFont(new FXFont(getApp(),"Helvetica",8,FXFont::Bold));
            shutterItem->getButton()->setPadLeft(5);
            shutterItem->getButton()->setPadTop(2);
            shutterItem->getButton()->setPadBottom(2);
            shutterItem->getButton()->setTextColor(FXRGB(0,0,150));
            shutterItem->getButton()->setJustify(JUSTIFY_LEFT);

            FXSplitter *hlps=new FXSplitter(shutterItem->getContent(),SPLITTER_VERTICAL|LAYOUT_FILL);
                hlps->setBackColor(FXRGB(120,120,120));
                helpTree=new FXTreeList(hlps,this,CMD_HELP_TREE,TREELIST_SINGLESELECT|TREELIST_SHOWS_LINES|TREELIST_SHOWS_BOXES|TREELIST_ROOT_BOXES|LAYOUT_FILL|FRAME_SUNKEN,0,0,0,250);
                if(oLanguage->getHelp()->existingEntry("INDEX","e1_"))
                    constructHelpBranch("e");
                else
                    hlps->hide();
                helpText=new FXText(hlps,NULL,0,LAYOUT_FILL|FRAME_SUNKEN|TEXT_READONLY|TEXT_WORDWRAP,0,0,0,0,5,5,5,5);
                helpTree->setScrollStyle(HSCROLLING_ON|VSCROLLING_ON|HSCROLLER_NEVER);
                helpText->setScrollStyle(HSCROLLING_ON|VSCROLLING_ON|VSCROLLER_ALWAYS);
                helpTree->setTextColor(FXRGB(0,20,190));
                helpText->setTextColor(FXRGB(120,120,120));

    // MDI container (right side content)
    FXVerticalFrame *splitterInner=new FXVerticalFrame(mainSplitter,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
        // MDI client
        FXSeparator *mdiBarSeparator=new FXSeparator(splitterInner,SEPARATOR_LINE|LAYOUT_FILL_X);
        mdiBarSeparator->setBorderColor(FXRGB(180,180,180));
        FXHorizontalFrame *mdiBar=new FXHorizontalFrame(splitterInner,LAYOUT_TOP|LAYOUT_FILL_X,0,0,0,0,0,0,0,0,2,0);
        toolToggleButton=new FXToggleButton(mdiBar,(char*)NULL,(char*)NULL,_img_toolmbopen,_img_toolmbclose,this,CMD_MSETTINGS_SIDEBAR,BUTTON_TOOLBAR|LAYOUT_LEFT|FRAME_RAISED|LAYOUT_CENTER_Y);
        toolToggleButton->setState(getApp()->reg().readIntEntry("settings","showInstrumentToolkit",1));
        toolToggleButton->setTipText(oLanguage->getText("str_tooltogglebuttonopen"));
        toolToggleButton->setAltTipText(oLanguage->getText("str_tooltogglebuttonclose"));

        FXHorizontalFrame *_mdiFrame=new FXHorizontalFrame(splitterInner,LAYOUT_FILL|FRAME_SUNKEN,0,0,0,0,0,0,0,0);
        mdiClient=new FXMDIClient(_mdiFrame,LAYOUT_FILL);
        mdiClient->setTarget(this);
        mdiClient->setSelector(ID_MDI_CLIENT);
        mdiMenu=new FXMDIMenu(this,mdiClient);

        new FXMDIWindowButton(mdiBar,mdiMenu,mdiClient,FXMDIClient::ID_MDI_MENUWINDOW,LAYOUT_LEFT|LAYOUT_CENTER_Y);

        mdiTitle=new FXLabel(mdiBar,"Database Title",NULL,LAYOUT_LEFT|JUSTIFY_LEFT,0,0,0,0,3,2,4,3);
        mdiTitle->setTarget(this);
        mdiTitle->setSelector(ID_MDI_TITLE);
        mdiTitle->setTextColor(FXRGB(0,20,200));
        mdiSubtitle=new FXLabel(mdiBar,(char*)NULL,NULL,LAYOUT_LEFT|JUSTIFY_LEFT,0,0,0,0,0,3,4,3);
        mdiSubtitle->setTextColor(FXRGB(120,120,120));

        new FXMDIDeleteButton(mdiBar,mdiClient,FXMDIClient::ID_MDI_MENUCLOSE,LAYOUT_RIGHT|LAYOUT_CENTER_Y);
        new FXMDIRestoreButton(mdiBar,mdiClient,FXMDIClient::ID_MDI_MENURESTORE,LAYOUT_RIGHT|LAYOUT_CENTER_Y);
        new FXMDIMinimizeButton(mdiBar,mdiClient,FXMDIClient::ID_MDI_MENUMINIMIZE,LAYOUT_RIGHT|LAYOUT_CENTER_Y);

            // Window menu
            windowMenuPane=new FXMenuPane(this);
            new FXMenuTitle(menuBar,oLanguage->getText("str_mwindows"),NULL,windowMenuPane);
                new FXMenuCommand(windowMenuPane,oLanguage->getText("str_mwindows_0"),NULL,mdiClient,FXMDIClient::ID_MDI_TILEHORIZONTAL);
                new FXMenuCommand(windowMenuPane,oLanguage->getText("str_mwindows_1"),NULL,mdiClient,FXMDIClient::ID_MDI_TILEVERTICAL);
                new FXMenuCommand(windowMenuPane,oLanguage->getText("str_mwindows_2"),NULL,mdiClient,FXMDIClient::ID_MDI_CASCADE);
                new FXMenuCommand(windowMenuPane,oLanguage->getText("str_mwindows_3"),NULL,mdiClient,FXMDIClient::ID_MDI_CLOSE);
                FXMenuSeparator *_mSep=new FXMenuSeparator(windowMenuPane);
                _mSep->setTarget(mdiClient);
                _mSep->setSelector(FXMDIClient::ID_MDI_ANY);
                new FXMenuRadio(windowMenuPane,(char*)NULL,mdiClient,FXMDIClient::ID_MDI_1);
                new FXMenuRadio(windowMenuPane,(char*)NULL,mdiClient,FXMDIClient::ID_MDI_2);
                new FXMenuRadio(windowMenuPane,(char*)NULL,mdiClient,FXMDIClient::ID_MDI_3);
                new FXMenuRadio(windowMenuPane,(char*)NULL,mdiClient,FXMDIClient::ID_MDI_4);
                new FXMenuRadio(windowMenuPane,(char*)NULL,mdiClient,FXMDIClient::ID_MDI_5);
                new FXMenuRadio(windowMenuPane,(char*)NULL,mdiClient,FXMDIClient::ID_MDI_6);
                new FXMenuRadio(windowMenuPane,(char*)NULL,mdiClient,FXMDIClient::ID_MDI_7);
                new FXMenuRadio(windowMenuPane,(char*)NULL,mdiClient,FXMDIClient::ID_MDI_8);
                new FXMenuRadio(windowMenuPane,(char*)NULL,mdiClient,FXMDIClient::ID_MDI_9);
                new FXMenuRadio(windowMenuPane,(char*)NULL,mdiClient,FXMDIClient::ID_MDI_10);
                new FXMenuCommand(windowMenuPane,oLanguage->getText("str_mwindows_4"),NULL,mdiClient,FXMDIClient::ID_MDI_OVER_10);

            // Help menu
            _menuPane=new FXMenuPane(this);
            new FXMenuTitle(menuBar,oLanguage->getText("str_mhelp"),NULL,_menuPane,LAYOUT_RIGHT);
                new FXMenuCommand(_menuPane,oLanguage->getText("str_mhelp_0"),new FXGIFIcon(getApp(),data_help_contents),this,CMD_MHELP_CONTENTS);
                new FXMenuCommand(_menuPane,oLanguage->getText("str_mhelp_2"),NULL,this,CMD_MHELP_QA);
                new FXMenuCommand(_menuPane,oLanguage->getText("str_mhelp_1"),NULL,this,CMD_MHELP_ABOUT);

    windows=new FXDict();
    readings=new FXDict();
    vaccines=new FXDict();
    tech="---";
    sw=new cSplashWindow(this,new FXBMPIcon(getApp(),data_logoa),SPLASH_SIMPLE|SPLASH_OWNS_ICON|SPLASH_DESTROY,4000);
}

cWinMain::~cWinMain()
{
}

void cWinMain::create()
{
    FXMainWindow::create();

    shutterContainer->setWidth(getApp()->reg().readIntEntry("settings","instrumentToolkitW",250));
    if(getApp()->reg().readIntEntry("settings","showInstrumentToolkit",1)==0)
        shutterContainer->hide();
    mainSplitter->layout();

    shutter->setCurrent(getApp()->reg().readIntEntry("settings","instrumentToolkitIndex",1));

    if(getApp()->reg().readIntEntry("settings","winMainMax",0))
        maximize();
    else if(getApp()->reg().readIntEntry("settings","winMainMin",0))
        minimize();
    else if(getApp()->reg().existingEntry("settings","winMainX"))
    {
#ifdef WIN32
        position(getApp()->reg().readIntEntry("settings","winMainX",0),
                 getApp()->reg().readIntEntry("settings","winMainY",0),
                 getApp()->reg().readIntEntry("settings","winMainW",WINMAIN_MIN_W),
                 getApp()->reg().readIntEntry("settings","winMainH",WINMAIN_MIN_H));
        show(PLACEMENT_SCREEN);
#else
        show(PLACEMENT_SCREEN);
        position(getApp()->reg().readIntEntry("settings","winMainX",0),
                 getApp()->reg().readIntEntry("settings","winMainY",0),
                 getApp()->reg().readIntEntry("settings","winMainW",WINMAIN_MIN_W),
                 getApp()->reg().readIntEntry("settings","winMainH",WINMAIN_MIN_H));
#endif
    }
    else
    {
        show(PLACEMENT_SCREEN);
        getApp()->reg().writeIntEntry("settings","winMainX",getX());
        getApp()->reg().writeIntEntry("settings","winMainY",getY());
        getApp()->reg().writeIntEntry("settings","winMainW",WINMAIN_MIN_W);
        getApp()->reg().writeIntEntry("settings","winMainH",WINMAIN_MIN_H);
        maximize();
        getApp()->reg().writeIntEntry("settings","winMainMax",1);
        getApp()->reg().writeIntEntry("settings","winMainMin",0);
    }

    sw->create();
    sw->show(PLACEMENT_OWNER);
    sw->setFocus();
    sw->raise();

    onEvtStatusTimer(NULL,0,NULL);
    getApp()->addTimeout(this,EVT_STATUSTIMER,1000);

    helpTree->expandTree(helpTree->findItem("2 ",NULL,SEARCH_FORWARD|SEARCH_PREFIX),true);
    helpTree->selectItem(helpTree->getFirstItem(),true);
    helpTree->setCurrentItem(helpTree->getFirstItem(),true);
    onCmdHelpTree(NULL,0,NULL);
}

int cWinMain::canQuit(void)
{
    if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_quit").text()))
        return 0;
    return 1;
}

void cWinMain::saveSettings(void)
{
    getApp()->reg().writeIntEntry("settings","winMainMax",isMaximized());
    getApp()->reg().writeIntEntry("settings","winMainMin",isMinimized());
    if(!isMaximized() && !isMinimized())
    {
        getApp()->reg().writeIntEntry("settings","winMainX",getX());
        getApp()->reg().writeIntEntry("settings","winMainY",getY());
        getApp()->reg().writeIntEntry("settings","winMainW",getWidth());
        getApp()->reg().writeIntEntry("settings","winMainH",getHeight());
    }

    getApp()->reg().writeIntEntry("settings","showInstrumentToolkit",shutterContainer->shown()?1:0);
    getApp()->reg().writeIntEntry("settings","instrumentToolkitW",shutterContainer->getWidth());
    getApp()->reg().writeIntEntry("settings","instrumentToolkitIndex",shutter->getCurrent());
    getApp()->reg().write();
}

void cWinMain::inputTechnician(void)
{
    while(!sw->over)
        getApp()->runOneEvent();
    FXDialogBox dlg(this,oLanguage->getText("str_dlg_tech"),DECOR_TITLE|DECOR_BORDER);
    FXVerticalFrame *_vframe0=new FXVerticalFrame(&dlg,LAYOUT_FILL);
    FXVerticalFrame *_vframe1=new FXVerticalFrame(_vframe0,FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
    FXComboBox *retName=new FXComboBox(_vframe1,20,NULL,0,COMBOBOX_INSERT_FIRST);
    FXString techs=getApp()->reg().readStringEntry("recents","technicians","");
    FXString techsv;
    while(!techs.empty())
    {
        techsv=techs.before('\t');
        if(!techsv.empty())
            retName->appendItem(techsv);
        techs=techs.after('\t');
    }
    int nw=0,exit=retName->getNumItems();
    retName->setNumVisible(exit>20?20:exit);
    new FXHorizontalSeparator(_vframe0,SEPARATOR_GROOVE|LAYOUT_FILL_X);
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(_vframe0,JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y|LAYOUT_CENTER_X);
    new FXButton(_hframe0,oLanguage->getText("str_but_accept"),NULL,&dlg,FXDialogBox::ID_ACCEPT,BUTTON_DEFAULT|BUTTON_INITIAL|FRAME_RAISED|FRAME_THICK|LAYOUT_CENTER_X);
    new FXButton(_hframe0,oLanguage->getText("str_but_cancel"),NULL,&dlg,FXDialogBox::ID_CANCEL,FRAME_RAISED|FRAME_THICK|LAYOUT_CENTER_X);
    exit=false;
    int r;
    if(tech.empty() || tech=="---")
    {
        r=getApp()->reg().readIntEntry("settings","tech",0);
        if(r>0 && r<retName->getNumItems())
            retName->setCurrentItem(r);
        nw=1;
    }
    retName->setSortFunc(FXList::ascendingCase);
    retName->sortItems();
    while(!exit)
    {
        exit=true;
        retName->setFocus();
        if(!dlg.execute(PLACEMENT_OWNER))
        {
            if(!tech.empty() && tech!="---")
                return;
            else
            {
                quitProgram();
                close();
            }
        }
        if(retName->getText().empty() || retName->getText()=="---")
        {
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_dlg_tech_incomplete").text());
            exit=false;
        }
    }
    if((nw==1) && !oDataLink->isOpened())
        cMDIDatabaseManager::load(mdiClient,mdiMenu);
    tech=retName->getText();
    techs="";
    retName->sortItems();
    for(int i=0;i<retName->getNumItems();i++)
        if(i>0)
        {
            if(retName->getItem(i-1)!=retName->getItem(i))
                techs=techs+retName->getItem(i)+'\t';
        }
        else
            techs=techs+retName->getItem(i)+'\t';
    statusTech->setText(tech);
    getApp()->reg().writeIntEntry("settings","tech",retName->findItem(retName->getText()));
    getApp()->reg().writeStringEntry("recents","technicians",techs.text());
    getApp()->reg().write();
}

FXString cWinMain::getTechnician(void)
{
    return tech;
}

cMDIChild *cWinMain::getWindow(const FXString &prTitle)
{
    return (cMDIChild *)windows->find(prTitle.text());
}

FXbool cWinMain::raiseWindow(const FXString &prTitle)
{
    cMDIChild *win=(cMDIChild *)windows->find(prTitle.text());
    if(win==NULL)
        return false;
    mdiClient->setActiveChild(win,true);
    return true;
}

void cWinMain::addWindow(const FXString &prTitle,cMDIChild *prWindow)
{
    FXString *res=new FXString(prTitle);
    windows->insert(res->text(),(void*)prWindow);
}

void cWinMain::removeWindow(const FXString &prTitle)
{
   windows->remove(prTitle.text());
}

int cWinMain::countWindows()
{
    return windows->no();
}

FXbool cWinMain::isWindow(const FXString &prTitle)
{
    return (windows->find(prTitle.text())!=NULL);
}

FXbool cWinMain::closeAllWindows(FXObject *prSender)
{
    return mdiClient->forallWindows(prSender,FXSEL(SEL_CLOSE,cWinMain::ID_MDI_CHILD),NULL);
}

long cWinMain::onRszWindow(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXint w=getWidth(),h=getHeight();
    if((w<WINMAIN_MIN_W || h<WINMAIN_MIN_H) && !(options&MDI_MINIMIZED) && !(options&MDI_MAXIMIZED))
        resize(w<WINMAIN_MIN_W?WINMAIN_MIN_W:w,h<WINMAIN_MIN_H?WINMAIN_MIN_H:h);
    return 1;
}

long cWinMain::onEvtStatusTimer(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXchar ftime[50];
    time_t curtime;
    struct tm *loctime;

    curtime=time(NULL);
    loctime=localtime(&curtime);
    strftime(ftime,100,"%Y-%m-%d, %H:%M:%S",loctime);
    statusTimer->setText(ftime);
    getApp()->addTimeout(this,EVT_STATUSTIMER,1000);
    return 1;
}

long cWinMain::onCmdQuitProgram(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!APP_ITERATE)
        return 1;
    if(!mainCanQuit())
        return 1;
    if(!closeAllWindows(NULL))
        return 1;
    mainSaveSettings();
    quitProgram();
    return 1;
}

long cWinMain::onCmdToolToggle(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(shutterContainer->shown())
        shutterContainer->hide();
    else
        shutterContainer->show();
    toolToggleButton->setState(shutterContainer->shown());
    toolToggleButton->killFocus();
    mainSplitter->layout();
    return 1;
}

long cWinMain::onCmdShutterSelect(FXObject *prSender,FXSelector prSelector,void *prData)
{
    shutter->killFocus();
    return 1;
}

long cWinMain::onUpdMdiTitle(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXString name=oDataLink->getName();
    if(!name.empty())
    {
        FXString s=oDataLink->getSpecies();
        mdiTitle->setText("["+s.mid(0,1).upper()+s.mid(1,s.length()-1).lower()+"] "+name);
        mdiTitle->setTextColor(FXRGB(0,40,200));
    }
    else
    {
        mdiTitle->setText(oLanguage->getText("str_nodb"));
        mdiTitle->setTextColor(FXRGB(120,120,120));
    }

    FXMDIChild *_ac=mdiClient->getActiveChild();
    if(_ac==NULL || !_ac->shown())
        mdiSubtitle->setText((char*)NULL);
    else
        mdiSubtitle->setText(": "+_ac->getTitle());
    return 1;
}


long cWinMain::onCmdMFileStartRead(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(this,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(mdiClient,mdiMenu);
        return 1;
    }
    cDLGChooseAT dlg(this);
    if(!dlg.execute(PLACEMENT_OWNER))
        return 1;
    cReadingsManager::makeReading(mdiClient,mdiMenu,dlg.getAssay(),dlg.getTemplate(),false,dlg.getLot(),dlg.getExpDate());
    return 1;
}

long cWinMain::onCmdMFileStartManual(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(this,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(mdiClient,mdiMenu);
        return 1;
    }
    cDLGChooseAT dlg(this);
    if(!dlg.execute(PLACEMENT_OWNER))
        return 1;
    cReadingsManager::makeReading(mdiClient,mdiMenu,dlg.getAssay(),dlg.getTemplate(),true,dlg.getLot(),dlg.getExpDate());
    return 1;
}

long cWinMain::onCmdMFileBrowseCases(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(this,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(mdiClient,mdiMenu);
        return 1;
    }
    cMDICaseManager::load(mdiClient,mdiMenu);
    return 1;
}

long cWinMain::onCmdMFileTemplateNew(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cTemplateManager::newTemplate(mdiClient,mdiMenu);
    return 1;
}

long cWinMain::onCmdMFileTemplateManager(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(this,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(mdiClient,mdiMenu);
        return 1;
    }
    cMDITemplateManager::load(mdiClient,mdiMenu);
    return 1;
}

long cWinMain::onCmdMFileDatabaseProperties(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!oDataLink->isOpened())
        return 1;
    cDatabaseManager::editDatabase(oDataLink->getFilename());
    cMDIDatabaseManager::cmdRefresh();
    return 1;
}

long cWinMain::onCmdMDatabaseNew(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDIDatabaseManager::load(mdiClient,mdiMenu);
    cMDIDatabaseManager::cmdNew();
    cMDIDatabaseManager::cmdRefresh();
    return 1;
}

long cWinMain::onCmdMDatabaseManager(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDIDatabaseManager::load(mdiClient,mdiMenu);
    return 1;
}

long cWinMain::onCmdMInputVeterinarianManager(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDIVeterinarianManager::load(mdiClient,mdiMenu);
    return 1;
}

long cWinMain::onCmdMInputOwnerManager(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDIOwnerManager::load(mdiClient,mdiMenu);
    return 1;
}

long cWinMain::onCmdMInputGrowerManager(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDIGrowerManager::load(mdiClient,mdiMenu);
    return 1;
}

long cWinMain::onCmdMInputVaccineManager(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDIVaccineManager::load(mdiClient,mdiMenu);
    return 1;
}

long cWinMain::onCmdMInputAssayManager(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDIAssayManager::load(mdiClient,mdiMenu);
    return 1;
}

long cWinMain::onCmdMInputBreedManager(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDIBreedManager::load(mdiClient,mdiMenu);
    return 1;
}

long cWinMain::onCmdMSettingsGeneral(FXObject *prSender,FXSelector prSelector,void *prData)
{

    return 1;
}

long cWinMain::onCmdMSettingsReader(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDIReaderManager::load(mdiClient,mdiMenu);
    return 1;
}

long cWinMain::onCmdMSettingsPrinter(FXObject *prSender,FXSelector prSelector,void *prData)
{

    return 1;
}

long cWinMain::onCmdMSettingsTech(FXObject *prSender,FXSelector prSelector,void *prData)
{
    inputTechnician();
    return 1;
}

long cWinMain::onCmdMReportsCA(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(this,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(mdiClient,mdiMenu);
        return 1;
    }
    cMDICaseManager::load(mdiClient,mdiMenu,cCaseAnalysisReport::openCases,oLanguage->getText("str_report_ca"));
    return 1;
}

long cWinMain::onCmdMReportsCC(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(this,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(mdiClient,mdiMenu);
        return 1;
    }
    cMDICaseManager::load(mdiClient,mdiMenu,cCaseCompareReport::openCases,oLanguage->getText("str_report_cc"));
    return 1;
}

long cWinMain::onCmdMReportsCM(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(this,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(mdiClient,mdiMenu);
        return 1;
    }
    cMDICaseManager::load(mdiClient,mdiMenu,cCaseCombineReport::openCases,oLanguage->getText("str_report_cm"));
    return 1;
}

long cWinMain::onCmdMReportsCS(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(this,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(mdiClient,mdiMenu);
        return 1;
    }
    cMDICaseManager::load(mdiClient,mdiMenu,cCaseSbsReport::openCases,oLanguage->getText("str_report_cs"));
    return 1;
}

long cWinMain::onCmdMReportsVH(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(this,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(mdiClient,mdiMenu);
        return 1;
    }
    cMDICaseManager::load(mdiClient,mdiMenu,cVaccineHistoryReport::openCases,oLanguage->getText("str_report_vh"));
    return 1;
}

long cWinMain::onCmdMReportsVD(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(this,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(mdiClient,mdiMenu);
        return 1;
    }
    cMDICaseManager::load(mdiClient,mdiMenu,cVaccineDateReport::openCases,oLanguage->getText("str_report_vd"));
    return 1;
}

long cWinMain::onCmdMReportsTC(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(this,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(mdiClient,mdiMenu);
        return 1;
    }
    cMDICaseManager::load(mdiClient,mdiMenu,cTotalCountsReport::openCases,oLanguage->getText("str_report_tc"));
    return 1;
}

long cWinMain::onCmdMReportsTB(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(this,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(mdiClient,mdiMenu);
        return 1;
    }
    cMDICaseManager::load(mdiClient,mdiMenu,cTiterBreakdownReport::openCases,oLanguage->getText("str_report_tb"));
    return 1;
}

long cWinMain::onCmdMDatabaseExport(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(this,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(mdiClient,mdiMenu);
        return 1;
    }
    cMDICaseManager::load(mdiClient,mdiMenu,cCaseExport::openCases,oLanguage->getText("str_export_title"));
    return 1;
}

long cWinMain::onCmdMDatabaseImport(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(this,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(mdiClient,mdiMenu);
        return 1;
    }
    cCaseImport::doImport();
    return 1;
}

long cWinMain::onCmdMHelpContents(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!shutterContainer->shown())
        onCmdToolToggle(NULL,0,NULL);
    shutter->setCurrent(1);
    return 1;
}

long cWinMain::onSplashHide(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(sw->over)
        return 1;
    sw->onHide(NULL,0,NULL);
    return 1;
}

long cWinMain::onCmdMHelpQA(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!shutterContainer->shown())
        onCmdToolToggle(NULL,0,NULL);
    shutter->setCurrent(0);
    return 1;
}

long cWinMain::onCmdMHelpAbout(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXDialogBox dlg(this,oLanguage->getText("str_about_title"),DECOR_TITLE|DECOR_BORDER);
    FXVerticalFrame *_vframe0=new FXVerticalFrame(&dlg,LAYOUT_FILL);
    FXLabel *_l0;

    FXHorizontalFrame *_hframe8=new FXHorizontalFrame(_vframe0,0,0,0,0,0,0,0,0,0,0,0);
    FXFont *ft1=new FXFont(oApplicationManager,"Times New Roman",26,FXFont::Normal);
    FXFont *ft2=new FXFont(oApplicationManager,"Times New Roman",18,FXFont::Normal);
    _l0=new cLabel(_hframe8,"U",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,0,0,0,0);
        _l0->setFont(ft1);
        _l0->setTextColor(FXRGB(160,0,0));
    _l0=new cLabel(_hframe8,"n",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,2,3,0,2);
        _l0->setFont(ft2);
        _l0->setTextColor(FXRGB(160,0,0));
    _l0=new cLabel(_hframe8,"i",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,0,0,0,2);
        _l0->setFont(ft2);
        _l0->setTextColor(FXRGB(160,0,0));
    _l0=new cLabel(_hframe8,"V",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,0,0,0,0);
        _l0->setFont(ft1);
        _l0->setTextColor(FXRGB(160,0,0));
    _l0=new cLabel(_hframe8,"e",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,0,3,0,2);
        _l0->setFont(ft2);
        _l0->setTextColor(FXRGB(160,0,0));
    _l0=new cLabel(_hframe8,"t",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,0,0,0,2);
        _l0->setFont(ft2);
        _l0->setTextColor(FXRGB(160,0,0));

    new FXLabel(_vframe0,FXStringFormat("  version %s",APP_VERSION));
    new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_WIDTH,0,450);
    new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_WIDTH|SEPARATOR_GROOVE,0,0,350);
    new FXLabel(_vframe0,"Copyright Pasteur Animal Health.");
    new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_WIDTH|SEPARATOR_GROOVE,0,0,350);
    new FXLabel(_vframe0,"Terms and Conditions");
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X|FRAME_SUNKEN|LAYOUT_FIX_HEIGHT,0,0,0,160,0,0,0,0);
    FXText *tx=new FXText(_hframe0,NULL,0,TEXT_WORDWRAP|TEXT_READONLY|LAYOUT_FILL);
    tx->setScrollStyle(HSCROLLER_NEVER|HSCROLLING_OFF|VSCROLLER_ALWAYS|VSCROLLING_ON);
    tx->setBackColor(FXRGB(242,240,240));
    tx->setText(APP_LEGAL);

    _l0=new FXLabel(_vframe0,FXStringFormat("Developed by Palos && Sons.\nCheck out the http://www.palos.ro website!\nEmail us at office@palos.ro!"),NULL,LAYOUT_CENTER_X);
    _l0->setTextColor(FXRGB(180,80,80));
    _l0->setFont(new FXFont(getApp(),"helvetica,80,bold"));
    new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_WIDTH|SEPARATOR_GROOVE,0,0,350);
    new FXButton(_vframe0,oLanguage->getText("str_but_ok"),NULL,&dlg,FXDialogBox::ID_ACCEPT,BUTTON_DEFAULT|FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH,0,0,80);
    dlg.execute(PLACEMENT_OWNER);
    return 1;
}

long cWinMain::onCmdHelpTree(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXTreeItem *ti=helpTree->getCurrentItem();
    helpTree->expandTree(ti,true);
    if(!ti)
        return 1;
    FXString *data=(FXString*)ti->getData();
    if(data)
    {
        helpText->setText(data->substitute("\\n","\n"));
    }
    return 1;
}

long cWinMain::onCmdHelpLink(FXObject *prSender,FXSelector prSelector,void *prData)
{

    // To Do?

    return 1;
}

long cWinMain::onCmdMSettingsLang(FXObject *prSender,FXSelector prSelector,void *prData)
{

    oLanguage->loadDesignated(true);
    FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_langchange").text());

    return 1;
}

long cWinMain::onCmdMInputBaselineManager(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(this,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(mdiClient,mdiMenu);
        return 1;
    }
    cMDIBaselineManager::load(mdiClient,mdiMenu);
    return 1;
}
