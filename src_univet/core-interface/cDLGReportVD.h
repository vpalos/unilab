#ifndef CDLGREPORTVD_H
#define CDLGREPORTVD_H

#include <fx.h>

class cDLGReportVD : public FXDialogBox
{
    FXDECLARE(cDLGReportVD);
    private:
        FXButton *butAccept;
        FXButton *butCancel;
        
    protected:
        FXLabel *lp1,*lp2;
        FXDataTarget *ftype;
        FXRadioButton *bLog2;
        FXRadioButton *bDeventer;

        FXDataTarget agtype;
        FXRadioButton *bAMean;
        FXRadioButton *bGMean;

        FXDataTarget stype;
        FXRadioButton *bAssay;
        FXRadioButton *bCase;

        cDLGReportVD(){}
        
    public:
        FXuint fchoice;
        FXuint agchoice;
        FXuint schoice;
        
        FXTextField *tfTT,*tfHL,*tfP1,*tfP2;
        FXSpinner *spSD;
        
        cDLGReportVD(FXWindow *prOwner);
        virtual ~cDLGReportVD();
        
        enum
        {
            ID_THIS=FXDialogBox::ID_LAST,
            ID_UPDATE,
            ID_FIELD,
            
            CMD_BUT_APPLY,
            ID_LAST
        };

        long onUpdField(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdUpdate(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdApply(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
