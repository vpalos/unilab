#ifndef CMDIREADINGS_H
#define CMDIREADINGS_H

#include <fx.h>
#include "ee.h"
#include "cDataResult.h"
#include "cMDIChild.h"
#include "cColorTable.h"
#include "cSortList.h"
#include "cMDITemplate.h"
#include "cVChart.h"

typedef struct sCDLGO
{
    FXString *id;
    FXString *id2;
    FXString *pop;
    FXString *pop2;
    FXString *age;
    FXString *asy;
    FXString *blDate;
    FXString *br1;
    FXString *br2;
    cDataResult *res;
};

typedef struct sCaseEntry
{
    FXString *id;
    FXString *reading_oid;
    FXint count;
    FXint replicates;
    FXString *age;
    FXint factor;
    FXint startCell;
    FXint startPlate;
    FXint alelisa;
    FXint plateSpanCount;
    double *data;

    double *controlsData;
    FXint controlsCount;

    FXString *tpl;
    FXString *veterinarian;
    FXString *controls_defs;
    FXString *readingDate;
    FXString *technician;
    FXString *reason;
    FXString *lot;
    FXString *expirationDate;
    FXString *spType;
    FXString *bleedDate;
    FXString *comments;
    FXString *population;
    FXString *assay;
    FXString *breed1,*breed2;

    int parentSet;
};

typedef struct sCasesSet
{
    sCaseEntry *cases;
    int casesCount;
    int platesCount;
    int sampleCount;

    cDataResult *assayRes;
    cDataResult *templateRes;

    bool alelisa;
    int orientation;
    bool useSecond;

    FXString *cala;
    FXString *calb;
    FXString *calaop,*calbop;
    FXString *calapco,*calbpco,*calasco,*calbsco;

    int factors[4];
    bool useTiters;
    int titerGroups[30];
    double slope,intercept;
    FXString *calt;
    double ratioGroups[30];
    FXString *assay;
    FXString *assaytitle;

    int controlsLayout[50];
    int controlsPerPlate;
};

class cLocInfo
{
    private:
    protected:
    public:
        int caseIndex;
        int sampleLoc;
        int plate;
        bool isValue;
        double value;
        int set;

        cLocInfo(int prCaseIndex=-1,int prSampleLoc=-1,int prPlate=-1,int prSet=-1);
        ~cLocInfo();

        void setValue(double prValue);
};

class cMDIReadings : public cMDIChild
{
    FXDECLARE(cMDIReadings);

    private:
    protected:
        FXbool saved,newdata;

        FXTabBook *tbMain;
        FXTabItem *tiPrimary;
        FXTabItem *tiStats;
        FXTabItem *tiGraph;
        FXButton *lbInv;
        FXButton *butSelall,*butDesel,*bchPoint,*bchLine,*bchBar;

        cColorTable *statsTable;
        cColorTable *plateTable1;
        cColorTable *caseList1;
        FXGroupBox *_gb0;
        FXLabel *lbPlates;
        FXLabel *lbStats;
        FXLabel *assayId;
        FXLabel *assayTpl;
        FXLabel *assayTech;

        FXMenuPane *edit;
        FXMenuButton *mbEdit;

        cColorTable *plateTable2;
        cColorTable *caseList2;
        FXGroupBox *_gb10;
        FXLabel *lbPlates2;
        FXCheckButton *showStats;

        FXMenuPane *vars;
        FXMenuCheck *mcCala;
        FXMenuCheck *mcCalb;
        FXMenuCheck *mcTiter;
        FXMenuCheck *mcTiters;
        FXMenuCheck *mcBins;
        FXMenuCheck *mcResult;
        FXMenuCheck *mcResult2;
        //FXMenuCheck *mcBins2;
        FXMenuCheck *mcAm;
        FXMenuCheck *mcAge;
        FXMenuCheck *mcLog2;
        FXMenuCheck *mcEU;
        FXMenuCheck *mcBreed1;
        FXMenuCheck *mcBreed2;
        FXMenuButton *mbCalc;

        FXMenuPane *reports;
        FXMenuButton *mbReports;

        FXVerticalFrame *statsFrame;
        FXString invalidPlatesDetails;
        FXScrollWindow *chartWin;
        FXVerticalFrame *chartFrame;
        cVChart **charts;

        sCasesSet *casesSets;
        int readingsWindow;
        int selectedCase;
        int tSetsCount;
        int tCasesCount;
        int tPlatesCount;
        int tSamplesCount;

        bool areTiters;
        bool areSecondary;
        bool mcAmCheck;

        cMDIReadings();

        void drawPlateTables(void);
        void locateCaseLists(int prIndex);
        void locatePlateTable1(int prIndex);
        void locatePlateTable2(int prIndex);
        void selectCase(int prIndex,int prPlate=-1);

        void updateData(void);
        FXbool saveData(void);

    public:
        static void sortControls(int *prData,int prLength,int prDesc=false);

        static FXString sampleKey(int prIndex);
        static FXString sampleVal(double prValue);
        static FXString calculVal(double prValue);
        static FXString displayPosition(int prPosition,bool prAlelisa,int prOrientation,bool prAlelisaValue=false);
        static int platePosition(int prPosition,bool prAlelisa,int prOrientation,bool prAlelisaValue=false);
        static bool isControlPosition(int prPosition,int *prControls,int prControlsCount,bool prAlelisa);
        static void appendControls(double *prDest,double *prSrc,int *prControls,int prControlsCount,int prOrientation,bool prAlelisa);

        cMDIReadings(FXMDIClient *prP,const FXString &prName,FXIcon *prIc=NULL,FXPopup *prPup=NULL,FXuint prOpts=0,FXint prX=0,FXint prY=0,FXint prW=0,FXint prH=0);
        virtual ~cMDIReadings();

        virtual void create();

        virtual FXbool canClose(void);
        virtual FXbool newReading(cDataResult *prResAssay,cDataResult *prResTemplate,int prPlateCount,double **prPlateData,FXString prLot="",FXString prExpirationDate="");
        virtual FXbool loadReadings(cSortList &prCases);

        enum
        {
            ID_MDIREADINGS=FXMDIChild::ID_LAST,
            ID_TBMAIN,

            ID_CALCMENU,

            ID_COPY,
            ID_PASTE,
            ID_SELALL,

            ID_PLATETABLE1,
            ID_CASELIST1,
            ID_PLATETABLE2,
            ID_CASELIST2,
            ID_STATSTABLE,
            ID_SHOWSTATS,
            ID_SHOWRULES,

            ID_REP_CA,
            ID_REP_CC,
            ID_REP_CM,
            ID_REP_SB,
            ID_REP_TC,
            ID_REP_TB,
            ID_REP_VH,
            ID_REP_VD,

            CMD_CHPOINT,
            CMD_CHLINE,
            CMD_CHBAR,

            CMD_TEMPLATE,
            CMD_REREAD,
            CMD_CASEEDIT,
            CMD_CLOSE,
            CMD_SAVE,

            ID_LAST
        };

        long onCmdShowStats(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCopyPlateTable(FXObject *prSender,FXSelector prSelector,void *prData);
        long onPastePlateTable(FXObject *prSender,FXSelector prSelector,void *prData);
        long onSelallPlateTable(FXObject *prSender,FXSelector prSelector,void *prData);

        long onRszPlateTable1(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdSelPlateTable1(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdPlateTable1(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdSelPlateTable2(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdPlateTable2(FXObject *prSender,FXSelector prSelector,void *prData);

        long onRszCaseList1(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSelCaseList1(FXObject *prSender,FXSelector prSelector,void *prData);
        long onRszCaseList2(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSelCaseList2(FXObject *prSender,FXSelector prSelector,void *prData);

        long onCmdCalcSel(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData);

        long onCmdShowRules(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdReread(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdTemplate(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdCaseEdit(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdReport(FXObject *prSender,FXSelector prSelector,void *prData);

        long onCmdChPoint(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdChLine(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdChBar(FXObject *prSender,FXSelector prSelector,void *prData);

        long onEvtRedir(FXObject *prSender,FXSelector prSelector,void *prData);
        long onRefresh(FXObject *prSender,FXSelector prSelector,void *prData);
        long onRefreshAsy(FXObject *prSender,FXSelector prSelector,void *prData);
        long onRefreshTpl(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif

