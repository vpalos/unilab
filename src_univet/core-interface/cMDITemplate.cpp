#include <time.h>
#include <ctype.h>
#include <stdlib.h>

#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cDateChooser.h"
#include "cColorsManager.h"
#include "cSpeciesManager.h"
#include "cTemplateManager.h"
#include "cCaseManager.h"
#include "cPopulationManager.h"
#include "cReadersManager.h"
#include "cReadingsManager.h"
#include "cWinMain.h"
#include "cDLGChooseAT.h"
#include "cMDITemplate.h"
#include "cMDITemplateManager.h"

FXDEFMAP(cMDITemplate) mapMDITemplate[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDITemplate::ID_MDITEMPLATE,cMDITemplate::onCmdClose),
    FXMAPFUNC(SEL_CONFIGURE,cMDITemplate::ID_PLATETABLE,cMDITemplate::onRszPlateTable),
    FXMAPFUNC(SEL_SELECTED,cMDITemplate::ID_PLATETABLE,cMDITemplate::onUpdSelPlateTable),
    FXMAPFUNC(SEL_UPDATE,cMDITemplate::ID_PLATETABLE,cMDITemplate::onUpdPlateTable),
    FXMAPFUNC(SEL_CLICKED,cMDITemplate::ID_PLATETABLE,cMDITemplate::onDbcPlateTable),
    FXMAPFUNC(SEL_CONFIGURE,cMDITemplate::ID_CASELIST,cMDITemplate::onRszCaseList),
    FXMAPFUNC(SEL_UPDATE,cMDITemplate::ID_CASELIST,cMDITemplate::onUpdCaseList),
    FXMAPFUNC(SEL_SELECTED,cMDITemplate::ID_CASELIST,cMDITemplate::onUpdSelCaseList),
    FXMAPFUNC(SEL_REPLACED,cMDITemplate::ID_CASELIST,cMDITemplate::onCmdCaseList),
    FXMAPFUNC(SEL_RIGHTBUTTONPRESS,cMDITemplate::ID_CASELIST,cMDITemplate::onCmdPpDate),
    //FXMAPFUNC(SEL_CLICKED,cMDITemplate::ID_CASELIST,cMDITemplate::onCmdPpDate),
    FXMAPFUNC(SEL_CHANGED,cMDITemplate::ID_TPLNAME,cMDITemplate::onChgTfName),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::ID_TPLNAME,cMDITemplate::onCmdTfName),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::ID_ORIENTATIONTGT,cMDITemplate::onCmdUpdateOrientation),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::ID_CONTROLSTGT,cMDITemplate::onCmdUpdateControls),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::ID_ALELISATGT,cMDITemplate::onCmdUpdateAlelisa),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::ID_SUBCTGT,cMDITemplate::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_CLOSE,cMDITemplate::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_SAVE,cMDITemplate::onCmdSave),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_READ,cMDITemplate::onCmdRead),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_FILL,cMDITemplate::onCmdFill),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_PRINT,cMDITemplate::onCmdPrint),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_CASENEW,cMDITemplate::onCmdCaseNew),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_CASEDEL,cMDITemplate::onCmdCaseDel),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_CASEUP,cMDITemplate::onCmdCaseUp),
    FXMAPFUNC(SEL_COMMAND,cMDITemplate::CMD_CASEDN,cMDITemplate::onCmdCaseDown),
};

FXIMPLEMENT(cMDITemplate,cMDIChild,mapMDITemplate,ARRAYNUMBER(mapMDITemplate))

cMDITemplate::cMDITemplate()
{
}

FXString cMDITemplate::sampleKey(int prIndex)
{
    FXString ret="";
    int diff='z'-'a'+1;
    while(prIndex>=0)
    {
        ret=FXStringFormat("%c",'a'+prIndex%diff)+ret;
        prIndex/=diff;
        prIndex--;
    }
    return ret;
}

int cMDITemplate::platePositionRow(int prIndex)
{
    return ((orientationChoice==1)?prIndex%8:prIndex/12);
}

int cMDITemplate::platePositionColumn(int prIndex)
{
    return ((orientationChoice==1)?prIndex/8:prIndex%12);
}

int cMDITemplate::platePosition(int prRow,int prColumn)
{
    return ((prColumn==-1)?prRow:(orientationChoice==1?prColumn*8+(prRow%8):(prRow%8)*12+prColumn));
}

bool cMDITemplate::isControl(int prRow,int prColumn)
{
    int i,pos=platePosition(prRow,prColumn);
    if(pos==0)
        pos=1000;
    for(i=0;i<TEMPLATE_MAX_CONTROLS;i++)
        if(abs(controls[i])==pos)
            return true;
    return false;
}

void cMDITemplate::placePositive(int prRow,int prColumn)
{
    if(controlsCount==TEMPLATE_MAX_CONTROLS)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_maxtplcontrols").text());
        return;
    }
    int i,pos=platePosition(prRow,prColumn);
    clearControl(pos);
    for(i=0;i<TEMPLATE_MAX_CONTROLS;i++)
        if(controls[i]==-100)
        {
            controls[i]=(pos==0?1000:pos);
            controlsCount++;
            break;
        }
}

void cMDITemplate::placeNegative(int prRow,int prColumn)
{
    if(controlsCount==TEMPLATE_MAX_CONTROLS)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_maxtplcontrols").text());
        return;
    }
    int i,pos=platePosition(prRow,prColumn);
    clearControl(pos);
    for(i=0;i<TEMPLATE_MAX_CONTROLS;i++)
        if(controls[i]==-100)
        {
            controls[i]=(pos==0?-1000:-pos);
            controlsCount++;
            break;
        }
}

void cMDITemplate::clearControl(int prRow,int prColumn)
{
    int i,pos=platePosition(prRow,prColumn);
    if(pos==0)
        pos=1000;
    for(i=0;i<TEMPLATE_MAX_CONTROLS;i++)
        if(abs(controls[i])==pos)
        {
            controls[i]=-100;
            controlsCount--;
            break;
        }
}

void cMDITemplate::clearAllControls(void)
{
    for(int i=0;i<TEMPLATE_MAX_CONTROLS;i++)
        controls[i]=-100;
    controlsCount=0;
}

void cMDITemplate::drawCaseList(void)
{
    for(int j=0;j<caseList->getNumRows();j++)
    {
        caseList->setCellColor(j,0,cColorsManager::getColor(j,-30));
        for(int i=1;i<caseList->getNumColumns();i++)
        {
            caseList->setItemBorders(j,i,0);
            caseList->setCellColor(j,i,cColorsManager::getColor(j));
        }
    }
}

void cMDITemplate::updateData(void)
{
    saved=false;
    switch(orientationChoice)
    {
        case 1:
            plateTable->showHorzGrid(false);
            plateTable->showVertGrid(true);
            break;
        case 2:
            plateTable->showHorzGrid(true);
            plateTable->showVertGrid(false);
            break;
    }

    if(controlsChoice==3)
    {
        butPos->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        butNeg->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        butNone->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    else
    {
        butPos->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        butNeg->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        butNone->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }

    int r,c,v,i,j;
    int counter,stock=0,plates;
    for(i=0;i<caseList->getNumRows();i++)
        stock+=FXIntVal(caseList->getItemText(i,2))*(FXIntVal(caseList->getItemText(i,6))+1);
    counter=controlsCount;
    if(orientationChoice==1 && alelisaChoice==2)
    {
        counter*=2;
        stock*=2;
    }
    plates=stock/(96-counter)+(stock%(96-counter)>0?1:0);
    if(plates==0)
        plates=1;
    lbPlates->setText(FXStringFormat(" - %d %s [%d %s]",plates,oLanguage->getText("str_tplmdi_plates").text(),(96-counter)-((stock-1)%(96-counter)+1),oLanguage->getText("str_tplmdi_cellsrem").text()));
    plateTable->removeRows(0,plateTable->getNumRows());
    plateTable->insertRows(0,plates*8);
    for(i=0;i<plateTable->getNumRows();i++)
        for(j=0;j<12;j++)
            plateTable->setItemJustify(i,j,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
    for(i=1;i<plateTable->getNumRows();i++)
        for(j=0;j<12;j++)
        {
            plateTable->setItemText(i,j,(char*)NULL);
            plateTable->setCellColor(i,j,FXRGB(255,255,255));
            plateTable->setItemStipple(i,j,STIPPLE_NONE);
            plateTable->setItemBorders(i,j,i%8==0?FXTableItem::TBORDER:0);
        }
    int row=plateTable->getNumRows()-1;
    for(j=0;j<12;j++)
        plateTable->setItemBorders(row,j,FXTableItem::BBORDER);
    for(j=0;j<plateTable->getNumRows();j++)
    {
        plateTable->getRowHeader()->setItemJustify(j,JUSTIFY_CENTER_X);
        plateTable->getRowHeader()->setItemText(j,FXStringFormat("P%d:%c",j/8+1,j%8+'A'));
    }
    for(i=0;i<plateTable->getNumRows();i++)
        plateTable->getRowHeader()->setItemPressed(i,false);
    for(i=0;i<12;i++)
        plateTable->getColumnHeader()->setItemPressed(i,false);
    FXbool alel=false;
    if(orientationChoice==1 && alelisaChoice==2)
    {
        alel=true;
        for(i=0;i<plateTable->getNumRows();i++)
            for(j=1;j<12;j+=2)
                plateTable->setItemStipple(i,j,STIPPLE_GRAY);
    }
    int m;
    for(i=0;i<TEMPLATE_MAX_CONTROLS;i++)
        if(controls[i]!=-100)
        {
            v=abs(controls[i])%1000;
            r=platePositionRow(v);
            c=platePositionColumn(v);
            if(alel && (c%2!=0))
                continue;
            for(j=0;j<plates;j++)
            {
                if(controls[i]>0)
                {
                    plateTable->setCellColor(r+8*j,c,FXRGB(255,90,90));
                    plateTable->setItemText(r+8*j,c,oLanguage->getText("str_pos"));
                }
                else
                {
                    plateTable->setCellColor(r+8*j,c,FXRGB(200,200,200));
                    plateTable->setItemText(r+8*j,c,oLanguage->getText("str_neg"));
                }
                m=r+8*j;
                plateTable->setItemBorders(m,c,FXTableItem::LBORDER|(((c<11 && isControl(m,c+1)) || alel)?0:FXTableItem::RBORDER)|FXTableItem::TBORDER|((m%8!=7 && !isControl(m+1,c)) || m==row?FXTableItem::BBORDER:0));
                if(alel)
                {
                    if(controls[i]>0)
                    {
                        plateTable->setCellColor(r+8*j,c+1,FXRGB(255,90,90));
                        plateTable->setItemText(r+8*j,c+1,oLanguage->getText("str_pos"));
                    }
                    else
                    {
                        plateTable->setCellColor(r+8*j,c+1,FXRGB(200,200,200));
                        plateTable->setItemText(r+8*j,c+1,oLanguage->getText("str_neg"));
                    }
                    m=r+8*j;
                    c++;
                    plateTable->setItemBorders(m,c,FXTableItem::LBORDER|((c<11 && isControl(m,c+1))?0:FXTableItem::RBORDER)|FXTableItem::TBORDER|((m%8!=7 && !isControl(m+1,c-1)) || m==row?FXTableItem::BBORDER:0));
                    c--;
                }
            }
        }
    counter=0;
    int p;
    char replicates[10];
    for(i=0;i<caseList->getNumRows();i++)
    {
        v=0;
        if(caseList->getItemText(i,1).empty())
            continue;
        p=0;
        stock=FXIntVal(caseList->getItemText(i,2));
        while(stock>0)
        {
            replicates[0]=0;
            for(j=0;j<FXIntVal(caseList->getItemText(i,6))+1;j++)
            {
                r=platePositionRow(counter%96);
                c=platePositionColumn(counter%96);
                while(!plateTable->getItemText(r+8*(counter/96),c).empty())
                {
                    counter++;
                    r=platePositionRow(counter%96);
                    c=platePositionColumn(counter%96);
                }
                if(j==0 && v==0)
                    caseList->setItemText(i,0,FXStringFormat("(%s) P%d:%c%d",sampleKey(i).text(),(counter/96)+1,'A'+r,c+1));
                plateTable->setCellColor(r+8*(counter/96),c,cColorsManager::getColor(i,-j*10));
                plateTable->setItemText(r+8*(counter/96),c,FXStringFormat("%s%d%s",sampleKey(i).text(),p+1,replicates));
                if(alel)
                {
                    plateTable->setCellColor(r+8*(counter/96),c+1,cColorsManager::getColor(i,-j*10));
                    plateTable->setItemText(r+8*(counter/96),c+1,FXStringFormat("%s%d%s",sampleKey(i).text(),p+1,replicates));
                }
                v=1;
                counter++;
                replicates[j]='\'';
                replicates[j+1]=0;
            }
            stock--;
            p++;
        }
    }
    onRszPlateTable(NULL,0,NULL);
    onRszCaseList(NULL,0,NULL);
}

FXbool cMDITemplate::saveData(void)
{
    caseList->acceptInput(true);
    plateTable->acceptInput(true);
    if(saved)
        return true;

    if(caseList->getNumRows()==0)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_nocase").text());
        return false;
    }

    FXbool noctl=false;
    if(controlsCount<=0)
        noctl=true;
    else
    {
        int jp=0,jn=0;
        for(int i=0;i<TEMPLATE_MAX_CONTROLS;i++)
            if(controls[i]>0)
                jp++;
            else if(controls[i]<0 && controls[i]!=-100)
                jn++;
        if(!jp || !jn)
            noctl=true;
    }

    if(noctl)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_nocontrols").text());
        return false;
    }

    FXString oldname=name;
    name=tfName->getText();
    if(name!=oldname && cTemplateManager::templateExists(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
        tfName->setText(cTemplateManager::getNewID());
        setTitle(tfName->getText());
        updateData();
        return false;
    }
    this->setTitle(name);

    FXString controls_defs=FXStringVal(controlsChoice)+"\n",cases_defs=FXStringVal(caseList->getNumRows())+"\n";

    for(int i=0;i<TEMPLATE_MAX_CONTROLS;i++)
        if(controls[i]!=-100)
            controls_defs=controls_defs+FXStringFormat("%d\t",controls[i]);
    controls_defs=controls_defs+"-100";

    for(int i=0;i<caseList->getNumRows();i++)
    {
        if(caseList->getItemText(i,1).empty())
            continue;
        for(int j=1;j<caseList->getNumColumns();j++)
            cases_defs=cases_defs+caseList->getItemText(i,j)+"\t";
        cases_defs=cases_defs+"\n";
    }
    if(oldname.empty())
        cTemplateManager::addTemplate(name,lbDate->getText(),orientationChoice,alelisaChoice,controls_defs,cases_defs);
    else
        cTemplateManager::setTemplate(oldname,name,lbDate->getText(),orientationChoice,alelisaChoice,controls_defs,cases_defs);
    saved=true;
    cMDITemplateManager::cmdRefresh();
    return true;
}

cMDITemplate::cMDITemplate(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH)
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH),
    orientationTgt(orientationChoice,this,ID_ORIENTATIONTGT),
    controlsTgt(controlsChoice,this,ID_CONTROLSTGT),
    subcTgt(subcChoice,this,ID_SUBCTGT),
    alelisaTgt(alelisaChoice,this,ID_ALELISATGT)
{
    setSelector(ID_MDITEMPLATE);
    saved=false;
    name="";
    clearAllControls();
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL);

        FXHorizontalFrame *_hframe0=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
            FXMatrix *_matrix0=new FXMatrix(_hframe0,2,MATRIX_BY_ROWS|PACK_UNIFORM_HEIGHT|LAYOUT_FILL_X,0,0,0,0,0,0,7,7);
            new FXLabel(_matrix0,oLanguage->getText("str_tplmdi_name"));
            new FXLabel(_matrix0,oLanguage->getText("str_tplmdi_date"));
            tfName=new FXTextField(_matrix0,15,this,ID_TPLNAME);
            tfName->setText(prName);
            tfName->setSelection(0,tfName->getText().length());
            tfName->setFocus();
            lbDate=new FXLabel(_matrix0,"");
            lbDate->setTextColor(FXRGB(0,130,0));
                FXchar ftime[100];
                time_t curtime;
                struct tm *loctime;
                curtime=time(NULL);
                loctime=localtime(&curtime);
                strftime(ftime,100,"%Y-%m-%d",loctime);
                lbDate->setText(ftime);
            orientationChoice=1;
            alelisaChoice=1;
            controlsChoice=1;
            oldControlsChoice=0;
            subcChoice=1;
            controls[0]=-340;
            FXGroupBox *_group2=new FXGroupBox(_hframe0,oLanguage->getText("str_tplmdi_alelisa"),GROUPBOX_TITLE_LEFT|FRAME_GROOVE|LAYOUT_RIGHT);
                new FXRadioButton(_group2,oLanguage->getText("str_tplmdi_alelisa1"),&alelisaTgt,FXDataTarget::ID_OPTION+1);
                new FXRadioButton(_group2,oLanguage->getText("str_tplmdi_alelisa2"),&alelisaTgt,FXDataTarget::ID_OPTION+2);
            FXGroupBox *_group1=new FXGroupBox(_hframe0,oLanguage->getText("str_tplmdi_controls"),GROUPBOX_TITLE_LEFT|FRAME_GROOVE|LAYOUT_RIGHT);
                FXMatrix *_matrix1=new FXMatrix(_group1,2,MATRIX_BY_ROWS|PACK_UNIFORM_HEIGHT,0,0,0,0,0,0,0,0);
                new FXRadioButton(_matrix1,oLanguage->getText("str_tplmdi_cdoubles"),&controlsTgt,FXDataTarget::ID_OPTION+1);
                new FXRadioButton(_matrix1,oLanguage->getText("str_tplmdi_ctriples"),&controlsTgt,FXDataTarget::ID_OPTION+2);
                new FXRadioButton(_matrix1,oLanguage->getText("str_tplmdi_ccustom"),&controlsTgt,FXDataTarget::ID_OPTION+3);
                FXHorizontalFrame *_hframe11=new FXHorizontalFrame(_matrix1,LAYOUT_FILL|JUSTIFY_CENTER_X,0,0,0,0,0,0,0,0);
                butNeg=new FXRadioButton(_hframe11,oLanguage->getText("str_tplmdi_cnegative"),&subcTgt,FXDataTarget::ID_OPTION+1);
                butPos=new FXRadioButton(_hframe11,oLanguage->getText("str_tplmdi_cpositive"),&subcTgt,FXDataTarget::ID_OPTION+2);
                butNone=new FXRadioButton(_hframe11,oLanguage->getText("str_tplmdi_cnone"),&subcTgt,FXDataTarget::ID_OPTION+3);
            FXGroupBox *_group0=new FXGroupBox(_hframe0,oLanguage->getText("str_tplmdi_orientation"),GROUPBOX_TITLE_LEFT|FRAME_GROOVE|LAYOUT_RIGHT);
                new FXRadioButton(_group0,oLanguage->getText("str_tplmdi_vert"),&orientationTgt,FXDataTarget::ID_OPTION+1);
                new FXRadioButton(_group0,oLanguage->getText("str_tplmdi_horiz"),&orientationTgt,FXDataTarget::ID_OPTION+2);

        FXHorizontalFrame *_hframe200=new FXHorizontalFrame(_vframe0,0,0,0,0,0,0,0,0,0,0,0);
        new FXLabel(_hframe200,oLanguage->getText("str_tplmdi_platel"));
        lbPlates=new FXLabel(_hframe200,oLanguage->getText("str_tplmdi_platel"));
        FXHorizontalFrame *_hframe1=new FXHorizontalFrame(_vframe0,LAYOUT_FILL,0,0,0,0,0,0,0,0);
        FXVerticalFrame *_vframe1=new FXVerticalFrame(_hframe1,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
            plateTable=new cColorTable(_vframe1,this,ID_PLATETABLE,TABLE_READONLY|TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT|LAYOUT_FILL,0,0,0,181);
            plateTable->setGridColor(FXRGB(120,120,120));
            plateTable->setDefColumnWidth(0);
            plateTable->setTableSize(8,12);
            plateTable->setSelTextColor(0);
            plateTable->setCellBorderWidth(2);
            plateTable->setBorderColor(FXRGB(120,120,120));
            plateTable->setStippleColor(FXRGB(250,250,240));
            plateTable->setScrollStyle(HSCROLLING_OFF|HSCROLLER_NEVER|VSCROLLING_ON|VSCROLLER_ALWAYS|SCROLLERS_TRACK);
            for(int j=0;j<12;j++)
            {
                plateTable->getColumnHeader()->setItemJustify(j,JUSTIFY_CENTER_X);
                plateTable->getColumnHeader()->setItemText(j,FXStringFormat("%d",j+1));
            }
        FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe1,LAYOUT_RIGHT|LAYOUT_FILL_Y,0,0,0,0,0,0,0,0,0,0);
            new FXButton(_vframe2,oLanguage->getText("str_tplmdi_save"),new FXGIFIcon(getApp(),data_new_template),this,CMD_SAVE,FRAME_RAISED|BUTTON_DEFAULT|BUTTON_INITIAL|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH,0,0,100,0);
            new FXHorizontalSeparator(_vframe2,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT,0,0,0,10);
            new FXButton(_vframe2,oLanguage->getText("str_tplmdi_read"),new FXGIFIcon(getApp(),data_start_reading),this,CMD_READ,FRAME_RAISED|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH,0,0,100,0);
            new FXButton(_vframe2,oLanguage->getText("str_tplmdi_fill"),NULL,this,CMD_FILL,FRAME_RAISED|LAYOUT_FILL_X);
            new FXHorizontalSeparator(_vframe2,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT,0,0,0,10);
            new FXButton(_vframe2,oLanguage->getText("str_but_close"),NULL,this,CMD_CLOSE,FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,100,0);
            new FXHorizontalSeparator(_vframe2,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT|LAYOUT_BOTTOM,0,0,0,10);
            //new FXButton(_vframe2,oLanguage->getText("str_tplmdi_print"),new FXGIFIcon(getApp(),data_settings_printer),this,CMD_PRINT,FRAME_RAISED|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,100,0);

        caselb=new FXLabel(_vframe0,oLanguage->getText("str_tplmdi_casesl"));
        FXHorizontalFrame *_hframe2=new FXHorizontalFrame(_vframe0,LAYOUT_FILL,0,0,0,0,0,0,0,0);
        FXHorizontalFrame *_hframe3=new FXHorizontalFrame(_hframe2,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
        caseList=new cColorTable(_hframe3,this,ID_CASELIST,TABLE_NO_COLSELECT|LAYOUT_FILL);
        caseList->setTipText(oLanguage->getText("str_tplmdi_casetip"));
        caseList->setTableSize(0,8);
        caseList->setRowHeaderWidth(0);
        caseList->setColumnText(0,oLanguage->getText("str_tplmdi_caseloc"));
        caseList->setColumnWidth(0,70);
        caseList->setColumnText(1,oLanguage->getText("str_tplmdi_casenam"));
        caseList->setColumnWidth(1,139);
        caseList->setColumnText(2,oLanguage->getText("str_tplmdi_casecnt"));
        caseList->setColumnWidth(2,35);
        caseList->setColumnText(3,oLanguage->getText("str_tplmdi_casepop"));
        caseList->setColumnWidth(3,109);
        caseList->setColumnText(4,oLanguage->getText("str_tplmdi_casebld"));
        caseList->setColumnWidth(4,75);
        caseList->setColumnText(5,oLanguage->getText("str_tplmdi_caseage"));
        caseList->setColumnWidth(5,33);
        caseList->setColumnText(6,oLanguage->getText("str_tplmdi_caserep"));
        caseList->setColumnWidth(6,40);
        caseList->setColumnText(7,oLanguage->getText("str_tplmdi_casefct"));
        caseList->setColumnWidth(7,34);
        caseList->getColumnHeader()->setItemJustify(0,JUSTIFY_CENTER_X);
        caseList->getColumnHeader()->setItemJustify(2,JUSTIFY_CENTER_X);
        caseList->getColumnHeader()->setItemJustify(4,JUSTIFY_CENTER_X);
        caseList->getColumnHeader()->setItemJustify(5,JUSTIFY_CENTER_X);
        caseList->getColumnHeader()->setItemJustify(6,JUSTIFY_CENTER_X);
        caseList->getColumnHeader()->setItemJustify(7,JUSTIFY_CENTER_X);
        caseList->setGridColor(FXRGB(255,255,255));
        caseList->setCellBorderWidth(1);
        caseList->setColumnHeaderHeight(32);
        caseList->showHorzGrid(true);
        caseList->showVertGrid(false);
        caseList->setSelTextColor(0);
        caseList->setCellBorderColor(FXRGB(255,0,0));
        caseList->setSelBackColor(FXRGB(180,180,180));
        caseList->setScrollStyle(HSCROLLING_ON|VSCROLLER_ALWAYS|VSCROLLING_ON);
        FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe2,LAYOUT_FILL_Y|LAYOUT_RIGHT,0,0,0,0,0,0,0,0,0,0);
            new FXButton(_vframe3,oLanguage->getText("str_tplmdi_casenew"),NULL,this,CMD_CASENEW,FRAME_RAISED|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH,0,0,100,0);
            new FXButton(_vframe3,oLanguage->getText("str_tplmdi_casedel"),NULL,this,CMD_CASEDEL,FRAME_RAISED|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH,0,0,100,0);
            new FXArrowButton(_vframe3,this,CMD_CASEDN,ARROW_DOWN|ARROW_REPEAT|FRAME_RAISED|LAYOUT_FIX_WIDTH|LAYOUT_FIX_HEIGHT|LAYOUT_BOTTOM,0,0,100,20);
            new FXArrowButton(_vframe3,this,CMD_CASEUP,ARROW_UP|ARROW_REPEAT|FRAME_RAISED|LAYOUT_FIX_WIDTH|LAYOUT_FIX_HEIGHT|LAYOUT_BOTTOM,0,0,100,20);
}

cMDITemplate::~cMDITemplate()
{
    getApp()->removeTimeout(this,EVT_LBDATE);
}

void cMDITemplate::create()
{
    cMDIChild::create();
    show();
    onCmdUpdateControls(NULL,0,NULL);
    updateData();
}

FXbool cMDITemplate::canClose(void)
{
    caseList->acceptInput(true);
    plateTable->acceptInput(true);
    if(!saved)
    {
        setFocus();
        int res=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),oLanguage->getText("str_qsave").text());
        switch(res)
        {
            case MBOX_CLICKED_YES:
                if(!saveData())
                    return false;
                break;
            case MBOX_CLICKED_CANCEL:
                return false;
            default:
                break;
        }
    }
    return true;
}

FXbool cMDITemplate::loadTemplate(const FXString &prTitle)
{
    if(!cTemplateManager::templateExists(prTitle))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_opentpl").text());
        return false;
    }
    cDataResult *res=cTemplateManager::getTemplate(prTitle);
    if(res==NULL)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_opentpl").text());
        return false;
    }
    name=res->getCellString(0,0);
    lbDate->setText(res->getCellString(0,1));
    orientationChoice=res->getCellInt(0,2);
    alelisaChoice=res->getCellInt(0,3);
    FXString controls_defs=res->getCellString(0,4);
    FXString cases_defs=res->getCellString(0,5);
    setTitle(name);
    oldControlsChoice=controlsChoice=FXIntVal(controls_defs.before('\n'));
    clearAllControls();
    int k;
    switch(controlsChoice)
    {
    case 3:
        controls_defs=controls_defs.after('\n');
        controlsCount=0;
        k=0;
        do{
            k=FXIntVal(controls_defs.before('\t'));
            controls_defs=controls_defs.after('\t');
            if(k!=0 && k!=-100)
                controls[controlsCount++]=k;
        }while(k!=0 && k!=-100);
        break;
    case 2:
        placePositive(0);
        placeNegative(1);
        placePositive(2);
        placeNegative(alelisaChoice==2?85:93);
        placePositive(alelisaChoice==2?86:94);
        placeNegative(alelisaChoice==2?87:95);
        break;
    default:
        placeNegative(0);
        placeNegative(1);
        placePositive(2);
        placePositive(3);
        break;
    }

    caseList->removeRows(0,caseList->getNumRows());
    int limita=FXIntVal(cases_defs.before('\n'));
    for(int i=0;i<limita;i++)
    {
        cases_defs=cases_defs.after('\n');
        caseList->insertRows(i);
        caseList->layout();
        for(int j=0;j<caseList->getNumColumns();j++)
        {
            caseList->setItemJustify(i,j,JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
            caseList->setCellColor(i,j,cColorsManager::getColor(i));
        }
        caseList->setCellColor(i,0,cColorsManager::getColor(i,-30));
        caseList->setCellEditable(i,0,false);
        caseList->setItemJustify(i,1,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
        caseList->setItemJustify(i,3,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);

        caseList->setItemText(i,0,"");
        caseList->setItemText(i,1,cases_defs.before('\t'));
        cases_defs=cases_defs.after('\t');
        caseList->setItemText(i,2,cases_defs.before('\t'));
        cases_defs=cases_defs.after('\t');
        caseList->setItemText(i,3,cases_defs.before('\t'));
        cases_defs=cases_defs.after('\t');
        caseList->setItemText(i,4,cases_defs.before('\t'));
        cases_defs=cases_defs.after('\t');
        caseList->setItemText(i,5,cases_defs.before('\t'));
        cases_defs=cases_defs.after('\t');
        caseList->setItemText(i,6,cases_defs.before('\t'));
        cases_defs=cases_defs.after('\t');
        caseList->setItemText(i,7,cases_defs.before('\t'));
    }
    updateData();
    saved=true;
    return true;
}

long cMDITemplate::onRszPlateTable(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int tw=plateTable->getWidth()-plateTable->verticalScrollBar()->getWidth();
    int cw=tw/13;
    int hw=tw-(cw*13)+cw;
    if(cw<30)
        cw=hw=30;
    plateTable->setRowHeaderWidth(hw);
    for(tw=0;tw<12;tw++)
        plateTable->setColumnWidth(tw,cw);
    return 1;
}

long cMDITemplate::onUpdPlateTable(FXObject *prSender,FXSelector prSelector,void *prData)
{
    return 1;
}

long cMDITemplate::onUpdSelPlateTable(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int sr,ssr=plateTable->getSelStartRow(),ser=plateTable->getSelEndRow();
    if(ssr<0 || ser<0)
        return 1;
    sr=ssr;
    if(ssr!=ser)
        sr=(ssr==plateTable->getAnchorRow()?ser:ssr);
    int sc,ssc=plateTable->getSelStartColumn(),sec=plateTable->getSelEndColumn();
    if(ssc<0 || sec<0)
        return 1;
    sc=ssc;

    if(ssc!=sec)
        sc=(ssc==plateTable->getAnchorColumn()?sec:ssc);
    for(int i=0;i<plateTable->getNumRows();i++)
        plateTable->getRowHeader()->setItemPressed(i,false);
    for(int i=0;i<12;i++)
        plateTable->getColumnHeader()->setItemPressed(i,false);
    plateTable->getRowHeader()->setItemPressed(sr,true);
    plateTable->getColumnHeader()->setItemPressed(sc,true);
    plateTable->killSelection();
    return 1;
}

long cMDITemplate::onUpdCaseList(FXObject *prSender,FXSelector prSelector,void *prData)
{
    caselb->setText(oLanguage->getText("str_tplmdi_casesl")+
                    (caseList->getNumRows()>0?FXStringFormat(" - %d %s",
                     caseList->getNumRows(),oLanguage->getText("str_tplmdi_caseslsx").text()):""));
    return 1;
}

long cMDITemplate::onRszCaseList(FXObject *prSender,FXSelector prSelector,void *prData)
{
    //int tw=caseList->getWidth()-caseList->verticalScrollBar()->getWidth();
    //caseList->setColumnWidth(7,tw-490);
    return 1;
}

long cMDITemplate::onUpdSelCaseList(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int sr,ssr=caseList->getSelStartRow(),ser=caseList->getSelEndRow();
    if(ssr<0 || ser<0)
        return 1;
    sr=ssr;
    if(ssr!=ser)
        sr=(ssr==caseList->getAnchorRow()?ser:ssr);
    drawCaseList();
    for(int i=1;i<caseList->getNumColumns();i++)
    {
        caseList->setCellColor(sr,i,cColorsManager::getColor(sr,-30));
        caseList->setItemBorders(sr,i,FXTableItem::TBORDER|FXTableItem::BBORDER);
    }
    caseList->setItemBorders(sr,1,FXTableItem::LBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    caseList->setItemBorders(sr,caseList->getNumColumns()-1,FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    caseList->killSelection();
    return 1;
}

long cMDITemplate::onChgTfName(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    return 1;
}

long cMDITemplate::onCmdTfName(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!tfName->getText().empty())
    {
        if(!cTemplateManager::templateExists(tfName->getText()))
        {
            updateData();
            return 1;
        }
        else
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
    }
    if(name.empty() && cTemplateManager::templateExists(getTitle()))
    {
        tfName->setText(cTemplateManager::getNewID());
        setTitle(tfName->getText());
    }
    else
        tfName->setText(getTitle());
    updateData();
    return 1;
}

long cMDITemplate::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(canClose())
    {
        close();
        return 1;
    }
    return 0;
}

long cMDITemplate::onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    return 1;
}

long cMDITemplate::onCmdUpdateControls(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(controlsChoice==oldControlsChoice)
        return 1;
    if(controls[0]!=-340)
        if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_resetcontrols").text()))
        {
            controlsChoice=oldControlsChoice;
            return 1;
        }
    oldControlsChoice=controlsChoice;
    switch(controlsChoice)
    {
        case 1:
            clearAllControls();
            placeNegative(0);
            placeNegative(1);
            placePositive(2);
            placePositive(3);
            break;
        case 2:
            clearAllControls();
            placePositive(0);
            placeNegative(1);
            placePositive(2);
            placeNegative(alelisaChoice==2?85:93);
            placePositive(alelisaChoice==2?86:94);
            placeNegative(alelisaChoice==2?87:95);
            break;
        default:
            clearAllControls();
            break;
    }
    updateData();
    return 1;
}

long cMDITemplate::onCmdUpdateOrientation(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(orientationChoice==2 && alelisaChoice==2)
    {
        orientationChoice=1;
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_alelisa").text());
    }
    updateData();
    return 1;
}

long cMDITemplate::onCmdUpdateAlelisa(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(orientationChoice==2 && alelisaChoice==2)
    {
        alelisaChoice=1;
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_alelisa").text());
        return 1;
    }
    if(controlsChoice==2)
    {
        if(alelisaChoice==2)
        {
            clearControl(93);
            clearControl(94);
            clearControl(95);
            placeNegative(85);
            placePositive(86);
            placeNegative(87);
        }
        else
        {
            clearControl(85);
            clearControl(86);
            clearControl(87);
            placeNegative(93);
            placePositive(94);
            placeNegative(95);
        }
    }
    else if(alelisaChoice==2)
    {
        int i,j,k=1;
        for(i=0;i<TEMPLATE_MAX_CONTROLS;i++)
        {
            j=abs(controls[i]);
            if(j<100 && ((j/8)%2)>0)
            {
                if(k && MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_resetcontrolsalelisa").text()))
                {
                    alelisaChoice=1;
                    return 1;
                }
                k=0;
                controls[i]=-100;
            }
        }
    }
    updateData();
    return 1;
}

long cMDITemplate::onDbcPlateTable(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(controlsChoice!=3 || (alelisaChoice==2 && (plateTable->getCurrentColumn()%2!=0)))
        return 1;
    switch(subcChoice)
    {
        case 1:
            placeNegative(plateTable->getCurrentRow(),plateTable->getCurrentColumn());
            break;
        case 2:
            placePositive(plateTable->getCurrentRow(),plateTable->getCurrentColumn());
            break;
        default:
            clearControl(plateTable->getCurrentRow(),plateTable->getCurrentColumn());
            break;
    }
    plateTable->killSelection();
    updateData();
    return 1;
}

long cMDITemplate::onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData)
{
    saveData();
    if(saved)
        onCmdClose(NULL,0,NULL);
    return 1;
}

long cMDITemplate::onCmdRead(FXObject *prSender,FXSelector prSelector,void *prData)
{
    caseList->acceptInput(true);
    plateTable->acceptInput(true);
    if(!saved)
    {
        setFocus();
        int res=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),oLanguage->getText("str_qsave").text());
        switch(res)
        {
            case MBOX_CLICKED_YES:
                if(!saveData())
                    return 1;
                break;
            case MBOX_CLICKED_CANCEL:
                return 1;
            default:
                break;
        }
    }
    cDLGChooseAT dlg(this,CHOOSEAT_ASSAY);
    dlg.setTemplate(name);
    if(!dlg.execute(PLACEMENT_OWNER))
        return 1;
    cReadingsManager::makeReading(mdiClient,mdiMenu,dlg.getAssay(),dlg.getTemplate(),false,dlg.getLot(),dlg.getExpDate());
    return 1;
}

long cMDITemplate::onCmdFill(FXObject *prSender,FXSelector prSelector,void *prData)
{
    caseList->acceptInput(true);
    plateTable->acceptInput(true);
    if(!saved)
    {
        setFocus();
        int res=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),oLanguage->getText("str_qsave").text());
        switch(res)
        {
            case MBOX_CLICKED_YES:
                if(!saveData())
                    return 1;
                break;
            case MBOX_CLICKED_CANCEL:
                return 1;
            default:
                break;
        }
    }
    cDLGChooseAT dlg(this,CHOOSEAT_ASSAY);
    dlg.setTemplate(name);
    if(!dlg.execute(PLACEMENT_OWNER))
        return 1;
    cReadingsManager::makeReading(mdiClient,mdiMenu,dlg.getAssay(),dlg.getTemplate(),true,dlg.getLot(),dlg.getExpDate());
    return 1;
}

long cMDITemplate::onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData)
{


    // TODO


    return 1;
}

long cMDITemplate::onCmdPpDate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXEvent *event=(FXEvent*)prData;
    int sc=caseList->colAtX(event->win_x);
    int sr=caseList->rowAtY(event->win_y);
    if(sc!=4 || sr<0)
        return 1;
    if(sr>=caseList->getNumRows())sr=caseList->getNumRows()-1;
    caseList->killSelection();
    caseList->setCurrentItem(sr,sc);
    caseList->selectRow(sr);
    onUpdSelCaseList(NULL,0,NULL);
    FXString res=cDateChooser::selectDate();
    event->click_count=0;
    if(!res.empty())
        caseList->setItemText(sr,sc,res);
    FXString s1=cPopulationManager::getPopulationAge(caseList->getItemText(sr,3),caseList->getItemText(sr,4));
    caseList->setItemText(sr,5,s1.empty()?"0-0":s1);
    return 1;
}

long cMDITemplate::onCmdCaseList(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(caseList->getNumRows()<=0)
        return 1;
    int uret,r=*((int*)prData),c=*((int*)prData+1);
    FXString s1,s2,s=caseList->getItemText(r,c),err="";
    struct tm prstime;
    switch(c)
    {
        case 1:
            if(s.empty())
            {
                caseList->setItemText(r,c,cCaseManager::getNewID());
                break;
            }
            for(int i=0;i<caseList->getNumRows();i++)
                if(i!=r && caseList->getItemText(i,1)==s)
                {
                    err="str_invalid_duplid";
                    break;
                }
            for(int i=0;i<s.length();i++)
                if(!isalnum(s.at(i)) && s.at(i)!=' ' && s.at(i)!='#' && s.at(i)!='-' && s.at(i)!='/')
                {
                    err="str_invalid_numlet";
                    break;
                }
            break;
        case 2:
            if(s.empty())
            {
                caseList->setItemText(r,c,"1");
                break;
            }
            for(int i=0;i<s.length();i++)
                if(!isdigit(s.at(i)))
                {
                    err="str_invalid_pnum";
                    break;
                }
            if(err.empty() && FXIntVal(s)==0)
                err="str_invalid_nzero";
            uret=(FXuint)FXIntVal(s);
            if(uret>1000)
                uret=1000;
            caseList->setItemText(r,c,FXStringFormat("%d",uret));
            break;
        case 7:
            if(s.empty())
            {
                caseList->setItemText(r,c,"1");
                break;
            }
            for(int i=0;i<s.length();i++)
                if(!isdigit(s.at(i)))
                {
                    err="str_invalid_pnum";
                    break;
                }
            if(err.empty() && FXIntVal(s)==0)
                err="str_invalid_nzero";
            uret=(FXuint)FXIntVal(s);
            if(uret>4)
                uret=4;
            caseList->setItemText(r,c,FXStringFormat("%d",uret));
            break;
        case 4:
            if(s.empty())
                caseList->setItemText(r,c,lbDate->getText());
            else
            {
                memset(&prstime,0,sizeof(prstime));
                if(sscanf(s.text(),"%d-%d-%d",&prstime.tm_year,&prstime.tm_mon,&prstime.tm_mday)!=3)
                {
                    err="str_invalid_date";
                    break;
                }
                prstime.tm_year-=prstime.tm_year<1900?0:1900;
                prstime.tm_mon-=1;
                caseList->setItemText(r,c,FXStringFormat("%04d-%02d-%02d",prstime.tm_year+1900,prstime.tm_mon+1,prstime.tm_mday));
            }
            s1=cPopulationManager::getPopulationAge(caseList->getItemText(r,3),caseList->getItemText(r,4));
            caseList->setItemText(r,5,s1.empty()?"0-0":s1);
            break;
        case 3:
            if(s.empty())
            {
                caseList->setItemText(r,c,cPopulationManager::getNewID());
                break;
            }
            for(int i=0;i<s.length();i++)
                if(!isalnum(s.at(i)) && s.at(i)!=' ' && s.at(i)!='#' && s.at(i)!='-' && s.at(i)!='/')
                {
                    err="str_invalid_numlet";
                    break;
                }
            s1=cPopulationManager::getPopulationAge(caseList->getItemText(r,3),caseList->getItemText(r,4));
            caseList->setItemText(r,5,s1.empty()?"0-0":s1);
            break;
        case 5:
            unsigned int t,v1,v2,cat;
            if(s.empty())
            {
                s1=cPopulationManager::getPopulationAge(caseList->getItemText(r,3),caseList->getItemText(r,4));
                caseList->setItemText(r,c,s1.empty()?"0-0":s1);
                break;
            }
            for(int i=0;i<s.length();i++)
                if(!isdigit(s.at(i)) && s.at(i)!='-')
                {
                    err="str_invalid_age";
                    break;
                }
            s1=s.before('-');
            s2=s.after('-');
            if(s2.empty())
            {
                v1=0;
                v2=FXIntVal(s1);
            }
            else
            {
                v1=FXIntVal(s1);
                v2=FXIntVal(s2);
            }
            cat=cSpeciesManager::getAgeFormat(oDataLink->getSpecies());
            t=v1*cat+v2;
            v1=t/cat;
            v2=t%cat;
            if((unsigned int)v1>999)v1=999;
            caseList->setItemText(r,c,FXStringFormat("%d-%d",v1,v2));
            break;
        case 6:
            if(s.empty())
            {
                caseList->setItemText(r,c,"0");
                break;
            }
            for(int i=0;i<s.length();i++)
                if(!isdigit(s.at(i)))
                {
                    err="str_invalid_pnum04";
                    break;
                }
            uret=(FXuint)FXIntVal(s);
            if(uret>3)
                uret=3;
            caseList->setItemText(r,c,FXStringFormat("%d",uret));
            break;
    }

    if(!err.empty())
    {
        caseList->setItemText(r,c,"");
        caseList->startInput(r,c);
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),
                              (oLanguage->getText("str_invalid")+"\n"+
                               oLanguage->getText(err)).text());
        return 1;
    }

    updateData();
    return 1;
}

long cMDITemplate::onCmdCaseNew(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int allow=true,r;
    caseList->acceptInput(true);
    if(caseList->getNumRows()>=1000)
        return 1;
    for(int i=0;i<caseList->getNumRows();i++)
        if(caseList->getItemText(i,1).empty())
        {
            allow=false;
            caseList->startInput(i,1);
        }
    if(allow==true)
    {
        r=caseList->getNumRows();
        caseList->insertRows(r);
        caseList->layout();
        for(int i=0;i<caseList->getNumColumns();i++)
        {
            caseList->setItemJustify(r,i,JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
            caseList->setCellColor(r,i,cColorsManager::getColor(r));
        }
        caseList->setCellColor(r,0,cColorsManager::getColor(r,-30));
        caseList->setCellEditable(r,0,false);
        caseList->setItemJustify(r,1,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
        caseList->setItemJustify(r,3,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
        caseList->setItemText(r,1,cCaseManager::getNewID());
        caseList->setItemText(r,2,"1");
        caseList->setItemText(r,3,cPopulationManager::getNewID());
        caseList->setItemText(r,4,lbDate->getText());
        caseList->setItemText(r,5,"0-0");
        caseList->setItemText(r,6,"0");
        caseList->setItemText(r,7,"1");
        caseList->setCurrentItem(r,1);
        caseList->makePositionVisible(r,1);
        caseList->startInput(r,1);
        updateData();
    }
    return 1;
}

long cMDITemplate::onCmdCaseDel(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int sr=caseList->getCurrentRow();
    if(sr==-1)
        return 1;
    if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_erasecs").text()))
        return 1;
    caseList->removeRows(sr,1);
    drawCaseList();
    updateData();
    return 1;
}

long cMDITemplate::onCmdCaseUp(FXObject *prSender,FXSelector prSelector,void *prData)
{
    caseList->acceptInput();
    int er,sr=caseList->getCurrentRow();
    if(sr<1)
        return 1;
    er=sr-1;
    FXString v;
    for(int i=1;i<caseList->getNumColumns();i++)
    {
        v=caseList->getItemText(er,i);
        caseList->setItemText(er,i,caseList->getItemText(sr,i));
        caseList->setItemText(sr,i,v);
    }
    drawCaseList();
    updateData();
    caseList->setCurrentItem(er,caseList->getCurrentColumn());
    caseList->selectRow(er);
    onUpdSelCaseList(NULL,0,NULL);
    return 1;
}

long cMDITemplate::onCmdCaseDown(FXObject *prSender,FXSelector prSelector,void *prData)
{
    caseList->acceptInput();
    int er,sr=caseList->getCurrentRow();
    if(sr<0 || (sr>=caseList->getNumRows()-1))
        return 1;
    er=sr+1;
    FXString v;
    for(int i=1;i<caseList->getNumColumns();i++)
    {
        v=caseList->getItemText(er,i);
        caseList->setItemText(er,i,caseList->getItemText(sr,i));
        caseList->setItemText(sr,i,v);
    }
    drawCaseList();
    updateData();
    caseList->setCurrentItem(er,caseList->getCurrentColumn());
    caseList->selectRow(er);
    onUpdSelCaseList(NULL,0,NULL);
    return 1;
}

