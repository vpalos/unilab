#include "engine.h"
#include "graphics.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cDLGReportCM.h"

FXDEFMAP(cDLGReportCM) mapDLGReportCM[]=
{
    FXMAPFUNC(SEL_UPDATE,cDLGReportCM::ID_UPDATE,cDLGReportCM::onCmdUpdate),
    FXMAPFUNC(SEL_COMMAND,cDLGReportCM::CMD_BUT_APPLY,cDLGReportCM::onCmdApply),
};

FXIMPLEMENT(cDLGReportCM,FXDialogBox,mapDLGReportCM,ARRAYNUMBER(mapDLGReportCM))

cDLGReportCM::cDLGReportCM(FXWindow *prOwner,FXbool areTiters,FXbool areSecondary,FXbool areBins) :
    FXDialogBox(prOwner,oLanguage->getText("str_report_cm")+"...",DECOR_TITLE|DECOR_BORDER),chtype(choice)
{
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL);
        bUseInfo=new FXCheckButton(_vframe0,oLanguage->getText("str_report_showInfo"),this,ID_UPDATE);
        bUseInfo->setTextColor(FXRGB(0,0,200));
        bUseInfo->setCheck(true);
        new FXHorizontalSeparator(_vframe0);
        choice=3;
        FXLabel *_lb0=new FXLabel(_vframe0,oLanguage->getText("str_report_gdata"));
            _lb0->setTextColor(FXRGB(0,0,200));
        FXGroupBox *_gb1=new FXGroupBox(_vframe0,(char*)NULL,GROUPBOX_NORMAL,0,0,0,0,17,0,0,0);
            if(areTiters)
            {
                bTitGraphs=new FXRadioButton(_gb1,oLanguage->getText("str_report_titGraph"),&chtype,FXDataTarget::ID_OPTION+1);
                bTitGraphs->setTextColor(FXRGB(120,120,120));
                choice=1;
            }
            else
                bTitGraphs=NULL;
            if(areBins)
            {
                bBinGraphs=new FXRadioButton(_gb1,oLanguage->getText("str_report_binGraph"),&chtype,FXDataTarget::ID_OPTION+2);
                bBinGraphs->setTextColor(FXRGB(120,120,120));
                if(!areTiters)
                    choice=2;
            }
            else
                bBinGraphs=NULL;
        new FXHorizontalSeparator(_vframe0);
        bUseData=new FXCheckButton(_vframe0,oLanguage->getText("str_report_showStats"),this,ID_UPDATE);
        bUseData->setTextColor(FXRGB(0,0,200));
        bUseData->setCheck(true);
        FXVerticalFrame *_gb0=new FXVerticalFrame (_vframe0,LAYOUT_FILL_X,0,0,0,0,17);
            FXVerticalFrame *_vframe1=new FXVerticalFrame(_gb0,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
                bcCala=new FXCheckButton(_vframe1,oLanguage->getText("str_asymdi_cala"));
                bcCala->setTextColor(FXRGB(120,120,120));
                bcCala->setCheck(true);
                if(areSecondary)
                {
                    bcCalb=new FXCheckButton(_vframe1,oLanguage->getText("str_asymdi_calb"));
                    bcCalb->setCheck(true);
                    bcCalb->setTextColor(FXRGB(120,120,120));
                }
                else
                    bcCalb=NULL;
                bcEU=new FXCheckButton(_vframe1,oLanguage->getText("str_rdgmdi_eu"));
                bcEU->setTextColor(FXRGB(120,120,120));
                if(areTiters)
                {
                    bcTiter=new FXCheckButton(_vframe1,oLanguage->getText("str_asymdi_titer"));
                    bcTiter->setCheck(true);
                    bcTiter->setTextColor(FXRGB(120,120,120));
                    bcLog2=new FXCheckButton(_vframe1,oLanguage->getText("str_rdgmdi_log2"));
                    bcLog2->setTextColor(FXRGB(120,120,120));
                }
                else
                {
                    bcTiter=NULL;
                    bcLog2=NULL;
                }
        new FXHorizontalSeparator(_vframe0);
        bcAm=new FXCheckButton(_vframe0,oLanguage->getText("str_rdgmdi_mean"));
        bcAm->setCheck(true);
        bcAm->setTextColor(FXRGB(0,0,200));
        new FXHorizontalSeparator(_vframe0);

        FXHorizontalFrame *_hframe100=new FXHorizontalFrame(_vframe0,LAYOUT_CENTER_X);
        butCancel=new FXButton(_hframe100,oLanguage->getText("str_but_cancel"),NULL,this,ID_CANCEL,BUTTON_DEFAULT|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
        butAccept=new FXButton(_hframe100,oLanguage->getText("str_but_ok"),NULL,this,CMD_BUT_APPLY,BUTTON_DEFAULT|BUTTON_INITIAL|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
}

cDLGReportCM::~cDLGReportCM()
{
}

long cDLGReportCM::onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData)
{
    handle(NULL,FXSEL(SEL_COMMAND,ID_ACCEPT),NULL);
    return 1;
}

long cDLGReportCM::onCmdUpdate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(bUseData->getCheck())
    {
        bcCala->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bcCalb)
            bcCalb->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bcTiter)
            bcTiter->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bcLog2)
            bcLog2->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        bcEU->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    else
    {
        bcCala->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        if(bcCalb)
            bcCalb->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        if(bcTiter)
            bcTiter->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        if(bcLog2)
            bcLog2->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        bcEU->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }
    return 1;
}


