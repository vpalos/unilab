#ifndef CMDIBASELINE_H
#define CMDIBASELINE_H

#include <fx.h>

#include "cMDIChild.h"
#include "cDLGBslCases.h"
#include "cMDIReadings.h"
#include "cColorTable.h"
#include "cSortList.h"

#define BASELINE_PARCOUNT 30

class cMDIBaseline : public cMDIChild
{
    FXDECLARE(cMDIBaseline);
    
    private:
        FXTextField *tfId;
        cColorTable *table;
        FXText *comments;

        FXdouble vstdev,vpercent;
        FXint cType;
        FXint mType;
        FXbool mcAm;
        FXbool saved;
        FXString name;

        sCasesSet *casesSets;
        int tSetsCount;
        int tCasesCount;
        bool areTiters;
        bool noTiters;
        bool areBins;
        bool areSecondary;
        
        FXint baselineWindow;

    protected:
        cMDIBaseline();
        
        FXbool loadReadings(cSortList &prCases);
        FXbool processReadings(void);
        FXbool askPrefs(void);

        void updateData(void);
        FXbool saveData(void);
        
        void update(void);
        
    public:
        cMDIBaseline(FXMDIClient *prP,const FXString &prName,FXIcon *prIc=NULL,FXPopup *prPup=NULL,FXuint prOpts=0,FXint prX=0,FXint prY=0,FXint prW=0,FXint prH=0);
        virtual ~cMDIBaseline();
        
        virtual void create();
        
        void openCases(FXMDIClient *prP, FXPopup *prPup,cSortList &prCases);
        
        virtual FXbool canClose(void);
        virtual FXbool loadBaseline(const FXString &prId);
        
        enum
        {
            ID_MDIBASELINE=FXMDIChild::ID_LAST,
            
            ID_BSLID,
            ID_BSLTABLE,
            ID_BSLCOMMENTS,
            
            CMD_LCASES,
            CMD_SAVE,
            CMD_PRINT,
            CMD_CLOSE,
            
            ID_LAST
        };

        long onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdTfId(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdTable(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdLCases(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif

