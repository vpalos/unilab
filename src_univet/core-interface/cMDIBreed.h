#ifndef CMDIBREED_H
#define CMDIBREED_H

#include <fx.h>

#include "cMDIChild.h"

class cMDIBreed : public cMDIChild
{
    FXDECLARE(cMDIBreed);
    
    private:
        FXTextField *tfId;
        FXTextField *tfLab;
        FXText *comments;

        FXbool saved;
        FXString name;
    
    protected:
        cMDIBreed();
        
        void updateData(void);
        FXbool saveData(void);
        
    public:
        cMDIBreed(FXMDIClient *prP,const FXString &prName,FXIcon *prIc=NULL,FXPopup *prPup=NULL,FXuint prOpts=0,FXint prX=0,FXint prY=0,FXint prW=0,FXint prH=0);
        virtual ~cMDIBreed();
        
        virtual void create();
        
        virtual FXbool canClose(void);
        virtual FXbool loadBreed(const FXString &prId);
        
        enum
        {
            ID_MDIBREED=FXMDIChild::ID_LAST,
            ID_BREID,
            ID_BRELAB,
            ID_BREADD,
            ID_BRECITY,
            ID_BRESTATE,
            ID_BREZIP,
            ID_BREPHONE,
            ID_BREEMAIL,
            ID_BRECOMMENTS,
            
            CMD_SAVE,
            CMD_PRINT,
            CMD_CLOSE,
            
            ID_LAST
        };

        long onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdTfId(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif

