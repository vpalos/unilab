#include <time.h>
#include <ctype.h>
#include <stdlib.h>

#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cColorsManager.h"
#include "cFormulaRatio.h"
#include "cSpeciesManager.h"
#include "cVeterinarianManager.h"
#include "cCaseManager.h"
#include "cPopulationManager.h"
#include "cWinMain.h"
#include "cMDIVeterinarian.h"
#include "cMDIVeterinarianManager.h"

FXDEFMAP(cMDIVeterinarian) mapMDIVeterinarian[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIVeterinarian::ID_MDIVETERINARIAN,cMDIVeterinarian::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIVeterinarian::ID_VETID,cMDIVeterinarian::onCmdTfId),
    FXMAPFUNC(SEL_CHANGED,cMDIVeterinarian::ID_VETID,cMDIVeterinarian::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIVeterinarian::ID_VETLAB,cMDIVeterinarian::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIVeterinarian::ID_VETADD,cMDIVeterinarian::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIVeterinarian::ID_VETCITY,cMDIVeterinarian::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIVeterinarian::ID_VETSTATE,cMDIVeterinarian::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIVeterinarian::ID_VETZIP,cMDIVeterinarian::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIVeterinarian::ID_VETPHONE,cMDIVeterinarian::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIVeterinarian::ID_VETFAX,cMDIVeterinarian::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIVeterinarian::ID_VETEMAIL,cMDIVeterinarian::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIVeterinarian::ID_VETCOMMENTS,cMDIVeterinarian::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIVeterinarian::CMD_CLOSE,cMDIVeterinarian::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIVeterinarian::CMD_SAVE,cMDIVeterinarian::onCmdSave),
    FXMAPFUNC(SEL_COMMAND,cMDIVeterinarian::CMD_PRINT,cMDIVeterinarian::onCmdPrint),
};

FXIMPLEMENT(cMDIVeterinarian,cMDIChild,mapMDIVeterinarian,ARRAYNUMBER(mapMDIVeterinarian))

cMDIVeterinarian::cMDIVeterinarian()
{
}

void cMDIVeterinarian::updateData(void)
{
    saved=false;
}

FXbool cMDIVeterinarian::saveData(void)
{
    if(saved)
        return true;
    
    if(!name.empty() && cVeterinarianManager::veterinarianSolid(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasevetsolid").text());
        saved=true;
        return 1;
    }
    
    FXString oldname=name;
    name=tfId->getText();
    if(name!=oldname && cVeterinarianManager::veterinarianExists(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
        tfId->setText(cVeterinarianManager::getNewID());
        setTitle(tfId->getText());
        updateData();
        return false;
    }
    this->setTitle(name);
    
    if(oldname.empty())
        cVeterinarianManager::addVeterinarian(name,tfLab->getText(),tfAdd->getText(),tfCity->getText(),tfState->getText(),tfZip->getText(),tfPhone->getText(),tfFax->getText(),tfEmail->getText(),comments->getText());
    else
        cVeterinarianManager::setVeterinarian(oldname,name,tfLab->getText(),tfAdd->getText(),tfCity->getText(),tfState->getText(),tfZip->getText(),tfPhone->getText(),tfFax->getText(),tfEmail->getText(),comments->getText());
    
    saved=true;
    cMDIVeterinarianManager::cmdRefresh();
    return true;
}

cMDIVeterinarian::cMDIVeterinarian(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH) 
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_MDIVETERINARIAN);
    saved=false;
    name="";
    
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL,0,0,0,0);
        new FXLabel(_vframe0,oLanguage->getText("str_vetmdi_id"));
        tfId=new FXTextField(_vframe0,35,this,ID_VETID);
            tfId->setText(prName);
            tfId->setSelection(0,tfId->getText().length());
            tfId->setFocus();

        new FXLabel(_vframe0,oLanguage->getText("str_vetmdi_lab"));
        tfLab=new FXTextField(_vframe0,35,this,ID_VETLAB);
        new FXLabel(_vframe0,oLanguage->getText("str_vetmdi_add"));
        FXVerticalFrame *_vframe7=new FXVerticalFrame(_vframe0,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
        tfAdd=new FXText(_vframe7,this,ID_VETADD,TEXT_WORDWRAP|TEXT_NO_TABS|LAYOUT_FILL);
        new FXLabel(_vframe0,oLanguage->getText("str_vetmdi_city"));
        tfCity=new FXTextField(_vframe0,35,this,ID_VETCITY);
        new FXLabel(_vframe0,oLanguage->getText("str_vetmdi_state"));
        tfState=new FXTextField(_vframe0,35,this,ID_VETSTATE);
        new FXLabel(_vframe0,oLanguage->getText("str_vetmdi_zip"));
        tfZip=new FXTextField(_vframe0,35,this,ID_VETZIP);
        new FXLabel(_vframe0,oLanguage->getText("str_vetmdi_phone"));
        tfPhone=new FXTextField(_vframe0,35,this,ID_VETPHONE);
        new FXLabel(_vframe0,oLanguage->getText("str_vetmdi_fax"));
        tfFax=new FXTextField(_vframe0,35,this,ID_VETFAX);
        new FXLabel(_vframe0,oLanguage->getText("str_vetmdi_email"));
        tfEmail=new FXTextField(_vframe0,35,this,ID_VETEMAIL);
            
        new FXLabel(_vframe0,oLanguage->getText("str_vetmdi_comments"));
        FXVerticalFrame *_vframe8=new FXVerticalFrame(_vframe0,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
        comments=new FXText(_vframe8,this,ID_VETCOMMENTS,TEXT_WORDWRAP|TEXT_NO_TABS|LAYOUT_FILL);
        FXHorizontalFrame *_hframe8=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,10,0);
            //new FXButton(_hframe8,oLanguage->getText("str_vetmdi_print"),new FXGIFIcon(getApp(),data_settings_printer),this,CMD_PRINT,FRAME_RAISED|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH,0,0,100,0);
            new FXButton(_hframe8,oLanguage->getText("str_but_close"),NULL,this,CMD_CLOSE,FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_FIX_WIDTH|LAYOUT_RIGHT,0,0,100,0);
            new FXButton(_hframe8,oLanguage->getText("str_vetmdi_save"),NULL,this,CMD_SAVE,FRAME_RAISED|BUTTON_DEFAULT|BUTTON_INITIAL|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_RIGHT|LAYOUT_FIX_WIDTH,0,0,100,0);
}

cMDIVeterinarian::~cMDIVeterinarian()
{
}

void cMDIVeterinarian::create()
{
    cMDIChild::create();
    show();
    updateData();
}

FXbool cMDIVeterinarian::canClose(void)
{
    if(!saved)
    {
        setFocus();
        int res=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),oLanguage->getText("str_qsave").text());
        switch(res)
        {
            case MBOX_CLICKED_YES:
                if(!saveData())
                    return false;
                break;
            case MBOX_CLICKED_CANCEL:
                return false;
            default:
                break;
        }
    }
    return true;
}

FXbool cMDIVeterinarian::loadVeterinarian(const FXString &prId)
{
    if(!cVeterinarianManager::veterinarianExists(prId))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_openvet").text());
        return false;
    }
    if(!oWinMain->isWindow(prId))
        return oWinMain->raiseWindow(prId);
    
    cDataResult *res=cVeterinarianManager::getVeterinarian(prId);
    if(res==NULL)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_openvet").text());
        return false;
    }
    
    name=res->getCellString(0,0);
    tfId->setText(name);
    tfLab->setText(res->getCellString(0,1));
    tfAdd->setText(res->getCellString(0,3));
    tfCity->setText(res->getCellString(0,4));
    tfState->setText(res->getCellString(0,5));
    tfZip->setText(res->getCellString(0,6));
    tfPhone->setText(res->getCellString(0,7));
    tfFax->setText(res->getCellString(0,8));
    tfEmail->setText(res->getCellString(0,9));
    comments->setText(res->getCellString(0,10));
    
    setTitle(name);
    updateData();
    saved=true;
    return true;
}

long cMDIVeterinarian::onCmdTfId(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    if(!tfId->getText().empty())
    {
        if(!cVeterinarianManager::veterinarianExists(tfId->getText()))
            return 1;
        else
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
    }
    tfId->setText(getTitle());
    return 1;
}

long cMDIVeterinarian::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(canClose())
    {
        close();
        return 1; 
    }
    return 0;
}

long cMDIVeterinarian::onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    return 1;
}

long cMDIVeterinarian::onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData)
{
    saveData();
    if(saved)
        onCmdClose(NULL,0,NULL);
    return 1;
}

long cMDIVeterinarian::onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData)
{
    
    
    // TODO
    
    
    return 1;
}

