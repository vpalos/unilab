#include "engine.h"
#include "graphics.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cDLGReportCC.h"
#include "cBaselineManager.h"

FXDEFMAP(cDLGReportCC) mapDLGReportCC[]=
{
    FXMAPFUNC(SEL_UPDATE,cDLGReportCC::ID_BSLCHK,cDLGReportCC::onUpdBslchk),
    FXMAPFUNC(SEL_COMMAND,cDLGReportCC::ID_UPDATE,cDLGReportCC::onCmdAllio),
    FXMAPFUNC(SEL_UPDATE,cDLGReportCC::ID_UPDATE,cDLGReportCC::onCmdUpdate),
    FXMAPFUNC(SEL_UPDATE,cDLGReportCC::CMD_BUT_APPLY,cDLGReportCC::onUpdApply),
    FXMAPFUNC(SEL_COMMAND,cDLGReportCC::ID_ALLIO,cDLGReportCC::onCmdAllio),
    FXMAPFUNC(SEL_COMMAND,cDLGReportCC::CMD_BUT_APPLY,cDLGReportCC::onCmdApply),
};

FXIMPLEMENT(cDLGReportCC,FXDialogBox,mapDLGReportCC,ARRAYNUMBER(mapDLGReportCC))

cDLGReportCC::cDLGReportCC(FXWindow *prOwner,FXbool areTiters,FXbool areSecondary,FXbool areBins,FXbool areSamples,FXbool areOnlyTs) :
    FXDialogBox(prOwner,oLanguage->getText("str_report_cc")+"...",DECOR_TITLE|DECOR_BORDER),chtype(choice)
{
    areSp=areSamples;
    onlyTs=areOnlyTs;
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL);
        bUseInfo=new FXCheckButton(_vframe0,oLanguage->getText("str_report_showInfo"),this,ID_UPDATE);
        bUseInfo->setTextColor(FXRGB(0,0,200));
        bUseInfo->setCheck(true);
        new FXHorizontalSeparator(_vframe0);
        bUseGraphs=new FXCheckButton(_vframe0,oLanguage->getText("str_report_showGraph"),this,ID_UPDATE);
        bUseGraphs->setCheck(true);
        bUseGraphs->setTextColor(FXRGB(0,0,200));
        choice=3;
        FXGroupBox *_gb1=new FXGroupBox(_vframe0,(char*)NULL,GROUPBOX_NORMAL,0,0,0,0,17,0,0,0,0,0);
            if(areTiters)
            {
                bTitGraphs=new FXRadioButton(_gb1,oLanguage->getText("str_report_titGraph"),&chtype,FXDataTarget::ID_OPTION+1);
                bTitGraphs->setTextColor(FXRGB(120,120,120));
                choice=1;
            }
            else
                bTitGraphs=NULL;
            if(areBins)
            {
                bBinGraphs=new FXRadioButton(_gb1,oLanguage->getText("str_report_binGraph"),&chtype,FXDataTarget::ID_OPTION+2);
                bBinGraphs->setTextColor(FXRGB(120,120,120));
                if(!areTiters)
                    choice=2;
            }
            else
                bBinGraphs=NULL;
            if(areTiters)
            {
                bTsGraphs=new FXRadioButton(_gb1,oLanguage->getText("str_report_tsGraph"),&chtype,FXDataTarget::ID_OPTION+4);
                bTsGraphs->setTextColor(FXRGB(120,120,120));
                choice=1;
            }
            else
                bTsGraphs=NULL;
            bSpGraphs=new FXRadioButton(_gb1,oLanguage->getText("str_report_spGraph"),&chtype,FXDataTarget::ID_OPTION+3);
            bSpGraphs->setTextColor(FXRGB(120,120,120));
            new FXHorizontalSeparator(_gb1,SEPARATOR_NONE|LAYOUT_FIX_HEIGHT,0,0,0,10);
            bcSingle=new FXCheckButton(_gb1,oLanguage->getText("str_report_ccsingle"),this,ID_ALLIO);
            bcSingle->setTextColor(FXRGB(0,0,0));
        new FXHorizontalSeparator(_vframe0);
        bUseData=new FXCheckButton(_vframe0,oLanguage->getText("str_report_showStats"),this,ID_UPDATE);
        bUseData->setTextColor(FXRGB(0,0,200));
        bUseData->setCheck(true);
        FXVerticalFrame *_gb0=new FXVerticalFrame (_vframe0,LAYOUT_FILL_X,0,0,0,0,17);
            FXVerticalFrame *_vframe1=new FXVerticalFrame(_gb0,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
                bcCala=new FXCheckButton(_vframe1,oLanguage->getText("str_asymdi_cala"));
                bcCala->setTextColor(FXRGB(120,120,120));
                bcCala->setCheck(true);
                if(areSecondary)
                {
                    bcCalb=new FXCheckButton(_vframe1,oLanguage->getText("str_asymdi_calb"));
                    bcCalb->setCheck(true);
                    bcCalb->setTextColor(FXRGB(120,120,120));
                }
                else
                    bcCalb=NULL;
                bcEU=new FXCheckButton(_vframe1,oLanguage->getText("str_rdgmdi_eu"));
                bcEU->setTextColor(FXRGB(120,120,120));
                if(areTiters)
                {
                    bcTiter=new FXCheckButton(_vframe1,oLanguage->getText("str_asymdi_titer"));
                    bcTiter->setCheck(true);
                    bcTiter->setTextColor(FXRGB(120,120,120));
                    bcLog2=new FXCheckButton(_vframe1,oLanguage->getText("str_rdgmdi_log2"));
                    bcLog2->setTextColor(FXRGB(120,120,120));
                }
                else
                {
                    bcTiter=NULL;
                    bcLog2=NULL;
                }
        new FXHorizontalSeparator(_vframe0);
        bcAm=new FXCheckButton(_vframe0,oLanguage->getText("str_rdgmdi_mean"));
        bcAm->setCheck(true);
        bcAm->setTextColor(FXRGB(0,0,200));
        new FXHorizontalSeparator(_vframe0);

        if(areTiters)
        {
            FXLabel *_lb0=new FXLabel(_vframe0,oLanguage->getText("str_report_bsl"));
                _lb0->setTextColor(FXRGB(0,120,0));
            FXVerticalFrame *_gb30=new FXVerticalFrame (_vframe0,LAYOUT_FILL_X,0,0,0,0,17,0,0,0);
                FXVerticalFrame *_gb31=new FXVerticalFrame (_gb30,LAYOUT_FILL_X,0,0,0,0,2,0,0,0);
                    bsl=new FXListBox(_gb31,NULL,0,FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X);
                    bsl->setTextColor(FXRGB(0,120,0));
                    bsl->appendItem(oLanguage->getText("str_report_bslno"));
                    sBaselineObject *res=cBaselineManager::listBaselines();
                    if(res==NULL)
                        return;
                    for(int i=0;i<cBaselineManager::getBaselineCount();i++)
                        bsl->appendItem(*res[i].id);
                    free(res);
                    bsl->setNumVisible(bsl->getNumItems()>7?7:bsl->getNumItems());
                bSS=new FXCheckButton(_gb30,oLanguage->getText("str_report_bslss"),this,ID_BSLCHK);
                bSS->setTextColor(FXRGB(0,120,0));
    
            new FXHorizontalSeparator(_vframe0);
        }
        else
            bsl=NULL;
        
        FXHorizontalFrame *_hframe100=new FXHorizontalFrame(_vframe0,LAYOUT_CENTER_X);
        butCancel=new FXButton(_hframe100,oLanguage->getText("str_but_cancel"),NULL,this,ID_CANCEL,BUTTON_DEFAULT|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
        butAccept=new FXButton(_hframe100,oLanguage->getText("str_but_ok"),NULL,this,CMD_BUT_APPLY,BUTTON_DEFAULT|BUTTON_INITIAL|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
}

cDLGReportCC::~cDLGReportCC()
{
}

long cDLGReportCC::onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData)
{
    handle(NULL,FXSEL(SEL_COMMAND,ID_ACCEPT),NULL);
    return 1;
}

long cDLGReportCC::onCmdAllio(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if((areSp || onlyTs) && bcSingle->getCheck())
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_report_ccalliosp").text());
        bcSingle->setCheck(false);
    }
    return 1;
}

long cDLGReportCC::onUpdApply(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(bUseGraphs->getCheck() || bUseInfo->getCheck() || bUseData->getCheck())
        butAccept->enable();
    else
        butAccept->disable();
    return 1;
}

long cDLGReportCC::onCmdUpdate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(bUseGraphs->getCheck())
    {
        if(bTitGraphs)
        {
            bTitGraphs->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
            bTsGraphs->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        }
        if(bBinGraphs)
            bBinGraphs->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bSpGraphs->getCheck() || (bTsGraphs && bTsGraphs->getCheck()))
        {
            bcSingle->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
            bcSingle->setCheck(false);
        }
        else
            bcSingle->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        bSpGraphs->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    else
    {
        if(bTitGraphs)
        {
            bTitGraphs->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
            bTsGraphs->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        }
        if(bBinGraphs)
            bBinGraphs->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        bcSingle->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        bSpGraphs->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }
    if(bUseData->getCheck())
    {
        bcCala->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bcCalb)
            bcCalb->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bcTiter)
            bcTiter->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bcLog2)
            bcLog2->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        bcEU->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    else
    {
        bcCala->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        if(bcCalb)
            bcCalb->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        if(bcTiter)
            bcTiter->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        if(bcLog2)
            bcLog2->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        bcEU->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }
    return 1;
}

long cDLGReportCC::onUpdBslchk(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!bUseGraphs->getCheck() || (choice!=1 && choice!=4))
    {
        bsl->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        bSS->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }
    else
    {
        bsl->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bsl->getCurrentItem()==0)
            bSS->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        else
            bSS->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    return 1;
}

