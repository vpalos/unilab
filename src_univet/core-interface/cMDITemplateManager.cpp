#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cTemplateManager.h"
#include "cWinMain.h"
#include "cMDIDatabaseManager.h"
#include "cMDITemplateManager.h"

cMDITemplateManager *oMDITemplateManager=NULL;

FXDEFMAP(cMDITemplateManager) mapMDITemplateManager[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDITemplateManager::ID_TEMPLATEMANAGER,cMDITemplateManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDITemplateManager::CMD_BUT_CLOSE,cMDITemplateManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDITemplateManager::CMD_BUT_OPEN,cMDITemplateManager::onCmdOpen),
    FXMAPFUNC(SEL_DOUBLECLICKED,cMDITemplateManager::ID_TEMPLATELIST,cMDITemplateManager::onCmdOpen),
    FXMAPFUNC(SEL_COMMAND,cMDITemplateManager::CMD_BUT_NEW,cMDITemplateManager::onCmdNew),
    FXMAPFUNC(SEL_COMMAND,cMDITemplateManager::CMD_BUT_DUPLICATE,cMDITemplateManager::onCmdDuplicate),
    FXMAPFUNC(SEL_COMMAND,cMDITemplateManager::CMD_BUT_REMOVE,cMDITemplateManager::onCmdRemove),
    
};

FXIMPLEMENT(cMDITemplateManager,cMDIChild,mapMDITemplateManager,ARRAYNUMBER(mapMDITemplateManager))

cMDITemplateManager::cMDITemplateManager()
{
}

cMDITemplateManager::cMDITemplateManager(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH) 
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_TEMPLATEMANAGER);
    client=prP;
    popup=prPup;
    
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(this,LAYOUT_FILL);
    FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe0,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
    new FXLabel(_vframe2,oLanguage->getText("str_tplman_tpllist_title"));
    FXHorizontalFrame *_hframe1=new FXHorizontalFrame(_vframe2,LAYOUT_FILL,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe4=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|LAYOUT_RIGHT,0,0,0,0,0,0,0,0,0,0);
    
    tplList=new cSortList(_vframe3,this,ID_TEMPLATELIST,ICONLIST_SINGLESELECT|ICONLIST_DETAILED|LAYOUT_FILL);
    tplList->appendHeader(oLanguage->getText("str_tplman_tplnameh"),NULL,264);
    tplList->appendHeader(oLanguage->getText("str_tplman_tpldateh"),NULL,120);
    oMDITemplateManager=this;
    this->update();
    if(tplList->getNumItems()>0)
    {
        tplList->selectItem(0);
        tplList->setCurrentItem(0);
    }
    tplList->setFocus();

    new FXButton(_vframe4,oLanguage->getText("str_tplman_new"),new FXGIFIcon(getApp(),data_new_template),this,CMD_BUT_NEW,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FIX_WIDTH,0,0,88);
    new FXHorizontalSeparator(_vframe4,LAYOUT_TOP|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_tplman_open"),new FXGIFIcon(getApp(),data_browse_templates),this,CMD_BUT_OPEN,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X);
    new FXButton(_vframe4,oLanguage->getText("str_tplman_duplicate"),NULL,this,CMD_BUT_DUPLICATE,LAYOUT_TOP|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
    new FXButton(_vframe4,oLanguage->getText("str_but_close"),NULL,this,CMD_BUT_CLOSE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
    new FXHorizontalSeparator(_vframe4,LAYOUT_BOTTOM|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_tplman_remove"),NULL,this,CMD_BUT_REMOVE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
}

cMDITemplateManager::~cMDITemplateManager()
{
    cMDITemplateManager::unload();
}

void cMDITemplateManager::create()
{
    cMDIChild::create();
    show();
}

void cMDITemplateManager::load(FXMDIClient *prP,FXPopup *prMenu)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(prP,prMenu);
        return;
    }
    
    if(oMDITemplateManager!=NULL)
    {
        oMDITemplateManager->setFocus();
        prP->setActiveChild(oMDITemplateManager);
        if(oMDITemplateManager->isMinimized())
            oMDITemplateManager->restore();
        return;
    }
    new cMDITemplateManager(prP,oLanguage->getText("str_tplman_title"),new FXGIFIcon(oApplicationManager,data_browse_templates),prMenu,MDI_NORMAL|MDI_TRACKING,10,10,515,350);
    oMDITemplateManager->create();
    oMDITemplateManager->setFocus();

}

FXbool cMDITemplateManager::isLoaded(void)
{
    return (oMDITemplateManager!=NULL);
}

void cMDITemplateManager::unload(void)
{
    oMDITemplateManager=NULL;
}

void cMDITemplateManager::cmdNew(void)
{
    if(!isLoaded())
        return;
    oMDITemplateManager->onCmdNew(NULL,0,NULL);
}

void cMDITemplateManager::update(void)
{
    if(!isLoaded())
        return;
    sTemplateObject *res=cTemplateManager::listTemplates();
    if(res==NULL)
        return;
    tplList->clearItems();
    for(int i=0;i<cTemplateManager::getTemplateCount();i++)
        tplList->appendItem(*(res[i].title)+"\t"+*(res[i].date));
    tplList->sortItems();
    free(res);
}

void cMDITemplateManager::cmdRefresh(void)
{
    if(!isLoaded())
        return;
    oMDITemplateManager->update();
}

long cMDITemplateManager::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDITemplateManager::unload();
    close();
    return 1;
}

long cMDITemplateManager::onCmdNew(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cTemplateManager::newTemplate(client,popup);
    return 1;
}

long cMDITemplateManager::onCmdOpen(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int db=-1,i;
    for(i=0;i<tplList->getNumItems();i++)
        if(tplList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        db=tplList->getCurrentItem();
    if(db==-1)
        return 1;
    FXString st=tplList->getItemText(db).before('\t');
    if(oWinMain->isWindow(st))
        oWinMain->raiseWindow(st);
    else
        cTemplateManager::editTemplate(client,popup,st);
    return 1;
}

long cMDITemplateManager::onCmdDuplicate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int db=-1,i,j;
    for(i=0;i<tplList->getNumItems();i++)
        if(tplList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        return 1;
    sTemplateObject *res=cTemplateManager::listTemplates();
    FXString res2="";
    if(res==NULL)
        return 1;
    for(i=0;i<cTemplateManager::getTemplateCount();i++)
    {
        if(strcasecmp(res[i].title->text(),tplList->getItemText(db).before('\t').text())==0)
        {
            res2=cTemplateManager::duplicateTemplate(*(res[i].title));
            this->update();
            break;                                                    
        }
    }
    free(res);
    if(res2.empty())
        return 1;
    for(j=0;j<tplList->getNumItems();j++)
        if(res2==tplList->getItemText(j).before('\t'))
        {
            tplList->selectItem(j);
            break;
        }
    return 1;
}

long cMDITemplateManager::onCmdRemove(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i,db=-1;
    for(i=0;i<tplList->getNumItems();i++)
        if(tplList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        return 1;
    if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_erasetpl").text()))
        return 1;
    sTemplateObject *res=cTemplateManager::listTemplates();
    if(res==NULL)
        return 1;
    cMDIChild *win=oWinMain->getWindow(tplList->getItemText(db).before('\t'));
    if(win!=NULL)
        win->close();
    cTemplateManager::removeTemplate(tplList->getItemText(db).before('\t'));
    this->update();
    if(db>=tplList->getNumItems())
        db=tplList->getNumItems()-1;
    if(db>=0)
        tplList->selectItem(db);
    free(res);
    return 1;
}


