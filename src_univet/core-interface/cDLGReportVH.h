#ifndef CDLGREPORTVH_H
#define CDLGREPORTVH_H

#include <fx.h>

class cDLGReportVH : public FXDialogBox
{
    FXDECLARE(cDLGReportVH);
    private:
        FXButton *butAccept;
        FXButton *butCancel;
        
    protected:

        cDLGReportVH(){}
        
    public:
        FXuint choice;
        FXDataTarget chtype;
        FXCheckButton *bcCala;
        FXCheckButton *bcCalb;
        FXCheckButton *bcTiter;
        FXCheckButton *bcAm;
        FXCheckButton *bcLog2;
        FXCheckButton *bcEU;
        
        FXCheckButton *bUseData;
        FXCheckButton *bUseInfo;
    
        FXRadioButton *bTitGraphs;
        FXRadioButton *bSpGraphs;
    
        FXuint agchoice;
        FXDataTarget agtype;
        FXRadioButton *bAMean;
        FXRadioButton *bGMean;
        
        cDLGReportVH(FXWindow *prOwner,FXbool areTiters,FXbool areSecondary);
        virtual ~cDLGReportVH();
        
        enum
        {
            ID_THIS=FXDialogBox::ID_LAST,
            ID_UPDATE,
            
            CMD_BUT_APPLY,
            ID_LAST
        };

        long onCmdUpdate(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdApply(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
