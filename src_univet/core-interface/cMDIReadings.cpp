#include <time.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include <fxkeys.h>
#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cColorsManager.h"
#include "cFormulaRatio.h"
#include "cReadingsManager.h"
#include "cDLGCasePopEdit.h"
#include "cCaseManager.h"
#include "cSortList.h"
#include "cPopulationManager.h"
#include "cCaseAnalysisReport.h"
#include "cCaseCompareReport.h"
#include "cCaseCombineReport.h"
#include "cCaseSbsReport.h"
#include "cTotalCountsReport.h"
#include "cTiterBreakdownReport.h"
#include "cVaccineHistoryReport.h"
#include "cVaccineDateReport.h"
#include "cTemplateManager.h"
#include "cMDIReadings.h"
#include "cMDICaseManager.h"
#include "cWinMain.h"

cLocInfo::cLocInfo(int prCaseIndex,int prSampleLoc,int prPlate,int prSet)
{
    this->caseIndex=prCaseIndex;
    this->sampleLoc=prSampleLoc;
    this->plate=prPlate;
    this->isValue=false;
    this->set=prSet;
}

cLocInfo::~cLocInfo()
{
}

void cLocInfo::setValue(double prValue)
{
    isValue=true;
    value=prValue;
}

FXDEFMAP(cMDIReadings) mapMDIReadings[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIReadings::ID_MDIREADINGS,cMDIReadings::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::CMD_CLOSE,cMDIReadings::onCmdClose),

    FXMAPFUNC(SEL_COMMAND,cMDIReadings::CMD_CHPOINT,cMDIReadings::onCmdChPoint),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::CMD_CHLINE,cMDIReadings::onCmdChLine),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::CMD_CHBAR,cMDIReadings::onCmdChBar),

    FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_COPY,cMDIReadings::onCopyPlateTable),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_PASTE,cMDIReadings::onPastePlateTable),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_SELALL,cMDIReadings::onSelallPlateTable),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_SHOWRULES,cMDIReadings::onCmdShowRules),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::CMD_SAVE,cMDIReadings::onCmdSave),

    FXMAPFUNC(SEL_UPDATE,cMDIReadings::ID_CALCMENU,cMDIReadings::onCmdCalcSel),
    FXMAPFUNC(SEL_UPDATE,cMDIReadings::ID_SHOWSTATS,cMDIReadings::onCmdShowStats),

    FXMAPFUNC(SEL_CONFIGURE,cMDIReadings::ID_PLATETABLE1,cMDIReadings::onRszPlateTable1),
    FXMAPFUNC(SEL_SELECTED,cMDIReadings::ID_PLATETABLE1,cMDIReadings::onUpdSelPlateTable1),
    //FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_PLATETABLE1,cMDIReadings::onCmdPlateTable1),
    FXMAPFUNC(SEL_REPLACED,cMDIReadings::ID_PLATETABLE1,cMDIReadings::onCmdPlateTable1),

    FXMAPFUNC(SEL_SELECTED,cMDIReadings::ID_PLATETABLE2,cMDIReadings::onUpdSelPlateTable2),
    //FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_PLATETABLE2,cMDIReadings::onCmdPlateTable2),
    FXMAPFUNC(SEL_REPLACED,cMDIReadings::ID_PLATETABLE2,cMDIReadings::onCmdPlateTable2),

    FXMAPFUNC(SEL_CONFIGURE,cMDIReadings::ID_CASELIST1,cMDIReadings::onRszCaseList1),
    FXMAPFUNC(SEL_SELECTED,cMDIReadings::ID_CASELIST1,cMDIReadings::onCmdSelCaseList1),
    FXMAPFUNC(SEL_CONFIGURE,cMDIReadings::ID_CASELIST2,cMDIReadings::onRszCaseList2),
    FXMAPFUNC(SEL_SELECTED,cMDIReadings::ID_CASELIST2,cMDIReadings::onCmdSelCaseList2),

    FXMAPFUNC(SEL_COMMAND,cMDIReadings::CMD_REREAD,cMDIReadings::onCmdReread),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::CMD_TEMPLATE,cMDIReadings::onCmdTemplate),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::CMD_CASEEDIT,cMDIReadings::onCmdCaseEdit),

    FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_REP_CA,cMDIReadings::onCmdReport),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_REP_CC,cMDIReadings::onCmdReport),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_REP_CM,cMDIReadings::onCmdReport),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_REP_SB,cMDIReadings::onCmdReport),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_REP_TC,cMDIReadings::onCmdReport),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_REP_TB,cMDIReadings::onCmdReport),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_REP_VH,cMDIReadings::onCmdReport),
    FXMAPFUNC(SEL_COMMAND,cMDIReadings::ID_REP_VD,cMDIReadings::onCmdReport),

    //FXMAPFUNC(SEL_KEYPRESS,0,cMDIReadings::onEvtRedir),
    //FXMAPFUNC(SEL_KEYRELEASE,0,cMDIReadings::onEvtRedir),
};

FXIMPLEMENT(cMDIReadings,cMDIChild,mapMDIReadings,ARRAYNUMBER(mapMDIReadings))

cMDIReadings::cMDIReadings()
{
}

void cMDIReadings::sortControls(int *prData,int prLength,int prDesc)
{
    int parcurgeri=1,a,b;
    bool schimbat=true;
    while(schimbat)
    {
        schimbat=false;
        for(int i=0;i<prLength-parcurgeri;i++)
        {
            a=abs(prData[i])%1000;
            b=abs(prData[i+1])%1000;
            if((a>b && !prDesc) || (a<b && prDesc))
            {
                a=prData[i+1];
                prData[i+1]=prData[i];
                prData[i]=a;
                schimbat=true;
            }
        }
        parcurgeri++;
    }
}

FXString cMDIReadings::sampleKey(int prIndex)
{
    FXString ret="";
    int diff='z'-'a'+1;
    while(prIndex>=0)
    {
        ret=FXStringFormat("%c",'a'+prIndex%diff)+ret;
        prIndex/=diff;
        prIndex--;
    }
    return ret;
}

FXString cMDIReadings::sampleVal(double prValue)
{
    return prValue>4.000?oLanguage->getText("str_rdgmdi_over"):FXStringFormat("%g",round(prValue*1000)/1000);
}

FXString cMDIReadings::calculVal(double prValue)
{
    double no=round(prValue*1000)/1000;
    FXString val=FXStringFormat("%.13g",no);
    if(prValue==-9999 || val.lower()=="inf" || val.lower()=="nan" || no>999999999.999)
        return oLanguage->getText("str_invalid");
    return val;
}

FXString cMDIReadings::displayPosition(int prPosition,bool prAlelisa,int prOrientation,bool prAlelisaValue)
{
    int pos=platePosition(prPosition,prAlelisa,prOrientation,false);
    return FXStringFormat("%c%d",'A'+pos/12,pos%12+1);
}

int cMDIReadings::platePosition(int prPosition,bool prAlelisa,int prOrientation,bool prAlelisaValue)
{
    if(prOrientation==2)
        return prPosition;
    if(prAlelisa)
        return (prPosition/8)*2+12*(prPosition%8)+(prAlelisaValue?1:0);
    return prPosition/8+12*(prPosition%8);
}

bool cMDIReadings::isControlPosition(int prPosition,int *prControls,int prControlsCount,bool prAlelisa)
{
    if(prAlelisa)
        prPosition=(prPosition/8)*16+(prPosition%8);
//fxmessage("> (%d) ", prPosition);
    for(int i=0;i<prControlsCount;i++)
    {
//fxmessage("%d,",prControls[i]);
        if(abs(prControls[i])==prPosition)
            return true;
        if(abs(prControls[i])==1000 && prPosition==0)
            return true;
    }
//fxmessage("\n");
    return false;
}

void cMDIReadings::appendControls(double *prDest,double *prSrc,int *prControls,int prControlsCount,int prOrientation,bool prAlelisa)
{
    int j,controli=0;
    double val;
    for(int i=0;i<prControlsCount;i++)
    {
        j=abs(prControls[i])==1000?0:abs(prControls[i]);
        val=prSrc[platePosition(j,false,prOrientation)];
        prDest[controli++]=val<0?0.000:val;
        if(prAlelisa)
        {
            val=prSrc[platePosition(j,false,prOrientation)+1];
            prDest[controli++]=val<0?0.000:val;
        }
    }
}

void cMDIReadings::drawPlateTables(void)
{
    if(!tPlatesCount)
        return;
    int platesgi=0;
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        for(int platesi=0;platesi<casesSets[setsi].platesCount;platesi++,platesgi++)
        {
            for(int i=platesgi*8;i<(platesgi+1)*8;i++)
                for(int j=0;j<12;j++)
                {
                    plateTable1->setItemJustify(i,j,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                    plateTable1->setItemText(i,j,(char*)NULL);
                    plateTable1->setItemData(i,j,NULL);
                    plateTable1->setCellEditable(i,j,false);
                    plateTable1->setCellColor(i,j,FXRGB(255,255,255));
                    if(j%2)
                    {
                        if(casesSets[0].alelisa && casesSets[0].orientation==1)
                            plateTable1->setItemStipple(i,j,STIPPLE_GRAY);
                        else
                            plateTable1->setItemStipple(i,j,STIPPLE_NONE);
                    }
                    plateTable1->setItemBorders(i,j,(i>0 && i%8==0?FXTableItem::TBORDER:0)|
                                                (casesSets[setsi].orientation==1?FXTableItem::RBORDER|
                                                 ((i+1)%8==0?FXTableItem::BBORDER:0):FXTableItem::BBORDER));
                }
        }
    }
    bool p2_filled=plateTable2->getNumRows()>0?true:false;
    int nc=1;
    if(mcCala->getCheck())
        nc++;
    if(mcCalb->getCheck())
        nc++;
    if(mcEU->getCheck())
        nc++;
    if(mcTiter->getCheck())
        nc++;
    if(mcLog2->getCheck())
        nc++;
    if(mcTiters->getCheck())
        nc++;
    if(mcAge->getCheck())
        nc++;
    if(mcBreed1->getCheck())
        nc++;
    if(mcBreed2->getCheck())
        nc++;
    if(mcBins->getCheck())
        nc++;
    //if(mcBins2->getCheck())
    //    nc++;
    if(mcResult->getCheck())
        nc++;
    if(mcResult2->getCheck())
        nc++;
    mcAmCheck=mcAm->getCheck();

    if(p2_filled && (nc!=plateTable2->getNumColumns()))
    {
        if(nc>plateTable2->getNumColumns())
            plateTable2->insertColumns(1,nc-plateTable2->getNumColumns());
        else
            plateTable2->removeColumns(1,plateTable2->getNumColumns()-nc);
        plateTable2->layout();
        for(int i=0;i<plateTable2->getNumRows();i++)
        {
            for(int j=0;j<nc;j++)
            {
                plateTable2->setItemJustify(i,j,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                plateTable2->setCellEditable(i,j,j>0?false:true);
            }
            plateTable2->getRowHeader()->setItemJustify(i,JUSTIFY_CENTER_X);
        }
    }
    if(!p2_filled)
    {
        plateTable2->setTableSize(plateTable2->getNumRows(),nc);
        plateTable2->layout();
    }

    for(int i=0;i<plateTable2->getNumRows();i++)
    {
        for(int j=0;j<plateTable2->getNumColumns();j++)
            plateTable2->setItemText(i,j,(char*)NULL);
        ((cLocInfo*)plateTable2->getItemData(i,0))->isValue=false;
    }
    for(int i=0;i<nc;i++)
        plateTable2->getColumnHeader()->setItemJustify(i,JUSTIFY_CENTER_X);
    plateTable2->getColumnHeader()->setItemText(0,oLanguage->getText("str_rdgmdi_od"));
    int kc=1,ps=selectedCase>=0?((sCaseEntry*)caseList1->getItemData(selectedCase,0))->parentSet:0;
    if(mcCala->getCheck())
        plateTable2->getColumnHeader()->setItemText(kc++,selectedCase>=0?casesSets[ps].cala->text():oLanguage->getText("str_rdgmdi_cala"));
    if(mcCalb->getCheck())
        plateTable2->getColumnHeader()->setItemText(kc++,selectedCase>=0 && casesSets[ps].useSecond?casesSets[ps].calb->text():oLanguage->getText("str_rdgmdi_calb"));
    if(mcEU->getCheck())
        plateTable2->getColumnHeader()->setItemText(kc++,oLanguage->getText("str_rdgmdi_eu"));
    if(mcTiter->getCheck())
        plateTable2->getColumnHeader()->setItemText(kc++,oLanguage->getText("str_asymdi_titer"));
    if(mcLog2->getCheck())
        plateTable2->getColumnHeader()->setItemText(kc++,oLanguage->getText("str_rdgmdi_log2"));
    if(mcTiters->getCheck())
        plateTable2->getColumnHeader()->setItemText(kc++,oLanguage->getText("str_asymdi_titers"));
    if(mcBins->getCheck())
        plateTable2->getColumnHeader()->setItemText(kc++,oLanguage->getText("str_asymdi_bins"));
    //if(mcBins2->getCheck())
    //    plateTable2->getColumnHeader()->setItemText(kc++,oLanguage->getText("str_asymdi_bins2"));
    if(mcAge->getCheck())
        plateTable2->getColumnHeader()->setItemText(kc++,oLanguage->getText("str_rdgmdi_clage"));
    if(mcBreed1->getCheck())
        plateTable2->getColumnHeader()->setItemText(kc++,oLanguage->getText("str_dlg_pop_breed1"));
    if(mcBreed2->getCheck())
        plateTable2->getColumnHeader()->setItemText(kc++,oLanguage->getText("str_dlg_pop_breed2"));
    if(mcResult->getCheck())
        plateTable2->getColumnHeader()->setItemText(kc++,oLanguage->getText("str_rdgmdi_result"));
    if(mcResult2->getCheck())
        plateTable2->getColumnHeader()->setItemText(kc++,oLanguage->getText("str_rdgmdi_result2"));

    for(int j=0;j<12;j++)
    {
        plateTable1->setItemBorders(platesgi*8-1,j,plateTable1->getItemBorders(platesgi*8-1,j)|FXTableItem::BBORDER);
        plateTable1->getColumnHeader()->setItemJustify(j,JUSTIFY_CENTER_X);
        plateTable1->getColumnHeader()->setItemText(j,FXStringFormat("%d",j+1));
    }
    for(int j=0;j<plateTable1->getNumRows();j++)
    {
        plateTable1->getRowHeader()->setItemJustify(j,JUSTIFY_CENTER_X);
        plateTable1->getRowHeader()->setItemText(j,FXStringFormat("P%d:%c",j/8+1,j%8+'A'));
    }

    statsTable->removeColumns(0,statsTable->getNumColumns());

    char replicates[10];
    bool __alelisa=false;
    int __alelisa_multiplier=1;
    int k,l,offset,m;
    int platesi=0;
    int platesci=0;
    int datai=0;
    int samplei=0;
    int casesgi=0;
    int controlsi=0;
    int casepi=0;
    int p2_datai=0;
    int deltasi=0;
    int replicatesi=0;
    int result;
    FXdouble statsBase[5][5];
    double *statsValues[5];
    FXint statsCount[5]={0,0,0,0,0};
    for(int i=0;i<5;i++)
        for(int j=0;j<5;j++)
            statsBase[i][j]=0;
    if(selectedCase>=0)
    {
        int ctui=((sCaseEntry*)caseList1->getItemData(selectedCase,0))->count*(!mcAm->getCheck()?(((sCaseEntry*)caseList1->getItemData(selectedCase,0))->replicates+1):1);
        for(int ui=0;ui<5;ui++)
        {
            statsValues[ui]=(double*)malloc(ctui*sizeof(double));
            if(!statsValues[ui])
            {
                tPlatesCount=0;
                tSetsCount=0;
                tCasesCount=0;
                saved=true;
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                close();
                return;
            }
            statsBase[ui][0]=999999;
            statsBase[ui][4]=ctui;
        }
    }
    int scPos=-1,stp=0,sts=0,stn=0,ste=0;
    FXbool foundSc=false;
    double *ctData=NULL,n,val;
    FXString invalidPlates="";
    invalidPlatesDetails="";
    int dp,rp,pd=0,diff=0,spi=-1,cplatei,splatei,cct=0;
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        spi=-1;
        splatei=-1;
        diff=0;
        __alelisa=casesSets[setsi].alelisa && casesSets[setsi].orientation==1?true:false;
        __alelisa_multiplier=__alelisa?2:1;
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++,casesgi++,cct++)
        {
            FXDict *cv=charts[cct]->dataSet();
            cv->clear();
            cplatei=0;
            int bct[31],tct[31],sct=0;
            int chtype=0;
            for(int h=0;h<31;h++)
                bct[h]=tct[h]=0;
            samplei=0;
            for(int g=spi+1;g<casesSets[setsi].cases[casei].startPlate;g++,diff++,splatei++);
            spi=casesSets[setsi].cases[casei].startPlate;
            while(samplei<casesSets[setsi].cases[casei].count*__alelisa_multiplier)
            {
                deltasi=0;
                replicatesi=0;
                replicates[0]=0;
                while(replicatesi<=casesSets[setsi].cases[casei].replicates)
                {
                    platesi=datai/(96/__alelisa_multiplier);  // PROBLEM
                    if(platesci==platesi)
                    {
                        splatei++;
                        if(samplei)cplatei++;
                        ctData=casesSets[setsi].cases[casei].controlsData+cplatei*casesSets[setsi].controlsPerPlate*__alelisa_multiplier;

                        if(p2_datai>0)
                            for(int xi=0;xi<nc;xi++)
                                plateTable2->setItemBorders(p2_datai-1,xi,plateTable2->getItemBorders(p2_datai-1,xi)|FXTableItem::BBORDER);

                        double na=0,pa=0,nha=0,pha=0,pc=0,nc=0;
                        for(int cswitchi=0;cswitchi<2;cswitchi++)
                        {
                            controlsi=0;
                            for(int i=0;i<casesSets[setsi].controlsPerPlate;i++)
                            {
                                if(((!cswitchi) && (casesSets[setsi].controlsLayout[i]>=0)) ||
                                   (cswitchi && (casesSets[setsi].controlsLayout[i]<0)))
                                {
                                    controlsi+=__alelisa?2:1;
                                    continue;
                                }

                                if(casesSets[setsi].controlsLayout[i]>=0)
                                    {pa+=ctData[controlsi];pc++;}
                                if(casesSets[setsi].controlsLayout[i]<0)
                                    {na+=ctData[controlsi];nc++;}

                                m=platePosition(abs(casesSets[setsi].controlsLayout[i])==1000?0:abs(casesSets[setsi].controlsLayout[i]),false,casesSets[setsi].orientation);
                                l=m%12;
                                k=platesi*8+m/12;
                                plateTable1->setItemText(k,l,sampleVal(ctData[controlsi]));
                                plateTable1->setItemData(k,l,new cLocInfo(casesSets[setsi].controlsLayout[i]>=0?-2:-3,controlsi,splatei,setsi));
                                if(casesSets[setsi].controlsLayout[i]<0)
                                    plateTable1->setCellColor(k,l,FXRGB(200,200,200));
                                else
                                    plateTable1->setCellColor(k,l,FXRGB(255,90,90));
                                plateTable1->setItemBorders(k,l,FXTableItem::LBORDER|FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
                                plateTable1->setCellEditable(k,l,true);
                                if(k>0)plateTable1->setItemBorders(k-1,l,plateTable1->getItemBorders(k-1,l)|FXTableItem::BBORDER);
                                if(l>0)plateTable1->setItemBorders(k,l-1,plateTable1->getItemBorders(k,l-1)|FXTableItem::RBORDER);
                                if((k+1)<plateTable1->getNumRows())plateTable1->setItemBorders(k+1,l,plateTable1->getItemBorders(k+1,l)|FXTableItem::TBORDER);
                                if(l<11)plateTable1->setItemBorders(k,l+1,plateTable1->getItemBorders(k,l+1)|FXTableItem::LBORDER);

                                if(!p2_filled)
                                {
                                    plateTable2->insertRows(p2_datai);
                                    plateTable2->getRowHeader()->setItemJustify(p2_datai,JUSTIFY_CENTER_X);
                                    plateTable2->getRowHeader()->setItemText(p2_datai,FXStringFormat("P%d:%c%d",platesi+1,'A'+m/12,l+1));
                                    for(int xi=0;xi<nc;xi++)
                                    {
                                        plateTable2->setItemJustify(p2_datai,xi,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                                        plateTable2->setItemStipple(p2_datai,xi,STIPPLE_NONE);
                                        plateTable2->setCellEditable(p2_datai,xi,xi>0?false:true);
                                    }
                                    plateTable2->setItemBorders(p2_datai,0,FXTableItem::LBORDER|FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
                                    if(casesSets[setsi].controlsLayout[i]<0)
                                        plateTable2->setCellColor(p2_datai,0,FXRGB(200,200,200));
                                    else
                                        plateTable2->setCellColor(p2_datai,0,FXRGB(255,90,90));
                                }
                                plateTable2->setItemText(p2_datai,0,calculVal(ctData[controlsi]));
                                plateTable2->setItemData(p2_datai,0,new cLocInfo(casesSets[setsi].controlsLayout[i]>=0?-2:-3,controlsi,splatei,setsi));
                                p2_datai++;
                                controlsi++;
                                if(__alelisa)
                                {
                                    if(casesSets[setsi].controlsLayout[i]>=0)
                                        pha+=ctData[controlsi];
                                    if(casesSets[setsi].controlsLayout[i]<0)
                                        nha+=ctData[controlsi];

                                    m=platePosition(abs(casesSets[setsi].controlsLayout[i])==1000?0:abs(casesSets[setsi].controlsLayout[i]),false,casesSets[setsi].orientation)+1;
                                    l=m%12;
                                    k=platesi*8+m/12;
                                    plateTable1->setItemText(k,l,sampleVal(ctData[controlsi]));
                                    plateTable1->setItemData(k,l,new cLocInfo(casesSets[setsi].controlsLayout[i]>=0?-2:-3,controlsi,splatei,setsi));
                                    if(casesSets[setsi].controlsLayout[i]<0)
                                        plateTable1->setCellColor(k,l,FXRGB(200,200,200));
                                    else
                                        plateTable1->setCellColor(k,l,FXRGB(255,90,90));
                                    plateTable1->setItemBorders(k,l,FXTableItem::LBORDER|FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
                                    plateTable1->setCellEditable(k,l,true);
                                    if(k>0)plateTable1->setItemBorders(k-1,l,plateTable1->getItemBorders(k-1,l)|FXTableItem::BBORDER);
                                    if(l>0)plateTable1->setItemBorders(k,l-1,plateTable1->getItemBorders(k,l-1)|FXTableItem::RBORDER);
                                    if((k+1)<plateTable1->getNumRows())plateTable1->setItemBorders(k+1,l,plateTable1->getItemBorders(k+1,l)|FXTableItem::TBORDER);
                                    if(l<11)plateTable1->setItemBorders(k,l+1,plateTable1->getItemBorders(k,l+1)|FXTableItem::LBORDER);

                                    if(!p2_filled)
                                    {
                                        plateTable2->insertRows(p2_datai);
                                        plateTable2->getRowHeader()->setItemJustify(p2_datai,JUSTIFY_CENTER_X);
                                        plateTable2->getRowHeader()->setItemText(p2_datai,FXStringFormat("P%d:%c%d",platesi+1,'A'+m/12,l+1));
                                        for(int xi=0;xi<nc;xi++)
                                        {
                                            plateTable2->setItemJustify(p2_datai,xi,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                                            plateTable2->setCellEditable(p2_datai,xi,xi>0?false:true);
                                            if(!xi)
                                                plateTable2->setItemStipple(p2_datai,xi,STIPPLE_GRAY);
                                            else
                                                plateTable2->setCellColor(p2_datai,xi,FXRGB(255,255,255));
                                        }
                                        plateTable2->setItemBorders(p2_datai,0,FXTableItem::LBORDER|FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
                                        if(casesSets[setsi].controlsLayout[i]<0)
                                            plateTable2->setCellColor(p2_datai,0,FXRGB(200,200,200));
                                        else
                                            plateTable2->setCellColor(p2_datai,0,FXRGB(255,90,90));
                                    }
                                    plateTable2->setItemText(p2_datai,0,calculVal(ctData[controlsi]));
                                    plateTable2->setItemData(p2_datai,0,new cLocInfo(casesSets[setsi].controlsLayout[i]>=0?-2:-3,controlsi,splatei,setsi));
                                    p2_datai++;
                                    controlsi++;
                                }
                            }
                        }

                        platesci++;
                        FXbool err=false;
                        FXString rule,op,r2,rules_defs=casesSets[setsi].assayRes->getCellString(0,10);
                        r2=rules_defs;
                        double sb=0,val;
                        na/=nc;
                        pa/=pc;
                        nha/=nc;
                        pha/=pc;
                        while(!rules_defs.empty())
                        {
                            rule=rules_defs.before('\t');
                            rules_defs=rules_defs.after('\t');
                            op=rules_defs.before('\t');
                            rules_defs=rules_defs.after('\t');
                            val=FXFloatVal(rules_defs.before('\n'));
                            rules_defs=rules_defs.after('\n');
                            if(rule=="Na")
                                sb=na;
                            else if(rule=="Pa")
                                sb=pa;
                            else if(rule=="Pa-Na")
                                sb=pa-na;
                            else if(rule=="Nha")
                                sb=nha;
                            else if(rule=="Pha")
                                sb=pha;
                            if(op==">=" && sb<val)
                                err=true;
                            else if(op==">" && sb<=val)
                                err=true;
                            else if(op=="<=" && sb>val)
                                err=true;
                            else if(op=="<" && sb>=val)
                                err=true;
                        }

                        if(err)
                        {
                            invalidPlates=invalidPlates+"\n"+oLanguage->getText("str_rdgmdi_plate")+" "+FXStringVal(platesci);
                            invalidPlatesDetails=invalidPlatesDetails+oLanguage->getText("str_rdgmdi_plate")+FXStringFormat(" %d [",platesci)+
                                                 *casesSets[setsi].assay+" - "+(*casesSets[setsi].assaytitle)+"]:\n           "+
                                                 r2.substitute(
                                                               "Pa-Na",oLanguage->getText("str_asymdi_rule3")).substitute(
                                                               "Na",oLanguage->getText("str_asymdi_rule1")).substitute(
                                                               "Pa",oLanguage->getText("str_asymdi_rule2")).substitute(
                                                               "Nha",oLanguage->getText("str_asymdi_rule4")).substitute(
                                                               "Pha",oLanguage->getText("str_asymdi_rule5")).substitute(
                                                               "\t"," ").substitute("\n","\n           ")+"\n";
                        }
                    }

                    casepi=datai%(96/__alelisa_multiplier);  // PROBLEM
                    dp=datai/(96/__alelisa_multiplier);  // PROBLEM
                    rp=pd+casesSets[setsi].cases[casei].startPlate-diff;
                    if(!isControlPosition(casepi,casesSets[setsi].controlsLayout,
                                          casesSets[setsi].controlsPerPlate,__alelisa) &&
                                          ((casepi>=casesSets[setsi].cases[casei].startCell && dp==rp) || (dp>rp)))
                    {
                        m=platePosition(datai%(96/__alelisa_multiplier),__alelisa,casesSets[setsi].orientation);  // PROBLEM
                        l=m%12;
                        k=(datai/(96/__alelisa_multiplier))*8+m/12;  // PROBLEM
                        offset=(casesSets[setsi].cases[casei].replicates+1)*samplei+__alelisa_multiplier*replicatesi;
                        plateTable1->setItemText(k,l,sampleVal(casesSets[setsi].cases[casei].data[offset]));
                        plateTable1->setItemData(k,l,new cLocInfo(casesgi,offset));
                        plateTable1->setCellEditable(k,l,true);
                        plateTable1->setCellColor(k,l,casesgi==selectedCase?FXRGB(180,180,255):cColorsManager::getColor(casei,-replicatesi*10));
                        if(!p2_filled)
                        {
                            plateTable2->insertRows(p2_datai);
                            plateTable2->getRowHeader()->setItemJustify(p2_datai,JUSTIFY_CENTER_X);
                            plateTable2->getRowHeader()->setItemText(p2_datai,FXStringFormat("(%s) P%d:%c%d",sampleKey(casei).text(),platesi+1,'A'+m/12,l+1));
                            for(int xi=0;xi<nc;xi++)
                            {
                                plateTable2->setItemJustify(p2_datai,xi,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                                plateTable2->setItemStipple(p2_datai,xi,STIPPLE_NONE);
                                plateTable2->setCellEditable(p2_datai,xi,xi>0?false:true);
                            }
                            plateTable2->setItemData(p2_datai,0,new cLocInfo(casesgi,offset,platesi));
                        }
                        plateTable2->setItemText(p2_datai,0,calculVal(casesSets[setsi].cases[casei].data[offset]));
                        if(mcAm->getCheck() && replicatesi==0)
                        {
                            val=n=0;
                            for(int xi=0;xi<=casesSets[setsi].cases[casei].replicates;xi++,n++)
                                val+=casesSets[setsi].cases[casei].data[(casesSets[setsi].cases[casei].replicates+1)*samplei+__alelisa_multiplier*xi];
                            ((cLocInfo*)plateTable2->getItemData(p2_datai,0))->setValue(val/n);
                        }
                        else if(!mcAm->getCheck())
                            ((cLocInfo*)plateTable2->getItemData(p2_datai,0))->setValue(casesSets[setsi].cases[casei].data[offset]);
                        else
                            for(int xi=1;xi<nc;xi++)
                                plateTable2->setItemText(p2_datai,xi,replicates);
                        plateTable2->setCellColor(p2_datai,0,casesgi==selectedCase?FXRGB(180,180,255):cColorsManager::getColor(casei,-replicatesi*10));
                        for(int xi=1;xi<nc;xi++)
                            plateTable2->setCellColor(p2_datai,xi,FXRGB(247,247,247));
                        deltasi=1;
                        if(__alelisa)
                        {
                            m=platePosition(datai%(96/__alelisa_multiplier),__alelisa,casesSets[setsi].orientation,true);  // PROBLEM
                            l=m%12;
                            k=(datai/(96/__alelisa_multiplier))*8+m/12;  // PROBLEM
                            offset=(casesSets[setsi].cases[casei].replicates+1)*samplei+__alelisa_multiplier*replicatesi+1;
                            plateTable1->setItemText(k,l,sampleVal(casesSets[setsi].cases[casei].data[offset]));
                            plateTable1->setItemData(k,l,new cLocInfo(casesgi,offset));
                            plateTable1->setCellEditable(k,l,true);
                            plateTable1->setCellColor(k,l,casesgi==selectedCase?FXRGB(180,180,255):cColorsManager::getColor(casei,-replicatesi*10));
                            if(!p2_filled)
                            {
                                plateTable2->insertRows(p2_datai+1);
                                plateTable2->getRowHeader()->setItemJustify(p2_datai+1,JUSTIFY_CENTER_X);
                                plateTable2->getRowHeader()->setItemText(p2_datai+1,FXStringFormat("(%s) P%d:%c%d",sampleKey(casei).text(),platesi+1,'A'+m/12,l+1));
                                for(int xi=0;xi<nc;xi++)
                                {
                                    plateTable2->setItemJustify(p2_datai+1,xi,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                                    plateTable2->setCellEditable(p2_datai+1,xi,xi>0?false:true);
                                    if(!xi)
                                        plateTable2->setItemStipple(p2_datai+1,xi,STIPPLE_GRAY);
                                    else
                                        plateTable2->setCellColor(p2_datai+1,xi,FXRGB(255,255,255));
                                }
                                plateTable2->setItemData(p2_datai+1,0,new cLocInfo(casesgi,offset,platesi));
                            }
                            plateTable2->setItemText(p2_datai+1,0,calculVal(casesSets[setsi].cases[casei].data[offset]));
                            if(mcAm->getCheck() && replicatesi==0)
                            {
                                val=n=0;
                                for(int xi=0;xi<=casesSets[setsi].cases[casei].replicates;xi++,n++)
                                    val+=casesSets[setsi].cases[casei].data[(casesSets[setsi].cases[casei].replicates+1)*samplei+__alelisa_multiplier*xi+1];
                                ((cLocInfo*)plateTable2->getItemData(p2_datai+1,0))->setValue(val/n);
                            }
                            else if(!mcAm->getCheck())
                                ((cLocInfo*)plateTable2->getItemData(p2_datai+1,0))->setValue(casesSets[setsi].cases[casei].data[offset]);
                            plateTable2->setCellColor(p2_datai+1,0,casesgi==selectedCase?FXRGB(180,180,255):cColorsManager::getColor(casei,-replicatesi*10));
                            deltasi=2;
                        }

                        if(((cLocInfo*)plateTable2->getItemData(p2_datai,0))->isValue)
                        {
                            double calt,titer=0,titerg,bin,cala,calb=0,caleu,titerl2;
                            int k=1,st=0;

                            if(!foundSc)
                            {
                                foundSc=true;
                                scPos=p2_datai;
                            }

                            cala=cFormulaRatio::calculateRatio(casesSets[setsi].cala,
                                                               ((cLocInfo*)plateTable2->getItemData(p2_datai,0))->value,
                                                               __alelisa?((cLocInfo*)plateTable2->getItemData(p2_datai+1,0))->value:0,
                                                               ctData,casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate,
                                                               casesSets[setsi].cases[casei].factor,casesSets[setsi].factors,__alelisa);
                            if(mcCala->getCheck())
                            {
                                plateTable2->setItemText(p2_datai,k++,calculVal(cala));
                                if(casesgi==selectedCase)
                                {
                                    if(cala<statsBase[st][0])
                                        statsBase[st][0]=cala;
                                    if(cala>statsBase[st][1])
                                        statsBase[st][1]=cala;
                                    statsBase[st][2]+=cala;
                                    statsBase[st][3]+=cala?log10(cala):0;
                                    statsValues[st][statsCount[st]++]=cala;
                                    st++;
                                }
                            }

                            if(casesSets[setsi].useSecond)
                                calb=cFormulaRatio::calculateRatio(casesSets[setsi].calb,
                                                                   ((cLocInfo*)plateTable2->getItemData(p2_datai,0))->value,
                                                                   __alelisa?((cLocInfo*)plateTable2->getItemData(p2_datai+1,0))->value:0,
                                                                   ctData,casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate,
                                                                   casesSets[setsi].cases[casei].factor,casesSets[setsi].factors,__alelisa);
                            if(mcCalb->getCheck())
                                if(!casesSets[setsi].useSecond)
                                    plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_na"));
                                else
                                {
                                    plateTable2->setItemText(p2_datai,k++,calculVal(calb));
                                    if(casesgi==selectedCase)
                                    {
                                        if(calb<statsBase[st][0])
                                            statsBase[st][0]=calb;
                                        if(calb>statsBase[st][1])
                                            statsBase[st][1]=calb;
                                        statsBase[st][2]+=calb;
                                        statsBase[st][3]+=calb?log10(calb):0;
                                        statsValues[st][statsCount[st]++]=calb;
                                        st++;
                                    }
                                }
                            if(mcEU->getCheck())
                            {
                                caleu=cFormulaRatio::calculateEU(((cLocInfo*)plateTable2->getItemData(p2_datai,0))->value,
                                                                                              ctData,casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate,
                                                                                              casesSets[setsi].cases[casei].factor,casesSets[setsi].factors,__alelisa);
                                if(casesgi==selectedCase)
                                {
                                    if(caleu<statsBase[st][0])
                                        statsBase[st][0]=caleu;
                                    if(caleu>statsBase[st][1])
                                        statsBase[st][1]=caleu;
                                    statsBase[st][2]+=caleu;
                                    statsBase[st][3]+=caleu?log10(caleu):0;
                                    statsValues[st][statsCount[st]++]=caleu;
                                    st++;
                                }

                                plateTable2->setItemText(p2_datai,k++,calculVal(caleu));
                            }

                            if(!casesSets[setsi].useTiters)
                            {
                                if(mcTiter->getCheck())
                                    plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_na"));
                            }
                            else
                            {
                                calt=cFormulaRatio::calculateRatio(casesSets[setsi].calt,
                                                                   ((cLocInfo*)plateTable2->getItemData(p2_datai,0))->value,
                                                                   __alelisa?((cLocInfo*)plateTable2->getItemData(p2_datai+1,0))->value:0,
                                                                   ctData,casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate,
                                                                   casesSets[setsi].cases[casei].factor,casesSets[setsi].factors,__alelisa);
                                titer=cFormulaRatio::calculateTiter(calt,casesSets[setsi].slope,casesSets[setsi].intercept,casesSets[setsi].cases[casei].factor,casesSets[setsi].factors);
                                if(mcTiter->getCheck())
                                    plateTable2->setItemText(p2_datai,k++,calculVal((int)titer));
                                if(casesgi==selectedCase)
                                {
                                    if(titer<statsBase[st][0])
                                        statsBase[st][0]=titer;
                                    if(titer>statsBase[st][1])
                                        statsBase[st][1]=titer;
                                    statsBase[st][2]+=titer;
                                    statsBase[st][3]+=titer?log10(titer):0;
                                    statsValues[st][statsCount[st]++]=titer;
                                    st++;
                                }
                            }

                            if(mcLog2->getCheck())
                            {
                                if(!casesSets[setsi].useTiters)
                                    plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_na"));
                                else
                                {
                                    titerl2=cFormulaRatio::calculateTiterLog2(titer);
                                    plateTable2->setItemText(p2_datai,k++,calculVal(titerl2));
                                    if(casesgi==selectedCase)
                                    {
                                        if(titerl2<statsBase[st][0])
                                            statsBase[st][0]=titerl2;
                                        if(titerl2>statsBase[st][1])
                                            statsBase[st][1]=titerl2;
                                        statsBase[st][2]+=titerl2;
                                        statsBase[st][3]+=titerl2?log10(titerl2):0;
                                        statsValues[st][statsCount[st]++]=titerl2;
                                        st++;
                                    }
                                }
                            }

                            bool is=false;
                            for(int we=0;we<30;we++)
                                if(casesSets[setsi].titerGroups[we]!=0)
                                {
                                    is=true;
                                    break;
                                }
                            if(is)
                                titerg=cFormulaRatio::calculateTiterGroup(titer,casesSets[setsi].titerGroups);
                            else
                                titerg=-1;
                            if(mcTiters->getCheck())
                            {
                                if(!casesSets[setsi].useTiters || !is)
                                    plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_na"));
                                else
                                    plateTable2->setItemText(p2_datai,k++,calculVal((int)titerg));
                            }

                            is=false;
                            for(int we=0;we<30;we++)
                                if(casesSets[setsi].ratioGroups[we]!=0)
                                {
                                    is=true;
                                    break;
                                }
                            if(is)
                                bin=cFormulaRatio::calculateRatioGroup(cala,casesSets[setsi].ratioGroups);
                            else
                                bin=-1;
                            if(mcBins->getCheck()/* || mcBins2->getCheck()*/)
                            {

                                if(mcBins->getCheck())
                                {
                                    if(!is)
                                        plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_na"));
                                    else
                                        plateTable2->setItemText(p2_datai,k++,calculVal((int)bin));
                                }
                                /*if(mcBins2->getCheck())
                                {
                                    if(!is || !casesSets[setsi].useSecond)
                                        plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_na"));
                                    else
                                        plateTable2->setItemText(p2_datai,k++,calculVal((int)cFormulaRatio::calculateRatioGroup(calb,casesSets[setsi].ratioGroups)));
                                }*/
                            }

                            if(mcAge->getCheck())
                                plateTable2->setItemText(p2_datai,k++,*casesSets[setsi].cases[casei].age);

                            if(mcBreed1->getCheck() || mcBreed2->getCheck())
                            {
                                if(mcBreed1->getCheck())
                                    plateTable2->setItemText(p2_datai,k++,*casesSets[setsi].cases[casei].breed1);
                                if(mcBreed2->getCheck())
                                    plateTable2->setItemText(p2_datai,k++,*casesSets[setsi].cases[casei].breed2);
                            }

                            if(mcResult->getCheck())
                            {
                                double __calapco,__calasco;
                                int r,a;
                                sControlAms cams=cFormulaRatio::calculateControlAms(ctData,casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate,__alelisa);
                                ClearAllVars();
                                SetValue("pa",&cams.posam);
                                SetValue("na",&cams.negam);
                                r=Evaluate((char*)casesSets[setsi].calapco->text(),&__calapco,&a);
                                r=Evaluate((char*)casesSets[setsi].calasco->text(),&__calasco,&a);
                                ClearAllVars();
                                result=0;
                                if(*casesSets[setsi].calaop==">=")
                                    result=cala==-9999?-9999:cala<__calasco?0:cala<__calapco?1:2;
                                else
                                    result=cala==-9999?-9999:cala>__calasco?0:cala>__calapco?1:2;
                                switch(result)
                                {
                                    case 0:
                                        plateTable2->setCellTextColor(p2_datai,k,FXRGB(200,200,200));
                                        plateTable2->useCellTextColor(p2_datai,k,true);
                                        plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_rdgmdi_neg"));
                                        if(casesgi==selectedCase)stn++;
                                        break;
                                    case 1:
                                        plateTable2->setCellTextColor(p2_datai,k,FXRGB(210,120,30));
                                        plateTable2->useCellTextColor(p2_datai,k,true);
                                        plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_rdgmdi_sus"));
                                        if(casesgi==selectedCase)sts++;
                                        break;
                                    case 2:
                                        plateTable2->setCellTextColor(p2_datai,k,FXRGB(255,0,0));
                                        plateTable2->useCellTextColor(p2_datai,k,true);
                                        plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_rdgmdi_pos"));
                                        if(casesgi==selectedCase)stp++;
                                        break;
                                    default:
                                        plateTable2->useCellTextColor(p2_datai,k,false);
                                        plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_invalid"));
                                        if(casesgi==selectedCase)ste++;
                                        break;
                                }
                            }

                            if(mcResult2->getCheck())
                            {
                                if(!casesSets[setsi].useSecond)
                                    plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_na"));
                                else
                                {
                                    double __calbpco,__calbsco;
                                    int r,a;
                                    sControlAms cams=cFormulaRatio::calculateControlAms(ctData,casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate,__alelisa);
                                    ClearAllVars();
                                    SetValue("pa",&cams.posam);
                                    SetValue("na",&cams.negam);
                                    r=Evaluate((char*)casesSets[setsi].calbpco->text(),&__calbpco,&a);
                                    r=Evaluate((char*)casesSets[setsi].calbsco->text(),&__calbsco,&a);
                                    ClearAllVars();
                                    result=0;
                                    if(*casesSets[setsi].calbop==">=")
                                        result=calb==-9999?-9999:calb<__calbsco?0:cala<__calbpco?1:2;
                                    else
                                        result=calb==-9999?-9999:calb>__calbsco?0:cala>__calbpco?1:2;
                                    switch(result)
                                    {
                                        case 0:
                                            plateTable2->setCellTextColor(p2_datai,k,FXRGB(200,200,200));
                                            plateTable2->useCellTextColor(p2_datai,k,true);
                                            plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_rdgmdi_neg"));
                                            break;
                                        case 1:
                                            plateTable2->setCellTextColor(p2_datai,k,FXRGB(230,200,50));
                                            plateTable2->useCellTextColor(p2_datai,k,true);
                                            plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_rdgmdi_sus"));
                                            break;
                                        case 2:
                                            plateTable2->setCellTextColor(p2_datai,k,FXRGB(255,0,0));
                                            plateTable2->useCellTextColor(p2_datai,k,true);
                                            plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_rdgmdi_pos"));
                                            break;
                                        default:
                                            plateTable2->useCellTextColor(p2_datai,k,false);
                                            plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_invalid"));
                                            break;
                                    }
                                }
                            }

                            if(titerg!=-1 && titerg!=-9999 && casesSets[setsi].useTiters)
                            {
                                tct[(int)titerg]++;
                                chtype=0;
                            }
                            else if(bin!=-1 && bin!=-9999)
                            {
                                bct[(int)bin]++;
                                chtype=1;
                            }
                            else
                            {
                                chtype=2;
                                if(cala>-9999)
                                {
                                    sVChartData *cval=new sVChartData;
                                    cval->value=cala<0?0:cala;
                                    cval->name=FXStringFormat("%d",sct+1);
                                    cv->insert(FXStringFormat("%d",sct).text(),cval);
                                    sct++;
                                }
                            }
                        }

                        p2_datai+=deltasi;
                        replicates[replicatesi]='\'';
                        replicates[replicatesi+1]=0;
                        replicatesi++;
                    }
                    datai++;
                }
                samplei+=deltasi;
            }
            int ccnt;
            double cl,dc=0;
            switch(chtype)
            {
                case 0:
                    cl=130;
                    for(ccnt=0;ccnt<30 && casesSets[setsi].titerGroups[ccnt]!=0;ccnt++);
                    ccnt++;
                    dc=50.0/(double)ccnt;
                    for(int i=0;i<ccnt;i++,cl-=dc)
                    {
                        sVChartData *cval=new sVChartData;
                        cval->value=tct[i];
                        cval->color=FXRGB(((int)cl)>>1,(int)cl,0);
                        cval->name=(i==ccnt-1)?FXStringFormat("%d+",i):FXStringFormat("%d",i);
                        cv->insert(FXStringFormat("%d",i).text(),cval);
                    }
                    charts[cct]->setHAxisTitle(oLanguage->getText("str_asymdi_titers"));
                    charts[cct]->setVAxisTitle(oLanguage->getText("str_rdgmdi_chcount"));
                    break;
                case 1:
                    cl=180;
                    for(ccnt=0;ccnt<30 && casesSets[setsi].ratioGroups[ccnt]!=0;ccnt++);
                    ccnt++;
                    dc=50.0/(double)ccnt;
                    for(int i=0;i<ccnt;i++,cl-=dc)
                    {
                        sVChartData *cval=new sVChartData;
                        cval->value=bct[i];
                        cval->color=FXRGB((int)cl,((int)cl)>>1,0);
                        cval->name=(i==ccnt-1)?FXStringFormat("%d+",i):FXStringFormat("%d",i);
                        cv->insert(FXStringFormat("%d",i).text(),cval);
                    }
                    charts[cct]->setHAxisTitle(oLanguage->getText("str_asymdi_bins"));
                    charts[cct]->setVAxisTitle(oLanguage->getText("str_rdgmdi_chcount"));
                    break;
                default:
                    cl=150;
                    ccnt=cv->no();
                    if(ccnt)
                        dc=30.0/(double)ccnt;
                    for(int i=0;i<ccnt;i++,cl-=dc)
                            ((sVChartData*)cv->find(FXStringFormat("%d",i).text()))->color=FXRGB(/*i%2?0:23*/0,((int)cl)>>2,(int)cl);
                    charts[cct]->setHAxisTitle(oLanguage->getText("str_rdgmdi_chsamples"));
                    charts[cct]->setVAxisTitle(*casesSets[setsi].cala);
                    //charts[cct]->setClusterCount(2);
                    //charts[cct]->legendStrings()->insert("0","First case");
                    //charts[cct]->legendStrings()->insert("1","Second case");
                    break;
            }
        }
        pd+=casesSets[setsi].platesCount;
        int mdi=datai%(96/__alelisa_multiplier);  // PROBLEM
        if(mdi)
            datai+=(96/__alelisa_multiplier)-mdi;  // PROBLEM
    }

    if(invalidPlates.empty())
    {
        lbInv->setText((char*)NULL);
        lbInv->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        lbInv->setTipText((char*)NULL);
        lbInv->hide();
    }
    else
    {
        lbInv->show();
        lbInv->setText(oLanguage->getText("str_rdgmdi_invplt")+""+invalidPlates);
        lbInv->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        lbInv->setTipText(oLanguage->getText("str_rdgmdi_rhint"));
    }

    lbStats->setText(oLanguage->getText("str_rdgmdi_posct")+FXStringFormat(" %d\n\n",stp)+
                     oLanguage->getText("str_rdgmdi_susct")+FXStringFormat(" %d\n\n",sts)+
                     oLanguage->getText("str_rdgmdi_negct")+FXStringFormat(" %d\n\n",stn)+
                     oLanguage->getText("str_rdgmdi_errct")+FXStringFormat(" %d",ste));

    if(selectedCase>=0)
    {
        int ps=((sCaseEntry*)caseList1->getItemData(selectedCase,0))->parentSet;

        int snc=mcCala->getCheck()?1:0,kc=0;
        if(mcCalb->getCheck() && casesSets[ps].useSecond)
            snc++;
        if(mcEU->getCheck())
            snc++;
        if(mcTiter->getCheck() && casesSets[ps].useTiters)
            snc++;
        if(mcLog2->getCheck() && casesSets[ps].useTiters)
            snc++;
        statsTable->insertColumns(0,snc);
        for(int t=0;t<snc;t++)
        {
            statsTable->getColumnHeader()->setItemJustify(t,JUSTIFY_CENTER_X);
            for(int yu=0;yu<6;yu++)
                statsTable->setItemJustify(yu,t,JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
        }
        double stdev,armean;
        if(mcCala->getCheck())
        {
            if(statsBase[kc][0]==-9999)
            {
                for(int ct=0;ct<6;ct++)
                    statsTable->setItemText(ct,kc,calculVal(-9999));
            }
            else
            {
                statsTable->setItemText(0,kc,calculVal(statsBase[kc][0]));
                statsTable->setItemText(1,kc,calculVal(statsBase[kc][1]));
                armean=statsBase[kc][2]/statsBase[kc][4];
                statsTable->setItemText(2,kc,calculVal(armean));
                statsTable->setItemText(3,kc,calculVal(pow(10,statsBase[kc][3]/statsBase[kc][4])));
                stdev=0;
                for(int ct=0;ct<statsBase[kc][4];ct++)
                {
                    stdev+=pow(statsValues[kc][ct]-armean,2);
                }
                stdev/=statsBase[kc][4];
                stdev=sqrt(stdev);
                statsTable->setItemText(4,kc,calculVal(stdev));
                statsTable->setItemText(5,kc,calculVal(!armean?0:round(10*(100*stdev/armean))/10)+"%");
            }
            statsTable->setColumnText(kc++,*casesSets[ps].cala);
        }
        if(mcCalb->getCheck() && casesSets[ps].useSecond)
        {
            if(statsBase[kc][0]==-9999)
            {
                for(int ct=0;ct<6;ct++)
                    statsTable->setItemText(ct,kc,calculVal(-9999));
            }
            else
            {
                statsTable->setItemText(0,kc,calculVal(statsBase[kc][0]));
                statsTable->setItemText(1,kc,calculVal(statsBase[kc][1]));
                armean=statsBase[kc][2]/statsBase[kc][4];
                statsTable->setItemText(2,kc,calculVal(armean));
                statsTable->setItemText(3,kc,calculVal(pow(10,statsBase[kc][3]/statsBase[kc][4])));
                stdev=0;
                for(int ct=0;ct<statsBase[kc][4];ct++)
                {
                    stdev+=pow(statsValues[kc][ct]-armean,2);
                }
                stdev/=statsBase[kc][4];
                stdev=sqrt(stdev);
                statsTable->setItemText(4,kc,calculVal(stdev));
                statsTable->setItemText(5,kc,calculVal(!armean?0:round(10*(100*stdev/armean))/10)+"%");
            }
            statsTable->setColumnText(kc++,*casesSets[ps].calb);
        }
        if(mcEU->getCheck())
        {
            if(statsBase[kc][0]==-9999)
            {
                for(int ct=0;ct<6;ct++)
                    statsTable->setItemText(ct,kc,calculVal(-9999));
            }
            else
            {
                statsTable->setItemText(0,kc,calculVal(statsBase[kc][0]));
                statsTable->setItemText(1,kc,calculVal(statsBase[kc][1]));
                armean=statsBase[kc][2]/statsBase[kc][4];
                statsTable->setItemText(2,kc,calculVal(round(armean)));
                statsTable->setItemText(3,kc,calculVal(round(pow(10,statsBase[kc][3]/statsBase[kc][4]))));
                stdev=0;
                for(int ct=0;ct<statsBase[kc][4];ct++)
                {
                    stdev+=pow(statsValues[kc][ct]-armean,2);
                }
                stdev/=statsBase[kc][4];
                stdev=sqrt(stdev);
                statsTable->setItemText(4,kc,calculVal(round(stdev)));
                statsTable->setItemText(5,kc,calculVal(round(!armean?0:round(10*(100*stdev/armean))/10))+"%");
            }
            statsTable->setColumnText(kc++,oLanguage->getText("str_rdgmdi_eu"));
        }
        if(mcTiter->getCheck() && casesSets[ps].useTiters)
        {
            if(statsBase[kc][0]==-9999)
            {
                for(int ct=0;ct<6;ct++)
                    statsTable->setItemText(ct,kc,calculVal(-9999));
            }
            else
            {
                statsTable->setItemText(0,kc,calculVal(statsBase[kc][0]));
                statsTable->setItemText(1,kc,calculVal(statsBase[kc][1]));
                armean=statsBase[kc][2]/statsBase[kc][4];
                statsTable->setItemText(2,kc,calculVal(round(armean)));
                statsTable->setItemText(3,kc,calculVal(round(pow(10,statsBase[kc][3]/statsBase[kc][4]))));
                stdev=0;
                for(int ct=0;ct<statsBase[kc][4];ct++)
                {
                    stdev+=pow(statsValues[kc][ct]-armean,2);
                }
                stdev/=statsBase[kc][4];
                stdev=sqrt(stdev);
                statsTable->setItemText(4,kc,calculVal(round(stdev)));
                statsTable->setItemText(5,kc,calculVal(round(!armean?0:round(10*(100*stdev/armean))/10))+"%");
            }
            statsTable->setColumnText(kc++,oLanguage->getText("str_asymdi_titer"));
        }
        if(mcLog2->getCheck() && casesSets[ps].useTiters)
        {
            if(statsBase[kc][0]==-9999)
            {
                for(int ct=0;ct<6;ct++)
                    statsTable->setItemText(ct,kc,calculVal(-9999));
            }
            else
            {
                statsTable->setItemText(0,kc,calculVal(statsBase[kc][0]));
                statsTable->setItemText(1,kc,calculVal(statsBase[kc][1]));
                armean=statsBase[kc][2]/statsBase[kc][4];
                statsTable->setItemText(2,kc,calculVal(armean));
                statsTable->setItemText(3,kc,calculVal(pow(10,statsBase[kc][3]/statsBase[kc][4])));
                stdev=0;
                for(int ct=0;ct<statsBase[kc][4];ct++)
                {
                    stdev+=pow(statsValues[kc][ct]-armean,2);
                }
                stdev/=statsBase[kc][4];
                stdev=sqrt(stdev);
                statsTable->setItemText(4,kc,calculVal(stdev));
                statsTable->setItemText(5,kc,calculVal(!armean?0:round(10*(100*stdev/armean))/10)+"%");
            }
            statsTable->setColumnText(kc++,oLanguage->getText("str_rdgmdi_log2"));
        }
    }
    if(selectedCase>=0)
        for(int ui=0;ui<5;ui++)
        {
            free(statsValues[ui]);
        }
}

void cMDIReadings::locateCaseLists(int prIndex)
{
    if(prIndex<0)
        return;
    caseList1->makePositionVisible(prIndex,0);
    caseList2->makePositionVisible(prIndex,0);
}

void cMDIReadings::locatePlateTable1(int prIndex)
{
    if(prIndex<0)
        return;
    for(int i=0;i<plateTable1->getNumRows();i++)
        for(int j=0;j<plateTable1->getNumColumns();j++)
        {
            if(!plateTable1->getItemData(i,j))
                continue;
            if((((cLocInfo*)plateTable1->getItemData(i,j))->caseIndex)==prIndex)
            {
                plateTable1->makePositionVisible(i,j);
                return;
            }
        }
}

void cMDIReadings::locatePlateTable2(int prIndex)
{
    if(prIndex<0)
        return;

    for(int i=0;i<plateTable2->getNumRows();i++)
    {
        if(!plateTable2->getItemData(i,0))
            continue;
        if((((cLocInfo*)plateTable2->getItemData(i,0))->caseIndex)==prIndex)
        {
            plateTable2->makePositionVisible(i,0);
            return;
        }
    }
}

void cMDIReadings::selectCase(int prIndex,int prPlate)
{
    selectedCase=prIndex;
    caseList1->killSelection();
    caseList2->killSelection();

    drawPlateTables();

    if(prIndex==-1)
    {
        assayId->setText(FXStringFormat("%s\n",oLanguage->getText("str_rdgmdi_nosel").text()));
        assayTpl->setText(FXStringFormat("%s\n",oLanguage->getText("str_rdgmdi_nosel").text()));
        assayTech->setText(oLanguage->getText("str_rdgmdi_nosel"));
        this->setTitle(oLanguage->getText("str_reading"));
        return;
    }

    sCaseEntry *found=NULL;
    if(prIndex<0)
    {
        if(prPlate<0)
            return;

        int platesi=0;
        for(int i=0;i<tSetsCount;i++)
        {
            platesi+=casesSets[i].platesCount;
            if(platesi>prPlate)
            {
                found=&casesSets[i].cases[0];
                break;
            }
        }
        if(found==NULL)
            return;
    }

    sCaseEntry *selCase=prIndex<0?found:(sCaseEntry*)caseList1->getItemData(prIndex,0);

    if(prIndex>=0)
    {
        caseList1->selectRow(prIndex);
        caseList2->selectRow(prIndex);
    }

    if(prIndex<0)
    {
        assayId->setText(FXStringFormat("%s\n",prIndex==-2?oLanguage->getText("str_rdgmdi_posc").text():oLanguage->getText("str_rdgmdi_negc").text()));
        this->setTitle(oLanguage->getText("str_reading"));
    }
    else
    {
        this->setTitle(oLanguage->getText("str_reading")+" ("+caseList1->getItemText(prIndex,1)+")");
        assayId->setText(FXStringFormat("%s \"%s\", %s %s,\n%s \"%s\", %s %s",
                         oLanguage->getText("str_rdgmdi_caseid").text(),caseList1->getItemText(prIndex,1).text(),
                         oLanguage->getText("str_rdgmdi_factor").text(),FXStringFormat("%d",selCase->factor).text(),
                         oLanguage->getText("str_rdgmdi_popid").text(),caseList1->getItemText(prIndex,3).text(),
                         oLanguage->getText("str_rdgmdi_bldate").text(),selCase->bleedDate->text()));
    }

    assayTpl->setText(FXStringFormat("%s \"%s\", %s%s\n%s \"%s\", %s \"%s\", %s \"%s\"",
                      oLanguage->getText("str_tplmdi_title").text(),
                      selCase->tpl->text(),
                      casesSets[selCase->parentSet].orientation==1?oLanguage->getText("str_tplmdi_vert").text():oLanguage->getText("str_tplmdi_horiz").text(),
                      casesSets[selCase->parentSet].alelisa?(", "+oLanguage->getText("str_rdgmdi_blelisa")).text():"",
                      oLanguage->getText("str_rdgmdi_usedagainst").text(),selCase->assay->text(),
                      oLanguage->getText("str_rdgmdi_assayLot").lower().text(),selCase->lot->text(),
                      oLanguage->getText("str_rdgmdi_assayExp").lower().text(),selCase->expirationDate->text()));
    assayTech->setText(oWinMain->getTechnician());
}

void cMDIReadings::updateData()
{
    saved=false;
}

FXbool cMDIReadings::saveData(void)
{
    plateTable1->acceptInput(true);
    plateTable2->acceptInput(true);
    if(saved)
        return true;

    sCasesSet *s;
    sCaseEntry *c;

    FXchar ftime[100];
    time_t curtime;
    struct tm *loctime;
    curtime=time(NULL);
    loctime=localtime(&curtime);
    strftime(ftime,100,"%Y-%m-%d",loctime);
    FXString nowDate(ftime);
    FXString data_defs,controls_defs;


    curtime=time(NULL);
    loctime=localtime(&curtime);
    strftime(ftime,100,"%s",loctime);
    FXString *sroid=new FXString(ftime);
    srand(curtime);
    FXString *nroid=new FXString();

    int cci=0,p=0,d,od;
    saved=true;
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        *nroid = *sroid+FXStringVal(rand());
        d=od=0;
        s=&(casesSets[setsi]);
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++,cci++)
        {
            c=&(s->cases[casei]);
            data_defs="";
            for(int i=0;i<c->controlsCount;i++)
                data_defs=data_defs+FXStringVal(casesSets[setsi].cases[casei].controlsData[i])+" ";
            data_defs=data_defs+"\n";
            for(int i=0;i<c->count*(c->alelisa?2:1)*(c->replicates+1);i++)
                data_defs=data_defs+FXStringVal(casesSets[setsi].cases[casei].data[i])+" ";
            if(newdata)
            {
                if(cCaseManager::caseExists(*c->id,*c->assay))
                {
                    int result=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),
                                                      (*c->id+"\n\n"+oLanguage->getText("str_cseman_duplicate")).text());
                    int ct=1;
                    FXString newId;
                    switch(result)
                    {
                        case MBOX_CLICKED_YES:
                            cCaseManager::setCase(*c->id,*c->assay,*c->id,*c->reading_oid,nowDate,*c->technician,*c->reason,*c->veterinarian,*c->assay,
                                      *c->lot,*c->expirationDate,*c->spType,nowDate,c->replicates,c->factor,*c->population,*c->age,
                                      c->count,*c->tpl,s->orientation,s->alelisa?2:1,c->startPlate,c->startCell,
                                      c->plateSpanCount,*c->controls_defs,data_defs,*c->comments);
                            continue;
                            break;
                        case MBOX_CLICKED_NO:
                            while(cCaseManager::caseExists(newId=*c->id+"_"+FXStringVal(ct++),*c->assay));
                            *c->id=newId;
                            caseList1->setItemText(cci,1,*c->id);
                            caseList2->setItemText(cci,1,*c->id);
                            selectCase(selectedCase);
                            charts[cci]->setTitle(oLanguage->getText("str_rdgmdi_plate")+FXStringFormat(" %d",p+1)+" - "+(*c->id)+" - "+(*c->assay));
                            charts[cci]->hide();
                            charts[cci]->show();
                            break;
                        default:
                            saved=false;
                            continue;
                            break;
                    }
                }

                cCaseManager::addCase(*c->id,*c->reading_oid,*c->readingDate,*c->technician,*c->reason,*c->veterinarian,*c->assay,
                                      *c->lot,*c->expirationDate,*c->spType,*c->bleedDate,c->replicates,c->factor,*c->population,*c->age,
                                      c->count,*c->tpl,s->orientation,s->alelisa?2:1,c->startPlate,c->startCell,
                                      c->plateSpanCount,*c->controls_defs,data_defs,*c->comments);
                if(!cPopulationManager::populationExists(*c->population))
                    cPopulationManager::addPopulation(*c->population,"",nowDate,"---","---","---","---","---","","","","");
            }
            else {
                cDataResult *res=oDataLink->execute("SELECT data_defs FROM t_gncases WHERE id='"+*c->id+"' AND assay_oid='"+*c->assay+"';");
                FXString od(res->getCellString(0,0).before('\n'));
                FXString nd(data_defs.before('\n'));

                if(od != nd) {
                    *c->reading_oid = *nroid;
                }

                cCaseManager::setCase(*c->id,*c->assay,*c->id,*c->reading_oid,*c->readingDate,*c->technician,*c->reason,*c->veterinarian,*c->assay,
                                      *c->lot,*c->expirationDate,*c->spType,*c->bleedDate,c->replicates,c->factor,*c->population,*c->age,
                                      c->count,*c->tpl,s->orientation,s->alelisa?2:1,c->startPlate,c->startCell,
                                      c->plateSpanCount,*c->controls_defs,data_defs,*c->comments);
            }

            d=casesSets[setsi].cases[casei].startPlate;
            if(d!=od)
            {
                p++;
                od=d;
            }
            p+=casesSets[setsi].cases[casei].plateSpanCount-1;
        }
    }
    if(saved)
        newdata=false;
    cMDICaseManager::cmdResearch();
    return true;
}

cMDIReadings::cMDIReadings(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH)
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_MDIREADINGS);
    saved=false;
    newdata=true;
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(this,LAYOUT_FILL);
    tbMain=new FXTabBook(_hframe0,this,ID_TBMAIN,PACK_UNIFORM_WIDTH|PACK_UNIFORM_HEIGHT|LAYOUT_FILL,0,0,0,0,0,0,0,0);
    tiPrimary=new FXTabItem(tbMain,oLanguage->getText("str_rdgmdi_primary"));
        FXVerticalFrame *_vframe0=new FXVerticalFrame(tbMain,FRAME_RAISED|FRAME_THICK|LAYOUT_FILL);
        FXHorizontalFrame *_hframe200=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
        new FXLabel(_hframe200,oLanguage->getText("str_rdgmdi_primary_od"));
        lbPlates=new FXLabel(_hframe200,(char*)NULL);
        FXVerticalFrame *_vframe01=new FXVerticalFrame(_vframe0,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
            plateTable1=new cColorTable(_vframe01,this,ID_PLATETABLE1,TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT|LAYOUT_FILL,0,0,0,181);
            plateTable1->setTableSize(8,12);
            plateTable1->setGridColor(FXRGB(120,120,120));
            plateTable1->setRowHeaderWidth(42);
            plateTable1->setDefColumnWidth(39);
            plateTable1->setCellBorderWidth(1);
            plateTable1->setSelBackColor(FXRGB(100,100,140));
            plateTable1->setSelTextColor(0);
            plateTable1->setGridColor(FXRGB(120,120,120));
            plateTable1->setBorderColor(FXRGB(120,120,120));
            plateTable1->setStippleColor(FXRGB(250,250,240));
            plateTable1->setScrollStyle(HSCROLLING_OFF|HSCROLLER_NEVER|VSCROLLING_ON|VSCROLLER_ALWAYS|SCROLLERS_TRACK);
            plateTable1->showVertGrid(false);
            plateTable1->showHorzGrid(false);

            edit=new FXMenuPane(this);
            new FXMenuCommand(edit,oLanguage->getText("str_rdgmdi_edit1"),NULL,plateTable1,FXTable::ID_SELECT_ALL);
            new FXMenuSeparator(edit);
            new FXMenuCommand(edit,oLanguage->getText("str_rdgmdi_edit2"),NULL,this,ID_COPY);
            new FXMenuCommand(edit,oLanguage->getText("str_rdgmdi_edit3"),NULL,this,ID_PASTE);
            mbEdit=new FXMenuButton(_hframe200,oLanguage->getText("str_rdgmdi_mbedit"),NULL,edit,ICON_AFTER_TEXT|LAYOUT_RIGHT|MENUBUTTON_DOWN|FRAME_RAISED);
            mbEdit->setTextColor(FXRGB(180,0,0));

            setAccelTable(new FXAccelTable());
            getAccelTable()->addAccel(MKUINT(KEY_A,CONTROLMASK),this,FXSEL(SEL_COMMAND,ID_SELALL));
            getAccelTable()->addAccel(MKUINT(KEY_a,CONTROLMASK),this,FXSEL(SEL_COMMAND,ID_SELALL));
            getAccelTable()->addAccel(MKUINT(KEY_C,CONTROLMASK),this,FXSEL(SEL_COMMAND,ID_COPY));
            getAccelTable()->addAccel(MKUINT(KEY_c,CONTROLMASK),this,FXSEL(SEL_COMMAND,ID_COPY));
            getAccelTable()->addAccel(MKUINT(KEY_V,CONTROLMASK),this,FXSEL(SEL_COMMAND,ID_PASTE));
            getAccelTable()->addAccel(MKUINT(KEY_v,CONTROLMASK),this,FXSEL(SEL_COMMAND,ID_PASTE));

        _gb0=new FXGroupBox(_vframe0,oLanguage->getText("str_rdgmdi_caseinfo"),GROUPBOX_TITLE_LEFT|LAYOUT_FILL_X|FRAME_GROOVE);
            FXVerticalFrame *_vframe02=new FXVerticalFrame(_gb0,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
            caseList1=new cColorTable(_vframe02,this,ID_CASELIST1,TABLE_NO_COLSELECT|LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT,0,0,0,100);
            caseList1->setTableSize(0,7);
            caseList1->setRowHeaderWidth(0);
            caseList1->setColumnText(0,oLanguage->getText("str_rdgmdi_cllayout"));
            caseList1->setColumnWidth(0,80);
            caseList1->setColumnText(1,oLanguage->getText("str_rdgmdi_clid"));
            caseList1->setColumnWidth(1,90);
            caseList1->setColumnText(2,oLanguage->getText("str_rdgmdi_clcount"));
            caseList1->setColumnWidth(2,48);
            caseList1->setColumnText(3,oLanguage->getText("str_rdgmdi_clpop"));
            caseList1->setColumnWidth(3,90);
            caseList1->setColumnText(4,oLanguage->getText("str_rdgmdi_clasy"));
            caseList1->setColumnWidth(4,90);
            caseList1->setColumnText(5,oLanguage->getText("str_rdgmdi_clage"));
            caseList1->setColumnWidth(5,32);
            caseList1->setColumnText(6,oLanguage->getText("str_rdgmdi_clrepl"));
            caseList1->setColumnWidth(6,40);
            caseList1->getColumnHeader()->setItemJustify(0,JUSTIFY_CENTER_X);
            caseList1->getColumnHeader()->setItemJustify(2,JUSTIFY_CENTER_X);
            caseList1->getColumnHeader()->setItemJustify(5,JUSTIFY_CENTER_X);
            caseList1->getColumnHeader()->setItemJustify(6,JUSTIFY_CENTER_X);
            caseList1->setGridColor(FXRGB(255,255,255));
            caseList1->setCellBorderWidth(1);
            caseList1->setColumnHeaderHeight(22);
            caseList1->showHorzGrid(true);
            caseList1->showVertGrid(false);
            caseList1->setSelTextColor(0);
            caseList1->setCellBorderColor(FXRGB(255,0,0));
            caseList1->setSelBackColor(FXRGB(180,180,255));
            caseList1->setScrollStyle(HSCROLLING_ON|VSCROLLER_ALWAYS|VSCROLLING_ON);

        FXGroupBox *_gb1=new FXGroupBox(_vframe0,oLanguage->getText("str_rdgmdi_assayinfo"),GROUPBOX_TITLE_LEFT|LAYOUT_FILL_X|FRAME_GROOVE);
            FXVerticalFrame *_vframe03=new FXVerticalFrame(_gb1,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,5);
                FXHorizontalFrame *_hframe01=new FXHorizontalFrame(_vframe03,LAYOUT_FILL,0,0,0,0,0,0,0,0);
                    new FXLabel(_hframe01,oLanguage->getText("str_rdgmdi_caseInfo")+":",NULL,LAYOUT_FIX_WIDTH|JUSTIFY_LEFT,0,0,100);
                    assayId=new FXLabel(_hframe01,oLanguage->getText("str_rdgmdi_nosel")+"\n",NULL,JUSTIFY_LEFT);
                FXHorizontalFrame *_hframe02=new FXHorizontalFrame(_vframe03,LAYOUT_FILL,0,0,0,0,0,0,0,0);
                    new FXLabel(_hframe02,oLanguage->getText("str_rdgmdi_tplInfo")+":",NULL,LAYOUT_FIX_WIDTH|JUSTIFY_LEFT,0,0,100);
                    assayTpl=new FXLabel(_hframe02,oLanguage->getText("str_rdgmdi_nosel")+"\n",NULL,JUSTIFY_LEFT);
                FXHorizontalFrame *_hframe03=new FXHorizontalFrame(_vframe03,LAYOUT_FILL,0,0,0,0,0,0,0,0);
                    new FXLabel(_hframe03,oLanguage->getText("str_rdgmdi_assayTech")+":",NULL,LAYOUT_FIX_WIDTH|JUSTIFY_LEFT,0,0,100);
                    assayTech=new FXLabel(_hframe03,oLanguage->getText("str_rdgmdi_nosel"),NULL,JUSTIFY_LEFT);

                    assayId->setTextColor(FXRGB(50,50,180));
                    assayTpl->setTextColor(FXRGB(50,50,180));
                    assayTech->setTextColor(FXRGB(50,50,180));

    tiStats=new FXTabItem(tbMain,oLanguage->getText("str_rdgmdi_stats"));
        FXVerticalFrame *_vframe1=new FXVerticalFrame(tbMain,FRAME_RAISED|FRAME_THICK|LAYOUT_FILL);
        FXHorizontalFrame *_hframe100=new FXHorizontalFrame(_vframe1,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,5,0);
        new FXLabel(_hframe100,oLanguage->getText("str_rdgmdi_primary_st"));
        lbPlates2=new FXLabel(_hframe100,(char*)NULL);
        vars=new FXMenuPane(this);
        mcCala=new FXMenuCheck(vars,oLanguage->getText("str_asymdi_cala"),this,ID_CALCMENU);
        mcCalb=new FXMenuCheck(vars,oLanguage->getText("str_asymdi_calb"),this,ID_CALCMENU);
        mcEU=new FXMenuCheck(vars,oLanguage->getText("str_rdgmdi_eu"),this,ID_CALCMENU);
        mcTiter=new FXMenuCheck(vars,oLanguage->getText("str_asymdi_titer"),this,ID_CALCMENU);
        mcLog2=new FXMenuCheck(vars,oLanguage->getText("str_rdgmdi_log2"),this,ID_CALCMENU);
        mcTiters=new FXMenuCheck(vars,oLanguage->getText("str_asymdi_titers"),this,ID_CALCMENU);
        mcBins=new FXMenuCheck(vars,oLanguage->getText("str_asymdi_bins"),this,ID_CALCMENU);
        //mcBins2=new FXMenuCheck(vars,oLanguage->getText("str_asymdi_bins2"),this,ID_CALCMENU);
        mcAge=new FXMenuCheck(vars,oLanguage->getText("str_rdgmdi_clage"),this,ID_CALCMENU);
        mcBreed1=new FXMenuCheck(vars,oLanguage->getText("str_dlg_pop_breed1"),this,ID_CALCMENU);
        mcBreed2=new FXMenuCheck(vars,oLanguage->getText("str_dlg_pop_breed2"),this,ID_CALCMENU);
        mcResult=new FXMenuCheck(vars,oLanguage->getText("str_rdgmdi_result"),this,ID_CALCMENU);
        mcResult2=new FXMenuCheck(vars,oLanguage->getText("str_rdgmdi_result2"),this,ID_CALCMENU);
        new FXMenuSeparator(vars);
        mcAm=new FXMenuCheck(vars,oLanguage->getText("str_rdgmdi_mean"),NULL,ID_CALCMENU);
        mbCalc=new FXMenuButton(_hframe100,oLanguage->getText("str_rdgmdi_mbcalc"),NULL,vars,ICON_AFTER_TEXT|LAYOUT_RIGHT|MENUBUTTON_DOWN|FRAME_RAISED);
        mbCalc->setTextColor(FXRGB(180,0,0));
        showStats=new FXCheckButton(_hframe100,oLanguage->getText("str_rdgmdi_showstats"),this,ID_SHOWSTATS,ICON_BEFORE_TEXT|LAYOUT_RIGHT);
        showStats->setCheck(true);

        FXVerticalFrame *_vframe11=new FXVerticalFrame(_vframe1,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
            plateTable2=new cColorTable(_vframe11,this,ID_PLATETABLE2,TABLE_COL_SIZABLE|TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT|LAYOUT_FILL,0,0,0,181);
            plateTable2->setGridColor(FXRGB(230,230,230));
            plateTable2->setTableSize(8,12);
            plateTable2->setDefColumnWidth(87);
            plateTable2->setColumnHeaderHeight(22);
            plateTable2->setRowHeaderWidth(81);
            plateTable2->setSelBackColor(FXRGB(100,100,140));
            plateTable2->setCellBorderWidth(1);
            plateTable2->setSelTextColor(0);
            plateTable2->setStippleColor(FXRGB(250,250,240));
            plateTable2->setScrollStyle(HSCROLLING_ON|VSCROLLING_ON|VSCROLLER_ALWAYS|SCROLLERS_TRACK);
        statsFrame=new FXVerticalFrame(_vframe1,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
        _gb10=new FXGroupBox(statsFrame,oLanguage->getText("str_rdgmdi_caseinfo"),GROUPBOX_TITLE_LEFT|LAYOUT_FILL_X|FRAME_GROOVE);
            FXVerticalFrame *_vframe12=new FXVerticalFrame(_gb10,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
            caseList2=new cColorTable(_vframe12,this,ID_CASELIST2,TABLE_NO_COLSELECT|LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT,0,0,0,75);
            caseList2->setDefRowHeight(18);
            caseList2->setTableSize(0,7);
            caseList2->setRowHeaderWidth(0);
            caseList2->setColumnText(0,oLanguage->getText("str_rdgmdi_cllayout"));
            caseList2->setColumnWidth(0,80);
            caseList2->setColumnText(1,oLanguage->getText("str_rdgmdi_clid"));
            caseList2->setColumnWidth(1,90);
            caseList2->setColumnText(2,oLanguage->getText("str_rdgmdi_clcount"));
            caseList2->setColumnWidth(2,48);
            caseList2->setColumnText(3,oLanguage->getText("str_rdgmdi_clpop"));
            caseList2->setColumnWidth(3,90);
            caseList2->setColumnText(4,oLanguage->getText("str_rdgmdi_clasy"));
            caseList2->setColumnWidth(4,90);
            caseList2->setColumnText(5,oLanguage->getText("str_rdgmdi_clage"));
            caseList2->setColumnWidth(5,32);
            caseList2->setColumnText(6,oLanguage->getText("str_rdgmdi_clrepl"));
            caseList2->setColumnWidth(6,40);
            caseList2->getColumnHeader()->setItemJustify(0,JUSTIFY_CENTER_X);
            caseList2->getColumnHeader()->setItemJustify(2,JUSTIFY_CENTER_X);
            caseList2->getColumnHeader()->setItemJustify(5,JUSTIFY_CENTER_X);
            caseList2->getColumnHeader()->setItemJustify(6,JUSTIFY_CENTER_X);
            caseList2->setGridColor(FXRGB(255,255,255));
            caseList2->setCellBorderWidth(1);
            caseList2->setColumnHeaderHeight(22);
            caseList2->showHorzGrid(true);
            caseList2->showVertGrid(false);
            caseList2->setSelTextColor(0);
            caseList2->setCellBorderColor(FXRGB(255,0,0));
            caseList2->setSelBackColor(FXRGB(180,180,255));
            caseList2->setScrollStyle(HSCROLLING_ON|VSCROLLER_ALWAYS|VSCROLLING_ON);

        FXGroupBox *_gb11=new FXGroupBox(statsFrame,oLanguage->getText("str_rdgmdi_statinfo"),GROUPBOX_TITLE_LEFT|LAYOUT_FILL_X|FRAME_GROOVE);
            FXHorizontalFrame *_hframe121=new FXHorizontalFrame(_gb11,LAYOUT_FILL_X|FRAME_NONE,0,0,0,0,0,0,0,0);
            FXHorizontalFrame *_hframe120=new FXHorizontalFrame(_hframe121,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK|JUSTIFY_CENTER_Y,0,0,0,0,0,0,0,0);
            statsTable=new cColorTable(_hframe120,this,ID_STATSTABLE,TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT|TABLE_READONLY|TABLE_COL_SIZABLE|LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT,0,0,0,120);
            statsTable->setSelectable(false);
            statsTable->setDefRowHeight(17);
            statsTable->setColumnHeaderHeight(17);
            statsTable->setTableSize(6,0);
            statsTable->setRowHeaderWidth(60);
            statsTable->getCornerButton()->setButtonStyle(BUTTON_TOOLBAR|BUTTON_AUTOGRAY);
            statsTable->setDefColumnWidth(87);
            statsTable->setRowText(0,oLanguage->getText("str_rdgmdi_min"));
            statsTable->setRowText(1,oLanguage->getText("str_rdgmdi_max"));
            statsTable->setRowText(2,oLanguage->getText("str_rdgmdi_amean"));
            statsTable->setRowText(3,oLanguage->getText("str_rdgmdi_gmean"));
            statsTable->setRowText(4,oLanguage->getText("str_rdgmdi_sd"));
            statsTable->setRowText(5,oLanguage->getText("str_rdgmdi_cv"));
            statsTable->getRowHeader()->setItemJustify(0,JUSTIFY_CENTER_X);
            statsTable->getRowHeader()->setItemJustify(1,JUSTIFY_CENTER_X);
            statsTable->getRowHeader()->setItemJustify(2,JUSTIFY_CENTER_X);
            statsTable->getRowHeader()->setItemJustify(3,JUSTIFY_CENTER_X);
            statsTable->getRowHeader()->setItemJustify(4,JUSTIFY_CENTER_X);
            statsTable->getRowHeader()->setItemJustify(5,JUSTIFY_CENTER_X);
            statsTable->setGridColor(FXRGB(200,200,200));
            statsTable->showHorzGrid(true);
            statsTable->showVertGrid(true);
            statsTable->setScrollStyle(HSCROLLING_ON|VSCROLLING_ON);
            lbStats=new FXLabel(_hframe121,(char*)NULL,NULL,LAYOUT_SIDE_RIGHT|LAYOUT_FILL_Y|LAYOUT_FIX_WIDTH,0,0,100,0);
            lbStats->setTextColor(FXRGB(0,0,100));
            lbStats->setJustify(JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);


    tiGraph=new FXTabItem(tbMain,oLanguage->getText("str_rdgmdi_graph"));
        FXVerticalFrame *_vframe2=new FXVerticalFrame(tbMain,FRAME_RAISED|FRAME_THICK|LAYOUT_FILL);
            FXHorizontalFrame *_hframe22=new FXHorizontalFrame(_vframe2,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                new FXLabel(_hframe22,oLanguage->getText("str_rdgmdi_primary_gp"));
                FXHorizontalFrame *_hframe223=new FXHorizontalFrame(_hframe22,FRAME_SUNKEN|LAYOUT_RIGHT,0,0,0,0,0,0,0,0,0,0);
                    bchPoint=new FXButton(_hframe223,(char*)NULL,new FXGIFIcon(getApp(),data_chart_point),this,CMD_CHPOINT,FRAME_RAISED);
                    bchLine=new FXButton(_hframe223,(char*)NULL,new FXGIFIcon(getApp(),data_chart_line),this,CMD_CHLINE,FRAME_RAISED);
                    bchBar=new FXButton(_hframe223,(char*)NULL,new FXGIFIcon(getApp(),data_chart_bar),this,CMD_CHBAR,FRAME_RAISED);
                    bchBar->setState(STATE_DOWN);
            FXVerticalFrame *_vframe22=new FXVerticalFrame(_vframe2,FRAME_SUNKEN|LAYOUT_FILL,0,0,0,0,0,0,0,0);
            chartWin=new FXScrollWindow(_vframe22,LAYOUT_FILL);
            chartFrame=new FXVerticalFrame(chartWin,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,1);
            chartFrame->setBackColor(FXRGB(190,190,190));

    FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe0,LAYOUT_FILL_Y|LAYOUT_RIGHT,0,0,0,0,5,0,0,0);
        new FXHorizontalSeparator(_vframe3,LAYOUT_FIX_HEIGHT,0,0,0,16);
        lbInv=new FXButton(_vframe3,(char*)NULL,NULL,this,ID_SHOWRULES,FRAME_RAISED|LAYOUT_FILL_X);
        lbInv->setBackColor(FXRGB(255,255,0));
        lbInv->setTextColor(FXRGB(255,0,0));
        lbInv->setJustify(JUSTIFY_CENTER_X);
        lbInv->setFont(new FXFont(getApp(),"Helvetica",9,FXFont::Bold));
        new FXButton(_vframe3,oLanguage->getText("str_but_close"),NULL,this,CMD_CLOSE,FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,80,0);
        new FXHorizontalSeparator(_vframe3,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT|LAYOUT_BOTTOM,0,0,0,10);

        reports=new FXMenuPane(this);
        new FXMenuCommand(reports,oLanguage->getText("str_mreport_0").before('\t'),new FXGIFIcon(getApp(),data_reports_ca),this,ID_REP_CA);
        new FXMenuCommand(reports,oLanguage->getText("str_mreport_2").before('\t'),new FXGIFIcon(getApp(),data_reports_cca),this,ID_REP_CC);
        new FXMenuCommand(reports,oLanguage->getText("str_mreport_6").before('\t'),new FXGIFIcon(getApp(),data_reports_pot),this,ID_REP_SB);
        new FXMenuCommand(reports,oLanguage->getText("str_mreport_1").before('\t'),new FXGIFIcon(getApp(),data_reports_mca),this,ID_REP_CM);
        new FXMenuCommand(reports,oLanguage->getText("str_mreport_7").before('\t'),new FXGIFIcon(getApp(),data_reports_vacd),this,ID_REP_TC);
        new FXMenuCommand(reports,oLanguage->getText("str_mreport_8").before('\t'),new FXGIFIcon(getApp(),data_reports_vacd),this,ID_REP_TB);
        new FXMenuSeparator(reports);
        new FXMenuCommand(reports,oLanguage->getText("str_mreport_3").before('\t'),new FXGIFIcon(getApp(),data_reports_vacc),this,ID_REP_VH);
        new FXMenuCommand(reports,oLanguage->getText("str_mreport_4").before('\t'),new FXGIFIcon(getApp(),data_reports_vacd),this,ID_REP_VD);
        mbReports=new FXMenuButton(_vframe3,oLanguage->getText("str_rdgmdi_report"),NULL,reports,ICON_AFTER_TEXT|MENUBUTTON_DOWN|FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,80,0);

        new FXButton(_vframe3,oLanguage->getText("str_rdgmdi_template"),NULL,this,CMD_TEMPLATE,FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,80,0);
        new FXButton(_vframe3,oLanguage->getText("str_rdgmdi_editc"),NULL,this,CMD_CASEEDIT,FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,80,0);
        new FXHorizontalSeparator(_vframe3,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT|LAYOUT_BOTTOM,0,0,0,10);
        new FXButton(_vframe3,oLanguage->getText("str_rdgmdi_reread"),NULL,this,CMD_REREAD,FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,80,0);
        new FXButton(_vframe3,oLanguage->getText("str_rdgmdi_save"),NULL,this,CMD_SAVE,BUTTON_INITIAL|BUTTON_DEFAULT|FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,80,0);
    charts=NULL;
    tCasesCount=0;
    readingsWindow=oWinMain->getReadings()->no();
    oWinMain->getReadings()->insert(FXStringFormat("%c%c",readingsWindow/256+1,readingsWindow%256+1).text(),this);
}

cMDIReadings::~cMDIReadings()
{
    oWinMain->getReadings()->remove(FXStringFormat("%c%c",readingsWindow/256+1,readingsWindow%256+1).text());
    if(casesSets)
    {
        for(int i=0;i<tSetsCount;i++)
        {
            if(casesSets[i].assayRes)
                delete casesSets[i].assayRes;
            if(casesSets[i].templateRes)
                delete casesSets[i].templateRes;
            for(int j=0;j<casesSets[i].casesCount;j++)
            {
                free(casesSets[i].cases[j].data);
                free(casesSets[i].cases[j].controlsData);
            }
            free(casesSets[i].cases);
        }
        free(casesSets);
        for(int i=0;i<plateTable1->getNumRows();i++)
            for(int j=0;j<plateTable1->getNumColumns();j++)
                if(plateTable1->getItemData(i,j)!=NULL)
                    delete (cLocInfo*)plateTable1->getItemData(i,j);
    }
    for(int i=0;i<tCasesCount;i++)
        delete charts[i];
    if(charts)
    {
        free(charts);
        delete vars;
        delete edit;
        delete reports;
        delete mbCalc;
        delete mbEdit;
        delete mbReports;
    }
}

void cMDIReadings::create()
{
    hide();
    cMDIChild::create();
    mcCala->setCheck(true);
    mcAm->setCheck(true);
    mcAmCheck=true;
}

FXbool cMDIReadings::canClose(void)
{
    plateTable1->acceptInput(true);
    plateTable2->acceptInput(true);
    if(!saved)
    {
        setFocus();
        int res=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),oLanguage->getText("str_qsave").text());
        switch(res)
        {
            case MBOX_CLICKED_YES:
                if(!saveData())
                    return false;
                break;
            case MBOX_CLICKED_CANCEL:
                return false;
            default:
                break;
        }
    }
    return true;
}

FXbool cMDIReadings::newReading(cDataResult *prResAssay,cDataResult *prResTemplate,int prPlateCount,double **prPlateData,FXString prLot, FXString prExpirationDate)
{
    if(!prResAssay || !prResTemplate || !prPlateCount || !prPlateData)
        return false;

    casesSets=(sCasesSet*)malloc(sizeof(sCasesSet));
    if(!casesSets)
    {
        tPlatesCount=0;
        tSetsCount=0;
        tCasesCount=0;
        saved=true;
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return false;
    }

    tSetsCount=1;
    tPlatesCount=casesSets[0].platesCount=prPlateCount;
    FXString __cases_defs=prResTemplate->getCellString(0,5);
    tCasesCount=casesSets[0].casesCount=FXIntVal(__cases_defs.before('\n'));

    casesSets[0].cases=(sCaseEntry*)malloc(tCasesCount*sizeof(sCaseEntry));
    if(!casesSets[0].cases)
    {
        tPlatesCount=0;
        tSetsCount=0;
        tCasesCount=0;
        saved=true;
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return false;
    }

    FXchar ftime[100];
    time_t curtime;
    struct tm *loctime;
    curtime=time(NULL);
    loctime=localtime(&curtime);
    strftime(ftime,100,"%Y-%m-%d",loctime);

    casesSets[0].templateRes=prResTemplate;
    casesSets[0].orientation=prResTemplate->getCellInt(0,2);
    casesSets[0].alelisa=prResTemplate->getCellInt(0,3)==2?true:false;

    bool __alelisa=false;
    if(casesSets[0].alelisa && casesSets[0].orientation==1)
        __alelisa=true;
    int __alelisa_multiplier=__alelisa?2:1;

    casesSets[0].assayRes=prResAssay;
    areTiters=FXIntVal(prResAssay->getCellString(0,7).before('\t'))>0?true:false;
    areSecondary=FXIntVal(prResAssay->getCellString(0,6).before('\n'))>1?true:false;
    casesSets[0].sampleCount=0;
    FXString *stime=new FXString(ftime);
    FXString *stech=new FXString(oWinMain->getTechnician());
    FXString *sreason=new FXString("---");
    FXString *slot=new FXString(prLot);
    FXString *sexp=new FXString(prExpirationDate);
    FXString *ssptype=new FXString("---");
    FXString *scomments=new FXString();
    FXString *sassay=new FXString(prResAssay->getCellString(0,0));
    FXString *scala;
    FXString *scalb=NULL;
    FXString *stpl=new FXString(prResTemplate->getCellString(0,0));
    FXString *scalaop;
    FXString *scalbop=NULL;
    FXString *scdefs=new FXString(prResTemplate->getCellString(0,4));
    FXString *svet=new FXString("---");
    curtime=time(NULL);
    loctime=localtime(&curtime);
    strftime(ftime,100,"%s",loctime);
    FXString *sroid=new FXString(ftime);
    srand(curtime);
    *sroid=*sroid+FXStringVal(rand());
    FXString *calapco,*calasco,*calbpco=NULL,*calbsco=NULL;

    FXString calculations_defs=prResAssay->getCellString(0,6);
    casesSets[0].useSecond=FXIntVal(calculations_defs.before('\n'))==2?true:false;
    calculations_defs=calculations_defs.after('\n');

    scala=new FXString(calculations_defs.before('\t'));
    calculations_defs=calculations_defs.after('\t');
    calapco=new FXString(calculations_defs.before('\t'));
    calculations_defs=calculations_defs.after('\t');
    scalaop=new FXString(calculations_defs.before('\t'));
    calculations_defs=calculations_defs.after('\t');
    calasco=new FXString(calculations_defs.before('\n'));
    calculations_defs=calculations_defs.after('\n');

    if(casesSets[0].useSecond)
    {
        scalb=new FXString(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        calbpco=new FXString(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        scalbop=new FXString(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        calbsco=new FXString(calculations_defs.before('\n'));
        calculations_defs=calculations_defs.after('\n');
    }

    casesSets[0].assay=sassay;
    casesSets[0].assaytitle=new FXString(prResAssay->getCellString(0,1));
    casesSets[0].cala=scala;
    casesSets[0].calb=scalb;
    casesSets[0].calaop=scalaop;
    casesSets[0].calbop=scalbop;
    casesSets[0].calapco=calapco;
    casesSets[0].calbpco=calbpco;
    casesSets[0].calasco=*calasco!="0"?calasco:calapco;
    casesSets[0].calbsco=(casesSets[0].useSecond && *calbsco!="0")?calbsco:calbpco;

    FXString factors_defs=prResAssay->getCellString(0,5);
    for(int i=0;i<4;i++)
    {
        casesSets[0].factors[i]=FXIntVal(factors_defs.before('\t'));
        factors_defs=factors_defs.after('\t');
    }

    FXString titers_defs=prResAssay->getCellString(0,7);
    casesSets[0].useTiters=FXIntVal(titers_defs.before('\t'))!=0?true:false;
    titers_defs=titers_defs.after('\t');
    casesSets[0].slope=FXFloatVal(titers_defs.before('\t'));
    titers_defs=titers_defs.after('\t');
    casesSets[0].intercept=FXFloatVal(titers_defs.before('\t'));
    titers_defs=titers_defs.after('\t');
    casesSets[0].calt=new FXString(titers_defs.before('\n'));
    titers_defs=titers_defs.after('\n');
    for(int i=0;i<30;i++)
    {
        casesSets[0].titerGroups[i]=FXIntVal(titers_defs.before('\t'));
        titers_defs=titers_defs.after('\t');
    }

    FXString bins_defs=prResAssay->getCellString(0,8);
    for(int i=0;i<30;i++)
    {
        casesSets[0].ratioGroups[i]=FXFloatVal(bins_defs.before('\t'));
        bins_defs=bins_defs.after('\t');
    }

    cDataResult *popl;
    for(int i=0;i<casesSets[0].casesCount;i++)
    {
        casesSets[0].cases[i].parentSet=0;
        __cases_defs=__cases_defs.after('\n');

        casesSets[0].cases[i].id=new FXString(__cases_defs.before('\t'));
        __cases_defs=__cases_defs.after('\t');

        casesSets[0].cases[i].count=FXIntVal(__cases_defs.before('\t'));
        __cases_defs=__cases_defs.after('\t');
        casesSets[0].sampleCount+=casesSets[0].cases[i].count*__alelisa_multiplier;

        casesSets[0].cases[i].alelisa=__alelisa?1:0;
        casesSets[0].cases[i].readingDate=stime;
        casesSets[0].cases[i].technician=stech;
        casesSets[0].cases[i].veterinarian=svet;
        casesSets[0].cases[i].reason=sreason;
        casesSets[0].cases[i].lot=slot;
        casesSets[0].cases[i].reading_oid=sroid;
        casesSets[0].cases[i].tpl=stpl;
        casesSets[0].cases[i].controls_defs=scdefs;
        casesSets[0].cases[i].expirationDate=sexp;
        casesSets[0].cases[i].spType=ssptype;
        casesSets[0].cases[i].comments=scomments;

        casesSets[0].cases[i].population=new FXString(__cases_defs.before('\t'));
        __cases_defs=__cases_defs.after('\t');

        popl=cPopulationManager::getPopulation(*casesSets[0].cases[i].population);
        if(popl==NULL || !popl->getRowCount())
        {
            casesSets[0].cases[i].breed1=new FXString(oLanguage->getText("str_na"));
            casesSets[0].cases[i].breed2=new FXString(oLanguage->getText("str_na"));
        }
        else
        {
            casesSets[0].cases[i].breed1=new FXString(popl->getCellString(0,3));
            casesSets[0].cases[i].breed2=new FXString(popl->getCellString(0,4));
        }
        if(popl!=NULL)
            delete popl;

        casesSets[0].cases[i].assay=sassay;

        casesSets[0].cases[i].bleedDate=new FXString(__cases_defs.before('\t'));
        __cases_defs=__cases_defs.after('\t');

        casesSets[0].cases[i].age=new FXString(__cases_defs.before('\t'));
        __cases_defs=__cases_defs.after('\t');

        casesSets[0].cases[i].replicates=FXIntVal(__cases_defs.before('\t'));
        __cases_defs=__cases_defs.after('\t');

        casesSets[0].cases[i].factor=FXIntVal(__cases_defs.before('\t'));
        __cases_defs=__cases_defs.after('\t');
    }
    tSamplesCount=casesSets[0].sampleCount;

    FXString __controls_defs(*scdefs);
    __controls_defs=__controls_defs.after('\n');
    casesSets[0].controlsPerPlate=0;
    int value;
    do{
        value=FXIntVal(__controls_defs.before('\t'));
        __controls_defs=__controls_defs.after('\t');
        if(value!=0 && value!=-100)
            casesSets[0].controlsLayout[casesSets[0].controlsPerPlate++]=value;
        else
            break;
    }while(true);

    this->sortControls(casesSets[0].controlsLayout,casesSets[0].controlsPerPlate);

    int casei=0;
    int platei=0;
    int stocki=96/__alelisa_multiplier*prPlateCount;
    int datai=0;
    int casepi=0;
    int samplei=0;
    bool newp=true;
    double var;

    while(stocki>0)
    {
        if(datai>=(96/__alelisa_multiplier))
        {
            newp=true;
            datai=0;
            platei++;
        }
        if(!isControlPosition(datai,casesSets[0].controlsLayout,casesSets[0].controlsPerPlate,__alelisa))
        {
            if(samplei==0)
            {
                casesSets[0].cases[casei].data=(double*)malloc(casesSets[0].cases[casei].count*
                                                               (casesSets[0].cases[casei].replicates+1)*
                                                               __alelisa_multiplier*sizeof(double));

                if(!casesSets[0].cases[casei].data)
                {
                    tPlatesCount=0;
                    tSetsCount=0;
                    tCasesCount=0;
                    saved=true;
                    FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                    return false;
                }
                casesSets[0].cases[casei].startCell=datai;
                casesSets[0].cases[casei].startPlate=platei;
            }
            if(samplei==0 || newp)
            {
                casepi++;
                casesSets[0].cases[casei].plateSpanCount=casepi;
                casesSets[0].cases[casei].controlsCount=casepi*casesSets[0].controlsPerPlate*__alelisa_multiplier;
                casesSets[0].cases[casei].controlsData=(double*)realloc(samplei==0?NULL:casesSets[0].cases[casei].controlsData,
                                                                        casesSets[0].cases[casei].controlsCount*sizeof(double));
                if(!casesSets[0].cases[casei].controlsData)
                {
                    tPlatesCount=0;
                    tSetsCount=0;
                    tCasesCount=0;
                    saved=true;
                    FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                    return false;
                }

                appendControls(casesSets[0].cases[casei].controlsData+(casepi-1)*casesSets[0].controlsPerPlate*__alelisa_multiplier,
                               prPlateData[platei],
                               casesSets[0].controlsLayout,
                               casesSets[0].controlsPerPlate,
                               casesSets[0].orientation,__alelisa);
            }
            var=prPlateData[platei][platePosition(datai,__alelisa,casesSets[0].orientation)];
            casesSets[0].cases[casei].data[samplei*__alelisa_multiplier]=var<0?0.000:var;
            if(__alelisa)
            {
                var=prPlateData[platei][platePosition(datai,__alelisa,casesSets[0].orientation,true)];
                casesSets[0].cases[casei].data[samplei*__alelisa_multiplier+1]=var<0?0.000:var;
            }
            samplei++;
            if(samplei>=(casesSets[0].cases[casei].count*(casesSets[0].cases[casei].replicates+1)))
            {
                samplei=0;
                casepi=0;
                casei++;
            }
            if(casei>=tCasesCount)
                break;

            newp=false;
        }
        stocki--;
        datai++;
    }

    caseList1->removeRows(0,caseList1->getNumRows());
    caseList1->insertRows(0,tCasesCount);
    caseList1->layout();
    caseList2->removeRows(0,caseList2->getNumRows());
    caseList2->insertRows(0,tCasesCount);
    caseList2->layout();

    charts=(cVChart**)malloc(tCasesCount*sizeof(cVChart*));
    if(!charts)
    {
        tPlatesCount=0;
        tSetsCount=0;
        tCasesCount=0;
        saved=true;
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return false;
    }

    for(int i=0;i<tCasesCount;i++)
    {
        for(int j=0;j<caseList1->getNumColumns();j++)
        {
            caseList1->setItemJustify(i,j,JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
            caseList1->setCellColor(i,j,cColorsManager::getColor(i));
            caseList1->setCellEditable(i,j,false);
            caseList2->setItemJustify(i,j,JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
            caseList2->setCellColor(i,j,cColorsManager::getColor(i));
            caseList2->setCellEditable(i,j,false);
        }
        caseList1->setCellColor(i,0,cColorsManager::getColor(i,-30));
        caseList1->setItemJustify(i,1,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
        caseList1->setItemJustify(i,3,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
        caseList1->setItemJustify(i,4,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
        caseList2->setCellColor(i,0,cColorsManager::getColor(i,-30));
        caseList2->setItemJustify(i,1,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
        caseList2->setItemJustify(i,3,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
        caseList2->setItemJustify(i,4,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);

        caseList1->setItemData(i,0,&casesSets[0].cases[i]);
        caseList2->setItemData(i,0,&casesSets[0].cases[i]);
        caseList1->setItemText(i,0,FXStringFormat("(%s) P%d:",sampleKey(i).text(),casesSets[0].cases[i].startPlate+1)+
                               displayPosition(casesSets[0].cases[i].startCell,__alelisa,casesSets[0].orientation));
        caseList2->setItemText(i,0,caseList1->getItemText(i,0));
        caseList1->setItemText(i,1,*casesSets[0].cases[i].id);
        caseList2->setItemText(i,1,*casesSets[0].cases[i].id);
        caseList1->setItemText(i,2,FXStringFormat("%d",casesSets[0].cases[i].count));
        caseList2->setItemText(i,2,FXStringFormat("%d",casesSets[0].cases[i].count));
        caseList1->setItemText(i,3,*casesSets[0].cases[i].population);
        caseList2->setItemText(i,3,*casesSets[0].cases[i].population);
        caseList1->setItemText(i,4,*casesSets[0].assay);
        caseList2->setItemText(i,4,*casesSets[0].assay);
        caseList1->setItemText(i,5,*casesSets[0].cases[i].age);
        caseList2->setItemText(i,5,*casesSets[0].cases[i].age);
        caseList1->setItemText(i,6,FXStringFormat("%d",casesSets[0].cases[i].replicates));
        caseList2->setItemText(i,6,FXStringFormat("%d",casesSets[0].cases[i].replicates));

        charts[i]=new cVChart(chartFrame,cVChart::CHART_BAR,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT|LAYOUT_TOP,0,0,600,400);
        charts[i]->setTitle(oLanguage->getText("str_rdgmdi_plate")+FXStringFormat(" %d",casesSets[0].cases[i].startPlate+1)+" - "+(*casesSets[0].cases[i].id)
                            +" - "+(*casesSets[0].cases[i].population)+" - "+(*casesSets[0].assay));
        charts[i]->create();
        charts[i]->recalc();
    }

    chartFrame->recalc();
    chartWin->recalc();

    mcCala->setCheck(true);
    mcCalb->setCheck(false);
    mcEU->setCheck(false);
    mcResult->setCheck(true);
    mcResult2->setCheck(false);
    mcAge->setCheck(false);
    mcBreed1->setCheck(false);
    mcBreed2->setCheck(false);
    mcLog2->setCheck(false);
    //mcBins2->setCheck(false);
    if(areTiters)
    {
        mcTiter->setCheck(true);
        mcTiters->setCheck(true);
        mcBins->setCheck(false);
    }
    else
    {
        mcTiter->setCheck(false);
        mcTiters->setCheck(false);
        mcBins->setCheck(true);
    }

    lbPlates->setText(FXStringFormat(" - %d %s",tPlatesCount,oLanguage->getText("str_rdgmdi_plates").text()));
    lbPlates2->setText(lbPlates->getText());
    _gb0->setText(FXStringFormat("%s - %d %s",oLanguage->getText("str_rdgmdi_caseinfo").text(),tCasesCount,oLanguage->getText("str_rdgmdi_caseslsx").text()));
    _gb10->setText(FXStringFormat("%s - %d %s",oLanguage->getText("str_rdgmdi_caseinfo").text(),tCasesCount,oLanguage->getText("str_rdgmdi_caseslsx").text()));

    plateTable1->setTableSize(8*tPlatesCount,12);
    plateTable1->layout();
    plateTable2->setTableSize(0,0);
    selectCase(0);
    updateData();
    onRszPlateTable1(NULL,0,NULL);
    onRszCaseList1(NULL,0,NULL);
    onRszCaseList2(NULL,0,NULL);
    tiPrimary->setFocus();
    newdata=true;
    show();
    return true;
}

FXbool cMDIReadings::loadReadings(cSortList &prCases)
{
    FXString criteria="";
    int j=0;
    for(int i=0;i<prCases.getNumItems();i++)
        if(prCases.isItemSelected(i))
        {
            if(j>0)
                criteria=criteria+" OR ";
            criteria=criteria+"(assay_oid='"+prCases.getItemText(i).before('\t')+"' AND "+"id='"+prCases.getItemText(i).after('\t').before('\t')+"')";
            j++;
        }
    if(!j)
        return false;
    cDataResult *resr=oDataLink->execute("SELECT DISTINCT reading_oid FROM t_gncases WHERE "+criteria+" ORDER BY assay_oid ASC;");
    tSetsCount=resr->getRowCount();
    if(!tSetsCount)
        return false;
    cDataResult *resc,*resp;
    casesSets=(sCasesSet*)malloc(tSetsCount*sizeof(sCasesSet));
    if(!casesSets)
    {
        tPlatesCount=0;
        tSetsCount=0;
        tCasesCount=0;
        saved=true;
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return false;
    }
    tCasesCount=0;
    tSamplesCount=0;
    tPlatesCount=0;
    areTiters=false;
    areSecondary=false;
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        FXString __reading_oid(resr->getCellString(setsi,0));
        resc=oDataLink->execute("SELECT * FROM t_gncases WHERE ("+criteria+") AND reading_oid='"+__reading_oid+"' ORDER BY (startPlate*100+startCell) ASC;");
        casesSets[setsi].casesCount=resc->getRowCount();
        tCasesCount+=casesSets[setsi].casesCount;
        casesSets[setsi].cases=(sCaseEntry*)malloc(casesSets[setsi].casesCount*sizeof(sCaseEntry));
        if(!casesSets[setsi].cases)
        {
            tPlatesCount=0;
            tSetsCount=0;
            tCasesCount=0;
            saved=true;
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
            return false;
        }

        FXString __assay(resc->getCellString(0,6));
        FXString __template(resc->getCellString(0,16));
        casesSets[setsi].assayRes=oDataLink->execute("SELECT * FROM t_assays WHERE id='"+__assay+"';");
        casesSets[setsi].templateRes=oDataLink->execute("SELECT * FROM t_templates WHERE id='"+__template+"';");

        casesSets[setsi].assay=new FXString(__assay);
        casesSets[setsi].assaytitle=new FXString(casesSets[setsi].assayRes->getCellString(0,1));
        casesSets[setsi].orientation=resc->getCellInt(0,17);
        {
            int k=resc->getCellInt(0,18);
            casesSets[setsi].alelisa=k==2?true:false;
        }
        int __alelisa=casesSets[setsi].alelisa;
        int __alelisa_multiplier=__alelisa?2:1;

        FXString __lot(resc->getCellString(0,7));
        FXString __expDate(resc->getCellString(0,8));

        FXString calculations_defs=casesSets[setsi].assayRes->getCellString(0,6);
        casesSets[setsi].useSecond=FXIntVal(calculations_defs.before('\n'))==2?true:false;
        if(casesSets[setsi].useSecond)
            areSecondary=true;
        calculations_defs=calculations_defs.after('\n');

        casesSets[setsi].cala=new FXString(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        casesSets[setsi].calapco=new FXString(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        casesSets[setsi].calaop=new FXString(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        casesSets[setsi].calasco=new FXString(calculations_defs.before('\n'));
        calculations_defs=calculations_defs.after('\n');

        if(casesSets[0].useSecond)
        {
            casesSets[setsi].calb=new FXString(calculations_defs.before('\t'));
            calculations_defs=calculations_defs.after('\t');
            casesSets[setsi].calbpco=new FXString(calculations_defs.before('\t'));
            calculations_defs=calculations_defs.after('\t');
            casesSets[setsi].calbop=new FXString(calculations_defs.before('\t'));
            calculations_defs=calculations_defs.after('\t');
            casesSets[setsi].calbsco=new FXString(calculations_defs.before('\n'));
            calculations_defs=calculations_defs.after('\n');
        }
        else
        {
            casesSets[setsi].calb=NULL;
            casesSets[setsi].calbop=NULL;
            casesSets[setsi].calbpco=new FXString("0");
            casesSets[setsi].calbsco=new FXString("0");
        }

        if(*casesSets[setsi].calasco=="0")
            *casesSets[setsi].calasco=*casesSets[setsi].calapco;
        if(*casesSets[setsi].calbsco=="0")
            *casesSets[setsi].calbsco=*casesSets[setsi].calbpco;

        FXString factors_defs=casesSets[setsi].assayRes->getCellString(0,5);
        for(int i=0;i<4;i++)
        {
            casesSets[setsi].factors[i]=FXIntVal(factors_defs.before('\t'));
            factors_defs=factors_defs.after('\t');
        }

        FXString titers_defs=casesSets[setsi].assayRes->getCellString(0,7);
        casesSets[setsi].useTiters=FXIntVal(titers_defs.before('\t'))!=0?true:false;
        if(casesSets[setsi].useTiters)
            areTiters=true;
        titers_defs=titers_defs.after('\t');
        casesSets[setsi].slope=FXFloatVal(titers_defs.before('\t'));
        titers_defs=titers_defs.after('\t');
        casesSets[setsi].intercept=FXFloatVal(titers_defs.before('\t'));
        titers_defs=titers_defs.after('\t');
        casesSets[setsi].calt=new FXString(titers_defs.before('\n'));
        titers_defs=titers_defs.after('\n');
        for(int i=0;i<30;i++)
        {
            casesSets[setsi].titerGroups[i]=FXIntVal(titers_defs.before('\t'));
            titers_defs=titers_defs.after('\t');
        }

        FXString bins_defs=casesSets[setsi].assayRes->getCellString(0,8);
        for(int i=0;i<30;i++)
        {
            casesSets[setsi].ratioGroups[i]=FXFloatVal(bins_defs.before('\t'));
            bins_defs=bins_defs.after('\t');
        }

        FXString __controls_defs(resc->getCellString(0,22));
        __controls_defs=__controls_defs.after('\n');
        casesSets[setsi].controlsPerPlate=0;
        int value;
        do{
            value=FXIntVal(__controls_defs.before('\t'));
            __controls_defs=__controls_defs.after('\t');
            if(value!=0 && value!=-100)
                casesSets[setsi].controlsLayout[casesSets[setsi].controlsPerPlate++]=value;
            else
                break;
        }while(true);
        this->sortControls(casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate);

        casesSets[setsi].platesCount=0;
        FXDict *pl=new FXDict();
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++)
        {
            casesSets[setsi].cases[casei].startCell=resc->getCellInt(casei,20);
            casesSets[setsi].cases[casei].startPlate=resc->getCellInt(casei,19);
            casesSets[setsi].cases[casei].plateSpanCount=resc->getCellInt(casei,21);

            int ps=casesSets[setsi].cases[casei].startPlate,pe=ps+casesSets[setsi].cases[casei].plateSpanCount-1;
            FXString key;
            for(int i=ps;i<=pe;i++)
                pl->insert(FXStringFormat("%c%c",i/256+1,i%256+1).text(),NULL);

            casesSets[setsi].cases[casei].id=new FXString(resc->getCellString(casei,0));
            casesSets[setsi].cases[casei].reading_oid=new FXString(__reading_oid);
            casesSets[setsi].cases[casei].count=resc->getCellInt(casei,15);
            casesSets[setsi].cases[casei].replicates=resc->getCellInt(casei,11);
            casesSets[setsi].cases[casei].factor=resc->getCellInt(casei,12);
            casesSets[setsi].cases[casei].alelisa=__alelisa;

            casesSets[setsi].sampleCount+=casesSets[0].cases[casei].count*__alelisa_multiplier;

            casesSets[setsi].cases[casei].age=new FXString(resc->getCellString(casei,14));
            casesSets[setsi].cases[casei].tpl=new FXString(__template);
            casesSets[setsi].cases[casei].veterinarian=new FXString(resc->getCellString(casei,5));
            casesSets[setsi].cases[casei].controls_defs=new FXString(resc->getCellString(casei,22));
            casesSets[setsi].cases[casei].readingDate=new FXString(resc->getCellString(casei,2));
            casesSets[setsi].cases[casei].technician=new FXString(resc->getCellString(casei,3));
            casesSets[setsi].cases[casei].reason=new FXString(resc->getCellString(casei,4));
            casesSets[setsi].cases[casei].lot=new FXString(__lot);
            casesSets[setsi].cases[casei].expirationDate=new FXString(__expDate);
            casesSets[setsi].cases[casei].spType=new FXString(resc->getCellString(casei,9));
            casesSets[setsi].cases[casei].bleedDate=new FXString(resc->getCellString(casei,10));
            casesSets[setsi].cases[casei].comments=new FXString(resc->getCellString(casei,24));
            casesSets[setsi].cases[casei].population=new FXString(resc->getCellString(casei,13));
            casesSets[setsi].cases[casei].assay=casesSets[setsi].assay;

            resp=oDataLink->execute("SELECT * FROM t_gnpopulations WHERE id='"+*casesSets[setsi].cases[casei].population+"';");
            casesSets[setsi].cases[casei].breed1=new FXString(resp->getCellString(casei,3));
            casesSets[setsi].cases[casei].breed2=new FXString(resp->getCellString(casei,4));
            resp->free();

            casesSets[setsi].cases[casei].parentSet=setsi;

            FXString __data(resc->getCellString(casei,23));
            FXString __controls=__data.before('\n');
            __data=__data.after('\n');

            casesSets[setsi].cases[casei].controlsCount=casesSets[setsi].cases[casei].plateSpanCount*__alelisa_multiplier*casesSets[setsi].controlsPerPlate;
            casesSets[setsi].cases[casei].controlsData=(double*)malloc(casesSets[setsi].cases[casei].controlsCount*sizeof(double));
            if(!casesSets[setsi].cases[casei].controlsData)
            {
                tPlatesCount=0;
                tSetsCount=0;
                tCasesCount=0;
                saved=true;
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                return false;
            }
            for(int i=0;i<casesSets[setsi].cases[casei].controlsCount;i++)
            {
                casesSets[setsi].cases[casei].controlsData[i]=FXFloatVal(__controls.before(' '));
                __controls=__controls.after(' ');
            }

            int ct=casesSets[setsi].cases[casei].count*__alelisa_multiplier*(casesSets[setsi].cases[casei].replicates+1);
            casesSets[setsi].cases[casei].data=(double*)malloc(ct*sizeof(double));
            if(!casesSets[setsi].cases[casei].data)
            {
                tPlatesCount=0;
                tSetsCount=0;
                tCasesCount=0;
                saved=true;
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                return false;
            }
            for(int i=0;i<ct;i++)
            {
                casesSets[setsi].cases[casei].data[i]=FXFloatVal(__data.before(' '));
                __data=__data.after(' ');
            }
        }
        casesSets[setsi].platesCount=pl->no();
        delete pl;
        tSamplesCount+=casesSets[setsi].sampleCount;
        tPlatesCount+=casesSets[setsi].platesCount;
        resc->free();
    }
    resr->free();

    caseList1->removeRows(0,caseList1->getNumRows());
    caseList1->insertRows(0,tCasesCount);
    caseList1->layout();
    caseList2->removeRows(0,caseList2->getNumRows());
    caseList2->insertRows(0,tCasesCount);
    caseList2->layout();

    if(charts)
        free(charts);

    charts=(cVChart**)malloc(tCasesCount*sizeof(cVChart*));
    if(!charts)
    {
        tPlatesCount=0;
        tSetsCount=0;
        tCasesCount=0;
        saved=true;
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return false;
    }

    int i=0;
    int p=0;
    bool incompatible=false,start=casesSets[0].alelisa;
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        if(start!=casesSets[setsi].alelisa)
        {
            incompatible=true;
            break;
        }
        int d=0,od=0;
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++,i++)
        {
            for(int j=0;j<caseList1->getNumColumns();j++)
            {
                caseList1->setItemJustify(i,j,JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
                caseList1->setCellColor(i,j,cColorsManager::getColor(i));
                caseList1->setCellEditable(i,j,false);
                caseList2->setItemJustify(i,j,JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
                caseList2->setCellColor(i,j,cColorsManager::getColor(i));
                caseList2->setCellEditable(i,j,false);
            }
            caseList1->setCellColor(i,0,cColorsManager::getColor(i,-30));
            caseList1->setItemJustify(i,1,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
            caseList1->setItemJustify(i,3,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
            caseList1->setItemJustify(i,4,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
            caseList2->setCellColor(i,0,cColorsManager::getColor(i,-30));
            caseList2->setItemJustify(i,1,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
            caseList2->setItemJustify(i,3,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);
            caseList2->setItemJustify(i,4,cColorTableItem::LEFT|cColorTableItem::CENTER_Y);

            caseList1->setItemData(i,0,&casesSets[setsi].cases[casei]);
            caseList2->setItemData(i,0,&casesSets[setsi].cases[casei]);
            caseList1->setItemText(i,1,*casesSets[setsi].cases[casei].id);
            caseList2->setItemText(i,1,*casesSets[setsi].cases[casei].id);
            caseList1->setItemText(i,2,FXStringFormat("%d",casesSets[setsi].cases[casei].count));
            caseList2->setItemText(i,2,FXStringFormat("%d",casesSets[setsi].cases[casei].count));
            caseList1->setItemText(i,3,*casesSets[setsi].cases[casei].population);
            caseList2->setItemText(i,3,*casesSets[setsi].cases[casei].population);
            caseList1->setItemText(i,4,*casesSets[setsi].assay);
            caseList2->setItemText(i,4,*casesSets[setsi].assay);
            caseList1->setItemText(i,5,*casesSets[setsi].cases[casei].age);
            caseList2->setItemText(i,5,*casesSets[setsi].cases[casei].age);
            caseList1->setItemText(i,6,FXStringFormat("%d",casesSets[setsi].cases[casei].replicates));
            caseList2->setItemText(i,6,FXStringFormat("%d",casesSets[setsi].cases[casei].replicates));

            charts[i]=new cVChart(chartFrame,cVChart::CHART_BAR,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT|LAYOUT_TOP,0,0,600,400);
            charts[i]->setTitle(oLanguage->getText("str_rdgmdi_plate")+FXStringFormat(" %d",p+1)+" - "+(*casesSets[setsi].cases[casei].id)
                            +" - "+(*casesSets[setsi].cases[casei].population)+" - "+(*casesSets[setsi].assay));
            charts[i]->create();
            charts[i]->recalc();

            d=casesSets[setsi].cases[casei].startPlate;
            if(d!=od)
            {
                p++;
                od=d;
            }
            caseList1->setItemText(i,0,FXStringFormat("(%s) P%d:",sampleKey(i).text(),p+1)+
                                   displayPosition(casesSets[setsi].cases[casei].startCell,casesSets[setsi].alelisa,casesSets[setsi].orientation));
            caseList2->setItemText(i,0,caseList1->getItemText(i,0));
            p+=casesSets[setsi].cases[casei].plateSpanCount-1;
        }
        p++;
    }

    mcCala->setCheck(true);
    mcCalb->setCheck(false);
    mcEU->setCheck(false);
    mcResult->setCheck(true);
    mcResult2->setCheck(false);
    mcAge->setCheck(false);
    mcBreed1->setCheck(false);
    mcBreed2->setCheck(false);
    mcLog2->setCheck(false);
    //mcBins2->setCheck(false);
    if(areTiters)
    {
        mcTiter->setCheck(true);
        mcTiters->setCheck(true);
        mcBins->setCheck(false);
    }
    else
    {
        mcTiter->setCheck(false);
        mcTiters->setCheck(false);
        mcBins->setCheck(true);
    }

    lbPlates->setText(FXStringFormat(" - %d %s",tPlatesCount,oLanguage->getText("str_rdgmdi_plates").text()));
    lbPlates2->setText(lbPlates->getText());
    _gb0->setText(FXStringFormat("%s - %d %s",oLanguage->getText("str_rdgmdi_caseinfo").text(),tCasesCount,oLanguage->getText("str_rdgmdi_caseslsx").text()));
    _gb10->setText(FXStringFormat("%s - %d %s",oLanguage->getText("str_rdgmdi_caseinfo").text(),tCasesCount,oLanguage->getText("str_rdgmdi_caseslsx").text()));

    if(incompatible)
    {
        tPlatesCount=0;
        tSetsCount=0;
        tCasesCount=0;
        saved=true;
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_reading_incompatcse").text());
        return false;
    }

    plateTable1->setTableSize(8*tPlatesCount,12);
    plateTable1->layout();
    plateTable2->setTableSize(0,0);
    newdata=false;
    saved=true;
    selectCase(0);
    onRszPlateTable1(NULL,0,NULL);
    onRszCaseList1(NULL,0,NULL);
    onRszCaseList2(NULL,0,NULL);
    tiPrimary->setFocus();
    show();
    return true;
}

long cMDIReadings::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{

    if(canClose())
    {
        close();
        return 1;
    }
    return 0;
}

long cMDIReadings::onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    return 1;
}

long cMDIReadings::onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData)
{
    saveData();
    return 1;
}

long cMDIReadings::onCopyPlateTable(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(tbMain->getCurrent()==0)
    {
        plateTable1->handle(this,FXSEL(SEL_COMMAND,FXTable::ID_COPY_SEL),NULL);
    }
    else if(tbMain->getCurrent()==1)
    {
        plateTable2->handle(this,FXSEL(SEL_COMMAND,FXTable::ID_COPY_SEL),NULL);
    }
    return 1;
}

long cMDIReadings::onPastePlateTable(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(tbMain->getCurrent()==0)
    {
        plateTable1->handle(this,FXSEL(SEL_COMMAND,FXTable::ID_PASTE_SEL),NULL);
        onCmdPlateTable1(NULL,0,NULL);
    }
    else if(tbMain->getCurrent()==1)
    {
        plateTable2->handle(this,FXSEL(SEL_COMMAND,FXTable::ID_PASTE_SEL),NULL);
        onCmdPlateTable2(NULL,0,NULL);
    }
    return 1;
}

long cMDIReadings::onSelallPlateTable(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(tbMain->getCurrent()==0)
    {
        plateTable1->handle(this,FXSEL(SEL_COMMAND,FXTable::ID_SELECT_ALL),NULL);
    }
    else if(tbMain->getCurrent()==1)
    {
        plateTable2->handle(this,FXSEL(SEL_COMMAND,FXTable::ID_SELECT_ALL),NULL);
    }
    return 1;
}

long cMDIReadings::onRszPlateTable1(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int tw=plateTable1->getWidth()-plateTable1->verticalScrollBar()->getWidth();
    int cw=tw/13;
    int hw=tw-(cw*13)+cw;
    if(cw<30)
        cw=hw=30;
    plateTable1->setRowHeaderWidth(hw);
    for(tw=0;tw<12;tw++)
        plateTable1->setColumnWidth(tw,cw);
    return 1;
}

long cMDIReadings::onUpdSelPlateTable1(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int r=*((int*)prData),c=*((int*)prData+1);
    int sr,ssr=plateTable1->getSelStartRow(),ser=plateTable1->getSelEndRow();
    if(ssr<0 || ser<0)
        return 1;
    sr=ssr;
    if(ssr!=ser)
        sr=(ssr==plateTable1->getAnchorRow()?ser:ssr);
    int sc,ssc=plateTable1->getSelStartColumn(),sec=plateTable1->getSelEndColumn();
    if(ssc<0 || sec<0)
        return 1;
    sc=ssc;
    if(ssc!=sec)
        sc=(ssc==plateTable1->getAnchorColumn()?sec:ssc);
    for(int i=0;i<plateTable1->getNumRows();i++)
        plateTable1->getRowHeader()->setItemPressed(i,false);
    for(int i=0;i<12;i++)
        plateTable1->getColumnHeader()->setItemPressed(i,false);
    plateTable1->getRowHeader()->setItemPressed(sr,true);
    plateTable1->getColumnHeader()->setItemPressed(sc,true);

    if(r!=sr || c!=sc)
        return 1;

    if(!plateTable1->getItemData(sr,sc))
    {
        selectCase(-1);
        return 1;
    }

    selectCase(((cLocInfo*)plateTable1->getItemData(sr,sc))->caseIndex,sr/8);
    locateCaseLists(((cLocInfo*)plateTable1->getItemData(sr,sc))->caseIndex);
    locatePlateTable2(((cLocInfo*)plateTable1->getItemData(sr,sc))->caseIndex);
    return 1;
}

long cMDIReadings::onUpdSelPlateTable2(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int r=*((int*)prData),c=*((int*)prData+1);
    int sr,ssr=plateTable2->getSelStartRow(),ser=plateTable2->getSelEndRow();
    if(ssr<0 || ser<0)
        return 1;
    sr=ssr;
    if(ssr!=ser)
        sr=(ssr==plateTable2->getAnchorRow()?ser:ssr);
    int sc,ssc=plateTable2->getSelStartColumn(),sec=plateTable2->getSelEndColumn();
    if(ssc<0 || sec<0)
        return 1;
    sc=ssc;
    if(ssc!=sec)
        sc=(ssc==plateTable2->getAnchorColumn()?sec:ssc);

    if((r!=sr) || (c!=sc))
        return 1;

    if(!plateTable2->getItemData(sr,0))
    {
        selectCase(-1);
        return 1;
    }
    selectCase(((cLocInfo*)plateTable2->getItemData(sr,0))->caseIndex,sr/8);
    locateCaseLists(((cLocInfo*)plateTable2->getItemData(sr,0))->caseIndex);
    locatePlateTable1(((cLocInfo*)plateTable2->getItemData(sr,0))->caseIndex);
    return 1;
}

long cMDIReadings::onRszCaseList1(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int tw=caseList1->getWidth()-caseList1->verticalScrollBar()->getWidth();
    caseList1->setColumnWidth(6,tw-430);
    return 1;
}

long cMDIReadings::onCmdSelCaseList1(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int sr,ssr=caseList1->getSelStartRow(),ser=caseList1->getSelEndRow();
    if(ssr<0 || ser<0)
        return 1;
    sr=ssr;
    if(ssr!=ser)
        sr=(ssr==caseList1->getAnchorRow()?ser:ssr);
    for(int j=0;j<caseList1->getNumRows();j++)
        for(int i=1;i<caseList1->getNumColumns();i++)
        {
            caseList1->setItemBorders(j,i,0);
            caseList2->setItemBorders(j,i,0);
        }
    for(int i=1;i<caseList1->getNumColumns();i++)
    {
        caseList1->setCellColor(sr,i,cColorsManager::getColor(sr));
        caseList1->setItemBorders(sr,i,FXTableItem::TBORDER|FXTableItem::BBORDER);
        caseList2->setCellColor(sr,i,cColorsManager::getColor(sr));
        caseList2->setItemBorders(sr,i,FXTableItem::TBORDER|FXTableItem::BBORDER);
    }
    caseList1->setItemBorders(sr,1,FXTableItem::LBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    caseList1->setItemBorders(sr,caseList1->getNumColumns()-1,FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    caseList2->setItemBorders(sr,1,FXTableItem::LBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    caseList2->setItemBorders(sr,caseList1->getNumColumns()-1,FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    selectCase(sr);
    locateCaseLists(sr);
    locatePlateTable1(sr);
    locatePlateTable2(sr);
    return 1;
}

long cMDIReadings::onRszCaseList2(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int tw=caseList2->getWidth()-caseList2->verticalScrollBar()->getWidth();
    caseList2->setColumnWidth(6,tw-430);
    return 1;
}

long cMDIReadings::onCmdSelCaseList2(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int sr,ssr=caseList2->getSelStartRow(),ser=caseList2->getSelEndRow();
    if(ssr<0 || ser<0)
        return 1;
    sr=ssr;
    if(ssr!=ser)
        sr=(ssr==caseList2->getAnchorRow()?ser:ssr);
    for(int j=0;j<caseList1->getNumRows();j++)
        for(int i=1;i<caseList1->getNumColumns();i++)
        {
            caseList1->setItemBorders(j,i,0);
            caseList2->setItemBorders(j,i,0);
        }
    for(int i=1;i<caseList1->getNumColumns();i++)
    {
        caseList1->setCellColor(sr,i,cColorsManager::getColor(sr));
        caseList1->setItemBorders(sr,i,FXTableItem::TBORDER|FXTableItem::BBORDER);
        caseList2->setCellColor(sr,i,cColorsManager::getColor(sr));
        caseList2->setItemBorders(sr,i,FXTableItem::TBORDER|FXTableItem::BBORDER);
    }
    caseList1->setItemBorders(sr,1,FXTableItem::LBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    caseList1->setItemBorders(sr,caseList1->getNumColumns()-1,FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    caseList2->setItemBorders(sr,1,FXTableItem::LBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    caseList2->setItemBorders(sr,caseList1->getNumColumns()-1,FXTableItem::RBORDER|FXTableItem::TBORDER|FXTableItem::BBORDER);
    selectCase(sr);
    locateCaseLists(sr);
    locatePlateTable1(sr);
    locatePlateTable2(sr);
    return 1;
}

long cMDIReadings::onCmdPlateTable1(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    int r,c,rs,cs,re,ce;
    if(prData)
    {
        rs=re=*(int*)prData;
        cs=ce=*((int*)prData+1);
    }
    else
    {
        rs=FXMIN(plateTable1->getSelStartRow(),plateTable1->getSelEndRow());
        cs=FXMIN(plateTable1->getSelStartColumn(),plateTable1->getSelEndColumn());
        re=FXMAX(plateTable1->getSelStartRow(),plateTable1->getSelEndRow());
        ce=FXMAX(plateTable1->getSelStartColumn(),plateTable1->getSelEndColumn());
    }
    if(rs==-1 || cs==-1 || re==-1 || ce==-1)
    {
        rs=cs=0;
        re=plateTable1->getNumRows()-1;
        ce=plateTable1->getNumColumns()-1;
    }
    for(r=rs;r<=re;r++)
        for(c=cs;c<=ce;c++)
        {
            if(!plateTable1->getItemData(r,c))
                continue;
            FXString s=plateTable1->getItemText(r,c);
			if(s.empty())
				{
					 FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),
										  oLanguage->getText("str_invalid").text());
					 drawPlateTables();
					 return 1;
				}

            if(s.lower()=="under" || s=="-.---")
                s="0.000";
            if(s.lower()=="over" || s=="+.+++")
                s="4.001";

            for(int k=0;k<s.length();k++)
                if(s.at(k)!='-' &&
                   s.at(k)!='+' &&
                   s.at(k)!='.' &&
                   !isdigit(s.at(k)))
                        {
                             FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),
                                                  oLanguage->getText("str_invalid").text());
							 drawPlateTables();
                             return 1;
                        }
		}
    for(r=rs;r<=re;r++)
        for(c=cs;c<=ce;c++)
        {
            if(!plateTable1->getItemData(r,c))
                continue;
            cLocInfo *cell=(cLocInfo*)plateTable1->getItemData(r,c);
            int index=cell->caseIndex;
            sCasesSet *set=NULL;
            sCaseEntry *cas=NULL;
            int platel=cell->plate;

            if(index==-1)
                continue;
            else if(index<0)
            {
                set=&casesSets[cell->set];

                if(set->casesCount==0)
                    continue;
            }
            else
            {
                cas=(sCaseEntry*)caseList1->getItemData(index,0);
                set=&casesSets[cas->parentSet];
            }

            FXString s=plateTable1->getItemText(r,c);

            if(s.lower()=="under" || s=="-.---")
                s="0.000";
            if(s.lower()=="over" || s=="+.+++")
                s="4.001";

            double value=FXDoubleVal(s);
            bool __alelisa=false;
            int __alelisa_multiplier=1;
            value=value<0?0.000:value;
            if(index<0)
            {
                if(set->alelisa && set->orientation==1)
                    __alelisa=true;
                __alelisa_multiplier=__alelisa?2:1;
                for(int casei=0;casei<set->casesCount;casei++)
                {
                    if((platel>=set->cases[casei].startPlate) && (platel<=(set->cases[casei].startPlate+set->cases[casei].plateSpanCount-1)))
                        set->cases[casei].controlsData[(platel-set->cases[casei].startPlate)*set->controlsPerPlate*__alelisa_multiplier+cell->sampleLoc]=value;
                }
            }
            else
                cas->data[cell->sampleLoc]=value;
        }
    drawPlateTables();
    return 1;
}

long cMDIReadings::onCmdPlateTable2(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    int r,c,rs,cs,re,ce;
    if(prData)
    {
        rs=re=*(int*)prData;
        cs=ce=*((int*)prData+1);
    }
    else
    {
        rs=FXMIN(plateTable2->getSelStartRow(),plateTable2->getSelEndRow());
        cs=FXMIN(plateTable2->getSelStartColumn(),plateTable2->getSelEndColumn());
        re=FXMAX(plateTable2->getSelStartRow(),plateTable2->getSelEndRow());
        ce=FXMAX(plateTable2->getSelStartColumn(),plateTable2->getSelEndColumn());
    }
   for(r=rs;r<=re;r++)
        for(c=cs;c<=ce;c++)
        {
            if(!plateTable2->getItemData(r,c))
                continue;
            FXString s=plateTable2->getItemText(r,c);

			if(s.empty())
				{
					 FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),
										  oLanguage->getText("str_invalid").text());
					 drawPlateTables();
					 return 1;
				}

            if(s.lower()=="under" || s=="-.---")
                s="0.000";
            if(s.lower()=="over" || s=="+.+++")
                s="4.001";

            for(int k=0;k<s.length();k++)
                if(s.at(k)!='-' &&
                   s.at(k)!='+' &&
                   s.at(k)!='.' &&
                   !isdigit(s.at(k)))
                        {
                             FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),
                                                  oLanguage->getText("str_invalid").text());
							 drawPlateTables();
                             return 1;
                        }
		}
    for(r=rs;r<=re;r++)
        for(c=cs;c<=cs;c++)
        {
            if(!plateTable2->getItemData(r,c))
                continue;
            cLocInfo *cell=(cLocInfo*)plateTable2->getItemData(r,c);
            int index=cell->caseIndex;
            sCasesSet *set=NULL;
            sCaseEntry *cas=NULL;
            int platel=cell->plate;

            if(index==-1)
                continue;
            else if(index<0)
            {
                set=&casesSets[cell->set];

                if(set->casesCount==0)
                    continue;
            }
            else
            {
                cas=(sCaseEntry*)caseList1->getItemData(index,0);
                set=&casesSets[cas->parentSet];
            }

            FXString s=plateTable2->getItemText(r,c);

            if(s.lower()=="under" || s=="-.---")
                s="0.000";
            if(s.lower()=="over" || s=="+.+++")
                s="4.001";

            double value=FXFloatVal(s);
            bool __alelisa=false;
            int __alelisa_multiplier=1;
            value=value<0?0.000:value;
            if(index<0)
            {
                if(set->alelisa && set->orientation==1)
                    __alelisa=true;
                __alelisa_multiplier=__alelisa?2:1;
                for(int casei=0;casei<set->casesCount;casei++)
                {
                    if((platel>=set->cases[casei].startPlate) && (platel<=(set->cases[casei].startPlate+set->cases[casei].plateSpanCount-1)))
                        set->cases[casei].controlsData[(platel-set->cases[casei].startPlate)*set->controlsPerPlate*__alelisa_multiplier+cell->sampleLoc]=value;
                }
            }
            else
                cas->data[cell->sampleLoc]=value;
        }
    drawPlateTables();
    return 1;
}

long cMDIReadings::onCmdCalcSel(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int nc=1;
    if(mcCala->getCheck())
        nc++;
    if(mcCalb->getCheck())
        nc++;
    if(mcEU->getCheck())
        nc++;
    if(mcTiter->getCheck())
        nc++;
    if(mcLog2->getCheck())
        nc++;
    if(mcTiters->getCheck())
        nc++;
    if(mcAge->getCheck())
        nc++;
    if(mcBreed1->getCheck())
        nc++;
    if(mcBreed2->getCheck())
        nc++;
    if(mcBins->getCheck())
        nc++;
    //if(mcBins2->getCheck())
    //    nc++;
    if(mcResult->getCheck())
        nc++;
    if(mcResult2->getCheck())
        nc++;

    if(!plateTable2->getNumRows() || nc!=plateTable2->getNumColumns() || mcAm->getCheck()!=mcAmCheck)
    {
        updateData();
        drawPlateTables();
    }
    return 1;
}

long cMDIReadings::onCmdShowStats(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(showStats->getCheck())
    {
        statsFrame->show();
        statsFrame->setHeight(0);
        layout();
    }
    else
    {
        statsFrame->hide();
        statsFrame->setHeight(400);
        layout();
    }
    return 1;
}

long cMDIReadings::onCmdReread(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!tPlatesCount)
        return 1;

    if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_rerdw").text()))
        return 1;

    double **plates;

    /*int ows=0;
    if(oWinMain->isMinimized())
        ows=1;
    else if(oWinMain->isMaximized())
        ows=2;
    oWinMain->minimize();*/

    int platesi=0;
    for(int i=0;i<tSetsCount;i++)
    {
        plates=cReadingsManager::remakeReading(casesSets[i].assayRes->getCellString(0,3),casesSets[i].assayRes->getCellString(0,4),casesSets[i].platesCount,platesi,tPlatesCount);
        if(!plates)
            break;

        for(int k=0;k<casesSets[i].platesCount;k++,platesi++)
        {
            for(int j=0;j<96;j++)
                plateTable1->setItemText(platesi*8+j/12,j%12,FXStringVal(plates[k][j]));
            free(plates[k]);
        }
    }
    /*switch(ows)
    {
        case 1:
            break;
        case 2:
            oWinMain->maximize();
            break;
        default:
            oWinMain->restore();
            break;
    }*/
    onCmdPlateTable1(NULL,0,NULL);
    return 1;
}

long cMDIReadings::onCmdTemplate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    plateTable1->acceptInput(true);
    plateTable2->acceptInput(true);
    if(selectedCase<0)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_nocasesel").text());
        return 1;
    }
    FXString st=*((sCaseEntry*)caseList1->getItemData(selectedCase,0))->tpl;

    if(!cTemplateManager::templateExists(st))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_rdgmdi_opentpl").text());
        return false;
    }

    if(oWinMain->isWindow(st))
        oWinMain->raiseWindow(st);
    else
        cTemplateManager::editTemplate(mdiClient,mdiMenu,st);
    return 1;
}

long cMDIReadings::onRefreshAsy(FXObject *prSender,FXSelector prSelector,void *prData)
{
    sCDLGO *data=(sCDLGO*)prData;
    FXString prId=*data->id;
    FXString prId2=*data->id2;
    FXString prAsy=*data->asy;
    int ci=0;
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        *casesSets[setsi].assay=prId2;
        *casesSets[setsi].assaytitle=prAsy;
        if(casesSets[setsi].assayRes)
            delete casesSets[setsi].assayRes;
        casesSets[setsi].assayRes=data->res;
        FXString calculations_defs=casesSets[setsi].assayRes->getCellString(0,6);
        casesSets[setsi].useSecond=FXIntVal(calculations_defs.before('\n'))==2?true:false;
        if(casesSets[setsi].useSecond)
            areSecondary=true;
        calculations_defs=calculations_defs.after('\n');

        *casesSets[setsi].cala=(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        casesSets[setsi].calapco=new FXString(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        *casesSets[setsi].calaop=(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        casesSets[setsi].calasco=new FXString(calculations_defs.before('\n'));
        calculations_defs=calculations_defs.after('\n');

        if(casesSets[0].useSecond)
        {
            *casesSets[setsi].calb=(calculations_defs.before('\t'));
            calculations_defs=calculations_defs.after('\t');
            casesSets[setsi].calbpco=new FXString(calculations_defs.before('\t'));
            calculations_defs=calculations_defs.after('\t');
            *casesSets[setsi].calbop=(calculations_defs.before('\t'));
            calculations_defs=calculations_defs.after('\t');
            casesSets[setsi].calbsco=new FXString(calculations_defs.before('\n'));
            calculations_defs=calculations_defs.after('\n');
        }
        else
        {
            casesSets[setsi].calb=NULL;
            casesSets[setsi].calbop=NULL;
            casesSets[setsi].calbpco=new FXString("0");
            casesSets[setsi].calbsco=new FXString("0");
        }

        FXString factors_defs=casesSets[setsi].assayRes->getCellString(0,5);
        for(int i=0;i<4;i++)
        {
            casesSets[setsi].factors[i]=FXIntVal(factors_defs.before('\t'));
            factors_defs=factors_defs.after('\t');
        }

        FXString titers_defs=casesSets[setsi].assayRes->getCellString(0,7);
        casesSets[setsi].useTiters=FXIntVal(titers_defs.before('\t'))!=0?true:false;
        if(casesSets[setsi].useTiters)
            areTiters=true;
        titers_defs=titers_defs.after('\t');
        casesSets[setsi].slope=FXFloatVal(titers_defs.before('\t'));
        titers_defs=titers_defs.after('\t');
        casesSets[setsi].intercept=FXFloatVal(titers_defs.before('\t'));
        titers_defs=titers_defs.after('\t');
        *casesSets[setsi].calt=(titers_defs.before('\n'));
        titers_defs=titers_defs.after('\n');
        for(int i=0;i<30;i++)
        {
            casesSets[setsi].titerGroups[i]=FXIntVal(titers_defs.before('\t'));
            titers_defs=titers_defs.after('\t');
        }

        FXString bins_defs=casesSets[setsi].assayRes->getCellString(0,8);
        for(int i=0;i<30;i++)
        {
            casesSets[setsi].ratioGroups[i]=FXFloatVal(bins_defs.before('\t'));
            bins_defs=bins_defs.after('\t');
        }

        for(int casei=0;casei<casesSets[setsi].casesCount;casei++,ci++)
        {
            sCaseEntry *c=&casesSets[setsi].cases[casei];
            if(prId==*c->assay)
            {
                *c->assay=prId2;
                caseList1->setItemText(ci,4,*c->assay);
                caseList2->setItemText(ci,4,*c->assay);
            }

            int cci=0,p=0,d,od,pp=0;
            for(int setsi=0;setsi<tSetsCount;setsi++)
            {
                d=od=0;
                for(int casei=0;casei<casesSets[setsi].casesCount;casei++,cci++)
                {
                    if(cci==selectedCase)
                        pp=p;
                    d=casesSets[setsi].cases[casei].startPlate;
                    if(d!=od)
                    {
                        p++;
                        od=d;
                    }
                    p+=casesSets[setsi].cases[casei].plateSpanCount-1;
                }
            }
            charts[ci]->setTitle(oLanguage->getText("str_rdgmdi_plate")+FXStringFormat(" %d",pp+1)+" - "+(*c->id)+" - "+(*c->assay));
            charts[ci]->hide();
            charts[ci]->show();
        }
    }
    selectCase(selectedCase);
    return 1;
}

long cMDIReadings::onRefreshTpl(FXObject *prSender,FXSelector prSelector,void *prData)
{
    sCDLGO *data=(sCDLGO*)prData;
    FXString prId=*data->id;
    FXString prId2=*data->id2;
    int ci=0;
    for(int setsi=0;setsi<tSetsCount;setsi++)
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++,ci++)
        {
            sCaseEntry *c=&casesSets[setsi].cases[casei];
            if(prId==*c->tpl)
            {
                *c->tpl=prId2;
                selectCase(selectedCase);
            }
        }
    return 1;
}

long cMDIReadings::onRefresh(FXObject *prSender,FXSelector prSelector,void *prData)
{
    sCDLGO *data=(sCDLGO*)prData;
    if(newdata)
        return 1;
    FXString prId=*data->id;
    FXString prId2=*data->id2;
    FXString prAsy=*data->asy;
    FXString prPop=*data->pop;
    FXString prPop2=*data->pop2;
    FXString prAge=*data->age;
    FXString prBlDate=*data->blDate;
    FXString id,asy;
    FXint ci=0;
    for(int setsi=0;setsi<tSetsCount;setsi++)
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++,ci++)
        {
            sCaseEntry *c=&casesSets[setsi].cases[casei];
            id=*c->id;
            asy=*c->assay;

            if(prPop==*c->population)
            {
                *c->population=prPop2;
                caseList1->setItemText(ci,3,*c->population);
                caseList2->setItemText(ci,3,*c->population);
            }

            if(prId!=id || prAsy!=asy)
                continue;

            *c->id=prId2;
            *c->age=prAge;
            *c->bleedDate=prBlDate;

            *c->veterinarian=data->res->getCellString(0,5);
            *c->reason=data->res->getCellString(0,4);
            *c->spType=data->res->getCellString(0,9);
            *c->comments=data->res->getCellString(0,24);
            *c->breed1=*data->br1;
            *c->breed2=*data->br2;

            caseList1->setItemText(ci,1,*c->id);
            caseList2->setItemText(ci,1,*c->id);
            caseList1->setItemText(ci,5,*c->age);
            caseList2->setItemText(ci,5,*c->age);
            selectCase(selectedCase);

            int cci=0,p=0,d,od,pp=0;
            for(int setsi=0;setsi<tSetsCount;setsi++)
            {
                d=od=0;
                for(int casei=0;casei<casesSets[setsi].casesCount;casei++,cci++)
                {
                    if(cci==selectedCase)
                        pp=p;
                    d=casesSets[setsi].cases[casei].startPlate;
                    if(d!=od)
                    {
                        p++;
                        od=d;
                    }
                    p+=casesSets[setsi].cases[casei].plateSpanCount-1;
                }
            }
            charts[ci]->setTitle(oLanguage->getText("str_rdgmdi_plate")+FXStringFormat(" %d",pp+1)+" - "+(*c->id)+" - "+(*c->assay));
            charts[ci]->hide();
            charts[ci]->show();
        }
    return 1;
}

long cMDIReadings::onCmdCaseEdit(FXObject *prSender,FXSelector prSelector,void *prData)
{
    plateTable1->acceptInput(true);
    plateTable2->acceptInput(true);
    if(selectedCase<0)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_nocasesel").text());
        return 1;
    }
    if(!saved)
    {
        if(MBOX_CLICKED_YES==FXMessageBox::warning(oWinMain,MBOX_YES_NO,oLanguage->getText("str_warning").text(),oLanguage->getText("str_rdgmdi_mustsave").text()))
            saveData();
        else
            return 1;
    }

    if(!saved)
        return 1;
    if(selectedCase<0 || selectedCase>=caseList1->getNumRows())
        return 1;
    FXString id,asy;

    sCaseEntry *c=(sCaseEntry*)caseList1->getItemData(selectedCase,0);
    id=*c->id;
    asy=*c->assay;

    if(!cCaseManager::caseExists(id,asy))
        return 1;

    cDLGCasePopEdit dlg(oWinMain,id,asy);
    if(dlg.execute(PLACEMENT_OWNER))
    {
        *c->id=dlg.getCase();
        *c->population=dlg.getPopulation();
        *c->age=dlg.getAge();
        *c->bleedDate=dlg.getBlDate();
        caseList1->setItemText(selectedCase,1,*c->id);
        caseList2->setItemText(selectedCase,1,*c->id);
        caseList1->setItemText(selectedCase,3,*c->population);
        caseList2->setItemText(selectedCase,3,*c->population);
        caseList1->setItemText(selectedCase,5,*c->age);
        caseList2->setItemText(selectedCase,5,*c->age);
        selectCase(selectedCase);

        int cci=0,p=0,d,od,pp=0;
        for(int setsi=0;setsi<tSetsCount;setsi++)
        {
            d=od=0;
            for(int casei=0;casei<casesSets[setsi].casesCount;casei++,cci++)
            {
                if(cci==selectedCase)
                    pp=p;
                d=casesSets[setsi].cases[casei].startPlate;
                if(d!=od)
                {
                    p++;
                    od=d;
                }
                p+=casesSets[setsi].cases[casei].plateSpanCount-1;
            }
        }
        charts[selectedCase]->setTitle(oLanguage->getText("str_rdgmdi_plate")+FXStringFormat(" %d",pp+1)+" - "+(*c->id)+" - "+(*c->assay));
        charts[selectedCase]->hide();
        charts[selectedCase]->show();
    }

    return 1;
}

long cMDIReadings::onCmdReport(FXObject *prSender,FXSelector prSelector,void *prData)
{
    plateTable1->acceptInput(true);
    plateTable2->acceptInput(true);
    if(!saved)
    {
        if(MBOX_CLICKED_YES==FXMessageBox::warning(oWinMain,MBOX_YES_NO,oLanguage->getText("str_warning").text(),oLanguage->getText("str_rdgmdi_mustsaverep").text()))
            saveData();
        else
            return 1;
    }
    if(!saved)
        return 1;

    cSortList *cases=new cSortList(this);
    for(int i=0;i<caseList1->getNumRows();i++)
    {
        cases->appendItem(caseList1->getItemText(i,4)+"\t"+caseList1->getItemText(i,1));
        cases->selectItem(i);
    }
    switch(prSelector)
    {
        case FXSEL(SEL_COMMAND,ID_REP_CA):
            cCaseAnalysisReport::openCases(mdiClient,mdiMenu,*cases);
            break;
        case FXSEL(SEL_COMMAND,ID_REP_CC):
            cCaseCompareReport::openCases(mdiClient,mdiMenu,*cases);
            break;
        case FXSEL(SEL_COMMAND,ID_REP_CM):
            cCaseCombineReport::openCases(mdiClient,mdiMenu,*cases);
            break;
        case FXSEL(SEL_COMMAND,ID_REP_SB):
            cCaseSbsReport::openCases(mdiClient,mdiMenu,*cases);
            break;
        case FXSEL(SEL_COMMAND,ID_REP_TC):
            cTotalCountsReport::openCases(mdiClient,mdiMenu,*cases);
            break;
        case FXSEL(SEL_COMMAND,ID_REP_TB):
            cTiterBreakdownReport::openCases(mdiClient,mdiMenu,*cases);
            break;
        case FXSEL(SEL_COMMAND,ID_REP_VH):
            cVaccineHistoryReport::openCases(mdiClient,mdiMenu,*cases);
            break;
        case FXSEL(SEL_COMMAND,ID_REP_VD):
            cVaccineDateReport::openCases(mdiClient,mdiMenu,*cases);
            break;
        default:
            break;
    }
    delete cases;
    return 1;
}

long cMDIReadings::onCmdShowRules(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXDialogBox msg(oWinMain,oLanguage->getText("str_rdgmdi_rules"),DECOR_TITLE|DECOR_BORDER);
    FXVerticalFrame *vf=new FXVerticalFrame(&msg,LAYOUT_CENTER_X|JUSTIFY_CENTER_X);
        FXVerticalFrame *vf2=new FXVerticalFrame(vf,FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
        FXText *tf=new FXText(vf2,NULL,0,LAYOUT_FIX_WIDTH|LAYOUT_FIX_HEIGHT|TEXT_READONLY,0,0,300,200);
        tf->setText(invalidPlatesDetails);
    new FXButton(vf,oLanguage->getText("str_but_close"),NULL,&msg,FXDialogBox::ID_ACCEPT,BUTTON_NORMAL|LAYOUT_CENTER_X);
    msg.create();
    msg.show(PLACEMENT_OWNER);
    msg.setFocus();
    msg.raise();
    getApp()->runModalWhileShown(&msg);
    return 1;
}

long cMDIReadings::onCmdChPoint(FXObject *prSender,FXSelector prSelector,void *prData)
{
    bchPoint->setState(STATE_DOWN);
    bchLine->setState(STATE_UP);
    bchBar->setState(STATE_UP);
    for(int i=0;i<tCasesCount;i++)
        charts[i]->setType(cVChart::CHART_POINT);
    chartFrame->hide();
    chartFrame->show();
    return 1;
}

long cMDIReadings::onCmdChLine(FXObject *prSender,FXSelector prSelector,void *prData)
{
    bchPoint->setState(STATE_UP);
    bchLine->setState(STATE_DOWN);
    bchBar->setState(STATE_UP);
    for(int i=0;i<tCasesCount;i++)
        charts[i]->setType(cVChart::CHART_LINE);
    chartFrame->hide();
    chartFrame->show();
    return 1;
}

long cMDIReadings::onCmdChBar(FXObject *prSender,FXSelector prSelector,void *prData)
{
    bchPoint->setState(STATE_UP);
    bchLine->setState(STATE_UP);
    bchBar->setState(STATE_DOWN);
    for(int i=0;i<tCasesCount;i++)
        charts[i]->setType(cVChart::CHART_BAR);
    chartFrame->hide();
    chartFrame->show();
    return 1;
}

long cMDIReadings::onEvtRedir(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXEvent *event=(FXEvent*)prData;

    switch(event->code)
    {
        case KEY_Page_Up:
        case KEY_Page_Down:
        case KEY_KP_Page_Up:
        case KEY_KP_Page_Down:
            if(tiPrimary->hasFocus() || tiStats->hasFocus())
                return 1;
            if(tbMain->getCurrent()==2)
                return chartWin->tryHandle(prSender,prSelector,prData);
            break;
        default:
            break;
    }
    return cMDIChild::handle(prSender,prSelector,prData);
}
