#ifndef CDLGBSLCASES_H
#define CDLGBSLCASES_H

#include <fx.h>

class cDLGBslCases : public FXDialogBox
{
    FXDECLARE(cDLGBslCases);
    private:
        FXButton *butAccept;
        FXButton *butCancel;
        
    protected:
        FXDataTarget ftype;
        FXRadioButton *bMinmax;
        FXRadioButton *bStdev;
        FXRadioButton *bPercent;

        FXDataTarget agtype;
        FXRadioButton *bAMean;
        FXRadioButton *bGMean;

        cDLGBslCases(){}
        
    public:
        FXCheckButton *bcAm;
        FXuint fchoice;
        FXuint agchoice;
        
        FXTextField *tfStdev,*tfPercent;
        
        cDLGBslCases(FXWindow *prOwner);
        virtual ~cDLGBslCases();
        
        enum
        {
            ID_THIS=FXDialogBox::ID_LAST,
            ID_UPDATE,
            
            CMD_BUT_APPLY,
            ID_LAST
        };

        long onUpdate(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
