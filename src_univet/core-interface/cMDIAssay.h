#ifndef CMDIASSAY_H
#define CMDIASSAY_H

#include <fx.h>

#include "ee.h"
#include "cMDIChild.h"
#include "cColorTable.h"

class cMDIAssay : public cMDIChild
{
    FXDECLARE(cMDIAssay);

    private:
        FXTextField *tfId;
        FXTextField *tfTitle;
        FXTextField *tfFilterA;
        FXTextField *tfFilterB;
        FXListBox *lxCalculusA;
        FXTextField *tfCutoffA;
        FXTextField *tfSuspectA;
        FXListBox *lxOperatorA;
        FXCheckButton *cbCalculusB;
        FXListBox *lxCalculusB;
        FXTextField *tfCutoffB;
        FXTextField *tfSuspectB;
        FXListBox *lxOperatorB;
        cColorTable *tbFactors;
        FXCheckButton *cbTiters;
        FXTextField *tfTiterSlope;
        FXTextField *tfTiterIntercept;
        FXListBox *lxCalculusT;
        cColorTable *tbTiters;
        cColorTable *tbBins;
        FXText *comments;
        FXListBox *opListR1;
        FXListBox *opListR2;
        FXListBox *opListR3;
        FXListBox *opListR4;
        FXListBox *opListR5;
        FXTextField *valueR1;
        FXTextField *valueR2;
        FXTextField *valueR3;
        FXTextField *valueR4;
        FXTextField *valueR5;
        FXCheckButton *cbR1;
        FXCheckButton *cbR2;
        FXCheckButton *cbR3;
        FXCheckButton *cbR4;
        FXCheckButton *cbR5;

        FXbool saved;
        FXString name;

    protected:
        cMDIAssay();

        FXbool updateData(void);
        FXbool saveData(void);

    public:
        cMDIAssay(FXMDIClient *prP,const FXString &prName,FXIcon *prIc=NULL,FXPopup *prPup=NULL,FXuint prOpts=0,FXint prX=0,FXint prY=0,FXint prW=0,FXint prH=0);
        virtual ~cMDIAssay();

        virtual void create();

        virtual FXbool canClose(void);
        virtual FXbool loadAssay(const FXString &prId);

        enum
        {
            ID_MDIASSAY=FXMDIChild::ID_LAST,
            ID_ASYID,
            ID_ASYTITLE,
            ID_ASYFILTERA,
            ID_ASYFILTERB,
            ID_ASYCALA,
            ID_ASYCALBCHECK,
            ID_ASYCALB,
            ID_ASYCALT,
            ID_ASYCUTA,
            ID_ASYCUTB,
            ID_ASYSUSA,
            ID_ASYSUSB,
            ID_ASYDIRA,
            ID_ASYDIRB,
            ID_ASYFACTORS,
            ID_ASYSLOPE,
            ID_ASYINTER,
            ID_ASYTITERSCHECK,
            ID_ASYTITERS,
            ID_ASYBINS,
            ID_ASYCOMMENTS,
            ID_RULES,

            CMD_SAVE,
            CMD_PRINT,
            CMD_CLOSE,

            ID_LAST
        };

        long onSelTbFactors(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdCalBCheck(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdTitersCheck(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdRules(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdRules(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdTfId(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
