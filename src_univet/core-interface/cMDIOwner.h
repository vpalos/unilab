#ifndef CMDIOWNER_H
#define CMDIOWNER_H

#include <fx.h>

#include "cMDIChild.h"
#include "cColorTable.h"

class cMDIOwner : public cMDIChild
{
    FXDECLARE(cMDIOwner);
    
    private:
        FXTextField *tfId;
        FXTextField *tfLab;
        FXText *tfAdd;
        FXTextField *tfCity;
        FXTextField *tfState;
        FXTextField *tfZip;
        FXTextField *tfPhone;
        FXTextField *tfFax;
        FXTextField *tfEmail;
        FXText *comments;

        FXbool saved;
        FXString name;
    
    protected:
        cMDIOwner();
        
        void updateData(void);
        FXbool saveData(void);
        
    public:
        cMDIOwner(FXMDIClient *prP,const FXString &prName,FXIcon *prIc=NULL,FXPopup *prPup=NULL,FXuint prOpts=0,FXint prX=0,FXint prY=0,FXint prW=0,FXint prH=0);
        virtual ~cMDIOwner();
        
        virtual void create();
        
        virtual FXbool canClose(void);
        virtual FXbool loadOwner(const FXString &prId);
        
        enum
        {
            ID_MDIOWNER=FXMDIChild::ID_LAST,
            ID_OWNID,
            ID_OWNLAB,
            ID_OWNADD,
            ID_OWNCITY,
            ID_OWNSTATE,
            ID_OWNZIP,
            ID_OWNPHONE,
            ID_OWNFAX,
            ID_OWNEMAIL,
            ID_OWNCOMMENTS,
            
            CMD_SAVE,
            CMD_PRINT,
            CMD_CLOSE,
            
            ID_LAST
        };

        long onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdTfId(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif

