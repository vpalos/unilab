#ifndef CDLGREPORTCM_H
#define CDLGREPORTCM_H

#include <fx.h>

class cDLGReportCM : public FXDialogBox
{
    FXDECLARE(cDLGReportCM);
    private:
        FXButton *butAccept;
        FXButton *butCancel;
        
    protected:

        cDLGReportCM(){}
        
    public:
        FXuint choice;
        FXDataTarget chtype;
        FXCheckButton *bcCala;
        FXCheckButton *bcCalb;
        FXCheckButton *bcTiter;
        FXCheckButton *bcAm;
        FXCheckButton *bcLog2;
        FXCheckButton *bcEU;
        
        FXCheckButton *bUseData;
        FXCheckButton *bUseInfo;
    
        FXRadioButton *bTitGraphs;
        FXRadioButton *bBinGraphs;
    
        cDLGReportCM(FXWindow *prOwner,FXbool areTiters,FXbool areSecondary,FXbool areBins);
        virtual ~cDLGReportCM();
        
        enum
        {
            ID_THIS=FXDialogBox::ID_LAST,
            ID_UPDATE,
            
            CMD_BUT_APPLY,
            ID_LAST
        };

        long onCmdUpdate(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
