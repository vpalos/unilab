#include "engine.h"
#include "graphics.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cDLGReportTB.h"

FXDEFMAP(cDLGReportTB) mapDLGReportTB[]=
{
    FXMAPFUNC(SEL_COMMAND,cDLGReportTB::CMD_BUT_APPLY,cDLGReportTB::onCmdApply),
};

FXIMPLEMENT(cDLGReportTB,FXDialogBox,mapDLGReportTB,ARRAYNUMBER(mapDLGReportTB))

cDLGReportTB::cDLGReportTB(FXWindow *prOwner) :
    FXDialogBox(prOwner,oLanguage->getText("str_report_tb")+"...",DECOR_TITLE|DECOR_BORDER),agtype(agchoice),stype(schoice)
{
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL);
        agchoice=2;
        FXLabel *_lb2=new FXLabel(_vframe0,oLanguage->getText("str_report_mtype"));
            _lb2->setTextColor(FXRGB(0,0,200));        
        FXGroupBox *_gb2=new FXGroupBox(_vframe0,(char*)NULL,GROUPBOX_NORMAL,0,0,0,0,17,0,0,0);
            bAMean=new FXRadioButton(_gb2,oLanguage->getText("str_report_ma"),&agtype,FXDataTarget::ID_OPTION+1);
            bAMean->setTextColor(FXRGB(120,120,120));
            bGMean=new FXRadioButton(_gb2,oLanguage->getText("str_report_mg"),&agtype,FXDataTarget::ID_OPTION+2);
            bGMean->setTextColor(FXRGB(120,120,120));
        
        new FXHorizontalSeparator(_vframe0);

        schoice=1;
        FXLabel *_lb1=new FXLabel(_vframe0,oLanguage->getText("str_report_stype"));
            _lb1->setTextColor(FXRGB(0,0,200));
        FXGroupBox *_gb1=new FXGroupBox(_vframe0,(char*)NULL,GROUPBOX_NORMAL,0,0,0,0,17,0,0,0);
            bAssay=new FXRadioButton(_gb1,oLanguage->getText("str_report_sa"),&stype,FXDataTarget::ID_OPTION+1);
            bAssay->setTextColor(FXRGB(120,120,120));
            bCase=new FXRadioButton(_gb1,oLanguage->getText("str_report_sc"),&stype,FXDataTarget::ID_OPTION+2);
            bCase->setTextColor(FXRGB(120,120,120));
        new FXHorizontalSeparator(_vframe0);
    
        bcAm=new FXCheckButton(_vframe0,oLanguage->getText("str_rdgmdi_mean"));
        bcAm->setCheck(true);
        bcAm->setTextColor(FXRGB(0,0,200));
        new FXHorizontalSeparator(_vframe0);
    
        FXHorizontalFrame *_hframe100=new FXHorizontalFrame(_vframe0,LAYOUT_CENTER_X);
        butCancel=new FXButton(_hframe100,oLanguage->getText("str_but_cancel"),NULL,this,ID_CANCEL,BUTTON_DEFAULT|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
        butAccept=new FXButton(_hframe100,oLanguage->getText("str_but_ok"),NULL,this,CMD_BUT_APPLY,BUTTON_DEFAULT|BUTTON_INITIAL|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
}

cDLGReportTB::~cDLGReportTB()
{
}

long cDLGReportTB::onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData)
{
    handle(NULL,FXSEL(SEL_COMMAND,ID_ACCEPT),NULL);
    return 1;
}

