#include <time.h>
#include <ctype.h>
#include <stdlib.h>

#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cColorsManager.h"
#include "cFormulaRatio.h"
#include "cAssayManager.h"
#include "cSpeciesManager.h"
#include "cVaccineManager.h"
#include "cCaseManager.h"
#include "cPopulationManager.h"
#include "cWinMain.h"
#include "cMDIReadings.h"
#include "cMDIVaccine.h"
#include "cMDIVaccineManager.h"

FXDEFMAP(cMDIVaccine) mapMDIVaccine[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIVaccine::ID_MDIVACCINE,cMDIVaccine::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIVaccine::ID_VACID,cMDIVaccine::onCmdTfId),
    FXMAPFUNC(SEL_CHANGED,cMDIVaccine::ID_VACID,cMDIVaccine::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIVaccine::ID_VACTITLE,cMDIVaccine::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIVaccine::ID_VACSCHEDULE,cMDIVaccine::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIVaccine::ID_VACBLEEDS,cMDIVaccine::onCmdUpdateData),
    FXMAPFUNC(SEL_REPLACED,cMDIVaccine::ID_VACSCHEDULE,cMDIVaccine::onRplSchedule),
    FXMAPFUNC(SEL_REPLACED,cMDIVaccine::ID_VACBLEEDS,cMDIVaccine::onRplBleeds),
    FXMAPFUNC(SEL_CHANGED,cMDIVaccine::ID_VACCOMMENTS,cMDIVaccine::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIVaccine::CMD_CLOSE,cMDIVaccine::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIVaccine::CMD_SAVE,cMDIVaccine::onCmdSave),
    FXMAPFUNC(SEL_COMMAND,cMDIVaccine::CMD_PRINT,cMDIVaccine::onCmdPrint),
    FXMAPFUNC(SEL_COMMAND,cMDIVaccine::ID_VACASYLIST,cMDIVaccine::onCmdAsyChange),
    FXMAPFUNC(SEL_UPDATE,cMDIVaccine::ID_VACASYLIST,cMDIVaccine::onUpdAsyList),
    FXMAPFUNC(SEL_COMMAND,cMDIVaccine::CMD_BUT_ASYADD,cMDIVaccine::onCmdAsyAdd),
    FXMAPFUNC(SEL_COMMAND,cMDIVaccine::CMD_BUT_ASYDEL,cMDIVaccine::onCmdAsyDel),
};

FXIMPLEMENT(cMDIVaccine,cMDIChild,mapMDIVaccine,ARRAYNUMBER(mapMDIVaccine))

cMDIVaccine::cMDIVaccine()
{
}

void cMDIVaccine::updateData(void)
{
    saved=false;
}

FXbool cMDIVaccine::saveData(void)
{
    sVAO *obj;
    for(int i=0;i<assayList->getNumItems();i++)
    {
        obj=(sVAO*)assayList->getItemData(i);
        obj->schedule->acceptInput(true);
        obj->bleeds->acceptInput(true);
    }
    
    if(saved)
        return true;
    
    if(!name.empty() && cVaccineManager::vaccineSolid(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasevacsolid").text());
        saved=true;
        return 1;
    }
    
    if(!assayCount)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_vacmdi_noasy2").text());
        return false;
    }

    FXString oldname=name;
    name=tfId->getText();
    if(name!=oldname && cVaccineManager::vaccineExists(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
        tfId->setText(cVaccineManager::getNewID());
        setTitle(tfId->getText());
        updateData();
        return false;
    }
    this->setTitle(name);
    
    FXString assay_defs=FXStringVal(assayCount)+"\r";
    for(int i=0;i<assayList->getNumItems();i++)
    {
        obj=(sVAO*)assayList->getItemData(i);
        assay_defs=assay_defs+assayList->getItemText(i).before('-').trim()+"\n";
        for(int j=0;j<obj->bleeds->getNumRows();j++)
            assay_defs=assay_defs+obj->bleeds->getItemText(j,0)+"\t";
        assay_defs=assay_defs+"\n";
        for(int j=0;j<obj->schedule->getNumRows();j++)
        {
            for(int k=0;k<obj->schedule->getNumColumns();k++)
                assay_defs=assay_defs+obj->schedule->getItemText(j,k).trim()+"\t";
            assay_defs=assay_defs+"\n";
        }
        assay_defs=assay_defs+"\r";
    }
    if(oldname.empty())
        cVaccineManager::addVaccine(name,tfTitle->getText(),assay_defs,comments->getText());
    else
        cVaccineManager::setVaccine(oldname,name,tfTitle->getText(),assay_defs,comments->getText());

    saved=true;
    cMDIVaccineManager::cmdRefresh();
    return true;
}

FXString cMDIVaccine::selectAssay(FXString &prOid)
{
    FXString choices="";
    int cnt=cAssayManager::getAssayCount();
    if(!cnt)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_vacmdi_noasy").text());
        return "";
    }
    sAssayObject *res=cAssayManager::listAssays();
    if(!res)
        return "";
    
    for(int i=0;i<cnt;i++)
        choices=choices+(*res[i].id)+" - "+(*res[i].title)+"\n";
    
    if((cnt=FXChoiceBox::ask(oWinMain,0,oLanguage->getText("str_question").text(),oLanguage->getText("str_vacmdi_chooseasy").text(),new FXGIFIcon(oApplicationManager,data_inputs),choices))==-1)
        return "";
    prOid=*res[cnt].id;
    return (*res[cnt].id)+" - "+(*res[cnt].title);
}

cMDIVaccine::cMDIVaccine(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH) 
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_MDIVACCINE);
    saved=false;
    name="";
    
    FXVerticalFrame *_vframe=new FXVerticalFrame(this,LAYOUT_FILL);
    
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(_vframe,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
        FXVerticalFrame *_vframe0=new FXVerticalFrame(_hframe0,0,0,0,0,0,0,0,0,0);
            new FXLabel(_vframe0,oLanguage->getText("str_vacmdi_id"));
            tfId=new FXTextField(_vframe0,25,this,ID_VACID);
            tfId->setText(prName);
            tfId->setSelection(0,tfId->getText().length());
            tfId->setFocus();
        FXVerticalFrame *_vframe1=new FXVerticalFrame(_hframe0,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
            new FXLabel(_vframe1,oLanguage->getText("str_vacmdi_title"));
            tfTitle=new FXTextField(_vframe1,30,this,ID_VACTITLE,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK);
    
    new FXLabel(_vframe,oLanguage->getText("str_vacmdi_assays"));
    FXHorizontalFrame *_hframe1=new FXHorizontalFrame(_vframe,LAYOUT_FILL,0,0,0,0,0,0,0,0);
        FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe1,LAYOUT_FILL,0,0,0,0,0,0,0,0);
            assayList=new FXListBox(_vframe2,this,ID_VACASYLIST,FRAME_SUNKEN|FRAME_THICK|LISTBOX_NORMAL|LAYOUT_FILL_X);
            assayList->setNumVisible(5);
            assayList->setTextColor(FXRGB(200,0,0));
            _gb0=new FXGroupBox(_vframe2,oLanguage->getText("str_vacmdi_details"),GROUPBOX_TITLE_LEFT|LAYOUT_FILL|FRAME_GROOVE);
                assaySwitcher=new FXSwitcher(_gb0,LAYOUT_FILL,0,0,0,0,0,0,0,0);
                    FXLabel *_lb0=new FXLabel(assaySwitcher,oLanguage->getText("str_vacmdi_swback"),NULL,LAYOUT_FILL|JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
                    _lb0->setTextColor(FXRGB(180,0,0));
            new FXLabel(_vframe2,oLanguage->getText("str_vacmdi_comments"));
            FXVerticalFrame *_dframe0=new FXVerticalFrame(_vframe2,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
            comments=new FXText(_dframe0,this,ID_VACCOMMENTS,TEXT_WORDWRAP|TEXT_NO_TABS|LAYOUT_FILL_X);
            
        FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y,0,0,0,0,0,0,0,0,0,0);
            new FXButton(_vframe3,oLanguage->getText("str_vacmdi_save"),NULL,this,CMD_SAVE,FRAME_RAISED|BUTTON_DEFAULT|BUTTON_INITIAL|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH,0,0,100,0);
            new FXHorizontalSeparator(_vframe3,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT,0,0,0,10);
            new FXButton(_vframe3,oLanguage->getText("str_vacmdi_add"),NULL,this,CMD_BUT_ASYADD,FRAME_RAISED|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH,0,0,100,0);
            new FXButton(_vframe3,oLanguage->getText("str_vacmdi_del"),NULL,this,CMD_BUT_ASYDEL,FRAME_RAISED|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH,0,0,100,0);

            new FXButton(_vframe3,oLanguage->getText("str_but_close"),NULL,this,CMD_CLOSE,FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,100,0);
            new FXHorizontalSeparator(_vframe3,LAYOUT_FILL_X|LAYOUT_BOTTOM|LAYOUT_FIX_HEIGHT,0,0,0,10);
            //new FXButton(_vframe3,oLanguage->getText("str_vacmdi_print"),new FXGIFIcon(getApp(),data_settings_printer),this,CMD_PRINT,FRAME_RAISED|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,100,0);
    
    assayCount=0;
    assayList->setSortFunc(FXList::ascending);
    vaccineWindow=oWinMain->getVaccines()->no();
    oWinMain->getVaccines()->insert(FXStringFormat("%c%c",vaccineWindow/256+1,vaccineWindow%256+1).text(),this);
}

cMDIVaccine::~cMDIVaccine()
{
    oWinMain->getVaccines()->remove(FXStringFormat("%c%c",vaccineWindow/256+1,vaccineWindow%256+1).text());
}

void cMDIVaccine::create()
{
    cMDIChild::create();
    show();
    updateData();
}

FXbool cMDIVaccine::canClose(void)
{
    sVAO *obj;
    for(int i=0;i<assayList->getNumItems();i++)
    {
        obj=(sVAO*)assayList->getItemData(i);
        obj->schedule->acceptInput(true);
        obj->bleeds->acceptInput(true);
    }
    if(!saved)
    {
        setFocus();
        int res=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),oLanguage->getText("str_qsave").text());
        switch(res)
        {
            case MBOX_CLICKED_YES:
                if(!saveData())
                    return false;
                break;
            case MBOX_CLICKED_CANCEL:
                return false;
            default:
                break;
        }
    }
    return true;
}

FXbool cMDIVaccine::loadVaccine(const FXString &prId)
{
    if(!cVaccineManager::vaccineExists(prId))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_openvac").text());
        return false;
    }
    if(!oWinMain->isWindow(prId))
        return oWinMain->raiseWindow(prId);
    
    cDataResult *res=cVaccineManager::getVaccine(prId);
    if(res==NULL)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_openvac").text());
        return false;
    }
    
    name=res->getCellString(0,0);
    tfId->setText(name);
    tfTitle->setText(res->getCellString(0,1));
    comments->setText(res->getCellString(0,4));
    FXString assay_defs=res->getCellString(0,3);
    FXString asy,range1="",range2="";
    int u,v,count=FXIntVal(assay_defs.before('\r'));
    FXString asy_oid="";
    assay_defs=assay_defs.after('\r');
    cDataResult *res2;
    sVAO *obj;
    for(int i=0;i<count;i++)
    {
        asy_oid=assay_defs.before('\n');
        assay_defs=assay_defs.after('\n');
        
        obj=(sVAO*)malloc(sizeof(sVAO));
        if(!obj)
        {
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
            return 1;
        }
        obj->frame=new FXHorizontalFrame(assaySwitcher,LAYOUT_FILL,0,0,0,0,0,0,0,0);
        FXVerticalFrame *_df1=new FXVerticalFrame(obj->frame,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
        obj->schedule=new cColorTable(_df1,this,ID_VACSCHEDULE,LAYOUT_FILL|TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT);
        obj->schedule->setTableSize(VACCINE_ASYCOUNT,6);
        for(u=0;u<VACCINE_ASYCOUNT;u++)
        {
            obj->schedule->setRowText(u,FXStringFormat("%d",u+1));
            obj->schedule->getRowHeader()->setItemJustify(u,JUSTIFY_CENTER_X);
            for(int v=0;v<6;v++)
                obj->schedule->setItemJustify(u,v,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
        }
        obj->schedule->setRowHeaderWidth(20);
        for(v=0;v<6;v++)
            obj->schedule->getColumnHeader()->setItemJustify(v,JUSTIFY_CENTER_X);
        obj->schedule->setScrollStyle(HSCROLLING_ON|VSCROLLING_ON|VSCROLLER_ALWAYS|SCROLLERS_TRACK);
        obj->schedule->setSelectable(false);
        obj->schedule->setColumnText(0,oLanguage->getText("str_vacmdi_age"));
        obj->schedule->setColumnWidth(0,40);
        obj->schedule->setColumnText(1,oLanguage->getText("str_vacmdi_vaccine"));
        obj->schedule->setColumnWidth(1,70);
        obj->schedule->setColumnText(2,oLanguage->getText("str_vacmdi_manufacturer"));
        obj->schedule->setColumnWidth(2,95);
        obj->schedule->setColumnText(3,oLanguage->getText("str_vacmdi_type"));
        obj->schedule->setColumnWidth(3,40);
        obj->schedule->setColumnText(4,oLanguage->getText("str_vacmdi_route"));
        obj->schedule->setColumnWidth(4,40);
        obj->schedule->setColumnText(5,oLanguage->getText("str_vacmdi_dose"));
        obj->schedule->setColumnWidth(5,55);
        
        FXVerticalFrame *_df0=new FXVerticalFrame(obj->frame,LAYOUT_FILL_Y|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
        obj->bleeds=new cColorTable(_df0,this,ID_VACBLEEDS,LAYOUT_FILL_Y|LAYOUT_FIX_WIDTH,0,0,105);
        obj->bleeds->setTableSize(VACCINE_BLDSCOUNT,1);
        for(u=0;u<VACCINE_BLDSCOUNT;u++)
        {
            obj->bleeds->setRowText(u,FXStringFormat("%d",u+1));
            obj->bleeds->getRowHeader()->setItemJustify(u,JUSTIFY_CENTER_X);
            obj->bleeds->setItemJustify(u,0,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
        }
        obj->bleeds->setRowHeaderWidth(20);
        obj->bleeds->getColumnHeader()->setItemJustify(0,JUSTIFY_CENTER_X);
        obj->bleeds->showVertGrid(false);
        obj->bleeds->setScrollStyle(HSCROLLING_OFF|HSCROLLER_NEVER|VSCROLLING_ON|VSCROLLER_ALWAYS|SCROLLERS_TRACK);
        obj->bleeds->setSelectable(false);
        obj->bleeds->setColumnText(0,oLanguage->getText("str_vacmdi_bleeds"));
        obj->bleeds->setColumnWidth(0,70);
        
        range1=assay_defs.before('\n');
        assay_defs=assay_defs.after('\n');

        u=0;
        while(!range1.empty() && u<VACCINE_BLDSCOUNT)
        {
            obj->bleeds->setItemText(u++,0,range1.before('\t'));
            range1=range1.after('\t');
        }
        
        range1=assay_defs.before('\r');
        assay_defs=assay_defs.after('\r');

        v=0;
        while(!range1.empty() && v<VACCINE_ASYCOUNT)
        {
            range2=range1.before('\n');
            range1=range1.after('\n');
            u=0;
            while(!range2.empty() && u<obj->schedule->getNumColumns())
            {
                obj->schedule->setItemText(v,u++,range2.before('\t'));
                range2=range2.after('\t');
            }
            v++;
        }
        
        assaySwitcher->create();
        res2=cAssayManager::getAssay(asy_oid);
        obj->oid=new FXString(res2->getCellString(0,0));
        assayList->appendItem(res2->getCellString(0,0)+" - "+res2->getCellString(0,1),NULL,(void*)obj);
        res2->free();
        assayList->setCurrentItem(0);
        assayCount++;
        assaySwitcher->setCurrent(1);

    }
    //assayList->sortItems();
    setTitle(name);
    updateData();
    saved=true;
    return true;
}

long cMDIVaccine::onCmdTfId(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    if(!tfId->getText().empty())
    {
        if(!cVaccineManager::vaccineExists(tfId->getText()))
            return 1;
        else
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
    }
    tfId->setText(getTitle());
    return 1;
}

long cMDIVaccine::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(canClose())
    {
        close();
        return 1; 
    }
    return 0;
}

long cMDIVaccine::onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    return 1;
}

long cMDIVaccine::onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData)
{
    saveData();
    if(saved)
        onCmdClose(NULL,0,NULL);
    return 1;
}

long cMDIVaccine::onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData)
{
    
    
    // TODO
    
    
    return 1;
}

long cMDIVaccine::onUpdAsyList(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i;
    if((i=assayList->getCurrentItem())>=0)
        _gb0->setText(oLanguage->getText("str_vacmdi_details")+FXStringFormat(" (%s)",assayList->getItemText(i).text()));
    else
        _gb0->setText(oLanguage->getText("str_vacmdi_details"));
    if(assayList->getNumItems()>0)
        assayList->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    else
        assayList->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    return 1;
}

long cMDIVaccine::onCmdAsyChange(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i;
    if((i=assayList->getCurrentItem())<0)
    {
        _gb0->setText(oLanguage->getText("str_vacmdi_details"));
        assaySwitcher->setCurrent(0);
        return 1;
    }
    _gb0->setText(oLanguage->getText("str_vacmdi_details")+FXStringFormat(" (%s)",assayList->getItemText(i).text()));
    if(i<assayCount)
        assaySwitcher->setCurrent(i+1);
    return 1;
}

long cMDIVaccine::onCmdAsyAdd(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXString asy="";
    FXString asyoid="";
    while(asy.empty())
    {
        asy=selectAssay(asyoid);
        if(asy.empty())
            return 1;
        if(assayList->findItem(asy)!=-1)
        {
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_vacmdi_asydupl").text());
            asy="";
        }
    }
    
    sVAO *obj=(sVAO*)malloc(sizeof(sVAO));
    if(!obj)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return 1;
    }
    int i,j;
    obj->frame=new FXHorizontalFrame(assaySwitcher,LAYOUT_FILL,0,0,0,0,0,0,0,0);
        FXVerticalFrame *_df1=new FXVerticalFrame(obj->frame,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
        obj->schedule=new cColorTable(_df1,this,ID_VACSCHEDULE,LAYOUT_FILL|TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT);
        obj->schedule->setTableSize(VACCINE_ASYCOUNT,6);
        for(i=0;i<VACCINE_ASYCOUNT;i++)
        {
            obj->schedule->setRowText(i,FXStringFormat("%d",i+1));
            obj->schedule->getRowHeader()->setItemJustify(i,JUSTIFY_CENTER_X);
            for(int j=0;j<6;j++)
                obj->schedule->setItemJustify(i,j,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
            obj->schedule->setItemText(i,0,"0-0");
        }
        obj->schedule->setRowHeaderWidth(20);
        for(j=0;j<6;j++)
            obj->schedule->getColumnHeader()->setItemJustify(j,JUSTIFY_CENTER_X);
        obj->schedule->setScrollStyle(HSCROLLING_ON|VSCROLLING_ON|VSCROLLER_ALWAYS|SCROLLERS_TRACK);
        obj->schedule->setSelectable(false);
        obj->schedule->setColumnText(0,oLanguage->getText("str_vacmdi_age"));
        obj->schedule->setColumnWidth(0,40);
        obj->schedule->setColumnText(1,oLanguage->getText("str_vacmdi_vaccine"));
        obj->schedule->setColumnWidth(1,70);
        obj->schedule->setColumnText(2,oLanguage->getText("str_vacmdi_manufacturer"));
        obj->schedule->setColumnWidth(2,95);
        obj->schedule->setColumnText(3,oLanguage->getText("str_vacmdi_type"));
        obj->schedule->setColumnWidth(3,40);
        obj->schedule->setColumnText(4,oLanguage->getText("str_vacmdi_route"));
        obj->schedule->setColumnWidth(4,40);
        obj->schedule->setColumnText(5,oLanguage->getText("str_vacmdi_dose"));
        obj->schedule->setColumnWidth(5,55);
        
        FXVerticalFrame *_df0=new FXVerticalFrame(obj->frame,LAYOUT_FILL_Y|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
        obj->bleeds=new cColorTable(_df0,this,ID_VACBLEEDS,LAYOUT_FILL_Y|LAYOUT_FIX_WIDTH,0,0,105);
        obj->bleeds->setTableSize(VACCINE_BLDSCOUNT,1);
        for(i=0;i<VACCINE_BLDSCOUNT;i++)
        {
            obj->bleeds->setRowText(i,FXStringFormat("%d",i+1));
            obj->bleeds->getRowHeader()->setItemJustify(i,JUSTIFY_CENTER_X);
            obj->bleeds->setItemJustify(i,0,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
            obj->bleeds->setItemText(i,0,"0-0");
        }
        obj->bleeds->setRowHeaderWidth(20);
        obj->bleeds->getColumnHeader()->setItemJustify(0,JUSTIFY_CENTER_X);
        obj->bleeds->showVertGrid(false);
        obj->bleeds->setScrollStyle(HSCROLLING_OFF|HSCROLLER_NEVER|VSCROLLING_ON|VSCROLLER_ALWAYS|SCROLLERS_TRACK);
        obj->bleeds->setSelectable(false);
        obj->bleeds->setColumnText(0,oLanguage->getText("str_vacmdi_bleeds"));
        obj->bleeds->setColumnWidth(0,70);
        
        assaySwitcher->create();
        
        obj->oid=new FXString(asyoid);
        assayList->appendItem(asy,NULL,(void*)obj);
        
        assayList->setCurrentItem(assayCount);
        assayCount++;
        assaySwitcher->setCurrent(assayCount);

    assayList->setNumVisible(assayList->getNumItems()>15?15:assayList->getNumItems());
    updateData();
    return 1;
}

long cMDIVaccine::onCmdAsyDel(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(assayCount==0)
        return 1;
    int pos=assayList->getCurrentItem();
    if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_eraseasy").text()))
        return 1;
    sVAO *obj=(sVAO*)assayList->getItemData(pos);
    
    obj->frame->destroy();
    obj->frame->~FXWindow();
    free(obj);
    assayCount--;
    assayList->removeItem(assayList->getCurrentItem());
    if(assayCount)
        assayList->setCurrentItem(0);
    assaySwitcher->recalc();
    assaySwitcher->setCurrent(assayCount>0?1:0);
    assayList->setNumVisible(assayList->getNumItems()>15?15:assayList->getNumItems());
    updateData();
    return 1;
}

long cMDIVaccine::onRplSchedule(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXString s,s1,s2;
    int i,r,c;
    unsigned int t,v1,v2,cat;
    sVAO *obj=(sVAO*)assayList->getItemData(assayList->getCurrentItem());

    r=obj->schedule->getCurrentRow();
    c=obj->schedule->getCurrentColumn();
    if(c!=0)
        return 1;
    s=obj->schedule->getItemText(r,c);
    if(s.empty())
    {
        obj->schedule->setItemText(r,c,"0-0");
        return 1;
    }
    for(i=0;i<s.length();i++)
        if(!isdigit(s.at(i)) && s.at(i)!='-')
        {
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),
                              (oLanguage->getText("str_invalid")+"\n"+
                               oLanguage->getText("str_invalid_age")).text());
            obj->schedule->setItemText(r,c,"0-0");
            obj->schedule->startInput(r,c);
            return 1;
        }
    s1=s.before('-');
    s2=s.after('-');
    if(s2.empty())
    {
        v1=0;
        v2=FXIntVal(s1);
    }
    else
    {
        v1=FXIntVal(s1);
        v2=FXIntVal(s2);
    }
    cat=cSpeciesManager::getAgeFormat(oDataLink->getSpecies());
    t=v1*cat+v2;
    v1=t/cat;
    v2=t%cat;
    if((unsigned int)v1>999)v1=999;
    obj->schedule->setItemText(r,c,FXStringFormat("%d-%d",v1,v2));
    return 1;
}

long cMDIVaccine::onRplBleeds(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXString s,s1,s2;
    int i,r,c;
    unsigned int t,v1,v2,cat;
    sVAO *obj=(sVAO*)assayList->getItemData(assayList->getCurrentItem());

    r=obj->bleeds->getCurrentRow();
    c=obj->bleeds->getCurrentColumn();
    
    s=obj->bleeds->getItemText(r,c);
    if(s.empty())
    {
        obj->bleeds->setItemText(r,c,"0-0");
        return 1;
    }
    for(i=0;i<s.length();i++)
        if(!isdigit(s.at(i)) && s.at(i)!='-')
        {
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),
                              (oLanguage->getText("str_invalid")+"\n"+
                               oLanguage->getText("str_invalid_age")).text());
            obj->bleeds->setItemText(r,c,"0-0");
            obj->bleeds->startInput(r,c);
            return 1;
        }
    s1=s.before('-');
    s2=s.after('-');
    if(s2.empty())
    {
        v1=0;
        v2=FXIntVal(s1);
    }
    else
    {
        v1=FXIntVal(s1);
        v2=FXIntVal(s2);
    }
    cat=cSpeciesManager::getAgeFormat(oDataLink->getSpecies());
    t=v1*cat+v2;
    v1=t/cat;
    v2=t%cat;
    if((unsigned int)v1>999)v1=999;
    obj->bleeds->setItemText(r,c,FXStringFormat("%d-%d",v1,v2));
    return 1;
}

long cMDIVaccine::onRefreshAsy(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!assayList->getNumItems())
        return 1;
    sCDLGO *data=(sCDLGO*)prData;
    FXString prOldAsy=*data->id;
    FXString prAsy=*data->id2;
    cDataResult *res=cAssayManager::getAssay(prAsy);
    for(int i=0;i<assayList->getNumItems();i++)
    {
        sVAO *obj=(sVAO*)assayList->getItemData(i);
        if(*obj->oid==prOldAsy)
        {
            *obj->oid=prAsy;
            assayList->setItemText(i,res->getCellString(0,0)+" - "+res->getCellString(0,1));
            break;
        }
    }
    res->free();
    return 1;
}
