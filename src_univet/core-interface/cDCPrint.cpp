#include "cDCPrint.h"

cDCPrint::cDCPrint(FXApp *a):
    FXDCPrint(a)
{
}

FXbool cDCPrint::beginPrint(FXPrinter& job)
{
    FXbool ret=FXDCPrint::beginPrint(job);
    Yr=(int)job.mediaheight;
    return ret;
}

void cDCPrint::drawText(FXint x,FXint y,const FXchar* string,FXuint len){
  FXfloat xx,yy;
  tfm(xx,yy,(FXfloat)x,(FXfloat)y);

  FXfloat fsize=0.1f*font->getSize();

  FXString fname=font->getName();
  if(fname=="times" || fname.lower()=="times new roman"){
    fname="Times";
    }
  else if(fname=="helvetica"){
    fname="Helvetica";
    }
  else if(fname=="courier" || fname.lower()=="courier new"){
    fname="Courier";
    }
  else{
    fname="Helvetica";
    }
  if(font->getWeight()==FXFont::Bold){
    if(font->getSlant()==FXFont::Italic){
      fname+="-BoldItalic";
      }
    else if(font->getSlant()==FXFont::Oblique){
      fname+="-BoldOblique";
      }
    else {
      fname+="-Bold";
      }
    }
  else{
    if(font->getSlant()==FXFont::Italic){
      fname+="-Italic";
      }
    else if(font->getSlant()==FXFont::Oblique){
      fname+="-Oblique";
      }
    }
  if(fname=="Times"){
    fname+="-Roman";
    }

  outf("(%s) %g %g %d /%s drawText\n",FXString(string).mid(0,len).text(),xx,yy,(int)fsize,fname.text());
  }

