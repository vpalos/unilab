#ifndef CMDIBASELINEMANAGER_H
#define CMDIBASELINEMANAGER_H

#include <fx.h>

#include "cSortList.h"
#include "cMDIChild.h"

class cMDIBaselineManager : public cMDIChild
{
    FXDECLARE(cMDIBaselineManager);

    private:
        cSortList *basList;
        FXMDIClient *client;
        FXPopup *popup;

        void update(void);
    protected:
        FXString getFileNameIm(void);
        FXString getFileNameEx(void);

        cMDIBaselineManager();

    public:
        cMDIBaselineManager(FXMDIClient *prP, const FXString &prName, FXIcon *prIc=NULL, FXPopup *prPup=NULL, FXuint prOpts=0, FXint prX=0, FXint prY=0, FXint prW=0, FXint prH=0);
        virtual ~cMDIBaselineManager();

        virtual void create();

        static void load(FXMDIClient *prP,FXPopup *prMenu);
        static FXbool isLoaded(void);
        static void unload(void);
        static void cmdRefresh(void);

        enum
        {
            ID_BASELINEMANAGER=FXMDIChild::ID_LAST,
            ID_BASELINELIST,

            CMD_BUT_NEW,
            CMD_BUT_OPEN,
            CMD_BUT_EXPORT,
            CMD_BUT_IMPORT,
            CMD_BUT_DUPLICATE,
            CMD_BUT_REMOVE,
            CMD_BUT_CLOSE,

            ID_LAST
        };

        long onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdNew(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdExport(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdImport(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdOpen(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdDuplicate(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdRemove(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif

