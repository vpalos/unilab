#ifndef CDLGREPORTCC_H
#define CDLGREPORTCC_H

#include <fx.h>

class cDLGReportCC : public FXDialogBox
{
    FXDECLARE(cDLGReportCC);
    private:
        FXButton *butAccept;
        FXButton *butCancel;
        
    protected:

        cDLGReportCC(){}
        
    public:
        FXuint choice;
        FXbool areSp,onlyTs;
        FXDataTarget chtype;
        FXCheckButton *bcCala;
        FXCheckButton *bcCalb;
        FXCheckButton *bcTiter;
        FXCheckButton *bcAm;
        FXCheckButton *bcSingle;
        FXCheckButton *bcLog2;
        FXCheckButton *bcEU;
        
        FXCheckButton *bUseGraphs;
        FXCheckButton *bUseData;
        FXCheckButton *bUseInfo;
    
        FXRadioButton *bTitGraphs;
        FXRadioButton *bBinGraphs;
        FXRadioButton *bTsGraphs;
        FXRadioButton *bSpGraphs;
    
        FXListBox *bsl;
        FXCheckButton *bSS;

        cDLGReportCC(FXWindow *prOwner,FXbool areTiters,FXbool areSecondary,FXbool areBins,FXbool areSamples,FXbool areOnlyTs);
        virtual ~cDLGReportCC();
        
        enum
        {
            ID_THIS=FXDialogBox::ID_LAST,
            ID_UPDATE,
            ID_BSLCHK,
            ID_ALLIO,
            
            CMD_BUT_APPLY,
            ID_LAST
        };

        long onUpdBslchk(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdUpdate(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdAllio(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdApply(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
