#include "engine.h"
#include "graphics.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cDLGBslCases.h"

FXDEFMAP(cDLGBslCases) mapDLGBslCases[]=
{
    FXMAPFUNC(SEL_UPDATE,cDLGBslCases::ID_UPDATE,cDLGBslCases::onUpdate),
    FXMAPFUNC(SEL_COMMAND,cDLGBslCases::CMD_BUT_APPLY,cDLGBslCases::onCmdApply),
};

FXIMPLEMENT(cDLGBslCases,FXDialogBox,mapDLGBslCases,ARRAYNUMBER(mapDLGBslCases))

cDLGBslCases::cDLGBslCases(FXWindow *prOwner) :
    FXDialogBox(prOwner,oLanguage->getText("str_bslmdi_csltitle")+"...",DECOR_TITLE|DECOR_BORDER),ftype(fchoice),agtype(agchoice)
{
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL);
        agchoice=1;
        FXLabel *_lb2=new FXLabel(_vframe0,oLanguage->getText("str_report_mtype"));
            _lb2->setTextColor(FXRGB(0,0,200));        
        FXGroupBox *_gb2=new FXGroupBox(_vframe0,(char*)NULL,GROUPBOX_NORMAL,0,0,0,0,17,0,0,0);
            bAMean=new FXRadioButton(_gb2,oLanguage->getText("str_report_ma"),&agtype,FXDataTarget::ID_OPTION+1);
            bAMean->setTextColor(FXRGB(120,120,120));
            bGMean=new FXRadioButton(_gb2,oLanguage->getText("str_report_mg"),&agtype,FXDataTarget::ID_OPTION+2);
            bGMean->setTextColor(FXRGB(120,120,120));
        new FXHorizontalSeparator(_vframe0);
        
        fchoice=1;
        FXLabel *_lb0=new FXLabel(_vframe0,oLanguage->getText("str_bslmdi_cslcaltype"));
            _lb0->setTextColor(FXRGB(0,0,200));        
        FXGroupBox *_gb0=new FXGroupBox(_vframe0,(char*)NULL,GROUPBOX_NORMAL,0,0,0,0,17,0,0,0);
            bMinmax=new FXRadioButton(_gb0,oLanguage->getText("str_bslmdi_cslmm"),&ftype,FXDataTarget::ID_OPTION+1);
            bMinmax->setTextColor(FXRGB(120,120,120));
            FXHorizontalFrame *_hframe10=new FXHorizontalFrame(_gb0,JUSTIFY_CENTER_Y|LAYOUT_FILL_X,0,0,0,0,0,17,0,0);
                bStdev=new FXRadioButton(_hframe10,oLanguage->getText("str_bslmdi_cslsd"),&ftype,FXDataTarget::ID_OPTION+2);
                bStdev->setTextColor(FXRGB(120,120,120));
                tfStdev=new FXTextField(_hframe10,5,this,ID_UPDATE,TEXTFIELD_NORMAL|TEXTFIELD_REAL|LAYOUT_RIGHT);
                tfStdev->setText("0");
                tfStdev->disable();
            FXHorizontalFrame *_hframe11=new FXHorizontalFrame(_gb0,JUSTIFY_CENTER_Y|LAYOUT_FILL_X,0,0,0,0,0,17,0,0);
                bPercent=new FXRadioButton(_hframe11,oLanguage->getText("str_bslmdi_cslpr"),&ftype,FXDataTarget::ID_OPTION+3);
                bPercent->setTextColor(FXRGB(120,120,120));
                tfPercent=new FXTextField(_hframe11,5,this,ID_UPDATE,TEXTFIELD_NORMAL|TEXTFIELD_REAL|LAYOUT_RIGHT);
                tfPercent->setText("0");
                tfPercent->disable();

        new FXHorizontalSeparator(_vframe0);
        bcAm=new FXCheckButton(_vframe0,oLanguage->getText("str_rdgmdi_mean"));
        bcAm->setCheck(true);
        bcAm->setTextColor(FXRGB(0,0,200));

        new FXHorizontalSeparator(_vframe0);
        FXHorizontalFrame *_hframe100=new FXHorizontalFrame(_vframe0,LAYOUT_CENTER_X);
        butCancel=new FXButton(_hframe100,oLanguage->getText("str_but_cancel"),NULL,this,ID_CANCEL,BUTTON_DEFAULT|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
        butAccept=new FXButton(_hframe100,oLanguage->getText("str_but_ok"),NULL,this,CMD_BUT_APPLY,BUTTON_DEFAULT|BUTTON_INITIAL|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
}

cDLGBslCases::~cDLGBslCases()
{
}

long cDLGBslCases::onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData)
{
    handle(NULL,FXSEL(SEL_COMMAND,ID_ACCEPT),NULL);
    return 1;
}

long cDLGBslCases::onUpdate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(agchoice==2)
    {
        bStdev->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        if(fchoice==2)
        {
            bMinmax->setCheck(true);
            bStdev->setCheck(false);
            fchoice=1;
        }
    }
    else
        bStdev->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    if(fchoice==2)
    {
        tfPercent->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        tfStdev->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    else if(fchoice==3)
    {
        tfPercent->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        tfStdev->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }
    else
    {
        tfPercent->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        tfStdev->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }
    return 1;
}


