#ifndef CDATECHOOSER_H
#define CDATECHOOSER_H

#include <fx.h>
#include <time.h>

class FXwDate {

private:

    unsigned long julian;
    short 	year;
    short	month;
    short 	day;
    char 	day_of_week;	/* 	1 = Sunday, ... 7 = Saturday */
    char	separator;
    short   hour;
    short   minute;
    short   second;

    FXint lang;

private:
    void julian_to_mdy ();
    void julian_to_wday ();
    void mdy_to_julian ();

    void validate();


public:

    enum Wday {SUN=1,MON,TUE,WED,THU,FRI,SAT};

    FXwDate();
    FXwDate(const FXwDate &dt);
    FXwDate(const time_t time);
    FXwDate(int year, int month, int day,int hour=0, int minute=0, int second=0);
    FXwDate(FXString datestring);
    FXwDate(int quarter);

    ~FXwDate() {}
    void operator= (FXwDate const  &param);
    FXwDate operator + (FXwDate param);
    FXwDate operator - (FXwDate param);
    FXwDate operator ++(int);
    FXwDate operator --(int);
    bool operator < (FXwDate param);
    bool operator > (FXwDate param);
    bool operator <= (FXwDate param);
    bool operator >= (FXwDate param);
    bool operator == (FXwDate param);
    bool operator != (FXwDate param) { return !(*this==param);};

    inline  unsigned long juliandate() { return julian; };

    time_t getSystime();
    int getYear();
    int getMonth();
    int getDay();
    int getDayOfWeek();			/* 	0 = Sunday, ... 6 = Saturday */
    int getDaysOfMonth(int month);
    int getHour();
    int getMinute();
    int getSecond();
    FXString getDayName();
    FXString getShortDayName();
    int getTTMM();
    int getTTMMJJ();
    short getYY();
    int getQuarter();

    void setYear(int YYYY);
    void setMonth(int MM);
    void setDay(int DD);
    void setMinute(int mm);
    void setHour(int hh);
    void setSec(int ss);
    void setNew(int YY, int MM, int DD,int hh = 0, int mm=0, int ss = 0);
    void setSystime(time_t time);
    void setDate(FXString newdate);
    void setQuarter(int quarter);
    void setHMS(int hh, int mm, int ss);

    bool isLeapYear();

    FXString text();
    FXString textWithDayName();
    FXString textWidthoutYear();
    FXString timeToText();
    FXString monthYearToText();
    FXString quarterToShortText();
    FXString quarterToText();
    FXString getMonthname();



    static bool checkStringFormat(FXString datestring);

    static FXString getShortDayName(FXint index); // 0 ... Mon
    static FXString getDayName(FXint index); // 0 ... Monday
    static FXString getMonthName(FXint index); // 0 ... January
    static FXString getMonthFName(int i);
    static FXString getWDayName(int i);

};

class FXwDatePickerLabel : public FXLabel
{
    FXDECLARE(FXwDatePickerLabel)
private:
    FXint selector;
    FXObject* target;
    FXbool istoday;
    FXColor todayColor;
    FXint dayvalue;
protected:
    FXwDatePickerLabel() { selector=0; target=NULL; istoday=false;dayvalue=-1;};
private:
    FXwDatePickerLabel(const FXwDatePickerLabel&);
    FXwDatePickerLabel &operator=(const FXwDatePickerLabel&);

public:
    /* Message Callbacks */
    long onClick(FXObject*,FXSelector,void*);
    long onLeftBtnPress(FXObject*,FXSelector,void*);
    long onPaint(FXObject*,FXSelector,void*);
public:
    enum {
        ID_NOP=FXLabel::ID_LAST,ID_LAST
    };
public:

    /// Construct
    FXwDatePickerLabel(FXComposite* p,FXObject* tgt=NULL,FXSelector sel=0,const FXString& text="",FXIcon* ic=0,FXuint opts=LABEL_NORMAL,FXint x=0,FXint y=0,FXint w=0,FXint h=0,FXint pl=DEFAULT_PAD,FXint pr=DEFAULT_PAD,FXint pt=DEFAULT_PAD,FXint pb=DEFAULT_PAD);
    // This Label is today
    void setToday(FXbool value = true) { istoday=value; }
    FXbool isToday() { return istoday; }

    // Setting color for today
    void setTodayColor(FXColor c) { todayColor = c; }
    // Getting color for today
    FXColor getTodayColor() { return todayColor; }
    // setting value for day
    void setDayValue(FXint dv) { dayvalue=dv; }
    // getting value for day
    FXint getDayValue() { return dayvalue; }
};

class FXwDatePickerHoliday
{
    public:
        FXwDatePickerHoliday() {};
        virtual ~FXwDatePickerHoliday() {};

        virtual FXbool isHoliday(FXwDate &date) = 0;

};

class cDateChooser : public FXVerticalFrame
{
    FXDECLARE(cDateChooser)
private:

    FXint selector;
    FXObject* target;
    FXLabel *monthname, *year;
    FXColor monthbkcolor,yearbkcolor;
    FXColor selectColor,offdaycolor, holidaycolor;
    FXArrowButton * next,*prev;
    FXHorizontalFrame * navframe;
    FXObjectList dayfields;
    FXMatrix * days;
    FXColor boderColorToday;
    FXwDatePickerLabel * selected;
    FXwDate actualDate;
    FXwDate selectedDate; // current selected day
    FXwDatePickerHoliday *holiday;    /* will be deleted automaticly */


protected:
    cDateChooser();
private:
    cDateChooser(const cDateChooser&);
    cDateChooser &operator=(const cDateChooser&);
    ~cDateChooser();

public:
    /* Message Callbacks */
    long onLabelDBC(FXObject*,FXSelector,void*);
    long onLabelSelected(FXObject*,FXSelector,void*);
    long onCmdPrev(FXObject*,FXSelector,void*);
    long onCmdNext(FXObject*,FXSelector,void*);
    long onCmdToday(FXObject*,FXSelector,void*);


public:
    enum {
        ID_NOP=FXVerticalFrame::ID_LAST,ID_BUTTON_PREV,ID_BUTTON_NEXT,ID_BUTTON_TODAY,ID_LABEL,ID_LAST
    };
public:

    /// Construct color well with initial color clr
    cDateChooser(FXComposite* p,FXwDatePickerHoliday *holi=NULL,FXObject* tgt=NULL,FXSelector sel=0,FXuint opts=0,FXint x=0,FXint y=0,FXint w=0,FXint h=0,FXint pl=DEFAULT_PAD,FXint pr=DEFAULT_PAD,FXint pt=DEFAULT_PAD,FXint pb=DEFAULT_PAD);

    /// Create server-side resources
    virtual void create();

    /// Detach server-side resources
    virtual void detach();

    void fill(FXwDate &adate);
    // reset picker to today
    void reset();
    // getting selected FXwDate
    FXwDate getSelectedDate();
    // setting selected FXwDate   //
    FXwDate setSelectedDate(FXwDate d); //

    static FXString selectDate(FXWindow *prOwner=NULL);

    // setting country dependend object to check holidays
    void setHolidayObject(FXwDatePickerHoliday *obj) { holiday=obj; }

    // setting colors
    void setMonthBkColor(FXColor col);
    void setYearBkColor(FXColor col);
    void setSelectionColor(FXColor col);
    void setOffdayColor(FXColor col);
    void setHolidayColor(FXColor col);

    // getting colors
    FXColor getMonthBkColor() { return  monthbkcolor; }
    FXColor getYearBkColor() { return  yearbkcolor; }
    FXColor getSelectionColor() { return  selectColor; }
    FXColor getOffdayColor() { return  offdaycolor; }
    FXColor getHolidayColor() { return  holidaycolor; }

};



#endif
