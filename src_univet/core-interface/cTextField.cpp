#include "cTextField.h"

FXDEFMAP(cTextField) mapTextField[]=
{
    FXMAPFUNC(SEL_PAINT,0,cTextField::onPaint),
};

FXIMPLEMENT(cTextField,FXTextField,mapTextField,ARRAYNUMBER(mapTextField))

cTextField::cTextField()
{
}

cTextField::cTextField(FXComposite *p, FXint ncols, FXObject *tgt, FXSelector sel, FXuint opts, FXint x, FXint y, FXint w, FXint h, FXint pl, FXint pr, FXint pt, FXint pb):
    FXTextField(p,ncols,tgt,sel,opts,x,y,w,h,pl,pr,pt,pb)
{
}

cTextField::~cTextField()
{
}

long cTextField::onPaint(FXObject *prSender,FXSelector prSelector,void *prData)
{
  FXDC *dc;
  
  if(prSender==NULL && prSelector==0)
      dc=(FXDC*)prData;
  else
  {
      FXEvent *ev=(FXEvent*)prData;
      dc=new FXDCWindow(this,ev);
  }
  
  dc->clearClipRectangle();
  
  // Draw frame
  drawFrame(*(FXDCWindow*)dc,0,0,width,height);

  // Gray background if disabled
  if(isEnabled())
    dc->setForeground(backColor);
  else
    dc->setForeground(baseColor);

  // Draw background
  dc->fillRectangle(border,border,width-(border<<1),height-(border<<1));

  // Draw text, clipped against frame interior
  dc->setClipRectangle(border,border,width-(border<<1),height-(border<<1));
  drawTextRange(*(FXDCWindow*)dc,0,contents.length());

  // Draw caret
  if(flags&FLAG_CARET){
    int xx=coord(cursor)-1;
    dc->setForeground(cursorColor);
    dc->fillRectangle(xx,padtop+border,1,height-padbottom-padtop-(border<<1));
    dc->fillRectangle(xx-2,padtop+border,5,1);
    dc->fillRectangle(xx-2,height-border-padbottom-1,5,1);
    }
    
  if(dc  && (prSender!=NULL || prSelector!=0))
    delete dc;
  return 1;
}


