#ifndef CDLGREPORTTB_H
#define CDLGREPORTTB_H

#include <fx.h>

class cDLGReportTB : public FXDialogBox
{
    FXDECLARE(cDLGReportTB);
    private:
        FXButton *butAccept;
        FXButton *butCancel;
        
    protected:
        FXDataTarget agtype;
        FXRadioButton *bAMean;
        FXRadioButton *bGMean;

        FXDataTarget stype;
        FXRadioButton *bAssay;
        FXRadioButton *bCase;

        cDLGReportTB(){}
        
    public:
        FXuint schoice,agchoice;
        FXCheckButton *bcAm;
        
        cDLGReportTB(FXWindow *prOwner);
        virtual ~cDLGReportTB();
        
        enum
        {
            ID_THIS=FXDialogBox::ID_LAST,
            ID_UPDATE,
            ID_FIELD,
            
            CMD_BUT_APPLY,
            ID_LAST
        };

        long onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
