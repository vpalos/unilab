#ifndef CSORTLIST_H
#define CSORTLIST_H

#include <fx.h>

class cSortList : public FXIconList
{
    FXDECLARE(cSortList);
    
    private:
        FXint sortColumn;
        FXbool sortDirection;
    
    protected:
        cSortList();
        FXint __listSortFunction(const FXIconItem *prItemA,const FXIconItem *prItemB,int prIndex);
    
    public:
        cSortList(FXComposite *p, FXObject *tgt=NULL, FXSelector sel=0, FXuint opts=ICONLIST_NORMAL, FXint x=0, FXint y=0, FXint w=0, FXint h=0);
        virtual ~cSortList();
        
        virtual void create();
        
        virtual void sortItems(void);
        
        enum
        {
            ID_SORTLIST=FXIconList::ID_LAST,
            
            CMD_SORT,
            
            ID_LAST
        };
        
        long onCmdSort(FXObject *prSender,FXSelector prSelector,void *prPmtr);
};

#endif

