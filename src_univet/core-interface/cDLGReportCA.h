#ifndef CDLGREPORTCA_H
#define CDLGREPORTCA_H

#include <fx.h>

class cDLGReportCA : public FXDialogBox
{
    FXDECLARE(cDLGReportCA);
    private:
        FXButton *butAccept;
        FXButton *butCancel;
        
    protected:

        cDLGReportCA(){}
        
    public:
        FXuint choice;
        FXDataTarget chtype;
        FXCheckButton *bcCala;
        FXCheckButton *bcCalb;
        FXCheckButton *bcTiter;
        FXCheckButton *bcTiters;
        FXCheckButton *bcBins;
        FXCheckButton *bcResult;
        FXCheckButton *bcResult2;
        //FXCheckButton *bcBins2;
        FXCheckButton *bcAm;
        FXCheckButton *bcAge;
        FXCheckButton *bcLog2;
        FXCheckButton *bcEU;
        FXCheckButton *bcBreed1;
        FXCheckButton *bcBreed2;
        
        FXCheckButton *bUseGraphs;
        FXCheckButton *bUseData;
        FXCheckButton *bUseInfo;
        FXCheckButton *bUsePlate;
        
        FXCheckButton *bcPCala;
    
        FXRadioButton *bTitGraphs;
        FXRadioButton *bBinGraphs;
        FXRadioButton *bTsGraphs;
        FXRadioButton *bSpGraphs;
        
        FXListBox *bsl;
        FXCheckButton *bSS;
    
        cDLGReportCA(FXWindow *prOwner,FXbool areTiters,FXbool areSecondary,FXbool areBins,FXbool areOnlyTs);
        virtual ~cDLGReportCA();
        
        enum
        {
            ID_THIS=FXDialogBox::ID_LAST,
            ID_UPDATE,
            ID_BSLCHK,
            
            CMD_BUT_APPLY,
            ID_LAST
        };

        long onCmdUpdate(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdBslchk(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdApply(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
