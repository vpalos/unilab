#include "engine.h"
#include "graphics.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cDLGReportCS.h"
#include "cBaselineManager.h"

FXDEFMAP(cDLGReportCS) mapDLGReportCS[]=
{
    FXMAPFUNC(SEL_UPDATE,cDLGReportCS::ID_BSLCHK,cDLGReportCS::onUpdBslchk),
    FXMAPFUNC(SEL_UPDATE,cDLGReportCS::ID_UPDATE,cDLGReportCS::onCmdUpdate),
    FXMAPFUNC(SEL_UPDATE,cDLGReportCS::CMD_BUT_APPLY,cDLGReportCS::onUpdApply),
    FXMAPFUNC(SEL_COMMAND,cDLGReportCS::CMD_BUT_APPLY,cDLGReportCS::onCmdApply),
};

FXIMPLEMENT(cDLGReportCS,FXDialogBox,mapDLGReportCS,ARRAYNUMBER(mapDLGReportCS))

cDLGReportCS::cDLGReportCS(FXWindow *prOwner,FXbool areTiters,FXbool areSecondary) :
    FXDialogBox(prOwner,oLanguage->getText("str_report_cs")+"...",DECOR_TITLE|DECOR_BORDER),chtype(choice),agtype(agchoice),aptype(apchoice)
{
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL);
        bUseInfo=new FXCheckButton(_vframe0,oLanguage->getText("str_report_showInfo"),this,ID_UPDATE);
        bUseInfo->setCheck(true);
        bUseInfo->setTextColor(FXRGB(0,0,200));
        new FXHorizontalSeparator(_vframe0);
        apchoice=1;
        FXLabel *_lb0=new FXLabel(_vframe0,oLanguage->getText("str_report_type"));
            _lb0->setTextColor(FXRGB(0,0,200));        
        FXGroupBox *_gb0=new FXGroupBox(_vframe0,(char*)NULL,GROUPBOX_NORMAL,0,0,0,0,17,0,0,0);
            bAssay=new FXRadioButton(_gb0,oLanguage->getText("str_report_ta"),&aptype,FXDataTarget::ID_OPTION+1);
            bAssay->setTextColor(FXRGB(160,0,0));
            bPop=new FXRadioButton(_gb0,oLanguage->getText("str_report_tp"),&aptype,FXDataTarget::ID_OPTION+2);
            bPop->setTextColor(FXRGB(160,0,0));
        choice=2;
        new FXHorizontalSeparator(_vframe0);
        FXLabel *_lb1=new FXLabel(_vframe0,oLanguage->getText("str_report_gdata"));
            _lb1->setTextColor(FXRGB(0,0,200));        
        FXGroupBox *_gb1=new FXGroupBox(_vframe0,(char*)NULL,GROUPBOX_NORMAL,0,0,0,0,17,0,0,0);
            if(areTiters)
            {
                bTitGraphs=new FXRadioButton(_gb1,oLanguage->getText("str_report_tit"),&chtype,FXDataTarget::ID_OPTION+1);
                bTitGraphs->setTextColor(FXRGB(120,120,120));
                choice=1;
            }
            else
                bTitGraphs=NULL;
            bSpGraphs=new FXRadioButton(_gb1,oLanguage->getText("str_report_sp"),&chtype,FXDataTarget::ID_OPTION+2);
            bSpGraphs->setTextColor(FXRGB(120,120,120));
        new FXHorizontalSeparator(_vframe0);
        agchoice=2;
        FXLabel *_lb2=new FXLabel(_vframe0,oLanguage->getText("str_report_mtype"));
            _lb2->setTextColor(FXRGB(0,0,200));        
        FXGroupBox *_gb2=new FXGroupBox(_vframe0,(char*)NULL,GROUPBOX_NORMAL,0,0,0,0,17,0,0,0);
            bAMean=new FXRadioButton(_gb2,oLanguage->getText("str_report_ma"),&agtype,FXDataTarget::ID_OPTION+1);
            bAMean->setTextColor(FXRGB(120,120,120));
            bGMean=new FXRadioButton(_gb2,oLanguage->getText("str_report_mg"),&agtype,FXDataTarget::ID_OPTION+2);
            bGMean->setTextColor(FXRGB(120,120,120));
        new FXHorizontalSeparator(_vframe0);
        bUseData=new FXCheckButton(_vframe0,oLanguage->getText("str_report_showStats"),this,ID_UPDATE);
        bUseData->setTextColor(FXRGB(0,0,200));
        bUseData->setCheck(true);
        FXVerticalFrame *_vf=new FXVerticalFrame (_vframe0,LAYOUT_FILL_X,0,0,0,0,17);
            FXVerticalFrame *_vframe1=new FXVerticalFrame(_vf,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
                bcCala=new FXCheckButton(_vframe1,oLanguage->getText("str_asymdi_cala"));
                bcCala->setTextColor(FXRGB(120,120,120));
                bcCala->setCheck(true);
                if(areSecondary)
                {
                    bcCalb=new FXCheckButton(_vframe1,oLanguage->getText("str_asymdi_calb"));
                    bcCalb->setCheck(true);
                    bcCalb->setTextColor(FXRGB(120,120,120));
                }
                else
                    bcCalb=NULL;
                bcEU=new FXCheckButton(_vframe1,oLanguage->getText("str_rdgmdi_eu"));
                bcEU->setTextColor(FXRGB(120,120,120));
                if(areTiters)
                {
                    bcTiter=new FXCheckButton(_vframe1,oLanguage->getText("str_asymdi_titer"));
                    bcTiter->setCheck(true);
                    bcTiter->setTextColor(FXRGB(120,120,120));
                    bcLog2=new FXCheckButton(_vframe1,oLanguage->getText("str_rdgmdi_log2"));
                    bcLog2->setTextColor(FXRGB(120,120,120));
                }
                else
                {
                    bcTiter=NULL;
                    bcLog2=NULL;
                }
        new FXHorizontalSeparator(_vframe0);
        bcAm=new FXCheckButton(_vframe0,oLanguage->getText("str_rdgmdi_mean"));
        bcAm->setCheck(true);
        bcAm->setTextColor(FXRGB(0,0,200));
        new FXHorizontalSeparator(_vframe0);

        if(areTiters)
        {
            FXLabel *_lb0=new FXLabel(_vframe0,oLanguage->getText("str_report_bsl"));
                _lb0->setTextColor(FXRGB(0,120,0));
            FXVerticalFrame *_gb30=new FXVerticalFrame (_vframe0,LAYOUT_FILL_X,0,0,0,0,17,0,0,0);
                FXVerticalFrame *_gb31=new FXVerticalFrame (_gb30,LAYOUT_FILL_X,0,0,0,0,2,0,0,0);
                    bsl=new FXListBox(_gb31,NULL,0,FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X);
                    bsl->setTextColor(FXRGB(0,120,0));
                    bsl->appendItem(oLanguage->getText("str_report_bslno"));
                    sBaselineObject *res=cBaselineManager::listBaselines();
                    if(res==NULL)
                        return;
                    for(int i=0;i<cBaselineManager::getBaselineCount();i++)
                        bsl->appendItem(*res[i].id);
                    free(res);
                    bsl->setNumVisible(bsl->getNumItems()>7?7:bsl->getNumItems());
                bSS=new FXCheckButton(_gb30,oLanguage->getText("str_report_bslss"),this,ID_BSLCHK);
                bSS->setTextColor(FXRGB(0,120,0));
    
            new FXHorizontalSeparator(_vframe0);
        }
        else
            bsl=NULL;
        
        FXHorizontalFrame *_hframe100=new FXHorizontalFrame(_vframe0,LAYOUT_CENTER_X);
        butCancel=new FXButton(_hframe100,oLanguage->getText("str_but_cancel"),NULL,this,ID_CANCEL,BUTTON_DEFAULT|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
        butAccept=new FXButton(_hframe100,oLanguage->getText("str_but_ok"),NULL,this,CMD_BUT_APPLY,BUTTON_DEFAULT|BUTTON_INITIAL|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
}

cDLGReportCS::~cDLGReportCS()
{
}

long cDLGReportCS::onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData)
{
    handle(NULL,FXSEL(SEL_COMMAND,ID_ACCEPT),NULL);
    return 1;
}

long cDLGReportCS::onUpdApply(FXObject *prSender,FXSelector prSelector,void *prData)
{
    return 1;
}

long cDLGReportCS::onCmdUpdate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(bUseData->getCheck())
    {
        bcCala->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bcCalb)
            bcCalb->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bcTiter)
            bcTiter->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bcLog2)
            bcLog2->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        bcEU->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    else
    {
        bcCala->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        if(bcCalb)
            bcCalb->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        if(bcTiter)
            bcTiter->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        if(bcLog2)
            bcLog2->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        bcEU->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }
    return 1;
}

long cDLGReportCS::onUpdBslchk(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(choice!=1 && choice!=4)
    {
        bsl->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        bSS->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }
    else
    {
        bsl->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bsl->getCurrentItem()==0)
            bSS->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        else
            bSS->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    return 1;
}


