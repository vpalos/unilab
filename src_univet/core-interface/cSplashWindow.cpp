#include "cWinMain.h"
#include "cSplashWindow.h"

FXDEFMAP(cSplashWindow) mapSplashWindow[]=
{
    FXMAPFUNC(SEL_TIMEOUT,FXWindow::ID_DELETE,cSplashWindow::onHide),
    FXMAPFUNC(SEL_LEFTBUTTONPRESS,0,cSplashWindow::onHide),
    FXMAPFUNC(SEL_KEYPRESS,0,cSplashWindow::onHide),
    //FXMAPFUNC(SEL_FOCUSOUT,0,cSplashWindow::onHide),
};

FXIMPLEMENT(cSplashWindow,FXSplashWindow,mapSplashWindow,ARRAYNUMBER(mapSplashWindow))

cSplashWindow::cSplashWindow()
{
}

cSplashWindow::cSplashWindow(FXWindow *ow, FXIcon *ic, FXuint opts, FXuint ms):
    FXSplashWindow(ow,ic,opts,ms)
{
    over=false;
}

cSplashWindow::~cSplashWindow()
{
}

long cSplashWindow::onHide(FXObject *prSender,FXSelector prSelector,void *prData)
{
    over=true;
    hide();
    //oWinMain->handle(this,FXSEL(SEL_COMMAND,cWinMain::CMD_MSETTINGS_TECH),NULL);
    close();
    return 1;
}


