#ifndef CDLGCASEPOPEDIT_H
#define CDLGCASEPOPEDIT_H

#include <fx.h>

#include "cColorTable.h"

class cDLGCasePopEdit : public FXDialogBox
{
    FXDECLARE(cDLGCasePopEdit);
    private:
    
    protected:
        FXString cse,asy,pop,dt1,dt2;
        FXTabBook *tbMain;
        FXVerticalFrame *vfCase;
        FXVerticalFrame *vfPop;
        FXHorizontalFrame *_hframe1;
        
        FXTabItem *tiCase;
        FXTextField *tfCaseId;
        FXTextField *tfCaseBlDate;
        FXTextField *tfCaseAge;
        FXText *tfCaseComments;
        FXListBox *lxCaseVet;
        FXComboBox *cxCaseSpType;
        FXComboBox *cxCaseReason;
        
        FXTabItem *tiPop;
        FXTextField *tfPopId;
        FXTextField *tfPopBrDate;
        FXTextField *tfPopUnit;
        FXTextField *tfPopLoc;
        FXTextField *tfPopSubLoc;
        FXText *tfPopComments;
        FXListBox *lxPopOwner;
        FXListBox *lxPopGrower;
        FXListBox *lxPopVaccine;
        FXListBox *lxPopBreed1;
        FXListBox *lxPopBreed2;
        
        FXIconList *tbPred;

        FXString selectPop(void);
        
        cDLGCasePopEdit(){}
        
    public:
        cDLGCasePopEdit(FXWindow *prOwner,FXString prCaseId,FXString prAssayId);
        virtual ~cDLGCasePopEdit();
        
        FXuint execute(FXuint prPlacement);
        
        enum
        {
            ID_DLGCASEPOPEDIT=FXDialogBox::ID_LAST,
            ID_TBMAIN,
            
            ID_UPDATE,
            CMD_ADDP,
            CMD_DELP,
            
            CMD_DC1,
            CMD_DC0,
            CMD_BUT_APPLY,
            ID_LAST
        };
        
        FXString getCase(void);
        FXString getPopulation(void);
        FXString getAge(void);
        FXString getBlDate(void);

        long onCmdAddPred(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdUpdate(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdDelPred(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdWindow(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdDateChooser0(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdDateChooser1(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
