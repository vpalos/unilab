#include <time.h>
#include <ctype.h>
#include <stdlib.h>

#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cColorsManager.h"
#include "cFormulaRatio.h"
#include "cSpeciesManager.h"
#include "cBreedManager.h"
#include "cCaseManager.h"
#include "cPopulationManager.h"
#include "cWinMain.h"
#include "cMDIBreed.h"
#include "cMDIBreedManager.h"

FXDEFMAP(cMDIBreed) mapMDIBreed[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIBreed::ID_MDIBREED,cMDIBreed::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIBreed::ID_BREID,cMDIBreed::onCmdTfId),
    FXMAPFUNC(SEL_CHANGED,cMDIBreed::ID_BREID,cMDIBreed::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIBreed::ID_BRELAB,cMDIBreed::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIBreed::ID_BREADD,cMDIBreed::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIBreed::ID_BRECITY,cMDIBreed::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIBreed::ID_BRESTATE,cMDIBreed::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIBreed::ID_BREZIP,cMDIBreed::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIBreed::ID_BREPHONE,cMDIBreed::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIBreed::ID_BREEMAIL,cMDIBreed::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIBreed::ID_BRECOMMENTS,cMDIBreed::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIBreed::CMD_CLOSE,cMDIBreed::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIBreed::CMD_SAVE,cMDIBreed::onCmdSave),
    FXMAPFUNC(SEL_COMMAND,cMDIBreed::CMD_PRINT,cMDIBreed::onCmdPrint),
};

FXIMPLEMENT(cMDIBreed,cMDIChild,mapMDIBreed,ARRAYNUMBER(mapMDIBreed))

cMDIBreed::cMDIBreed()
{
}

void cMDIBreed::updateData(void)
{
    saved=false;
}

FXbool cMDIBreed::saveData(void)
{
    if(saved)
        return true;
    
    if(!name.empty() && cBreedManager::breedSolid(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasebresolid").text());
        saved=true;
        return 1;
    }
    
    FXString oldname=name;
    name=tfId->getText();
    if(name!=oldname && cBreedManager::breedExists(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
        tfId->setText(cBreedManager::getNewID());
        setTitle(tfId->getText());
        updateData();
        return false;
    }
    this->setTitle(name);
    
    if(oldname.empty())
        cBreedManager::addBreed(name,tfLab->getText(),comments->getText());
    else
        cBreedManager::setBreed(oldname,name,tfLab->getText(),comments->getText());
    
    saved=true;
    cMDIBreedManager::cmdRefresh();
    return true;
}

cMDIBreed::cMDIBreed(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH) 
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_MDIBREED);
    saved=false;
    name="";
    
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL,0,0,0,0);
        new FXLabel(_vframe0,oLanguage->getText("str_bremdi_id"));
        tfId=new FXTextField(_vframe0,35,this,ID_BREID);
            tfId->setText(prName);
            tfId->setSelection(0,tfId->getText().length());
            tfId->setFocus();

        new FXLabel(_vframe0,oLanguage->getText("str_bremdi_title"));
        tfLab=new FXTextField(_vframe0,35,this,ID_BRELAB);
        new FXLabel(_vframe0,oLanguage->getText("str_bremdi_comments"));
        FXVerticalFrame *_vframe8=new FXVerticalFrame(_vframe0,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
        comments=new FXText(_vframe8,this,ID_BRECOMMENTS,TEXT_WORDWRAP|TEXT_NO_TABS|LAYOUT_FILL);
        FXHorizontalFrame *_hframe8=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,10,0);
            //new FXButton(_hframe8,oLanguage->getText("str_bremdi_print"),new FXGIFIcon(getApp(),data_settings_printer),this,CMD_PRINT,FRAME_RAISED|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH,0,0,100,0);
            new FXButton(_hframe8,oLanguage->getText("str_but_close"),NULL,this,CMD_CLOSE,FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_FIX_WIDTH|LAYOUT_RIGHT,0,0,100,0);
            new FXButton(_hframe8,oLanguage->getText("str_bremdi_save"),NULL,this,CMD_SAVE,FRAME_RAISED|BUTTON_DEFAULT|BUTTON_INITIAL|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_RIGHT|LAYOUT_FIX_WIDTH,0,0,100,0);
}

cMDIBreed::~cMDIBreed()
{
}

void cMDIBreed::create()
{
    cMDIChild::create();
    show();
    updateData();
}

FXbool cMDIBreed::canClose(void)
{
    if(!saved)
    {
        setFocus();
        int res=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),oLanguage->getText("str_qsave").text());
        switch(res)
        {
            case MBOX_CLICKED_YES:
                if(!saveData())
                    return false;
                break;
            case MBOX_CLICKED_CANCEL:
                return false;
            default:
                break;
        }
    }
    return true;
}

FXbool cMDIBreed::loadBreed(const FXString &prId)
{
    if(!cBreedManager::breedExists(prId))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_openbre").text());
        return false;
    }
    if(!oWinMain->isWindow(prId))
        return oWinMain->raiseWindow(prId);
    
    cDataResult *res=cBreedManager::getBreed(prId);
    if(res==NULL)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_openbre").text());
        return false;
    }
    
    name=res->getCellString(0,0);
    tfId->setText(name);
    tfLab->setText(res->getCellString(0,1));
    comments->setText(res->getCellString(0,3));
    
    setTitle(name);
    updateData();
    saved=true;
    return true;
}

long cMDIBreed::onCmdTfId(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    if(!tfId->getText().empty())
    {
        if(!cBreedManager::breedExists(tfId->getText()))
            return 1;
        else
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
    }
    tfId->setText(getTitle());
    return 1;
}

long cMDIBreed::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(canClose())
    {
        close();
        return 1; 
    }
    return 0;
}

long cMDIBreed::onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    return 1;
}

long cMDIBreed::onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData)
{
    saveData();
    if(saved)
        onCmdClose(NULL,0,NULL);
    return 1;
}

long cMDIBreed::onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData)
{
    
    
    // TODO
    
    
    return 1;
}

