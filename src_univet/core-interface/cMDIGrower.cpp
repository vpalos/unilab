#include <time.h>
#include <ctype.h>
#include <stdlib.h>

#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cColorsManager.h"
#include "cFormulaRatio.h"
#include "cSpeciesManager.h"
#include "cGrowerManager.h"
#include "cCaseManager.h"
#include "cPopulationManager.h"
#include "cWinMain.h"
#include "cMDIGrower.h"
#include "cMDIGrowerManager.h"

FXDEFMAP(cMDIGrower) mapMDIGrower[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIGrower::ID_MDIGROWER,cMDIGrower::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIGrower::ID_GRWID,cMDIGrower::onCmdTfId),
    FXMAPFUNC(SEL_CHANGED,cMDIGrower::ID_GRWID,cMDIGrower::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIGrower::ID_GRWLAB,cMDIGrower::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIGrower::ID_GRWADD,cMDIGrower::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIGrower::ID_GRWCITY,cMDIGrower::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIGrower::ID_GRWSTATE,cMDIGrower::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIGrower::ID_GRWZIP,cMDIGrower::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIGrower::ID_GRWPHONE,cMDIGrower::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIGrower::ID_GRWFAX,cMDIGrower::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIGrower::ID_GRWEMAIL,cMDIGrower::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIGrower::ID_GRWCOMMENTS,cMDIGrower::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIGrower::CMD_CLOSE,cMDIGrower::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIGrower::CMD_SAVE,cMDIGrower::onCmdSave),
    FXMAPFUNC(SEL_COMMAND,cMDIGrower::CMD_PRINT,cMDIGrower::onCmdPrint),
};

FXIMPLEMENT(cMDIGrower,cMDIChild,mapMDIGrower,ARRAYNUMBER(mapMDIGrower))

cMDIGrower::cMDIGrower()
{
}

void cMDIGrower::updateData(void)
{
    saved=false;
}

FXbool cMDIGrower::saveData(void)
{
    if(saved)
        return true;
    
    if(!name.empty() && cGrowerManager::growerSolid(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasegrwsolid").text());
        saved=true;
        return 1;
    }
    
    FXString oldname=name;
    name=tfId->getText();
    if(name!=oldname && cGrowerManager::growerExists(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
        tfId->setText(cGrowerManager::getNewID());
        setTitle(tfId->getText());
        updateData();
        return false;
    }
    this->setTitle(name);
    
    if(oldname.empty())
        cGrowerManager::addGrower(name,tfLab->getText(),tfAdd->getText(),tfCity->getText(),tfState->getText(),tfZip->getText(),tfPhone->getText(),tfFax->getText(),tfEmail->getText(),comments->getText());
    else
        cGrowerManager::setGrower(oldname,name,tfLab->getText(),tfAdd->getText(),tfCity->getText(),tfState->getText(),tfZip->getText(),tfPhone->getText(),tfFax->getText(),tfEmail->getText(),comments->getText());
    
    saved=true;
    cMDIGrowerManager::cmdRefresh();
    return true;
}

cMDIGrower::cMDIGrower(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH) 
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_MDIGROWER);
    saved=false;
    name="";
    
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL,0,0,0,0);
        new FXLabel(_vframe0,oLanguage->getText("str_grwmdi_id"));
        tfId=new FXTextField(_vframe0,35,this,ID_GRWID);
            tfId->setText(prName);
            tfId->setSelection(0,tfId->getText().length());
            tfId->setFocus();

        new FXLabel(_vframe0,oLanguage->getText("str_grwmdi_ctp"));
        tfLab=new FXTextField(_vframe0,35,this,ID_GRWLAB);
        new FXLabel(_vframe0,oLanguage->getText("str_grwmdi_add"));
        FXVerticalFrame *_vframe7=new FXVerticalFrame(_vframe0,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
        tfAdd=new FXText(_vframe7,this,ID_GRWADD,TEXT_WORDWRAP|TEXT_NO_TABS|LAYOUT_FILL);
        new FXLabel(_vframe0,oLanguage->getText("str_grwmdi_city"));
        tfCity=new FXTextField(_vframe0,35,this,ID_GRWCITY);
        new FXLabel(_vframe0,oLanguage->getText("str_grwmdi_state"));
        tfState=new FXTextField(_vframe0,35,this,ID_GRWSTATE);
        new FXLabel(_vframe0,oLanguage->getText("str_grwmdi_zip"));
        tfZip=new FXTextField(_vframe0,35,this,ID_GRWZIP);
        new FXLabel(_vframe0,oLanguage->getText("str_grwmdi_phone"));
        tfPhone=new FXTextField(_vframe0,35,this,ID_GRWPHONE);
        new FXLabel(_vframe0,oLanguage->getText("str_grwmdi_fax"));
        tfFax=new FXTextField(_vframe0,35,this,ID_GRWFAX);
        new FXLabel(_vframe0,oLanguage->getText("str_grwmdi_email"));
        tfEmail=new FXTextField(_vframe0,35,this,ID_GRWEMAIL);
            
        new FXLabel(_vframe0,oLanguage->getText("str_grwmdi_comments"));
        FXVerticalFrame *_vframe8=new FXVerticalFrame(_vframe0,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
        comments=new FXText(_vframe8,this,ID_GRWCOMMENTS,TEXT_WORDWRAP|TEXT_NO_TABS|LAYOUT_FILL);
        FXHorizontalFrame *_hframe8=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,10,0);
            //new FXButton(_hframe8,oLanguage->getText("str_grwmdi_print"),new FXGIFIcon(getApp(),data_settings_printer),this,CMD_PRINT,FRAME_RAISED|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH,0,0,100,0);
            new FXButton(_hframe8,oLanguage->getText("str_but_close"),NULL,this,CMD_CLOSE,FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_FIX_WIDTH|LAYOUT_RIGHT,0,0,100,0);
            new FXButton(_hframe8,oLanguage->getText("str_grwmdi_save"),NULL,this,CMD_SAVE,FRAME_RAISED|BUTTON_DEFAULT|BUTTON_INITIAL|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_RIGHT|LAYOUT_FIX_WIDTH,0,0,100,0);
}

cMDIGrower::~cMDIGrower()
{
}

void cMDIGrower::create()
{
    cMDIChild::create();
    show();
    updateData();
}

FXbool cMDIGrower::canClose(void)
{
    if(!saved)
    {
        setFocus();
        int res=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),oLanguage->getText("str_qsave").text());
        switch(res)
        {
            case MBOX_CLICKED_YES:
                if(!saveData())
                    return false;
                break;
            case MBOX_CLICKED_CANCEL:
                return false;
            default:
                break;
        }
    }
    return true;
}

FXbool cMDIGrower::loadGrower(const FXString &prId)
{
    if(!cGrowerManager::growerExists(prId))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_opengrw").text());
        return false;
    }
    if(!oWinMain->isWindow(prId))
        return oWinMain->raiseWindow(prId);
    
    cDataResult *res=cGrowerManager::getGrower(prId);
    if(res==NULL)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_opengrw").text());
        return false;
    }
    
    name=res->getCellString(0,0);
    tfId->setText(name);
    tfLab->setText(res->getCellString(0,1));
    tfAdd->setText(res->getCellString(0,3));
    tfCity->setText(res->getCellString(0,4));
    tfState->setText(res->getCellString(0,5));
    tfZip->setText(res->getCellString(0,6));
    tfPhone->setText(res->getCellString(0,7));
    tfFax->setText(res->getCellString(0,8));
    tfEmail->setText(res->getCellString(0,9));
    comments->setText(res->getCellString(0,10));
    
    setTitle(name);
    updateData();
    saved=true;
    return true;
}

long cMDIGrower::onCmdTfId(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    if(!tfId->getText().empty())
    {
        if(!cGrowerManager::growerExists(tfId->getText()))
            return 1;
        else
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
    }
    tfId->setText(getTitle());
    return 1;
}

long cMDIGrower::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(canClose())
    {
        close();
        return 1; 
    }
    return 0;
}

long cMDIGrower::onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    return 1;
}

long cMDIGrower::onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData)
{
    saveData();
    if(saved)
        onCmdClose(NULL,0,NULL);
    return 1;
}

long cMDIGrower::onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData)
{
    
    
    // TODO
    
    
    return 1;
}

