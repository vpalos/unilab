#include "engine.h"
#include "graphics.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cLanguage.h"
#include "cDateChooser.h"
#include "cCaseManager.h"
#include "cDLGCasePopEdit.h"
#include "cMDICaseManager.h"
#include "cMDIReadings.h"
#include "cPopulationManager.h"
#include "cVeterinarianManager.h"
#include "cAssayManager.h"
#include "cOwnerManager.h"
#include "cBreedManager.h"
#include "cGrowerManager.h"
#include "cTemplateManager.h"
#include "cVaccineManager.h"
#include "cSpeciesManager.h"
#include "cWinMain.h"

FXDEFMAP(cDLGCasePopEdit) mapDLGCasePopEdit[]=
{
    FXMAPFUNC(SEL_COMMAND,cDLGCasePopEdit::ID_UPDATE,cDLGCasePopEdit::onCmdUpdate),
    FXMAPFUNC(SEL_COMMAND,cDLGCasePopEdit::CMD_ADDP,cDLGCasePopEdit::onCmdAddPred),
    FXMAPFUNC(SEL_COMMAND,cDLGCasePopEdit::CMD_DELP,cDLGCasePopEdit::onCmdDelPred),
    FXMAPFUNC(SEL_COMMAND,cDLGCasePopEdit::CMD_DC0,cDLGCasePopEdit::onCmdDateChooser0),
    FXMAPFUNC(SEL_COMMAND,cDLGCasePopEdit::CMD_DC1,cDLGCasePopEdit::onCmdDateChooser1),
    FXMAPFUNC(SEL_UPDATE,cDLGCasePopEdit::ID_DLGCASEPOPEDIT,cDLGCasePopEdit::onUpdWindow),
    FXMAPFUNC(SEL_COMMAND,cDLGCasePopEdit::CMD_BUT_APPLY,cDLGCasePopEdit::onCmdApply),
};

FXIMPLEMENT(cDLGCasePopEdit,FXDialogBox,mapDLGCasePopEdit,ARRAYNUMBER(mapDLGCasePopEdit))

FXString cDLGCasePopEdit::selectPop(void)
{
    FXString choices="";
    cDataResult *res=oDataLink->execute("SELECT id FROM t_gnpopulations WHERE id!='"+tfPopId->getText().substitute("'","")+"';");
    int cnt=res->getRowCount();
    if(!cnt)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_dlg_pop_nopop").text());
        return "";
    }
    
    for(int i=0;i<cnt;i++)
    {
        if(res->getCellString(i,0)!=pop)
            choices=choices+res->getCellString(i,0)+"\n";
    }
    
    if((cnt=FXChoiceBox::ask(oWinMain,0,oLanguage->getText("str_question").text(),oLanguage->getText("str_dlg_pop_choosepop").text(),new FXGIFIcon(oApplicationManager,data_inputs),choices))==-1)
        return "";
    
    return res->getCellString(cnt,0);
}

cDLGCasePopEdit::cDLGCasePopEdit(FXWindow *prOwner,FXString prCaseId,FXString prAssayId) :
    FXDialogBox(prOwner,"",DECOR_TITLE|DECOR_BORDER)
{
    setTarget(this);
    setSelector(ID_DLGCASEPOPEDIT);
    cse=prCaseId;
    asy=prAssayId;
    
    setTitle(oLanguage->getText("str_dlg_casepop_title")+FXStringFormat(" %s - %s",prCaseId.text(),prAssayId.text()));
    
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(this,LAYOUT_FILL,0,0,0,0,0,0,0,0);
    tbMain=new FXTabBook(_hframe0,this,ID_TBMAIN,PACK_UNIFORM_WIDTH|PACK_UNIFORM_HEIGHT|LAYOUT_FILL,0,0,0,0,0,0,0,0);
    tiCase=new FXTabItem(tbMain,oLanguage->getText("str_dlg_casepop_casetitle"));
        vfCase=new FXVerticalFrame(tbMain,FRAME_RAISED|FRAME_THICK|LAYOUT_FILL);
            new FXLabel(vfCase,oLanguage->getText("str_dlg_case_id"));
            tfCaseId=new FXTextField(vfCase,50,this,ID_UPDATE,TEXTFIELD_NORMAL|LAYOUT_FILL_X);
            new FXLabel(vfCase,oLanguage->getText("str_dlg_case_vet"));
            lxCaseVet=new FXListBox(vfCase,NULL,0,FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X);

            FXHorizontalFrame *_hframeu=new FXHorizontalFrame(vfCase,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                FXVerticalFrame *_vframeu1=new FXVerticalFrame(_hframeu,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                    new FXLabel(_vframeu1,oLanguage->getText("str_dlg_case_bldate"));
                    FXHorizontalFrame *_hframe0b=new FXHorizontalFrame(_vframeu1,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
                    new FXButton(_hframe0b,(char*)NULL,new FXGIFIcon(getApp(),data_calendar),this,CMD_DC0,FRAME_RAISED|LAYOUT_LEFT);
                    tfCaseBlDate=new FXTextField(_hframe0b,47,this,ID_UPDATE,TEXTFIELD_NORMAL|LAYOUT_FILL_X);
                FXVerticalFrame *_vframeu2=new FXVerticalFrame(_hframeu,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                    new FXLabel(_vframeu2,oLanguage->getText("str_dlg_case_age"));
                    tfCaseAge=new FXTextField(_vframeu2,7,this,ID_UPDATE,TEXTFIELD_NORMAL);

            new FXLabel(vfCase,oLanguage->getText("str_dlg_case_sptype"));
            cxCaseSpType=new FXComboBox(vfCase,48,NULL,0,COMBOBOX_NORMAL|FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X);
            new FXLabel(vfCase,oLanguage->getText("str_dlg_case_reason"));
            cxCaseReason=new FXComboBox(vfCase,48,NULL,0,COMBOBOX_NORMAL|FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X);
            FXVerticalFrame *_vframe1=new FXVerticalFrame(vfCase,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK|LAYOUT_BOTTOM,0,0,0,0,0,0,0,0);
            tfCaseComments=new FXText(_vframe1,NULL,0,TEXT_NO_TABS|TEXT_WORDWRAP|LAYOUT_FILL);
            new FXLabel(vfCase,oLanguage->getText("str_dlg_case_comments"),NULL,LAYOUT_BOTTOM);
    
    tiPop=new FXTabItem(tbMain,oLanguage->getText("str_dlg_casepop_poptitle"));
        vfPop=new FXVerticalFrame(tbMain,FRAME_RAISED|FRAME_THICK|LAYOUT_FILL);
            new FXLabel(vfPop,oLanguage->getText("str_dlg_pop_id"));
            tfPopId=new FXTextField(vfPop,50,this,ID_UPDATE,TEXTFIELD_NORMAL|LAYOUT_FILL_X);
            
            new FXLabel(vfPop,oLanguage->getText("str_dlg_pop_brdate"));
            FXHorizontalFrame *_hframe0a=new FXHorizontalFrame(vfPop,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
            new FXButton(_hframe0a,(char*)NULL,new FXGIFIcon(getApp(),data_calendar),this,CMD_DC1,FRAME_RAISED|LAYOUT_LEFT);
            tfPopBrDate=new FXTextField(_hframe0a,47,this,ID_UPDATE,TEXTFIELD_NORMAL|LAYOUT_FILL_X);
            
            new FXLabel(vfPop,oLanguage->getText("str_dlg_pop_vaccine"));
            lxPopVaccine=new FXListBox(vfPop,NULL,0,FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X);

            FXMatrix *_mat2=new FXMatrix(vfPop,4,MATRIX_BY_ROWS|LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                new FXLabel(_mat2,oLanguage->getText("str_dlg_pop_owner"));
                lxPopOwner=new FXListBox(_mat2,NULL,0,COMBOBOX_NORMAL|FRAME_SUNKEN|FRAME_THICK|LAYOUT_FIX_WIDTH,0,0,175);
                new FXLabel(_mat2,oLanguage->getText("str_dlg_pop_grower"));
                lxPopGrower=new FXListBox(_mat2,NULL,0,COMBOBOX_NORMAL|FRAME_SUNKEN|FRAME_THICK|LAYOUT_FIX_WIDTH,0,0,175);
                new FXLabel(_mat2,oLanguage->getText("str_dlg_pop_breed1"));
                lxPopBreed1=new FXListBox(_mat2,NULL,0,COMBOBOX_NORMAL|FRAME_SUNKEN|FRAME_THICK|LAYOUT_FIX_WIDTH,0,0,180);
                new FXLabel(_mat2,oLanguage->getText("str_dlg_pop_breed2"));
                lxPopBreed2=new FXListBox(_mat2,NULL,0,COMBOBOX_NORMAL|FRAME_SUNKEN|FRAME_THICK|LAYOUT_FIX_WIDTH,0,0,180);
            
            _hframe1=new FXHorizontalFrame(vfPop,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y,0,0,0,0,0,0,0,0);
                    new FXLabel(_vframe2,oLanguage->getText("str_dlg_pop_unit"));
                    tfPopUnit=new FXTextField(_vframe2,24,NULL,0,TEXTFIELD_NORMAL);
                    new FXLabel(_vframe2,oLanguage->getText("str_dlg_pop_location"));
                    tfPopLoc=new FXTextField(_vframe2,24,NULL,0,TEXTFIELD_NORMAL);
                    new FXLabel(_vframe2,oLanguage->getText("str_dlg_pop_sublocation"));
                    tfPopSubLoc=new FXTextField(_vframe2,24,NULL,0,TEXTFIELD_NORMAL);
                    
                FXHorizontalFrame *_hframe7=new FXHorizontalFrame(_hframe1,LAYOUT_FILL,0,0,0,0,0,0,0,0);
                FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe7,LAYOUT_FILL,0,0,0,0,0,0,0,0);
                    new FXLabel(_vframe3,oLanguage->getText("str_dlg_pop_predecessors"));
                    FXVerticalFrame *_vframe5=new FXVerticalFrame(_vframe3,FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_Y|LAYOUT_FIX_WIDTH,0,0,180,0,0,0,0,0);
                    tbPred=new FXIconList(_vframe5,NULL,0,ICONLIST_DETAILED|ICONLIST_EXTENDEDSELECT|LAYOUT_FILL);
                    tbPred->appendHeader(oLanguage->getText("str_dlg_pop_preds"),NULL,170);
                    tbPred->setScrollStyle(HSCROLLING_OFF|HSCROLLER_NEVER|VSCROLLING_ON|VSCROLLER_ALWAYS);
                FXVerticalFrame *_vframe6=new FXVerticalFrame(_hframe7,LAYOUT_RIGHT|LAYOUT_FILL_Y,0,0,0,0,0,0,0,0);
                    new FXButton(_vframe6,oLanguage->getText("str_but_remove"),NULL,this,CMD_DELP,FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FILL_X|LAYOUT_BOTTOM);
                    new FXButton(_vframe6,oLanguage->getText("str_but_add"),NULL,this,CMD_ADDP,FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FILL_X|LAYOUT_BOTTOM);
    
            new FXLabel(vfPop,oLanguage->getText("str_dlg_pop_comments"));
            FXVerticalFrame *_vframe4=new FXVerticalFrame(vfPop,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
                tfPopComments=new FXText(_vframe4,NULL,0,TEXT_NO_TABS|TEXT_WORDWRAP|LAYOUT_FILL);
            
    FXVerticalFrame *_vframe0=new FXVerticalFrame(_hframe0,LAYOUT_FILL_Y|LAYOUT_RIGHT,0,0,0,0,5,0,0,0);
        new FXButton(_vframe0,oLanguage->getText("str_but_cancel"),NULL,this,ID_CANCEL,FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,80,0);
        new FXHorizontalSeparator(_vframe0,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT|LAYOUT_BOTTOM,0,0,0,10);
        new FXButton(_vframe0,oLanguage->getText("str_but_ok"),NULL,this,CMD_BUT_APPLY,BUTTON_INITIAL|BUTTON_DEFAULT|FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,80,0);
}

cDLGCasePopEdit::~cDLGCasePopEdit()
{
}

FXuint cDLGCasePopEdit::execute(FXuint prPlacement)
{
    create();
    cDataResult *csr=cCaseManager::getCase(cse,asy);
    if(!csr || csr->getRowCount()<1)
        return 0;
    cDataResult *ppr=cPopulationManager::getPopulation(csr->getCellString(0,13));
    if(!ppr || ppr->getRowCount()!=1)
        return 0;

    pop=ppr->getCellString(0,0);
    dt1=csr->getCellString(0,10);
    dt2=ppr->getCellString(0,2);
    
    FXString s;
    cxCaseReason->appendItem("---");
    FXString defs=oLanguage->getText("str_reason_defs");
    while(!defs.empty())
    {
        cxCaseReason->appendItem(defs.before('\t'));
        defs=defs.after('\t');
    }
    
    cxCaseSpType->appendItem("---");
    defs=oLanguage->getText("str_sptype_defs");
    while(!defs.empty())
    {
        cxCaseSpType->appendItem(defs.before('\t'));
        defs=defs.after('\t');
    }
    
    tfCaseId->setText(csr->getCellString(0,0));
    tfCaseBlDate->setText(csr->getCellString(0,10));
    tfCaseComments->setText(csr->getCellString(0,24));
    tfCaseAge->setText(csr->getCellString(0,14));
    cxCaseReason->setText(csr->getCellString(0,4));
    cxCaseSpType->setText(csr->getCellString(0,9));
    bool is;
    
    {
    is=false;
    s=csr->getCellString(0,5);
    lxCaseVet->appendItem("---");
    sVeterinarianObject *res=cVeterinarianManager::listVeterinarians();
    if(res!=NULL)
    {
        for(int i=0;i<cVeterinarianManager::getVeterinarianCount();i++)
        {
            lxCaseVet->appendItem(*(res[i].id));
            if(!is && (*res[i].id)==s)
            {
                is=true;
                lxCaseVet->setCurrentItem(i+1);
            }
        }
        lxCaseVet->sortItems();
        free(res);
    }
    if(!is)
        lxCaseVet->setCurrentItem(0);
    }
    
    tfPopId->setText(ppr->getCellString(0,0));
    tfPopBrDate->setText(ppr->getCellString(0,2));
    tfPopUnit->setText(ppr->getCellString(0,8));
    tfPopLoc->setText(ppr->getCellString(0,9));
    tfPopSubLoc->setText(ppr->getCellString(0,10));
    tfPopComments->setText(ppr->getCellString(0,11));
    
    {
    is=false;
    s=ppr->getCellString(0,5);
    lxPopOwner->appendItem("---");
    sOwnerObject *res=cOwnerManager::listOwners();
    if(res!=NULL)
    {
        for(int i=0;i<cOwnerManager::getOwnerCount();i++)
        {
            lxPopOwner->appendItem(*(res[i].id));
            if(!is && (*res[i].id)==s)
            {
                is=true;
                lxPopOwner->setCurrentItem(i+1);
            }
        }
        lxPopOwner->sortItems();
        free(res);
    }
    if(!is)
        lxPopOwner->setCurrentItem(0);
    }
    
    {
    is=false;
    s=ppr->getCellString(0,6);
    lxPopGrower->appendItem("---");
    sGrowerObject *res=cGrowerManager::listGrowers();
    if(res!=NULL)
    {
        for(int i=0;i<cGrowerManager::getGrowerCount();i++)
        {
            lxPopGrower->appendItem(*(res[i].id));
            if(!is && (*res[i].id)==s)
            {
                is=true;
                lxPopGrower->setCurrentItem(i+1);
            }
        }
        lxPopGrower->sortItems();
        free(res);
    }
    if(!is)
        lxPopGrower->setCurrentItem(0);
    }
    
    {
    is=false;
    s=ppr->getCellString(0,3);
    lxPopBreed1->appendItem("---");
    sBreedObject *res=cBreedManager::listBreeds();
    if(res!=NULL)
    {
        for(int i=0;i<cBreedManager::getBreedCount();i++)
        {
            lxPopBreed1->appendItem(*(res[i].id));
            if(!is && (*res[i].id)==s)
            {
                is=true;
                lxPopBreed1->setCurrentItem(i+1);
            }
        }
        lxPopBreed1->sortItems();
        free(res);
    }
    if(!is)
        lxPopBreed1->setCurrentItem(0);
    }
    
    {
    is=false;
    s=ppr->getCellString(0,4);
    lxPopBreed2->appendItem("---");
    sBreedObject *res=cBreedManager::listBreeds();
    if(res!=NULL)
    {
        for(int i=0;i<cBreedManager::getBreedCount();i++)
        {
            lxPopBreed2->appendItem(*(res[i].id));
            if(!is && (*res[i].id)==s)
            {
                is=true;
                lxPopBreed2->setCurrentItem(i+1);
            }
        }
        lxPopBreed2->sortItems();
        free(res);
    }
    if(!is)
        lxPopBreed2->setCurrentItem(0);
    }
    
    {
    is=false;
    s=ppr->getCellString(0,7);
    lxPopVaccine->appendItem("---");
    sVaccineObject *res=cVaccineManager::listVaccines();
    if(res!=NULL)
    {
        for(int i=0;i<cVaccineManager::getVaccineCount();i++)
        {
            lxPopVaccine->appendItem(*(res[i].id));
            if(!is && (*res[i].id)==s)
            {
                is=true;
                lxPopVaccine->setCurrentItem(i+1);
            }
        }
        lxPopVaccine->sortItems();
        free(res);
    }
    if(!is)
        lxPopVaccine->setCurrentItem(0);
    }
    
    defs=ppr->getCellString(0,1);
    while(!defs.empty())
    {
        tbPred->appendItem(defs.before('\t'));
        defs=defs.after('\t');
    }
    
    lxCaseVet->setNumVisible(lxCaseVet->getNumItems()>10?10:lxCaseVet->getNumItems());
    cxCaseSpType->setNumVisible(cxCaseSpType->getNumItems()>10?10:cxCaseSpType->getNumItems());
    cxCaseReason->setNumVisible(cxCaseReason->getNumItems()>10?10:cxCaseReason->getNumItems());
    lxPopOwner->setNumVisible(lxPopOwner->getNumItems()>10?10:lxPopOwner->getNumItems());
    lxPopGrower->setNumVisible(lxPopGrower->getNumItems()>10?10:lxPopGrower->getNumItems());
    lxPopVaccine->setNumVisible(lxPopVaccine->getNumItems()>10?10:lxPopVaccine->getNumItems());
    lxPopBreed1->setNumVisible(lxPopBreed1->getNumItems()>10?10:lxPopBreed1->getNumItems());
    lxPopBreed2->setNumVisible(lxPopBreed2->getNumItems()>10?10:lxPopBreed2->getNumItems());

    show(prPlacement);
    return getApp()->runModalFor(this);
}

FXString cDLGCasePopEdit::getCase(void)
{
    return tfCaseId->getText();
}

FXString cDLGCasePopEdit::getPopulation(void)
{
    return tfPopId->getText();
}

FXString cDLGCasePopEdit::getAge(void)
{
    return tfCaseAge->getText();
}

FXString cDLGCasePopEdit::getBlDate(void)
{
    return tfCaseBlDate->getText();
}

long cDLGCasePopEdit::onCmdDateChooser0(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXString res=cDateChooser::selectDate(this);
    if(!res.empty())
        tfCaseBlDate->setText(res);
    return 1;
}

long cDLGCasePopEdit::onCmdDateChooser1(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXString res=cDateChooser::selectDate(this);
    if(!res.empty())
        tfPopBrDate->setText(res);
    return 1;
}

long cDLGCasePopEdit::onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXString preds="";
    for(int i=0;i<tbPred->getNumItems();i++)
        preds=preds+(i>0?"\t":"")+tbPred->getItemText(i);
    cPopulationManager::setPopulation(pop,preds,tfPopId->getText(),tfPopBrDate->getText(),
                                      lxPopBreed1->getItemText(lxPopBreed1->getCurrentItem()),
                                      lxPopBreed2->getItemText(lxPopBreed2->getCurrentItem()),
                                      lxPopOwner->getItemText(lxPopOwner->getCurrentItem()),
                                      lxPopGrower->getItemText(lxPopGrower->getCurrentItem()),
                                      lxPopVaccine->getItemText(lxPopVaccine->getCurrentItem()),
                                      tfPopUnit->getText(),tfPopLoc->getText(),tfPopSubLoc->getText(),tfPopComments->getText());

    cCaseManager::setCase(cse,asy,tfCaseId->getText(),cxCaseReason->getText(),
                          lxCaseVet->getItemText(lxCaseVet->getCurrentItem()),cxCaseSpType->getText(),
                          tfCaseBlDate->getText(),tfPopId->getText(),tfCaseAge->getText(),tfCaseComments->getText());
    sCDLGO *dt=new sCDLGO;
    dt->id=new FXString(cse);
    dt->id2=new FXString(tfCaseId->getText());
    dt->asy=new FXString(asy);
    dt->age=new FXString(tfCaseAge->getText());
    dt->pop=new FXString(pop);
    dt->pop2=new FXString(tfPopId->getText());
    dt->blDate=new FXString(tfCaseBlDate->getText());
    dt->br1=new FXString(lxPopBreed1->getItemText(lxPopBreed1->getCurrentItem()));
    dt->br2=new FXString(lxPopBreed2->getItemText(lxPopBreed2->getCurrentItem()));
    dt->res=cCaseManager::getCase(tfCaseId->getText(),asy);
    for(int i=oWinMain->getReadings()->first();i<=oWinMain->getReadings()->last();i=oWinMain->getReadings()->next(i))
    {
        cMDIReadings *w=(cMDIReadings*)oWinMain->getReadings()->data(i);
        if(w)
        {
            w->onRefresh(NULL,0,dt);
        }
    }
    delete dt->id;
    delete dt->id2;
    delete dt->asy;
    delete dt->age;
    delete dt->pop;
    delete dt->blDate;
    delete dt->br1;
    delete dt->br2;
    delete dt->res;
    delete dt;
    //if(cse!=tfCaseId->getText())
    {
        cMDICaseManager::cmdRefresh();
        cMDICaseManager::cmdResearch();
    }
    handle(NULL,FXSEL(SEL_COMMAND,ID_ACCEPT),NULL);
    return 1;
}

long cDLGCasePopEdit::onUpdWindow(FXObject *prSender,FXSelector prSelector,void *prData)
{
    return 1;
}

long cDLGCasePopEdit::onCmdAddPred(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(tbPred->getNumItems()>10)
        return 1;
    FXString pop="";
    while(pop.empty())
    {
        pop=selectPop();
        if(pop.empty())
            return 1;
        bool is=false;
        for(int i=0;i<tbPred->getNumItems();i++)
            if(pop==tbPred->getItemText(i))
                {is=true;break;}
        if(is)
        {
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_dlg_pop_dupl").text());
            pop="";
        }
    }
    tbPred->appendItem(pop);
    return 1;
}

long cDLGCasePopEdit::onCmdDelPred(FXObject *prSender,FXSelector prSelector,void *prData)
{
    for(int i=tbPred->getNumItems()-1;i>=0;i--)
        if(tbPred->isItemSelected(i))
            tbPred->removeItem(i);
    return 1;
}

long cDLGCasePopEdit::onCmdUpdate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(cse!=tfCaseId->getText() && cCaseManager::caseExists(tfCaseId->getText(),asy))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_cseman_dupl").text());
        tfCaseId->setText(cse);
    }
    
    if(pop!=tfPopId->getText() && cPopulationManager::populationExists(tfPopId->getText()))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_dlg_pop_dupl").text());
        tfPopId->setText(pop);
    }
    
    unsigned int t,v1,v2,cat;
    FXString s1,s2;
    s1=tfCaseAge->getText().before('-');
    s2=tfCaseAge->getText().after('-');
    if(s2.empty())
    {
        v1=0;
        v2=FXIntVal(s1);
    }
    else
    {
        v1=FXIntVal(s1);
        v2=FXIntVal(s2);
    }
    cat=cSpeciesManager::getAgeFormat(oDataLink->getSpecies());
    t=v1*cat+v2;
    v1=t/cat;
    v2=t%cat;
    if((unsigned int)v1>999)v1=999;
    tfCaseAge->setText(FXStringFormat("%d-%d",v1,v2));
    
    if(tfCaseBlDate->getText().empty())
        tfCaseBlDate->setText(dt1);
    else
    {
        struct tm prstime;
        memset(&prstime,0,sizeof(prstime));
        sscanf(tfCaseBlDate->getText().text(),"%d-%d-%d",&prstime.tm_year,&prstime.tm_mon,&prstime.tm_mday);       
        prstime.tm_year-=prstime.tm_year<1900?0:1900;
        prstime.tm_mon-=1;
        tfCaseBlDate->setText(FXStringFormat("%04d-%02d-%02d",prstime.tm_year+1900,prstime.tm_mon+1,prstime.tm_mday));
    }
    
    if(tfPopBrDate->getText().empty())
        tfPopBrDate->setText(dt2);
    else
    {
        struct tm prstime;
        memset(&prstime,0,sizeof(prstime));
        if(sscanf(tfPopBrDate->getText().text(),"%d-%d-%d",&prstime.tm_year,&prstime.tm_mon,&prstime.tm_mday)!=3)
        {
            prstime.tm_year-=prstime.tm_year<1900?0:1900;
            prstime.tm_mon-=1;
            tfPopBrDate->setText(FXStringFormat("%04d-%02d-%02d",prstime.tm_year+1900,prstime.tm_mon+1,prstime.tm_mday));
        }
        else
            tfPopBrDate->setText(dt2);
    }
    
    return 1;
}

