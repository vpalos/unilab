#ifndef CDLGCHOOSEAT_H
#define CDLGCHOOSEAT_H

#include <fx.h>

#include "cSortList.h"

#define CHOOSEAT_ASSAY 1
#define CHOOSEAT_TEMPLATE 2
#define CHOOSEAT_BOTH 3

class cDLGChooseAT : public FXDialogBox
{
    FXDECLARE(cDLGChooseAT);
    private:
    
    protected:
        FXString asy;
        FXString tpl;
        FXString lot;
        FXString expdate;
        FXint flags,state;
        FXbool p;
        FXLabel *lbStatus;
        FXGroupBox *gbMain;
        FXSwitcher *swMain;
        cSortList *asyList;
        cSortList *tplList;
        FXButton *btProceed;
        FXTextField *tfLot,*tfDate;
        FXVerticalFrame *s1Frame;
        FXHorizontalFrame *s2Frame;

        cDLGChooseAT(){}
        
    public:
        cDLGChooseAT(FXWindow *prOwner,FXint prFlags=CHOOSEAT_BOTH);
        virtual ~cDLGChooseAT();
        
        enum
        {
            ID_DLGCHOOSEAT=FXDialogBox::ID_LAST,
            ID_ASSAYLIST,
            ID_TEMPLATELIST,
            ID_DATETF,
            ID_BUTDATETF,
            CMD_BUT_PROCEED
        };
        
        void setChoices(FXint prFlags=CHOOSEAT_BOTH);

        void setAssay(const FXString &prAssay);
        void setTemplate(const FXString &prTemplate);
        FXString getLot(void);
        FXString getExpDate(void);
        FXString getAssay(void);
        FXString getTemplate(void);

        long onCmdDateTf(FXObject *prSender,FXSelector prSelector,void *prData);
        long onDbcList(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdWindow(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdProceed(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
