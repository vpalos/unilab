#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cVeterinarianManager.h"
#include "cWinMain.h"
#include "cMDIDatabaseManager.h"
#include "cMDIVeterinarianManager.h"

cMDIVeterinarianManager *oMDIVeterinarianManager=NULL;

FXDEFMAP(cMDIVeterinarianManager) mapMDIVeterinarianManager[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIVeterinarianManager::ID_VETERINARIANMANAGER,cMDIVeterinarianManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIVeterinarianManager::CMD_BUT_CLOSE,cMDIVeterinarianManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIVeterinarianManager::CMD_BUT_OPEN,cMDIVeterinarianManager::onCmdOpen),
    FXMAPFUNC(SEL_COMMAND,cMDIVeterinarianManager::CMD_BUT_NEW,cMDIVeterinarianManager::onCmdNew),
    FXMAPFUNC(SEL_COMMAND,cMDIVeterinarianManager::CMD_BUT_DUPLICATE,cMDIVeterinarianManager::onCmdDuplicate),
    FXMAPFUNC(SEL_DOUBLECLICKED,cMDIVeterinarianManager::ID_VETERINARIANLIST,cMDIVeterinarianManager::onCmdOpen),
    FXMAPFUNC(SEL_COMMAND,cMDIVeterinarianManager::CMD_BUT_REMOVE,cMDIVeterinarianManager::onCmdRemove),
    
};

FXIMPLEMENT(cMDIVeterinarianManager,cMDIChild,mapMDIVeterinarianManager,ARRAYNUMBER(mapMDIVeterinarianManager))

cMDIVeterinarianManager::cMDIVeterinarianManager()
{
}

cMDIVeterinarianManager::cMDIVeterinarianManager(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH) 
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_VETERINARIANMANAGER);
    client=prP;
    popup=prPup;
    
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(this,LAYOUT_FILL);
    FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe0,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
    new FXLabel(_vframe2,oLanguage->getText("str_vetman_vetlist_title"));
    FXHorizontalFrame *_hframe1=new FXHorizontalFrame(_vframe2,LAYOUT_FILL,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe4=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|LAYOUT_RIGHT,0,0,0,0,0,0,0,0,0,0);
    
    vetList=new cSortList(_vframe3,this,ID_VETERINARIANLIST,ICONLIST_SINGLESELECT|ICONLIST_DETAILED|LAYOUT_FILL);
    vetList->appendHeader(oLanguage->getText("str_vetman_vetnameh"),NULL,120);
    vetList->appendHeader(oLanguage->getText("str_vetman_vettitleh"),NULL,264);
    oMDIVeterinarianManager=this;
    this->update();
    if(vetList->getNumItems()>0)
    {
        vetList->selectItem(0);
        vetList->setCurrentItem(0);
    }
    vetList->setFocus();

    new FXButton(_vframe4,oLanguage->getText("str_vetman_new"),NULL,this,CMD_BUT_NEW,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_FIX_WIDTH,0,0,88,0);
    new FXHorizontalSeparator(_vframe4,LAYOUT_TOP|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_vetman_open"),NULL,this,CMD_BUT_OPEN,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X);
    new FXButton(_vframe4,oLanguage->getText("str_vetman_duplicate"),NULL,this,CMD_BUT_DUPLICATE,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X);
    new FXButton(_vframe4,oLanguage->getText("str_but_close"),NULL,this,CMD_BUT_CLOSE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
    new FXHorizontalSeparator(_vframe4,LAYOUT_BOTTOM|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_vetman_remove"),NULL,this,CMD_BUT_REMOVE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
}

cMDIVeterinarianManager::~cMDIVeterinarianManager()
{
    cMDIVeterinarianManager::unload();
}

void cMDIVeterinarianManager::create()
{
    cMDIChild::create();
    show();
}

void cMDIVeterinarianManager::load(FXMDIClient *prP,FXPopup *prMenu)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(prP,prMenu);
        return;
    }
    
    if(oMDIVeterinarianManager!=NULL)
    {
        oMDIVeterinarianManager->setFocus();
        prP->setActiveChild(oMDIVeterinarianManager);
        if(oMDIVeterinarianManager->isMinimized())
            oMDIVeterinarianManager->restore();
        return;
    }
    new cMDIVeterinarianManager(prP,oLanguage->getText("str_vetman_title"),new FXGIFIcon(oApplicationManager,data_inputs),prMenu,MDI_NORMAL|MDI_TRACKING,10,10,515,200);
    oMDIVeterinarianManager->create();
    oMDIVeterinarianManager->setFocus();

}

FXbool cMDIVeterinarianManager::isLoaded(void)
{
    return (oMDIVeterinarianManager!=NULL);
}

void cMDIVeterinarianManager::unload(void)
{
    oMDIVeterinarianManager=NULL;
}

void cMDIVeterinarianManager::update(void)
{
    if(!isLoaded())
        return;
    sVeterinarianObject *res=cVeterinarianManager::listVeterinarians();
    if(res==NULL)
        return;
    vetList->clearItems();
    for(int i=0;i<cVeterinarianManager::getVeterinarianCount();i++)
        vetList->appendItem(*(res[i].id)+"\t"+(res[i].solid>0?"* ":"")+*(res[i].title));
    vetList->sortItems();
    free(res);
}

void cMDIVeterinarianManager::cmdRefresh(void)
{
    if(!isLoaded())
        return;
    oMDIVeterinarianManager->update();
}

long cMDIVeterinarianManager::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDIVeterinarianManager::unload();
    close();
    return 1;
}

long cMDIVeterinarianManager::onCmdNew(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cVeterinarianManager::newVeterinarian(client,popup);
    return 1;
}

long cMDIVeterinarianManager::onCmdOpen(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int db=-1,i;
    for(i=0;i<vetList->getNumItems();i++)
        if(vetList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        db=vetList->getCurrentItem();
    if(db==-1)
        return 1;
    if(cVeterinarianManager::veterinarianSolid(vetList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasevetsolid").text());
        return 1;
    }
    FXString st=vetList->getItemText(db).before('\t');
    if(oWinMain->isWindow(st))
        oWinMain->raiseWindow(st);
    else
        cVeterinarianManager::editVeterinarian(client,popup,st);
    return 1;
}

long cMDIVeterinarianManager::onCmdDuplicate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int db=-1,i,j;
    for(i=0;i<vetList->getNumItems();i++)
        if(vetList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        return 1;
    FXString res2=cVeterinarianManager::duplicateVeterinarian(vetList->getItemText(db).before('\t').text());
    this->update();
    if(res2.empty())
        return 1;
    for(j=0;j<vetList->getNumItems();j++)
        if(res2==vetList->getItemText(j).before('\t'))
        {
            vetList->selectItem(j);
            break;
        }
    return 1;
}

long cMDIVeterinarianManager::onCmdRemove(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i,db=-1;
    for(i=0;i<vetList->getNumItems();i++)
        if(vetList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        return 1;
    if(cVeterinarianManager::veterinarianSolid(vetList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasevetsolid").text());
        return 1;
    }
    if(cVeterinarianManager::veterinarianNeeded(vetList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasedataneeded").text());
        return 1;
    }
    if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_erasevet").text()))
        return 1;
    cMDIChild *win=oWinMain->getWindow(vetList->getItemText(db).before('\t'));
    if(win!=NULL)
        win->close();
    cVeterinarianManager::removeVeterinarian(vetList->getItemText(db).before('\t'));
    this->update();
    if(db>=vetList->getNumItems())
        db=vetList->getNumItems()-1;
    if(db>=0)
        vetList->selectItem(db);
    return 1;
}


