#include "engine.h"
#include "graphics.h"
#include "cLanguage.h"
#include "cDLGReportCA.h"
#include "cBaselineManager.h"

FXDEFMAP(cDLGReportCA) mapDLGReportCA[]=
{
    FXMAPFUNC(SEL_UPDATE,cDLGReportCA::ID_BSLCHK,cDLGReportCA::onUpdBslchk),
    FXMAPFUNC(SEL_UPDATE,cDLGReportCA::ID_UPDATE,cDLGReportCA::onCmdUpdate),
    FXMAPFUNC(SEL_UPDATE,cDLGReportCA::CMD_BUT_APPLY,cDLGReportCA::onUpdApply),
    FXMAPFUNC(SEL_COMMAND,cDLGReportCA::CMD_BUT_APPLY,cDLGReportCA::onCmdApply),
};

FXIMPLEMENT(cDLGReportCA,FXDialogBox,mapDLGReportCA,ARRAYNUMBER(mapDLGReportCA))

cDLGReportCA::cDLGReportCA(FXWindow *prOwner,FXbool areTiters,FXbool areSecondary,FXbool areBins,FXbool areOnlyTs) :
    FXDialogBox(prOwner,oLanguage->getText("str_report_ca")+"...",DECOR_TITLE|DECOR_BORDER),chtype(choice)
{
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL);
        bUseInfo=new FXCheckButton(_vframe0,oLanguage->getText("str_report_showInfo"),this,ID_UPDATE);
        bUseInfo->setTextColor(FXRGB(0,0,200));
        bUseInfo->setCheck(true);
        new FXHorizontalSeparator(_vframe0);
        bUseGraphs=new FXCheckButton(_vframe0,oLanguage->getText("str_report_showGraph"),this,ID_UPDATE);
        bUseGraphs->setCheck(true);
        bUseGraphs->setTextColor(FXRGB(0,0,200));
        choice=3;
        FXGroupBox *_gb1=new FXGroupBox(_vframe0,(char*)NULL,GROUPBOX_NORMAL,0,0,0,0,17,0,0,0,0,0);
            if(areTiters /*&& !areOnlyTs*/)
            {
                bTitGraphs=new FXRadioButton(_gb1,oLanguage->getText("str_report_titGraph"),&chtype,FXDataTarget::ID_OPTION+1);
                bTitGraphs->setTextColor(FXRGB(120,120,120));
                choice=1;
            }
            else
                bTitGraphs=NULL;
            if(areBins)
            {
                bBinGraphs=new FXRadioButton(_gb1,oLanguage->getText("str_report_binGraph"),&chtype,FXDataTarget::ID_OPTION+2);
                bBinGraphs->setTextColor(FXRGB(120,120,120));
                if(!areTiters)
                    choice=2;
            }
            else
                bBinGraphs=NULL;
            if(areTiters)
            {
                bTsGraphs=new FXRadioButton(_gb1,oLanguage->getText("str_report_tsGraph"),&chtype,FXDataTarget::ID_OPTION+4);
                bTsGraphs->setTextColor(FXRGB(120,120,120));
                //if(areOnlyTs)
                //    choice=4;
            }
            else
                bTsGraphs=NULL;
            bSpGraphs=new FXRadioButton(_gb1,oLanguage->getText("str_report_spGraph"),&chtype,FXDataTarget::ID_OPTION+3);
            bSpGraphs->setTextColor(FXRGB(120,120,120));
        new FXHorizontalSeparator(_vframe0);
        bUsePlate=new FXCheckButton(_vframe0,oLanguage->getText("str_report_showPlate"),this,ID_UPDATE);
        bUsePlate->setTextColor(FXRGB(0,0,200));
        bUsePlate->setCheck(true);
        FXVerticalFrame *_gg0=new FXVerticalFrame (_vframe0,LAYOUT_FILL_X,0,0,0,0,17);
            FXVerticalFrame *_gframe1=new FXVerticalFrame(_gg0,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
                bcPCala=new FXCheckButton(_gframe1,oLanguage->getText("str_asymdi_cala"));
                bcPCala->setTextColor(FXRGB(120,120,120));
                bcPCala->setCheck(true);
        new FXHorizontalSeparator(_vframe0);
        bUseData=new FXCheckButton(_vframe0,oLanguage->getText("str_report_showData"),this,ID_UPDATE);
        bUseData->setTextColor(FXRGB(0,0,200));
        bUseData->setCheck(true);
        FXVerticalFrame *_gb0=new FXVerticalFrame (_vframe0,LAYOUT_FILL_X,0,0,0,0,17);
            FXVerticalFrame *_vframe1=new FXVerticalFrame(_gb0,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
                bcCala=new FXCheckButton(_vframe1,oLanguage->getText("str_asymdi_cala"));
                bcCala->setTextColor(FXRGB(120,120,120));
                bcCala->setCheck(true);
                if(areSecondary)
                {
                    bcCalb=new FXCheckButton(_vframe1,oLanguage->getText("str_asymdi_calb"));
                    bcCalb->setCheck(true);
                    bcCalb->setTextColor(FXRGB(120,120,120));
                }
                else
                    bcCalb=NULL;
                bcEU=new FXCheckButton(_vframe1,oLanguage->getText("str_rdgmdi_eu"));
                bcEU->setCheck(true);
                bcEU->setTextColor(FXRGB(120,120,120));
                if(areTiters)
                {
                    bcTiter=new FXCheckButton(_vframe1,oLanguage->getText("str_asymdi_titer"));
                    bcTiter->setCheck(true);
                    bcTiter->setTextColor(FXRGB(120,120,120));
                    bcLog2=new FXCheckButton(_vframe1,oLanguage->getText("str_rdgmdi_log2"));
                    bcLog2->setTextColor(FXRGB(120,120,120));
                    bcTiters=new FXCheckButton(_vframe1,oLanguage->getText("str_asymdi_titers"));
                    bcTiters->setCheck(true);
                    bcTiters->setTextColor(FXRGB(120,120,120));
                }
                else
                {
                    bcTiter=NULL;
                    bcLog2=NULL;
                    bcTiters=NULL;
                }
                if(areBins)
                {
                    bcBins=new FXCheckButton(_vframe1,oLanguage->getText("str_asymdi_bins"));
                    bcBins->setTextColor(FXRGB(120,120,120));
                    if(!areTiters)
                        bcBins->setCheck(true);
                    //if(areSecondary)
                    //{
                    //    bcBins2=new FXCheckButton(_vframe1,oLanguage->getText("str_asymdi_bins2"));
                    //    bcBins2->setCheck(true);
                    //    bcBins2->setTextColor(FXRGB(120,120,120));
                    //}
                    //else
                    //    bcBins2=NULL;
                }
                else
                {
                    bcBins=NULL;
                    //bcBins2=NULL;
                }
                bcAge=new FXCheckButton(_vframe1,oLanguage->getText("str_rdgmdi_clage"));
                bcAge->setCheck(true);
                bcAge->setTextColor(FXRGB(120,120,120));
                bcBreed1=new FXCheckButton(_vframe1,oLanguage->getText("str_dlg_pop_breed1"));
                bcBreed1->setTextColor(FXRGB(120,120,120));
                bcBreed2=new FXCheckButton(_vframe1,oLanguage->getText("str_dlg_pop_breed2"));
                bcBreed2->setTextColor(FXRGB(120,120,120));
                bcResult=new FXCheckButton(_vframe1,oLanguage->getText("str_rdgmdi_result"));
                bcResult->setCheck(true);
                bcResult->setTextColor(FXRGB(120,120,120));
                if(areSecondary)
                {
                    bcResult2=new FXCheckButton(_vframe1,oLanguage->getText("str_rdgmdi_result2"));
                    bcResult2->setTextColor(FXRGB(120,120,120));
                }
                else
                    bcResult2=NULL;
        new FXHorizontalSeparator(_vframe0);
        bcAm=new FXCheckButton(_vframe0,oLanguage->getText("str_rdgmdi_mean"));
        bcAm->setCheck(true);
        bcAm->setTextColor(FXRGB(0,0,200));
        new FXHorizontalSeparator(_vframe0);
        
        if(areTiters)
        {
            FXLabel *_lb0=new FXLabel(_vframe0,oLanguage->getText("str_report_bsl"));
                _lb0->setTextColor(FXRGB(0,120,0));
            FXVerticalFrame *_gb30=new FXVerticalFrame (_vframe0,LAYOUT_FILL_X,0,0,0,0,17,0,0,0);
                FXVerticalFrame *_gb31=new FXVerticalFrame (_gb30,LAYOUT_FILL_X,0,0,0,0,2,0,0,0);
                    bsl=new FXListBox(_gb31,NULL,0,FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X);
                    bsl->setTextColor(FXRGB(0,120,0));
                    bsl->appendItem(oLanguage->getText("str_report_bslno"));
                    sBaselineObject *res=cBaselineManager::listBaselines();
                    if(res==NULL)
                        return;
                    for(int i=0;i<cBaselineManager::getBaselineCount();i++)
                        bsl->appendItem(*res[i].id);
                    free(res);
                    bsl->setNumVisible(bsl->getNumItems()>7?7:bsl->getNumItems());
                bSS=new FXCheckButton(_gb30,oLanguage->getText("str_report_bslss"),this,ID_BSLCHK);
                bSS->setTextColor(FXRGB(0,120,0));
    
            new FXHorizontalSeparator(_vframe0);
        }
        else
            bsl=NULL;
        
        FXHorizontalFrame *_hframe100=new FXHorizontalFrame(_vframe0,LAYOUT_CENTER_X);
        butCancel=new FXButton(_hframe100,oLanguage->getText("str_but_cancel"),NULL,this,ID_CANCEL,BUTTON_DEFAULT|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
        butAccept=new FXButton(_hframe100,oLanguage->getText("str_but_ok"),NULL,this,CMD_BUT_APPLY,BUTTON_DEFAULT|BUTTON_INITIAL|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
}

cDLGReportCA::~cDLGReportCA()
{
}

long cDLGReportCA::onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData)
{
    handle(NULL,FXSEL(SEL_COMMAND,ID_ACCEPT),NULL);
    return 1;
}

long cDLGReportCA::onUpdApply(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(bUseGraphs->getCheck() || bUseInfo->getCheck() || bUseData->getCheck() || bUsePlate->getCheck())
        butAccept->enable();
    else
        butAccept->disable();
    return 1;
}

long cDLGReportCA::onCmdUpdate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(bUseGraphs->getCheck())
    {
        if(bTitGraphs)
        {
            bTitGraphs->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
            bTsGraphs->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        }
        if(bBinGraphs)
            bBinGraphs->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        bSpGraphs->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    else
    {
        if(bTitGraphs)
        {
            bTitGraphs->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
            bTsGraphs->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        }
        if(bBinGraphs)
            bBinGraphs->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        bSpGraphs->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }
    
    if(bUseData->getCheck())
    {
        bcCala->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bcCalb)
            bcCalb->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bcTiter)
            bcTiter->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bcTiters)
            bcTiters->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bcBins)
            bcBins->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        bcResult->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bcResult2)
            bcResult2->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        //bcBins2->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        bcAge->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bcLog2)
            bcLog2->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        bcEU->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        bcBreed1->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        bcBreed2->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    else
    {
        bcCala->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        if(bcCalb)
            bcCalb->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        if(bcTiter)
            bcTiter->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        if(bcTiters)
            bcTiters->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        if(bcBins)
            bcBins->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        bcResult->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        if(bcResult2)
            bcResult2->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        //bcBins2->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        bcAge->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        if(bcLog2)
            bcLog2->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        bcEU->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        bcBreed1->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        bcBreed2->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }
    return 1;
}

long cDLGReportCA::onUpdBslchk(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!bUseGraphs->getCheck() || (choice!=1 && choice!=4))
    {
        bsl->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        bSS->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }
    else
    {
        bsl->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        if(bsl->getCurrentItem()==0)
            bSS->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        else
            bSS->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    return 1;
}


