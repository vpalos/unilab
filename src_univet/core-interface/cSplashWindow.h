#ifndef CSPLASHWINDOW_H
#define CSPLASHWINDOW_H

#include <fx.h>

class cSplashWindow : public FXSplashWindow
{
    FXDECLARE(cSplashWindow);
    
    private:
    protected:
        cSplashWindow();
    
    public:
        FXbool over;
        
        cSplashWindow(FXWindow *ow, FXIcon *ic, FXuint opts=SPLASH_SIMPLE, FXuint ms=5000);
        ~cSplashWindow();
        
        long onHide(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
