#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cVaccineManager.h"
#include "cWinMain.h"
#include "cMDIDatabaseManager.h"
#include "cMDIVaccineManager.h"

cMDIVaccineManager *oMDIVaccineManager=NULL;

FXDEFMAP(cMDIVaccineManager) mapMDIVaccineManager[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIVaccineManager::ID_VACCINEMANAGER,cMDIVaccineManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIVaccineManager::CMD_BUT_CLOSE,cMDIVaccineManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIVaccineManager::CMD_BUT_OPEN,cMDIVaccineManager::onCmdOpen),
    FXMAPFUNC(SEL_COMMAND,cMDIVaccineManager::CMD_BUT_NEW,cMDIVaccineManager::onCmdNew),
    FXMAPFUNC(SEL_COMMAND,cMDIVaccineManager::CMD_BUT_DUPLICATE,cMDIVaccineManager::onCmdDuplicate),
    FXMAPFUNC(SEL_DOUBLECLICKED,cMDIVaccineManager::ID_VACCINELIST,cMDIVaccineManager::onCmdOpen),
    FXMAPFUNC(SEL_COMMAND,cMDIVaccineManager::CMD_BUT_REMOVE,cMDIVaccineManager::onCmdRemove),
    
};

FXIMPLEMENT(cMDIVaccineManager,cMDIChild,mapMDIVaccineManager,ARRAYNUMBER(mapMDIVaccineManager))

cMDIVaccineManager::cMDIVaccineManager()
{
}

cMDIVaccineManager::cMDIVaccineManager(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH) 
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_VACCINEMANAGER);
    client=prP;
    popup=prPup;
    
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(this,LAYOUT_FILL);
    FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe0,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
    new FXLabel(_vframe2,oLanguage->getText("str_vacman_vaclist_title"));
    FXHorizontalFrame *_hframe1=new FXHorizontalFrame(_vframe2,LAYOUT_FILL,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe4=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|LAYOUT_RIGHT,0,0,0,0,0,0,0,0,0,0);
    
    vacList=new cSortList(_vframe3,this,ID_VACCINELIST,ICONLIST_SINGLESELECT|ICONLIST_DETAILED|LAYOUT_FILL);
    vacList->appendHeader(oLanguage->getText("str_vacman_vacnameh"),NULL,150);
    vacList->appendHeader(oLanguage->getText("str_vacman_vactitleh"),NULL,234);
    oMDIVaccineManager=this;
    this->update();
    if(vacList->getNumItems()>0)
    {
        vacList->selectItem(0);
        vacList->setCurrentItem(0);
    }
    vacList->setFocus();

    new FXButton(_vframe4,oLanguage->getText("str_vacman_new"),NULL,this,CMD_BUT_NEW,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_FIX_WIDTH,0,0,88,0);
    new FXHorizontalSeparator(_vframe4,LAYOUT_TOP|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_vacman_open"),NULL,this,CMD_BUT_OPEN,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X);
    new FXButton(_vframe4,oLanguage->getText("str_vacman_duplicate"),NULL,this,CMD_BUT_DUPLICATE,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X);
    new FXButton(_vframe4,oLanguage->getText("str_but_close"),NULL,this,CMD_BUT_CLOSE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
    new FXHorizontalSeparator(_vframe4,LAYOUT_BOTTOM|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_vacman_remove"),NULL,this,CMD_BUT_REMOVE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
}

cMDIVaccineManager::~cMDIVaccineManager()
{
    cMDIVaccineManager::unload();
}

void cMDIVaccineManager::create()
{
    cMDIChild::create();
    show();
}

void cMDIVaccineManager::load(FXMDIClient *prP,FXPopup *prMenu)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(prP,prMenu);
        return;
    }
    
    if(oMDIVaccineManager!=NULL)
    {
        oMDIVaccineManager->setFocus();
        prP->setActiveChild(oMDIVaccineManager);
        if(oMDIVaccineManager->isMinimized())
            oMDIVaccineManager->restore();
        return;
    }
    new cMDIVaccineManager(prP,oLanguage->getText("str_vacman_title"),new FXGIFIcon(oApplicationManager,data_inputs),prMenu,MDI_NORMAL|MDI_TRACKING,10,10,515,300);
    oMDIVaccineManager->create();
    oMDIVaccineManager->setFocus();

}

FXbool cMDIVaccineManager::isLoaded(void)
{
    return (oMDIVaccineManager!=NULL);
}

void cMDIVaccineManager::unload(void)
{
    oMDIVaccineManager=NULL;
}

void cMDIVaccineManager::update(void)
{
    if(!isLoaded())
        return;
    sVaccineObject *res=cVaccineManager::listVaccines();
    if(res==NULL)
        return;
    vacList->clearItems();
    for(int i=0;i<cVaccineManager::getVaccineCount();i++)
    vacList->appendItem(*(res[i].id)+"\t"+(res[i].solid>0?"* ":"")+*(res[i].title));
    vacList->sortItems();
    free(res);
}

void cMDIVaccineManager::cmdRefresh(void)
{
    if(!isLoaded())
        return;
    oMDIVaccineManager->update();
}

long cMDIVaccineManager::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDIVaccineManager::unload();
    close();
    return 1;
}

long cMDIVaccineManager::onCmdNew(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cVaccineManager::newVaccine(client,popup);
    return 1;
}

long cMDIVaccineManager::onCmdOpen(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int db=-1,i;
    for(i=0;i<vacList->getNumItems();i++)
        if(vacList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        db=vacList->getCurrentItem();
    if(db==-1)
        return 1;
    if(cVaccineManager::vaccineSolid(vacList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasevacsolid").text());
        return 1;
    }
    FXString st=vacList->getItemText(db).before('\t');
    if(oWinMain->isWindow(st))
        oWinMain->raiseWindow(st);
    else
        cVaccineManager::editVaccine(client,popup,st);
    return 1;
}

long cMDIVaccineManager::onCmdDuplicate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int db=-1,i,j;
    for(i=0;i<vacList->getNumItems();i++)
        if(vacList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        return 1;
    FXString res2=cVaccineManager::duplicateVaccine(vacList->getItemText(db).before('\t').text());
    this->update();
    if(res2.empty())
        return 1;
    for(j=0;j<vacList->getNumItems();j++)
        if(res2==vacList->getItemText(j).before('\t'))
        {
            vacList->selectItem(j);
            break;
        }
    return 1;
}

long cMDIVaccineManager::onCmdRemove(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i,db=-1;
    for(i=0;i<vacList->getNumItems();i++)
        if(vacList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        return 1;
    if(cVaccineManager::vaccineSolid(vacList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasevacsolid").text());
        return 1;
    }
    if(cVaccineManager::vaccineNeeded(vacList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasedataneeded").text());
        return 1;
    }
    if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_erasevac").text()))
        return 1;
    cMDIChild *win=oWinMain->getWindow(vacList->getItemText(db).before('\t'));
    if(win!=NULL)
        win->close();
    cVaccineManager::removeVaccine(vacList->getItemText(db).before('\t'));
    this->update();
    if(db>=vacList->getNumItems())
        db=vacList->getNumItems()-1;
    if(db>=0)
        vacList->selectItem(db);
    return 1;
}


