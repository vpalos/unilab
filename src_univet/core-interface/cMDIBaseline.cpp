#include <time.h>
#include <ctype.h>
#include <stdlib.h>

#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cColorsManager.h"
#include "cFormulaRatio.h"
#include "cSpeciesManager.h"
#include "cBaselineManager.h"
#include "cCaseManager.h"
#include "cPopulationManager.h"
#include "cWinMain.h"
#include "cMDIBaseline.h"
#include "cMDIBaselineManager.h"
#include "cAssayManager.h"
#include "cTemplateManager.h"
#include "cMDICaseManager.h"

FXDEFMAP(cMDIBaseline) mapMDIBaseline[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIBaseline::ID_MDIBASELINE,cMDIBaseline::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIBaseline::ID_BSLID,cMDIBaseline::onCmdTfId),
    FXMAPFUNC(SEL_CHANGED,cMDIBaseline::ID_BSLID,cMDIBaseline::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIBaseline::ID_BSLTABLE,cMDIBaseline::onCmdUpdateData),
    FXMAPFUNC(SEL_REPLACED,cMDIBaseline::ID_BSLTABLE,cMDIBaseline::onCmdTable),
    FXMAPFUNC(SEL_CHANGED,cMDIBaseline::ID_BSLCOMMENTS,cMDIBaseline::onCmdUpdateData),

    FXMAPFUNC(SEL_COMMAND,cMDIBaseline::CMD_LCASES,cMDIBaseline::onCmdLCases),
    FXMAPFUNC(SEL_COMMAND,cMDIBaseline::CMD_CLOSE,cMDIBaseline::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIBaseline::CMD_SAVE,cMDIBaseline::onCmdSave),
    FXMAPFUNC(SEL_COMMAND,cMDIBaseline::CMD_PRINT,cMDIBaseline::onCmdPrint),
};

FXIMPLEMENT(cMDIBaseline,cMDIChild,mapMDIBaseline,ARRAYNUMBER(mapMDIBaseline))

cMDIBaseline::cMDIBaseline()
{
}

void cMDIBaseline::updateData(void)
{
    saved=false;
}

FXbool cMDIBaseline::saveData(void)
{
    table->acceptInput(true);
    if(saved)
        return true;

    if(!name.empty() && cBaselineManager::baselineSolid(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasebslsolid").text());
        saved=true;
        return true;
    }

    FXString oldname=name;
    name=tfId->getText();
    if(name!=oldname && cBaselineManager::baselineExists(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
        tfId->setText(cBaselineManager::getNewID());
        setTitle(tfId->getText());
        updateData();
        return false;
    }
    this->setTitle(name);

    FXString params;

    for(int i=0;i<table->getNumRows();i++)
    {
        for(int j=0;j<table->getNumColumns();j++)
            params=params+(j?"\t":"")+table->getItemText(i,j);
        params=params+"\n";
    }

    if(oldname.empty())
        cBaselineManager::addBaseline(name,params,comments->getText());
    else
        cBaselineManager::setBaseline(oldname,name,params,comments->getText());

    saved=true;
    cMDIBaselineManager::cmdRefresh();
    return true;
}

cMDIBaseline::cMDIBaseline(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH)
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_MDIBASELINE);
    saved=false;
    name="";

    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(this,LAYOUT_FILL,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe0=new FXVerticalFrame(_hframe0,LAYOUT_FILL,0,0,0,0);
        new FXLabel(_vframe0,oLanguage->getText("str_bslmdi_id"));
        tfId=new FXTextField(_vframe0,5,this,ID_BSLID,LAYOUT_FILL_X|TEXTFIELD_NORMAL);
            tfId->setText(prName);
            tfId->setSelection(0,tfId->getText().length());
            tfId->setFocus();
        new FXLabel(_vframe0,oLanguage->getText("str_bslmdi_table"));
        FXVerticalFrame *_vframe6=new FXVerticalFrame(_vframe0,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
            table=new cColorTable(_vframe6,this,ID_BSLTABLE,LAYOUT_FILL|TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT);
            table->setTableSize(BASELINE_PARCOUNT,5);
            for(int i=0;i<BASELINE_PARCOUNT;i++)
            {
                table->setRowText(i,FXStringFormat("%d",i+1));
                table->getRowHeader()->setItemJustify(i,JUSTIFY_CENTER_X);
                for(int j=0;j<5;j++)
                    table->setItemJustify(i,j,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                table->setItemText(i,0,"-");
                table->setItemText(i,1,"-");
                table->setCellColor(i,2,FXRGB(240,240,240));
                table->setCellColor(i,3,FXRGB(240,240,240));
                table->setCellColor(i,4,FXRGB(240,230,200));
            }
            table->setRowHeaderWidth(20);
            for(int j=0;j<5;j++)
                table->getColumnHeader()->setItemJustify(j,JUSTIFY_CENTER_X);
            table->setDefRowHeight(17);
            table->showVertGrid(false);
            table->setScrollStyle(HSCROLLING_ON|VSCROLLING_ON|VSCROLLER_ALWAYS|SCROLLERS_TRACK);
            table->setSelectable(false);
            table->setColumnText(0,oLanguage->getText("str_bslmdi_fa"));
            table->setColumnWidth(0,77);
            table->setColumnText(1,oLanguage->getText("str_bslmdi_ta"));
            table->setColumnWidth(1,80);
            table->setColumnText(2,oLanguage->getText("str_bslmdi_l"));
            table->setColumnWidth(2,65);
            table->setColumnText(3,oLanguage->getText("str_bslmdi_h"));
            table->setColumnWidth(3,65);
            table->setColumnText(4,oLanguage->getText("str_bslmdi_s"));
            table->setColumnWidth(4,70);

        new FXLabel(_vframe0,oLanguage->getText("str_bslmdi_comments"));
        FXVerticalFrame *_vframe7=new FXVerticalFrame(_vframe0,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT|FRAME_SUNKEN|FRAME_THICK,0,0,0,44,0,0,0,0);
        comments=new FXText(_vframe7,this,ID_BSLCOMMENTS,TEXT_WORDWRAP|TEXT_NO_TABS|LAYOUT_FILL);

    FXVerticalFrame *_vframe8=new FXVerticalFrame(_hframe0,LAYOUT_FILL_Y,0,0,0,0);
        new FXButton(_vframe8,oLanguage->getText("str_but_close"),NULL,this,CMD_CLOSE,FRAME_RAISED|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,80,0);
        //new FXButton(_vframe8,oLanguage->getText("str_bslmdi_print"),new FXGIFIcon(getApp(),data_settings_printer),this,CMD_PRINT,FRAME_RAISED|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,80,0);
        //new FXHorizontalSeparator(_vframe8,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT|LAYOUT_BOTTOM,0,0,0,1);
        new FXButton(_vframe8,oLanguage->getText("str_bslmdi_save"),NULL,this,CMD_SAVE,FRAME_RAISED|BUTTON_DEFAULT|BUTTON_INITIAL|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,80,0);
        new FXHorizontalSeparator(_vframe8,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT|LAYOUT_BOTTOM,0,0,0,168);
        new FXButton(_vframe8,oLanguage->getText("str_bslmdi_lcases"),NULL,this,CMD_LCASES,FRAME_RAISED|LAYOUT_FIX_WIDTH|LAYOUT_BOTTOM,0,0,80,0);

    tSetsCount=0;
    tCasesCount=0;
    casesSets=NULL;
}

cMDIBaseline::~cMDIBaseline()
{
    if(casesSets)
    {
        for(int i=0;i<tSetsCount;i++)
        {
            if(casesSets[i].assayRes)
                delete casesSets[i].assayRes;
            if(casesSets[i].templateRes)
                delete casesSets[i].templateRes;
            for(int j=0;j<casesSets[i].casesCount;j++)
            {
                free(casesSets[i].cases[j].data);
                free(casesSets[i].cases[j].controlsData);
            }
            free(casesSets[i].cases);
        }
        free(casesSets);
    }
}

void cMDIBaseline::create()
{
    cMDIChild::create();
    show();
    updateData();
}

FXbool cMDIBaseline::canClose(void)
{
    table->acceptInput(true);
    if(!saved)
    {
        setFocus();
        int res=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),oLanguage->getText("str_qsave").text());
        switch(res)
        {
            case MBOX_CLICKED_YES:
                if(!saveData())
                    return false;
                break;
            case MBOX_CLICKED_CANCEL:
                return false;
            default:
                break;
        }
    }
    if(cMDICaseManager::isLoaded() && cMDICaseManager::sameHandler(this))
    {
        cMDICaseManager::unload();
        cMDICaseManager::unshow();
    }
    return true;
}

FXbool cMDIBaseline::loadBaseline(const FXString &prId)
{
    if(!cBaselineManager::baselineExists(prId))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_openbsl").text());
        return false;
    }
    if(!oWinMain->isWindow(prId))
        return oWinMain->raiseWindow(prId);

    cDataResult *res=cBaselineManager::getBaseline(prId);
    if(res==NULL)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_openbsl").text());
        return false;
    }

    name=res->getCellString(0,0);
    tfId->setText(name);

    FXString params=res->getCellString(0,2);
    int r=0;
    while(!params.empty())
    {
        if(table->getNumRows()<=r)
            break;
        FXString psrow=params.before('\n');
        params=params.after('\n');
        int c=0;
        while(!psrow.empty())
        {
            if(table->getNumColumns()<=c)
                break;
            table->setItemText(r,c,psrow.before('\t'));
            psrow=psrow.after('\t');
            c++;
        }
        r++;
    }

    comments->setText(res->getCellString(0,3));
    setTitle(name);
    updateData();
    saved=true;
    return true;
}

long cMDIBaseline::onCmdTfId(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    if(!tfId->getText().empty() && tfId->getText()!=name)
    {
        if(!cBaselineManager::baselineExists(tfId->getText()))
            return 1;
        else
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
    }
    tfId->setText(getTitle());
    return 1;
}

long cMDIBaseline::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(canClose())
    {
        close();
        return 1;
    }
    return 0;
}

long cMDIBaseline::onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    return 1;
}

long cMDIBaseline::onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData)
{
    saveData();
    if(saved)
        onCmdClose(NULL,0,NULL);
    return 1;
}

long cMDIBaseline::onCmdTable(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXString s,s1,s2;
    int i,r,c;
    unsigned int t,v1,v2,cat;

    r=table->getCurrentRow();
    c=table->getCurrentColumn();
    s=table->getItemText(r,c);
    if(c!=0 && c!=1)
    {
        if(!s.empty())
            table->setItemText(r,c,cMDIReadings::calculVal(FXFloatVal(s)));
        return 1;
    }
    if(s.empty())
        table->setItemText(r,c,"-");
    if(table->getItemText(r,c)=="-")
        return 1;
    for(i=0;i<s.length();i++)
        if(!isdigit(s.at(i)) && s.at(i)!='-')
        {
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),
                              (oLanguage->getText("str_invalid")+"\n"+
                               oLanguage->getText("str_invalid_age")).text());;
            table->setItemText(r,c,"-");
            table->startInput(r,c);
            return 1;
        }
    s1=s.before('-');
    s2=s.after('-');
    if(s2.empty())
    {
        v1=0;
        v2=FXIntVal(s1);
    }
    else
    {
        v1=FXIntVal(s1);
        v2=FXIntVal(s2);
    }
    cat=cSpeciesManager::getAgeFormat(oDataLink->getSpecies());
    t=v1*cat+v2;
    v1=t/cat;
    v2=t%cat;
    if((unsigned int)v1>999)v1=999;
    table->setItemText(r,c,FXStringFormat("%d-%d",v1,v2));

    updateData();
    return 1;
}

long cMDIBaseline::onCmdLCases(FXObject *prSender,FXSelector prSelector,void *prData)
{
    table->acceptInput(true);
    bool is=false;
    for(int i=0;i<BASELINE_PARCOUNT;i++)
    {
        if(table->getItemText(i,0)!="-" && table->getItemText(i,1)!="-")
        {
            is=true;
            break;
        }
    }
    if(!is)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),
                          oLanguage->getText("str_bslmdi_arerr").text());
        return 1;
    }
    cMDICaseManager::loadBsl(mdiClient,mdiMenu,this,oLanguage->getText("str_bslmdi_csltitle"));
    return 1;
}

long cMDIBaseline::onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData)
{
    // TODO
    return 1;
}

FXbool cMDIBaseline::loadReadings(cSortList &prCases)
{
    FXDialogBox msg(oWinMain,oLanguage->getText("str_report_wtitle"),DECOR_TITLE|DECOR_BORDER);
    new FXLabel(&msg,oLanguage->getText("str_report_ltext"),NULL,LAYOUT_FILL|JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
    msg.create();
    msg.show(PLACEMENT_OWNER);
    msg.setFocus();
    msg.raise();
    oApplicationManager->beginWaitCursor();
    while(oApplicationManager->peekEvent())
        oApplicationManager->runOneEvent(false);
    FXString criteria="";
    int j=0;
    for(int i=0;i<prCases.getNumItems();i++)
        if(prCases.isItemSelected(i))
        {
            if(j>0)
                criteria=criteria+" OR ";
            criteria=criteria+"(assay_oid='"+prCases.getItemText(i).before('\t')+"' AND "+"id='"+prCases.getItemText(i).after('\t').before('\t')+"')";
            j++;
        }
    if(!j)
        return false;
    cDataResult *resr=oDataLink->execute("SELECT DISTINCT reading_oid FROM t_gncases WHERE "+criteria+" ORDER BY assay_oid ASC;");
    tSetsCount=resr->getRowCount();
    if(!tSetsCount)
        return false;

    cDataResult *resc,*resp;
    casesSets=(sCasesSet*)malloc(tSetsCount*sizeof(sCasesSet));
    if(!casesSets)
    {
        tSetsCount=0;
        tCasesCount=0;
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return false;
    }

    tCasesCount=0;
    areBins=false;
    areTiters=false;
    noTiters=false;
    areSecondary=false;

    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        FXString __reading_oid(resr->getCellString(setsi,0));
        resc=oDataLink->execute("SELECT * FROM t_gncases WHERE ("+criteria+") AND reading_oid='"+__reading_oid+"' ORDER BY (startPlate*100+startCell) ASC;");
        casesSets[setsi].casesCount=resc->getRowCount();
        tCasesCount+=casesSets[setsi].casesCount;
        casesSets[setsi].cases=(sCaseEntry*)malloc(casesSets[setsi].casesCount*sizeof(sCaseEntry));
        if(!casesSets[setsi].cases)
        {
            tSetsCount=0;
            tCasesCount=0;
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
            return false;
        }

        FXString __assay(resc->getCellString(0,6));
        FXString __template(resc->getCellString(0,16));
        casesSets[setsi].assayRes=oDataLink->execute("SELECT * FROM t_assays WHERE id='"+__assay+"';");
        casesSets[setsi].templateRes=oDataLink->execute("SELECT * FROM t_templates WHERE id='"+__template+"';");

        casesSets[setsi].orientation=resc->getCellInt(0,17);
        {
            int k=resc->getCellInt(0,18);
            casesSets[setsi].alelisa=k==2?true:false;
        }
        int __alelisa=casesSets[setsi].alelisa;
        int __alelisa_multiplier=__alelisa?2:1;

        FXString __lot(resc->getCellString(0,7));
        FXString __expDate(resc->getCellString(0,8));

        FXString calculations_defs=casesSets[setsi].assayRes->getCellString(0,6);
        casesSets[setsi].useSecond=FXIntVal(calculations_defs.before('\n'))==2?true:false;
        if(casesSets[setsi].useSecond)
            areSecondary=true;
        calculations_defs=calculations_defs.after('\n');

        casesSets[setsi].cala=new FXString(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        casesSets[setsi].calapco=new FXString(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        casesSets[setsi].calaop=new FXString(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        casesSets[setsi].calasco=new FXString(calculations_defs.before('\n'));
        calculations_defs=calculations_defs.after('\n');

        if(casesSets[0].useSecond)
        {
            casesSets[setsi].calb=new FXString(calculations_defs.before('\t'));
            calculations_defs=calculations_defs.after('\t');
            casesSets[setsi].calbpco=new FXString(calculations_defs.before('\t'));
            calculations_defs=calculations_defs.after('\t');
            casesSets[setsi].calbop=new FXString(calculations_defs.before('\t'));
            calculations_defs=calculations_defs.after('\t');
            casesSets[setsi].calbsco=new FXString(calculations_defs.before('\n'));
            calculations_defs=calculations_defs.after('\n');
        }
        else
        {
            casesSets[setsi].calb=NULL;
            casesSets[setsi].calbop=NULL;
            casesSets[setsi].calbpco=new FXString("0");
            casesSets[setsi].calbsco=new FXString("0");
        }

        if(*casesSets[setsi].calasco=="0")
            *casesSets[setsi].calasco=*casesSets[setsi].calapco;
        if(*casesSets[setsi].calbsco=="0")
            *casesSets[setsi].calbsco=*casesSets[setsi].calbpco;

        FXString factors_defs=casesSets[setsi].assayRes->getCellString(0,5);
        for(int i=0;i<4;i++)
        {
            casesSets[setsi].factors[i]=FXIntVal(factors_defs.before('\t'));
            factors_defs=factors_defs.after('\t');
        }

        FXString titers_defs=casesSets[setsi].assayRes->getCellString(0,7);
        casesSets[setsi].useTiters=FXIntVal(titers_defs.before('\t'))!=0?true:false;
        if(casesSets[setsi].useTiters)
            areTiters=true;
        else
            noTiters=true;
        titers_defs=titers_defs.after('\t');
        casesSets[setsi].slope=FXFloatVal(titers_defs.before('\t'));
        titers_defs=titers_defs.after('\t');
        casesSets[setsi].intercept=FXFloatVal(titers_defs.before('\t'));
        titers_defs=titers_defs.after('\t');
        casesSets[setsi].calt=new FXString(titers_defs.before('\n'));
        titers_defs=titers_defs.after('\n');
        for(int i=0;i<30;i++)
        {
            casesSets[setsi].titerGroups[i]=FXIntVal(titers_defs.before('\t'));
            titers_defs=titers_defs.after('\t');
        }

        FXString bins_defs=casesSets[setsi].assayRes->getCellString(0,8);
        for(int i=0;i<30;i++)
        {
            casesSets[setsi].ratioGroups[i]=FXFloatVal(bins_defs.before('\t'));
            bins_defs=bins_defs.after('\t');
            if(casesSets[setsi].ratioGroups[i]>0)
                areBins=true;
        }

        FXString __controls_defs(resc->getCellString(0,22));
        __controls_defs=__controls_defs.after('\n');
        casesSets[setsi].controlsPerPlate=0;
        int value;
        do{
            value=FXIntVal(__controls_defs.before('\t'));
            __controls_defs=__controls_defs.after('\t');
            if(value!=0 && value!=-100)
                casesSets[setsi].controlsLayout[casesSets[setsi].controlsPerPlate++]=value;
            else
                break;
        }while(true);
        cMDIReadings::sortControls(casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate);

        casesSets[setsi].platesCount=0;
        FXDict *pl=new FXDict();
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++)
        {
            casesSets[setsi].cases[casei].startCell=resc->getCellInt(casei,20);
            casesSets[setsi].cases[casei].startPlate=resc->getCellInt(casei,19);
            casesSets[setsi].cases[casei].plateSpanCount=resc->getCellInt(casei,21);

            int ps=casesSets[setsi].cases[casei].startPlate,pe=ps+casesSets[setsi].cases[casei].plateSpanCount-1;
            FXString key;
            for(int i=ps;i<=pe;i++)
                pl->insert(FXStringFormat("%c%c",i/256+1,i%256+1).text(),NULL);

            casesSets[setsi].cases[casei].id=new FXString(resc->getCellString(casei,0));
            casesSets[setsi].cases[casei].reading_oid=new FXString(__reading_oid);
            casesSets[setsi].cases[casei].count=resc->getCellInt(casei,15);
            casesSets[setsi].cases[casei].replicates=resc->getCellInt(casei,11);
            casesSets[setsi].cases[casei].factor=resc->getCellInt(casei,12);
            casesSets[setsi].cases[casei].alelisa=__alelisa;

            casesSets[setsi].sampleCount+=casesSets[0].cases[casei].count*__alelisa_multiplier;

            casesSets[setsi].cases[casei].age=new FXString(resc->getCellString(casei,14));
            casesSets[setsi].cases[casei].tpl=new FXString(__template);
            casesSets[setsi].cases[casei].veterinarian=new FXString(resc->getCellString(casei,5));
            casesSets[setsi].cases[casei].controls_defs=new FXString(resc->getCellString(casei,22));
            casesSets[setsi].cases[casei].readingDate=new FXString(resc->getCellString(casei,2));
            casesSets[setsi].cases[casei].technician=new FXString(resc->getCellString(casei,3));
            casesSets[setsi].cases[casei].reason=new FXString(resc->getCellString(casei,4));
            casesSets[setsi].cases[casei].lot=new FXString(__lot);
            casesSets[setsi].cases[casei].expirationDate=new FXString(__expDate);
            casesSets[setsi].cases[casei].spType=new FXString(resc->getCellString(casei,9));
            casesSets[setsi].cases[casei].bleedDate=new FXString(resc->getCellString(casei,10));
            casesSets[setsi].cases[casei].comments=new FXString(resc->getCellString(casei,24));
            casesSets[setsi].cases[casei].population=new FXString(resc->getCellString(casei,13));
            casesSets[setsi].cases[casei].assay=new FXString(__assay);

            resp=oDataLink->execute("SELECT * FROM t_gnpopulations WHERE id='"+*casesSets[setsi].cases[casei].population+"';");
            casesSets[setsi].cases[casei].breed1=new FXString(resp->getCellString(casei,3));
            casesSets[setsi].cases[casei].breed2=new FXString(resp->getCellString(casei,4));
            resp->free();

            casesSets[setsi].cases[casei].parentSet=setsi;

            FXString __data(resc->getCellString(casei,23));
            FXString __controls=__data.before('\n');
            __data=__data.after('\n');

            casesSets[setsi].cases[casei].controlsCount=casesSets[setsi].cases[casei].plateSpanCount*__alelisa_multiplier*casesSets[setsi].controlsPerPlate;
            casesSets[setsi].cases[casei].controlsData=(double*)malloc(casesSets[setsi].cases[casei].controlsCount*sizeof(double));
            if(!casesSets[setsi].cases[casei].controlsData)
            {
                tSetsCount=0;
                tCasesCount=0;
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                return false;
            }
            for(int i=0;i<casesSets[setsi].cases[casei].controlsCount;i++)
            {
                casesSets[setsi].cases[casei].controlsData[i]=FXFloatVal(__controls.before(' '));
                __controls=__controls.after(' ');
            }

            int ct=casesSets[setsi].cases[casei].count*__alelisa_multiplier*(casesSets[setsi].cases[casei].replicates+1);
            casesSets[setsi].cases[casei].data=(double*)malloc(ct*sizeof(double));
            if(!casesSets[setsi].cases[casei].data)
            {
                tSetsCount=0;
                tCasesCount=0;
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                return false;
            }
            for(int i=0;i<ct;i++)
            {
                casesSets[setsi].cases[casei].data[i]=FXFloatVal(__data.before(' '));
                __data=__data.after(' ');
            }
        }
        casesSets[setsi].platesCount=pl->no();
        delete pl;
        resc->free();
    }
    resr->free();
    bool incompatible=false,start=casesSets[0].alelisa;
    for(int setsi=0;setsi<tSetsCount;setsi++)
        if(start!=casesSets[setsi].alelisa)
        {
            incompatible=true;
            break;
        }

    if(noTiters)
    {
        tSetsCount=0;
        tCasesCount=0;
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_report_notiter").text());
        return false;
    }

    oApplicationManager->endWaitCursor();
    msg.hide();

    if(incompatible)
    {
        tSetsCount=0;
        tCasesCount=0;
        oApplicationManager->endWaitCursor();
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_reading_incompatcse").text());
        return false;
    }

    return true;
}

FXbool cMDIBaseline::processReadings(void)
{
    if(!tCasesCount)
        return false;
    char replicates[10];
    bool __alelisa=false;
    int __alelisa_multiplier=1;
    int k,l,offset,m;
    int platesi=0;
    int platesci=0;
    int cat=cSpeciesManager::getAgeFormat(oDataLink->getSpecies());
    int datai=0;
    int samplei=0;
    int casesgi=0;
    int controlsi=0;
    int casepi=0;
    int p2_datai=1;
    int deltasi=0;
    int replicatesi=0;
    FXdouble *cv[BASELINE_PARCOUNT];
    FXdouble tv[BASELINE_PARCOUNT][7];
    for(int i=0;i<BASELINE_PARCOUNT;i++)
    {
        cv[i]=NULL;
        tv[i][0]=tv[i][1]=tv[i][2]=0;
        tv[i][3]=999999;
        tv[i][4]=-1;
        tv[i][5]=tv[i][6]=0;

        FXString sage=table->getItemText(i,0);
        if(sage=="-")
            tv[i][0]=-1;
        else
            tv[i][0]=FXIntVal(sage.before('-'))*cat+FXIntVal(sage.after('-'));

        sage=table->getItemText(i,1);
        if(sage=="-")
            tv[i][1]=-1;
        else
            tv[i][1]=FXIntVal(sage.before('-'))*cat+FXIntVal(sage.after('-'));
    }
    FXdouble statsBase[5];
    double *statsValues;
    double ndata,aldata=0;
    FXint statsCount=0;
    double *ctData=NULL,n,val;
    FXString invalidPlates="";
    int dp,rp,pd=0,diff=0,spi=-1,cplatei,splatei;
    FXStringDict *csList=new FXStringDict();
    FXDialogBox msg(oWinMain,oLanguage->getText("str_report_wtitle"),DECOR_TITLE|DECOR_BORDER);
    new FXLabel(&msg,oLanguage->getText("str_bslmdi_wtext"),NULL,LAYOUT_FILL|JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
    msg.create();
    msg.show(PLACEMENT_OWNER);
    msg.setFocus();
    msg.raise();
    oApplicationManager->beginWaitCursor();
    while(oApplicationManager->peekEvent())
        oApplicationManager->runOneEvent(false);
    FXStringDict *sList=new FXStringDict();
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        spi=-1;
        splatei=-1;
        diff=0;
        __alelisa=casesSets[setsi].alelisa && casesSets[setsi].orientation==1?true:false;
        __alelisa_multiplier=__alelisa?2:1;
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++,casesgi++)
        {
            for(int i=0;i<5;i++)
                statsBase[i]=0;

            int ctui=casesSets[setsi].cases[casei].count;
            statsValues=(double*)malloc(ctui*sizeof(double));
            if(!statsValues)
            {
                tSetsCount=0;
                tCasesCount=0;
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                return false;
            }
            statsBase[0]=999999;
            statsBase[4]=ctui;

            cplatei=0;
            samplei=0;
            int rdi=datai;
            for(int g=spi+1;g<casesSets[setsi].cases[casei].startPlate;g++,diff++,splatei++);
            spi=casesSets[setsi].cases[casei].startPlate;
            while(samplei<casesSets[setsi].cases[casei].count*__alelisa_multiplier)
            {
                deltasi=0;
                replicatesi=0;
                replicates[0]=0;
                while(replicatesi<=casesSets[setsi].cases[casei].replicates)
                {
                    platesi=datai/(96/__alelisa_multiplier);
                    if(platesci==platesi || datai==rdi)
                    {
                        splatei++;
                        if(samplei)cplatei++;
                        ctData=casesSets[setsi].cases[casei].controlsData+cplatei*casesSets[setsi].controlsPerPlate*__alelisa_multiplier;

                        double na=0,pa=0,nha=0,pha=0,pc=0,nc=0;
                        for(int cswitchi=0;cswitchi<2;cswitchi++)
                        {
                            controlsi=0;
                            for(int i=0;i<casesSets[setsi].controlsPerPlate;i++)
                            {
                                if(((!cswitchi) && (casesSets[setsi].controlsLayout[i]>=0)) ||
                                   (cswitchi && (casesSets[setsi].controlsLayout[i]<0)))
                                {
                                    controlsi+=__alelisa?2:1;
                                    continue;
                                }

                                if(casesSets[setsi].controlsLayout[i]>=0)
                                    {pa+=ctData[controlsi];pc++;}
                                if(casesSets[setsi].controlsLayout[i]<0)
                                    {na+=ctData[controlsi];nc++;}

                                m=cMDIReadings::platePosition(abs(casesSets[setsi].controlsLayout[i])==1000?0:abs(casesSets[setsi].controlsLayout[i]),false,casesSets[setsi].orientation);
                                l=m%12;
                                p2_datai++;
                                controlsi++;
                                if(__alelisa)
                                {
                                    if(casesSets[setsi].controlsLayout[i]>=0)
                                        pha+=ctData[controlsi];
                                    if(casesSets[setsi].controlsLayout[i]<0)
                                        nha+=ctData[controlsi];

                                    m=cMDIReadings::platePosition(abs(casesSets[setsi].controlsLayout[i])==1000?0:abs(casesSets[setsi].controlsLayout[i]),false,casesSets[setsi].orientation)+1;
                                    l=m%12;
                                    p2_datai++;
                                    controlsi++;
                                }
                            }
                        }

                        platesci++;
                        FXString rule,op,r2,rules_defs=casesSets[setsi].assayRes->getCellString(0,10);
                        r2=rules_defs;
                        na/=nc;
                        pa/=pc;
                        nha/=nc;
                        pha/=pc;
                    }

                    casepi=datai%(96/__alelisa_multiplier);
                    dp=datai/(96/__alelisa_multiplier);
                    rp=pd+casesSets[setsi].cases[casei].startPlate-diff;
                    if(!cMDIReadings::isControlPosition(casepi,casesSets[setsi].controlsLayout,
                                          casesSets[setsi].controlsPerPlate,__alelisa) &&
                                          ((casepi>=casesSets[setsi].cases[casei].startCell && dp==rp) || (dp>rp)))
                    {
                        m=cMDIReadings::platePosition(datai%(96/__alelisa_multiplier),__alelisa,casesSets[setsi].orientation);
                        l=m%12;
                        k=(datai/(96/__alelisa_multiplier))*8+m/12;
                        offset=(casesSets[setsi].cases[casei].replicates+1)*samplei+__alelisa_multiplier*replicatesi;
                        if(replicatesi==0)
                        {
                            val=n=0;
                            for(int xi=0;xi<=casesSets[setsi].cases[casei].replicates;xi++,n++)
                                val+=casesSets[setsi].cases[casei].data[(casesSets[setsi].cases[casei].replicates+1)*samplei+__alelisa_multiplier*xi];
                            ndata=val/n;
                        }
                        else
                        {
                            ndata=-999;
                        }
                        deltasi=1;
                        if(__alelisa)
                        {
                            m=cMDIReadings::platePosition(datai%(96/__alelisa_multiplier),__alelisa,casesSets[setsi].orientation,true);
                            l=m%12;
                            k=(datai/(96/__alelisa_multiplier))*8+m/12;
                            offset=(casesSets[setsi].cases[casei].replicates+1)*samplei+__alelisa_multiplier*replicatesi+1;
                            if(replicatesi==0)
                            {
                                val=n=0;
                                for(int xi=0;xi<=casesSets[setsi].cases[casei].replicates;xi++,n++)
                                    val+=casesSets[setsi].cases[casei].data[(casesSets[setsi].cases[casei].replicates+1)*samplei+__alelisa_multiplier*xi+1];
                                aldata=val/n;
                            }
                            else
                                aldata=-999;
                            deltasi=2;
                        }

                        if(ndata!=-999)
                        {
                            double calt,titer;
                            calt=cFormulaRatio::calculateRatio(casesSets[setsi].calt,
                                                               ndata,
                                                               __alelisa?aldata:0,
                                                               ctData,casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate,
                                                               casesSets[setsi].cases[casei].factor,casesSets[setsi].factors,__alelisa);
                            titer=cFormulaRatio::calculateTiter(calt,casesSets[setsi].slope,casesSets[setsi].intercept,casesSets[setsi].cases[casei].factor,casesSets[setsi].factors);
                            if(titer>-9999)
                            {
                                if(titer<statsBase[0])
                                    statsBase[0]=titer;
                                if(titer>statsBase[1])
                                    statsBase[1]=titer;
                                statsBase[2]+=titer;
                                statsBase[3]+=titer?log10(titer):0;
                                statsValues[statsCount++]=titer;
                            }
                            else
                            {
                                statsValues[statsCount++]=0;
                            }
                        }

                        p2_datai+=deltasi;
                        replicatesi++;
                    }
                    datai++;
                }
                samplei+=deltasi;
            }
            int age=FXIntVal(casesSets[setsi].cases[casei].age->before('-'))*cat+FXIntVal(casesSets[setsi].cases[casei].age->after('-'));
            for(int i=0;i<BASELINE_PARCOUNT;i++)
            {
                if((tv[i][0]>-1) && (tv[i][1]>=tv[i][0]) && (tv[i][0]<=age) && (tv[i][1]>=age))
                {
                    if(statsBase[0]==999999)
                        statsBase[0]=0;
                    if(tv[i][3]>statsBase[0])
                        tv[i][3]=statsBase[0];
                    if(tv[i][4]<statsBase[1])
                        tv[i][4]=statsBase[1];
                    tv[i][5]+=round(statsBase[2]/statsBase[4]);
                    double t=round(pow(10,statsBase[3]/statsBase[4]));
                    tv[i][6]+=t?log10(t):0;
                    cv[i]=(double*)realloc(cv[i],((int)tv[i][2]+1)*sizeof(double));
                    if(!cv[i])
                    {
                        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                        return false;
                    }
                    double sdv=0;
                    for(int ct=0;ct<statsBase[4];ct++)
                        sdv+=pow(statsValues[ct]-round(statsBase[2]/statsBase[4]),2);
                    sdv/=statsBase[4];
                    sdv=sqrt(sdv);
                    cv[i][(int)tv[i][2]]=sdv;
                    tv[i][2]++;
                }
            }

            statsCount=0;
            free(statsValues);

            p2_datai=1;
        }
        pd+=casesSets[setsi].platesCount;
        int mdi=datai%(96/__alelisa_multiplier);
        if(mdi)
            datai+=(96/__alelisa_multiplier)-mdi;
    }

    for(int i=0;i<BASELINE_PARCOUNT;i++)
    {
        if(tv[i][2])
        {
            tv[i][5]/=tv[i][2];
            tv[i][6]=pow(10,tv[i][6]/tv[i][2]);
            double mean=mType==1?tv[i][5]:tv[i][6];
            tv[i][7]=0;
            for(int ct=0;ct<tv[i][2];ct++)
                tv[i][7]+=cv[i][ct];
            tv[i][7]/=tv[i][2];
            //    tv[i][7]+=pow(cv[i][ct]-tv[i][5],2);
            //tv[i][7]=sqrt(tv[i][7]);

            if(cv[i])
                free(cv[i]);

            double step;
            switch(cType)
            {
                case 2:
                    table->setItemText(i,4,FXStringVal((int)round(mean)));
                    step=vstdev?tv[i][7]*vstdev:0;
                    table->setItemText(i,2,FXStringVal((int)round(step>mean?0:mean-step)));
                    table->setItemText(i,3,FXStringVal((int)round(mean+step)));
                    break;
                case 3:
                    table->setItemText(i,4,FXStringVal((int)round(mean)));
                    step=vpercent?mean*(vpercent/100):0;
                    table->setItemText(i,2,FXStringVal((int)round(step>mean?0:mean-step)));
                    table->setItemText(i,3,FXStringVal((int)round(mean+step)));
                    break;
                default:
                    table->setItemText(i,2,FXStringVal((int)round(tv[i][3])));
                    table->setItemText(i,3,FXStringVal((int)round(tv[i][4])));
                    table->setItemText(i,4,FXStringVal((int)round(mean)));
                    break;
            }
        }
        else
        {
            //table->setItemText(i,4,"");
            //table->setItemText(i,2,"");
            //table->setItemText(i,3,"");
        }
    }

    oApplicationManager->endWaitCursor();
    msg.hide();
    delete csList;
    delete sList;
    return true;
}

FXbool cMDIBaseline::askPrefs(void)
{
    cDLGBslCases *dlg=new cDLGBslCases(oWinMain);
    if(!dlg->execute(PLACEMENT_OWNER))
        return false;
    mType=dlg->agchoice;
    cType=dlg->fchoice;
    mcAm=dlg->bcAm->getCheck();
    vstdev=FXFloatVal(dlg->tfStdev->getText());
    vpercent=FXFloatVal(dlg->tfPercent->getText());
    return true;
}

void cMDIBaseline::openCases(FXMDIClient *prP, FXPopup *prPup,cSortList &prCases)
{
    if(!this->loadReadings(prCases) || !this->askPrefs() || !this->processReadings())
        return;
    updateData();
}

