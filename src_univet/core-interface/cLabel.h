#ifndef CLABEL_H
#define CLABEL_H

#include <fx.h>

class cLabel : public FXLabel
{
    FXDECLARE(cLabel);
    
    private:
    protected:
        cLabel();
    
    public:
        cLabel(FXComposite *p, const FXString &text, FXIcon *ic=0, FXuint opts=LABEL_NORMAL, FXint x=0, FXint y=0, FXint w=0, FXint h=0, FXint pl=DEFAULT_PAD, FXint pr=DEFAULT_PAD, FXint pt=DEFAULT_PAD, FXint pb=DEFAULT_PAD);
        ~cLabel();
        
        long onPaint(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
