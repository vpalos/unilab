#include "engine.h"
#include "graphics.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cDLGReportTC.h"

FXDEFMAP(cDLGReportTC) mapDLGReportTC[]=
{
    FXMAPFUNC(SEL_COMMAND,cDLGReportTC::CMD_BUT_APPLY,cDLGReportTC::onCmdApply),
};

FXIMPLEMENT(cDLGReportTC,FXDialogBox,mapDLGReportTC,ARRAYNUMBER(mapDLGReportTC))

cDLGReportTC::cDLGReportTC(FXWindow *prOwner) :
    FXDialogBox(prOwner,oLanguage->getText("str_report_tc")+"...",DECOR_TITLE|DECOR_BORDER),stype(schoice)
{
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL);
        schoice=1;
        FXLabel *_lb1=new FXLabel(_vframe0,oLanguage->getText("str_report_stype"));
            _lb1->setTextColor(FXRGB(0,0,200));
        FXGroupBox *_gb1=new FXGroupBox(_vframe0,(char*)NULL,GROUPBOX_NORMAL,0,0,0,0,17,0,0,0);
            bAssay=new FXRadioButton(_gb1,oLanguage->getText("str_report_sa"),&stype,FXDataTarget::ID_OPTION+1);
            bAssay->setTextColor(FXRGB(120,120,120));
            bCase=new FXRadioButton(_gb1,oLanguage->getText("str_report_sc"),&stype,FXDataTarget::ID_OPTION+2);
            bCase->setTextColor(FXRGB(120,120,120));
        new FXHorizontalSeparator(_vframe0);
    
        bcAm=new FXCheckButton(_vframe0,oLanguage->getText("str_rdgmdi_mean"));
        bcAm->setCheck(true);
        bcAm->setTextColor(FXRGB(0,0,200));
        new FXHorizontalSeparator(_vframe0);
    
        FXHorizontalFrame *_hframe100=new FXHorizontalFrame(_vframe0,LAYOUT_CENTER_X);
        butCancel=new FXButton(_hframe100,oLanguage->getText("str_but_cancel"),NULL,this,ID_CANCEL,BUTTON_DEFAULT|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
        butAccept=new FXButton(_hframe100,oLanguage->getText("str_but_ok"),NULL,this,CMD_BUT_APPLY,BUTTON_DEFAULT|BUTTON_INITIAL|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
}

cDLGReportTC::~cDLGReportTC()
{
}

long cDLGReportTC::onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData)
{
    handle(NULL,FXSEL(SEL_COMMAND,ID_ACCEPT),NULL);
    return 1;
}

