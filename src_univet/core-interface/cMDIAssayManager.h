#ifndef CMDIASSAYMANAGER_H
#define CMDIASSAYMANAGER_H

#include <fx.h>

#include "cSortList.h" 
#include "cMDIChild.h" 

class cMDIAssayManager : public cMDIChild
{
    FXDECLARE(cMDIAssayManager);
    
    private:
        cSortList *asyList;
        FXMDIClient *client; 
        FXPopup *popup;
    
        void update(void);
    protected:
        cMDIAssayManager();
        
    public:
        cMDIAssayManager(FXMDIClient *prP, const FXString &prName, FXIcon *prIc=NULL, FXPopup *prPup=NULL, FXuint prOpts=0, FXint prX=0, FXint prY=0, FXint prW=0, FXint prH=0);
        virtual ~cMDIAssayManager();
        
        virtual void create();
        
        static void load(FXMDIClient *prP,FXPopup *prMenu);
        static FXbool isLoaded(void);
        static void unload(void);
        static void cmdRefresh(void);
        
        enum
        {
            ID_ASSAYMANAGER=FXMDIChild::ID_LAST,
            ID_ASSAYLIST,
            
            CMD_BUT_NEW,
            CMD_BUT_OPEN,
            CMD_BUT_DUPLICATE,
            CMD_BUT_REMOVE,
            CMD_BUT_CLOSE,
            
            ID_LAST
        };

        long onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdNew(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdOpen(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdDuplicate(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdRemove(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif

