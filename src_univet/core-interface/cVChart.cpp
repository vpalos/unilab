#include "engine.h"
#include "cLanguage.h"
#include "cVChart.h"
#include "cColorsManager.h"
#include "cDCNativePrinter.h"
#include <math.h>

#define CVCHART_AXIS_MIN_TICK 2
#define CVCHART_AXIS_MAJ_TICK 5
#define CVCHART_BORDER_SPACING 10
#define CVCHART_SPACING 3
#define CVCHART_CLUSTER_SP 10

// Must be 0. Set to 1 ONLY for development purposes
#define __ONLY_BW 0

FXDEFMAP(cVChart) mapVChart[]=
{
    FXMAPFUNC(SEL_MOUSEWHEEL,0,cVChart::onEvtRedir),
    FXMAPFUNC(SEL_PAINT,0,cVChart::onPaint),
};

FXIMPLEMENT(cVChart,FXFrame,mapVChart,ARRAYNUMBER(mapVChart))

cVChart::cVChart()
{
}

FXString cVChart::sampleVal(double prValue)
{
    return FXStringFormat("%g",round(prValue*1000)/1000);
}

void cVChart::fillRectPattern(FXDC &prDC,int x,int y,int w,int h,FXColor prColor)
{
    unsigned long r,g,b;
    r=FXREDVAL(prColor);
    g=FXGREENVAL(prColor);
    b=FXBLUEVAL(prColor);

    FXColor fg,bg;
    bg=FXRGB(0,0,0);
    fg=FXRGB(255,255,255);
    int isr=1,isb=1,isg=1;

    isr=r%56;
    isr=3*(!isr?1:isr)/56;

    isg=g%56;
    isg=7*(!isg?1:isg)/56;

    isb=b%56;
    isb=5*(!isb?1:isb)/56;

    //isr=3-isr;
    //isg=7-isg;
    //isb=5-isb;

    isr=isr<1?1:isr;
    isg=isg<1?1:isg;
    isb=isb<1?1:isb;

    int cx=prDC.getClipX(),cy=prDC.getClipY(),cw=prDC.getClipWidth(),ch=prDC.getClipHeight();
    prDC.setClipRectangle(x,y,w,h);
    prDC.setForeground(bg);
    prDC.fillRectangle(x,y,w,h);
    prDC.setForeground(fg);

    if((g+b)<r) // rects
    {
        int bc=0,br=0;
        while(br<h)
        {
            prDC.fillRectangle(x+bc,y+br,isb,isg);
            bc+=isr+isb;
            if(bc>w)
            {
                bc=-(w-bc%w);
                br+=isr+isg;
            }
        }
    }
    else if((r+b)<g) // angled crosses
    {
        int bc=0,br=0;
        while(br<h)
        {
            prDC.drawLine(x+bc,y+br,x+bc+isb,y+br+isg);
            prDC.drawLine(x+bc,y+br+isg,x+bc+isb,y+br);

            bc+=isr+isb;
            if(bc>w)
            {
                bc=-(w-bc%w);
                br+=isr+isg;
            }
        }
    }
    else // straight crosses
    {
        int bc=0,br=0;
        while(br<h)
        {
            prDC.drawLine(x+bc+isb/2,y+br,x+bc+isb/2,y+br+isg);
            prDC.drawLine(x+bc,y+br+isg/2,x+bc+isb,y+br+isg/2);

            bc+=isr+isb;
            if(bc>w)
            {
                bc=-(w-bc%w);
                br+=isr+isg;
            }
        }
    }
    prDC.setClipRectangle(cx,cy,cw,ch);
}

bool cVChart::compute(FXDC &prDC)
{
    dataCount=values->no();
    if(!dataCount)
        return false;

    dataMin=0;
    dataMax=(int)((sVChartData*)values->find("0"))->value;

    sVChartData *val;

    for(int i=0;i<values->no();i++)
    {
        val=((sVChartData*)values->find(FXStringFormat("%d",i).text()));
        if(val->value<0 && val->value!=-1000)
            val->value=0;
        if(val->value>dataMax)
            dataMax=(int)ceil(val->value);
    }

    //if(!dataMax && !dataMin)
        //return false;

    unit=1;
    if(!dataMax)
        dataMax=1;
    for(int n=dataMax;n;n/=10,unit*=10);
    unit/=10;
    if(dataMax%unit)
        dataMax=dataMax-(dataMax%unit)+unit;
    if(dataMin%unit || dataMin==dataMax)
        dataMin-=dataMax%unit;
    if(fontValues)
        delete fontValues;
    if(fontTitle)
        delete fontTitle;
    if(fontAxis)
    {
        delete fontAxis;
        //delete fontVAxis;
    }

    int repeat=1;
    while(repeat)
    {
        int dw=(int)sqrt((width*height)/((double)dataCount*1.4));
        if(dw>100)dw=100;
        if(dw<40)dw=40;
        fontValues=new FXFont(oApplicationManager,"Helvetica",(int)(dw/10),FXFont::Normal);
        dw=(width-(CVCHART_BORDER_SPACING>>1))/10;
        if(dw>70)dw=70;
        if(dw<50)dw=50;
        fontTitle=new FXFont(oApplicationManager,"Helvetica",(int)(dw*1.9/10),FXFont::Bold);
        fontAxis=new FXFont(oApplicationManager,"Helvetica",(int)(dw*1.3/10),FXFont::Bold);
        //fontVAxis=new FXFont(oApplicationManager,"Arial Black",(int)(dw*1.7/10),FXFont::Bold,FXFont::Straight,FONTENCODING_DEFAULT,FXFont::Expanded,FXFont::Rotatable);
        //fontVAxis->setAngle(90*64);
        fontValues->create();
        fontAxis->create();
        //fontVAxis->create();
        fontTitle->create();

        th=title.empty()?0:fontTitle->getTextHeight(title);
        vah=fontAxis->getTextHeight(vaxis);
        hah=fontAxis->getTextHeight(haxis);
        vw=fontValues->getTextWidth(FXStringVal(dataMax));
        vh=fontValues->getTextHeight(FXStringVal(dataMax));
        lvh=fontLegend->getTextHeight(FXStringVal(dataMax));

        dataW=width-(border<<1)-(CVCHART_BORDER_SPACING*3)-vw-vah-CVCHART_AXIS_MAJ_TICK-CVCHART_SPACING;
        lh=0;
        if((showLegend && legend->no()))
        {
            int lw=0,k,yi=0;
            lh=CVCHART_SPACING*5;
            rows=0;
            for(int i=0;i<20;i++)tn[i]=0;
            for(int i=0;i<legend->no();i++,tn[rows]++,yi++)
            {
                FXString s=legend->find(FXStringFormat("%d",yi).text());
                s=s.before('\n');
                k=lvh+(CVCHART_SPACING*3)+fontLegend->getTextWidth(s);
                if((lw+k)>dataW && lw && rows<19)
                {
                    tw[rows++]=lw;
                    lw=k;
                }
                else
                    lw+=k;
            }
            tw[rows++]=lw;
            lh+=rows*(int)((double)lvh*1.4);
        }

        dataX=border+(CVCHART_BORDER_SPACING<<1)+vw+vah+CVCHART_AXIS_MAJ_TICK+CVCHART_SPACING;
        dataY=border+(CVCHART_BORDER_SPACING<<1)+(CVCHART_SPACING*6)+th;
        dataH=height-(border<<1)-(CVCHART_BORDER_SPACING*3)-lh-vh-hah-th-CVCHART_AXIS_MAJ_TICK-(CVCHART_SPACING*6);
        repeat=0;
        while((dataH/dataCount)>60)
        {
            dataCount++;
            repeat=1;
        }
    }

    unitPixels=(FXdouble)dataH/((FXdouble)(dataMax-dataMin)/(FXdouble)unit);
    elementPixels=unitPixels/unit;
    clusterCount=dataCount/setCount;
    dataPixels=(FXdouble)dataW/(FXdouble)clusterCount;
    space=(FXdouble)dataPixels/CVCHART_CLUSTER_SP;
    clusterPixels=(FXdouble)dataPixels-space*2;
    valPixels=clusterPixels/(FXdouble)setCount;
    return true;
}

void cVChart::drawCanvas(FXDC &prDC)
{
    prDC.setForeground(canvasColor);
    prDC.fillRectangle(0,0,width,height);
}

void cVChart::drawData(FXDC &prDC)
{
    int di=0,n,dit=0,rsc=dataCount/setCount;
    double m,l,r,g,b,dr,dg,db,tr,tg,tb;
    FXString s;
    FXColor c,d;
    sVChartData *val;
    bool stay=true;
    prDC.setFont(fontValues);
    switch(type)
    {
        case CHART_POINT:
            for(FXdouble i=(FXdouble)dataX;i<=(FXdouble)(dataX+dataW);i+=dataPixels,dit++)
            {
                di=dit;
                m=i+space;
                for(int j=0;j<setCount;j++,di+=rsc,m+=valPixels)
                    if(di<dataCount)
                    {
                        val=(sVChartData*)values->find(FXStringFormat("%d",di).text());
                        if(!val)
                        {
                            stay=false;
                            break;
                        }
                        l=val->value;
                        if(l==-1000)
                        {
                            prDC.setForeground(gridColor);
                            prDC.fillRectangle((int)round(m),dataY,(int)round(round(m+valPixels)-m),dataH);
                            //prDC.setForeground(valuesColor);
                            //prDC.drawRectangle((int)round(m),dataY,(int)round(round(m+valPixels)-m),dataH);
                            continue;
                        }
                        n=(int)round(l*elementPixels);
                        s=sampleVal(l);
                        if(val->color==0)
                            c=dataColor;
                        else
                            c=val->color;
                        prDC.setForeground(c);
                        prDC.fillRectangle((int)round(m)+((int)valPixels>>1)-1,dataY+dataH-n-2,5,5);
                    }
                if(!stay)
                    break;
            }
            break;
        case CHART_LINE:
            b=dataPixels/2;
            for(int j=0;j<setCount;j++,dit+=rsc)
            {
                r=0;
                g=0;
                di=0;
                for(FXdouble i=(FXdouble)dataX;i<=(FXdouble)(dataX+dataW);i+=dataPixels,di++)
                {
                    m=b+i;
                    if(di<rsc)
                    {
                        val=((sVChartData*)values->find(FXStringFormat("%d",dit+di).text()));
                        if(!val)
                        {
                            stay=false;
                            break;
                        }
                        l=val->value;
                        if(l==-1000)
                        {
                            prDC.setForeground(gridColor);
                            prDC.fillRectangle((int)round(m-b+space),dataY,(int)round(round(m+valPixels)-m),dataH);
                            //prDC.setForeground(valuesColor);
                            //prDC.drawRectangle((int)round(m-b+space),dataY,(int)round(round(m+valPixels)-m),dataH);
                            continue;
                        }
                        n=(int)round(l*elementPixels);
                        s=sampleVal(l);
                        tr=(int)round(m);
                        tg=dataY+dataH-n;
                        if(val->color==0)
                            c=dataColor;
                        else
                            c=val->color;
                        prDC.setForeground(c);
                        if(r>0)
                            prDC.drawLine((int)r+1,(int)g+2,(int)tr+1,(int)tg+2);
                        prDC.fillRectangle((int)tr-1,(int)tg-2,5,5);
                        r=tr;
                        g=tg;
                    }
                }
                if(!stay)
                    break;
            }
            break;
        default:
            for(FXdouble i=(FXdouble)dataX;i<=(FXdouble)(dataX+dataW);i+=dataPixels,dit++)
            {
                di=dit;
                m=i+space;
                for(int j=0;j<setCount;j++,di+=rsc,m+=valPixels)
                    if(dit<rsc)
                    {
                        val=((sVChartData*)values->find(FXStringFormat("%d",di).text()));
                        if(!val)
                        {
                            stay=false;
                            break;
                        }
                        l=val->value;
                        if(l==-1000)
                        {
                            prDC.setForeground(gridColor);
                            prDC.fillRectangle((int)round(m),dataY,(int)round(round(m+valPixels)-m),dataH);
                            //prDC.setForeground(valuesColor);
                            //prDC.drawRectangle((int)round(m),dataY,(int)round(round(m+valPixels)-m),dataH);
                            continue;
                        }
                        n=(int)round(l*elementPixels);
                        s=sampleVal(l);
                        if(val->color==0)
                            c=dataColor;
                        else
                            c=val->color;
                        r=FXREDVAL(c);
                        g=FXGREENVAL(c);
                        b=FXBLUEVAL(c);
                        d=FXRGB(r+120>255?255:r+120,
                                g+120>255?255:g+120,
                                b+120>255?255:b+120);
                        tr=FXREDVAL(d)-r;
                        tg=FXGREENVAL(d)-g;
                        tb=FXBLUEVAL(d)-b;
                        dr=tr/(double)n;
                        dg=tg/(double)n;
                        db=tb/(double)n;
                        r=FXREDVAL(d);
                        g=FXGREENVAL(d);
                        b=FXBLUEVAL(d);

                        if(__ONLY_BW || (isprint && ((cDCNativePrinter*)&prDC)->isColor()==false))
                            this->fillRectPattern(prDC,(int)round(m),dataY+dataH-n,(int)round(round(m+valPixels)-m),n+1,c);
                        else
                        {
                            for(int u=n;u>0;u--)
                            {
                                r-=dr;
                                g-=dg;
                                b-=db;
                                prDC.setForeground(FXRGB(r,g,b));
                                prDC.fillRectangle((int)round(m),dataY+dataH-u,(int)round(round(m+valPixels)-m),u+1);
                            }
                        }

                        prDC.setForeground(borderColor);
                        //prDC.drawRectangle((int)round(m),dataY+dataH-n,(int)round(round(m+valPixels)-m),n+1);
                        int h1=dataY+dataH-n,h2=dataY+dataH+1;
                        prDC.drawLine((int)round(m),h1,(int)round(m+valPixels),h1);
                        prDC.drawLine((int)round(m),h1,(int)round(m),h2);
                        prDC.drawLine((int)round(m+valPixels),h1,(int)round(m+valPixels),h2);
                    }
                if(!stay)
                    break;
            }
            break;
    }

    prDC.setForeground(borderColor);
    prDC.drawLine(dataX-1-CVCHART_AXIS_MAJ_TICK,dataY+dataH+1,dataX+dataW,dataY+dataH+1);

    if(showLegend && legend->no() && (((setCount<=legend->no()) && (setCount<=dataCount)) || (setCount==1)))
    {
        int lm=setCount==1?1:rsc,yi=0;
        FXString s;
        prDC.setFont(fontLegend);
        stay=true;

        for(int j=0;j<rows;j++)
        {
            int x=dataX+(dataW>>1)-(tw[j]>>1),opm=0;
            int y=dataY+dataH+CVCHART_AXIS_MAJ_TICK+(CVCHART_SPACING*3)+CVCHART_BORDER_SPACING+(lvh<<1)+hah+(j*(int)((double)lvh*1.4));
            for(int i=0;i<tn[j];i++,yi++)
            {
                FXColor flclr=0;
                s=legend->find(FXStringFormat("%d",yi).text());
                if(!s.after('\n').empty())
                {
                    FXString rs=s.after('\n');
                    s=s.before('\n');
                    int r,g,b;
                    r=FXIntVal(rs.before('\t'));
                    rs=rs.after('\t');
                    g=FXIntVal(rs.before('\t'));
                    rs=rs.after('\t');
                    b=FXIntVal(rs);
                    flclr=FXRGB(r,g,b);
                    opm++;
                }
                val=((sVChartData*)values->find(FXStringFormat("%d",(yi-opm)*lm).text()));
                if(!val && s.empty())
                {
                    stay=false;
                    break;
                }
                if(flclr!=0)
                    c=flclr;
                else if(!val || (val->color==0))
                    c=dataColor;
                else
                    c=val->color;
                r=FXREDVAL(c);
                g=FXGREENVAL(c);
                b=FXBLUEVAL(c);
                d=FXRGB(r+150>255?255:r+150,
                        g+150>255?255:g+150,
                        b+150>255?255:b+150);
                tr=FXREDVAL(d)-r;
                tg=FXGREENVAL(d)-g;
                tb=FXBLUEVAL(d)-b;
                dr=tr/(double)lvh;
                dg=tg/(double)lvh;
                db=tb/(double)lvh;
                r=FXREDVAL(d);
                g=FXGREENVAL(d);
                b=FXBLUEVAL(d);
                if(__ONLY_BW || (isprint && ((cDCNativePrinter*)&prDC)->isColor()==false))
                    this->fillRectPattern(prDC,(int)round(x),y-lvh,lvh,lvh,c);
                else
                {
                    for(int u=lvh;u>0;u--)
                    {
                        r-=dr;
                        g-=dg;
                        b-=db;
                        prDC.setForeground(FXRGB(r,g,b));
                        prDC.fillRectangle((int)round(x),y-u,lvh,u);
                    }
                }
                prDC.setForeground(borderColor);
                prDC.drawRectangle((int)round(x),y-lvh,lvh,lvh);
                x+=lvh+CVCHART_SPACING;
                prDC.setForeground(valuesColor);
                prDC.drawText(x,y-(int)(lvh*0.1),s.text(),s.length());
                x+=fontLegend->getTextWidth(s)+(CVCHART_SPACING<<1);
            }
            if(!stay)
                break;
        }
    }

    if(showValues)
    {
        int w,h;
        double tp=0,rtp=-100;
        di=0;
        dit=0;
        stay=true;
        prDC.setFont(fontValues);
        for(FXdouble i=(FXdouble)dataX;i<=(FXdouble)(dataX+dataW+1);i+=dataPixels,dit++)
        {
            di=dit;
            m=i+(type==CHART_LINE?dataPixels/2:space);
            for(int j=0;j<setCount;j++,di+=rsc,m+=(type==CHART_LINE?0:valPixels),tp+=valPixels)
            {
                if(dit<rsc)
                {
                    val=((sVChartData*)values->find(FXStringFormat("%d",di).text()));
                    if(!val)
                    {
                        stay=false;
                        break;
                    }
                    l=val->value;

                    n=(int)round(l*elementPixels)+2;
                    if(l==-1000)
                    {
                        s=val->sValue.upper();
                        n=2;
                    }
                    else
                        s=sampleVal(l);
                    w=fontValues->getTextWidth(s);
                    if((l==-1000) || (w+(CVCHART_SPACING<<1))<=((tp-rtp)*1.7))
                    {
                        h=fontValues->getTextHeight(s);
                        if(val->color==0)
                            c=dataColor;
                        else
                            c=val->color;
                        /*prDC.setForeground(canvasColor);
                        prDC.fillRectangle((int)round(m)+(type==CHART_LINE?0:(int)valPixels>>1)-(w>>1)-CVCHART_SPACING,
                                           dataY+dataH-n-h-(CVCHART_SPACING),w+(CVCHART_SPACING<<1),h+(CVCHART_SPACING));
                        prDC.setForeground(gridColor);
                        prDC.drawRectangle((int)round(m)+(type==CHART_LINE?0:(int)valPixels>>1)-(w>>1)-CVCHART_SPACING,
                                           dataY+dataH-n-h-(CVCHART_SPACING),w+(CVCHART_SPACING<<1),h+(CVCHART_SPACING));*/
                        prDC.setForeground(l==-1000?FXRGB(200,0,0):valuesColor);
                        if(l==-1000)
                        {
                            FXString ss;
                            int dvah=(int)(vh/1.1);
                            int y=dataY+(dataH>>1)-((s.length()*dvah)>>1)+dvah;
                            for(int qi=0;qi<s.length();qi++,y+=dvah)
                            {
                                ss=s.mid(qi,1);
                                prDC.drawText((int)round(m-(type==CHART_LINE?dataPixels/2-space:0))+((int)valPixels>>1)-(fontValues->getTextWidth(ss)>>1),y,ss.text(),1);
                            }
                        }
                        else
                        {
                            FXString sv=val->sValue;
                            bool bsvn=!sv.after('\n').empty();
                            if(bsvn)
                            {
                                FXString rs=sv.after('\n');
                                sv=sv.before('\n');
                                int r,g,b;
                                r=FXIntVal(rs.before('\t'));
                                rs=rs.after('\t');
                                g=FXIntVal(rs.before('\t'));
                                rs=rs.after('\t');
                                b=FXIntVal(rs);

                                prDC.setForeground(FXRGB(r,g,b));
                                if(__ONLY_BW || (isprint && ((cDCNativePrinter*)&prDC)->isColor()==false))
                                    this->fillRectPattern(prDC,(int)round(m)+(type==CHART_LINE?0:(int)valPixels>>1)-(int)(valPixels/2)+1,
                                                          dataY+dataH-n-CVCHART_SPACING-vh+1,
                                                          (int)valPixels-1,(int)vh+2,FXRGB(r,g,b));
                                else
                                {
                                    prDC.fillRectangle((int)round(m)+(type==CHART_LINE?0:(int)valPixels>>1)-(int)(valPixels/2)+1,
                                                       dataY+dataH-n-CVCHART_SPACING-vh+1,
                                                       (int)valPixels-1,(int)vh+2);
                                }

                                prDC.setForeground(valuesColor);
                                FXuint olw=prDC.getLineWidth();
                                prDC.setLineWidth(1);
                                prDC.drawRectangle((int)round(m)+(type==CHART_LINE?0:(int)valPixels>>1)-(int)(valPixels/2)+1,
                                                   dataY+dataH-n-CVCHART_SPACING-vh+1,
                                                   (int)valPixels-1,(int)vh+2);
                                prDC.setLineWidth(olw);
                            }
                            prDC.drawText((int)round(m)+(type==CHART_LINE?0:(int)valPixels>>1)-(w>>1)+1,dataY+dataH-n-CVCHART_SPACING-
                                          ((bsvn && (__ONLY_BW || (isprint && ((cDCNativePrinter*)&prDC)->isColor()==false)))*fontValues->getTextHeight(s.text(),s.length())),s.text(),s.length());
                        }
                        rtp=tp;
                    }
                }
            }
            if(!stay)
                break;
            tp+=space*2;
        }
    }
}

void cVChart::drawAxes(FXDC &prDC)
{
    double r,g,b,rsc=dataCount/setCount;

    r=FXREDVAL(canvasColor)-3;
    g=FXGREENVAL(canvasColor)-3;
    b=FXBLUEVAL(canvasColor)-3;
    prDC.setForeground(FXRGB(r,g,b));
    prDC.fillRectangle(dataX,dataY,dataW,dataH);

    prDC.setForeground(borderColor);
    prDC.drawLine(dataX-1,dataY,dataX-1,dataY+dataH+CVCHART_AXIS_MAJ_TICK+1);
    //prDC.drawLine(dataX-1-CVCHART_AXIS_MAJ_TICK,dataY+dataH+1,dataX+dataW,dataY+dataH+1);

    if(showVGrid)
    {
        prDC.setForeground(gridColor);
        prDC.drawLine(dataX+dataW,dataY,dataX+dataW,dataY+dataH);
    }
    prDC.setFont(fontValues);
    FXString s="0";
    FXint t=dataMin+unit,w=fontValues->getTextWidth(s),h=(int)floor(vh/2.8);
    prDC.setForeground(borderColor);
    prDC.drawText(dataX-1-CVCHART_AXIS_MAJ_TICK-CVCHART_SPACING-w,dataY+dataH+h,s.text(),s.length());

    for(FXdouble i=(FXdouble)(dataY+dataH-unitPixels);i>=(FXdouble)dataY-1;i-=unitPixels,t+=unit)
    {
        s=FXStringFormat("%d",t);
        w=fontValues->getTextWidth(s);
        prDC.setForeground(borderColor);
        prDC.drawLine(dataX-1,(int)i,dataX-1-CVCHART_AXIS_MAJ_TICK,(int)i);
        prDC.drawText(dataX-1-CVCHART_AXIS_MAJ_TICK-CVCHART_SPACING-w,(int)i+h,s.text(),s.length());
        if(showVGrid)
        {
            prDC.setForeground(gridColor);
            prDC.drawLine(dataX,(int)i,dataX+dataW,(int)i);
        }
    }
    for(FXdouble i=(FXdouble)dataX+dataPixels;i<=(FXdouble)(dataX+dataW+1);i+=dataPixels)
    {
        prDC.setForeground(borderColor);
        prDC.drawLine((int)round(i),dataY+dataH+1,(int)round(i),dataY+dataH+1+CVCHART_AXIS_MAJ_TICK);
        if(type!=CHART_BAR)
        {
            prDC.setForeground(gridColor);
            prDC.drawLine((int)round(i),dataY+dataH,(int)round(i),dataY);
        }
    }

    int di=0;
    prDC.setForeground(valuesColor);
    for(FXdouble i=(FXdouble)dataX+(dataPixels/2);i<=(FXdouble)(dataX+dataW+1);i+=dataPixels,di++)
    {
        if(di<rsc)
        {
            prDC.drawLine((int)round(i),dataY+dataH+2,(int)round(i),dataY+dataH+1+CVCHART_AXIS_MIN_TICK);
            if((sVChartData*)values->find(FXStringFormat("%d",di).text())!=NULL)
            {
                s=((sVChartData*)values->find(FXStringFormat("%d",di).text()))->name;
                if(!s.after('\n').empty())
                {
                    FXString rs=s.after('\n');
                    s=s.before('\n');
                    int r,g,b;
                    r=FXIntVal(rs.before('\t'));
                    rs=rs.after('\t');
                    g=FXIntVal(rs.before('\t'));
                    rs=rs.after('\t');
                    b=FXIntVal(rs);
                    prDC.setForeground(FXRGB(r,g,b));
                    prDC.fillRectangle((int)round(i)-(int)(dataPixels/2)+2,(int)dataY+dataH+1+CVCHART_AXIS_MIN_TICK+1,
                                       (int)dataPixels-4,(int)vh+2);
                    prDC.setForeground(valuesColor);
                }
                prDC.drawText((int)round(i)-(fontValues->getTextWidth(s)>>1),dataY+dataH+1+CVCHART_AXIS_MIN_TICK+vh,s.text(),s.length());
            }
        }
    }

    prDC.setForeground(borderColor);
    if(!title.empty())
    {
        prDC.setFont(fontTitle);
        prDC.drawText((width>>1)-(fontTitle->getTextWidth(title)>>1)+(int)(width*0.025),border+CVCHART_BORDER_SPACING+(CVCHART_SPACING*3)+(int)(th/1.2),title.text(),title.length());
    }

    if(!haxis.empty())
    {
        prDC.setFont(fontAxis);
        prDC.drawText(dataX+(dataW>>1)-(fontAxis->getTextWidth(haxis)>>1),dataY+dataH+CVCHART_AXIS_MAJ_TICK+(CVCHART_SPACING<<1)+vh+fontAxis->getTextHeight(haxis),haxis.text(),haxis.length());
    }

    /*if(!vaxis.empty())
    {
        prDC.setFont(fontVAxis);
        prDC.drawText(border+CVCHART_BORDER_SPACING+(int)round(vah/1.2),dataY+(dataH>>1)+(fontVAxis->getTextWidth(vaxis)>>1),
                      vaxis.text(),vaxis.length());
    }*/

    if(!vaxis.empty())
    {
        prDC.setFont(fontAxis);
        int dvah=(int)(vah/1.1);
        int y=dataY+(dataH>>1)-((vaxis.length()*dvah)>>1)+dvah;
        for(int i=0;i<vaxis.length();i++,y+=dvah)
        {
            s=vaxis.mid(i,1);
            prDC.drawText(border+CVCHART_BORDER_SPACING+(vah>>1)-(fontAxis->getTextWidth(s)>>1),y,s.text(),1);
        }
    }
}

cVChart::cVChart(FXComposite *prP, FXuint prType, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH)
    :FXFrame(prP,prOpts,prX,prY,prW,prH,0,0,0,0)
{
    setTarget(prP);
    enable();
    type=prType;

    title="";
    haxis="";
    vaxis="";

    canvasColor=FXRGB(255,255,255);
    borderColor=FXRGB(0,0,0);
    gridColor=FXRGB(190,190,190);
    valuesColor=FXRGB(100,100,100);
    dataColor=FXRGB(0,100,0);

    showValues=true;
    showVGrid=true;
    showLegend=true;

    doubleBuffering=false;
    values=new FXDict();
    legend=new FXStringDict();
    fontValues=NULL;
    fontTitle=NULL;
    fontAxis=NULL;
    //fontVAxis=NULL;
    fontLegend=new FXFont(oApplicationManager,"helvetica",7,FXFont::Bold);
    fontLegend->create();

    setCount=1;
    isprint=false;
}

cVChart::~cVChart()
{
    delete values;
}

void cVChart::create()
{
    FXFrame::create();
}

FXString cVChart::getTitle(void)
{
    return title;
}

FXString cVChart::getHAxisTitle(void)
{
    return haxis;
}

FXString cVChart::getVAxisTitle(void)
{
    return vaxis;
}

void cVChart::setTitle(FXString prText)
{
    title=prText;
}

void cVChart::setHAxisTitle(FXString prText)
{
    haxis=prText;
}

void cVChart::setVAxisTitle(FXString prText)
{
    vaxis=prText;
}

FXuint cVChart::getType(void)
{
    return type;
}

void cVChart::setType(FXuint prType)
{
    type=prType;
}

FXDict *cVChart::dataSet(void)
{
    return values;
}

FXStringDict *cVChart::legendStrings(void)
{
    return legend;
}

FXColor cVChart::getCanvasColor(void)
{
    return canvasColor;
}

FXColor cVChart::getAxesColor(void)
{
    return borderColor;
}

FXColor cVChart::getGridColor(void)
{
    return gridColor;
}

FXColor cVChart::getDataColor(void)
{
    return dataColor;
}

FXColor cVChart::getValuesColor(void)
{
    return valuesColor;
}

void cVChart::setCanvasColor(FXColor prColor)
{
    canvasColor=prColor;
}

void cVChart::setAxesColor(FXColor prColor)
{
    borderColor=prColor;
}

void cVChart::setGridColor(FXColor prColor)
{
    gridColor=prColor;
}

void cVChart::setDataColor(FXColor prColor)
{
    dataColor=prColor;
}

void cVChart::setValuesColor(FXColor prColor)
{
    valuesColor=prColor;
}

FXbool cVChart::getDoubleBuffering(void)
{
    return doubleBuffering;
}

FXbool cVChart::getShowValues(void)
{
    return showValues;
}

FXbool cVChart::getShowGrid(void)
{
    return showVGrid;
}

FXbool cVChart::getShowLegend(void)
{
    return showLegend;
}

void cVChart::setDoubleBuffering(FXbool prFlag)
{
    doubleBuffering=prFlag;
}

void cVChart::setShowValues(FXbool prFlag)
{
    showValues=prFlag;
}

void cVChart::setShowGrid(FXbool prFlag)
{
    showVGrid=prFlag;
}

void cVChart::setShowLegend(FXbool prFlag)
{
    showLegend=prFlag;
}

FXint cVChart::getClusterCount(void)
{
    return setCount;
}

void cVChart::setClusterCount(FXint prCount)
{
    setCount=prCount<=0?1:prCount;
}

long cVChart::onEvtRedir(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXObject *target=getTarget();
    if(!target)
        return 1;
    return target->tryHandle(prSender,prSelector,prData);
}

long cVChart::onPaint(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXEvent *event=(FXEvent*)prData;
    FXDC *rdc;

    if(prSender==NULL && prSelector==0)
    {
        isprint=true;
        rdc=(FXDC*)prData;
        rdc->drawRectangle(10,10,100,100);
        rdc->fillRectangle(40,40,60,60);
    }
    else
    {
        isprint=false;
        rdc=new FXDCWindow(this,event);
    }

    if(width<200 || height<120)
        return 1;

    if(!compute(*rdc))
    {
        drawCanvas(*rdc);
        FXFont *ft=new FXFont(oApplicationManager,"helvetica",7);
        ft->create();
        rdc->setFont(ft);
        rdc->setForeground(FXRGB(255,0,0));
        rdc->drawText(10,20,title.text(),title.length());
        FXString msg=oLanguage->getText("str_invalid_data");
        rdc->drawText(10,35,msg.text(),msg.length());
        delete ft;
        if(rdc  && (prSender!=NULL || prSelector!=0))
            delete rdc;
        return 1;
    }

    /*if(doubleBuffering)
    {
    FXBMPImage *picture=new FXBMPImage(getApp(),NULL,IMAGE_SHMI|IMAGE_SHMP,width,height);
    picture->create();
    FXDC dc(picture);

    drawCanvas(dc);
    drawAxes(dc);
    drawData(dc);
    drawLegend(dc);

    rdc.drawImage(picture,0,0);
    delete picture;
    }
    else*/
    //{
        rdc->clearClipRectangle();
        drawCanvas(*rdc);
        drawAxes(*rdc);
        drawData(*rdc);
    //}
    if(rdc  && (prSender!=NULL || prSelector!=0))
        delete rdc;
    return 1;
}


