#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cGrowerManager.h"
#include "cWinMain.h"
#include "cMDIDatabaseManager.h"
#include "cMDIGrowerManager.h"

cMDIGrowerManager *oMDIGrowerManager=NULL;

FXDEFMAP(cMDIGrowerManager) mapMDIGrowerManager[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIGrowerManager::ID_GROWERMANAGER,cMDIGrowerManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIGrowerManager::CMD_BUT_CLOSE,cMDIGrowerManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIGrowerManager::CMD_BUT_OPEN,cMDIGrowerManager::onCmdOpen),
    FXMAPFUNC(SEL_COMMAND,cMDIGrowerManager::CMD_BUT_NEW,cMDIGrowerManager::onCmdNew),
    FXMAPFUNC(SEL_COMMAND,cMDIGrowerManager::CMD_BUT_DUPLICATE,cMDIGrowerManager::onCmdDuplicate),
    FXMAPFUNC(SEL_DOUBLECLICKED,cMDIGrowerManager::ID_GROWERLIST,cMDIGrowerManager::onCmdOpen),
    FXMAPFUNC(SEL_COMMAND,cMDIGrowerManager::CMD_BUT_REMOVE,cMDIGrowerManager::onCmdRemove),
    
};

FXIMPLEMENT(cMDIGrowerManager,cMDIChild,mapMDIGrowerManager,ARRAYNUMBER(mapMDIGrowerManager))

cMDIGrowerManager::cMDIGrowerManager()
{
}

cMDIGrowerManager::cMDIGrowerManager(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH) 
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_GROWERMANAGER);
    client=prP;
    popup=prPup;
    
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(this,LAYOUT_FILL);
    FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe0,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
    new FXLabel(_vframe2,oLanguage->getText("str_grwman_grwlist_title"));
    FXHorizontalFrame *_hframe1=new FXHorizontalFrame(_vframe2,LAYOUT_FILL,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe4=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|LAYOUT_RIGHT,0,0,0,0,0,0,0,0,0,0);
    
    grwList=new cSortList(_vframe3,this,ID_GROWERLIST,ICONLIST_SINGLESELECT|ICONLIST_DETAILED|LAYOUT_FILL);
    grwList->appendHeader(oLanguage->getText("str_grwman_grwnameh"),NULL,120);
    grwList->appendHeader(oLanguage->getText("str_grwman_grwtitleh"),NULL,264);
    oMDIGrowerManager=this;
    this->update();
    if(grwList->getNumItems()>0)
    {
        grwList->selectItem(0);
        grwList->setCurrentItem(0);
    }
    grwList->setFocus();

    new FXButton(_vframe4,oLanguage->getText("str_grwman_new"),NULL,this,CMD_BUT_NEW,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_FIX_WIDTH,0,0,88,0);
    new FXHorizontalSeparator(_vframe4,LAYOUT_TOP|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_grwman_open"),NULL,this,CMD_BUT_OPEN,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X);
    new FXButton(_vframe4,oLanguage->getText("str_grwman_duplicate"),NULL,this,CMD_BUT_DUPLICATE,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X);
    new FXButton(_vframe4,oLanguage->getText("str_but_close"),NULL,this,CMD_BUT_CLOSE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
    new FXHorizontalSeparator(_vframe4,LAYOUT_BOTTOM|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_grwman_remove"),NULL,this,CMD_BUT_REMOVE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
}

cMDIGrowerManager::~cMDIGrowerManager()
{
    cMDIGrowerManager::unload();
}

void cMDIGrowerManager::create()
{
    cMDIChild::create();
    show();
}

void cMDIGrowerManager::load(FXMDIClient *prP,FXPopup *prMenu)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(prP,prMenu);
        return;
    }
    
    if(oMDIGrowerManager!=NULL)
    {
        oMDIGrowerManager->setFocus();
        prP->setActiveChild(oMDIGrowerManager);
        if(oMDIGrowerManager->isMinimized())
            oMDIGrowerManager->restore();
        return;
    }
    new cMDIGrowerManager(prP,oLanguage->getText("str_grwman_title"),new FXGIFIcon(oApplicationManager,data_inputs),prMenu,MDI_NORMAL|MDI_TRACKING,10,10,515,200);
    oMDIGrowerManager->create();
    oMDIGrowerManager->setFocus();

}

FXbool cMDIGrowerManager::isLoaded(void)
{
    return (oMDIGrowerManager!=NULL);
}

void cMDIGrowerManager::unload(void)
{
    oMDIGrowerManager=NULL;
}

void cMDIGrowerManager::update(void)
{
    if(!isLoaded())
        return;
    sGrowerObject *res=cGrowerManager::listGrowers();
    if(res==NULL)
        return;
    grwList->clearItems();
    for(int i=0;i<cGrowerManager::getGrowerCount();i++)
    grwList->appendItem(*(res[i].id)+"\t"+(res[i].solid>0?"* ":"")+*(res[i].title));
    grwList->sortItems();
    free(res);
}

void cMDIGrowerManager::cmdRefresh(void)
{
    if(!isLoaded())
        return;
    oMDIGrowerManager->update();
}

long cMDIGrowerManager::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDIGrowerManager::unload();
    close();
    return 1;
}

long cMDIGrowerManager::onCmdNew(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cGrowerManager::newGrower(client,popup);
    return 1;
}

long cMDIGrowerManager::onCmdOpen(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int db=-1,i;
    for(i=0;i<grwList->getNumItems();i++)
        if(grwList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        db=grwList->getCurrentItem();
    if(db==-1)
        return 1;
    if(cGrowerManager::growerSolid(grwList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasegrwsolid").text());
        return 1;
    }
    FXString st=grwList->getItemText(db).before('\t');
    if(oWinMain->isWindow(st))
        oWinMain->raiseWindow(st);
    else
        cGrowerManager::editGrower(client,popup,st);
    return 1;
}

long cMDIGrowerManager::onCmdDuplicate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int db=-1,i,j;
    for(i=0;i<grwList->getNumItems();i++)
        if(grwList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        return 1;
    FXString res2=cGrowerManager::duplicateGrower(grwList->getItemText(db).before('\t').text());
    this->update();
    if(res2.empty())
        return 1;
    for(j=0;j<grwList->getNumItems();j++)
        if(res2==grwList->getItemText(j).before('\t'))
        {
            grwList->selectItem(j);
            break;
        }
    return 1;
}

long cMDIGrowerManager::onCmdRemove(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i,db=-1;
    for(i=0;i<grwList->getNumItems();i++)
        if(grwList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        return 1;
    if(cGrowerManager::growerSolid(grwList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasegrwsolid").text());
        return 1;
    }
    if(cGrowerManager::growerNeeded(grwList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasedataneeded").text());
        return 1;
    }
    if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_erasegrw").text()))
        return 1;
    cMDIChild *win=oWinMain->getWindow(grwList->getItemText(db).before('\t'));
    if(win!=NULL)
        win->close();
    cGrowerManager::removeGrower(grwList->getItemText(db).before('\t'));
    this->update();
    if(db>=grwList->getNumItems())
        db=grwList->getNumItems()-1;
    if(db>=0)
        grwList->selectItem(db);
    return 1;
}


