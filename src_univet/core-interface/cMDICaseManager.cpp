#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cCaseSelector.h"
#include "cAssayManager.h"
#include "cDLGCasePopEdit.h"
#include "cDateChooser.h"
#include "cWinMain.h"
#include "cMDIDatabaseManager.h"
#include "cMDICaseManager.h"
#include "cReadingsManager.h"

cMDICaseManager *oMDICaseManager=NULL;

FXDEFMAP(cMDICaseManager) mapMDICaseManager[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDICaseManager::ID_CASEMANAGER,cMDICaseManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,FXMDIClient::ID_MDI_MENUCLOSE,cMDICaseManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,FXWindow::ID_MDI_CLOSE,cMDICaseManager::onCmdClose),
    FXMAPFUNC(SEL_CLOSE,cWinMain::ID_MDI_CHILD,cMDICaseManager::onCmdClose),

    FXMAPFUNC(SEL_FOCUSOUT,cMDICaseManager::ID_DCRITLIST,cMDICaseManager::onCritLooseFocus),

    FXMAPFUNC(SEL_COMMAND,cMDICaseManager::ID_LSALL,cMDICaseManager::onCmdLSall),
    FXMAPFUNC(SEL_COMMAND,cMDICaseManager::ID_LDSEL,cMDICaseManager::onCmdLDsel),
    FXMAPFUNC(SEL_COMMAND,cMDICaseManager::ID_SELALL0,cMDICaseManager::onCmdSelAll0),
    FXMAPFUNC(SEL_COMMAND,cMDICaseManager::ID_SELALL1,cMDICaseManager::onCmdSelAll1),

    FXMAPFUNC(SEL_COMMAND,cMDICaseManager::CMD_BUT_QSELECT,cMDICaseManager::onCmdQSelect),
    FXMAPFUNC(SEL_COMMAND,cMDICaseManager::CMD_BUT_DSELECT,cMDICaseManager::onCmdDSelect),
    FXMAPFUNC(SEL_COMMAND,cMDICaseManager::CMD_BUT_PROCEED,cMDICaseManager::onCmdProceed),
    FXMAPFUNC(SEL_COMMAND,cMDICaseManager::CMD_BUT_CASEOPEN,cMDICaseManager::onCmdCaseOpen),

    FXMAPFUNC(SEL_COMMAND,cMDICaseManager::CMD_DC0,cMDICaseManager::onCmdDateChooser0),
    FXMAPFUNC(SEL_COMMAND,cMDICaseManager::CMD_DC1,cMDICaseManager::onCmdDateChooser1),

    FXMAPFUNC(SEL_UPDATE,cMDICaseManager::CMD_BUT_PROCEED,cMDICaseManager::onUpdProceed),
    FXMAPFUNC(SEL_UPDATE,cMDICaseManager::ID_DCRIT,cMDICaseManager::onUpdCriteria),
    FXMAPFUNC(SEL_SELECTED,cMDICaseManager::ID_DCRIT,cMDICaseManager::onCmdCriteria),
    FXMAPFUNC(SEL_CONFIGURE,cMDICaseManager::ID_DCRITLIST,cMDICaseManager::onRszDCritList),

    FXMAPFUNC(SEL_REPLACED,cMDICaseManager::ID_DCRITLIST,cMDICaseManager::onCmdChangeCrit),

    FXMAPFUNC(SEL_COMMAND,cMDICaseManager::CMD_BUT_CLOSE,cMDICaseManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDICaseManager::CMD_BUT_OPEN,cMDICaseManager::onCmdOpen),
    FXMAPFUNC(SEL_COMMAND,cMDICaseManager::CMD_BUT_REMOVE,cMDICaseManager::onCmdRemove),

};

FXIMPLEMENT(cMDICaseManager,cMDIChild,mapMDICaseManager,ARRAYNUMBER(mapMDICaseManager))

cMDICaseManager::cMDICaseManager()
{
}

void cMDICaseManager::makeSelection(int prSource)
{
    if(prSource==-1)
        prSource=selSource;
    else
        selSource=prSource;
    sCaseObject *res;
    FXint count;
    switch(prSource)
    {
        case 1:
            res=cCaseSelector::makeQuickSelection(casea->getText(),caseb->getText(),popa->getText(),popb->getText(),datea->getText(),dateb->getText(),assayList,&count);
            break;
        case 0:
            res=cCaseSelector::makeSelection(criteriaList,&count);
            break;
        default:
            res=cCaseSelector::makeAllSelection(&count);
            break;
    }
    drawCases(res,count,prSource==0?true:false);
}

void cMDICaseManager::drawCases(sCaseObject *prCases,FXint prCount,FXbool prAddCols)
{
    cases->clearItems();

    sCriteriaObject *co;
    FXString ssc="";

    for(int i=cases->getNumHeaders()-1;i>=7;i--)
        cases->removeHeader(i);

    if(prAddCols)
    {
        for(int i=0;i<criteriaList->getNumItems();i++)
        {
            co=(sCriteriaObject*)criteriaList->getItemData(i);
            if(!co || !co->values1)
                continue;
            if(strstr(",id,readingDate,assay_oid,population_oid,count,template_oid,age,",co->attribute->text()))
               continue;

               cases->appendHeader(*co->name,(FXIcon*)NULL,80);
        }
    }

    for(int i=0;i<prCount;i++)
    {
        FXString clst=*prCases[i].assay_oid+"\t"+*prCases[i].id+"\t"+*prCases[i].readingDate+"\t"+FXStringVal(prCases[i].count)+"\t"+*prCases[i].age+"\t"+*prCases[i].population+"\t"+*prCases[i].tpl;

        if(prAddCols)
        {
            for(int j=0;j<criteriaList->getNumItems();j++)
            {
                co=(sCriteriaObject*)criteriaList->getItemData(j);
                if(!co || !co->values1)
                    continue;
                if(strstr(",id,readingDate,assay_oid,population_oid,count,template_oid,age,",co->attribute->text()))
                   continue;

                cDataResult *res1=oDataLink->execute("SELECT "+*co->attribute+((co->flag==CRITERIA_CASE)?" FROM t_gncases WHERE id='"+*prCases[i].id+"';":" FROM t_gnpopulations WHERE id='"+*prCases[i].population+"';"));

                fxmessage(">");
                clst=clst+(clst.empty()?"":"\t");
                clst=clst+(res1->getRowCount()?res1->getCellString(0,0):"");

                delete res1;
            }
        }

        cases->appendItem(clst);
        if(prCases[i].assay_oid)delete prCases[i].assay_oid;
        if(prCases[i].id)delete prCases[i].id;
        if(prCases[i].readingDate)delete prCases[i].readingDate;
        if(prCases[i].age)delete prCases[i].age;
        if(prCases[i].population)delete prCases[i].population;
        if(prCases[i].population_oid)delete prCases[i].population_oid;
        if(prCases[i].tpl)delete prCases[i].tpl;

    }
    if(prCount)
    {
        nocTitle->setText(oLanguage->getText("str_csmdi_cases"));
        nocWarn->setText("");
    }
    else
    {
        nocTitle->setText("");
        nocWarn->setText(oLanguage->getText("str_casesel_no"));
    }
    free(prCases);
    cases->sortItems();
}

void cMDICaseManager::drawByDict(void)
{
    for(int i=0;i<criteriaList->getNumItems();i++)
    {
        if(((sCriteriaObject*)criteriaList->getItemData(i))->values1!=NULL)
            criteriaList->setItemMiniIcon(i,critIcon,false);
        else
            criteriaList->setItemMiniIcon(i,deadIcon,false);
    }
    int ci=criteriaList->getCurrentItem();
    if(ci==-1)
        return;
    sCriteriaObject *co=(sCriteriaObject*)criteriaList->getItemData(ci);
    for(int l=0;l<50;l++)
        for(int k=0;k<2;k++)
            params->setItemText(l,k,(char*)NULL);
    if(!co->values1)
        return;
    FXchar *key;
    int total=co->values1->no(),i=co->values1->first(),j=0;
    for(;i<total;i=co->values1->next(i),j++)
    {
        params->setItemText(j,0,co->values1->data(i));
        key=(FXchar *)co->values2->find(co->values1->key(i));
        params->setItemText(j,1,!key?NULL:key);
    }
}

cMDICaseManager::cMDICaseManager(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH)
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_CASEMANAGER);
    client=prP;
    popup=prPup;
    targetFunc=NULL;
    selSource=0;

    critIcon=new FXGIFIcon(getApp(),data_criteria_select);
    deadIcon=new FXGIFIcon(getApp(),data_criteria_deselect);

    FXVerticalFrame *_vframe=new FXVerticalFrame(this,LAYOUT_FILL,0,0,0,0,0,0,5,0);
    tbMain=new FXTabBook(_vframe,this,ID_TBMAIN,PACK_UNIFORM_WIDTH|PACK_UNIFORM_HEIGHT|LAYOUT_FILL,0,0,0,0,0,0,0,0);
    tiQuick=new FXTabItem(tbMain,oLanguage->getText("str_csmdi_quick"));
        FXHorizontalFrame *_hframe1=new FXHorizontalFrame(tbMain,LAYOUT_FILL_X|FRAME_RAISED|FRAME_THICK);
            FXVerticalFrame *_vframe0=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y,0,0,0,0,0,0,10,0,0,0);
            new FXLabel(_vframe0,oLanguage->getText("str_csmdi_case"));
            new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_HEIGHT,0,0,DEFAULT_PAD,2);
            casea=new FXTextField(_vframe0,10,this,ID_QCRIT,FRAME_SUNKEN|LAYOUT_FILL_X);
            new FXLabel(_vframe0,(char*)NULL,new FXGIFIcon(getApp(),data_fromto),LAYOUT_FILL_X|JUSTIFY_CENTER_X);
            caseb=new FXTextField(_vframe0,10,this,ID_QCRIT,FRAME_SUNKEN|LAYOUT_FILL_X);
            caseb->setBackColor(FXRGB(250,250,250));
            new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_HEIGHT,0,0,0,15);
            new FXLabel(_vframe0,oLanguage->getText("str_csmdi_pop"));
            new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_HEIGHT,0,0,DEFAULT_PAD,2);
            popa=new FXTextField(_vframe0,10,this,ID_QCRIT,FRAME_SUNKEN|LAYOUT_FILL_X);
            new FXLabel(_vframe0,(char*)NULL,new FXGIFIcon(getApp(),data_fromto),LAYOUT_FILL_X|JUSTIFY_CENTER_X);
            popb=new FXTextField(_vframe0,10,this,ID_QCRIT,FRAME_SUNKEN|LAYOUT_FILL_X);
            popb->setBackColor(FXRGB(250,250,250));
            new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_HEIGHT,0,0,0,15);
            new FXLabel(_vframe0,oLanguage->getText("str_csmdi_date"));
            new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_HEIGHT,0,0,DEFAULT_PAD,2);
            FXHorizontalFrame *_hframe2=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
                new FXButton(_hframe2,(char*)NULL,new FXGIFIcon(getApp(),data_calendar),this,CMD_DC0,FRAME_RAISED|LAYOUT_LEFT);
                datea=new FXTextField(_hframe2,10,this,ID_QCRIT,FRAME_SUNKEN|LAYOUT_FILL_Y);
                datea->setJustify(JUSTIFY_CENTER_X);
                new FXLabel(_vframe0,(char*)NULL,new FXGIFIcon(getApp(),data_fromto),LAYOUT_FILL_X|JUSTIFY_CENTER_X,0,0,0,0,25);
            FXHorizontalFrame *_hframe3=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
                new FXButton(_hframe3,(char*)NULL,new FXGIFIcon(getApp(),data_calendar),this,CMD_DC1,FRAME_RAISED|LAYOUT_LEFT);
                dateb=new FXTextField(_hframe3,10,this,ID_QCRIT,FRAME_SUNKEN|LAYOUT_FILL_Y);
                dateb->setBackColor(FXRGB(250,250,250));
                dateb->setJustify(JUSTIFY_CENTER_X);
            new FXButton(_vframe0,"\n"+oLanguage->getText("str_csmdi_selall")+"\n",NULL,this,ID_SELALL0,FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_BOTTOM);
            FXVerticalFrame *_vframe1=new FXVerticalFrame(_hframe1,LAYOUT_FILL,0,0,0,0,0,0,8,0);
                new FXLabel(_vframe1,oLanguage->getText("str_asyman_asylist_title"));
                FXHorizontalFrame *_hframe4=new FXHorizontalFrame(_vframe1,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
                    assayList=new cSortList(_hframe4,this,ID_QCRIT,LAYOUT_FILL|ICONLIST_MULTIPLESELECT);
                    assayList->appendHeader(oLanguage->getText("str_asyman_asynameh"),NULL,120);
                    assayList->appendHeader(oLanguage->getText("str_asyman_asytitleh"),NULL,264);
                    assayList->setBackColor(FXRGB(240,242,245));
                new FXButton(_vframe1,"\n"+oLanguage->getText("str_csmdi_select")+"\n",NULL,this,CMD_BUT_QSELECT,FRAME_RAISED|LAYOUT_FILL_X|BUTTON_DEFAULT|BUTTON_INITIAL);

    tiDetailed=new FXTabItem(tbMain,oLanguage->getText("str_csmdi_detailed"));
        FXHorizontalFrame *_hframe10=new FXHorizontalFrame(tbMain,LAYOUT_FILL_X|FRAME_RAISED|FRAME_THICK);
            FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe10,LAYOUT_FILL_Y|LAYOUT_FIX_WIDTH,0,0,150,0,0,0,8,0);
                new FXLabel(_vframe2,oLanguage->getText("str_csmdi_criteria"));
                FXHorizontalFrame *_hframe5=new FXHorizontalFrame(_vframe2,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
                    criteriaList=new FXIconList(_hframe5,this,ID_DCRIT,LAYOUT_FILL|ICONLIST_BROWSESELECT);
                    criteriaList->setScrollStyle(HSCROLLER_NEVER|VSCROLLER_ALWAYS);
                    criteriaList->appendHeader(oLanguage->getText("str_csmdi_field"),NULL,131);
                    criteriaList->getHeader()->setHeight(18);
                    sCriteriaObject *res=cCaseSelector::listCriteria();
                    for(int i=0;i<cCaseSelector::getCriteriaCount();i++)
                    {
                        criteriaList->appendItem(*res[i].name);
                        criteriaList->setItemData(i,(void*)&res[i]);
                    }
                    criteriaList->setTextColor(FXRGB(0,0,180));
            FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe10,LAYOUT_FILL,0,0,0,0,0,0,8,0);
                paramsTitle=new FXLabel(_vframe3,(char*)NULL);
                FXHorizontalFrame *_hframe6=new FXHorizontalFrame(_vframe3,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
                    params=new cColorTable(_hframe6,this,ID_DCRITLIST,LAYOUT_FILL);
                    params->setTableSize(50,2);
                    params->setColumnWidth(0,100);
                    params->setColumnWidth(1,100);
                    params->setColumnText(0,oLanguage->getText("str_csmdi_from"));
                    params->setColumnText(1,oLanguage->getText("str_csmdi_to"));
                    params->setRowHeaderWidth(0);
                    params->showVertGrid(false);
                    params->getColumnHeader()->setItemJustify(0,JUSTIFY_CENTER_X);
                    params->getColumnHeader()->setItemJustify(1,JUSTIFY_CENTER_X);
                    for(int i=0;i<50;i++)
                    {
                        params->setItemJustify(i,0,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                        params->setItemJustify(i,1,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                        params->setCellColor(i,0,!(i%2)?FXRGB(253,253,253):FXRGB(250,250,250));
                        params->setCellColor(i,1,!(i%2)?FXRGB(250,250,250):FXRGB(247,247,247));
                    }
                FXHorizontalFrame *_hframe12=new FXHorizontalFrame(_vframe3,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                    new FXButton(_hframe12,"\n"+oLanguage->getText("str_csmdi_clearall")+"\n",NULL,this,ID_SELALL1,FRAME_RAISED|LAYOUT_FIX_WIDTH,0,0,80);
                    new FXButton(_hframe12,"\n"+oLanguage->getText("str_csmdi_select")+"\n",NULL,this,CMD_BUT_DSELECT,FRAME_RAISED|LAYOUT_FILL_X|BUTTON_DEFAULT);

    tiList=new FXTabItem(tbMain,oLanguage->getText("str_csmdi_list"));
        FXHorizontalFrame *_hframe100=new FXHorizontalFrame(tbMain,LAYOUT_FILL_X|FRAME_RAISED|FRAME_THICK);
            FXVerticalFrame *_vframe4=new FXVerticalFrame(_hframe100,LAYOUT_FILL,0,0,0,0,0,0,8,0);
                FXHorizontalFrame *_hframe9=new FXHorizontalFrame(_vframe4,LAYOUT_FILL_X|0,0,0,0,0,0,0,0);
                    nocTitle=new FXLabel(_hframe9,oLanguage->getText("str_csmdi_cases"));
                    nocWarn=new FXLabel(_hframe9,"",NULL,LABEL_NORMAL,0,0,0,0,2);
                    nocWarn->setTextColor(FXRGB(200,30,0));
                    nocWarn->setFont(new FXFont(oApplicationManager,"sans-serif",9,FXFont::Bold));
                    butDesel=new FXButton(_hframe9,oLanguage->getText("str_cseman_desel"),NULL,this,ID_LDSEL,LAYOUT_RIGHT|FRAME_RAISED|LAYOUT_FIX_WIDTH,0,0,70);
                    butSelall=new FXButton(_hframe9,oLanguage->getText("str_cseman_selall"),NULL,this,ID_LSALL,LAYOUT_RIGHT|FRAME_RAISED|LAYOUT_FIX_WIDTH,0,0,70);
                FXHorizontalFrame *_hframe7=new FXHorizontalFrame(_vframe4,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
                    cases=new cSortList(_hframe7,this,ID_CASELIST,LAYOUT_FILL|ICONLIST_EXTENDEDSELECT);
                    cases->appendHeader(oLanguage->getText("str_csmdi_c2"),NULL,60);
                    cases->appendHeader(oLanguage->getText("str_csmdi_c0"),NULL,85);
                    cases->appendHeader(oLanguage->getText("str_csmdi_c6"),NULL,85);
                    cases->appendHeader(oLanguage->getText("str_csmdi_c7"),NULL,45);
                    cases->appendHeader(oLanguage->getText("str_csmdi_c4"),NULL,35);
                    cases->appendHeader(oLanguage->getText("str_csmdi_c1"),NULL,80);
                    cases->appendHeader(oLanguage->getText("str_csmdi_c3"),NULL,96);

                FXHorizontalFrame *_hframe8=new FXHorizontalFrame(_vframe4,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                    //new FXButton(_hframe8,oLanguage->getText("str_tplmdi_print"),new FXGIFIcon(getApp(),data_settings_printer),this,CMD_BUT_PRINT,LAYOUT_LEFT|LAYOUT_FILL_Y|FRAME_RAISED|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH,0,0,70);
                    new FXButton(_hframe8,oLanguage->getText("str_cseman_remove"),NULL,this,CMD_BUT_REMOVE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_Y|LAYOUT_FIX_WIDTH,0,0,100);
                    new FXButton(_hframe8,oLanguage->getText("str_cseman_open"),NULL,this,CMD_BUT_CASEOPEN,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_Y|LAYOUT_FIX_WIDTH,0,0,100);
                    proceed=new FXButton(_hframe8,"\n"+oLanguage->getText("str_csmdi_proceed")+"\n",NULL,this,CMD_BUT_PROCEED,FRAME_RAISED|LAYOUT_FILL_X|BUTTON_DEFAULT);

    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(_vframe,LAYOUT_FILL_X|JUSTIFY_CENTER_X,0,0,0,0,0,0,0);
        new FXButton(_hframe0,oLanguage->getText("str_but_close"),NULL,this,CMD_BUT_CLOSE,LAYOUT_RIGHT|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_Y|LAYOUT_FIX_WIDTH,0,0,70);
    casea->setFocus();
    oMDICaseManager=this;
    this->update();
    handler=NULL;
}

cMDICaseManager::~cMDICaseManager()
{
    sCriteriaObject *co;
    for(int i=0;i<criteriaList->getNumItems();i++)
    {
        co=((sCriteriaObject*)criteriaList->getItemData(i));

        if(co->name)delete co->name;
        if(co->attribute)delete co->attribute;
        if(co->values1)delete co->values1;
        if(co->values2)delete co->values2;
    }
    oMDICaseManager=NULL;
}

void cMDICaseManager::create()
{
    cMDIChild::create();
    critIcon->create();
    deadIcon->create();
    show();
    update();
}

void cMDICaseManager::load(FXMDIClient *prP,FXPopup *prMenu)
{
    cMDICaseManager::load(prP,prMenu,cReadingsManager::openReading);
}

void cMDICaseManager::loadBsl(FXMDIClient *prP,FXPopup *prMenu,cMDIBaseline *prHandler,FXString prForTitle)
{
    cMDICaseManager::load(prP,prMenu,NULL,prForTitle);
    if(oMDICaseManager!=NULL)
    {
        oMDICaseManager->handler=prHandler;
        oMDICaseManager->targetFunc=NULL;
    }
}

void cMDICaseManager::load(FXMDIClient *prP,FXPopup *prMenu,void (*prFunction)(FXMDIClient*,FXPopup*,cSortList&),FXString prForTitle)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(prP,prMenu);
        return;
    }

    if(oMDICaseManager!=NULL)
    {
        oMDICaseManager->handler=NULL;
        oMDICaseManager->show();
        oMDICaseManager->setFocus();
        prP->setActiveChild(oMDICaseManager);
        if(oMDICaseManager->isMinimized())
            oMDICaseManager->restore();
        oMDICaseManager->layout();
        oMDICaseManager->recalc();
        oMDICaseManager->targetFunc=prFunction;
        oMDICaseManager->setTitle(prForTitle.empty()?oLanguage->getText("str_cseman_title"):oLanguage->getText("str_cseman_title_for")+" "+prForTitle);
        return;
    }
    new cMDICaseManager(prP,prForTitle.empty()?oLanguage->getText("str_cseman_title"):oLanguage->getText("str_cseman_title_for")+" "+prForTitle,
                        new FXGIFIcon(oApplicationManager,data_browse_templates),prMenu,MDI_NORMAL|MDI_TRACKING,10,10,600,430);
    oMDICaseManager->create();
    oMDICaseManager->makeSelection(2);
    oMDICaseManager->setFocus();
    oMDICaseManager->targetFunc=prFunction;
}

FXbool cMDICaseManager::isLoaded(void)
{
    return (oMDICaseManager!=NULL);
}

void cMDICaseManager::unload(void)
{
    if(!isLoaded())
        return;
    if(oMDICaseManager->mdiClient->numChildren()==1)
        oMDICaseManager->mdiClient->setActiveChild(NULL);
    //oMDICaseManager=NULL;
}

void cMDICaseManager::unshow(void)
{
    if(!isLoaded())
        return;
    oMDICaseManager->hide();
}

FXbool cMDICaseManager::sameHandler(cMDIBaseline *prHandler)
{
    return (oMDICaseManager->handler==prHandler);
}

void cMDICaseManager::update(void)
{
    if(!isLoaded())
        return;
    sAssayObject *res=cAssayManager::listAssays();
    assayList->clearItems();
    if(res==NULL)
        return;
    for(int i=0;i<cAssayManager::getAssayCount();i++)
    assayList->appendItem(*(res[i].id)+"\t"+(res[i].solid>0?"* ":"")+*(res[i].title));
    assayList->sortItems();
    free(res);
    onCmdCriteria(NULL,0,NULL);
}

void cMDICaseManager::cmdRefresh(void)
{
    if(!isLoaded())
        return;
    oMDICaseManager->update();
    oMDICaseManager->cases->killSelection();
}

void cMDICaseManager::cmdResearch(void)
{
    if(!isLoaded())
        return;
    oMDICaseManager->makeSelection(-1);
    oMDICaseManager->cases->killSelection();
}

long cMDICaseManager::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDICaseManager::unload();
    hide();
    return 1;
}

long cMDICaseManager::onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData)
{


    // TO DO


    return 1;
}

long cMDICaseManager::onCmdOpen(FXObject *prSender,FXSelector prSelector,void *prData)
{
    /*int db=-1,i;
    for(i=0;i<cseList->getNumItems();i++)
        if(cseList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        db=cseList->getCurrentItem();
    if(db==-1)
        return 1;
    FXString st=cseList->getItemText(db).before('\t');
    if(oWinMain->isWindow(st))
        oWinMain->raiseWindow(st);
    else
        cTemplateManager::editTemplate(client,popup,st);*/
    return 1;
}

long cMDICaseManager::onCmdRemove(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_erasecse").text()))
        return 1;
    for(int i=0;i<cases->getNumItems();i++)
        if(cases->isItemSelected(i))
            cCaseManager::removeCase(cases->getItemText(i).after('\t').before('\t'),cases->getItemText(i).before('\t'));
    makeSelection(-1);
    if(cases->getNumItems()>0)
        cases->selectItem(0);
    return 1;
}

long cMDICaseManager::onRszDCritList(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int tw=params->getWidth()-params->verticalScrollBar()->getWidth();
    params->setColumnWidth(0,tw>>1);
    params->setColumnWidth(1,tw-(tw>>1));
    return 1;
}

long cMDICaseManager::onCmdCriteria(FXObject *prSender,FXSelector prSelector,void *prData)
{
    params->acceptInput(true);
    if(criteriaList->getCurrentItem()==-1)
        params->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    else
    {
        params->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        drawByDict();
    }
    return 1;
}

long cMDICaseManager::onCmdDateChooser0(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXString res=cDateChooser::selectDate();
    if(!res.empty())
        datea->setText(res);
    return 1;
}

long cMDICaseManager::onCmdDateChooser1(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXString res=cDateChooser::selectDate();
    if(!res.empty())
        dateb->setText(res);
    return 1;
}

long cMDICaseManager::onCmdQSelect(FXObject *prSender,FXSelector prSelector,void *prData)
{
    tbMain->setCurrent(2);
    makeSelection(1);
    return 1;
}

long cMDICaseManager::onCmdDSelect(FXObject *prSender,FXSelector prSelector,void *prData)
{
    tbMain->setCurrent(2);
    params->acceptInput(true);
    makeSelection(0);
    return 1;
}

long cMDICaseManager::onCmdSelAll0(FXObject *prSender,FXSelector prSelector,void *prData)
{
    casea->setText((char*)NULL);
    caseb->setText((char*)NULL);
    popa->setText((char*)NULL);
    popb->setText((char*)NULL);
    datea->setText((char*)NULL);
    dateb->setText((char*)NULL);
    assayList->killSelection();
    return 1;
}

long cMDICaseManager::onCmdSelAll1(FXObject *prSender,FXSelector prSelector,void *prData)
{
    sCriteriaObject *co;
    for(int i=0;i<criteriaList->getNumItems();i++)
    {
        co=(sCriteriaObject*)criteriaList->getItemData(i);
        if(co->values1!=NULL)
            delete co->values1;
        if(co->values2!=NULL)
            delete co->values2;
        co->values1=NULL;
        co->values2=NULL;
    }
    criteriaList->selectItem(0);
    drawByDict();
    params->killSelection();
    return 1;
}

long cMDICaseManager::onCmdProceed(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(targetFunc)
        (*targetFunc)(mdiClient,mdiMenu,*cases);
    else if(handler)
        (handler->openCases)(mdiClient,mdiMenu,*cases);
    else
        cReadingsManager::openReading(mdiClient,mdiMenu,*cases);
    cMDICaseManager::unload();
    hide();
    return 1;
}

long cMDICaseManager::onUpdProceed(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXbool sel=false;
    for(int i=0;i<cases->getNumItems();i++)
        if(cases->isItemSelected(i))
        {
            sel=true;
            break;
        }
    if(!sel)
        proceed->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    else
        proceed->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    return 1;
}

long cMDICaseManager::onCmdCaseOpen(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int sel=0,pos=-1;
    FXString id,asy;
    for(int i=0;i<cases->getNumItems();i++)
        if(cases->isItemSelected(i))
        {
            asy=cases->getItemText(i).before('\t');
            id=cases->getItemText(i).after('\t').before('\t');
            sel++;
            pos=i;
        }
    if(sel!=1)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_reading_opnmc").text());
        return 1;
    }

    cDLGCasePopEdit dlg(oWinMain,id,asy);
    dlg.execute(PLACEMENT_OWNER);

    return 1;
}

long cMDICaseManager::onUpdCriteria(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int ci=criteriaList->getCurrentItem();
    FXString crit="";
    if(ci!=-1)
    {
        sCriteriaObject *co=(sCriteriaObject*)criteriaList->getItemData(ci);
        crit=*co->name;
    }
    paramsTitle->setText(oLanguage->getText("str_csmdi_params")+(crit.empty()?(char*)NULL:" ["+crit+"]"));
    return 1;
}

long cMDICaseManager::onCmdChangeCrit(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int ci=criteriaList->getCurrentItem();
    if(ci==-1)
        return 1;
    sCriteriaObject *co=(sCriteriaObject*)criteriaList->getItemData(ci);
    FXbool found1=false,found2=false;
    for(int i=0;i<50;i++)
    {
        if(!params->getItemText(i,0).empty())
            found1=true;
        if(!params->getItemText(i,1).empty())
            found2=true;
    }
    if(found1 || found2)
    {
        if(co->values1!=NULL)
            delete co->values1;
        if(co->values2!=NULL)
            delete co->values2;
        co->values1=new FXStringDict();
        co->values2=new FXStringDict();
        int counter=0;
        FXString s1,s2;
        for(int i=0;i<50;i++)
        {
            s1=params->getItemText(i,0);
            s2=params->getItemText(i,1);
            if(!s1.empty() && s2.empty())
                co->values1->insert(FXStringFormat("%c",counter++).text(),s1.text());
            else if(s1.empty() && !s2.empty())
                co->values1->insert(FXStringFormat("%c",counter++).text(),s2.text());
            else if(!s1.empty() && !s2.empty())
            {
                co->values1->insert(FXStringFormat("%c",counter).text(),s1.text());
                co->values2->insert(FXStringFormat("%c",counter++).text(),s2.text());
            }
        }
    }
    else
    {
        if(co->values1)
            delete co->values1;
        if(co->values2)
            delete co->values2;
        co->values1=NULL;
        co->values2=NULL;
    }
    drawByDict();
    return 1;
}

long cMDICaseManager::onCmdLSall(FXObject *prSender,FXSelector prSelector,void *prData)
{
    for(int i=0;i<cases->getNumItems();i++)
        cases->selectItem(i);
    return 1;
}

long cMDICaseManager::onCmdLDsel(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cases->killSelection();
    return 1;
}

long cMDICaseManager::onCritLooseFocus(FXObject *prSender,FXSelector prSelector,void *prData)
{
    params->acceptInput(true);
    return 1;
}

