#include <time.h>
#include <ctype.h>
#include <stdlib.h>
#include <fxkeys.h>
#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cColorsManager.h"
#include "cFormulaRatio.h"
#include "cBreedManager.h"
#include "cCaseManager.h"
#include "cPopulationManager.h"
#include "cWinMain.h"
#include "cMDIReport.h"
#include "cPrintDialog.h"

FXDEFMAP(cMDIReport) mapMDIReport[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIReport::ID_MDIREPORT,cMDIReport::onCmdClose),
    FXMAPFUNC(SEL_KEYPRESS,cMDIReport::ID_COMMENT,cMDIReport::onEvtKRedir),
    FXMAPFUNC(SEL_KEYRELEASE,cMDIReport::ID_COMMENT,cMDIReport::onEvtKRedir),
    FXMAPFUNC(SEL_KEYPRESS,cMDIReport::ID_HA,cMDIReport::onEvtKRedir),
    FXMAPFUNC(SEL_KEYRELEASE,cMDIReport::ID_HA,cMDIReport::onEvtKRedir),
    FXMAPFUNC(SEL_KEYPRESS,cMDIReport::ID_HB,cMDIReport::onEvtKRedir),
    FXMAPFUNC(SEL_KEYRELEASE,cMDIReport::ID_HB,cMDIReport::onEvtKRedir),
    FXMAPFUNC(SEL_COMMAND,cMDIReport::CMD_CHPOINT,cMDIReport::onCmdChPoint),
    FXMAPFUNC(SEL_COMMAND,cMDIReport::CMD_CHLINE,cMDIReport::onCmdChLine),
    FXMAPFUNC(SEL_COMMAND,cMDIReport::CMD_CHBAR,cMDIReport::onCmdChBar),
    FXMAPFUNC(SEL_UPDATE,cMDIReport::CMD_CHPOINT,cMDIReport::onUpdGB),
    FXMAPFUNC(SEL_UPDATE,cMDIReport::CMD_CHLINE,cMDIReport::onUpdGB),
    FXMAPFUNC(SEL_UPDATE,cMDIReport::CMD_CHBAR,cMDIReport::onUpdGB),
    FXMAPFUNC(SEL_COMMAND,cMDIReport::ID_COMMENT,cMDIReport::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIReport::ID_HA,cMDIReport::onCmdHA),
    FXMAPFUNC(SEL_REPLACED,cMDIReport::ID_HB,cMDIReport::onCmdHB),
    FXMAPFUNC(SEL_LEFTBUTTONRELEASE,cMDIReport::ID_SEP,cMDIReport::onCmdSeparator),
    FXMAPFUNC(SEL_UPDATE,cMDIReport::CMD_NEXT,cMDIReport::onUpdNav),
    FXMAPFUNC(SEL_UPDATE,cMDIReport::CMD_PREV,cMDIReport::onUpdNav),
    FXMAPFUNC(SEL_COMMAND,cMDIReport::CMD_NEXT,cMDIReport::onCmdNext),
    FXMAPFUNC(SEL_COMMAND,cMDIReport::CMD_PREV,cMDIReport::onCmdPrev),
    FXMAPFUNC(SEL_COMMAND,cMDIReport::CMD_SAVE,cMDIReport::onCmdSave),
    FXMAPFUNC(SEL_COMMAND,cMDIReport::CMD_PRINT,cMDIReport::onCmdPrint),
    FXMAPFUNC(SEL_COMMAND,cMDIReport::CMD_CLOSE,cMDIReport::onCmdClose),
};

FXIMPLEMENT(cMDIReport,cMDIChild,mapMDIReport,ARRAYNUMBER(mapMDIReport))

cMDIReport::cMDIReport()
{
}

FXbool cMDIReport::saveData(void)
{
    if(saved)
        return true;
    return true;
}

cMDIReport::cMDIReport(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH)
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_MDIREPORT);
    saved=false;
    name="";
    charts=new FXDict();
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL);
        FXHorizontalFrame *_hframe0=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X|JUSTIFY_CENTER_X,0,0,0,0,0,0,0,0,0,0);
            new FXButton(_hframe0,oLanguage->getText("str_report_print"),new FXGIFIcon(getApp(),data_settings_printer),this,CMD_PRINT,LAYOUT_LEFT|FRAME_RAISED|LAYOUT_FIX_WIDTH|JUSTIFY_NORMAL|ICON_BEFORE_TEXT,0,0,80);
            FXHorizontalFrame *_hframe1=new FXHorizontalFrame(_hframe0,LAYOUT_CENTER_X|LAYOUT_FILL_Y|FRAME_SUNKEN,0,0,0,0,0,0,0,0,0,0);
                butPrev=new FXButton(_hframe1,oLanguage->getText("str_report_prev"),NULL,this,CMD_PREV,LAYOUT_FILL_Y|FRAME_RAISED|LAYOUT_FIX_WIDTH,0,0,100);
                lbPage=new cLabel(_hframe1,"  "+oLanguage->getText("str_report_page")+" 1/1  ",NULL,LAYOUT_FILL_Y);
                lbPage->setBackColor(FXRGB(255,255,255));
                butNext=new FXButton(_hframe1,oLanguage->getText("str_report_next"),NULL,this,CMD_NEXT,LAYOUT_FILL_Y|FRAME_RAISED|LAYOUT_FIX_WIDTH,0,0,116);
            new FXButton(_hframe0,oLanguage->getText("str_but_close"),NULL,this,CMD_CLOSE,FRAME_RAISED|LAYOUT_FIX_WIDTH|LAYOUT_FILL_Y|LAYOUT_RIGHT,0,0,80);
        FXHorizontalFrame *_hframe2=new FXHorizontalFrame(_vframe0,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0,0,0);
            scrollWindow=new FXScrollWindow(_hframe2,LAYOUT_FILL);
            scrollWindow->enable();
                FXVerticalFrame *_hframe3=new FXVerticalFrame(scrollWindow,LAYOUT_FILL|JUSTIFY_CENTER_Y|JUSTIFY_CENTER_X,0,0,0,0,5,5,5,20,0,20);
                    _hframe3->setBackColor(FXRGB(120,120,120));
                    _hframe3->setTarget(this);
                    _hframe3->setSelector(ID_REDIR);
                    _hframe3->enable();
                    FXHorizontalFrame *_hframe223=new FXHorizontalFrame(_hframe3,LAYOUT_CENTER_X,0,0,0,0,3,3,3,3,0,0);
                        _hframe223->setBackColor(FXRGB(50,50,50));
                        bchPoint=new FXButton(_hframe223,(char*)NULL,new FXGIFIcon(getApp(),data_chart_point),this,CMD_CHPOINT,FRAME_RAISED);
                        bchLine=new FXButton(_hframe223,(char*)NULL,new FXGIFIcon(getApp(),data_chart_line),this,CMD_CHLINE,FRAME_RAISED);
                        bchBar=new FXButton(_hframe223,(char*)NULL,new FXGIFIcon(getApp(),data_chart_bar),this,CMD_CHBAR,FRAME_RAISED);

                    FXHorizontalFrame *_hframe4=new FXHorizontalFrame(_hframe3,LAYOUT_CENTER_Y|LAYOUT_CENTER_X,0,0,0,0,3,3,3,3,0,0);
                        _hframe4->setBackColor(FXRGB(50,50,50));
                        _hframe4->setTarget(this);
                        _hframe4->setSelector(ID_REDIR);
                        _hframe4->enable();
                        FXHorizontalFrame *_hframe5=new FXHorizontalFrame(_hframe4,LAYOUT_CENTER_Y|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH|LAYOUT_FIX_HEIGHT,0,0,700,990,15,15,15,15,0,0);
                            _hframe5->setBackColor(FXRGB(255,255,255));
                            _hframe5->setTarget(this);
                            _hframe5->setSelector(ID_REDIR);
                            _hframe5->enable();
                            switcher=new FXSwitcher(_hframe5,LAYOUT_FILL,0,0,0,0,0,0,0,0);
                                switcher->setTarget(this);
                                switcher->setSelector(ID_REDIR);
                                switcher->enable();
    pages=new FXDict();

    counter=0;
    currentPage=0;
    addPage(true);

    sPageObject *page;
    page=(sPageObject*)pages->find("0");
    page->headerA->selectAll();
    page->headerA->setFocus();
    page->headerA->setCursorPos(page->headerA->getText().length());
    seps=true;
    comment=NULL;
    pcWidth=page->contents->getWidth();
    pcSize=page->contents->getHeight();
    pcPos=0;
    pcPage=0;
    titleCon="";
    isCom=false;
}


cMDIReport::~cMDIReport()
{
}

void cMDIReport::create()
{
    cMDIChild::create();
    show();
    sPageObject *page=(sPageObject*)pages->find("0");
    if(page)
    {
        pcWidth=page->contents->getWidth();
        pcSize=page->contents->getHeight();
    }
}

FXbool cMDIReport::canClose(void)
{
    /*if(!saved)
    {
        setFocus();
        int res=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),oLanguage->getText("str_qsave").text());
        switch(res)
        {
            case MBOX_CLICKED_YES:
                if(!saveData())
                    return false;
                break;
            case MBOX_CLICKED_CANCEL:
                return false;
            default:
                break;
        }
    }*/
    return true;
}

FXComposite *cMDIReport::addPage(FXbool prNoCreate)
{
    saved=false;

    sPageObject *pg,*page=new sPageObject;
    cLabel *lb;
    FXFont *ft1,*ft2;
    page->container=new FXVerticalFrame(switcher,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
    page->container->setBackColor(FXRGB(255,255,255));
    page->container->setTarget(this);
    page->container->setSelector(ID_REDIR);
    page->container->enable();
        FXHorizontalFrame *_hframe6=new FXHorizontalFrame(page->container,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT|JUSTIFY_CENTER_Y,0,0,0,73);
            _hframe6->setBackColor(FXRGB(255,255,255));
            _hframe6->setTarget(this);
            _hframe6->setSelector(ID_REDIR);
            _hframe6->enable();
            FXVerticalFrame *_vframe1=new FXVerticalFrame(_hframe6,LAYOUT_FILL,0,0,0,0,0,0,0,0);
                _vframe1->setBackColor(FXRGB(255,255,255));
                _vframe1->setTarget(this);
                _vframe1->setSelector(ID_REDIR);
                _vframe1->enable();
                ft1=new FXFont(oApplicationManager,"Arial",16,FXFont::Bold);
                page->headerA=new cTextField(_vframe1,20,this,ID_HA,LAYOUT_FILL_X);
                page->headerA->setText(oLanguage->getText("str_report_typetitle"));
                page->headerA->setFont(ft1);
                page->headerA->setTextColor(FXRGB(160,0,0));
                page->headerA->setBaseColor(FXRGB(255,255,255));
                page->headerA->setSelBackColor(FXRGB(230,230,230));
                page->headerA->setSelTextColor(FXRGB(0,0,0));
                //ft1=new FXFont(oApplicationManager,"Helvetica",8,FXFont::Normal);
                page->headerB=new cText(_vframe1,this,ID_HB,LAYOUT_FILL|TEXT_WORDWRAP);
                page->headerB->setScrollStyle(HSCROLLER_NEVER|VSCROLLER_NEVER);
                page->headerB->setText(oLanguage->getText("str_report_typeheader"));
                //page->headerB->setFont(ft1);
                page->headerB->setBackColor(FXRGB(255,255,255));
                page->headerB->setSelBackColor(FXRGB(230,230,230));
                page->headerB->setSelTextColor(FXRGB(0,0,0));

            FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe6,LAYOUT_RIGHT|LAYOUT_CENTER_Y);
                _vframe2->setBackColor(FXRGB(255,255,255));
                _vframe2->setTarget(this);
                _vframe2->setSelector(ID_REDIR);
                _vframe2->enable();
                FXHorizontalFrame *_hframe8=new FXHorizontalFrame(_vframe2,0,0,0,0,0,0,0,0,0,0,0);
                    _hframe8->setBackColor(FXRGB(255,255,255));
                    _hframe8->setTarget(this);
                    _hframe8->setSelector(ID_REDIR);
                    _hframe8->enable();
                    ft1=new FXFont(oApplicationManager,"Times New Roman",26,FXFont::Normal);
                    ft2=new FXFont(oApplicationManager,"Times New Roman",18,FXFont::Normal);
                    lb=new cLabel(_hframe8,"U",NULL,LAYOUT_BOTTOM,0,0,0,0,0,0,0,0);
                        lb->setFont(ft1);
                        lb->setBackColor(FXRGB(255,255,255));
                        lb->setTextColor(FXRGB(160,0,0));
                        lb->setTarget(this);
                        lb->setSelector(ID_REDIR);
                        lb->enable();
                    lb=new cLabel(_hframe8,"n",NULL,LAYOUT_BOTTOM,0,0,0,0,2,3,0,2);
                        lb->setFont(ft2);
                        lb->setBackColor(FXRGB(255,255,255));
                        lb->setTextColor(FXRGB(160,0,0));
                        lb->setTarget(this);
                        lb->setSelector(ID_REDIR);
                        lb->enable();
                    lb=new cLabel(_hframe8,"i",NULL,LAYOUT_BOTTOM,0,0,0,0,0,0,0,2);
                        lb->setFont(ft2);
                        lb->setBackColor(FXRGB(255,255,255));
                        lb->setTextColor(FXRGB(160,0,0));
                        lb->setTarget(this);
                        lb->setSelector(ID_REDIR);
                        lb->enable();
                    lb=new cLabel(_hframe8,"V",NULL,LAYOUT_BOTTOM,0,0,0,0,0,0,0,0);
                        lb->setFont(ft1);
                        lb->setBackColor(FXRGB(255,255,255));
                        lb->setTextColor(FXRGB(160,0,0));
                        lb->setTarget(this);
                        lb->setSelector(ID_REDIR);
                        lb->enable();
                    lb=new cLabel(_hframe8,"e",NULL,LAYOUT_BOTTOM,0,0,0,0,0,3,0,2);
                        lb->setFont(ft2);
                        lb->setBackColor(FXRGB(255,255,255));
                        lb->setTextColor(FXRGB(160,0,0));
                        lb->setTarget(this);
                        lb->setSelector(ID_REDIR);
                        lb->enable();
                    lb=new cLabel(_hframe8,"t",NULL,LAYOUT_BOTTOM,0,0,0,0,0,0,0,2);
                        lb->setFont(ft2);
                        lb->setBackColor(FXRGB(255,255,255));
                        lb->setTextColor(FXRGB(160,0,0));
                        lb->setTarget(this);
                        lb->setSelector(ID_REDIR);
                        lb->enable();

                FXHorizontalFrame *_hframe9=new FXHorizontalFrame(_vframe2,LAYOUT_FILL_X|JUSTIFY_CENTER_X,0,0,0,0,0,0,0,0,0,0);
                    _hframe9->setBackColor(FXRGB(255,255,255));
                    _hframe9->setTarget(this);
                    _hframe9->setSelector(ID_REDIR);
                    _hframe9->enable();
                ft1=new FXFont(oApplicationManager,"Times New Roman",7,FXFont::Normal);
                lb=new cLabel(_hframe9,oLanguage->getText("str_report_version")+FXStringFormat(" %s",APP_VERSION),NULL,LAYOUT_CENTER_X,0,0,0,0,0,0,0,0);
                    lb->setFont(ft1);
                    lb->setBackColor(FXRGB(255,255,255));
                    lb->setTarget(this);
                    lb->setSelector(ID_REDIR);
                    lb->enable();
        page->hs=new cHorizontalSeparator(page->container,SEPARATOR_LINE|LAYOUT_FILL_X,0,0,0,0,0,0,5,5);
        page->hs->setBackColor(FXRGB(255,255,255));
        page->hs->setTarget(this);
        page->hs->setSelector(ID_SEP);
        page->hs->enable();
        page->contents=new FXVerticalFrame(page->container,LAYOUT_FILL);
            page->contents->setBackColor(FXRGB(255,255,255));
            page->contents->setTarget(this);
            page->contents->setSelector(ID_REDIR);
            page->contents->enable();
        page->fs=new cHorizontalSeparator(page->container,SEPARATOR_LINE|LAYOUT_FILL_X,0,0,0,0,0,0,5,5);
        page->fs->setBackColor(FXRGB(255,255,255));
        page->fs->setTarget(this);
        page->fs->setSelector(ID_SEP);
        page->fs->enable();
        FXHorizontalFrame *_hframe7=new FXHorizontalFrame(page->container,LAYOUT_FILL_X|LAYOUT_BOTTOM|LAYOUT_FIX_HEIGHT,0,0,0,40);
            _hframe7->setBackColor(FXRGB(255,255,255));
            _hframe7->setTarget(this);
            _hframe7->setSelector(ID_REDIR);
            _hframe7->enable();
            page->lb=new cLabel(_hframe7,(char*)NULL,NULL,LAYOUT_RIGHT|LAYOUT_CENTER_Y,0,0,0,0,0,0,0,0);
                page->lb->setFont(new FXFont(oApplicationManager,"Arial",8,FXFont::Bold));
                page->lb->setBackColor(FXRGB(255,255,255));
            FXchar ftime[50];
            time_t curtime;
            struct tm *loctime;
            curtime=time(NULL);
            loctime=localtime(&curtime);
            strftime(ftime,100,"%Y-%m-%d, %H:%M",loctime);
            FXString pdate(ftime);
            cLabel *pf=new cLabel(_hframe7,oWinMain->getTechnician()+" - "+pdate,NULL,LAYOUT_FILL_Y);
            pf->setBackColor(FXRGB(255,255,255));
            pf->setFont(new FXFont(oApplicationManager,"Helvetica",8,FXFont::Normal));

    pages->insert(FXStringFormat("%d",counter).text(),page);

    if(!prNoCreate)
        page->container->create();

    counter++;

    for(int i=0;i<pages->no();i++)
    {
        pg=(sPageObject*)pages->find(FXStringFormat("%d",i).text());
        if(pg)
            pg->lb->setText(oLanguage->getText("str_report_page")+FXStringFormat(" %d/%d",i+1,counter));
    }

    lbPage->setText("  "+oLanguage->getText("str_report_page")+FXStringFormat(" %d/%d  ",currentPage+1,counter));
    if(!prNoCreate)
    {
        lbPage->forceRefresh();
        switcher->forceRefresh();
        onCmdHA(NULL,0,NULL);
        onCmdHB(NULL,0,NULL);
    }
    return page->contents;
}

FXComposite *cMDIReport::getPageContents(int prPage)
{
    if(prPage==-1)prPage=pcPage;
    sPageObject *ret=(sPageObject*)pages->find(FXStringFormat("%d",prPage).text());
    return ret?ret->contents:NULL;
}

int cMDIReport::getCurrentPage(void)
{
    return currentPage;
}

void cMDIReport::setPage(int prPage)
{
    currentPage=prPage;
    switcher->setCurrent(prPage);
    lbPage->setText("  "+oLanguage->getText("str_report_page")+FXStringFormat(" %d/%d  ",currentPage+1,counter));
    int x,y;
    scrollWindow->getPosition(x,y);
    scrollWindow->setPosition(x,0);
}

void cMDIReport::setTitle(FXString prText)
{
    sPageObject *page;
    for(int i=0;i<pages->no();i++)
    {
        page=(sPageObject*)pages->find(FXStringFormat("%d",i).text());
        if(page)
            page->headerA->setText(prText);
    }
}

void cMDIReport::setHeader(FXString prText)
{
    sPageObject *page;
    for(int i=0;i<pages->no();i++)
    {
        page=(sPageObject*)pages->find(FXStringFormat("%d",i).text());
        if(page)
            page->headerB->setText(prText);
    }
}

long cMDIReport::onCmdPrev(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(currentPage>0)
        setPage(currentPage-1);
    return 1;
}

long cMDIReport::onCmdNext(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(currentPage<(counter-1))
        setPage(currentPage+1);
    return 1;
}

long cMDIReport::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(canClose())
    {
        close();
        return 1;
    }
    return 0;
}

long cMDIReport::onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData)
{
    saveData();
    //onCmdClose(NULL,0,NULL);
    return 1;
}

long cMDIReport::onUpdNav(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(currentPage==0)
        butPrev->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    else
        butPrev->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    if(currentPage==counter-1)
        butNext->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    else
        butNext->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    return 1;
}

long cMDIReport::onEvtRedir(FXObject *prSender,FXSelector prSelector,void *prData)
{
    return scrollWindow->tryHandle(prSender,prSelector,prData);
}

long cMDIReport::onEvtKRedir(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXEvent *event=(FXEvent*)prData;
    switch(event->code)
    {
        case KEY_Page_Up:
        case KEY_Page_Down:
        case KEY_KP_Page_Up:
        case KEY_KP_Page_Down:
            return scrollWindow->handle(this,ID_MDIREPORT,prData);
            break;
        default:
            break;
    }
    return prSender->handle(prSender,prSelector,prData);
}

long cMDIReport::onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData)
{
    saved=false;
    return 1;
}

long cMDIReport::onCmdSeparator(FXObject *prSender,FXSelector prSelector,void *prData)
{
    sPageObject *page;
    if(seps)
    {
        seps=false;
        for(int i=0;i<pages->no();i++)
        {
            page=(sPageObject*)pages->find(FXStringFormat("%d",i).text());
            if(page)
            {
                page->fs->setSeparatorStyle(SEPARATOR_NONE);
                page->hs->setSeparatorStyle(SEPARATOR_NONE);
                page->fs->forceRefresh();
                page->hs->forceRefresh();
            }
        }
    }
    else
    {
        seps=true;
        for(int i=0;i<pages->no();i++)
        {
            page=(sPageObject*)pages->find(FXStringFormat("%d",i).text());
            if(page)
            {
                page->fs->setSeparatorStyle(SEPARATOR_LINE);
                page->hs->setSeparatorStyle(SEPARATOR_LINE);
                page->fs->forceRefresh();
                page->hs->forceRefresh();
            }
        }
    }
    return 1;
}

long cMDIReport::onCmdHA(FXObject *prSender,FXSelector prSelector,void *prData)
{
    saved=false;
    sPageObject *page,*page0;
    page0=(sPageObject*)pages->find(FXStringFormat("%d",switcher->getCurrent()).text());
    for(int i=0;i<pages->no();i++)
    {
        page=(sPageObject*)pages->find(FXStringFormat("%d",i).text());
        if(page)
            page->headerA->setText(page0->headerA->getText());
    }
    return 1;
}

long cMDIReport::onCmdHB(FXObject *prSender,FXSelector prSelector,void *prData)
{
    saved=false;
    sPageObject *page=NULL,*page0;
    page0=(sPageObject*)pages->find(FXStringFormat("%d",switcher->getCurrent()).text());
    for(int i=0;i<pages->no();i++)
    {
        page=(sPageObject*)pages->find(FXStringFormat("%d",i).text());
        if(page)
            page->headerB->setText(page0->headerB->getText());
    }
    return page->headerB->tryHandle(prSender,prSelector,prData);
}

void cMDIReport::addTitle(FXString prText,FXint prOpts,FXbool prContinuum)
{
    if(prText.empty())
        return;

    if(prContinuum)
    {
        titleCon=prText;
        titleConOpts=prOpts;
    }
    else
        titleCon="";

    if(!fitsOnPage(130))
    {
        nextPage();
        if(!titleCon.empty())
            return;
    }
    sPageObject *page;
    page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
    cLabel *lb=new cLabel(page->contents,prText,NULL,prOpts|LAYOUT_FIX_HEIGHT|JUSTIFY_BOTTOM,0,0,0,20,0,0,0,0);
    lb->setFont(new FXFont(oApplicationManager,"Arial Black",9,FXFont::Normal));
    lb->setBackColor(FXRGB(255,255,255));
    lb->setTarget(this);
    lb->setSelector(ID_REDIR);
    lb->enable();
    lb->create();
    lb->recalc();
    page->contents->recalc();
    pcPos+=lb->getHeight()+DEFAULT_SPACING;
}

void cMDIReport::addSubTitle(FXString prText,FXint prOpts)
{
    if(prText.empty())
        return;
    sPageObject *page;
    page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
    cLabel *lb=new cLabel(page->contents,prText,NULL,prOpts|LAYOUT_FIX_HEIGHT|JUSTIFY_BOTTOM,0,0,0,15,0,0,0,0);
    lb->setFont(new FXFont(oApplicationManager,"Arial",8,FXFont::Bold));
    lb->setBackColor(FXRGB(255,255,255));
    lb->setTarget(this);
    lb->setSelector(ID_REDIR);
    lb->enable();
    lb->create();
    lb->recalc();
    page->contents->recalc();
    pcPos+=lb->getHeight()+DEFAULT_SPACING;
}

void cMDIReport::addError(FXString prText,FXint prOpts)
{
    if(prText.empty())
        return;
    sPageObject *page;
    page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
    cLabel *lb=new cLabel(page->contents,FXStringFormat(" %s ", prText.text()),NULL,prOpts|LAYOUT_FIX_HEIGHT|JUSTIFY_BOTTOM,0,0,0,15,0,0,0,0);
    lb->setFont(new FXFont(oApplicationManager,"Arial",8,FXFont::Bold));
    lb->setBackColor(FXRGB(150,0,0));
    lb->setTextColor(FXRGB(255,255,255));
    lb->setTarget(this);
    lb->setSelector(ID_REDIR);
    lb->enable();
    lb->create();
    lb->recalc();
    page->contents->recalc();
    pcPos+=lb->getHeight()+DEFAULT_SPACING;
}

void cMDIReport::addSuccess(FXString prText,FXint prOpts)
{
    if(prText.empty())
        return;
    sPageObject *page;
    page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
    cLabel *lb=new cLabel(page->contents,prText,NULL,prOpts|LAYOUT_FIX_HEIGHT|JUSTIFY_BOTTOM,0,0,0,15,0,0,0,0);
    lb->setFont(new FXFont(oApplicationManager,"Arial",8,FXFont::Bold));
    lb->setBackColor(FXRGB(255,255,255));
    lb->setTextColor(FXRGB(0,150,0));
    lb->setTarget(this);
    lb->setSelector(ID_REDIR);
    lb->enable();
    lb->create();
    lb->recalc();
    page->contents->recalc();
    pcPos+=lb->getHeight()+DEFAULT_SPACING;
}

FXint cMDIReport::getTextWidth(FXString prText)
{
    FXFont *ft=new FXFont(oApplicationManager,"Helvetica",7,FXFont::Normal);
    ft->create();
    int ret=ft->getTextWidth(prText);
    delete ft;
    return ret;
}

void cMDIReport::addText(FXString prText,FXint prOpts,FXbool prIndent)
{
    if(prText.empty())
        return;
    sPageObject *page,*page0;
    page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
    if(!page)
        return;
    FXFont *ft=new FXFont(oApplicationManager,"Helvetica",7,FXFont::Normal);
    ft->create();
    int lh=ft->getTextHeight(prText.before('\n')),nc=0;
    FXString prt=prText;
    while(prt.find('\n')!=-1)
    {
        prt=prt.after('\n');
        nc++;
        lh+=ft->getTextHeight(prText.before('\n'));
    }

    cLabel *lb=new cLabel(page->contents,prText,NULL,prOpts|LAYOUT_FIX_HEIGHT,0,0,0,lh,prIndent?40:0,0,0,0);
    lb->setFont(ft);
    lb->setTarget(this);
    lb->setSelector(ID_REDIR);
    lb->enable();
    lb->create();
    lb->recalc();
    page->contents->recalc();
    if(!fitsOnPage(lh))
    {
        page0=page;
        nextPage();
        page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
        if(!page)
            return;
        lb->reparent(page->contents);
        page0->contents->recalc();
    }

    lb->setBackColor(FXRGB(255,255,255));
    lb->recalc();
    page->contents->recalc();
    pcPos+=lh+DEFAULT_SPACING;
}

void cMDIReport::addTextMono(FXString prText,FXint prOpts,FXbool prIndent,FXbool prSmall)
{
    if(prText.empty())
        return;
    sPageObject *page,*page0;
    page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
    if(!page)
        return;
    FXFont *ft=new FXFont(oApplicationManager,"Courier",prSmall?8:8,FXFont::Normal);
    ft->create();
    int lh=ft->getTextHeight(prText.before('\n')),nc=0;
    FXString prt=prText;
    while(prt.find('\n')!=-1)
    {
        prt=prt.after('\n');
        nc++;
        lh+=ft->getTextHeight(prText.before('\n'));
    }

    cLabel *lb=new cLabel(page->contents,prText,NULL,prOpts|LAYOUT_FIX_HEIGHT,0,0,0,lh,prIndent?40:0,0,0,0);
    lb->setFont(ft);
    lb->setTarget(this);
    lb->setSelector(ID_REDIR);
    lb->enable();
    lb->create();
    lb->recalc();
    page->contents->recalc();
    if(!fitsOnPage(lh))
    {
        page0=page;
        nextPage();
        page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
        if(!page)
            return;
        lb->reparent(page->contents);
        page0->contents->recalc();
    }

    lb->setBackColor(FXRGB(255,255,255));
    lb->recalc();
    page->contents->recalc();
    pcPos+=lh+DEFAULT_SPACING;
}

void cMDIReport::addTextBold(FXString prText,FXint prOpts,FXbool prIndent)
{
    if(prText.empty())
        return;
    sPageObject *page,*page0;
    page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
    if(!page)
        return;
    FXFont *ft=new FXFont(oApplicationManager,"Helvetica",7,FXFont::Bold);
    ft->create();
    int lh=ft->getTextHeight(prText.before('\n')),nc=0;
    FXString prt=prText;
    while(prt.find('\n')!=-1)
    {
        prt=prt.after('\n');
        nc++;
        lh+=ft->getTextHeight(prText.before('\n'));
    }

    cLabel *lb=new cLabel(page->contents,prText,NULL,prOpts|LAYOUT_FIX_HEIGHT,0,0,0,lh,prIndent?40:0,0,0,0);
    lb->setFont(ft);
    lb->setTarget(this);
    lb->setSelector(ID_REDIR);
    lb->enable();
    lb->create();
    lb->recalc();
    page->contents->recalc();
    if(!fitsOnPage(lh))
    {
        page0=page;
        nextPage();
        page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
        if(!page)
            return;
        lb->reparent(page->contents);
        page0->contents->recalc();
    }

    lb->setBackColor(FXRGB(255,255,255));
    lb->recalc();
    page->contents->recalc();
    pcPos+=lh+DEFAULT_SPACING;
}

void cMDIReport::addTextBoldMono(FXString prText,FXint prOpts,FXbool prIndent,FXbool prSmall)
{
    if(prText.empty())
        return;
    sPageObject *page,*page0;
    page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
    if(!page)
        return;
    FXFont *ft=new FXFont(oApplicationManager,"Courier",prSmall?8:8,FXFont::Bold);
    ft->create();
    int lh=ft->getTextHeight(prText.before('\n')),nc=0;
    FXString prt=prText;
    while(prt.find('\n')!=-1)
    {
        prt=prt.after('\n');
        nc++;
        lh+=ft->getTextHeight(prText.before('\n'));
    }

    cLabel *lb=new cLabel(page->contents,prText,NULL,prOpts|LAYOUT_FIX_HEIGHT,0,0,0,lh,prIndent?40:0,0,0,0);
    lb->setFont(ft);
    lb->setTarget(this);
    lb->setSelector(ID_REDIR);
    lb->enable();
    lb->create();
    lb->recalc();
    page->contents->recalc();
    if(!fitsOnPage(lh))
    {
        page0=page;
        nextPage();
        page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
        if(!page)
            return;
        lb->reparent(page->contents);
        page0->contents->recalc();
    }

    lb->setBackColor(FXRGB(255,255,255));
    lb->recalc();
    page->contents->recalc();
    pcPos+=lh+DEFAULT_SPACING;
}

void cMDIReport::addDoubleCharts(cVChart *prObj1,cVChart *prObj2)
{
    if(!prObj1)
        return;
    sPageObject *page;
    FXWindow *page0,*page00;
    page0=prObj1->getParent();
    page00=prObj2?prObj2->getParent():NULL;
    int maxHeight=prObj2?(prObj1->getHeight()>prObj2->getHeight()?prObj1->getHeight():prObj2->getHeight()):prObj1->getHeight();
    if(!fitsOnPage(maxHeight))
        nextPage();
    page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
    if(!page)
        return;
    prObj1->create();
    if(prObj2)prObj2->create();
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(page->contents,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
        _hframe0->setBackColor(FXRGB(255,255,255));
        _hframe0->setTarget(this);
        _hframe0->setSelector(ID_REDIR);
        _hframe0->enable();
        _hframe0->create();
        _hframe0->recalc();

    prObj1->reparent(_hframe0);
    //prObj1->setLayoutHints(LAYOUT_LEFT|LAYOUT_CENTER_Y|LAYOUT_FILL);
    prObj1->setWidth(pcWidth>>1);
    prObj1->setBackColor(FXRGB(255,255,255));
    prObj1->setTarget(this);
    prObj1->setSelector(ID_REDIR);
    prObj1->enable();
    prObj1->recalc();

    if(prObj2)
    {
        prObj2->reparent(_hframe0);
        prObj2->setWidth(pcWidth>>1);
        //prObj2->setLayoutHints(LAYOUT_RIGHT|LAYOUT_CENTER_Y||LAYOUT_FILL);
        prObj2->setBackColor(FXRGB(255,255,255));
        prObj2->setTarget(this);
        prObj2->setSelector(ID_REDIR);
        prObj2->enable();
        prObj2->recalc();
    }

    _hframe0->recalc();
    page->contents->recalc();
    page0->recalc();
    if(page00)
        page00->recalc();
    if(!charts->no())
        bchBar->setState(STATE_DOWN);
    charts->insert(FXStringFormat("%d",charts->no()).text(),prObj1);
    if(prObj2)
        charts->insert(FXStringFormat("%d",charts->no()).text(),prObj2);
    pcPos+=maxHeight+DEFAULT_SPACING;
}

void cMDIReport::addChart(cVChart *prObj)
{
    if(!prObj)
        return;
    sPageObject *page;
    FXWindow *page0;
    page0=prObj->getParent();
    if(!fitsOnPage(prObj->getHeight()))
        nextPage();
    page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
    if(!page)
        return;
    prObj->create();
    if(page0!=page->contents)
        prObj->reparent(page->contents);
    prObj->setBackColor(FXRGB(255,255,255));
    prObj->setTarget(this);
    prObj->setSelector(ID_REDIR);
    prObj->enable();
    prObj->recalc();
    page->contents->recalc();
    if(page0 && page0!=page->contents)
        page0->recalc();
    if(!charts->no())
        bchBar->setState(STATE_DOWN);
    charts->insert(FXStringFormat("%d",charts->no()).text(),prObj);
    pcPos+=prObj->getHeight()+DEFAULT_SPACING;
}

void cMDIReport::addTable(cColorTable *prObj,FXbool prIndent)
{
    if(!prObj)
        return;
    const int trh=15;
    FXFont *ft=new FXFont(oApplicationManager,"Helvetica",7,FXFont::Normal);
    ft->create();
    prObj->setFont(ft);
    prObj->setSelectable(false);
    prObj->showVertGrid(false);
    prObj->showHorzGrid(false);
    prObj->setTableStyle(TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT|TABLE_READONLY);
    prObj->setScrollStyle(HSCROLLER_NEVER|HSCROLLING_OFF|VSCROLLER_NEVER|VSCROLLING_OFF);
    prObj->fitColumnsToContents(0,prObj->getNumColumns());
    for(int j=0;j<prObj->getNumColumns();j++)
    {
        prObj->setColumnWidth(j,prObj->getColumnWidth(j)+10);
        prObj->setColumnText(j,(char*)NULL);
    }
    prObj->setColumnHeaderHeight(0);
    prObj->setRowHeaderWidth(0);
    prObj->create();
    prObj->recalc();

    int cols=prObj->getNumColumns();
    int totalw=prObj->getContentWidth();
    if(prObj->getWidth()>totalw)
        totalw=prObj->getWidth();
    FXHorizontalFrame *hf;
    cColorTable *ct;
    int stocki=1;
    sPageObject *page;
    FXWindow *page0;
    page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
    page0=prObj->getParent();
    if(!page)
        return;

    while(stocki<(prObj->getNumRows()))
    {
        if((pcSize-pcPos)<80)
            nextPage();
        page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
        if(!page)
            break;
        int fitRows=(pcSize-pcPos-4)/trh,hrw=0;
        int fitCols=(pcWidth-(prIndent?40:0))/(totalw+4);
        if(!fitCols)fitCols=1;
        if((fitRows*fitCols)>(prObj->getNumRows()-stocki+15) && (prObj->getNumRows()-stocki)>15)
        {
            fitRows=(prObj->getNumRows()-stocki+fitCols)/fitCols;
            if((prObj->getNumRows()-stocki+fitCols)>(fitRows*fitCols))
                fitRows++;
            if(fitRows<13)
                fitRows=13;
        }
        hf=new FXHorizontalFrame(page->contents,LAYOUT_FILL_X,0,0,0,0,prIndent?40:0,0,0,0,4);
        hf->setBackColor(FXRGB(255,255,255));
        hf->setTarget(this);
        hf->setSelector(ID_REDIR);
        hf->enable();
        hf->create();
        hf->recalc();
        for(int coli=0;coli<fitCols;coli++)
        {
            if(fitRows>(prObj->getNumRows()-stocki+1))
                fitRows=prObj->getNumRows()-stocki+1;
            if(coli==0)hrw=fitRows;
            if(fitRows<=1)
                break;
            ct=new cColorTable(hf,this,ID_REDIR,(coli==0?0:0)|TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT|TABLE_READONLY|LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                ct->setTableSize(fitRows,cols);
                ct->setFont(ft);
                ct->setSelectable(false);
                ct->showVertGrid(false);
                ct->showHorzGrid(false);
                ct->setGridColor(FXRGB(210,210,210));
                ct->setCellBorderColor(FXRGB(130,130,130));
                ct->setCellBorderWidth(1);
                ct->setScrollStyle(HSCROLLER_NEVER|HSCROLLING_OFF|VSCROLLER_NEVER|VSCROLLING_OFF);
                //ct->fitColumnsToContents(0,ct->getNumColumns());
                ct->setColumnHeaderHeight(0);
                ct->setRowHeaderWidth(0);
                ct->setBackColor(FXRGB(255,255,255));
            for(int i=0;i<ct->getNumRows();i++)
            {
                for(int j=0;j<ct->getNumColumns();j++)
                {
                    if(i==0)
                    {
                        ct->setItemText(i,j,prObj->getItemText(0,j));
                        ct->setColumnWidth(j,prObj->getColumnWidth(j));
                    }
                    else
                        ct->setItemText(i,j,prObj->getItemText(stocki,j));

                    ct->setItemJustify(i,j,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                    ct->setCellTextColor(i,j,i==0?FXRGB(0,0,0):(prObj->useCellTextColor(stocki,j)?prObj->getCellTextColor(stocki,j):FXRGB(0,0,0)));
                    ct->useCellTextColor(i,j,true);
                    ct->setCellColor(i,j,i==0?FXRGB(220,220,220):FXRGB(255,255,255));
                }
                if(i>0)
                {
                    ct->setItemBorders(i,0,FXTableItem::LBORDER);
                    ct->setItemBorders(i,ct->getNumColumns()-1,FXTableItem::RBORDER);
                }

                if(i==ct->getNumRows()-1)
                    for(int j=0;j<ct->getNumColumns();j++)
                        ct->setItemBorders(i,j,ct->getItemBorders(i,j)+FXTableItem::BBORDER);
                ct->setRowHeight(i,trh);
                if(i>0)
                    stocki++;
            }
            ct->disable();
            ct->create();
            ct->recalc();
        }
        hf->recalc();
        page->contents->recalc();
        pcPos+=trh*hrw+4;
    }
    delete prObj;
    page0->recalc();
}

void cMDIReport::addPlateTable(cColorTable *prObj,FXbool prIndent)
{
    if(!prObj)
        return;
    const int trh=30;
    FXFont *ft=new FXFont(oApplicationManager,"Arial",7,FXFont::Normal);
    ft->create();
    prObj->setFont(ft);
    prObj->setSelectable(false);
    prObj->showVertGrid(false);
    prObj->showHorzGrid(false);
    prObj->setTableStyle(TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT|TABLE_READONLY);
    prObj->setScrollStyle(HSCROLLER_NEVER|HSCROLLING_OFF|VSCROLLER_NEVER|VSCROLLING_OFF);
    //prObj->fitColumnsToContents(0,prObj->getNumColumns());
    for(int j=0;j<prObj->getNumColumns();j++)
    {
        prObj->setColumnWidth(j,prObj->getColumnWidth(j)+4);
        prObj->setColumnText(j,(char*)NULL);
    }
    prObj->setColumnHeaderHeight(0);
    prObj->setRowHeaderWidth(0);
    prObj->create();
    prObj->recalc();

    int cols=prObj->getNumColumns();
    int totalw=prObj->getContentWidth();
    if(prObj->getWidth()>totalw)
        totalw=prObj->getWidth();
    FXHorizontalFrame *hf;
    cColorTable *ct;
    int stocki=1;
    sPageObject *page;
    FXWindow *page0;
    page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
    page0=prObj->getParent();
    if(!page)
        return;

    while(stocki<(prObj->getNumRows()))
    {
        if((pcSize-pcPos)<80)
            nextPage();
        page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
        if(!page)
            break;
        int fitRows=(pcSize-pcPos-4)/trh,hrw=0;
        int fitCols=(pcWidth-(prIndent?40:0))/(totalw+4);
        if(!fitCols)fitCols=1;
        if((fitRows*fitCols)>(prObj->getNumRows()-stocki+15) && (prObj->getNumRows()-stocki)>15)
        {
            fitRows=(prObj->getNumRows()-stocki+fitCols)/fitCols;
            if((prObj->getNumRows()-stocki+fitCols)>(fitRows*fitCols))
                fitRows++;
            if(fitRows<13)
                fitRows=13;
        }
        hf=new FXHorizontalFrame(page->contents,LAYOUT_FILL_X,0,0,0,0,prIndent?40:0,0,0,0,4);
        hf->setBackColor(FXRGB(255,255,255));
        hf->setTarget(this);
        hf->setSelector(ID_REDIR);
        hf->enable();
        hf->create();
        hf->recalc();
        for(int coli=0;coli<fitCols;coli++)
        {
            if(fitRows>(prObj->getNumRows()-stocki+1))
                fitRows=prObj->getNumRows()-stocki+1;
            if(coli==0)hrw=fitRows;
            if(fitRows<=1)
                break;
            ct=new cColorTable(hf,this,ID_REDIR,(coli==0?0:0)|TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT|TABLE_READONLY|LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                ct->setTableSize(fitRows,cols);
                ct->setFont(ft);
                ct->setSelectable(false);
                ct->showVertGrid(true);
                ct->showHorzGrid(true);
                ct->setGridColor(FXRGB(150,150,150));
                ct->setCellBorderColor(FXRGB(150,150,150));
                ct->setCellBorderWidth(0);
                ct->setScrollStyle(HSCROLLER_NEVER|HSCROLLING_OFF|VSCROLLER_NEVER|VSCROLLING_OFF);
                //ct->fitColumnsToContents(0,ct->getNumColumns());
                ct->setColumnHeaderHeight(0);
                ct->setRowHeaderWidth(0);
                ct->setBackColor(FXRGB(255,255,255));
            for(int i=0;i<ct->getNumRows();i++)
            {
                for(int j=0;j<ct->getNumColumns();j++)
                {
                    if(i==0)
                    {
                        ct->setItemText(i,j,prObj->getItemText(0,j));
                        ct->setColumnWidth(j,prObj->getColumnWidth(j));
                    }
                    else
                        ct->setItemText(i,j,prObj->getItemText(stocki,j));

                    ct->setItemJustify(i,j,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                    ct->setCellTextColor(i,j,(i==0 || j==0)?FXRGB(0,0,0):(prObj->useCellTextColor(stocki,j)?prObj->getCellTextColor(stocki,j):FXRGB(0,0,0)));
                    ct->useCellTextColor(i,j,true);
                    ct->setCellColor(i,j,(i==0 || j==0)?FXRGB(220,220,220):prObj->getCellColor(stocki,j));
                }
                if(i>0)
                {
                    ct->setItemBorders(i,0,FXTableItem::LBORDER);
                    ct->setItemBorders(i,ct->getNumColumns()-1,FXTableItem::RBORDER);
                }

                if(i==ct->getNumRows()-1)
                    for(int j=0;j<ct->getNumColumns();j++)
                        ct->setItemBorders(i,j,ct->getItemBorders(i,j)+FXTableItem::BBORDER);
                ct->setRowHeight(i,trh);
                if(i>0)
                    stocki++;
            }
            ct->disable();
            ct->create();
            ct->recalc();
        }
        hf->recalc();
        page->contents->recalc();
        pcPos+=trh*hrw+4;
    }
    delete prObj;
    page0->recalc();
}

void cMDIReport::addDetailTable(cColorTable *prObj,FXbool prIndent)
{
    if(!prObj)
        return;
    const int trh=15;
    FXFont *ft=new FXFont(oApplicationManager,"Arial",6,FXFont::Normal);
    ft->create();
    prObj->setFont(ft);
    prObj->setSelectable(false);
    prObj->showVertGrid(false);
    prObj->showHorzGrid(false);
    prObj->setTableStyle(TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT|TABLE_READONLY);
    prObj->setScrollStyle(HSCROLLER_NEVER|HSCROLLING_OFF|VSCROLLER_NEVER|VSCROLLING_OFF);
    prObj->fitColumnsToContents(0,prObj->getNumColumns());
    for(int j=0;j<prObj->getNumColumns();j++)
    {
        prObj->setColumnWidth(j,prObj->getColumnWidth(j)+4);
        prObj->setColumnText(j,(char*)NULL);
    }
    prObj->setColumnHeaderHeight(0);
    prObj->setRowHeaderWidth(0);
    prObj->create();
    prObj->recalc();

    int cols=prObj->getNumColumns();
    int totalw=prObj->getContentWidth();
    if(prObj->getWidth()>totalw)
        totalw=prObj->getWidth();
    FXHorizontalFrame *hf;
    cColorTable *ct;
    int stocki=1;
    sPageObject *page;
    FXWindow *page0;
    page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
    page0=prObj->getParent();
    if(!page)
        return;

    while(stocki<(prObj->getNumRows()))
    {
        if((pcSize-pcPos)<80)
            nextPage();
        page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
        if(!page)
            break;
        int fitRows=(pcSize-pcPos-4)/trh,hrw=0;
        int fitCols=(pcWidth-(prIndent?40:0))/(totalw+4);
        if(!fitCols)fitCols=1;
        if((fitRows*fitCols)>(prObj->getNumRows()-stocki+15) && (prObj->getNumRows()-stocki)>15)
        {
            fitRows=(prObj->getNumRows()-stocki+fitCols)/fitCols;
            if((prObj->getNumRows()-stocki+fitCols)>(fitRows*fitCols))
                fitRows++;
            if(fitRows<13)
                fitRows=13;
        }
        hf=new FXHorizontalFrame(page->contents,LAYOUT_FILL_X,0,0,0,0,prIndent?40:0,0,0,0,4);
        hf->setBackColor(FXRGB(255,255,255));
        hf->setTarget(this);
        hf->setSelector(ID_REDIR);
        hf->enable();
        hf->create();
        hf->recalc();
        for(int coli=0;coli<fitCols;coli++)
        {
            if(fitRows>(prObj->getNumRows()-stocki+1))
                fitRows=prObj->getNumRows()-stocki+1;
            if(coli==0)hrw=fitRows;
            if(fitRows<=1)
                break;
            ct=new cColorTable(hf,this,ID_REDIR,(coli==0?0:0)|TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT|TABLE_READONLY|LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                ct->setTableSize(fitRows,cols);
                ct->setFont(ft);
                ct->setSelectable(false);
                ct->showVertGrid(true);
                ct->showHorzGrid(true);
                ct->setGridColor(FXRGB(210,210,210));
                ct->setCellBorderColor(FXRGB(110,110,110));
                ct->setCellBorderWidth(0);
                ct->setScrollStyle(HSCROLLER_NEVER|HSCROLLING_OFF|VSCROLLER_NEVER|VSCROLLING_OFF);
                ct->fitColumnsToContents(0,ct->getNumColumns());
                ct->setColumnHeaderHeight(0);
                ct->setRowHeaderWidth(0);
                ct->setBackColor(FXRGB(255,255,255));
            for(int i=0;i<ct->getNumRows();i++)
            {
                for(int j=0;j<ct->getNumColumns();j++)
                {
                    if(i==0)
                    {
                        ct->setItemText(i,j,prObj->getItemText(0,j));
                        ct->setColumnWidth(j,prObj->getColumnWidth(j));
                    }
                    else
                        ct->setItemText(i,j,prObj->getItemText(stocki,j));

                    ct->setItemJustify(i,j,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                    ct->setCellTextColor(i,j,i==0?FXRGB(0,0,0):(prObj->useCellTextColor(stocki,j)?prObj->getCellTextColor(stocki,j):FXRGB(0,0,0)));
                    ct->useCellTextColor(i,j,true);
                    ct->setCellColor(i,j,i==0?FXRGB(220,220,220):FXRGB(255,255,255));
                }
                if(i>0)
                {
                    ct->setItemBorders(i,0,FXTableItem::LBORDER);
                    ct->setItemBorders(i,ct->getNumColumns()-1,FXTableItem::RBORDER);
                }

                if(i==ct->getNumRows()-1)
                    for(int j=0;j<ct->getNumColumns();j++)
                        ct->setItemBorders(i,j,ct->getItemBorders(i,j)+FXTableItem::BBORDER);
                ct->setRowHeight(i,trh);
                if(i>0)
                    stocki++;
            }
            ct->disable();
            ct->create();
            ct->recalc();
        }
        hf->recalc();
        page->contents->recalc();
        pcPos+=trh*hrw+4;
    }
    delete prObj;
    page0->recalc();
}

void cMDIReport::addComments(void)
{
    if(isCom)
        return;
    //isCom=true;

    if(!fitsOnPage(182+(DEFAULT_PAD<<1)))
        nextPage();
    sPageObject *page;
    page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());

    addSubTitle(oLanguage->getText("str_report_comments"));
    comment=new cText(page->contents,this,ID_COMMENT,LAYOUT_FILL_X|TEXT_WORDWRAP|LAYOUT_FIX_HEIGHT|FRAME_LINE,0,0,0,150,0,0,0,0);
    comment->setScrollStyle(HSCROLLER_NEVER|VSCROLLER_NEVER);
    comment->setText(oLanguage->getText("str_report_typecomments"));
    comment->setSelBackColor(FXRGB(230,230,230));
    comment->setSelTextColor(FXRGB(0,0,0));
    //comment->setFont(new FXFont(oApplicationManager,"Helvetica",10,FXFont::Normal));
    comment->setBackColor(FXRGB(255,255,255));
    comment->create();
    comment->recalc();

    page->contents->recalc();
    pcPos+=182+(DEFAULT_PAD<<1)+(DEFAULT_SPACING<<1);
}

FXbool cMDIReport::fitsOnPage(FXint prSize)
{
    return (pcPos+prSize<=pcSize);
}

void cMDIReport::nextPage(void)
{
    pcPage=counter;
    pcPos=0;
    addPage();

    if(!titleCon.empty())
    {
        sPageObject *page;
        page=(sPageObject*)pages->find(FXStringFormat("%d",pcPage).text());
        cLabel *lb=new cLabel(page->contents,oLanguage->getText("str_report_continued")+" "+titleCon,NULL,titleConOpts|LAYOUT_FIX_HEIGHT|JUSTIFY_BOTTOM,0,0,0,30,0,0,0,10);
        lb->setFont(new FXFont(oApplicationManager,"Arial",8,FXFont::Normal));
        lb->setBackColor(FXRGB(255,255,255));
        lb->setTarget(this);
        lb->setSelector(ID_REDIR);
        lb->enable();
        lb->create();
        lb->recalc();
        page->contents->recalc();
        pcPos+=lb->getHeight()+DEFAULT_SPACING;
    }
}

FXint cMDIReport::getPageSize(void)
{
    return pcSize;
}

long cMDIReport::onCmdChPoint(FXObject *prSender,FXSelector prSelector,void *prData)
{
    bchPoint->setState(STATE_DOWN);
    bchLine->setState(STATE_UP);
    bchBar->setState(STATE_UP);
    for(int i=0;i<charts->no();i++)
        ((cVChart*)charts->find(FXStringFormat("%d",i).text()))->setType(cVChart::CHART_POINT);
    scrollWindow->hide();
    scrollWindow->show();
    return 1;
}

long cMDIReport::onCmdChLine(FXObject *prSender,FXSelector prSelector,void *prData)
{
    bchPoint->setState(STATE_UP);
    bchLine->setState(STATE_DOWN);
    bchBar->setState(STATE_UP);
    for(int i=0;i<charts->no();i++)
        ((cVChart*)charts->find(FXStringFormat("%d",i).text()))->setType(cVChart::CHART_LINE);
    scrollWindow->hide();
    scrollWindow->show();
    return 1;
}

long cMDIReport::onCmdChBar(FXObject *prSender,FXSelector prSelector,void *prData)
{
    bchPoint->setState(STATE_UP);
    bchLine->setState(STATE_UP);
    bchBar->setState(STATE_DOWN);
    for(int i=0;i<charts->no();i++)
        ((cVChart*)charts->find(FXStringFormat("%d",i).text()))->setType(cVChart::CHART_BAR);
    scrollWindow->hide();
    scrollWindow->show();
    return 1;
}

long cMDIReport::onUpdGB(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(!charts->no())
    {
        bchPoint->disable();
        bchLine->disable();
        bchBar->disable();
    }
    else
    {
        bchPoint->enable();
        bchLine->enable();
        bchBar->enable();
    }
    return 1;
}

void cMDIReport::printObject(cDCNativePrinter *prDC,FXWindow *prObj,unsigned int prFlags,int prX,int prY)
{
    prDC->TranslateX(prX);
    prDC->TranslateY(prY);

    if(!strcmp(prObj->getClassName(),"cVChart"))
        ((cVChart*)prObj)->onPaint(NULL,0,prDC);
    if(!strcmp(prObj->getClassName(),"cColorTable"))
        ((cColorTable*)prObj)->onPaint(NULL,0,prDC);
    if(!strcmp(prObj->getClassName(),"cLabel"))
        ((cLabel*)prObj)->onPaint(NULL,0,prDC);
    if(!strcmp(prObj->getClassName(),"cText"))
        ((cText*)prObj)->onPaint(NULL,0,prDC);
    if(!strcmp(prObj->getClassName(),"cTextField"))
        ((cTextField*)prObj)->onPaint(NULL,0,prDC);
    if(!strcmp(prObj->getClassName(),"cHorizontalSeparator"))
        ((cHorizontalSeparator*)prObj)->onPaint(NULL,0,prDC);

    if(!strcmp(prObj->getClassName(),"FXPacker") || !strcmp(prObj->getClassName(),"FXHorizontalFrame") || !strcmp(prObj->getClassName(),"FXVerticalFrame"))
    {
        for(int ci=0;ci<prObj->numChildren();ci++)
        {
            FXWindow *w=prObj->childAtIndex(ci);
            printObject(prDC,w,prFlags,prX+w->getX(),prY+w->getY());
        }
    }
}

void cMDIReport::printPage(cDCNativePrinter *prDC,int prPage,FXPrinter &prPrinter,int prSx,int prSy)
{
    if(prPage<1)
        return;
    if(prPage>pages->no())
        return;

    sPageObject *page=(sPageObject*)pages->find(FXStringFormat("%d",prPage-1).text());
    if(!page)
        return;

    prDC->beginPage(prPage);
    printObject(prDC,page->container,prPrinter.flags,prSx,prSy);
    prDC->endPage();
}

long cMDIReport::onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData)
{
    sPageObject *page;
    for(int i=0;i<pages->no();i++)
    {
        page=(sPageObject*)pages->find(FXStringFormat("%d",i).text());
        if(page)
        {
            page->headerA->killSelection();
            page->headerB->killSelection();
            page->headerA->forceRefresh();
            page->headerB->forceRefresh();
        }
    }
    if(comment)
        comment->killSelection();

    cPrintDialog dlg(oWinMain,oLanguage->getText("str_print_dlgtitle"));
    FXPrinter printer;
    dlg.getPrinter(printer);
    printer.frompage=printer.firstpage=1;
    printer.topage=printer.lastpage=pages->no();
    printer.currentpage=1;
    printer.numcopies=1;
    dlg.setPrinter(printer);
    cDCNativePrinter *pdc;
    if(dlg.execute())
    {
        dlg.getPrinter(printer);
        pdc=new cDCNativePrinter(getApp());
        if(!pdc->beginPrint(printer))
        {
            FXMessageBox::error(this,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_print_error").text());
            return 1;
        }

        //FXMessageBox::information(this,MBOX_OK,"Info-debug",FXStringFormat("%d",printer.mediasize).text());

        #ifndef WIN32
        int omh=(int)printer.mediaheight;
        #endif
        pdc->setHorzUnitsInch(((double)(770)/(double)(printer.mediawidth))*72.0);
        pdc->setVertUnitsInch(((double)(1050)/(double)(printer.mediaheight))*72.0);

        printer.mediawidth=700;
        printer.mediaheight=1050;
        printer.leftmargin=printer.rightmargin=0.07*printer.mediawidth;
        printer.topmargin=0.05*printer.mediawidth;
        printer.bottommargin=0.03*printer.mediawidth;

        int start=1,end=pages->no(),pinc=1;
        int sx=(int)printer.leftmargin,sy=0;

        #ifdef WIN32
        if(!(printer.flags&PRINT_DEST_FILE))
        {
            pdc->setLineWidth((int)pdc->ScaleFX(0.1));
            sy+=(int)printer.topmargin-pdc->ScaleFX(2);
            sx-=pdc->ScaleX(3);
        }
        else
            sy-=75;
        #else
        if(!(printer.flags&PRINT_DEST_FILE))
            sy-=60+842-omh;
        else
            sy-=75;
        #endif


        if(printer.flags&PRINT_PAGES_RANGE)
        {
            start=printer.frompage;
            end=printer.topage;
        }
        if(printer.flags&PRINT_COLLATE_REVERSED)
        {
            pinc=end;
            end=start;
            start=pinc;
            pinc=-1;
        }

        for(unsigned int j=0;j<printer.numcopies;j++)
        {
            for(int i=start;(printer.flags&PRINT_COLLATE_REVERSED)?i>=end:i<=end;i+=pinc)
            {
                if((printer.flags&PRINT_PAGES_EVEN) && (i%2))
                    continue;
                if((printer.flags&PRINT_PAGES_ODD) && (i%2==0))
                    continue;
                printPage(pdc,i,printer,sx,sy);
            }
        }

        if(!pdc->endPrint())
        {
            FXMessageBox::error(this,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_print_error").text());
            return 1;
        }
        delete pdc;
    }

    return 1;
}
