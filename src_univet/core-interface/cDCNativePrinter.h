#ifndef CDCNATIVEPRINTER_H
#define CDCNATIVEPRINTER_H

#include <fx.h>
#include <math.h>
#ifdef WIN32
#include "windows.h"
#endif

class FXAPI cDCNativePrinter : public FXDC
{
    private:
    protected:
        FXbool    iscolor;
        FXint     pagecount;
        FXint     dctype;
        FXdouble  scalex;
        FXdouble  scaley;
        FXdouble  bscale;
        FXint     trsx,trsy;
        FXint     logpixelsx;
        FXint     logpixelsy;
        FXfloat   unitsx;
        FXfloat   unitsy;
        FXDC     *pdc;
        #ifdef WIN32
        HDC       dc;
        DOCINFO   di;
        DEVMODE   devmode;
        HANDLE    devmode_handle;
        FXObject *opaque;
        #endif
        FXbool    bw;

    public:
        enum
        {
            TYPE_PS=0,
            TYPE_WIN32=1
        };

        /// Construct
        cDCNativePrinter(FXApp* a);

        FXbool isColor(){return iscolor;};

        /// Generate print job prolog
        virtual FXbool beginPrint(FXPrinter& job);

        /// Generate print job epilog
        virtual FXbool endPrint();

        /// Generate begin of page
        virtual FXbool beginPage(FXuint page=1);

        /// Generate end of page
        virtual FXbool endPage();

        /// setup paper dimensions
        void setHorzUnitsInch(FXfloat sx);
        void setVertUnitsInch(FXfloat sy);
        FXint ScaleX(FXint x) { return (int)round((FXdouble)x*scalex); }
        FXint ScaleY(FXint y) { return (int)round((FXdouble)y*scaley); }
        FXint ScaleFX(FXdouble x) { return (int)round(x*scalex); }
        void TranslateX(FXint x) { trsx=x; }
        void TranslateY(FXint y) { trsy=y; }
        FXfloat getHorzUnitsInch(void) { return unitsx; }
        FXfloat getVertUnitsInch(void) { return unitsy; }

        /// setup scaling
        void scalePoints(FXPoint *dst, FXPoint *src, FXuint npoints );
        void scaleRectangles(FXRectangle *dst, FXRectangle *src, FXuint nrectangles );
        void scaleSegments(FXSegment *dst, FXSegment *src, FXuint nsegments );
        void scaleArcs(FXArc *dst, FXArc *src, FXuint narcs );

        /// Draw points
        virtual void drawPoint(FXint x,FXint y);
        virtual void drawPoints(const FXPoint* points,FXuint npoints);
        virtual void drawPointsRel(const FXPoint* points,FXuint npoints);

        /// Draw lines
        virtual void drawLine(FXint x1,FXint y1,FXint x2,FXint y2);
        virtual void drawLines(const FXPoint* points,FXuint npoints);
        virtual void drawLinesRel(const FXPoint* points,FXuint npoints);
        virtual void drawLineSegments(const FXSegment* segments,FXuint nsegments);

        /// Draw rectangles
        virtual void drawRectangle(FXint x,FXint y,FXint w,FXint h);
        virtual void drawRectangles(const FXRectangle* rectangles,FXuint nrectangles);

        /// Draw arcs
        virtual void drawArc(FXint x,FXint y,FXint w,FXint h,FXint ang1,FXint ang2);
        virtual void drawArcs(const FXArc* arcs,FXuint narcs);

        /// Filled rectangles
        virtual void fillRectangle(FXint x,FXint y,FXint w,FXint h);
        virtual void fillRectangles(const FXRectangle* rectangles,FXuint nrectangles);

        /// Draw arcs
        virtual void fillArc(FXint x,FXint y,FXint w,FXint h,FXint ang1,FXint ang2);
        virtual void fillArcs(const FXArc* arcs,FXuint narcs);

        /// Filled polygon
        virtual void fillPolygon(const FXPoint* points,FXuint npoints);
        virtual void fillConcavePolygon(const FXPoint* points,FXuint npoints);
        virtual void fillComplexPolygon(const FXPoint* points,FXuint npoints);

        /// Filled polygon with relative points
        virtual void fillPolygonRel(const FXPoint* points,FXuint npoints);
        virtual void fillConcavePolygonRel(const FXPoint* points,FXuint npoints);
        virtual void fillComplexPolygonRel(const FXPoint* points,FXuint npoints);

        /// Draw hashed box
        virtual void drawHashBox(FXint x,FXint y,FXint w,FXint h,FXint b=1);

        /// Draw area from source
        virtual void drawArea(const FXDrawable* source,FXint sx,FXint sy,FXint sw,FXint sh,FXint dx,FXint dy);

        /// Draw image
        virtual void drawImage(const FXImage* image,FXint dx,FXint dy);

        /// Draw bitmap
        virtual void drawBitmap(const FXBitmap* bitmap,FXint dx,FXint dy);

        /// Draw icon
        virtual void drawIcon(const FXIcon* icon,FXint dx,FXint dy);
        virtual void drawIconShaded(const FXIcon* icon,FXint dx,FXint dy);
        virtual void drawIconSunken(const FXIcon* icon,FXint dx,FXint dy);

        /// Draw string
        virtual void drawText(FXint x,FXint y,const FXchar* string,FXuint length);
        virtual void drawImageText(FXint x,FXint y,const FXchar* string,FXuint length);

        /// Set foreground/background drawing color
        virtual void setForeground(FXColor prClr);
        virtual void setBackground(FXColor clr);

        /// Set dash pattern
        virtual void setDashes(FXuint dashoffset,const FXchar *dashlist,FXuint n);

        /// Set line width
        virtual void setLineWidth(FXuint linewidth=0);

        /// Set line cap style
        virtual void setLineCap(FXCapStyle capstyle=CAP_BUTT);

        /// Set line join style
        virtual void setLineJoin(FXJoinStyle joinstyle=JOIN_MITER);

        /// Set line style
        virtual void setLineStyle(FXLineStyle linestyle=LINE_SOLID);

        /// Set fill style
        virtual void setFillStyle(FXFillStyle fillstyle=FILL_SOLID);

        /// Set fill rule
        virtual void setFillRule(FXFillRule fillrule=RULE_EVEN_ODD);

        /// Set blit function
        virtual void setFunction(FXFunction func=BLT_SRC);

        /// Set the tile
        virtual void setTile(FXImage* tile,FXint dx=0,FXint dy=0);

        /// Set the stipple pattern
        virtual void setStipple(FXBitmap *stipple,FXint dx=0,FXint dy=0);

        /// Set the stipple pattern
        virtual void setStipple(FXStipplePattern stipple,FXint dx=0,FXint dy=0);

        /// Set clip rectangle
        virtual void setClipRectangle(FXint x,FXint y,FXint w,FXint h);

        /// Set clip rectangle
        virtual void setClipRectangle(const FXRectangle& rectangle);

        /// Clear clipping
        virtual void clearClipRectangle();

        /// Set clip mask
        virtual void setClipMask(FXBitmap* mask,FXint dx=0,FXint dy=0);

        /// Clear clip mask
        virtual void clearClipMask();

        /// Set font to draw text with
        virtual void setFont(FXFont *fnt);
        virtual FXFont *getFont(void);

        /// Clip drawing by child windows
        virtual void clipChildren(FXbool yes);

        /// Cleanup
        virtual ~cDCNativePrinter();
};

#endif
