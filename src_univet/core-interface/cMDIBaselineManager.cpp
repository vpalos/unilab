#include <time.h>
#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cBaselineManager.h"
#include "cWinMain.h"
#include "cMDIDatabaseManager.h"
#include "cMDIBaselineManager.h"

cMDIBaselineManager *oMDIBaselineManager=NULL;

FXDEFMAP(cMDIBaselineManager) mapMDIBaselineManager[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIBaselineManager::ID_BASELINEMANAGER,cMDIBaselineManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIBaselineManager::CMD_BUT_CLOSE,cMDIBaselineManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIBaselineManager::CMD_BUT_OPEN,cMDIBaselineManager::onCmdOpen),
    FXMAPFUNC(SEL_COMMAND,cMDIBaselineManager::CMD_BUT_EXPORT,cMDIBaselineManager::onCmdExport),
    FXMAPFUNC(SEL_COMMAND,cMDIBaselineManager::CMD_BUT_IMPORT,cMDIBaselineManager::onCmdImport),
    FXMAPFUNC(SEL_COMMAND,cMDIBaselineManager::CMD_BUT_NEW,cMDIBaselineManager::onCmdNew),
    FXMAPFUNC(SEL_COMMAND,cMDIBaselineManager::CMD_BUT_DUPLICATE,cMDIBaselineManager::onCmdDuplicate),
    FXMAPFUNC(SEL_DOUBLECLICKED,cMDIBaselineManager::ID_BASELINELIST,cMDIBaselineManager::onCmdOpen),
    FXMAPFUNC(SEL_COMMAND,cMDIBaselineManager::CMD_BUT_REMOVE,cMDIBaselineManager::onCmdRemove),

};

FXIMPLEMENT(cMDIBaselineManager,cMDIChild,mapMDIBaselineManager,ARRAYNUMBER(mapMDIBaselineManager))

cMDIBaselineManager::cMDIBaselineManager()
{
}

cMDIBaselineManager::cMDIBaselineManager(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH)
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_BASELINEMANAGER);
    client=prP;
    popup=prPup;

    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(this,LAYOUT_FILL);
    FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe0,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
    new FXLabel(_vframe2,oLanguage->getText("str_basman_baslist_title"));
    FXHorizontalFrame *_hframe1=new FXHorizontalFrame(_vframe2,LAYOUT_FILL,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe4=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|LAYOUT_RIGHT,0,0,0,0,0,0,0,0,0,0);

    basList=new cSortList(_vframe3,this,ID_BASELINELIST,ICONLIST_EXTENDEDSELECT|ICONLIST_DETAILED|LAYOUT_FILL);
    basList->appendHeader(oLanguage->getText("str_basman_basnameh"),NULL,364);
    basList->appendHeader("*",NULL,20);
    oMDIBaselineManager=this;
    this->update();
    if(basList->getNumItems()>0)
    {
        basList->selectItem(0);
        basList->setCurrentItem(0);
    }
    basList->setFocus();

    new FXButton(_vframe4,oLanguage->getText("str_basman_new"),NULL,this,CMD_BUT_NEW,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_FIX_WIDTH,0,0,88,0);
    new FXHorizontalSeparator(_vframe4,LAYOUT_TOP|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_basman_open"),NULL,this,CMD_BUT_OPEN,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X);
    new FXButton(_vframe4,oLanguage->getText("str_basman_duplicate"),NULL,this,CMD_BUT_DUPLICATE,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X);
    new FXHorizontalSeparator(_vframe4,LAYOUT_TOP|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_basman_ex"),NULL,this,CMD_BUT_EXPORT,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X);
    new FXButton(_vframe4,oLanguage->getText("str_basman_im"),NULL,this,CMD_BUT_IMPORT,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X);

    new FXButton(_vframe4,oLanguage->getText("str_but_close"),NULL,this,CMD_BUT_CLOSE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
    new FXHorizontalSeparator(_vframe4,LAYOUT_BOTTOM|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_basman_remove"),NULL,this,CMD_BUT_REMOVE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
}

cMDIBaselineManager::~cMDIBaselineManager()
{
    cMDIBaselineManager::unload();
}

void cMDIBaselineManager::create()
{
    cMDIChild::create();
    show();
}

void cMDIBaselineManager::load(FXMDIClient *prP,FXPopup *prMenu)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(prP,prMenu);
        return;
    }

    if(oMDIBaselineManager!=NULL)
    {
        oMDIBaselineManager->setFocus();
        prP->setActiveChild(oMDIBaselineManager);
        if(oMDIBaselineManager->isMinimized())
            oMDIBaselineManager->restore();
        return;
    }
    new cMDIBaselineManager(prP,oLanguage->getText("str_basman_title"),new FXGIFIcon(oApplicationManager,data_inputs),prMenu,MDI_NORMAL|MDI_TRACKING,10,10,516,300);
    oMDIBaselineManager->create();
    oMDIBaselineManager->setFocus();

}

FXbool cMDIBaselineManager::isLoaded(void)
{
    return (oMDIBaselineManager!=NULL);
}

void cMDIBaselineManager::unload(void)
{
    oMDIBaselineManager=NULL;
}

void cMDIBaselineManager::update(void)
{
    if(!isLoaded())
        return;
    sBaselineObject *res=cBaselineManager::listBaselines();
    if(res==NULL)
        return;
    basList->clearItems();
    for(int i=0;i<cBaselineManager::getBaselineCount();i++)
    basList->appendItem(*(res[i].id)+"\t"+(res[i].solid>0?"* ":""));
    basList->sortItems();
    free(res);
}

void cMDIBaselineManager::cmdRefresh(void)
{
    if(!isLoaded())
        return;
    oMDIBaselineManager->update();
}

long cMDIBaselineManager::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDIBaselineManager::unload();
    close();
    return 1;
}

long cMDIBaselineManager::onCmdNew(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cBaselineManager::newBaseline(client,popup);
    return 1;
}

long cMDIBaselineManager::onCmdOpen(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i,j=0,db=-1;
    for(i=0;i<basList->getNumItems();i++)
        if(basList->isItemSelected(i))
        {
            j++;
            db=i;
            //break;
        }
    if(j!=1)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_basonlyone").text());
        return 1;
    }
    if(db==-1)
        db=basList->getCurrentItem();
    if(db==-1)
        return 1;
    if(cBaselineManager::baselineSolid(basList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasebassolid").text());
        return 1;
    }
    FXString st=basList->getItemText(db).before('\t');
    if(oWinMain->isWindow(st))
        oWinMain->raiseWindow(st);
    else
        cBaselineManager::editBaseline(client,popup,basList->getItemText(db).before('\t'));
    return 1;
}

long cMDIBaselineManager::onCmdDuplicate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i,j=0,db=-1;
    for(i=0;i<basList->getNumItems();i++)
        if(basList->isItemSelected(i))
        {
            j++;
            db=i;
            //break;
        }
    if(j!=1)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_basonlyone").text());
        return 1;
    }
    if(db==-1)
        return 1;
    FXString res2=cBaselineManager::duplicateBaseline(basList->getItemText(db).before('\t'));
    this->update();
    if(res2.empty())
        return 1;
    for(j=0;j<basList->getNumItems();j++)
        if(res2==basList->getItemText(j).before('\t'))
        {
            basList->selectItem(j);
            break;
        }
    return 1;
}

long cMDIBaselineManager::onCmdRemove(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i,j=0,db=-1;
    for(i=0;i<basList->getNumItems();i++)
        if(basList->isItemSelected(i))
        {
            j++;
            db=i;
            //break;
        }
    if(j!=1)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_basonlyone").text());
        return 1;
    }
    if(db==-1)
        return 1;
    if(cBaselineManager::baselineSolid(basList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasebslsolid").text());
        return 1;
    }
    if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_erasebsl").text()))
        return 1;
    cMDIChild *win=oWinMain->getWindow(basList->getItemText(db).before('\t'));
    if(win!=NULL)
        win->close();
    cBaselineManager::removeBaseline(basList->getItemText(db).before('\t'));
    this->update();
    if(db>=basList->getNumItems())
        db=basList->getNumItems()-1;
    if(db>=0)
        basList->selectItem(db);
    return 1;
}

FXString cMDIBaselineManager::getFileNameEx(void)
{
    FXString ret,dir=oApplicationManager->reg().readStringEntry("settings","exportdir",(FXSystem::getHomeDirectory()+PATHSEP).text());
    bool found=false;
    while(!found)
    {
        ret=FXFileDialog::getSaveFilename(oWinMain,oLanguage->getText("str_export_dlgtitle"),dir,oLanguage->getText("str_export_dlgpatt1")+" (*)\n"+oLanguage->getText("str_export_dlgpatt2")+" (*.uv"+oDataLink->getSpecies()+")",1);
        if(!ret.empty())
        {
            dir=FXPath::directory(ret)+PATHSEP;
            oApplicationManager->reg().writeStringEntry("settings","exportdir",dir.text());
        }
        if(ret=="")
            return ret;
        if(FXPath::extension(ret).lower()!=("uv"+oDataLink->getSpecies()))
            ret=ret+".uv"+oDataLink->getSpecies();
        if(FXStat::exists(ret))
        {
            if(MBOX_CLICKED_YES==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_error").text(),oLanguage->getText("str_fileovr").text()))
                found=true;
        }
        else
            found=true;
    }
    return ret;
}

FXString cMDIBaselineManager::getFileNameIm(void)
{
    FXString ret,dir=oApplicationManager->reg().readStringEntry("settings","importdir",(FXSystem::getHomeDirectory()+PATHSEP).text());
    ret=FXFileDialog::getOpenFilename(oWinMain,oLanguage->getText("str_import_dlgtitle"),dir,oLanguage->getText("str_export_dlgpatt1")+" (*)\n"+oLanguage->getText("str_export_dlgpatt2")+" (*.uv"+oDataLink->getSpecies()+")",1);
    if(!ret.empty())
    {
        dir=FXPath::directory(ret)+PATHSEP;
        oApplicationManager->reg().writeStringEntry("settings","importdir",dir.text());
    }
    if((!ret.empty()) && (MBOX_CLICKED_NO==FXMessageBox::warning(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_import_bslconfirm").text())))
        return "";
    return ret;
}

long cMDIBaselineManager::onCmdExport(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i,j=0;
    for(i=0;i<basList->getNumItems();i++)
        if(basList->isItemSelected(i))
            j++;

    if(!j)
        return 1;

    FXString prFile=getFileNameEx();
    if(prFile.empty())
        return true;

    if(prFile==oDataLink->getFilename())
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_expdupl").text());
        return 1;
    }

    cDataLink *link=new cDataLink();
    if(FXStat::exists(prFile))
        FXFile::remove(prFile.text());
    if(!link->open(prFile,true))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_opendberror").text());
        return 1;
    }

    cDataResult *rr;
    for(i=0;i<basList->getNumItems();i++)
        if(basList->isItemSelected(i))
        {
            rr=oDataLink->execute("SELECT * FROM t_baselines WHERE id='"+basList->getItemText(i).before('\t')+"';");
            if(rr->getRowCount())
            {
                link->execute(FXStringFormat("INSERT INTO t_baselines VALUES('%s','%s','%s','%s');",
                                        rr->getCellString(0,0).text(),
                                        rr->getCellString(0,1).text(),
                                        rr->getCellString(0,2).text(),
                                        rr->getCellString(0,3).text()
                                        ).text());
            }
            delete rr;
        }

    FXchar ftime[100];
    time_t curtime;
    struct tm *loctime;
    curtime=time(NULL);
    loctime=localtime(&curtime);
    strftime(ftime,100,"%Y-%m-%d %H:%M:%S",loctime);
    FXString nowDate(ftime);
    link->setProperty("database_name",oLanguage->getText("str_export_bsldbname")+FXStringFormat(" \"%s\"",oDataLink->getName().text())+" - "+nowDate);
    link->setProperty("database_species",oDataLink->getSpecies());
    link->setProperty("database_comments","");
    link->setProperty("database_owner",FXPath::title(prFile));

    link->close();

    FXMessageBox::information(oWinMain,MBOX_OK,oLanguage->getText("str_information").text(),oLanguage->getText("str_success").text());
    return 1;
}

long cMDIBaselineManager::onCmdImport(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i;

    FXString prFile=getFileNameIm();
    if(prFile.empty())
        return true;

    if(prFile==oDataLink->getFilename())
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_impdupl").text());
        return 1;
    }

    cDataLink *link=new cDataLink();
    if(!link->open(prFile,true))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_opendberror").text());
        return 1;
    }

    FXString rules_defs=link->getProperty("database_version");
    FXuint version=0,v1=0,v2=0,v3=0;

    v1=FXIntVal(rules_defs.before('.'));
    rules_defs=rules_defs.after('.');
    v2=FXIntVal(rules_defs.before('.'));
    rules_defs=rules_defs.after('.');
    v3=FXIntVal(rules_defs);
    version=FXIntVal(FXStringFormat("1%03d%03d%03d",v1,v2,v3));

    if(version>1001000000)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_dbnew_version").text());
        return false;
    }

    cDataResult *rr;

    rr=link->execute("SELECT * FROM t_baselines;");
    for(i=0;i<rr->getRowCount();i++)
    {
        FXString name=rr->getCellString(i,0);
        int ct=1;
        if(cBaselineManager::baselineExists(name))
            while(cBaselineManager::baselineExists(name=rr->getCellString(i,0)+"_"+FXStringVal(ct++)));

        oDataLink->execute(FXStringFormat("INSERT INTO t_baselines VALUES('%s','%s','%s','%s');",
                                name.text(),
                                rr->getCellString(0,1).text(),
                                rr->getCellString(0,2).text(),
                                rr->getCellString(0,3).text()
                                ).text());
    }
    delete rr;

    link->close();

    cMDIBaselineManager::cmdRefresh();

    FXMessageBox::information(oWinMain,MBOX_OK,oLanguage->getText("str_information").text(),oLanguage->getText("str_success").text());
    return 1;
}


