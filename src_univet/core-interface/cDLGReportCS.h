#ifndef CDLGREPORTCS_H
#define CDLGREPORTCS_H

#include <fx.h>

class cDLGReportCS : public FXDialogBox
{
    FXDECLARE(cDLGReportCS);
    private:
        FXButton *butAccept;
        FXButton *butCancel;
        
    protected:

        cDLGReportCS(){}
        
    public:
        FXuint choice;
        FXDataTarget chtype;
        FXCheckButton *bcCala;
        FXCheckButton *bcCalb;
        FXCheckButton *bcTiter;
        FXCheckButton *bcAm;
        FXCheckButton *bcLog2;
        FXCheckButton *bcEU;
        
        FXCheckButton *bUseData;
        FXCheckButton *bUseInfo;
    
        FXRadioButton *bTitGraphs;
        FXRadioButton *bSpGraphs;
    
        FXuint agchoice;
        FXDataTarget agtype;
        FXRadioButton *bAMean;
        FXRadioButton *bGMean;
        
        FXuint apchoice;
        FXDataTarget aptype;
        FXRadioButton *bAssay;
        FXRadioButton *bPop;
        
        FXListBox *bsl;
        FXCheckButton *bSS;
    
        cDLGReportCS(FXWindow *prOwner,FXbool areTiters,FXbool areSecondary);
        virtual ~cDLGReportCS();
        
        enum
        {
            ID_THIS=FXDialogBox::ID_LAST,
            ID_UPDATE,
            ID_BSLCHK,
            
            CMD_BUT_APPLY,
            ID_LAST
        };

        long onUpdBslchk(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdUpdate(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdApply(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
