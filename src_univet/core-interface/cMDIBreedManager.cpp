#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cBreedManager.h"
#include "cWinMain.h"
#include "cMDIDatabaseManager.h"
#include "cMDIBreedManager.h"

cMDIBreedManager *oMDIBreedManager=NULL;

FXDEFMAP(cMDIBreedManager) mapMDIBreedManager[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIBreedManager::ID_BREEDMANAGER,cMDIBreedManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIBreedManager::CMD_BUT_CLOSE,cMDIBreedManager::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIBreedManager::CMD_BUT_OPEN,cMDIBreedManager::onCmdOpen),
    FXMAPFUNC(SEL_COMMAND,cMDIBreedManager::CMD_BUT_NEW,cMDIBreedManager::onCmdNew),
    FXMAPFUNC(SEL_COMMAND,cMDIBreedManager::CMD_BUT_DUPLICATE,cMDIBreedManager::onCmdDuplicate),
    FXMAPFUNC(SEL_DOUBLECLICKED,cMDIBreedManager::ID_BREEDLIST,cMDIBreedManager::onCmdOpen),
    FXMAPFUNC(SEL_COMMAND,cMDIBreedManager::CMD_BUT_REMOVE,cMDIBreedManager::onCmdRemove),
    
};

FXIMPLEMENT(cMDIBreedManager,cMDIChild,mapMDIBreedManager,ARRAYNUMBER(mapMDIBreedManager))

cMDIBreedManager::cMDIBreedManager()
{
}

cMDIBreedManager::cMDIBreedManager(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH) 
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_BREEDMANAGER);
    client=prP;
    popup=prPup;
    
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(this,LAYOUT_FILL);
    FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe0,LAYOUT_FILL,0,0,0,0,0,0,0,0,0,0);
    new FXLabel(_vframe2,oLanguage->getText("str_breman_brelist_title"));
    FXHorizontalFrame *_hframe1=new FXHorizontalFrame(_vframe2,LAYOUT_FILL,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
    FXVerticalFrame *_vframe4=new FXVerticalFrame(_hframe1,LAYOUT_FILL_Y|LAYOUT_RIGHT,0,0,0,0,0,0,0,0,0,0);
    
    breList=new cSortList(_vframe3,this,ID_BREEDLIST,ICONLIST_SINGLESELECT|ICONLIST_DETAILED|LAYOUT_FILL);
    breList->appendHeader(oLanguage->getText("str_breman_brenameh"),NULL,120);
    breList->appendHeader(oLanguage->getText("str_breman_bretitleh"),NULL,264);
    oMDIBreedManager=this;
    this->update();
    if(breList->getNumItems()>0)
    {
        breList->selectItem(0);
        breList->setCurrentItem(0);
    }
    breList->setFocus();

    new FXButton(_vframe4,oLanguage->getText("str_breman_new"),NULL,this,CMD_BUT_NEW,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_FIX_WIDTH,0,0,88,0);
    new FXHorizontalSeparator(_vframe4,LAYOUT_TOP|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_breman_open"),NULL,this,CMD_BUT_OPEN,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X);
    new FXButton(_vframe4,oLanguage->getText("str_breman_duplicate"),NULL,this,CMD_BUT_DUPLICATE,LAYOUT_TOP|ICON_BEFORE_TEXT|JUSTIFY_NORMAL|FRAME_RAISED|LAYOUT_FILL_X);
    new FXButton(_vframe4,oLanguage->getText("str_but_close"),NULL,this,CMD_BUT_CLOSE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
    new FXHorizontalSeparator(_vframe4,LAYOUT_BOTTOM|LAYOUT_FIX_HEIGHT,0,0,0,10);
    new FXButton(_vframe4,oLanguage->getText("str_breman_remove"),NULL,this,CMD_BUT_REMOVE,LAYOUT_BOTTOM|FRAME_RAISED|JUSTIFY_NORMAL|LAYOUT_FILL_X);
}

cMDIBreedManager::~cMDIBreedManager()
{
    cMDIBreedManager::unload();
}

void cMDIBreedManager::create()
{
    cMDIChild::create();
    show();
}

void cMDIBreedManager::load(FXMDIClient *prP,FXPopup *prMenu)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(prP,prMenu);
        return;
    }
    
    if(oMDIBreedManager!=NULL)
    {
        oMDIBreedManager->setFocus();
        prP->setActiveChild(oMDIBreedManager);
        if(oMDIBreedManager->isMinimized())
            oMDIBreedManager->restore();
        return;
    }
    new cMDIBreedManager(prP,oLanguage->getText("str_breman_title"),new FXGIFIcon(oApplicationManager,data_inputs),prMenu,MDI_NORMAL|MDI_TRACKING,10,10,515,200);
    oMDIBreedManager->create();
    oMDIBreedManager->setFocus();

}

FXbool cMDIBreedManager::isLoaded(void)
{
    return (oMDIBreedManager!=NULL);
}

void cMDIBreedManager::unload(void)
{
    oMDIBreedManager=NULL;
}

void cMDIBreedManager::update(void)
{
    if(!isLoaded())
        return;
    sBreedObject *res=cBreedManager::listBreeds();
    if(res==NULL)
        return;
    breList->clearItems();
    for(int i=0;i<cBreedManager::getBreedCount();i++)
    breList->appendItem(*(res[i].id)+"\t"+(res[i].solid>0?"* ":"")+*(res[i].title));
    breList->sortItems();
    free(res);
}

void cMDIBreedManager::cmdRefresh(void)
{
    if(!isLoaded())
        return;
    oMDIBreedManager->update();
}

long cMDIBreedManager::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cMDIBreedManager::unload();
    close();
    return 1;
}

long cMDIBreedManager::onCmdNew(FXObject *prSender,FXSelector prSelector,void *prData)
{
    cBreedManager::newBreed(client,popup);
    return 1;
}

long cMDIBreedManager::onCmdOpen(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int db=-1,i;
    for(i=0;i<breList->getNumItems();i++)
        if(breList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        db=breList->getCurrentItem();
    if(db==-1)
        return 1;
    if(cBreedManager::breedSolid(breList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasebresolid").text());
        return 1;
    }
    FXString st=breList->getItemText(db).before('\t');
    if(oWinMain->isWindow(st))
        oWinMain->raiseWindow(st);
    else
        cBreedManager::editBreed(client,popup,st);
    return 1;
}

long cMDIBreedManager::onCmdDuplicate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int db=-1,i,j;
    for(i=0;i<breList->getNumItems();i++)
        if(breList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        return 1;
    FXString res2=cBreedManager::duplicateBreed(breList->getItemText(db).before('\t').text());
    this->update();
    if(res2.empty())
        return 1;
    for(j=0;j<breList->getNumItems();j++)
        if(res2==breList->getItemText(j).before('\t'))
        {
            breList->selectItem(j);
            break;
        }
    return 1;
}

long cMDIBreedManager::onCmdRemove(FXObject *prSender,FXSelector prSelector,void *prData)
{
    int i,db=-1;
    for(i=0;i<breList->getNumItems();i++)
        if(breList->isItemSelected(i))
        {
            db=i;
            break;
        }
    if(db==-1)
        return 1;
    if(cBreedManager::breedSolid(breList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasebresolid").text());
        return 1;
    }
    if(cBreedManager::breedNeeded(breList->getItemText(db).before('\t')))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_erasedataneeded").text());
        return 1;
    }
    if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_erasebre").text()))
        return 1;
    cMDIChild *win=oWinMain->getWindow(breList->getItemText(db).before('\t'));
    if(win!=NULL)
        win->close();
    cBreedManager::removeBreed(breList->getItemText(db).before('\t'));
    this->update();
    if(db>=breList->getNumItems())
        db=breList->getNumItems()-1;
    if(db>=0)
        breList->selectItem(db);
    return 1;
}


