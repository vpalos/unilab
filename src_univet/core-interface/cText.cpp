#include "cText.h"

FXDEFMAP(cText) mapText[]=
{
    FXMAPFUNC(SEL_PAINT,0,cText::onPaint),
};

FXIMPLEMENT(cText,FXText,mapText,ARRAYNUMBER(mapText))

cText::cText()
{
}

cText::cText(FXComposite *p, FXObject *tgt, FXSelector sel, FXuint opts, FXint x, FXint y, FXint w, FXint h, FXint pl, FXint pr, FXint pt, FXint pb):
    FXText(p,tgt,sel,opts,x,y,w,h,pl,pr,pt,pb)
{
}

cText::~cText()
{
}

long cText::onPaint(FXObject *prSender,FXSelector prSelector,void *prData)
{
  FXDC *dc;
  FXEvent *event;
  
  if(prSender==NULL && prSelector==0)
      dc=(FXDC*)prData;
  else
  {
      event=(FXEvent*)prData;
      dc=new FXDCWindow(this,event);
  }
  
  dc->clearClipRectangle();
  dc->setFont(font);
  //dc->setForeground(FXRGB(255,0,0));
  //dc->fillRectangle(event->rect.x,event->rect.y,event->rect.w,event->rect.h);

  if(prSender==NULL && prSelector==0)
  {
    // Paint top margin
    dc->setForeground(backColor);
    dc->fillRectangle(barwidth,0,viewport_w-barwidth,margintop);
    
    // Paint bottom margin
    dc->setForeground(backColor);
    dc->fillRectangle(barwidth,viewport_h-marginbottom,viewport_w-barwidth,marginbottom);
    
    // Paint left margin
    dc->setForeground(backColor);
    dc->fillRectangle(barwidth,margintop,marginleft,viewport_h-margintop-marginbottom);
    
    // Paint right margin
    dc->setForeground(backColor);
    dc->fillRectangle(viewport_w-marginright,margintop,marginright,viewport_h-margintop-marginbottom);
    
    if(barwidth)
    {
        // Paint line numbers
        dc->setClipRectangle(0,0,barwidth,height);
        drawNumbers(*(FXDCWindow*)dc,0,0,width,height);
    }
    
    // Paint text
    dc->setClipRectangle(marginleft+barwidth,margintop,viewport_w-marginright-marginleft-barwidth,viewport_h-margintop-marginbottom);
    drawContents(*(FXDCWindow*)dc,0,0,viewport_w-marginright-marginleft-barwidth,viewport_h-margintop-marginbottom);
  }
  else
  {
      // Paint top margin
      if(event->rect.y<=margintop){
        dc->setForeground(backColor);
        dc->fillRectangle(barwidth,0,viewport_w-barwidth,margintop);
        }
    
      // Paint bottom margin
      if(event->rect.y+event->rect.h>=viewport_h-marginbottom){
        dc->setForeground(backColor);
        dc->fillRectangle(barwidth,viewport_h-marginbottom,viewport_w-barwidth,marginbottom);
        }
    
      // Paint left margin
      if(event->rect.x<barwidth+marginleft){
        dc->setForeground(backColor);
        dc->fillRectangle(barwidth,margintop,marginleft,viewport_h-margintop-marginbottom);
        }
    
      // Paint right margin
      if(event->rect.x+event->rect.w>=viewport_w-marginright){
        dc->setForeground(backColor);
        dc->fillRectangle(viewport_w-marginright,margintop,marginright,viewport_h-margintop-marginbottom);
        }
    
      // Paint line numbers
      if(event->rect.x<barwidth){
        dc->setClipRectangle(0,0,barwidth,height);
        drawNumbers(*(FXDCWindow*)dc,event->rect.x,event->rect.y,event->rect.w,event->rect.h);
        }
    
      // Paint text
      dc->setClipRectangle(marginleft+barwidth,margintop,viewport_w-marginright-marginleft-barwidth,viewport_h-margintop-marginbottom);
      drawContents(*(FXDCWindow*)dc,event->rect.x,event->rect.y,event->rect.w,event->rect.h);
    
      // FIXME make sure its drawn if we have focus
      drawCursor(flags);
  }
  
  if(dc  && (prSender!=NULL || prSelector!=0))
    delete dc;
  return 1;
}

