#include <time.h>
#include <ctype.h>
#include <stdlib.h>

#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cColorsManager.h"
#include "cFormulaRatio.h"
#include "cSpeciesManager.h"
#include "cAssayManager.h"
#include "cCaseManager.h"
#include "cPopulationManager.h"
#include "cWinMain.h"
#include "cMDIAssay.h"
#include "cMDIAssayManager.h"
#include "cMDICaseManager.h"

#define ASSAY_TTRCOUNT 30
#define ASSAY_BINCOUNT 30

FXDEFMAP(cMDIAssay) mapMDIAssay[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIAssay::ID_MDIASSAY,cMDIAssay::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYID,cMDIAssay::onCmdTfId),
    FXMAPFUNC(SEL_CHANGED,cMDIAssay::ID_ASYID,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYTITLE,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYFILTERA,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYFILTERB,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIAssay::ID_ASYCALA,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_UPDATE,cMDIAssay::ID_ASYCALB,cMDIAssay::onUpdCalBCheck),
    FXMAPFUNC(SEL_UPDATE,cMDIAssay::ID_ASYTITERS,cMDIAssay::onUpdTitersCheck),
    FXMAPFUNC(SEL_CHANGED,cMDIAssay::ID_ASYCALB,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIAssay::ID_ASYCALT,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIAssay::ID_ASYCOMMENTS,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYCUTA,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYCUTB,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYSUSA,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYSUSB,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYDIRA,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYDIRB,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_RULES,cMDIAssay::onCmdRules),
    FXMAPFUNC(SEL_UPDATE,cMDIAssay::ID_RULES,cMDIAssay::onUpdRules),
    FXMAPFUNC(SEL_SELECTED,cMDIAssay::ID_ASYFACTORS,cMDIAssay::onSelTbFactors),
    FXMAPFUNC(SEL_REPLACED,cMDIAssay::ID_ASYFACTORS,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIAssay::ID_ASYFACTORS,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYSLOPE,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYINTER,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYCALBCHECK,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::ID_ASYTITERSCHECK,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_REPLACED,cMDIAssay::ID_ASYTITERS,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_REPLACED,cMDIAssay::ID_ASYBINS,cMDIAssay::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::CMD_CLOSE,cMDIAssay::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::CMD_SAVE,cMDIAssay::onCmdSave),
    FXMAPFUNC(SEL_COMMAND,cMDIAssay::CMD_PRINT,cMDIAssay::onCmdPrint),
};

FXIMPLEMENT(cMDIAssay,cMDIChild,mapMDIAssay,ARRAYNUMBER(mapMDIAssay))

cMDIAssay::cMDIAssay()
{
}

FXbool cMDIAssay::updateData(void)
{
    saved=false;

    FXdouble v;
    if(tfFilterA->getText().empty())
        tfFilterA->setText("410");
    if(tfFilterB->getText().empty())
        tfFilterB->setText("0");
    if(tfCutoffA->getText().empty())
        tfCutoffA->setText("0");
    if(tfCutoffB->getText().empty())
        tfCutoffB->setText("0");
    if(tfSuspectA->getText().empty())
        tfSuspectA->setText("0");

    int r,a;
    double y;
    ClearAllVars();
    y=999999;
    SetValue("na",&y);
    y=9999999;
    SetValue("pa",&y);
    r=Evaluate((char*)tfCutoffA->getText().text(),&y,&a);
    if(r)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalidexp").text());
        //tfCutoffA->setText("0");
        return true;
    }
    r=Evaluate((char*)tfCutoffB->getText().text(),&y,&a);
    if(r)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalidexp").text());
        //tfCutoffB->setText("0");
        return true;
    }
    r=Evaluate((char*)tfSuspectA->getText().text(),&y,&a);
    if(r)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalidexp").text());
        //tfSuspectA->setText("0");
        return true;
    }
    r=Evaluate((char*)tfSuspectB->getText().text(),&y,&a);
    if(r)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalidexp").text());
        //tfSuspectB->setText("0");
        return true;
    }
    ClearAllVars();

    for(int i=1;i<4;i++)
    {
        v=FXIntVal(tbFactors->getItemText(i,0));
        if(v<=0)v=0;
        if(v>1000)v=1000;
        tbFactors->setItemText(i,0,FXStringVal(v));
    }

    for(int i=0;i<ASSAY_TTRCOUNT;i++)
    {
        v=FXIntVal(tbTiters->getItemText(i,0));
        if(v<=0)v=0;
        if(v>999999)v=999999;
        tbTiters->setItemText(i,0,FXStringVal(v));
        v=FXFloatVal(tbBins->getItemText(i,0));
        if(v<=0)v=0;
        if(v>999999)v=999999;
        tbBins->setItemText(i,0,FXStringVal(v));
    }

    v=FXFloatVal(tfTiterSlope->getText());
    if(v<0)v=0;
    if(v>999999)v=999999;
    tfTiterSlope->setText(FXStringVal(v));

    v=FXFloatVal(tfTiterIntercept->getText());
    if(v<0)v=0;
    if(v>999999)v=999999;
    tfTiterIntercept->setText(FXStringVal(v));

    return false;
}

FXbool cMDIAssay::saveData(void)
{
    tbFactors->acceptInput(true);
    tbTiters->acceptInput(true);
    tbBins->acceptInput(true);

    if(updateData())
        return false;

    if(saved)
        return true;

    FXdouble v=1,v1,v2,v3;
    int p=1;
    while(v!=0)
    {
        v=0;
        for(int i=0;i<tbTiters->getNumRows()-p;i++)
        {
            v1=FXIntVal(tbTiters->getItemText(i,0));
            v2=FXIntVal(tbTiters->getItemText(i+1,0));
            if(v1==0 && v2==0)
                continue;
            if((v1>v2 && v1!=0 && v2!=0) || (v1==0))
            {
                v3=v1;
                v1=v2;
                v2=v3;
                tbTiters->setItemText(i,0,FXStringVal(v1));
                tbTiters->setItemText(i+1,0,FXStringVal(v2));
                v=1;
            }
        }
        //p++;
    }
    p=1,v=1;
    while(v!=0)
    {
        v=0;
        for(int i=0;i<tbBins->getNumRows()-p;i++)
        {
            v1=FXFloatVal(tbBins->getItemText(i,0));
            v2=FXFloatVal(tbBins->getItemText(i+1,0));
            if(v1==0 && v2==0)
                continue;
            if((v1>v2 && v1!=0 && v2!=0) || (v1==0))
            {
                v3=v1;
                v1=v2;
                v2=v3;
                tbBins->setItemText(i,0,FXStringVal(v1));
                tbBins->setItemText(i+1,0,FXStringVal(v2));
                v=1;
            }
        }
        //p++;
    }

    if(!name.empty() && cAssayManager::assaySolid(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_eraseasysolid").text());
        saved=true;
        return 1;
    }

    FXString oldname=name;
    name=tfId->getText();
    if(name!=oldname && cAssayManager::assayExists(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
        tfId->setText(cAssayManager::getNewID());
        setTitle(tfId->getText());
        updateData();
        return false;
    }
    this->setTitle(name);

    FXString factors_defs="",calculations_defs,titers_defs,bins_defs="";

    for(int i=0;i<4;i++)
        factors_defs=factors_defs+tbFactors->getItemText(i,0)+"\t";

    calculations_defs=cbCalculusB->getCheck()?"2\n":"1\n";
    calculations_defs=calculations_defs+lxCalculusA->getItem(lxCalculusA->getCurrentItem()).before('=').trim()+"\t";
    calculations_defs=calculations_defs+tfCutoffA->getText()+"\t";
    calculations_defs=calculations_defs+lxOperatorA->getItem(lxOperatorA->getCurrentItem())+"\t";
    calculations_defs=calculations_defs+tfSuspectA->getText()+"\n";
    if(cbCalculusB->getCheck())
    {
        calculations_defs=calculations_defs+lxCalculusB->getItem(lxCalculusB->getCurrentItem()).before('=').trim()+"\t";
        calculations_defs=calculations_defs+tfCutoffB->getText()+"\t";
        calculations_defs=calculations_defs+lxOperatorB->getItem(lxOperatorB->getCurrentItem())+"\t";
        calculations_defs=calculations_defs+tfSuspectB->getText()+"\n";
    }

    titers_defs=cbTiters->getCheck()==true?"1\t":"0\t";
    titers_defs=titers_defs+tfTiterSlope->getText()+"\t";
    titers_defs=titers_defs+tfTiterIntercept->getText()+"\t";
    titers_defs=titers_defs+lxCalculusT->getItem(lxCalculusT->getCurrentItem())+"\n";
    for(int i=0;i<tbTiters->getNumRows();i++)
        titers_defs=titers_defs+tbTiters->getItemText(i,0)+"\t";
    for(int i=0;i<tbBins->getNumRows();i++)
        bins_defs=bins_defs+tbBins->getItemText(i,0)+"\t";

    FXString rules_defs="";

    if(cbR1->getCheck())
        rules_defs=rules_defs+"Na\t"+opListR1->getItemText(opListR1->getCurrentItem())+"\t"+valueR1->getText()+"\n";
    if(cbR2->getCheck())
        rules_defs=rules_defs+"Pa\t"+opListR2->getItemText(opListR2->getCurrentItem())+"\t"+valueR2->getText()+"\n";
    if(cbR3->getCheck())
        rules_defs=rules_defs+"Pa-Na\t"+opListR3->getItemText(opListR3->getCurrentItem())+"\t"+valueR3->getText()+"\n";
    if(cbR4->getCheck())
        rules_defs=rules_defs+"Nha\t"+opListR4->getItemText(opListR4->getCurrentItem())+"\t"+valueR4->getText()+"\n";
    if(cbR5->getCheck())
        rules_defs=rules_defs+"Pha\t"+opListR5->getItemText(opListR5->getCurrentItem())+"\t"+valueR5->getText()+"\n";

    if(oldname.empty())
        cAssayManager::addAssay(name,tfTitle->getText(),tfFilterA->getText(),tfFilterB->getText(),factors_defs,calculations_defs,titers_defs,bins_defs,comments->getText(),rules_defs);
    else
        cAssayManager::setAssay(oldname,name,tfTitle->getText(),tfFilterA->getText(),tfFilterB->getText(),factors_defs,calculations_defs,titers_defs,bins_defs,comments->getText(),rules_defs);

    saved=true;
    cMDIAssayManager::cmdRefresh();
    cMDICaseManager::cmdRefresh();
    cMDICaseManager::cmdResearch();
    return true;
}

cMDIAssay::cMDIAssay(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH)
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_MDIASSAY);
    saved=false;
    name="";

    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL,0,0,0,0);

    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
        FXVerticalFrame *_vframe1=new FXVerticalFrame(_hframe0,0,0,0,0,0,0,0,0,0);
            new FXLabel(_vframe1,oLanguage->getText("str_asymdi_id"),NULL,LAYOUT_CENTER_X);
            tfId=new FXTextField(_vframe1,15,this,ID_ASYID);
                tfId->setText(prName);
                tfId->setJustify(JUSTIFY_CENTER_X);
                tfId->setSelection(0,tfId->getText().length());
                tfId->setFocus();
        FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe0,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
            new FXLabel(_vframe2,oLanguage->getText("str_asymdi_title"));
            tfTitle=new FXTextField(_vframe2,0,this,ID_ASYTITLE,LAYOUT_FILL_X|TEXTFIELD_NORMAL);
                tfTitle->setText(oLanguage->getText("str_asymdi_asytitle"));
        FXVerticalFrame *_vframe3=new FXVerticalFrame(_hframe0,0,0,0,0,0,0,0,0,0);
            new FXLabel(_vframe3,oLanguage->getText("str_asymdi_filtera"));
            tfFilterA=new FXTextField(_vframe3,10,this,ID_ASYFILTERA,FRAME_SUNKEN|FRAME_THICK|TEXTFIELD_INTEGER);
                tfFilterA->setText("410");
                tfFilterA->setJustify(JUSTIFY_CENTER_X);
                tfFilterA->setBackColor(FXRGB(245,245,255));
                tfFilterA->setTextColor(FXRGB(255,0,0));
        FXVerticalFrame *_vframe4=new FXVerticalFrame(_hframe0,0,0,0,0,0,0,0,0,0);
            new FXLabel(_vframe4,oLanguage->getText("str_asymdi_filterb"));
            tfFilterB=new FXTextField(_vframe4,10,this,ID_ASYFILTERB,FRAME_SUNKEN|FRAME_THICK|TEXTFIELD_INTEGER);
                tfFilterB->setText("0");
                tfFilterB->setJustify(JUSTIFY_CENTER_X);
                tfFilterB->setBackColor(FXRGB(245,245,255));
                tfFilterB->setTextColor(FXRGB(255,0,0));
    int ch=0;
    sFormulaRatioObject *res=cFormulaRatio::listFormulaRatio();
    //new FXHorizontalSeparator(_vframe0,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT,0,0,3,3);
    FXGroupBox *_gbox0=new FXGroupBox(_vframe0,oLanguage->getText("str_asymdi_calculus"),GROUPBOX_TITLE_CENTER|LAYOUT_FILL_X|FRAME_GROOVE);
    _gbox0->setTextColor(FXRGB(150,150,150));
        FXHorizontalFrame *_hframe1=new FXHorizontalFrame(_gbox0,LAYOUT_FILL_X,0,0,0,0,0,0,0,0,0,0);
            FXVerticalFrame *_vframe5=new FXVerticalFrame(_hframe1,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                new FXLabel(_vframe5,oLanguage->getText("str_asymdi_cala"));
                lxCalculusA=new FXListBox(_vframe5,this,ID_ASYCALA,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK);
                lxCalculusA->setNumVisible(cFormulaRatio::getCount());
                ch=0;
                for(int i=0;i<cFormulaRatio::getCount();i++)
                {
                    if(res[i].flags&FORMULA_DEFAULT)
                        ch=i;
                    if(res[i].flags&FORMULA_PRICAL)
                        lxCalculusA->appendItem(FXStringFormat("%s  =  %s",res[i].id->text(),res[i].title->text()));
                }
                lxCalculusA->setCurrentItem(ch);
                cbCalculusB=new FXCheckButton(_vframe5,oLanguage->getText("str_asymdi_calb"),this,ID_ASYCALBCHECK,CHECKBUTTON_NORMAL,0,0,0,0,0);
                lxCalculusB=new FXListBox(_vframe5,this,ID_ASYCALB,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK);
                lxCalculusB->setNumVisible(cFormulaRatio::getCount());
                ch=0;
                for(int i=0;i<cFormulaRatio::getCount();i++)
                {
                    if(res[i].flags&FORMULA_DEFAULT)
                        ch=i;
                    if(res[i].flags&FORMULA_SECCAL)
                        lxCalculusB->appendItem(FXStringFormat("%s  =  %s",res[i].id->text(),res[i].title->text()));
                }
                lxCalculusB->setCurrentItem(ch);
            FXMatrix *_matrix1=new FXMatrix(_hframe1,3,MATRIX_BY_COLUMNS|LAYOUT_RIGHT,0,0,0,0,0,0,0,0);
                new FXLabel(_matrix1,oLanguage->getText("str_asymdi_cutoff"),NULL,LAYOUT_CENTER_X);
                new FXLabel(_matrix1,oLanguage->getText("str_asymdi_direction"),NULL,LAYOUT_CENTER_X);
                new FXLabel(_matrix1,oLanguage->getText("str_asymdi_suspect"),NULL,LAYOUT_CENTER_X);
                tfCutoffA=new FXTextField(_matrix1,10,this,ID_ASYCUTA,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK);
                tfCutoffA->setJustify(JUSTIFY_CENTER_X);
                tfCutoffA->setText("0");
                tfCutoffA->setBackColor(FXRGB(235,255,230));
                lxOperatorA=new FXListBox(_matrix1,this,ID_ASYDIRA,FRAME_SUNKEN|FRAME_THICK);
                lxOperatorA->setTextColor(FXRGB(255,0,0));
                lxOperatorA->appendItem(">=");
                lxOperatorA->appendItem("<=");
                tfSuspectA=new FXTextField(_matrix1,10,this,ID_ASYSUSA,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK);
                tfSuspectA->setJustify(JUSTIFY_CENTER_X);
                tfSuspectA->setText("0");
                tfSuspectA->setBackColor(FXRGB(235,255,230));
                new FXLabel(_matrix1,oLanguage->getText("str_asymdi_cutoff"),NULL,LAYOUT_CENTER_X);
                new FXLabel(_matrix1,oLanguage->getText("str_asymdi_direction"),NULL,LAYOUT_CENTER_X);
                new FXLabel(_matrix1,oLanguage->getText("str_asymdi_suspect"),NULL,LAYOUT_CENTER_X);
                tfCutoffB=new FXTextField(_matrix1,10,this,ID_ASYCUTB,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK);
                tfCutoffB->setJustify(JUSTIFY_CENTER_X);
                tfCutoffB->setText("0");
                tfCutoffB->setBackColor(FXRGB(235,255,230));
                lxOperatorB=new FXListBox(_matrix1,this,ID_ASYDIRB,FRAME_SUNKEN|FRAME_THICK);
                lxOperatorB->setTextColor(FXRGB(255,0,0));
                lxOperatorB->appendItem(">=");
                lxOperatorB->appendItem("<=");
                tfSuspectB=new FXTextField(_matrix1,10,this,ID_ASYSUSB,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK);
                tfSuspectB->setJustify(JUSTIFY_CENTER_X);
                tfSuspectB->setText("0");
                tfSuspectB->setBackColor(FXRGB(235,255,230));

    FXHorizontalFrame *_hframe2=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,5,0);
        FXGroupBox *_gbox1=new FXGroupBox(_hframe2,oLanguage->getText("str_asymdi_factors"),GROUPBOX_TITLE_LEFT|FRAME_GROOVE);
        _gbox1->setTextColor(FXRGB(150,150,150));
            FXHorizontalFrame *_hframe3=new FXHorizontalFrame(_gbox1,FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
                tbFactors=new cColorTable(_hframe3,this,ID_ASYFACTORS,TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT);
                tbFactors->setTableSize(4,1);
                for(int i=0;i<4;i++)
                {
                    tbFactors->setRowText(i,FXStringFormat("x%d",i+1));
                    tbFactors->getRowHeader()->setItemJustify(i,JUSTIFY_CENTER_X);
                    tbFactors->setItemText(i,0,FXStringVal(100*(i+1)));
                    tbFactors->setItemJustify(i,0,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                }
                tbFactors->setColumnText(0,oLanguage->getText("str_asymdi_precentage"));
                tbFactors->getColumnHeader()->setItemJustify(0,JUSTIFY_CENTER_X);
                tbFactors->setRowHeaderWidth(30);
                tbFactors->setColumnWidth(0,70);
                tbFactors->showVertGrid(false);
                tbFactors->setScrollStyle(HSCROLLING_OFF|HSCROLLER_NEVER|VSCROLLING_OFF|VSCROLLER_NEVER);
                tbFactors->setCellEditable(0,0,false);
                tbFactors->setSelectable(false);
                tbFactors->setItemStipple(0,0,STIPPLE_GRAY);
                tbFactors->setStippleColor(FXRGB(230,230,200));

        FXGroupBox *_gbox2=new FXGroupBox(_hframe2,oLanguage->getText("str_asymdi_titers"),GROUPBOX_TITLE_LEFT|LAYOUT_FILL_X|FRAME_GROOVE);
        _gbox2->setTextColor(FXRGB(150,150,150));
            FXHorizontalFrame *_hframe4=new FXHorizontalFrame(_gbox2,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                FXVerticalFrame *_vframe6=new FXVerticalFrame(_hframe4,LAYOUT_LEFT,0,0,0,0,0,0,0,0);
                    cbTiters=new FXCheckButton(_vframe6,oLanguage->getText("str_asymdi_titerscheck"),this,ID_ASYTITERSCHECK,CHECKBUTTON_NORMAL,0,0,0,0,0);
                    FXTextField *_tf0=new FXTextField(_vframe6,1,NULL,0,FRAME_NONE|TEXTFIELD_READONLY);
                    new FXLabel(_vframe6,oLanguage->getText("str_asymdi_calt"));
                    lxCalculusT=new FXListBox(_vframe6,this,ID_ASYCALT,LAYOUT_FILL_X|FRAME_SUNKEN|FRAME_THICK|LAYOUT_FIX_WIDTH,0,0,101,0);
                    lxCalculusT->setNumVisible(cFormulaRatio::getCount());
                    ch=0;
                    for(int i=0;i<cFormulaRatio::getCount();i++)
                    {
                        if(res[i].flags&FORMULA_DEFAULT)
                            ch=i;
                        if(res[i].flags&FORMULA_TITERS)
                            lxCalculusT->appendItem(res[i].id->text());
                    }
                    lxCalculusT->setCurrentItem(ch);
                    _tf0->setBackColor(getBaseColor());
                new FXVerticalSeparator(_hframe4,SEPARATOR_GROOVE|LAYOUT_FILL_Y,0,0,0,0,3,3,0,0);
                FXVerticalFrame *_vframe7=new FXVerticalFrame(_hframe4,LAYOUT_LEFT,0,0,0,0,0,0,0,0);
                    new FXLabel(_vframe7,oLanguage->getText("str_asymdi_slope"));
                    tfTiterSlope=new FXTextField(_vframe7,7,this,ID_ASYSLOPE,FRAME_SUNKEN|FRAME_THICK|TEXTFIELD_REAL);
                    tfTiterSlope->setText("0");
                    tfTiterSlope->setJustify(JUSTIFY_CENTER_X);
                    new FXLabel(_vframe7,oLanguage->getText("str_asymdi_inter"));
                    tfTiterIntercept=new FXTextField(_vframe7,7,this,ID_ASYINTER,FRAME_SUNKEN|FRAME_THICK|TEXTFIELD_REAL);
                    tfTiterIntercept->setText("0");
                    tfTiterIntercept->setJustify(JUSTIFY_CENTER_X);
                new FXVerticalSeparator(_hframe4,LAYOUT_FILL_Y);
                FXHorizontalFrame *_hframe5=new FXHorizontalFrame(_hframe4,FRAME_SUNKEN|FRAME_THICK|LAYOUT_LEFT,0,0,0,0,0,0,0,0);
                    tbTiters=new cColorTable(_hframe5,this,ID_ASYTITERS,TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT|LAYOUT_FIX_WIDTH,0,0,115);
                    tbTiters->setTableSize(ASSAY_TTRCOUNT,1);
                    tbTiters->setVisibleRows(4);
                    for(int i=0;i<ASSAY_TTRCOUNT;i++)
                    {
                        tbTiters->setRowText(i,FXStringFormat("%d",i));
                        tbTiters->getRowHeader()->setItemJustify(i,JUSTIFY_CENTER_X);
                        tbTiters->setItemText(i,0,"0");
                        tbTiters->setItemJustify(i,0,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                    }
                    tbTiters->setColumnText(0,oLanguage->getText("str_asymdi_titer"));
                    tbTiters->getColumnHeader()->setItemJustify(0,JUSTIFY_CENTER_X);
                    tbTiters->setRowHeaderWidth(30);
                    tbTiters->setColumnWidth(0,70);
                    tbTiters->showVertGrid(false);
                    tbTiters->setSelectable(false);
                    tbTiters->setScrollStyle(HSCROLLING_OFF|HSCROLLER_NEVER|VSCROLLING_ON|VSCROLLER_ALWAYS|SCROLLERS_TRACK);

        FXGroupBox *_gbox3=new FXGroupBox(_hframe2,oLanguage->getText("str_asymdi_bins"),GROUPBOX_TITLE_LEFT|FRAME_GROOVE);
        _gbox3->setTextColor(FXRGB(150,150,150));
            FXHorizontalFrame *_hframe6=new FXHorizontalFrame(_gbox3,FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
                tbBins=new cColorTable(_hframe6,this,ID_ASYBINS,TABLE_NO_COLSELECT|TABLE_NO_ROWSELECT|LAYOUT_FIX_WIDTH,0,0,115);
                tbBins->setTableSize(ASSAY_BINCOUNT,1);
                tbBins->setVisibleRows(4);
                for(int i=0;i<ASSAY_BINCOUNT;i++)
                {
                    tbBins->setRowText(i,FXStringFormat("%d",i));
                    tbBins->getRowHeader()->setItemJustify(i,JUSTIFY_CENTER_X);
                    tbBins->setItemText(i,0,"0");
                    tbBins->setItemJustify(i,0,cColorTableItem::CENTER_X|cColorTableItem::CENTER_Y);
                }
                tbBins->setColumnText(0,oLanguage->getText("str_asymdi_bin"));
                tbBins->getColumnHeader()->setItemJustify(0,JUSTIFY_CENTER_X);
                tbBins->setRowHeaderWidth(ASSAY_BINCOUNT);
                tbBins->setColumnWidth(0,70);
                tbBins->showVertGrid(false);
                tbBins->setScrollStyle(HSCROLLING_OFF|HSCROLLER_NEVER|VSCROLLING_ON|VSCROLLER_ALWAYS|SCROLLERS_TRACK);
                tbBins->setSelectable(false);

    FXHorizontalFrame *_hframe7=new FXHorizontalFrame(_vframe0,LAYOUT_FILL,0,0,0,0,0,0,5,0);
        FXVerticalFrame *_vframe9=new FXVerticalFrame(_hframe7,LAYOUT_LEFT,0,0,0,0,0,0,0,0,0,0);
            FXHorizontalFrame *_hframe201=new FXHorizontalFrame(_vframe9,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                cbR1=new FXCheckButton(_hframe201,oLanguage->getText("str_asymdi_rule1"),this,ID_RULES,CHECKBUTTON_NORMAL);
                valueR1=new FXTextField(_hframe201,6,this,ID_RULES,LAYOUT_RIGHT|TEXTFIELD_NORMAL|TEXTFIELD_LIMITED|TEXTFIELD_REAL);
                valueR1->setText("0");
                valueR1->setJustify(JUSTIFY_CENTER_X);
                opListR1=new FXListBox(_hframe201,this,ID_RULES,LAYOUT_RIGHT|FRAME_SUNKEN|FRAME_THICK);
                opListR1->setNumVisible(4);
                opListR1->appendItem(">=");
                opListR1->appendItem(">");
                opListR1->appendItem("<=");
                opListR1->appendItem("<");
            FXHorizontalFrame *_hframe202=new FXHorizontalFrame(_vframe9,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                cbR2=new FXCheckButton(_hframe202,oLanguage->getText("str_asymdi_rule2"),this,ID_RULES,CHECKBUTTON_NORMAL);
                valueR2=new FXTextField(_hframe202,6,this,ID_RULES,LAYOUT_RIGHT|TEXTFIELD_NORMAL|TEXTFIELD_LIMITED|TEXTFIELD_REAL);
                valueR2->setText("0");
                valueR2->setJustify(JUSTIFY_CENTER_X);
                opListR2=new FXListBox(_hframe202,this,ID_RULES,LAYOUT_RIGHT|FRAME_SUNKEN|FRAME_THICK);
                opListR2->setNumVisible(4);
                opListR2->appendItem(">=");
                opListR2->appendItem(">");
                opListR2->appendItem("<=");
                opListR2->appendItem("<");
            FXHorizontalFrame *_hframe203=new FXHorizontalFrame(_vframe9,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                cbR3=new FXCheckButton(_hframe203,oLanguage->getText("str_asymdi_rule3"),this,ID_RULES,CHECKBUTTON_NORMAL);
                valueR3=new FXTextField(_hframe203,6,this,ID_RULES,LAYOUT_RIGHT|TEXTFIELD_NORMAL|TEXTFIELD_LIMITED|TEXTFIELD_REAL);
                valueR3->setText("0");
                valueR3->setJustify(JUSTIFY_CENTER_X);
                opListR3=new FXListBox(_hframe203,this,ID_RULES,LAYOUT_RIGHT|FRAME_SUNKEN|FRAME_THICK);
                opListR3->setNumVisible(4);
                opListR3->appendItem(">=");
                opListR3->appendItem(">");
                opListR3->appendItem("<=");
                opListR3->appendItem("<");
            FXHorizontalFrame *_hframe204=new FXHorizontalFrame(_vframe9,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                cbR4=new FXCheckButton(_hframe204,oLanguage->getText("str_asymdi_rule4"),this,ID_RULES,CHECKBUTTON_NORMAL);
                valueR4=new FXTextField(_hframe204,6,this,ID_RULES,LAYOUT_RIGHT|TEXTFIELD_NORMAL|TEXTFIELD_LIMITED|TEXTFIELD_REAL);
                valueR4->setText("0");
                valueR4->setJustify(JUSTIFY_CENTER_X);
                opListR4=new FXListBox(_hframe204,this,ID_RULES,LAYOUT_RIGHT|FRAME_SUNKEN|FRAME_THICK);
                opListR4->setNumVisible(4);
                opListR4->appendItem(">=");
                opListR4->appendItem(">");
                opListR4->appendItem("<=");
                opListR4->appendItem("<");
            FXHorizontalFrame *_hframe205=new FXHorizontalFrame(_vframe9,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                cbR5=new FXCheckButton(_hframe205,oLanguage->getText("str_asymdi_rule5"),this,ID_RULES,CHECKBUTTON_NORMAL);
                valueR5=new FXTextField(_hframe205,6,this,ID_RULES,LAYOUT_RIGHT|TEXTFIELD_NORMAL|TEXTFIELD_LIMITED|TEXTFIELD_REAL);
                valueR5->setText("0");
                valueR5->setJustify(JUSTIFY_CENTER_X);
                opListR5=new FXListBox(_hframe205,this,ID_RULES,LAYOUT_RIGHT|FRAME_SUNKEN|FRAME_THICK);
                opListR5->setNumVisible(4);
                opListR5->appendItem(">=");
                opListR5->appendItem(">");
                opListR5->appendItem("<=");
                opListR5->appendItem("<");
        FXVerticalFrame *_vframe10=new FXVerticalFrame(_hframe7,LAYOUT_RIGHT|LAYOUT_FILL,0,0,0,0,0,0,0,0);
            new FXLabel(_vframe10,oLanguage->getText("str_asymdi_comments"));
            FXVerticalFrame *_vframe8=new FXVerticalFrame(_vframe10,LAYOUT_RIGHT|LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
            comments=new FXText(_vframe8,this,ID_ASYCOMMENTS,TEXT_WORDWRAP|TEXT_NO_TABS|LAYOUT_FILL);

    FXHorizontalFrame *_hframe8=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,10,0);
        //new FXButton(_hframe8,oLanguage->getText("str_asymdi_print"),new FXGIFIcon(getApp(),data_settings_printer),this,CMD_PRINT,FRAME_RAISED|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH,0,0,100,0);
        new FXButton(_hframe8,oLanguage->getText("str_but_close"),NULL,this,CMD_CLOSE,FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_FIX_WIDTH|LAYOUT_RIGHT,0,0,100,0);
        new FXButton(_hframe8,oLanguage->getText("str_asymdi_save"),NULL,this,CMD_SAVE,FRAME_RAISED|BUTTON_DEFAULT|BUTTON_INITIAL|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_RIGHT|LAYOUT_FIX_WIDTH,0,0,100,0);

    free(res);
}

cMDIAssay::~cMDIAssay()
{
}

void cMDIAssay::create()
{
    cMDIChild::create();
    show();
    updateData();
}

FXbool cMDIAssay::canClose(void)
{
    tbFactors->acceptInput(true);
    tbTiters->acceptInput(true);
    tbBins->acceptInput(true);
    if(!saved)
    {
        setFocus();
        int res=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),oLanguage->getText("str_qsave").text());
        switch(res)
        {
            case MBOX_CLICKED_YES:
                if(!saveData())
                    return false;
                break;
            case MBOX_CLICKED_CANCEL:
                return false;
            default:
                break;
        }
    }
    return true;
}

FXbool cMDIAssay::loadAssay(const FXString &prId)
{
    if(!cAssayManager::assayExists(prId))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_openasy").text());
        return false;
    }
    if(!oWinMain->isWindow(prId))
        return oWinMain->raiseWindow(prId);

    cDataResult *res=cAssayManager::getAssay(prId);
    if(res==NULL)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_openasy").text());
        return false;
    }

    name=res->getCellString(0,0);
    tfId->setText(name);
    tfTitle->setText(res->getCellString(0,1));
    tfFilterA->setText(res->getCellString(0,3));
    tfFilterB->setText(res->getCellString(0,4));
    comments->setText(res->getCellString(0,9));

    int i=0,p;
    FXString factors_defs=res->getCellString(0,5);
    while(!factors_defs.empty())
    {
        tbFactors->setItemText(i++,0,factors_defs.before('\t'));
        factors_defs=factors_defs.after('\t');
    }
    FXString calculations_defs=res->getCellString(0,6);
    i=FXIntVal(calculations_defs.before('\n'));
    cbCalculusB->setCheck(i==2?true:false);
    calculations_defs=calculations_defs.after('\n');
    p=lxCalculusA->findItem(calculations_defs.before('\t')+"  =  ",-1,SEARCH_FORWARD|SEARCH_WRAP|SEARCH_PREFIX);
    if(p!=-1)
        lxCalculusA->setCurrentItem(p);
    calculations_defs=calculations_defs.after('\t');
    tfCutoffA->setText(calculations_defs.before('\t'));
    calculations_defs=calculations_defs.after('\t');
    p=lxOperatorA->findItem(calculations_defs.before('\t'));
    if(p!=-1)
        lxOperatorA->setCurrentItem(p);
    calculations_defs=calculations_defs.after('\t');
    tfSuspectA->setText(calculations_defs.before('\n'));
    calculations_defs=calculations_defs.after('\n');
    if(i==2 && !calculations_defs.empty())
    {
        p=lxCalculusB->findItem(calculations_defs.before('\t')+"  =  ",-1,SEARCH_FORWARD|SEARCH_WRAP|SEARCH_PREFIX);
        if(p!=-1)
            lxCalculusB->setCurrentItem(p);
        calculations_defs=calculations_defs.after('\t');
        tfCutoffB->setText(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        p=lxOperatorB->findItem(calculations_defs.before('\t'));
        if(p!=-1)
            lxOperatorB->setCurrentItem(p);
        calculations_defs=calculations_defs.after('\t');
        tfSuspectB->setText(calculations_defs.before('\n'));
    }

    FXString titers_defs=res->getCellString(0,7);
    cbTiters->setCheck(FXIntVal(titers_defs.before('\t'))==1?true:false);
    titers_defs=titers_defs.after('\t');
    tfTiterSlope->setText(titers_defs.before('\t'));
    titers_defs=titers_defs.after('\t');
    tfTiterIntercept->setText(titers_defs.before('\t'));
    titers_defs=titers_defs.after('\t');
    i=lxCalculusT->findItem(titers_defs.before('\n'),-1,SEARCH_FORWARD|SEARCH_WRAP);
    titers_defs=titers_defs.after('\n');
    if(i!=-1)
        lxCalculusT->setCurrentItem(i);
    i=0;
    while(!titers_defs.empty() && i<tbTiters->getNumRows())
    {
        tbTiters->setItemText(i++,0,titers_defs.before('\t'));
        titers_defs=titers_defs.after('\t');
    }

    FXString bins_defs=res->getCellString(0,8);
    i=0;
    while(!bins_defs.empty() && i<tbBins->getNumRows())
    {
        tbBins->setItemText(i++,0,bins_defs.before('\t'));
        bins_defs=bins_defs.after('\t');
    }

    FXString rule,op,val,rules_defs=res->getCellString(0,10);
    while(!rules_defs.empty())
    {
        rule=rules_defs.before('\t');
        rules_defs=rules_defs.after('\t');
        op=rules_defs.before('\t');
        rules_defs=rules_defs.after('\t');
        val=rules_defs.before('\n');
        rules_defs=rules_defs.after('\n');
        if(rule=="Na")
        {
            cbR1->setCheck(true);
            p=opListR1->findItem(op);
            if(p!=-1)opListR1->setCurrentItem(p);
            valueR1->setText(val);
        }
        else if(rule=="Pa")
        {
            cbR2->setCheck(true);
            p=opListR2->findItem(op);
            if(p!=-1)opListR2->setCurrentItem(p);
            valueR2->setText(val);
        }
        else if(rule=="Pa-Na")
        {
            cbR3->setCheck(true);
            p=opListR3->findItem(op);
            if(p!=-1)opListR3->setCurrentItem(p);
            valueR3->setText(val);
        }
        else if(rule=="Nha")
        {
            cbR4->setCheck(true);
            p=opListR4->findItem(op);
            if(p!=-1)opListR4->setCurrentItem(p);
            valueR4->setText(val);
        }
        else if(rule=="Pha")
        {
            cbR5->setCheck(true);
            p=opListR5->findItem(op);
            if(p!=-1)opListR5->setCurrentItem(p);
            valueR5->setText(val);
        }
    }

    setTitle(name);
    updateData();
    saved=true;
    return true;
}

long cMDIAssay::onUpdCalBCheck(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(cbCalculusB->getCheck())
    {
        lxCalculusB->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        lxOperatorB->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        tfCutoffB->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        tfSuspectB->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    else
    {
        lxCalculusB->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        lxOperatorB->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        tfCutoffB->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        tfSuspectB->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }
    return 1;
}

long cMDIAssay::onUpdTitersCheck(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(cbTiters->getCheck())
    {
        tbTiters->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        for(int i=0;i<tbTiters->getNumRows();i++)
            tbTiters->setCellColor(i,0,FXRGB(255,255,255));
        tbTiters->setTextColor(0);
        tbTiters->setBackColor(FXRGB(255,255,255));
        tbTiters->setScrollStyle(HSCROLLING_OFF|HSCROLLER_NEVER|VSCROLLING_ON|VSCROLLER_ALWAYS|SCROLLERS_TRACK);
        tbTiters->getRowHeader()->setTextColor(0);
        tbTiters->getColumnHeader()->setTextColor(0);
        tfTiterSlope->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        tfTiterIntercept->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        lxCalculusT->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    else
    {
        tbTiters->acceptInput();
        for(int i=0;i<tbTiters->getNumRows();i++)
            tbTiters->setCellColor(i,0,FXRGB(223,220,220));
        tbTiters->setTextColor(FXRGB(223,220,220));
        tbTiters->setBackColor(FXRGB(223,220,220));
        tbTiters->setScrollStyle(HSCROLLING_OFF|HSCROLLER_NEVER|VSCROLLING_OFF|VSCROLLER_NEVER);
        tbTiters->getRowHeader()->setTextColor(FXRGB(223,220,220));
        tbTiters->getColumnHeader()->setTextColor(FXRGB(223,220,220));
        tbTiters->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        tfTiterSlope->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        tfTiterIntercept->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        lxCalculusT->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }
    return 1;
}

long cMDIAssay::onCmdTfId(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    if(!tfId->getText().empty())
    {
        if(!cAssayManager::assayExists(tfId->getText()))
            return 1;
        else
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
    }
    tfId->setText(getTitle());
    return 1;
}

long cMDIAssay::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(canClose())
    {
        close();
        return 1;
    }
    return 0;
}

long cMDIAssay::onSelTbFactors(FXObject *prSender,FXSelector prSelector,void *prData)
{
    return 1;
}

long cMDIAssay::onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    return 1;
}

long cMDIAssay::onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData)
{
    saveData();
    if(saved)
        onCmdClose(NULL,0,NULL);
    return 1;
}

long cMDIAssay::onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData)
{


    // TODO


    return 1;
}

long cMDIAssay::onCmdRules(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(valueR1->getText().empty())
        valueR1->setText("0");
    if(valueR2->getText().empty())
        valueR2->setText("0");
    if(valueR3->getText().empty())
        valueR3->setText("0");
    if(valueR4->getText().empty())
        valueR4->setText("0");
    if(valueR5->getText().empty())
        valueR5->setText("0");

    updateData();
    return 1;
}

long cMDIAssay::onUpdRules(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(cbR1->getCheck())
    {
        opListR1->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        valueR1->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    else
    {
        opListR1->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        valueR1->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }

    if(cbR2->getCheck())
    {
        opListR2->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        valueR2->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    else
    {
        opListR2->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        valueR2->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }

    if(cbR3->getCheck())
    {
        opListR3->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        valueR3->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    else
    {
        opListR3->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        valueR3->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }

    if(cbR4->getCheck())
    {
        opListR4->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        valueR4->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    else
    {
        opListR4->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        valueR4->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }

    if(cbR5->getCheck())
    {
        opListR5->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        valueR5->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
    }
    else
    {
        opListR5->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        valueR5->handle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
    }
    return 1;
}


