#ifndef CMDIVACCINE_H
#define CMDIVACCINE_H

#include <fx.h>

#include "cMDIChild.h"
#include "cColorTable.h"

#define VACCINE_ASYCOUNT 30
#define VACCINE_BLDSCOUNT 30

class cMDIVaccine : public cMDIChild
{
    FXDECLARE(cMDIVaccine);
    
    private:
        typedef struct sVAO
        {
            FXString *oid;
            FXHorizontalFrame *frame;
            cColorTable *schedule;
            cColorTable *bleeds;
        };

        FXTextField *tfId;
        FXTextField *tfTitle;
        FXText *comments;
        FXGroupBox *_gb0;

        FXListBox *assayList;
        FXSwitcher *assaySwitcher;

        int assayCount;
        FXbool saved;
        FXString name;
        
        FXint vaccineWindow;
    
    protected:
        cMDIVaccine();
        
        void updateData(void);
        FXbool saveData(void);
        
        FXString selectAssay(FXString &prOid);
        
    public:
        cMDIVaccine(FXMDIClient *prP,const FXString &prName,FXIcon *prIc=NULL,FXPopup *prPup=NULL,FXuint prOpts=0,FXint prX=0,FXint prY=0,FXint prW=0,FXint prH=0);
        virtual ~cMDIVaccine();
        
        virtual void create();
        
        virtual FXbool canClose(void);
        virtual FXbool loadVaccine(const FXString &prId);
        
        enum
        {
            ID_MDIVACCINE=FXMDIChild::ID_LAST,
            ID_VACASYLIST,
            ID_VACID,
            ID_VACTITLE,
            ID_VACSCHEDULE,
            ID_VACBLEEDS,
            ID_VACCOMMENTS,
            
            CMD_SAVE,
            CMD_PRINT,
            CMD_CLOSE,
            CMD_BUT_ASYADD,
            CMD_BUT_ASYDEL,
            
            ID_LAST
        };

        long onRefreshAsy(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdTfId(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData);
        long onRplSchedule(FXObject *prSender,FXSelector prSelector,void *prData);
        long onRplBleeds(FXObject *prSender,FXSelector prSelector,void *prData);
        long onUpdAsyList(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdAsyChange(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdAsyAdd(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdAsyDel(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif

