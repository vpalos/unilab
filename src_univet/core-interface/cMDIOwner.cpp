#include <time.h>
#include <ctype.h>
#include <stdlib.h>

#include "graphics.h"
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cColorsManager.h"
#include "cFormulaRatio.h"
#include "cSpeciesManager.h"
#include "cOwnerManager.h"
#include "cCaseManager.h"
#include "cPopulationManager.h"
#include "cWinMain.h"
#include "cMDIOwner.h"
#include "cMDIOwnerManager.h"

FXDEFMAP(cMDIOwner) mapMDIOwner[]=
{
    FXMAPFUNC(SEL_CLOSE,cMDIOwner::ID_MDIOWNER,cMDIOwner::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIOwner::ID_OWNID,cMDIOwner::onCmdTfId),
    FXMAPFUNC(SEL_CHANGED,cMDIOwner::ID_OWNID,cMDIOwner::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIOwner::ID_OWNLAB,cMDIOwner::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIOwner::ID_OWNADD,cMDIOwner::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIOwner::ID_OWNCITY,cMDIOwner::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIOwner::ID_OWNSTATE,cMDIOwner::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIOwner::ID_OWNZIP,cMDIOwner::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIOwner::ID_OWNPHONE,cMDIOwner::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIOwner::ID_OWNFAX,cMDIOwner::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIOwner::ID_OWNEMAIL,cMDIOwner::onCmdUpdateData),
    FXMAPFUNC(SEL_CHANGED,cMDIOwner::ID_OWNCOMMENTS,cMDIOwner::onCmdUpdateData),
    FXMAPFUNC(SEL_COMMAND,cMDIOwner::CMD_CLOSE,cMDIOwner::onCmdClose),
    FXMAPFUNC(SEL_COMMAND,cMDIOwner::CMD_SAVE,cMDIOwner::onCmdSave),
    FXMAPFUNC(SEL_COMMAND,cMDIOwner::CMD_PRINT,cMDIOwner::onCmdPrint),
};

FXIMPLEMENT(cMDIOwner,cMDIChild,mapMDIOwner,ARRAYNUMBER(mapMDIOwner))

cMDIOwner::cMDIOwner()
{
}

void cMDIOwner::updateData(void)
{
    saved=false;
}

FXbool cMDIOwner::saveData(void)
{
    if(saved)
        return true;

    if(!name.empty() && cOwnerManager::ownerSolid(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_eraseownsolid").text());
        saved=true;
        return 1;
    }
    
    FXString oldname=name;
    name=tfId->getText();
    if(name!=oldname && cOwnerManager::ownerExists(name))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
        tfId->setText(cOwnerManager::getNewID());
        setTitle(tfId->getText());
        updateData();
        return false;
    }
    this->setTitle(name);
    
    if(oldname.empty())
        cOwnerManager::addOwner(name,tfLab->getText(),tfAdd->getText(),tfCity->getText(),tfState->getText(),tfZip->getText(),tfPhone->getText(),tfFax->getText(),tfEmail->getText(),comments->getText());
    else
        cOwnerManager::setOwner(oldname,name,tfLab->getText(),tfAdd->getText(),tfCity->getText(),tfState->getText(),tfZip->getText(),tfPhone->getText(),tfFax->getText(),tfEmail->getText(),comments->getText());
    
    saved=true;
    cMDIOwnerManager::cmdRefresh();
    return true;
}

cMDIOwner::cMDIOwner(FXMDIClient *prP, const FXString &prName, FXIcon *prIc, FXPopup *prPup, FXuint prOpts, FXint prX, FXint prY, FXint prW, FXint prH) 
    :cMDIChild(prP,prName,prIc,prPup,prOpts,prX,prY,prW,prH)
{
    setSelector(ID_MDIOWNER);
    saved=false;
    name="";
    
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL,0,0,0,0);
        new FXLabel(_vframe0,oLanguage->getText("str_ownmdi_id"));
        tfId=new FXTextField(_vframe0,35,this,ID_OWNID);
            tfId->setText(prName);
            tfId->setSelection(0,tfId->getText().length());
            tfId->setFocus();

        new FXLabel(_vframe0,oLanguage->getText("str_ownmdi_ctp"));
        tfLab=new FXTextField(_vframe0,35,this,ID_OWNLAB);
        new FXLabel(_vframe0,oLanguage->getText("str_ownmdi_add"));
        FXVerticalFrame *_vframe7=new FXVerticalFrame(_vframe0,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
        tfAdd=new FXText(_vframe7,this,ID_OWNADD,TEXT_WORDWRAP|TEXT_NO_TABS|LAYOUT_FILL);
        new FXLabel(_vframe0,oLanguage->getText("str_ownmdi_city"));
        tfCity=new FXTextField(_vframe0,35,this,ID_OWNCITY);
        new FXLabel(_vframe0,oLanguage->getText("str_ownmdi_state"));
        tfState=new FXTextField(_vframe0,35,this,ID_OWNSTATE);
        new FXLabel(_vframe0,oLanguage->getText("str_ownmdi_zip"));
        tfZip=new FXTextField(_vframe0,35,this,ID_OWNZIP);
        new FXLabel(_vframe0,oLanguage->getText("str_ownmdi_phone"));
        tfPhone=new FXTextField(_vframe0,35,this,ID_OWNPHONE);
        new FXLabel(_vframe0,oLanguage->getText("str_ownmdi_fax"));
        tfFax=new FXTextField(_vframe0,35,this,ID_OWNFAX);
        new FXLabel(_vframe0,oLanguage->getText("str_ownmdi_email"));
        tfEmail=new FXTextField(_vframe0,35,this,ID_OWNEMAIL);
            
        new FXLabel(_vframe0,oLanguage->getText("str_ownmdi_comments"));
        FXVerticalFrame *_vframe8=new FXVerticalFrame(_vframe0,LAYOUT_FILL|FRAME_SUNKEN|FRAME_THICK,0,0,0,0,0,0,0,0);
        comments=new FXText(_vframe8,this,ID_OWNCOMMENTS,TEXT_WORDWRAP|TEXT_NO_TABS|LAYOUT_FILL);
        FXHorizontalFrame *_hframe8=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,10,0);
            //new FXButton(_hframe8,oLanguage->getText("str_ownmdi_print"),new FXGIFIcon(getApp(),data_settings_printer),this,CMD_PRINT,FRAME_RAISED|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_FIX_WIDTH,0,0,100,0);
            new FXButton(_hframe8,oLanguage->getText("str_but_close"),NULL,this,CMD_CLOSE,FRAME_RAISED|LAYOUT_FILL_X|LAYOUT_FIX_WIDTH|LAYOUT_RIGHT,0,0,100,0);
            new FXButton(_hframe8,oLanguage->getText("str_ownmdi_save"),NULL,this,CMD_SAVE,FRAME_RAISED|BUTTON_DEFAULT|BUTTON_INITIAL|LAYOUT_FILL_X|JUSTIFY_NORMAL|ICON_BEFORE_TEXT|LAYOUT_RIGHT|LAYOUT_FIX_WIDTH,0,0,100,0);
}

cMDIOwner::~cMDIOwner()
{
}

void cMDIOwner::create()
{
    cMDIChild::create();
    show();
    updateData();
}

FXbool cMDIOwner::canClose(void)
{
    if(!saved)
    {
        setFocus();
        int res=FXMessageBox::question(oWinMain,MBOX_YES_NO_CANCEL,oLanguage->getText("str_question").text(),oLanguage->getText("str_qsave").text());
        switch(res)
        {
            case MBOX_CLICKED_YES:
                if(!saveData())
                    return false;
                break;
            case MBOX_CLICKED_CANCEL:
                return false;
            default:
                break;
        }
    }
    return true;
}

FXbool cMDIOwner::loadOwner(const FXString &prId)
{
    if(!cOwnerManager::ownerExists(prId))
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_openown").text());
        return false;
    }
    if(!oWinMain->isWindow(prId))
        return oWinMain->raiseWindow(prId);
    
    cDataResult *res=cOwnerManager::getOwner(prId);
    if(res==NULL)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_openown").text());
        return false;
    }
    
    name=res->getCellString(0,0);
    tfId->setText(name);
    tfLab->setText(res->getCellString(0,1));
    tfAdd->setText(res->getCellString(0,3));
    tfCity->setText(res->getCellString(0,4));
    tfState->setText(res->getCellString(0,5));
    tfZip->setText(res->getCellString(0,6));
    tfPhone->setText(res->getCellString(0,7));
    tfFax->setText(res->getCellString(0,8));
    tfEmail->setText(res->getCellString(0,9));
    comments->setText(res->getCellString(0,10));
    
    setTitle(name);
    updateData();
    saved=true;
    return true;
}

long cMDIOwner::onCmdTfId(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    if(!tfId->getText().empty())
    {
        if(!cOwnerManager::ownerExists(tfId->getText()))
            return 1;
        else
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_invalid_duplid").text());
    }
    tfId->setText(getTitle());
    return 1;
}

long cMDIOwner::onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(canClose())
    {
        close();
        return 1; 
    }
    return 0;
}

long cMDIOwner::onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData)
{
    updateData();
    return 1;
}

long cMDIOwner::onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData)
{
    saveData();
    if(saved)
        onCmdClose(NULL,0,NULL);
    return 1;
}

long cMDIOwner::onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData)
{
    
    
    // TODO
    
    
    return 1;
}

