#include "engine.h"
#include "graphics.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cDLGReportVD.h"

FXDEFMAP(cDLGReportVD) mapDLGReportVD[]=
{
    FXMAPFUNC(SEL_COMMAND,cDLGReportVD::ID_FIELD,cDLGReportVD::onUpdField),
    FXMAPFUNC(SEL_COMMAND,cDLGReportVD::ID_UPDATE,cDLGReportVD::onCmdUpdate),
    FXMAPFUNC(SEL_UPDATE,cDLGReportVD::CMD_BUT_APPLY,cDLGReportVD::onUpdApply),
    FXMAPFUNC(SEL_COMMAND,cDLGReportVD::CMD_BUT_APPLY,cDLGReportVD::onCmdApply),
};

FXIMPLEMENT(cDLGReportVD,FXDialogBox,mapDLGReportVD,ARRAYNUMBER(mapDLGReportVD))

cDLGReportVD::cDLGReportVD(FXWindow *prOwner) :
    FXDialogBox(prOwner,oLanguage->getText("str_report_vd")+"...",DECOR_TITLE|DECOR_BORDER),agtype(agchoice),stype(schoice)
{
    ftype=new FXDataTarget(fchoice,this,ID_UPDATE);
    FXVerticalFrame *_vframe0=new FXVerticalFrame(this,LAYOUT_FILL);

        fchoice=1;
        FXLabel *_lb0=new FXLabel(_vframe0,oLanguage->getText("str_report_ftype"));
            _lb0->setTextColor(FXRGB(0,0,200));        
        FXGroupBox *_gb0=new FXGroupBox(_vframe0,(char*)NULL,GROUPBOX_NORMAL,0,0,0,0,17,0,0,0);
            bLog2=new FXRadioButton(_gb0,oLanguage->getText("str_report_flog2"),ftype,FXDataTarget::ID_OPTION+1);
            bLog2->setTextColor(FXRGB(200,0,0));
            bDeventer=new FXRadioButton(_gb0,oLanguage->getText("str_report_fdevt"),ftype,FXDataTarget::ID_OPTION+2);
            bDeventer->setTextColor(FXRGB(200,0,0));
            
        new FXHorizontalSeparator(_vframe0);
        
        agchoice=2;
        FXLabel *_lb2=new FXLabel(_vframe0,oLanguage->getText("str_report_mtype"));
            _lb2->setTextColor(FXRGB(0,0,200));        
        FXGroupBox *_gb2=new FXGroupBox(_vframe0,(char*)NULL,GROUPBOX_NORMAL,0,0,0,0,17,0,0,0);
            bAMean=new FXRadioButton(_gb2,oLanguage->getText("str_report_ma"),&agtype,FXDataTarget::ID_OPTION+1);
            bAMean->setTextColor(FXRGB(120,120,120));
            bGMean=new FXRadioButton(_gb2,oLanguage->getText("str_report_mg"),&agtype,FXDataTarget::ID_OPTION+2);
            bGMean->setTextColor(FXRGB(120,120,120));
        
        new FXHorizontalSeparator(_vframe0);

        schoice=1;
        FXLabel *_lb1=new FXLabel(_vframe0,oLanguage->getText("str_report_stype"));
            _lb1->setTextColor(FXRGB(0,0,200));
        FXGroupBox *_gb1=new FXGroupBox(_vframe0,(char*)NULL,GROUPBOX_NORMAL,0,0,0,0,17,0,0,0);
            bAssay=new FXRadioButton(_gb1,oLanguage->getText("str_report_sa"),&stype,FXDataTarget::ID_OPTION+1);
            bAssay->setTextColor(FXRGB(120,120,120));
            bCase=new FXRadioButton(_gb1,oLanguage->getText("str_report_sc"),&stype,FXDataTarget::ID_OPTION+2);
            bCase->setTextColor(FXRGB(120,120,120));
        
        new FXHorizontalSeparator(_vframe0);

        FXLabel *_lb3=new FXLabel(_vframe0,oLanguage->getText("str_report_params"));
            _lb3->setTextColor(FXRGB(0,0,200));
            FXHorizontalFrame *_hframe0=new FXHorizontalFrame(_vframe0,0,0,0,0,0,0,0,0,0,10);
                FXVerticalFrame *_vframe1=new FXVerticalFrame(_hframe0,LAYOUT_FILL_Y,0,0,0,0,0,0,1,0,0,5);
                    FXLabel *_lb10=new FXLabel(_vframe1,oLanguage->getText("str_report_tt"));
                        _lb10->setTextColor(FXRGB(0,0,0));
                    tfTT=new FXTextField(_vframe1,10,this,ID_FIELD,TEXTFIELD_INTEGER|TEXTFIELD_LIMITED|FRAME_SUNKEN);
                    tfTT->setText("100");
                    tfHL=new FXTextField(_vframe1,10,this,ID_FIELD,LAYOUT_BOTTOM|TEXTFIELD_REAL|TEXTFIELD_LIMITED|FRAME_SUNKEN);
                    tfHL->setText("2.35");
                    FXLabel *_lb11=new FXLabel(_vframe1,oLanguage->getText("str_report_hl"),NULL,LAYOUT_BOTTOM);
                        _lb11->setTextColor(FXRGB(0,0,0));
                FXVerticalFrame *_vframe2=new FXVerticalFrame(_hframe0,LAYOUT_FILL_Y,0,0,0,0,0,0,0,0);
                    FXHorizontalFrame *_hframe1=new FXHorizontalFrame(_vframe2,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                        FXLabel *_lb20=new FXLabel(_hframe1,oLanguage->getText("str_report_p1"),NULL,LAYOUT_CENTER_Y);
                            _lb20->setTextColor(FXRGB(0,0,0));
                        FXHorizontalFrame *_hframe2=new FXHorizontalFrame(_hframe1,LAYOUT_RIGHT|FRAME_SUNKEN,0,0,0,0,0,0,0,0,0,0);
                            tfP1=new FXTextField(_hframe2,3,this,ID_FIELD,TEXTFIELD_INTEGER|TEXTFIELD_LIMITED|JUSTIFY_RIGHT);
                            tfP1->setText("75");
                        lp1=new FXLabel(_hframe2,"%",NULL,LAYOUT_FILL_Y|JUSTIFY_CENTER_Y,0,0,0,0,0,DEFAULT_PAD,0,0);
                            lp1->setBackColor(this->getBackColor());
                    FXHorizontalFrame *_hframe3=new FXHorizontalFrame(_vframe2,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
                        FXLabel *_lb22=new FXLabel(_hframe3,oLanguage->getText("str_report_p2"),NULL,LAYOUT_CENTER_Y);
                            _lb22->setTextColor(FXRGB(0,0,0));
                        FXHorizontalFrame *_hframe4=new FXHorizontalFrame(_hframe3,LAYOUT_RIGHT|FRAME_SUNKEN,0,0,0,0,0,0,0,0,0,0);
                            tfP2=new FXTextField(_hframe4,3,this,ID_FIELD,TEXTFIELD_INTEGER|TEXTFIELD_LIMITED|JUSTIFY_RIGHT);
                            tfP2->setText("25");
                        lp2=new FXLabel(_hframe4,"%",NULL,LAYOUT_FILL_Y|JUSTIFY_CENTER_Y,0,0,0,0,0,DEFAULT_PAD,0,0);
                            lp2->setBackColor(this->getBackColor());

                    spSD=new FXSpinner(_vframe2,10,NULL,0,LAYOUT_FILL_X|SPIN_NORMAL|FRAME_SUNKEN|LAYOUT_BOTTOM);
                    spSD->setRange(0,4);
                    spSD->setValue(0);
                    FXLabel *_lb21=new FXLabel(_vframe2,oLanguage->getText("str_report_sd"),NULL,LAYOUT_BOTTOM);
                        _lb21->setTextColor(FXRGB(0,0,0));
                    
        new FXHorizontalSeparator(_vframe0);
        
        tfP1->disable();
        tfP2->disable();
        spSD->disable();

        FXHorizontalFrame *_hframe100=new FXHorizontalFrame(_vframe0,LAYOUT_CENTER_X);
        butCancel=new FXButton(_hframe100,oLanguage->getText("str_but_cancel"),NULL,this,ID_CANCEL,BUTTON_DEFAULT|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
        butAccept=new FXButton(_hframe100,oLanguage->getText("str_but_ok"),NULL,this,CMD_BUT_APPLY,BUTTON_DEFAULT|BUTTON_INITIAL|FRAME_RAISED|LAYOUT_CENTER_X,0,0,0,0,10,10);
}

cDLGReportVD::~cDLGReportVD()
{
}

long cDLGReportVD::onCmdApply(FXObject *prSender,FXSelector prSelector,void *prData)
{
    handle(NULL,FXSEL(SEL_COMMAND,ID_ACCEPT),NULL);
    return 1;
}

long cDLGReportVD::onUpdApply(FXObject *prSender,FXSelector prSelector,void *prData)
{
    return 1;
}

long cDLGReportVD::onUpdField(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(tfP1->getText().empty() || tfP1->getText()=="-")
        tfP1->setText("0");
    if(tfP2->getText().empty() || tfP2->getText()=="-")
        tfP2->setText("0");
    int v=FXIntVal(tfP1->getText());
    if(v<1)
        tfP1->setText("75");
    if(v>100)
        tfP1->setText("100");
    v=FXIntVal(tfP2->getText());
    if(v<0)
        tfP2->setText("0");
    if(v>100)
        tfP2->setText("100");
    v=FXIntVal(tfTT->getText());
    if(v<1)
        tfTT->setText("1");
    if(v>1000000)
        tfTT->setText("1000000");
    v=FXIntVal(tfHL->getText());
    if(v<0)
        tfHL->setText("0");
    if(v>1000000)
        tfHL->setText("1000000");
    return 1;
}

long cDLGReportVD::onCmdUpdate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    if(fchoice==2)
    {
        tfP1->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        tfP2->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        spSD->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_ENABLE),NULL);
        lp1->setBackColor(FXRGB(255,255,255));
        lp2->setBackColor(FXRGB(255,255,255));
    }
    else
    {
        tfP1->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        tfP2->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        spSD->tryHandle(this,FXSEL(SEL_COMMAND,FXWindow::ID_DISABLE),NULL);
        lp1->setBackColor(this->getBackColor());
        lp2->setBackColor(this->getBackColor());
    }
    return 1;
}


