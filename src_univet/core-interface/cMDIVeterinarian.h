#ifndef CMDIVETERINARIAN_H
#define CMDIVETERINARIAN_H

#include <fx.h>

#include "cMDIChild.h"
#include "cColorTable.h"

class cMDIVeterinarian : public cMDIChild
{
    FXDECLARE(cMDIVeterinarian);
    
    private:
        FXTextField *tfId;
        FXTextField *tfLab;
        FXText *tfAdd;
        FXTextField *tfCity;
        FXTextField *tfState;
        FXTextField *tfZip;
        FXTextField *tfPhone;
        FXTextField *tfFax;
        FXTextField *tfEmail;
        FXText *comments;

        FXbool saved;
        FXString name;
    
    protected:
        cMDIVeterinarian();
        
        void updateData(void);
        FXbool saveData(void);
        
    public:
        cMDIVeterinarian(FXMDIClient *prP,const FXString &prName,FXIcon *prIc=NULL,FXPopup *prPup=NULL,FXuint prOpts=0,FXint prX=0,FXint prY=0,FXint prW=0,FXint prH=0);
        virtual ~cMDIVeterinarian();
        
        virtual void create();
        
        virtual FXbool canClose(void);
        virtual FXbool loadVeterinarian(const FXString &prId);
        
        enum
        {
            ID_MDIVETERINARIAN=FXMDIChild::ID_LAST,
            ID_VETID,
            ID_VETLAB,
            ID_VETADD,
            ID_VETCITY,
            ID_VETSTATE,
            ID_VETZIP,
            ID_VETPHONE,
            ID_VETFAX,
            ID_VETEMAIL,
            ID_VETCOMMENTS,
            
            CMD_SAVE,
            CMD_PRINT,
            CMD_CLOSE,
            
            ID_LAST
        };

        long onCmdUpdateData(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdTfId(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdSave(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdPrint(FXObject *prSender,FXSelector prSelector,void *prData);
        long onCmdClose(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif

