#ifndef CTEXT_H
#define CTEXT_H

#include <fx.h>

class cText : public FXText
{
    FXDECLARE(cText);
    
    private:
    protected:
        cText();
    
    public:
        cText(FXComposite *p, FXObject *tgt=NULL, FXSelector sel=0, FXuint opts=0, FXint x=0, FXint y=0, FXint w=0, FXint h=0, FXint pl=3, FXint pr=3, FXint pt=2, FXint pb=2);
        ~cText();
        
        long onPaint(FXObject *prSender,FXSelector prSelector,void *prData);
};

#endif
