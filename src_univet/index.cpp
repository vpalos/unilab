#include <fx.h>
#include "index.h"
#include "engine.h"
#include "strings.h"

int main(int prArgc,char **prArgv)
{
    if(prArgc>1 && !strcmp(prArgv[1],"--definitelyDeleteRegistry"))
    {
        oApplicationManager=new FXApp(APP_TITLE,APP_VENDOR);
        oApplicationManager->init(prArgc,prArgv);
        oApplicationManager->reg().clear();
        oApplicationManager->exit();
        return 0;
    }
    return initiateCore(prArgc,prArgv);
}
