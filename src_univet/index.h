#ifndef INDEX_H
#define INDEX_H

#define APP_TITLE "UniVet Software"
#define APP_VERSION "1.3.3"
#define APP_VENDOR "Palos & Sons - The Experts (www.palos.ro)"
#define APP_VERSION_NO 1001003003
#define DBS_VERSION_NO 1001000000

#define APP_LEGAL "Pasteur Animal Health\n\
UniVet(tm) SOFTWARE LICENSE AGREEMENT\n\
\n\
This agreement sets forth the terms of the license from Pasteur Animal Health. for UniVet(tm) software applications provided by Pasteur, or Pasteur authorized distributors.  BY USING THE UniVet(tm) SOFTWARE, YOU AGREE TO BE BOUND BY THE TERMS AND CONDITIONS OF THIS AGREEMENT.  IF YOU DO NOT ACCEPT THESE TERMS AND CONDITIONS, DO NOT USE THE UniVet(tm) SOFTWARE.\n\
\n\
1.	DEFINITIONS:  as used in this agreement have the following meanings: \n\
\n\
1.1	\"Software Product(s)\" meaning any series of instructions in machine readable, object code form only; any fixes, upgrades, revisions or enhancement or other modifications to the foregoing of the UniVet(tm) computer program(s).  Also can consist of any related user guide or other printed, written, \"online\" or electronic documentation and materials.\n\
\n\
1.2	\"Use\" means installing any portion of the Software Products from storage units or media for the purpose of processing the information contained in the Software Products.\n\
\n\
2.	LICENSE:  Subject to the terms and conditions set forth in this agreement, Pasteur Animal Health. grants a non-exclusive, non-transferable license, without the right to sublicense, to Use the Software Products for internal purposes only.  Without limiting the foregoing, Pasteur grants no rights for the Use or other utilization of the Software Products, directly or indirectly, for benefit of any person or entity other than yourself, or for the purpose of processing application information, circulation materials or other products.\n\
\n\
3.	OWNERSHIP, INTELLECTUAL PROPERTY RIGHTS AND OTHER RESTRICTIONS:\n\
\n\
3.1	Confidential Information has been and will be provided with the express understanding that neither party hereto is obligated to enter into any further agreement relating to such Confidential Information.  In addition, Pasteur Animal Health. discloses the Software Products to in confidence, and you shall not cause or permit any dissemination of Software Products, to any third party without our prior written consent.  You shall limit use of the Software Products to employees that are directly involved.  You shall take steps to ensure the validity of the authorized personnel using the Software Products.  Report any suspected violation of this Confidentiality.\n\
\n\
3.2	All copyright, trade secret, patent, proprietary and/or other legal notices contained on or in copies of the Software Products shall remain intact integral with the Software Product.  The existence of any such copyright notice on Software Products shall not be construed as an admission, or be deemed to create a presumption, that publication of such materials has occurred.\n\
\n\
3.3	Pasteur Animal Health. retains all right, title and interest in the Software Products and any copies thereof, including all copyright, trade secret, and other technical property rights therein.  All physical copies of the Software Products including CD-ROM, diskette, tape or any other media, which is provided either physically or electronically, such as via the Internet or otherwise shall remain Pasteur Animal Health. property.  For the term of this license agreement, all copies shall be considered a loan from Pasteur Animal Health.\n\
\n\
3.4	Changes and/or supplements the Software Products are not allowed except by Pasteur Animal Health. or authorized distributors.\n\
\n\
4.	UPGRADES:  Upgrades are available according to the terms of the LIMITED WARRENTY.  User of the Software Product has no direct rights to future software upgrades.  All copies of upgrages and former versions of the Software Product remain the property of Pasteur Animal Health.\n\
\n\
5.	LIMITED WARRANTY:  We warrant that the Software Products will perform in accordance within specifications for a period of 45 days from the date of delivery of such Software Products.  Claims of non-performance of the Software Product need to be reported within 45 days of delivery of such Software Product.  Pasteur Animal Health. will provide upgrades to correct any non-performance issues in an unspecified amount of time.  This warranty shall not be applicable in the event that replacement is ineffective due to your hardware or other software.\n\
\n\
6.	DISCLAIMER OF ADDITIONAL WARRANTIES:  EXCEPT AS STATED IN THIS AGREEMENT, WE MAKE NO OTHER WARRANTY, EXPRESS OR IMPLIED, WRITTEN OR ORAL, AND THERE IS NO WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE OR NON-INFRINGEMENT.\n\
\n\
7.	LIMITATION OF LIABILITY:  Pasteur Animal Health. will replace all Software Products in a timely manner if able.  Pasteur Animal Health. will not be liable for replacements that cannot be created in a timely manner due to circumstances beyond our reasonable control.\n\
\n\
8.	Pasteur Animal Health. IS NOT LIABLE TO FOR ANY INCIDENTAL, CONSEQUENTIAL, INDIRECT DAMAGES, OR FOR LOSS OF PROFITS, GOODWILL, DATA OR USE DAMAGES ARISING OUT OF THE USE OR PERFORMANCE OF THE SOFTWARE PRODUCTS OR FAILURE OR DELAY IN DELIVERING THE SOFTWARE PRODUCTS, WHETHER BASED ON WARRANTY, CONTRACT, TORT OR OTHERWISE.   DURING THE WARRANTY PERIOD, Pasteur Animal Health?S LIABILITY FOR THE SOFTWARE PRODUCTS, WHETHER BASED ON WARRANTY, CONTRACT, TORT OR OTHERWISE, SHALL NOT EXCEED THE AMOUNT, IF ANY, YOU PAID FOR THE SOFTWARE PRODUCTS.  Some jurisdictions do not allow the exclusion or limitation of special, incidental, consequential, indirect or exemplary damages or the limitation of liability, so the above limitations may not apply to you.\n\
\n\
8.	TERM: The license granted under this agreement is effective from the date of delivery of the Software Product and shall remain in effect until termination is granted because of failure to comply with any provision of this agreement or agreement is terminated by providing written notice to Pasteur Animal Health.  All of obligations of the user under this agreement survive the termination of the license.\n\
\n\
8.2	Upon termination of this agreement, return to Pasteur Animal Health. or destroy the original and all copies of the Software Products.\n\
\n\
8.3	Unauthorized use or transfer of the Software Products may harm Pasteur Animal Health.  The provisions of Section 3 of this agreement, if broken, shall entitle Pasteur Animal Health. to take injunctive action afforded by law, to prevent a breach contract.\n\
\n\
9.	COMPLIANCE WITH U.S. EXPORT ADMINISTRATION LAWS:  User agrees to not export or re-export Pasteur Animal Health. Software Product or repackage Pasteur Animal Health. Software Product under another identity in order to export to U.S. embargoed countries.\n\
\n\
10.	GOVERNMENT CONTRACT PROVISIONS:  Any Software Product or other software or documentation supplied hereunder that is acquired by or on behalf of the United States Government, is \"commercial computer software\" or \"commercial computer software documentation,\" and absent a written agreement to the contrary, the United States Government's rights with respect to such Software Products, or other software or documentation, are limited by the terms of this document, pursuant to FAR � 12.212(a) and/or DFARS � 227.7202-1(a) or such other rules or regulations applicable now or in the future, as applicable.\n\
\n\
11.	TIME LIMIT TO BRING SUIT:  any suit or action against Pasteur Animal Health. relating to the Software Products shall be initiated no more than one year after the related cause of action has occurred.\n\
\n\
12.	ENTIRE AGREEMENT; NO MODIFICATIONS:  This Entire Agreement supersedes all prior oral or written agreements or statements.  Neither party can modify this agreement.\n\
\n\
13.	NO ASSIGNMENT:  No assignments of this agreement or rights or duties hereunder can be made to any other person.\n\
\n\
14.	ENFORCEABILITY:  If any provisions of this agreement are held by a court of competent jurisdiction to be unenforceable in any respect, the enforceability of the remaining provisions shall not be affected in any way, and both parties agree to replace any unenforceable provision with an enforceable and valid arrangement which, in its economic effect, is as close as possible to the unenforceable provision.\n\
\n\
15.	CHOICE OF LAW; EXCLUSIVE JURISDICTION:  This agreement is governed by the laws of the State of Arkansas, U.S.A. without regard to conflict of laws rules.  You agree that this agreement shall be treated as though executed and performed in Benton County, Arkansas, and that any action relating to this agreement must be brought in the court of appropriate jurisdiction in the State of Arkansas, which shall have exclusive jurisdiction.\n\
\n\
� 2005 Pasteur Animal Health.\n\n\
\n\
WARNING:  This program is protected by copyright law and international treaties.\n\
\n\
Unauthorized reproduction and/or distribution of this program, or any portion of it, may result in severe civil and criminal penalties, and will be prosecuted to the maximum possible extent under law.\n\
\n\
By using this software, you agree to be bound by the terms and conditions of the UniVet(tm) software product license agreement.\n\
\n"

#endif
