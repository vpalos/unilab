#include <strings.h>
#include <math.h>
#include <time.h>
#include "engine.h"
#include "graphics.h"
#include "cWinMain.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cColorsManager.h"
#include "cDataResult.h"
#include "cAssayManager.h"
#include "cTemplateManager.h"
#include "cPopulationManager.h"
#include "cFormulaRatio.h"
#include "cTotalCountsReport.h"

cTotalCountsReport *oTotalCountsReport;

FXbool cTotalCountsReport::loadReadings(cSortList &prCases)
{
    FXDialogBox msg(oWinMain,oLanguage->getText("str_report_wtitle"),DECOR_TITLE|DECOR_BORDER);
    new FXLabel(&msg,oLanguage->getText("str_report_ltext"),NULL,LAYOUT_FILL|JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
    msg.create();
    msg.show(PLACEMENT_OWNER);
    msg.setFocus();
    msg.raise();
    oApplicationManager->beginWaitCursor();
    while(oApplicationManager->peekEvent())
        oApplicationManager->runOneEvent(false);

    FXString criteria="";
    int j=0;
    for(int i=0;i<prCases.getNumItems();i++)
        if(prCases.isItemSelected(i))
        {
            if(j>0)
                criteria=criteria+" OR ";
            criteria=criteria+"(assay_oid='"+prCases.getItemText(i).before('\t')+"' AND "+"id='"+prCases.getItemText(i).after('\t').before('\t')+"')";
            j++;
        }
    if(!j)
        return false;
    cDataResult *resr=oDataLink->execute("SELECT DISTINCT reading_oid FROM t_gncases WHERE "+criteria+" ORDER BY assay_oid ASC;");
    tSetsCount=resr->getRowCount();
    if(!tSetsCount)
        return false;
    cDataResult *resc,*resp;
    casesSets=(sCasesSet*)malloc(tSetsCount*sizeof(sCasesSet));
    if(!casesSets)
    {
        tSetsCount=0;
        tCasesCount=0;
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return false;
    }

    bool notiter=false;
    tCasesCount=0;
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        FXString __reading_oid(resr->getCellString(setsi,0));
        resc=oDataLink->execute("SELECT * FROM t_gncases WHERE ("+criteria+") AND reading_oid='"+__reading_oid+"' ORDER BY (startPlate*100+startCell) ASC;");
        casesSets[setsi].casesCount=resc->getRowCount();
        tCasesCount+=casesSets[setsi].casesCount;
        casesSets[setsi].cases=(sCaseEntry*)malloc(casesSets[setsi].casesCount*sizeof(sCaseEntry));
        if(!casesSets[setsi].cases)
        {
            tSetsCount=0;
            tCasesCount=0;
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
            return false;
        }

        FXString __assay(resc->getCellString(0,6));
        FXString __template(resc->getCellString(0,16));
        casesSets[setsi].assayRes=oDataLink->execute("SELECT * FROM t_assays WHERE id='"+__assay+"';");
        casesSets[setsi].templateRes=oDataLink->execute("SELECT * FROM t_templates WHERE id='"+__template+"';");

        casesSets[setsi].orientation=resc->getCellInt(0,17);
        {
            int k=resc->getCellInt(0,18);
            casesSets[setsi].alelisa=k==2?true:false;
        }
        int __alelisa=casesSets[setsi].alelisa;
        int __alelisa_multiplier=__alelisa?2:1;

        FXString __lot(resc->getCellString(0,7));
        FXString __expDate(resc->getCellString(0,8));

        FXString calculations_defs=casesSets[setsi].assayRes->getCellString(0,6);
        casesSets[setsi].useSecond=FXIntVal(calculations_defs.before('\n'))==2?true:false;
        calculations_defs=calculations_defs.after('\n');

        casesSets[setsi].cala=new FXString(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        casesSets[setsi].calapco=new FXString(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        casesSets[setsi].calaop=new FXString(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        casesSets[setsi].calasco=new FXString(calculations_defs.before('\n'));
        calculations_defs=calculations_defs.after('\n');

        if(casesSets[0].useSecond)
        {
            casesSets[setsi].calb=new FXString(calculations_defs.before('\t'));
            calculations_defs=calculations_defs.after('\t');
            casesSets[setsi].calbpco=new FXString(calculations_defs.before('\t'));
            calculations_defs=calculations_defs.after('\t');
            casesSets[setsi].calbop=new FXString(calculations_defs.before('\t'));
            calculations_defs=calculations_defs.after('\t');
            casesSets[setsi].calbsco=new FXString(calculations_defs.before('\n'));
            calculations_defs=calculations_defs.after('\n');
        }
        else
        {
            casesSets[setsi].calb=NULL;
            casesSets[setsi].calbop=NULL;
            casesSets[setsi].calbpco=new FXString("0");
            casesSets[setsi].calbsco=new FXString("0");
        }

        if(*casesSets[setsi].calasco=="0")
            *casesSets[setsi].calasco=*casesSets[setsi].calapco;
        if(*casesSets[setsi].calbsco=="0")
            *casesSets[setsi].calbsco=*casesSets[setsi].calbpco;

        FXString factors_defs=casesSets[setsi].assayRes->getCellString(0,5);
        for(int i=0;i<4;i++)
        {
            casesSets[setsi].factors[i]=FXIntVal(factors_defs.before('\t'));
            factors_defs=factors_defs.after('\t');
        }

        FXString titers_defs=casesSets[setsi].assayRes->getCellString(0,7);
        casesSets[setsi].useTiters=FXIntVal(titers_defs.before('\t'))!=0?true:false;
        if(!casesSets[setsi].useTiters)
            notiter=true;
        titers_defs=titers_defs.after('\t');
        casesSets[setsi].slope=FXFloatVal(titers_defs.before('\t'));
        titers_defs=titers_defs.after('\t');
        casesSets[setsi].intercept=FXFloatVal(titers_defs.before('\t'));
        titers_defs=titers_defs.after('\t');
        casesSets[setsi].calt=new FXString(titers_defs.before('\n'));
        titers_defs=titers_defs.after('\n');
        for(int i=0;i<30;i++)
        {
            casesSets[setsi].titerGroups[i]=FXIntVal(titers_defs.before('\t'));
            titers_defs=titers_defs.after('\t');
        }

        FXString bins_defs=casesSets[setsi].assayRes->getCellString(0,8);
        for(int i=0;i<30;i++)
        {
            casesSets[setsi].ratioGroups[i]=FXFloatVal(bins_defs.before('\t'));
            bins_defs=bins_defs.after('\t');
        }

        FXString __controls_defs(resc->getCellString(0,22));
        __controls_defs=__controls_defs.after('\n');
        casesSets[setsi].controlsPerPlate=0;
        int value;
        do{
            value=FXIntVal(__controls_defs.before('\t'));
            __controls_defs=__controls_defs.after('\t');
            if(value!=0 && value!=-100)
                casesSets[setsi].controlsLayout[casesSets[setsi].controlsPerPlate++]=value;
            else
                break;
        }while(true);
        cMDIReadings::sortControls(casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate);

        casesSets[setsi].platesCount=0;
        FXDict *pl=new FXDict();
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++)
        {
            casesSets[setsi].cases[casei].startCell=resc->getCellInt(casei,20);
            casesSets[setsi].cases[casei].startPlate=resc->getCellInt(casei,19);
            casesSets[setsi].cases[casei].plateSpanCount=resc->getCellInt(casei,21);

            int ps=casesSets[setsi].cases[casei].startPlate,pe=ps+casesSets[setsi].cases[casei].plateSpanCount-1;
            FXString key;
            for(int i=ps;i<=pe;i++)
                pl->insert(FXStringFormat("%c%c",i/256+1,i%256+1).text(),NULL);

            casesSets[setsi].cases[casei].id=new FXString(resc->getCellString(casei,0));
            casesSets[setsi].cases[casei].reading_oid=new FXString(__reading_oid);
            casesSets[setsi].cases[casei].count=resc->getCellInt(casei,15);
            casesSets[setsi].cases[casei].replicates=resc->getCellInt(casei,11);
            casesSets[setsi].cases[casei].factor=resc->getCellInt(casei,12);
            casesSets[setsi].cases[casei].alelisa=__alelisa;

            casesSets[setsi].sampleCount+=casesSets[0].cases[casei].count*__alelisa_multiplier;

            casesSets[setsi].cases[casei].age=new FXString(resc->getCellString(casei,14));
            casesSets[setsi].cases[casei].tpl=new FXString(__template);
            casesSets[setsi].cases[casei].veterinarian=new FXString(resc->getCellString(casei,5));
            casesSets[setsi].cases[casei].controls_defs=new FXString(resc->getCellString(casei,22));
            casesSets[setsi].cases[casei].readingDate=new FXString(resc->getCellString(casei,2));
            casesSets[setsi].cases[casei].technician=new FXString(resc->getCellString(casei,3));
            casesSets[setsi].cases[casei].reason=new FXString(resc->getCellString(casei,4));
            casesSets[setsi].cases[casei].lot=new FXString(__lot);
            casesSets[setsi].cases[casei].expirationDate=new FXString(__expDate);
            casesSets[setsi].cases[casei].spType=new FXString(resc->getCellString(casei,9));
            casesSets[setsi].cases[casei].bleedDate=new FXString(resc->getCellString(casei,10));
            casesSets[setsi].cases[casei].comments=new FXString(resc->getCellString(casei,24));
            casesSets[setsi].cases[casei].population=new FXString(resc->getCellString(casei,13));
            casesSets[setsi].cases[casei].assay=new FXString(__assay);

            resp=oDataLink->execute("SELECT * FROM t_gnpopulations WHERE id='"+*casesSets[setsi].cases[casei].population+"';");
            casesSets[setsi].cases[casei].breed1=new FXString(resp->getCellString(casei,3));
            casesSets[setsi].cases[casei].breed2=new FXString(resp->getCellString(casei,4));
            resp->free();

            casesSets[setsi].cases[casei].parentSet=setsi;

            FXString __data(resc->getCellString(casei,23));
            FXString __controls=__data.before('\n');
            __data=__data.after('\n');

            casesSets[setsi].cases[casei].controlsCount=casesSets[setsi].cases[casei].plateSpanCount*__alelisa_multiplier*casesSets[setsi].controlsPerPlate;
            casesSets[setsi].cases[casei].controlsData=(double*)malloc(casesSets[setsi].cases[casei].controlsCount*sizeof(double));
            if(!casesSets[setsi].cases[casei].controlsData)
            {
                tSetsCount=0;
                tCasesCount=0;
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                return false;
            }
            for(int i=0;i<casesSets[setsi].cases[casei].controlsCount;i++)
            {
                casesSets[setsi].cases[casei].controlsData[i]=FXFloatVal(__controls.before(' '));
                __controls=__controls.after(' ');
            }

            int ct=casesSets[setsi].cases[casei].count*__alelisa_multiplier*(casesSets[setsi].cases[casei].replicates+1);
            casesSets[setsi].cases[casei].data=(double*)malloc(ct*sizeof(double));
            if(!casesSets[setsi].cases[casei].data)
            {
                tSetsCount=0;
                tCasesCount=0;
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                return false;
            }
            for(int i=0;i<ct;i++)
            {
                casesSets[setsi].cases[casei].data[i]=FXFloatVal(__data.before(' '));
                __data=__data.after(' ');
            }
        }
        casesSets[setsi].platesCount=pl->no();
        delete pl;
        resc->free();
    }
    resr->free();

    bool incompatible=false,start=casesSets[0].alelisa;
    for(int setsi=0;setsi<tSetsCount;setsi++)
        if(start!=casesSets[setsi].alelisa)
        {
            incompatible=true;
            break;
        }

    oApplicationManager->endWaitCursor();
    msg.hide();

    if(incompatible)
    {
        tSetsCount=0;
        tCasesCount=0;
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_reading_incompatcse").text());
        return false;
    }

    return true;
}

FXbool cTotalCountsReport::processReadings(void)
{
    if(!tCasesCount)
        return false;
    char replicates[10];
    bool __alelisa=false;
    int __alelisa_multiplier=1;
    int k,l,offset,m;
    int platesi=0;
    int platesci=0;
    int datai=0;
    int samplei=0;
    int casesgi=0;
    int controlsi=0;
    int casepi=0;
    int p2_datai=1;
    int deltasi=0;
    int replicatesi=0;
    double ndata,aldata=0;
    double *ctData=NULL,n,val;
    FXString invalidPlates="";
    int dp,rp,pd=0,diff=0,spi=-1,cplatei,splatei;
    FXStringDict *csList=new FXStringDict();
    FXDialogBox msg(oWinMain,oLanguage->getText("str_report_wtitle"),DECOR_TITLE|DECOR_BORDER);
    new FXLabel(&msg,oLanguage->getText("str_report_wtext"),NULL,LAYOUT_FILL|JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
    msg.create();
    msg.show(PLACEMENT_OWNER);
    msg.setFocus();
    msg.raise();
    oApplicationManager->beginWaitCursor();
    while(oApplicationManager->peekEvent())
        oApplicationManager->runOneEvent(false);
    FXStringDict *sList=new FXStringDict();
    int gstp=0,gsts=0,gstn=0,gste=0,gstt=0;
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        spi=-1;
        splatei=-1;
        diff=0;
        __alelisa=casesSets[setsi].alelisa && casesSets[setsi].orientation==1?true:false;
        __alelisa_multiplier=__alelisa?2:1;
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++,casesgi++)
        {
            int stp=0,sts=0,stn=0,ste=0,stt=0;

            cVChart *chart=new cVChart(repWin,cVChart::CHART_BAR,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT,0,0,0,250);
            chart->setTitle((*casesSets[setsi].cases[casei].id)+" - "+(*casesSets[setsi].cases[casei].population)+" - "+(*casesSets[setsi].cases[casei].assay)+" - "+(*casesSets[setsi].cases[casei].readingDate));
            chart->create();
            chart->recalc();
            cplatei=0;
            samplei=0;
            int rdi=datai;
            for(int g=spi+1;g<casesSets[setsi].cases[casei].startPlate;g++,diff++,splatei++);
            spi=casesSets[setsi].cases[casei].startPlate;
            while(samplei<casesSets[setsi].cases[casei].count*__alelisa_multiplier)
            {
                deltasi=0;
                replicatesi=0;
                replicates[0]=0;
                while(replicatesi<=casesSets[setsi].cases[casei].replicates)
                {
                    platesi=datai/(96/__alelisa_multiplier);
                    if(platesci==platesi || datai==rdi)
                    {
                        splatei++;
                        if(samplei)cplatei++;
                        ctData=casesSets[setsi].cases[casei].controlsData+cplatei*casesSets[setsi].controlsPerPlate*__alelisa_multiplier;

                        double na=0,pa=0,nha=0,pha=0,pc=0,nc=0;
                        for(int cswitchi=0;cswitchi<2;cswitchi++)
                        {
                            controlsi=0;
                            for(int i=0;i<casesSets[setsi].controlsPerPlate;i++)
                            {
                                if(((!cswitchi) && (casesSets[setsi].controlsLayout[i]>=0)) ||
                                   (cswitchi && (casesSets[setsi].controlsLayout[i]<0)))
                                {
                                    controlsi+=__alelisa?2:1;
                                    continue;
                                }

                                if(casesSets[setsi].controlsLayout[i]>=0)
                                    {pa+=ctData[controlsi];pc++;}
                                if(casesSets[setsi].controlsLayout[i]<0)
                                    {na+=ctData[controlsi];nc++;}

                                m=cMDIReadings::platePosition(abs(casesSets[setsi].controlsLayout[i])==1000?0:abs(casesSets[setsi].controlsLayout[i]),false,casesSets[setsi].orientation);
                                l=m%12;
                                p2_datai++;
                                controlsi++;
                                if(__alelisa)
                                {
                                    if(casesSets[setsi].controlsLayout[i]>=0)
                                        pha+=ctData[controlsi];
                                    if(casesSets[setsi].controlsLayout[i]<0)
                                        nha+=ctData[controlsi];

                                    m=cMDIReadings::platePosition(abs(casesSets[setsi].controlsLayout[i])==1000?0:abs(casesSets[setsi].controlsLayout[i]),false,casesSets[setsi].orientation)+1;
                                    l=m%12;
                                    p2_datai++;
                                    controlsi++;
                                }
                            }
                        }

                        platesci++;
                        FXString rule,op,r2,rules_defs=casesSets[setsi].assayRes->getCellString(0,10);
                        r2=rules_defs;
                        na/=nc;
                        pa/=pc;
                        nha/=nc;
                        pha/=pc;
                    }

                    casepi=datai%(96/__alelisa_multiplier);
                    dp=datai/(96/__alelisa_multiplier);
                    rp=pd+casesSets[setsi].cases[casei].startPlate-diff;
                    if(!cMDIReadings::isControlPosition(casepi,casesSets[setsi].controlsLayout,
                                          casesSets[setsi].controlsPerPlate,__alelisa) &&
                                          ((casepi>=casesSets[setsi].cases[casei].startCell && dp==rp) || (dp>rp)))
                    {
                        m=cMDIReadings::platePosition(datai%(96/__alelisa_multiplier),__alelisa,casesSets[setsi].orientation);
                        l=m%12;
                        k=(datai/(96/__alelisa_multiplier))*8+m/12;
                        offset=(casesSets[setsi].cases[casei].replicates+1)*samplei+__alelisa_multiplier*replicatesi;
                        if(replicatesi==0)
                        {
                            val=n=0;
                            for(int xi=0;xi<=casesSets[setsi].cases[casei].replicates;xi++,n++)
                                val+=casesSets[setsi].cases[casei].data[(casesSets[setsi].cases[casei].replicates+1)*samplei+__alelisa_multiplier*xi];
                            ndata=val/n;
                        }
                        else
                        {
                            ndata=-999;
                        }
                        deltasi=1;
                        if(__alelisa)
                        {
                            m=cMDIReadings::platePosition(datai%(96/__alelisa_multiplier),__alelisa,casesSets[setsi].orientation,true);
                            l=m%12;
                            k=(datai/(96/__alelisa_multiplier))*8+m/12;
                            offset=(casesSets[setsi].cases[casei].replicates+1)*samplei+__alelisa_multiplier*replicatesi+1;
                            if(replicatesi==0)
                            {
                                val=n=0;
                                for(int xi=0;xi<=casesSets[setsi].cases[casei].replicates;xi++,n++)
                                    val+=casesSets[setsi].cases[casei].data[(casesSets[setsi].cases[casei].replicates+1)*samplei+__alelisa_multiplier*xi+1];
                                aldata=val/n;
                            }
                            else
                                aldata=-999;
                            deltasi=2;
                        }

                        if(ndata!=-999)
                        {
                            double cala;
                            cala=cFormulaRatio::calculateRatio(casesSets[setsi].cala,
                                                               ndata,
                                                               __alelisa?aldata:0,
                                                               ctData,casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate,
                                                               casesSets[setsi].cases[casei].factor,casesSets[setsi].factors,__alelisa);
                            int result=0;
                                double __calapco,__calasco;
                                int r,a;
                                sControlAms cams=cFormulaRatio::calculateControlAms(ctData,casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate,__alelisa);
                                ClearAllVars();
                                SetValue("pa",&cams.posam);
                                SetValue("na",&cams.negam);
                                r=Evaluate((char*)casesSets[setsi].calapco->text(),&__calapco,&a);
                                r=Evaluate((char*)casesSets[setsi].calasco->text(),&__calasco,&a);
                                ClearAllVars();
                                result=0;
                                if(*casesSets[setsi].calaop==">=")
                                    result=cala==-9999?-9999:cala<__calasco?0:cala<__calapco?1:2;
                                else
                                    result=cala==-9999?-9999:cala>__calasco?0:cala>__calapco?1:2;

                            switch(result)
                            {
                                case 0:
                                    stt++;
                                    stn++;
                                    break;
                                case 1:
                                    stt++;
                                    sts++;
                                    break;
                                case 2:
                                    stt++;
                                    stp++;
                                    break;
                                default:
                                    stt++;
                                    ste++;
                                    break;
                            }
                        }
                        else
                        {
                            ste++;
                            stt++;
                        }

                        p2_datai+=deltasi;
                        replicatesi++;
                    }
                    datai++;
                }
                samplei+=deltasi;
            }

            sList->insert(FXStringFormat("%d",sList->no()).text(),sType==2?casesSets[setsi].cases[casei].id->text():casesSets[setsi].cases[casei].assay->text());
            csList->insert(FXStringFormat("%d",csList->no()).text(),
                           FXStringFormat("%s\t%s\t%s\t%s\t%d\t%d\t%d\t%d\t%d",
                                          casesSets[setsi].cases[casei].assay->text(),
                                          casesSets[setsi].cases[casei].id->text(),
                                          casesSets[setsi].cases[casei].bleedDate->text(),
                                          casesSets[setsi].cases[casei].age->text(),
                                          stp,sts,stn,ste,stt).text());


            gstt+=stt;
            gste+=ste;
            gstp+=stp;
            gstn+=stn;
            gsts+=sts;

            p2_datai=1;
        }
        pd+=casesSets[setsi].platesCount;
        int mdi=datai%(96/__alelisa_multiplier);
        if(mdi)
            datai+=(96/__alelisa_multiplier)-mdi;
    }
    repWin->addTitle(oLanguage->getText("str_report_cmclist"),0,true);
    repWin->addText(" ");
    FXString header=FXStringFormat("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",
                                          oLanguage->getText("str_report_sa").text(),
                                          oLanguage->getText("str_report_sc").text(),
                                          oLanguage->getText("str_dlg_case_bldate").text(),
                                          oLanguage->getText("str_rdgmdi_clage").text(),
                                          oLanguage->getText("str_pos").text(),
                                          oLanguage->getText("str_sus").text(),
                                          oLanguage->getText("str_neg").text(),
                                          oLanguage->getText("str_report_err").text(),
                                          oLanguage->getText("str_report_all").text());

    int p=1;
    bool swm=true;
    FXString ss1,ss2,s1,s2;
    while(swm)
    {
        swm=false;
        for(int i=0;i<csList->no()-p;i++)
        {
            FXString k1=FXStringFormat("%d",i),k2=FXStringFormat("%d",i+1);
            ss1=csList->find(k1.text());
            s1=sList->find(k1.text());
            ss2=csList->find(k2.text());
            s2=sList->find(k2.text());
            if(s1>s2)
            {
                swm=true;
                csList->replace(k1.text(),ss2.text());
                csList->replace(k2.text(),ss1.text());
                sList->replace(k1.text(),s2.text());
                sList->replace(k2.text(),s1.text());
            }
        }
    }

    cColorTable *dct=new cColorTable(oWinMain,NULL,0,LAYOUT_FILL_X);
    dct->setTableSize(csList->no()+1,9);
    dct->create();

    int itc=0;
    while(!header.empty() && (itc<dct->getNumColumns()))
    {
        dct->setItemText(0,itc++,header.before('\t'));
        header=header.after('\t');
    }

    for(int i=0;i<csList->no();i++)
    {
        FXString line(csList->find(FXStringFormat("%d",i).text()));
        if(line.empty())
            continue;

        itc=0;
        while(!line.empty() && (itc<dct->getNumColumns()))
        {
            dct->setItemText(i+1,itc++,line.before('\t'));
            line=line.after('\t');
        }
    }

    repWin->addTable(dct);
    repWin->addText(" ");

    header=FXStringFormat("%s\t%s\t%s\t%s\t%s\t%s",
                                          oLanguage->getText("str_report_totals").text(),
                                          oLanguage->getText("str_pos").text(),
                                          oLanguage->getText("str_sus").text(),
                                          oLanguage->getText("str_neg").text(),
                                          oLanguage->getText("str_report_err").text(),
                                          oLanguage->getText("str_report_all").text());

    cColorTable *dct1=new cColorTable(oWinMain,NULL,0,LAYOUT_FILL_X);
    dct1->setTableSize(2,6);
    dct1->create();

    itc=0;
    while(!header.empty() && (itc<dct1->getNumColumns()))
    {
        dct1->setItemText(0,itc++,header.before('\t'));
        header=header.after('\t');
    }

    dct1->setItemText(1,0,FXStringVal(csList->no()));
    dct1->setItemText(1,1,FXStringVal(gstp));
    dct1->setItemText(1,2,FXStringVal(gsts));
    dct1->setItemText(1,3,FXStringVal(gstn));
    dct1->setItemText(1,4,FXStringVal(gste));
    dct1->setItemText(1,5,FXStringVal(gstt));

    repWin->addTable(dct1);
    repWin->addText(" ");
    repWin->addComments();
    oApplicationManager->endWaitCursor();
    msg.hide();
    delete csList;
    delete sList;
    return true;
}

FXbool cTotalCountsReport::askPrefs(void)
{
    dlg=new cDLGReportTC(oWinMain);
    if(!dlg->execute(PLACEMENT_OWNER))
        return false;

    sType=dlg->schoice;
    mcAm=dlg->bcAm->getCheck();
    return true;
}

void cTotalCountsReport::openCases(FXMDIClient *prP, FXPopup *prPup,cSortList &prCases)
{
    oTotalCountsReport=new cTotalCountsReport();

    oTotalCountsReport->repWin=new cMDIReport(prP,oLanguage->getText("str_report_tc"),new FXGIFIcon(oApplicationManager,data_reports_vacd),prPup,MDI_NORMAL,0,0,400,300);
    oTotalCountsReport->repWin->setWidth(770);
    oTotalCountsReport->repWin->setHeight(520);
    //oTotalCountsReport->repWin->maximize();
    oTotalCountsReport->repWin->hide();
    oTotalCountsReport->repWin->create();
    oTotalCountsReport->repWin->setFocus();
    oTotalCountsReport->repWin->setTitle(oLanguage->getText("str_report_tc"));
    oTotalCountsReport->repWin->hide();

    if(!oTotalCountsReport->loadReadings(prCases) || !oTotalCountsReport->askPrefs() || !oTotalCountsReport->processReadings())
    {
        delete oTotalCountsReport->repWin;
        return;
    }

    oTotalCountsReport->repWin->recalc();
    oTotalCountsReport->repWin->show();
}


