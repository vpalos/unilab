#ifndef CGROWERMANAGER_H
#define CGROWERMANAGER_H

#include <fx.h>

typedef struct sGrowerObject
{
    FXString *id;
    FXString *title;
    int solid;
};

class cGrowerManager
{
    private:
    protected:
    public:
        static FXbool growerSolid(const FXString &prId);
        static FXbool growerExists(const FXString &prId);
        static FXbool growerNeeded(const FXString &prId);
        static FXString getNewID(void);
        static int getGrowerCount(void);
        
        static sGrowerObject *listGrowers(void);
        static cDataResult *getGrower(const FXString &prId);
        
        static FXString newGrower(FXMDIClient *prP, FXPopup *prPup);
        static FXString editGrower(FXMDIClient *prP, FXPopup *prPup,const FXString &prId);
        static FXString duplicateGrower(const FXString &prId);
        
        static FXbool addGrower(FXString prId,FXString prCtp,FXString prAdd,FXString prCity,FXString prState,FXString prZip,FXString prPhone,FXString prFax,FXString prEmail,FXString prComments);
        static FXbool setGrower(FXString prOldId,FXString prId,FXString prCtp,FXString prAdd,FXString prCity,FXString prState,FXString prZip,FXString prPhone,FXString prFax,FXString prEmail,FXString prComments);
        static FXbool removeGrower(const FXString &prId);
};

#endif
