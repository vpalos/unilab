#ifndef CDATARESULT_H
#define CDATARESULT_H

#include <sqlite3.h>
#include <fx.h>

class cDataResult
{
    private:
        char **result;
        int rowCount,fieldCount,affectedCount;
    
    protected:
    public:
        cDataResult(char **prResult,int prRowCount,int prFieldCount);
        ~cDataResult();
    
        void free(void);
        
        int getRowCount(void);
        int getFieldCount(void);
        
        FXString getFieldName(int prIndex);
        
        char **getRow(int prIndex);
        void *getCell(int prRow,int prField);
        FXString getCellString(int prRow,int prField);
        int getCellInt(int prRow,int prField);
        long long int getCellLongInt(int prRow,int prField);
        float getCellFloat(int prRow,int prField);
        double getCellDouble(int prRow,int prField);
};

#endif
