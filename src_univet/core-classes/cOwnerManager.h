#ifndef COWNERMANAGER_H
#define COWNERMANAGER_H

#include <fx.h>

typedef struct sOwnerObject
{
    FXString *id;
    FXString *title;
    int solid;
};

class cOwnerManager
{
    private:
    protected:
    public:
        static FXbool ownerSolid(const FXString &prId);
        static FXbool ownerExists(const FXString &prId);
        static FXbool ownerNeeded(const FXString &prId);
        static FXString getNewID(void);
        static int getOwnerCount(void);
        
        static sOwnerObject *listOwners(void);
        static cDataResult *getOwner(const FXString &prId);
        
        static FXString newOwner(FXMDIClient *prP, FXPopup *prPup);
        static FXString editOwner(FXMDIClient *prP, FXPopup *prPup,const FXString &prId);
        static FXString duplicateOwner(const FXString &prId);
        
        static FXbool addOwner(FXString prId,FXString prCtp,FXString prAdd,FXString prCity,FXString prState,FXString prZip,FXString prPhone,FXString prFax,FXString prEmail,FXString prComments);
        static FXbool setOwner(FXString prOldId,FXString prId,FXString prCtp,FXString prAdd,FXString prCity,FXString prState,FXString prZip,FXString prPhone,FXString prFax,FXString prEmail,FXString prComments);
        static FXbool removeOwner(const FXString &prId);
};

#endif
