#include "engine.h"
#include "cDataLink.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cMDIVeterinarian.h"
#include "cVeterinarianManager.h"

FXbool cVeterinarianManager::veterinarianSolid(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_veterinarians WHERE id='"+ress.trim().substitute("'","")+"' AND solid=1;");
    return res->getRowCount()>0;
}

FXbool cVeterinarianManager::veterinarianExists(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_veterinarians WHERE id='"+ress.trim().substitute("'","")+"';");
    return (res->getRowCount()>0) || (prId=="---");
}

FXbool cVeterinarianManager::veterinarianNeeded(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_gncases WHERE veterinarian_oid='"+ress.trim().substitute("'","")+"';");
    return (res->getRowCount()>0);
}

FXString cVeterinarianManager::getNewID(void)
{
    static int vetNr=oApplicationManager->reg().readIntEntry("oids","veterinarians",1);
    FXString res=oLanguage->getText("str_vetmdi_vettitle");
    FXString ret=res+FXStringFormat(" #%d",++vetNr);
    while(veterinarianExists(ret))
        ret=res+FXStringFormat(" #%d",++vetNr);
    oApplicationManager->reg().writeIntEntry("oids","veterinarians",vetNr);
    return ret;
}

int cVeterinarianManager::getVeterinarianCount(void)
{
    if(!oDataLink->isOpened())
        return 0;
    cDataResult *res=oDataLink->execute("SELECT COUNT(id) FROM t_veterinarians;");
    return res->getCellInt(0,0);
}

sVeterinarianObject *cVeterinarianManager::listVeterinarians()
{
    if(!oDataLink->isOpened())
        return NULL;

    sVeterinarianObject *ret=(sVeterinarianObject*)malloc(getVeterinarianCount()*sizeof(sVeterinarianObject));
    if(!ret)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }
    cDataResult *res=oDataLink->execute(("SELECT id,laboratory,solid FROM t_veterinarians;"));
    for(int i=0;i<res->getRowCount();i++)
    {
        ret[i].id=new FXString(res->getCellString(i,0));
        ret[i].title=new FXString(res->getCellString(i,1));
        ret[i].solid=res->getCellInt(i,2);
    }
    return ret;
}

cDataResult *cVeterinarianManager::getVeterinarian(const FXString &prId)
{
    FXString res=prId;
    return oDataLink->execute("SELECT * FROM t_veterinarians WHERE id='"+res+"';");
}

FXString cVeterinarianManager::newVeterinarian(FXMDIClient *prP, FXPopup *prPup)
{
    FXString ret=getNewID();
    cMDIVeterinarian *vetWin=new cMDIVeterinarian(prP,ret,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,300,570);
    vetWin->create();
    vetWin->setFocus();
    return ret;
}

FXString cVeterinarianManager::editVeterinarian(FXMDIClient *prP, FXPopup *prPup,const FXString &prId)
{
    cMDIVeterinarian *vetWin=new cMDIVeterinarian(prP,prId,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,300,570);
    vetWin->create();
    vetWin->loadVeterinarian(prId);
    vetWin->setFocus();
    return prId;
}

FXString cVeterinarianManager::duplicateVeterinarian(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return "";
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT * FROM t_veterinarians WHERE id='"+ress.trim().substitute("'","")+"';");
    if(res->getRowCount()<=0)
        return "";
    int i=1;
    FXString ret=res->getCellString(0,0);
    while(veterinarianExists(ret))
        ret=res->getCellString(0,0)+FXStringFormat(" #%d",i++);
    FXString res2="INSERT INTO t_veterinarians VALUES('"+ret.trim().substitute("'","")+"'";
    for(i=1;i<res->getFieldCount();i++)
        res2=res2+",'"+res->getCellString(0,i).trim().substitute("'","")+"'";
    res2=res2+");";
    res=oDataLink->execute(res2);
    return ret;
}

FXbool cVeterinarianManager::addVeterinarian(FXString prId,FXString prLab,FXString prAdd,FXString prCity,FXString prState,FXString prZip,FXString prPhone,FXString prFax,FXString prEmail,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("INSERT INTO t_veterinarians VALUES('"+
                                                    prId.trim().substitute("'","")+"','"+
                                                    prLab.trim().substitute("'","")+"','"+
                                                    "0','"+
                                                    prAdd.trim().substitute("'","")+"','"+
                                                    prCity.trim().substitute("'","")+"','"+
                                                    prState.trim().substitute("'","")+"','"+
                                                    prZip.trim().substitute("'","")+"','"+
                                                    prPhone.trim().substitute("'","")+"','"+
                                                    prFax.trim().substitute("'","")+"','"+
                                                    prEmail.trim().substitute("'","")+"','"+
                                                    prComments.trim().substitute("'","")+"');");
    return (oDataLink->getAffectedRowCount()==1);
}

FXbool cVeterinarianManager::setVeterinarian(FXString prOldId,FXString prId,FXString prLab,FXString prAdd,FXString prCity,FXString prState,FXString prZip,FXString prPhone,FXString prFax,FXString prEmail,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("UPDATE t_veterinarians SET id='"+prId.trim().substitute("'","")+
                       "', laboratory='"+prLab.trim().substitute("'","")+
                       "', solid='0"+
                       "', address='"+prAdd.trim().substitute("'","")+
                       "', city='"+prCity.trim().substitute("'","")+
                       "', state='"+prState.trim().substitute("'","")+
                       "', zip='"+prZip.trim().substitute("'","")+
                       "', phone='"+prPhone.trim().substitute("'","")+
                       "', fax='"+prFax.trim().substitute("'","")+
                       "', email='"+prEmail.trim().substitute("'","")+
                       "', comments='"+prComments.trim().substitute("'","")+
                       "' WHERE id='"+prOldId.trim().substitute("'","")+"' AND solid='0';");
    int ret=oDataLink->getAffectedRowCount();
    oDataLink->execute("UPDATE t_gncases SET veterinarian_oid='"+prId.trim().substitute("'","")+"' WHERE veterinarian_oid='"+prOldId+"';");
    return (ret==1);
}

FXbool cVeterinarianManager::removeVeterinarian(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT oid FROM t_veterinarians WHERE id='"+ress.trim().substitute("'","")+"' AND solid='0';");
    if(!res || res->getRowCount()==0)
        return false;
    res=oDataLink->execute("DELETE FROM t_veterinarians WHERE id='"+ress.trim().substitute("'","")+"' AND solid='0';");
    return (res!=NULL && oDataLink->getAffectedRowCount()>0);
}


