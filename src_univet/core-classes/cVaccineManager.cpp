#include "engine.h"
#include "cDataLink.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cMDIVaccine.h"
#include "cVaccineManager.h"

FXbool cVaccineManager::vaccineSolid(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_vaccines WHERE id='"+ress.trim().substitute("'","")+"' AND solid='1';");
    return res->getRowCount()>0;
}

FXbool cVaccineManager::vaccineExists(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_vaccines WHERE id='"+ress.trim().substitute("'","")+"';");
    return (res->getRowCount()>0) || (prId=="---");
}

FXbool cVaccineManager::vaccineNeeded(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_gnpopulations WHERE vaccine_oid='"+ress.trim().substitute("'","")+"';");
    return res->getRowCount()>0;
}

FXString cVaccineManager::getNewID(void)
{
    static int vacNr=oApplicationManager->reg().readIntEntry("oids","vaccines",1);
    FXString res=oLanguage->getText("str_vacmdi_vactitle");
    FXString ret=res+FXStringFormat(" #%d",++vacNr);
    while(vaccineExists(ret))
        ret=res+FXStringFormat(" #%d",++vacNr);
    oApplicationManager->reg().writeIntEntry("oids","vaccines",vacNr);
    return ret;
}

int cVaccineManager::getVaccineCount(void)
{
    if(!oDataLink->isOpened())
        return 0;
    cDataResult *res=oDataLink->execute("SELECT COUNT(id) FROM t_vaccines;");
    return res->getCellInt(0,0);
}

sVaccineObject *cVaccineManager::listVaccines()
{
    if(!oDataLink->isOpened())
        return NULL;

    sVaccineObject *ret=(sVaccineObject*)malloc(getVaccineCount()*sizeof(sVaccineObject));
    if(!ret)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }
    cDataResult *res=oDataLink->execute(("SELECT id,title,solid FROM t_vaccines;"));
    for(int i=0;i<res->getRowCount();i++)
    {
        ret[i].id=new FXString(res->getCellString(i,0));
        ret[i].title=new FXString(res->getCellString(i,1));
        ret[i].solid=res->getCellInt(i,2);
    }
    return ret;
}

cDataResult *cVaccineManager::getVaccine(const FXString &prId)
{
    FXString res=prId;
    return oDataLink->execute("SELECT * FROM t_vaccines WHERE id='"+res.trim().substitute("'","")+"';");
}

FXString cVaccineManager::newVaccine(FXMDIClient *prP, FXPopup *prPup)
{
    FXString ret=getNewID();
    cMDIVaccine *vacWin=new cMDIVaccine(prP,ret,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,629,407);
    vacWin->create();
    vacWin->setFocus();
    return ret;
}

FXString cVaccineManager::editVaccine(FXMDIClient *prP, FXPopup *prPup,const FXString &prId)
{
    cMDIVaccine *vacWin=new cMDIVaccine(prP,prId,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,629,407);
    vacWin->create();
    vacWin->loadVaccine(prId);
    vacWin->setFocus();
    return prId;
}

FXString cVaccineManager::duplicateVaccine(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return "";
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT * FROM t_vaccines WHERE id='"+ress.trim().substitute("'","")+"';");
    if(res->getRowCount()<=0)
        return "";
    int i=1;
    FXString ret=res->getCellString(0,0);
    while(vaccineExists(ret))
        ret=res->getCellString(0,0)+FXStringFormat(" #%d",i++);
    FXString res2="INSERT INTO t_vaccines VALUES('"+ret.trim().substitute("'","")+"'";
    for(i=1;i<res->getFieldCount();i++)
        res2=res2+",'"+res->getCellString(0,i).trim().substitute("'","")+"'";
    res2=res2+");";
    res=oDataLink->execute(res2);
    return ret;
}

FXbool cVaccineManager::addVaccine(FXString prId,FXString prTitle,FXString prAssays_defs,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("INSERT INTO t_vaccines VALUES('"+
                                                    prId.trim().substitute("'","")+"','"+
                                                    prTitle.trim().substitute("'","")+"','"+
                                                    "0','"+
                                                    prAssays_defs.trim().substitute("'","")+"','"+
                                                    prComments.trim().substitute("'","")+"');");
    return (oDataLink->getAffectedRowCount()==1);
}

FXbool cVaccineManager::setVaccine(FXString prOldId,FXString prId,FXString prTitle,FXString prAssays_defs,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("UPDATE t_vaccines SET id='"+prId.trim().substitute("'","")+
                       "', title='"+prTitle.trim().substitute("'","")+
                       "', solid='0"+
                       "', assays_defs='"+prAssays_defs.trim().substitute("'","")+
                       "', comments='"+prComments.trim().substitute("'","")+
                       "' WHERE id='"+prOldId.trim().substitute("'","")+"' AND id!='---';");
    int ret=oDataLink->getAffectedRowCount();
    oDataLink->execute("UPDATE t_gnpopulations SET vaccine_oid='"+prId.trim().substitute("'","")+"' WHERE vaccine_oid='"+prOldId+"';");
    return (ret==1);
}

FXbool cVaccineManager::removeVaccine(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT oid FROM t_vaccines WHERE id='"+ress.trim().substitute("'","")+"' AND solid='0';");
    if(!res || res->getRowCount()==0)
        return false;
    res=oDataLink->execute("DELETE FROM t_vaccines WHERE id='"+ress.trim().substitute("'","")+"' AND solid='0';");
    return (res!=NULL && oDataLink->getAffectedRowCount()>0);
}


