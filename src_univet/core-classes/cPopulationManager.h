#ifndef CPOPULATIONMANAGER_H
#define CPOPULATIONMANAGER_H

#include <fx.h>
#include "cDataLink.h"

class cPopulationManager
{
    private:
    protected:
    public:
        static FXbool populationExists(const FXString &prTitle);
        static FXString getNewID(void);
        static cDataResult *getPopulation(FXString prId);

        static FXString getPopulationAge(const FXString &prTitle,const FXString &prDate);

        static FXbool addPopulation(cDataLink *prLink,FXString prId,FXString prPredId,FXString prBirthDate,FXString prBreed1,FXString prBreed2,FXString prOwner_oid,FXString prGrower_oid,FXString prVaccine_oid,FXString prUnit,FXString prLocation,FXString prSublocation,FXString prComments);
        static FXbool addPopulation(FXString prId,FXString prPredId,FXString prBirthDate,FXString prBreed1,FXString prBreed2,FXString prOwner_oid,FXString prGrower_oid,FXString prVaccine_oid,FXString prUnit,FXString prLocation,FXString prSublocation,FXString prComments);
        static FXbool setPopulation(FXString prOldId,FXString prPredId,FXString prId,FXString prBirthDate,FXString prBreed1,FXString prBreed2,FXString prOwner_oid,FXString prGrower_oid,FXString prVaccine_oid,FXString prUnit,FXString prLocation,FXString prSublocation,FXString prComments);
        static FXbool removePopulation(const FXString &prId);
};

#endif
