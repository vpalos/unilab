#include <stdio.h>
#include <sqlite3.h>
#include "engine.h"
#include "cLanguage.h"
#include "cDataResult.h"
#include "cWinMain.h"

cDataResult::cDataResult(char **prResult,int prRowCount,int prFieldCount)
{
    result=prResult;
    rowCount=prRowCount;
    fieldCount=prFieldCount;
}

cDataResult::~cDataResult()
{
    free();
}

void cDataResult::free(void)
{
    if(result!=NULL)
        sqlite3_free_table(result);
}

int cDataResult::getRowCount(void)
{
    return rowCount;
}

int cDataResult::getFieldCount(void)
{
    return fieldCount;
}

FXString cDataResult::getFieldName(int prIndex)
{
    if(result==NULL || prIndex>=fieldCount)
        return (char*)NULL;
    return result[prIndex];
}

char **cDataResult::getRow(int prIndex)
{
    if(result==NULL || prIndex>=rowCount)
        return NULL;
    return (result+(prIndex+1)*fieldCount);
}

void *cDataResult::getCell(int prRow,int prField)
{
    if(result==NULL || prRow>=rowCount || prField>=fieldCount)
        return NULL;
    return (void*)result[(prRow+1)*fieldCount+prField];
}

FXString cDataResult::getCellString(int prRow,int prField)
{
    void *ret=getCell(prRow,prField);
    if(ret==NULL)
        return "";
    return (char*)ret;
}

int cDataResult::getCellInt(int prRow,int prField)
{
    char *tail;
    return strtol((const char*)getCell(prRow,prField),&tail,0);
}

long long int cDataResult::getCellLongInt(int prRow,int prField)
{
    char *tail;
    return strtoll((const char*)getCell(prRow,prField),&tail,0);
}

float cDataResult::getCellFloat(int prRow,int prField)
{
    char *tail;
    return (float)strtod((const char*)getCell(prRow,prField),&tail);
}

double cDataResult::getCellDouble(int prRow,int prField)
{
    char *tail;
    return strtod((const char*)getCell(prRow,prField),&tail);
}


