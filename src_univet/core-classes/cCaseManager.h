#ifndef CCASEMANAGER_H
#define CCASEMANAGER_H

#include <fx.h>
#include "cDataLink.h"
#include "cDataResult.h"

typedef struct sCaseObject
{
    FXString *id;
    FXString *readingDate;
    FXString *assay_oid;
    FXString *population_oid;
    FXString *population;
    FXString *age;
    FXString *tpl;
    char **res;
    FXint count;
};

class cCaseManager
{
    private:
    protected:
    public:
        static FXbool caseExists(const FXString &prTitle,const FXString &prAssay);
        static FXString getNewID(void);
        static FXString getNewID(FXString prAssay);
        static int getCaseCount(const FXString &prCriteria="");

        static sCaseObject *listCases(const FXString &prCriteria="",FXint *prCount=NULL);
        static cDataResult *getCase(FXString prId,FXString prAssay);

        //static FXString editCase(FXMDIClient *prP, FXPopup *prPup,const FXString &prId);

        static FXbool addCase(cDataLink *prLink,FXString prId,FXString prReading_oid,FXString prReadingDate,FXString prTechnician,FXString prReason,FXString prVeterinarian_oid,FXString prAssay_oid,FXString prLot,FXString prExpirationDate,FXString prSampleType,FXString prBleedDate,FXint prReplicates,FXint prFactor,FXString prPopulation_oid,FXString prAge,FXint prCount,FXString prTemplate,FXint prOrientation,FXint prAlelisa,FXint startPlate,FXint startCell,FXint plateCount,FXString prControls_defs,FXString prData_defs,FXString prComments);
        static FXbool addCase(FXString prId,FXString prReading_oid,FXString prReadingDate,FXString prTechnician,FXString prReason,FXString prVeterinarian_oid,FXString prAssay_oid,FXString prLot,FXString prExpirationDate,FXString prSampleType,FXString prBleedDate,FXint prReplicates,FXint prFactor,FXString prPopulation_oid,FXString prAge,FXint prCount,FXString prTemplate,FXint prOrientation,FXint prAlelisa,FXint startPlate,FXint startCell,FXint plateCount,FXString prControls_defs,FXString prData_defs,FXString prComments);
        static FXbool setCase(FXString prOldId,FXString prOldAssay,FXString prId,FXString prReading_oid,FXString prReadingDate,FXString prTechnician,FXString prReason,FXString prVeterinarian_oid,FXString prAssay_oid,FXString prLot,FXString prExpirationDate,FXString prSampleType,FXString prBleedDate,FXint prReplicates,FXint prFactor,FXString prPopulation_oid,FXString prAge,FXint prCount,FXString prTemplate,FXint prOrientation,FXint prAlelisa,FXint startPlate,FXint startCell,FXint plateCount,FXString prControls_defs,FXString prData_defs,FXString prComments);
        static FXbool setCase(FXString prOldId,FXString prOldAssay,FXString prId,FXString prReason,FXString prVeterinarian_oid,FXString prSampleType,FXString prBleedDate,FXString prPopulation_oid,FXString prAge,FXString prComments);
        static FXbool removeCase(const FXString &prId,const FXString &prAssay);
};

#endif
