#ifndef CVACCINEDATEREPORT_H
#define CVACCINEDATEREPORT_H

#include <fx.h>
#include "cVChart.h"
#include "cColorTable.h"
#include "cDLGReportVD.h"
#include "cMDIReadings.h"
#include "cMDIReport.h"
#include "cSortList.h"

class cVaccineDateReport
{
    private:
        cDLGReportVD *dlg;
        cMDIReport *repWin;
        cColorTable *plateTable2;
        cColorTable *statsTable;
    
        FXint fType;
        FXint mType;
        FXint sType;
        FXdouble tt,sd,p1,p2,hl;
        
        sCasesSet *casesSets;
        int tSetsCount;
        int tCasesCount;
        
    protected:
        FXbool loadReadings(cSortList &prCases);
        FXbool processReadings(void);
        FXbool askPrefs(void);
        
    public:
        static void openCases(FXMDIClient *prP, FXPopup *prPup,cSortList &prCases);
};

#endif
