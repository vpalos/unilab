#ifndef CCASESBSREPORT_H
#define CCASESBSREPORT_H

#include <fx.h>
#include "cVChart.h"
#include "cColorTable.h"
#include "cDLGReportCS.h"
#include "cMDIReadings.h"
#include "cMDIReport.h"
#include "cSortList.h"

typedef struct sCSRSubObject
{
    FXString *results;
    FXint setsi,casei,stdicti;
};

typedef struct sCSRObject
{
    cVChart *chart;
    FXDict *cases;
};

class cCaseSbsReport
{
    private:
        cDLGReportCS *dlg;
        cMDIReport *repWin;
        cColorTable *plateTable2;
        cColorTable *statsTable;
    
        FXbool mcCala;
        FXbool mcCalb;
        FXbool mcTiter;
        FXbool mcTiters;
        FXbool mcBins;
        FXbool mcResult;
        FXbool mcResult2;
        //FXbool mcBins2;
        FXbool mcAm;
        FXbool mcAge;
        FXbool mcLog2;
        FXbool mcEU;
        FXbool mcBreed1;
        FXbool mcBreed2;
        
        FXint graphType;
        FXbool useGraphs;
        FXbool useData;
        FXbool useInfo;
        
        sCasesSet *casesSets;
        int tSetsCount;
        int tCasesCount;
        
        FXbool mcPop;
        FXbool mcGMean;
        
        bool areTiters;
        bool areSecondary;
        
        FXbool baseline;
        FXString bslid;
        FXbool bslss;
        
    protected:
        void sortAges(FXDict *prData,FXDict *prDataSec);        
    
        FXbool loadReadings(cSortList &prCases);
        FXbool processReadings(void);
        FXbool askPrefs(void);
        
        
    public:
        static void openCases(FXMDIClient *prP, FXPopup *prPup,cSortList &prCases);
};

#endif
