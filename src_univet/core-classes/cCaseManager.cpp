#include <ctype.h>
#include <stdio.h>
#include <strings.h>

#include "engine.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cDataLink.h"
#include "cCaseManager.h"
#include "cMDITemplate.h"
#include "cDLGCasePopEdit.h"

FXbool cCaseManager::caseExists(const FXString &prTitle,const FXString &prAssay)
{
    if(!oDataLink->isOpened())
        return false;
    cDataResult *res=oDataLink->execute("SELECT bleedDate FROM t_gncases WHERE id='"+prTitle+"' AND assay_oid='"+prAssay+"';");
    return res->getRowCount()>0;
}

FXString cCaseManager::getNewID()
{
    return getNewID("");
}

FXString cCaseManager::getNewID(FXString prAssay)
{
    static int csNr=oApplicationManager->reg().readIntEntry("oids","cases",1);
    FXString res=oLanguage->getText("str_csmdi_title");
    FXString ret;
    cDataResult *res2;
    bool found;
    do
    {
        found=true;
        ret=res+FXStringFormat(" #%d",++csNr);
        res2=oDataLink->execute("SELECT id FROM t_gncases WHERE id='"+ret+(prAssay.empty()?"":"' AND assay_oid='"+prAssay)+"';");
        if(res2->getRowCount()>0)
            found=false;
    }while(!found);
    oApplicationManager->reg().writeIntEntry("oids","cases",csNr);
    return ret;
}

int cCaseManager::getCaseCount(const FXString &prCriteria)
{
    if(!oDataLink->isOpened())
        return 0;
    cDataResult *res=oDataLink->execute("SELECT COUNT(id) FROM t_gncases"+(prCriteria.empty()?"":" WHERE "+prCriteria)+";");
    return res->getCellInt(0,0);
}

sCaseObject *cCaseManager::listCases(const FXString &prCriteria,FXint *prCount)
{
    if(!oDataLink->isOpened())
        return NULL;

    *prCount=0;
    cDataResult *res=oDataLink->execute("SELECT id,readingDate,assay_oid,population_oid,population_oid,count,template_oid,age FROM t_gncases"+(prCriteria.empty()?"":" WHERE "+prCriteria)+";");
    if(!res->getRowCount())
        return NULL;
    *prCount=res->getRowCount();
    sCaseObject *ret=(sCaseObject*)malloc(*prCount*sizeof(sCaseObject));
    if(!ret)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }
    for(int i=0;i<res->getRowCount();i++)
    {
        ret[i].id=new FXString(res->getCellString(i,0));
        ret[i].readingDate=new FXString(res->getCellString(i,1));
        ret[i].assay_oid=new FXString(res->getCellString(i,2));
        ret[i].population_oid=new FXString(res->getCellString(i,3));
        ret[i].population=new FXString(res->getCellString(i,4));
        ret[i].age=new FXString(res->getCellString(i,7));
        ret[i].count=res->getCellInt(i,5);
        ret[i].tpl=new FXString(res->getCellString(i,6));
    }
    return ret;
}

/*FXString cCaseManager::editCase(FXMDIClient *prP, FXPopup *prPup,const FXString &prId)
{
    cMDICase *cseWin=new cMDICase(prP,prId,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,678,507);
    cseWin->create();
    cseWin->loadCase(prId);
    cseWin->setFocus();
    return prId;
}*/

FXbool cCaseManager::addCase(FXString prId,FXString prReading_oid,FXString prReadingDate,FXString prTechnician,FXString prReason,FXString prVeterinarian_oid,FXString prAssay_oid,FXString prLot,FXString prExpirationDate,FXString prSampleType,FXString prBleedDate,FXint prReplicates,FXint prFactor,FXString prPopulation_oid,FXString prAge,FXint prCount,FXString prTemplate,FXint prOrientation,FXint prAlelisa,FXint startPlate,FXint startCell,FXint plateCount,FXString prControls_defs,FXString prData_defs,FXString prComments)
{
    return addCase(oDataLink,prId,prReading_oid,prReadingDate,prTechnician,prReason,prVeterinarian_oid,prAssay_oid,prLot,prExpirationDate,prSampleType,prBleedDate,prReplicates,prFactor,prPopulation_oid,prAge,prCount,prTemplate,prOrientation,prAlelisa,startPlate,startCell,plateCount,prControls_defs,prData_defs,prComments);
}

FXbool cCaseManager::addCase(cDataLink *prLink,FXString prId,FXString prReading_oid,FXString prReadingDate,FXString prTechnician,FXString prReason,FXString prVeterinarian_oid,FXString prAssay_oid,FXString prLot,FXString prExpirationDate,FXString prSampleType,FXString prBleedDate,FXint prReplicates,FXint prFactor,FXString prPopulation_oid,FXString prAge,FXint prCount,FXString prTemplate,FXint prOrientation,FXint prAlelisa,FXint startPlate,FXint startCell,FXint plateCount,FXString prControls_defs,FXString prData_defs,FXString prComments)
{
    if(!prLink->isOpened())
        return false;
    prLink->execute("INSERT INTO t_gncases VALUES('"+
                                                    prId.trim().substitute("'","")+"','"+
                                                    prReading_oid.trim().substitute("'","")+"','"+
                                                    prReadingDate.trim().substitute("'","")+"','"+
                                                    prTechnician.trim().substitute("'","")+"','"+
                                                    prReason.trim().substitute("'","")+"','"+
                                                    prVeterinarian_oid.trim().substitute("'","")+"','"+
                                                    prAssay_oid.trim().substitute("'","")+"','"+
                                                    prLot.trim().substitute("'","")+"','"+
                                                    prExpirationDate.trim().substitute("'","")+"','"+
                                                    prSampleType.trim().substitute("'","")+"','"+
                                                    prBleedDate.trim().substitute("'","")+"','"+
                                                    FXStringVal(prReplicates)+"','"+
                                                    FXStringVal(prFactor)+"','"+
                                                    prPopulation_oid.trim().substitute("'","")+"','"+
                                                    prAge.trim().substitute("'","")+"','"+
                                                    FXStringVal(prCount)+"','"+
                                                    prTemplate.trim().substitute("'","")+"','"+
                                                    FXStringVal(prOrientation)+"','"+
                                                    FXStringVal(prAlelisa)+"','"+
                                                    FXStringVal(startPlate)+"','"+
                                                    FXStringVal(startCell)+"','"+
                                                    FXStringVal(plateCount)+"','"+
                                                    prControls_defs.trim().substitute("'","")+"','"+
                                                    prData_defs.trim().substitute("'","")+"','"+
                                                    prComments.trim().substitute("'","")+"');");
    return (prLink->getAffectedRowCount()==1);
}

FXbool cCaseManager::setCase(FXString prOldId,FXString prOldAssay,FXString prId,FXString prReason,FXString prVeterinarian_oid,FXString prSampleType,FXString prBleedDate,FXString prPopulation_oid,FXString prAge,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("UPDATE t_gncases SET id='"+prId.trim().substitute("'","")+
                       "', reason='"+prReason.trim().substitute("'","")+
                       "', veterinarian_oid='"+prVeterinarian_oid.trim().substitute("'","")+
                       "', spType='"+prSampleType.trim().substitute("'","")+
                       "', bleedDate='"+prBleedDate.trim().substitute("'","")+
                       "', population_oid='"+prPopulation_oid.trim().substitute("'","")+
                       "', age='"+prAge.trim().substitute("'","")+
                       "', comments='"+prComments.trim().substitute("'","")+
                       "' WHERE id='"+prOldId+"' AND assay_oid='"+prOldAssay+"';");

    return (oDataLink->getAffectedRowCount()==1);
}

FXbool cCaseManager::setCase(FXString prOldId,FXString prOldAssay,FXString prId,FXString prReading_oid,FXString prReadingDate,FXString prTechnician,FXString prReason,FXString prVeterinarian_oid,FXString prAssay_oid,FXString prLot,FXString prExpirationDate,FXString prSampleType,FXString prBleedDate,FXint prReplicates,FXint prFactor,FXString prPopulation_oid,FXString prAge,FXint prCount,FXString prTemplate,FXint prOrientation,FXint prAlelisa,FXint startPlate,FXint startCell,FXint plateCount,FXString prControls_defs,FXString prData_defs,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("UPDATE t_gncases SET id='"+prId.trim().substitute("'","")+
                       "', reading_oid='"+prReading_oid.trim().substitute("'","")+
                       "', readingDate='"+prReadingDate.trim().substitute("'","")+
                       "', technician='"+prTechnician.trim().substitute("'","")+
                       "', reason='"+prReason.trim().substitute("'","")+
                       "', veterinarian_oid='"+prVeterinarian_oid.trim().substitute("'","")+
                       "', assay_oid='"+prAssay_oid.trim().substitute("'","")+
                       "', lot='"+prLot.trim().substitute("'","")+
                       "', expirationDate='"+prExpirationDate.trim().substitute("'","")+
                       "', spType='"+prSampleType.trim().substitute("'","")+
                       "', bleedDate='"+prBleedDate.trim().substitute("'","")+
                       "', replicates='"+FXStringVal(prReplicates)+
                       "', factor='"+FXStringVal(prFactor)+
                       "', population_oid='"+prPopulation_oid.trim().substitute("'","")+
                       "', age='"+prAge.trim().substitute("'","")+
                       "', count='"+FXStringVal(prCount)+
                       "', template_oid='"+prTemplate.trim().substitute("'","")+
                       "', orientation='"+FXStringVal(prOrientation)+
                       "', alelisa='"+FXStringVal(prAlelisa)+
                       "', startPlate='"+FXStringVal(startPlate)+
                       "', startCell='"+FXStringVal(startCell)+
                       "', plateCount='"+FXStringVal(plateCount)+
                       "', controls_defs='"+prControls_defs.trim().substitute("'","")+
                       "', data_defs='"+prData_defs.trim().substitute("'","")+
                       "', comments='"+prComments.trim().substitute("'","")+
                       "' WHERE id='"+prOldId+"' AND assay_oid='"+prOldAssay+"';");

    return (oDataLink->getAffectedRowCount()==1);
}

cDataResult *cCaseManager::getCase(FXString prId,FXString prAssay)
{
    return oDataLink->execute("SELECT * FROM t_gncases WHERE id='"+prId+"' AND assay_oid='"+prAssay+"';");
}

FXbool cCaseManager::removeCase(const FXString &prId,const FXString &prAssay)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *ret;
    ret=oDataLink->execute("SELECT population_oid FROM t_gncases WHERE id='"+ress.trim().substitute("'","")+"';");
    if(!ret)
        return false;
    FXString poid="";
    if(ret->getRowCount()>0)
        poid=ret->getCellString(0,0);
    ret=oDataLink->execute("DELETE FROM t_gncases WHERE id='"+prId+"' AND assay_oid='"+prAssay+"';");
    int okdel=false;
    if(!poid.empty())
    {
        ret=oDataLink->execute("SELECT oid FROM t_gncases WHERE population_oid='"+poid+"';");
        if(ret && (ret->getRowCount()==0))
            okdel=true;
        /*ret=oDataLink->execute("SELECT oid FROM t_gnpopulations WHERE predId_defs LIKE '%\t"+poid+"\t%';");
        if(ret && (ret->getRowCount()==0))
            okdel=true;*/
        if(okdel && (MBOX_CLICKED_YES==FXMessageBox::question(oWinMain,MBOX_YES_NO,
                                                              oLanguage->getText("str_question").text(),
                                                              (oLanguage->getText("str_erasepopsolo1")+" "+poid+"\n\n"+
                                                               oLanguage->getText("str_erasepopsolo2")).text())))
                oDataLink->execute("DELETE FROM t_gnpopulations WHERE id='"+poid+"';");
    }
    return (ret!=NULL);
}

