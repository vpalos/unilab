#ifndef CSERIALLINK_H
#define CSERIALLINK_H

#include <fx.h>

#ifdef WIN32

    #include <strings.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <time.h>
    #include "windows.h"
    
    #define COM1 "COM1"
    #define COM2 "COM2"
    #define COM3 "COM3"
    #define COM4 "COM4"
    
    #define BAUD600 600
    #define BAUD1200 1200
    #define BAUD1800 1800
    #define BAUD2400 2400
    #define BAUD4800 4800
    #define BAUD9600 9600
    #define BAUD19200 19200
    #define BAUD38400 38400
    #define BAUD57600 57600
    #define BAUD115200 115200
    
    #define BITS5 5
    #define BITS6 6
    #define BITS7 7
    #define BITS8 8
    
    #define PARITYNONE 0
    #define PARITYODD 1
    #define PARITYEVEN 2
    
    #define STOPBITS1 1
    #define STOPBITS2 2
    
    #define FLOWNONE 0
    #define FLOWCRTSCTS 1
    
    class cSerialLink
    {
        private:
        protected:
            HANDLE handle;
            int opened;
        
        public:
            cSerialLink();
            ~cSerialLink();
            
            FXbool connect(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout);
            FXbool disconnect(void);
            FXbool isConnected(void);
            
            FXbool setTimeouts(FXint prTimeout);
            void flush(void);
            void sendBreak(int prMs);
            
            int input(char *prBuffer,int prCount);
            int output(const char *prBuffer,int prCount,int prGetEcho=false);
    };

#else

    #include <strings.h>
    #include <sys/signal.h>
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <fcntl.h>
    #include <unistd.h>
    #include <termios.h>
    #include <stdio.h>
    #include <time.h>
    
    #define COM1 "/dev/ttyS0"
    #define COM2 "/dev/ttyS1"
    #define COM3 "/dev/ttyS2"
    #define COM4 "/dev/ttyS3"
    
    #define BAUD600 B600
    #define BAUD1200 B1200
    #define BAUD1800 B1800
    #define BAUD2400 B2400
    #define BAUD4800 B4800
    #define BAUD9600 B9600
    #define BAUD19200 B19200
    #define BAUD38400 B38400
    #define BAUD57600 B57600
    #define BAUD115200 B115200
    
    #define BITS5 CS5
    #define BITS6 CS6
    #define BITS7 CS7
    #define BITS8 CS8
    
    #define PARITYNONE 0
    #define PARITYODD PARENB|PARODD
    #define PARITYEVEN PARENB
    
    #define STOPBITS1 0
    #define STOPBITS2 CSTOPB
    
    #define FLOWNONE 0
    #define FLOWCRTSCTS CRTSCTS
    
    class cSerialLink
    {
        private:
        protected:
            int handle;
            struct termios oldtio,newtio;
        
        public:
            cSerialLink();
            ~cSerialLink();
            
            FXbool connect(const FXString &prSerialDevice,int prBaudRate,int prBits,int prParity,int prStopBits,int prFlow,int prTimeout);
            FXbool disconnect(void);
            FXbool isConnected(void);
            
            FXbool setTimeouts(FXint prTimeout);
            void flush(void);
            void sendBreak(int prMs);
            
            int input(char *prBuffer,int prCount);
            int output(const char *prBuffer,int prCount,int prGetEcho=false);
    };
#endif

#endif

