#ifndef CDATALINK_H
#define CDATALINK_H

#include <fx.h>
#include <sqlite3.h>
#include "cDataResult.h"

typedef struct sDefAssay
{
    FXString id,title,solid,filterA,filterB,factors_defs,calculations_defs,titers_defs,bins_defs,comments,rules_defs;
};

class cDataLink
{
    private:
        FXString dbFile;
        FXString dbName;
        FXString dbSpecies;
        FXbool opened;
        sqlite3 *link;

    protected:
        void vacuumDatabase(void);
        void executeSqlBase(void);

    public:
        cDataLink();
        ~cDataLink();

        void ensureKitUpgrade(FXbool prSilent=false);
        FXbool ensureUpgrade(void);

        FXbool openDesignated(void);
        FXbool openDesignated(const FXString &prDBFile,FXbool prCreate=true,FXObject *prSender=NULL);
        FXbool saveDesignated(void);
        FXbool saveDesignated(const FXString &prDBFile);
        FXbool closeDesignated(void);

        FXbool open(const FXString &prDBFile,FXbool prCreate=true,FXObject *prSender=NULL,FXbool prNoInv=false);
        FXbool close(void);

        FXbool isOpened(void);

        FXString getFilename(void);
        FXString getName(void);
        FXString getSpecies(void);
        FXString getProperty(FXString prProperty);
        FXbool setProperty(FXString prProperty,FXString prValue);

        FXString getInfo(void);
        FXString getLastError(void);
        int getLastErrorCode(void);

        int getLastId(void);
        int getAffectedRowCount(void);

        FXbool tableExists(const FXString &prTable);
        cDataResult *execute(const FXString &prQuery,FXbool prAsk=true);
};

extern cDataLink *oDataLink;

#endif
