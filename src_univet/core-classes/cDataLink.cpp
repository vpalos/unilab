#include <stdio.h>
#include "engine.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cWinMain.h"
#include "cAssayManager.h"
#include "cMDICaseManager.h"

cDataLink *oDataLink;

cDataLink::cDataLink()
{
    link=NULL;
    opened=false;
    dbFile="";
    dbName="";
}

cDataLink::~cDataLink()
{
}

void cDataLink::vacuumDatabase(void)
{
    if(!opened)
        return;

    execute("VACUUM;");
}

void cDataLink::executeSqlBase(void)
{
    if(!opened)
        return;

    execute("\
            CREATE TABLE t_properties(property,value);\
            \
            CREATE TABLE t_baselines(id,solid,params_defs,comments);\
            \
            CREATE TABLE t_templates(id,date,orientation,alelisa,controls_defs,cases_defs);\
            \
            CREATE TABLE t_assays(id,title,solid,filterA,filterB,factors_defs,calculations_defs,titers_defs,bins_defs,comments,rules_defs);\
            CREATE TABLE t_vaccines(id,title,solid,assays_defs,comments);\
            \
            CREATE TABLE t_gncases(id,reading_oid,readingDate,technician,reason,veterinarian_oid,assay_oid,\
                                   lot,expirationDate,spType,bleedDate,replicates,factor,population_oid,age,count,\
                                   template_oid,orientation,alelisa,startPlate,startCell,plateCount,controls_defs,data_defs,comments);\
            CREATE TABLE t_gnpopulations(id,predId_defs,birthDate,breed1,breed2,owner_oid,grower_oid,vaccine_oid,unit,location,sublocation,comments);\
            \
            CREATE TABLE t_veterinarians(id,laboratory,solid,address,city,state,zip,phone,fax,email,comments);\
            CREATE TABLE t_owners(id,ctperson,solid,address,city,state,zip,phone,fax,email,comments);\
            CREATE TABLE t_growers(id,ctperson,solid,address,city,state,zip,phone,fax,email,comments);\
            \
            CREATE TABLE t_breeds(id,title,solid,comments);\
            \
            INSERT INTO t_properties VALUES('database_version','1.0');\
            \
            CREATE TABLE t_reserved0(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved1(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved2(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved3(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved4(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved5(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved6(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved7(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved8(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            CREATE TABLE t_reserved9(id,solid,data0,data1,data2,data3,data4,data5,data6,data7,data8,data9,comments);\
            \
            ",false);
}

void cDataLink::ensureKitUpgrade(FXbool prSilent)
{
    if(!opened)
        return;
    FXDict *dict=new FXDict();

    if(dbSpecies=="poultry")
    {
        sDefAssay *def;

        def=new sDefAssay;
        def->id="AEV.aft";
        def->title="Avian Encephalomeyelitis Virus";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nCorrected S/P\t0.15\t>=\t0.15\n";
        def->titers_defs="1\t1.544\t3.8738\tCorrected S/P\n399\t799\t1499\t2999\t4999\t6999\t8999\t10999\t12999\t14999\t16999\t18999\t20999\t22999\t24999\t26999\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.15\t0.2\t0.3\t0.4\t0.5\t0.6\t0.7\t0.8\t0.9\t1.0\t1.1\t1.2\t1.3\t1.4\t1.5\t1.6\t1.7\t1.8\t1.9\t2.0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->comments="";
        def->rules_defs="Na\t<\t0.4\nPa-Na\t>=\t0.7\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="MGA.aft";
        def->title="Mycoplasma Gallisepticum";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nCorrected S/P\t0.05\t>=\t0.05\n";
        def->titers_defs="1\t1.3644\t4.377\tCorrected S/P\n399\t799\t1499\t2999\t4999\t6999\t8999\t10999\t12999\t14999\t16999\t18999\t20999\t22999\t24999\t26999\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.05\t0.2\t0.3\t0.4\t0.5\t0.6\t0.7\t0.8\t0.9\t1.0\t1.1\t1.2\t1.3\t1.4\t1.5\t1.6\t1.7\t1.8\t1.9\t2.0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->comments="";
        def->rules_defs="Na\t<\t0.4\nPa-Na\t>=\t0.7\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="MSA.aft";
        def->title="Mycoplasma Synoviae";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nCorrected S/P\t0.05\t>=\t0.05\n";
        def->titers_defs="1\t1.3644\t4.377\tCorrected S/P\n399\t799\t1499\t2999\t4999\t6999\t8999\t10999\t12999\t14999\t16999\t18999\t20999\t22999\t24999\t26999\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.05\t0.2\t0.3\t0.4\t0.5\t0.6\t0.7\t0.8\t0.9\t1.0\t1.1\t1.2\t1.3\t1.4\t1.5\t1.6\t1.7\t1.8\t1.9\t2.0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->comments="";
        def->rules_defs="Na\t<\t0.4\nPa-Na\t>=\t0.7\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="IBD.aft";
        def->title="Infectious Bursal Disease";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nCorrected S/P\t0.05\t>=\t0.05\n";
        def->titers_defs="1\t1.3054\t4.3\tCorrected S/P\n399\t799\t1499\t2999\t4999\t6999\t8999\t10999\t12999\t14999\t16999\t18999\t20999\t22999\t24999\t26999\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.05\t0.2\t0.3\t0.4\t0.5\t0.6\t0.7\t0.8\t0.9\t1.0\t1.1\t1.2\t1.3\t1.4\t1.5\t1.6\t1.7\t1.8\t1.9\t2.0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->comments="";
        def->rules_defs="Na\t<\t0.4\nPa-Na\t>=\t0.7\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="IBV.aft";
        def->title="Infectious Bronchitis Virus";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nCorrected S/P\t0.1\t>=\t0.1\n";
        def->titers_defs="1\t1.598\t4.2\tCorrected S/P\n399\t799\t1499\t2999\t4999\t6999\t8999\t10999\t12999\t14999\t16999\t18999\t20999\t22999\t24999\t26999\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.1\t0.2\t0.3\t0.4\t0.5\t0.6\t0.7\t0.8\t0.9\t1.0\t1.1\t1.2\t1.3\t1.4\t1.5\t1.6\t1.7\t1.8\t1.9\t2.0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->comments="";
        def->rules_defs="Na\t<\t0.4\nPa-Na\t>=\t0.7\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="REO.aft";
        def->title="Reovirus";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nCorrected S/P\t0.1\t>=\t0.1\n";
        def->titers_defs="1\t1.7\t4.302\tCorrected S/P\n399\t799\t1499\t2999\t4999\t6999\t8999\t10999\t12999\t14999\t16999\t18999\t20999\t22999\t24999\t26999\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.10\t0.2\t0.3\t0.4\t0.5\t0.6\t0.7\t0.8\t0.9\t1.0\t1.1\t1.2\t1.3\t1.4\t1.5\t1.6\t1.7\t1.8\t1.9\t2.0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->comments="";
        def->rules_defs="Na\t<\t0.4\nPa-Na\t>=\t0.7\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="MGA+MSA.aft";
        def->title="Mycoplasma Combination";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nCorrected S/P\t0.05\t>=\t0.05\n";
        def->titers_defs="1\t1.3644\t4.377\tCorrected S/P\n399\t799\t1499\t2999\t4999\t6999\t8999\t10999\t12999\t14999\t16999\t18999\t20999\t22999\t24999\t26999\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.1\t0.2\t0.3\t0.4\t0.5\t0.6\t0.7\t0.8\t0.9\t1.0\t1.1\t1.2\t1.3\t1.4\t1.5\t1.6\t1.7\t1.8\t1.9\t2.0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->comments="";
        def->rules_defs="Na\t<\t0.4\nPa-Na\t>=\t0.7\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="ALV.aft";
        def->title="Avian Leukosis Virus";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nCorrected S/P\t0.1\t>=\t0.1\n";
        def->titers_defs="1\t1.398\t4\tCorrected S/P\n399\t799\t1499\t2999\t4999\t6999\t8999\t10999\t12999\t14999\t16999\t18999\t20999\t22999\t24999\t26999\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.1\t0.2\t0.3\t0.4\t0.5\t0.6\t0.7\t0.8\t0.9\t1.0\t1.1\t1.2\t1.3\t1.4\t1.5\t1.6\t1.7\t1.8\t1.9\t2.0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->comments="";
        def->rules_defs="Na\t<\t0.4\nPa-Na\t>=\t0.7\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="AIV.aft";
        def->title="Avian Influenza Virus";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nCorrected S/P\t0.15\t>=\t0.15\n";
        def->titers_defs="1\t2.122\t4.35\tCorrected S/P\n399\t799\t1499\t2999\t4999\t6999\t8999\t10999\t12999\t14999\t16999\t18999\t20999\t22999\t24999\t26999\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.15\t0.2\t0.3\t0.4\t0.5\t0.6\t0.7\t0.8\t0.9\t1.0\t1.1\t1.2\t1.3\t1.4\t1.5\t1.6\t1.7\t1.8\t1.9\t2.0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->comments="";
        def->rules_defs="Na\t<\t0.4\nPa-Na\t>=\t0.7\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="EDS.pst";
        def->title="Egg Drop Sindrome";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nCorrected S/P\t0.3\t>=\t0.25\n";
        def->titers_defs="1\t2.0406\t4.1258\tCorrected S/P\n789\t1145\t1568\t2060\t3247\t4711\t7428\t9598\t12032\t14758\t17769\t22820\t30558\t39451\t49502\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.25\t0.3\t0.5\t0.7\t0.9\t1.1\t1.3\t1.5\t1.7\t1.9\t2.1\t2.3\t2.5\t2.7\t2.9\t3.1\t3.3\t3.5\t3.7\t3.9\t4.1\t4.3\t4.5\t4.7\t4.9\t5.1\t5.3\t5.5\t5.7\t5.9\t";
        def->comments="";
        def->rules_defs="Na\t<=\t0.3\nPa\t>=\t0.8\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="NDV.aft";
        def->title="Newcastle Disease Virus";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nCorrected S/P\t0.15\t>=\t0.15\n";
        def->titers_defs="1\t1.5825\t3.9057\tCorrected S/P\n399\t799\t1499\t2999\t4999\t6999\t8999\t10999\t12999\t14999\t16999\t18999\t20999\t22999\t24999\t26999\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.15\t0.2\t0.3\t0.4\t0.5\t0.6\t0.7\t0.8\t0.9\t1.0\t1.1\t1.2\t1.3\t1.4\t1.5\t1.6\t1.7\t1.8\t1.9\t2.0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->comments="";
        def->rules_defs="Na\t<\t0.4\nPa-Na\t>=\t0.7\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="NDV.pst";
        def->title="Newcastle Disease Virus";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nRaw S/P\t0.2\t>=\t0.15\n";
        def->titers_defs="1\t1.5506\t3.863\tRaw S/P\n385\t600\t1000\t1432\t2887\t4670\t6000\t7000\t8000\t9000\t10000\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.15\t0.2\t0.35\t0.75\t1.0\t1.1\t1.2\t1.3\t1.4\t1.5\t1.6\t1.7\t1.8\t1.9\t2.0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->comments="";
        def->rules_defs="Na\t<\t0.2\nPa\t>=\t0.7\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="ILT.pst";
        def->title="Infectious Laringotracheitis Virus";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nCorrected S/P\t0.2\t>=\t0.15\n";
        def->titers_defs="1\t1.9075\t4.1728\tCorrected S/P\n399\t799\t1499\t2999\t4999\t6999\t8999\t10999\t12999\t14999\t16999\t18999\t20999\t22999\t24999\t26999\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.15\t0.2\t0.4\t0.6\t0.8\t1.0\t1.1\t1.3\t1.5\t1.7\t1.9\t2.1\t2.3\t2.5\t2.7\t2.9\t3.1\t3.3\t3.5\t3.7\t3.9\t4.1\t4.3\t4.5\t4.7\t4.9\t5.1\t5.3\t5.5\t5.7\t";
        def->comments="";
        def->rules_defs="Na\t<=\t0.3\nPa\t>=\t0.8\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="IBV.pst";
        def->title="Infectious Bronchitis Virus";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nCorrected S/P\t0.15\t>=\t0.05\n";
        def->titers_defs="1\t0.7759\t3.0406\tCorrected S/P\n107\t252\t340\t431\t590\t738\t923\t1100\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.05\t0.15\t0.23\t0.3\t0.45\t0.6\t0.8\t1.0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->comments="";
        def->rules_defs="Na\t<=\t0.2\nPa\t>=\t0.6\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="SE.pst";
        def->title="Salmonella Enteriditis";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nCorrected S/P\t0.35\t>=\t0.2\n";
        def->titers_defs="1\t0.9397\t3.0244\tCorrected S/P\n197\t332\t799\t1499\t2999\t4999\t6999\t8999\t10999\t12999\t14999\t16999\t18999\t20999\t22999\t24999\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.2\t0.35\t0.6\t0.8\t1.0\t1.1\t1.3\t1.5\t1.7\t1.9\t2.1\t2.3\t2.5\t2.7\t2.9\t3.1\t3.3\t3.5\t3.7\t3.9\t4.1\t4.3\t4.5\t4.7\t4.9\t5.1\t5.3\t5.5\t5.7\t5.9\t";
        def->comments="";
        def->rules_defs="Na\t<\t0.2\nPa\t>\t0.6\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="ST.pst";
        def->title="Salmonella Typhimurium";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nCorrected S/P\t0.35*(Pa-Na)+Na\t>=\t0.2*(Pa-Na)+Na\n";
        def->titers_defs="1\t0.9284\t2.944\tCorrected S/P\n233\t395\t799\t1499\t2999\t4999\t6999\t8999\t10999\t12999\t14999\t16999\t18999\t20999\t22999\t24999\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.2\t0.35\t0.6\t0.8\t1.0\t1.1\t1.3\t1.5\t1.7\t1.9\t2.1\t2.3\t2.5\t2.7\t2.9\t3.1\t3.3\t3.5\t3.7\t3.9\t4.1\t4.3\t4.5\t4.7\t4.9\t5.1\t5.3\t5.5\t5.7\t5.9\t";
        def->comments="";
        def->rules_defs="Na\t<\t0.2\nPa\t>\t0.6\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="AIVB.pst";
        def->title="Avian Influenza Virus (Blocking LSI Kit)";
        def->solid="1";
        def->filterA="450";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nBlock Neg\t50\t>=\t40\n";
        def->titers_defs="0\t0\t0\tRaw S/P\n0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="40\t50\t60\t70\t80\t90\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->comments="";
        def->rules_defs="Na\t>\t0.5\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        bool first=true;
        for(int i=0;i<dict->no();i++)
        {
            def=(sDefAssay*)dict->find(FXStringFormat("%d",i).text());
            if(!def)
                continue;
            if(!cAssayManager::assayExists(def->id))
            {
                if(first)
                {
                    if(!prSilent)
                    {
                        if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_upgdb").text()))
                            break;
                    }
                    first=false;
                }
                cAssayManager::addAssay(def->id,def->solid,def->title,def->filterA,def->filterB,def->factors_defs,def->calculations_defs,def->titers_defs,def->bins_defs,def->comments,def->rules_defs);
            }
        }
    }

    if(dbSpecies=="swine")
    {
        sDefAssay *def;

        def=new sDefAssay;
        def->id="CSF.pst";
        def->title="Classical Swine Fever";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nRaw S/P\t0.3\t>=\t0.25\n";
        def->titers_defs="0\t0\t0\tRaw S/P\n0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.25\t0.3\t0.5\t0.7\t0.9\t1.1\t1.3\t1.5\t1.7\t1.9\t2.1\t2.3\t2.5\t2.7\t2.9\t3.1\t3.3\t3.5\t3.7\t3.9\t4.1\t4.3\t4.5\t4.7\t4.9\t5.1\t5.3\t5.5\t5.7\t5.9\t";
        def->comments="";
        def->rules_defs="Na\t<=\t0.15\nPa\t>=\t0.7\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="ECOLI-LT.pst";
        def->title="Eschrichia COLI (LT)";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nRaw S/P\t0.35\t>=\t0.25\n";
        def->titers_defs="0\t0\t0\tRaw S/P\n0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.25\t0.35\t0.55\t0.75\t0.95\t1.15\t1.35\t1.55\t1.75\t1.95\t2.15\t2.35\t2.55\t2.75\t2.95\t3.15\t3.35\t3.55\t3.75\t3.95\t4.15\t4.35\t4.55\t4.75\t4.95\t5.15\t5.35\t5.55\t5.75\t5.95\t";
        def->comments="";
        def->rules_defs="Na\t<=\t0.2\nPa\t>=\t0.7\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="AD.pst";
        def->title="Aujeszky Disease";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nRaw S/P\t0.9\t>=\t0.7\n";
        def->titers_defs="0\t0\t0\tRaw S/P\n0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.7\t0.9\t1.1\t1.3\t1.5\t1.7\t1.9\t2.1\t2.3\t2.5\t2.7\t2.9\t3.1\t3.3\t3.5\t3.7\t3.9\t4.1\t4.3\t4.5\t4.7\t4.9\t5.1\t5.3\t5.5\t5.7\t5.9\t6.1\t6.3\t6.5\t";
        def->comments="";
        def->rules_defs="Pa\t>=\t0.3\nPa/Na\t>=\t3\nPa/Na\t<=\t5\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="SS.pst";
        def->title="Swine Salmonella";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nCorrected S/P\t0.25\t>=\t0.25\n";
        def->titers_defs="0\t0\t0\tRaw S/P\n0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.25\t0.4\t0.6\t0.8\t1.1\t1.3\t1.5\t1.7\t1.9\t2.1\t2.3\t2.5\t2.7\t2.9\t3.1\t3.3\t3.5\t3.7\t3.9\t4.1\t4.3\t4.5\t4.7\t4.9\t5.1\t5.3\t5.5\t5.7\t5.9\t6.1\t";
        def->comments="";
        def->rules_defs="Na\t<=\t0.2\nPa\t>=\t0.8\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="TRICH.pst";
        def->title="Trichinella Spiralis";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nRaw OD\t2.1*Na\t>=\t2.1*Na\n";
        def->titers_defs="0\t0\t0\tRaw OD\n0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.2\t0.4\t0.6\t0.8\t1.1\t1.3\t1.5\t1.7\t1.9\t2.1\t2.3\t2.5\t2.7\t2.9\t3.1\t3.3\t3.5\t3.7\t3.9\t4.1\t4.3\t4.5\t4.7\t4.9\t5.1\t5.3\t5.5\t5.7\t5.9\t6.1\t";
        def->comments="";
        def->rules_defs="Na\t<=\t0.1\nPa\t>=\t1.0\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        /*def=new sDefAssay;
        def->id="BRUP.pst";
        def->title="Brucella Suis";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nRaw OD\t2.1*Na\t>=\t2.1*Na\n";
        def->titers_defs="0\t0\t0\tRaw S/P\n0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.2\t0.4\t0.6\t0.8\t1.1\t1.3\t1.5\t1.7\t1.9\t2.1\t2.3\t2.5\t2.7\t2.9\t3.1\t3.3\t3.5\t3.7\t3.9\t4.1\t4.3\t4.5\t4.7\t4.9\t5.1\t5.3\t5.5\t5.7\t5.9\t6.1\t";
        def->comments="";
        def->rules_defs="Na\t<=\t0.1\nPa\t>=\t1.0\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);*/

        bool first=true;
        for(int i=0;i<dict->no();i++)
        {
            def=(sDefAssay*)dict->find(FXStringFormat("%d",i).text());
            if(!def)
                continue;
            if(!cAssayManager::assayExists(def->id))
            {
                if(first)
                {
                    if(!prSilent)
                    {
                        if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_upgdb").text()))
                            break;
                    }
                    first=false;
                }
                cAssayManager::addAssay(def->id,def->solid,def->title,def->filterA,def->filterB,def->factors_defs,def->calculations_defs,def->titers_defs,def->bins_defs,def->comments,def->rules_defs);
            }
        }
    }

    if(dbSpecies=="bovine")
    {
        sDefAssay *def;

        def=new sDefAssay;
        def->id="PARA-B.pst";
        def->title="Mycobacterium Paratuberculosis";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nRaw OD\t0.4+Na\t>=\t0.4+Na\n";
        def->titers_defs="0\t0\t0\tRaw OD\n0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.2\t0.3\t0.5\t0.7\t0.9\t1.1\t1.3\t1.5\t1.7\t1.9\t2.1\t2.3\t2.5\t2.7\t2.9\t3.1\t3.3\t3.5\t3.7\t3.9\t4.1\t4.3\t4.5\t4.7\t4.9\t5.1\t5.3\t5.5\t5.7\t5.9\t";
        def->comments="";
        def->rules_defs="Na\t<=\t0.35\nPa\t>=\t1.0\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="IBR.pst";
        def->title="Infectious Bovine Rhinotracheitis";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nRaw S/P\t0.27\t>=\t0.25\n";
        def->titers_defs="0\t0\t0\tRaw S/P\n0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.25\t0.27\t0.4\t0.6\t0.8\t1.0\t1.1\t1.3\t1.5\t1.7\t1.9\t2.1\t2.3\t2.5\t2.7\t2.9\t3.1\t3.3\t3.5\t3.7\t3.9\t4.1\t4.3\t4.5\t4.7\t4.9\t5.1\t5.3\t5.5\t5.7\t";
        def->comments="";
        def->rules_defs="Na\t<=\t0.25\nPa\t>=\t0.7\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="PTRM.pst";
        def->title="Ruminant Paratuberculosis (LSI Kit)";
        def->solid="1";
        def->filterA="450";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nRaw S/P\t0.7\t>=\t0.6\n";
        def->titers_defs="0\t0\t0\tRaw S/P\n0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.6\t0.7\t2.0\t3.0\t4.0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->comments="";
        def->rules_defs="Na\t<\t0.25\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        bool first=true;
        for(int i=0;i<dict->no();i++)
        {
            def=(sDefAssay*)dict->find(FXStringFormat("%d",i).text());
            if(!def)
                continue;
            if(!cAssayManager::assayExists(def->id))
            {
                if(first)
                {
                    if(!prSilent)
                    {
                        if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_upgdb").text()))
                            break;
                    }
                    first=false;
                }
                cAssayManager::addAssay(def->id,def->solid,def->title,def->filterA,def->filterB,def->factors_defs,def->calculations_defs,def->titers_defs,def->bins_defs,def->comments,def->rules_defs);
            }
        }
    }

    if(dbSpecies=="ovine")
    {
        sDefAssay *def;

        def=new sDefAssay;
        def->id="TOX-O.pst";
        def->title="Toxoplasma Gondii";
        def->solid="1";
        def->filterA="410";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nCorrected S/P\t0.25\t>=\t0.15\n";
        def->titers_defs="0\t0\t0\tRaw S/P\n0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.15\t0.25\t0.4\t0.6\t0.8\t1.0\t1.1\t1.3\t1.5\t1.7\t1.9\t2.1\t2.3\t2.5\t2.7\t2.9\t3.1\t3.3\t3.5\t3.7\t3.9\t4.1\t4.3\t4.5\t4.7\t4.9\t5.1\t5.3\t5.5\t5.7\t";
        def->comments="";
        def->rules_defs="Na\t<=\t0.2\nPa\t>=\t0.8\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        def=new sDefAssay;
        def->id="PTRM.pst";
        def->title="Ruminant Paratuberculosis (LSI Kit)";
        def->solid="1";
        def->filterA="450";
        def->filterB="0";
        def->factors_defs="100\t200\t300\t400\t";
        def->calculations_defs="1\nRaw S/P\t0.7\t>=\t0.6\n";
        def->titers_defs="0\t0\t0\tRaw S/P\n0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->bins_defs="0.6\t0.7\t2.0\t3.0\t4.0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t0\t";
        def->comments="";
        def->rules_defs="Na\t<\t0.25\n";
        dict->insert(FXStringFormat("%d",dict->no()).text(),def);

        bool first=true;
        for(int i=0;i<dict->no();i++)
        {
            def=(sDefAssay*)dict->find(FXStringFormat("%d",i).text());
            if(!def)
                continue;
            if(!cAssayManager::assayExists(def->id))
            {
                if(first)
                {
                    if(!prSilent)
                    {
                        if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_upgdb").text()))
                            break;
                    }
                    first=false;
                }
                cAssayManager::addAssay(def->id,def->solid,def->title,def->filterA,def->filterB,def->factors_defs,def->calculations_defs,def->titers_defs,def->bins_defs,def->comments,def->rules_defs);
            }
        }
    }

    delete dict;
}

FXbool cDataLink::ensureUpgrade(void)
{
    if(getProperty("database_version")=="0.1")
    {
        setProperty("database_version","1.0");
    }
    return true;
}

FXbool cDataLink::open(const FXString &prDBFile,FXbool prCreate,FXObject *prSender,FXbool prNoInv)
{
    if(prDBFile.empty())
        return true;
    if(opened)
    {
        if(dbFile==prDBFile)
            return true;
        if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_changedb").text()))
            return false;
        oWinMain->closeAllWindows(prSender);
        if(!close())
            return false;
    }

    FXbool dbexists=true;
    sqlite3 *olink=link;

    if(!FXStat::exists(prDBFile))
    {
        if(!prCreate)
        {
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_opendberror").text());
            return false;
        }
        dbexists=false;
    }

    if(dbexists && (FXStat::isFile(prDBFile)) && (!FXStat::isWritable(prDBFile)))
    {
        if(prSender || (!prNoInv))
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_dbro").text());
        link=olink;
        return false;
    }

    if(sqlite3_open(prDBFile.text(),&link)!=SQLITE_OK)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),"%s (%s)\n\n%s",sqlite3_errmsg(link),prDBFile.text(),
                              dbexists?oLanguage->getText("str_opendberror").text():oLanguage->getText("str_newdberror").text());
        sqlite3_close(link);
        link=olink;
        return false;
    }

    dbSpecies=prDBFile.rafter('.');
    dbSpecies=dbSpecies.mid(2,dbSpecies.length()-2).lower();

    opened=true;
    if(dbexists && dbSpecies!=this->getProperty("database_species").lower())
    {
        if(!prNoInv)
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),"%s\n\n%s",prDBFile.text(),
                                  oLanguage->getText("str_invdberror").text());

        sqlite3_close(link);
        link=olink;
        opened=false;
        return false;
    }

    if(olink)
        sqlite3_close(olink);

    dbFile=prDBFile;
    if(!dbexists)
        executeSqlBase();
    dbName=this->getProperty("database_name");
    cMDICaseManager::cmdRefresh();
    cMDICaseManager::cmdResearch();
    return true;
}

FXbool cDataLink::openDesignated(void)
{
    FXString ret=oApplicationManager->reg().readStringEntry("settings","recent_db","");
    if(ret.empty())
        return true;
    return openDesignated(DATABASES_PATH+FXPath::name(ret),false);
}

FXbool cDataLink::openDesignated(const FXString &prDBFile,FXbool prCreate,FXObject *prSender)
{
    FXbool ret=open(prDBFile,prCreate,prSender);
    if(!ret)
        return false;

    FXString rules_defs=getProperty("database_version");
    FXuint version=0,v1=0,v2=0,v3=0;

    v1=FXIntVal(rules_defs.before('.'));
    rules_defs=rules_defs.after('.');
    v2=FXIntVal(rules_defs.before('.'));
    rules_defs=rules_defs.after('.');
    v3=FXIntVal(rules_defs);
    version=FXIntVal(FXStringFormat("1%03d%03d%03d",v1,v2,v3));
    if(version>DBS_VERSION_NO)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_dbnew_version").text());
        close();
        saveDesignated("");
        return false;
    }

    if(version<DBS_VERSION_NO)
    {
        if(MBOX_CLICKED_NO==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_upgrade_db").text()) || !ensureUpgrade())
        {
            //FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_dbnew_version").text());
            close();
            saveDesignated("");
            return false;
        }
    }

    if(ret==true)
    {
        saveDesignated();
        vacuumDatabase();
    }
    else
        if(!opened)
            saveDesignated("");
    if(dbName=="Demo Database")
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_demodatabase").text());
    ensureKitUpgrade();
    return ret;
}

FXbool cDataLink::saveDesignated(void)
{
    if(opened)
        return saveDesignated(dbFile);
    return false;
}

FXbool cDataLink::saveDesignated(const FXString &prDBFile)
{
    oApplicationManager->reg().writeStringEntry("settings","recent_db",FXPath::name(prDBFile).text());
    oApplicationManager->reg().write();
    return true;
}

FXbool cDataLink::close(void)
{
    if(!opened)
        return false;
    if(SQLITE_OK!=sqlite3_close(link))
        return false;
    opened=false;
    link=NULL;
    dbName="";
    return true;
}

FXbool cDataLink::closeDesignated(void)
{
    if(!opened)
        return false;
    if(SQLITE_OK!=sqlite3_close(link))
        return false;
    opened=false;
    dbName="";
    link=NULL;
    saveDesignated("");
    return true;
}

FXbool cDataLink::isOpened(void)
{
    return opened;
}

FXString cDataLink::getFilename(void)
{
    return dbFile;
}

FXString cDataLink::getName(void)
{
    return dbName;
}

FXString cDataLink::getSpecies(void)
{
    return dbSpecies;
}

FXString cDataLink::getProperty(FXString prProperty)
{
    if(!opened)
        return "";
    cDataResult *res=execute("SELECT value FROM t_properties WHERE property='"+prProperty+"';");
    if(res->getRowCount()==0)
        return "";
    return res->getCellString(0,0);
}

FXbool cDataLink::setProperty(FXString prProperty,FXString prValue)
{
    if(!opened)
        return false;
    cDataResult *res=execute("SELECT value FROM t_properties WHERE property='"+prProperty.trim().substitute("'","")+"';");
    if(res->getRowCount()==0)
        res=execute("INSERT INTO t_properties VALUES('"+prProperty.trim().substitute("'","")+"','"+prValue.trim().substitute("'","")+"');");
    else
        res=execute("UPDATE t_properties SET value='"+prValue.trim().substitute("'","")+"' WHERE property='"+prProperty.trim().substitute("'","")+"';");
    if(!res || getAffectedRowCount()!=1)
        return false;
    return true;
}

FXString cDataLink::getInfo(void)
{
    if(!opened)
        return "";
    return sqlite3_libversion();
}

FXString cDataLink::getLastError(void)
{
    if(!opened)
        return "";
    return sqlite3_errmsg(link);
}

int cDataLink::getLastErrorCode(void)
{
    if(!opened)
        return SQLITE_OK;
    return sqlite3_errcode(link);
}

int cDataLink::getLastId(void)
{
    if(!opened)
        return -1;
    return sqlite3_last_insert_rowid(link);
}

int cDataLink::getAffectedRowCount(void)
{
    return sqlite3_changes(link);
}

FXbool cDataLink::tableExists(const FXString &prTable)
{
    if(!opened)
        return false;
    cDataResult *res=execute("SELECT oid FROM sqlite_master WHERE type='table' AND name='"+prTable+"';");
    if(res->getRowCount()>0)
        return true;
    return false;
}

cDataResult *cDataLink::execute(const FXString &prQuery,FXbool prAsk)
{
    if(!opened)
        return NULL;
    char **res,*errmsg;
    int nrows,ncols;

    //fxmessage("\nSQL: %s\n\n",prQuery.text());

    while(sqlite3_get_table(link,prQuery.text(),&res,&nrows,&ncols,&errmsg)!=SQLITE_OK)
        if(!prAsk || MBOX_CLICKED_NO==FXMessageBox::error(oWinMain,MBOX_YES_NO,oLanguage->getText("str_error").text(),"[%s]\n\n%s",
                                                             errmsg,oLanguage->getText("str_sqlerror").text()))
            return NULL;
    cDataResult *ret=new cDataResult(res,nrows,ncols);
    return ret;
}
