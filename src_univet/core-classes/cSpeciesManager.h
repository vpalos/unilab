#ifndef CSPECIES_H
#define CSPECIES_H

#include <fx.h>

typedef struct sSpeciesObject
{
    FXString *name;
    unsigned char *image_data;
    int age_format;
};

class cSpeciesManager
{
    private:
    protected:
    public:
        static int getSpeciesCount(void);
        static sSpeciesObject *getSpeciesList(void);
        
        static FXString getSpeciesSqlBase(const FXString &prSpecies);
        static int getAgeFormat(const FXString &prSpecies);
};

#endif
