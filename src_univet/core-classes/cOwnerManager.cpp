#include "engine.h"
#include "cDataLink.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cMDIOwner.h"
#include "cOwnerManager.h"

FXbool cOwnerManager::ownerSolid(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_owners WHERE id='"+ress.trim().substitute("'","")+"' AND solid=1;");
    return (res->getRowCount()>0) || (prId=="---");
}

FXbool cOwnerManager::ownerExists(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_owners WHERE id='"+ress.trim().substitute("'","")+"';");
    return (res->getRowCount()>0) || (prId=="---");
}

FXbool cOwnerManager::ownerNeeded(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_gnpopulations WHERE owner_oid='"+ress.trim().substitute("'","")+"';");
    return res->getRowCount()>0;
}

FXString cOwnerManager::getNewID(void)
{
    static int ownNr=oApplicationManager->reg().readIntEntry("oids","owners",1);
    FXString res=oLanguage->getText("str_ownmdi_owntitle");
    FXString ret=res+FXStringFormat(" #%d",++ownNr);
    while(ownerExists(ret))
        ret=res+FXStringFormat(" #%d",++ownNr);
    oApplicationManager->reg().writeIntEntry("oids","owners",ownNr);
    return ret;
}

int cOwnerManager::getOwnerCount(void)
{
    if(!oDataLink->isOpened())
        return 0;
    cDataResult *res=oDataLink->execute("SELECT COUNT(id) FROM t_owners;");
    return res->getCellInt(0,0);
}

sOwnerObject *cOwnerManager::listOwners()
{
    if(!oDataLink->isOpened())
        return NULL;

    sOwnerObject *ret=(sOwnerObject*)malloc(getOwnerCount()*sizeof(sOwnerObject));
    if(!ret)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }
    cDataResult *res=oDataLink->execute(("SELECT id,ctperson,solid FROM t_owners;"));
    for(int i=0;i<res->getRowCount();i++)
    {
        ret[i].id=new FXString(res->getCellString(i,0));
        ret[i].title=new FXString(res->getCellString(i,1));
        ret[i].solid=res->getCellInt(i,2);
    }
    return ret;
}

cDataResult *cOwnerManager::getOwner(const FXString &prId)
{
    FXString res=prId;
    return oDataLink->execute("SELECT * FROM t_owners WHERE id='"+res.trim().substitute("'","")+"';");
}

FXString cOwnerManager::newOwner(FXMDIClient *prP, FXPopup *prPup)
{
    FXString ret=getNewID();
    cMDIOwner *ownWin=new cMDIOwner(prP,ret,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,300,570);
    ownWin->create();
    ownWin->setFocus();
    return ret;
}

FXString cOwnerManager::editOwner(FXMDIClient *prP, FXPopup *prPup,const FXString &prId)
{
    cMDIOwner *ownWin=new cMDIOwner(prP,prId,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,300,570);
    ownWin->create();
    ownWin->loadOwner(prId);
    ownWin->setFocus();
    return prId;
}

FXString cOwnerManager::duplicateOwner(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return "";
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT * FROM t_owners WHERE id='"+ress.trim().substitute("'","")+"';");
    if(res->getRowCount()<=0)
        return "";
    int i=1;
    FXString ret=res->getCellString(0,0);
    while(ownerExists(ret))
        ret=res->getCellString(0,0)+FXStringFormat(" #%d",i++);
    FXString res2="INSERT INTO t_owners VALUES('"+ret.trim().substitute("'","")+"'";
    for(i=1;i<res->getFieldCount();i++)
        res2=res2+",'"+res->getCellString(0,i).trim().substitute("'","")+"'";
    res2=res2+");";
    res=oDataLink->execute(res2);
    return ret;
}

FXbool cOwnerManager::addOwner(FXString prId,FXString prCtp,FXString prAdd,FXString prCity,FXString prState,FXString prZip,FXString prPhone,FXString prFax,FXString prEmail,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("INSERT INTO t_owners VALUES('"+
                                                    prId.trim().substitute("'","")+"','"+
                                                    prCtp.trim().substitute("'","")+"','"+
                                                    "0','"+
                                                    prAdd.trim().substitute("'","")+"','"+
                                                    prCity.trim().substitute("'","")+"','"+
                                                    prState.trim().substitute("'","")+"','"+
                                                    prZip.trim().substitute("'","")+"','"+
                                                    prPhone.trim().substitute("'","")+"','"+
                                                    prFax.trim().substitute("'","")+"','"+
                                                    prEmail.trim().substitute("'","")+"','"+
                                                    prComments.trim().substitute("'","")+"');");
    return (oDataLink->getAffectedRowCount()==1);
}

FXbool cOwnerManager::setOwner(FXString prOldId,FXString prId,FXString prCtp,FXString prAdd,FXString prCity,FXString prState,FXString prZip,FXString prPhone,FXString prFax,FXString prEmail,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("UPDATE t_owners SET id='"+prId.trim().substitute("'","")+
                       "', ctperson='"+prCtp.trim().substitute("'","")+
                       "', solid='0"+
                       "', address='"+prAdd.trim().substitute("'","")+
                       "', city='"+prCity.trim().substitute("'","")+
                       "', state='"+prState.trim().substitute("'","")+
                       "', zip='"+prZip.trim().substitute("'","")+
                       "', phone='"+prPhone.trim().substitute("'","")+
                       "', fax='"+prFax.trim().substitute("'","")+
                       "', email='"+prEmail.trim().substitute("'","")+
                       "', comments='"+prComments.trim().substitute("'","")+
                       "' WHERE id='"+prOldId.trim().substitute("'","")+"' AND solid='0';");
    int ret=oDataLink->getAffectedRowCount();
    oDataLink->execute("UPDATE t_gnpopulations SET owner_oid='"+prId.trim().substitute("'","")+"' WHERE owner_oid='"+prOldId+"';");
    return (ret==1);
}

FXbool cOwnerManager::removeOwner(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT oid FROM t_owners WHERE id='"+ress.trim().substitute("'","")+"' AND solid='0';");
    if(!res || res->getRowCount()==0)
        return false;
    res=oDataLink->execute("DELETE FROM t_owners WHERE id='"+ress.trim().substitute("'","")+"' AND solid='0';");
    return (res!=NULL && oDataLink->getAffectedRowCount()>0);
}


