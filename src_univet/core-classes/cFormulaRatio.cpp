#include <fx.h>
#include <strings.h>
#include <math.h>
#include "engine.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cFormulaRatio.h"

#define FORMULA_COUNT 9

int cFormulaRatio::getCount(void)
{
    return FORMULA_COUNT;
}

sFormulaRatioObject *cFormulaRatio::listFormulaRatio(void)
{
    sFormulaRatioObject *ret=(sFormulaRatioObject *)malloc(FORMULA_COUNT*sizeof(sFormulaRatioObject));
    if(!ret)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }

    int i=0;
    ret[i].id=new FXString("Raw OD");
    ret[i].title=new FXString(oLanguage->getText("str_fratio_rawod_title"));
    ret[i].flags=FORMULA_ALL;

    i++;
    ret[i].id=new FXString("Corrected S/P");
    ret[i].title=new FXString(oLanguage->getText("str_fratio_corsp_title"));
    ret[i].flags=FORMULA_ALL|FORMULA_DEFAULT;

    i++;
    ret[i].id=new FXString("Corrected S");
    ret[i].title=new FXString(oLanguage->getText("str_fratio_corod_title"));
    ret[i].flags=FORMULA_ALL;

    i++;
    ret[i].id=new FXString("Raw S/P");
    ret[i].title=new FXString(oLanguage->getText("str_fratio_uncsp_title"));
    ret[i].flags=FORMULA_ALL;

    i++;
    ret[i].id=new FXString("2xELISA S/P");
    ret[i].title=new FXString(oLanguage->getText("str_fratio_2xelisa_title"));
    ret[i].flags=FORMULA_ALL;

    i++;
    ret[i].id=new FXString("Raw S/NHC");
    ret[i].title=new FXString(oLanguage->getText("str_fratio_snhc_title"));
    ret[i].flags=FORMULA_ALL;

    i++;
    ret[i].id=new FXString("Raw S/N");
    ret[i].title=new FXString(oLanguage->getText("str_fratio_uncsn_title"));
    ret[i].flags=FORMULA_ALL;

    i++;
    ret[i].id=new FXString("Block Neg");
    ret[i].title=new FXString(oLanguage->getText("str_fratio_blkn_title"));
    ret[i].flags=FORMULA_PRICAL|FORMULA_SECCAL;

    i++;
    ret[i].id=new FXString("Block Pos");
    ret[i].title=new FXString(oLanguage->getText("str_fratio_blkp_title"));
    ret[i].flags=FORMULA_PRICAL|FORMULA_SECCAL;

    return ret;
}

sControlAms cFormulaRatio::calculateControlAms(FXdouble *prControlsData,FXint *prControlsLayout,FXint prControlsCount,FXbool prAlelisa)
{
    sControlAms ret={0,0};

    int __alelisa_multiplier=prAlelisa?2:1;
    double posama=0,pta=0,posam=0,negam=0,pt=0,nt=0;
    for(int i=0;i<prControlsCount;i++)
        if(prControlsLayout[i]>=0)
            {
                posam+=prControlsData[i*__alelisa_multiplier];pt++;
                if(prAlelisa)
                    {posama+=prControlsData[i*__alelisa_multiplier+1];pta++;}
            }
        else
            {negam+=prControlsData[i*__alelisa_multiplier];nt++;}
    posam/=pt;
    negam/=nt;
    posama/=pta;
    ret.posam=posam;
    ret.negam=negam;
    return ret;
}

FXdouble cFormulaRatio::calculateRatio(FXString *prFormula,FXdouble prValue,FXdouble prNhc,FXdouble *prControlsData,FXint *prControlsLayout,FXint prControlsCount,FXint prFactor,FXint prFactors[4],FXbool prAlelisa)
{
    FXdouble ret=0;

    if(!prFormula || prFormula->empty() || prValue==-9999)
        return -9999;

    if(*prFormula=="Raw OD")
        return prValue<0?0:prValue;

    int __alelisa_multiplier=prAlelisa?2:1;
    double posama=0,pta=0,posam=0,negam=0,pt=0,nt=0;
    for(int i=0;i<prControlsCount;i++)
        if(prControlsLayout[i]>=0)
            {
                posam+=prControlsData[i*__alelisa_multiplier];pt++;
                if(prAlelisa)
                    {posama+=prControlsData[i*__alelisa_multiplier+1];pta++;}
            }
        else
            {negam+=prControlsData[i*__alelisa_multiplier];nt++;}
    posam/=pt;
    negam/=nt;
    posama/=pta;

    if(*prFormula=="Corrected S/P")
    {
        if(posam-negam==0)
            return -9999;
        ret=(prValue-negam)/(posam-negam);
        //ret*=((double)prFactors[(prFactor>4?4:prFactor<1?1:prFactor)-1])/100;
        return ret<0?0:ret;
    }
    if(*prFormula=="Corrected S")
    {
        ret=prValue-negam;
        //ret*=((double)prFactors[(prFactor>4?4:prFactor<1?1:prFactor)-1])/100;
        return ret<0?0:ret;
    }
    if(*prFormula=="Raw S/P")
    {
        if(posam==0)
            return -9999;
        ret=prValue/posam;
        //ret*=((double)prFactors[(prFactor>4?4:prFactor<1?1:prFactor)-1])/100;
        return ret<0?0:ret;
    }
    if(*prFormula=="Raw S/N")
    {
        if(negam==0)
            return -9999;
        ret=prValue/negam;
        //ret*=((double)prFactors[(prFactor>4?4:prFactor<1?1:prFactor)-1])/100;
        return ret<0?0:ret;
    }
    if(*prFormula=="Block Neg")
    {
        if(negam==0)
            return -9999;
        ret=100*(negam-prValue)/negam;
        //ret*=((double)prFactors[(prFactor>4?4:prFactor<1?1:prFactor)-1])/100;
        return ret<0?0:ret;
    }
    if(*prFormula=="Block Pos")
    {
        if(posam==0)
            return -9999;
        ret=100*(posam-prValue)/posam;
        //ret*=((double)prFactors[(prFactor>4?4:prFactor<1?1:prFactor)-1])/100;
        return ret<0?0:ret;
    }

    prNhc*=((double)prFactors[(prFactor>4?4:prFactor<1?1:prFactor)-1])/100;
    if(*prFormula=="Raw S/NHC")
    {
        if(prNhc==0)
            return -9999;
        ret=prValue/prNhc;
        //ret*=((double)prFactors[(prFactor>4?4:prFactor<1?1:prFactor)-1])/100;
        return ret<0?0:ret;
    }

    if(*prFormula=="2xELISA S/P")
    {
        if(posam-posama==0)
            return -9999;
        ret=(prValue-prNhc)/(posam-posama);
        //ret*=((double)prFactors[(prFactor>4?4:prFactor<1?1:prFactor)-1])/100;
        return ret<0?0:ret;
    }
    return ret<0?0:ret;
}

FXdouble cFormulaRatio::calculateEU(FXdouble prValue,FXdouble *prControlsData,FXint *prControlsLayout,FXint prControlsCount,FXint prFactor,FXint prFactors[4],FXbool prAlelisa)
{
    if(prValue==-9999)
        return -9999;
    FXdouble ret=calculateRatio(new FXString("Corrected S/P"),prValue,0,prControlsData,prControlsLayout,prControlsCount,prFactor,prFactors,prAlelisa);
    return ret==-9999?-9999:ret<0?0:round(ret*100);
}

FXdouble cFormulaRatio::calculateRatioGroup(FXdouble prValue,FXdouble prRatioGroups[30])
{
    if(prValue==-9999)
        return -9999;
    int i;
    if(prValue<=0)
        return 0;
    for(i=0;i<30;i++)
    {
        if(prRatioGroups[i]==0)
            break;
        if(prValue<prRatioGroups[i])
            return i;
    }
    return i;
}

FXdouble cFormulaRatio::calculateTiter(FXdouble prValue,FXdouble prSlope,FXdouble prIntercept,FXint prFactor,FXint prFactors[4])
{
    if(prValue==-9999)
        return -9999;
    if(prValue<=0)
        return 0;
    FXdouble ret=round(pow(10,prSlope*log10(prValue)+prIntercept));
    ret=ret==-9999?-9999:ret<0?0:ret;
    ret*=((double)prFactors[(prFactor>4?4:prFactor<1?1:prFactor)-1])/100;
    return ret;
}

FXdouble cFormulaRatio::calculateTiterLog2(FXdouble prValue)
{
    if(prValue==-9999)
        return -9999;
    if(prValue<=0)
        return 0;
    return log2(prValue);
}

FXdouble cFormulaRatio::calculateTiterGroup(FXdouble prValue,FXint prTiterGroups[30])
{
    if(prValue==-9999)
        return -9999;
    int i;
    if(prValue<=0)
        return 0;
    for(i=0;i<30;i++)
    {
        if(prTiterGroups[i]==0)
            break;
        if(prValue<prTiterGroups[i])
            return i;
    }
    return i;
}

FXbool cFormulaRatio::testCompatible(cDataResult *prAssayRes,cDataResult *prTemplateRes)
{
    FXString calculations_defs,rules_defs;
    calculations_defs=prAssayRes->getCellString(0,6);
    rules_defs=prAssayRes->getCellString(0,10);
    FXbool compatible=true,ast=false,tpt=false;
    if(calculations_defs.find("2xELISA S/P")!=-1 ||
       calculations_defs.find("Raw S/NHC")!=-1)
        ast=true;
    else if(rules_defs.find('h')!=-1)
        ast=true;
    if(prTemplateRes->getCellInt(0,3)==2)
        tpt=true;

    if(ast && !tpt)
        compatible=false;

    if(!compatible)
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_reading_incompat").text());

    return compatible;
}

FXbool cFormulaRatio::testChangeCompatible(FXString prCal_defs,FXString prOldCal_defs,FXString prRul_defs,FXString prOldRul_defs)
{
    int ast=0,as2t=0;
    if(prCal_defs.find("2xELISA S/P")!=-1 ||
       prCal_defs.find("Raw S/NHC")!=-1)
        ast=1;
    else if(prRul_defs.find('h')!=-1)
        ast=1;
    if(prOldCal_defs.find("2xELISA S/P")!=-1 ||
       prOldCal_defs.find("Raw S/NHC")!=-1)
        as2t=1;
    else if(prOldRul_defs.find('h')!=-1)
        as2t=1;
    if(ast!=as2t)
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nochassay").text());
    return (ast==as2t);
}

