#include <strings.h>
#include "cSpeciesManager.h"
#include "cBaselineManager.h"
#include "cMDIBaseline.h"
#include "engine.h"
#include "graphics.h"
#include "cWinMain.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cColorsManager.h"
#include "cDataResult.h"
#include "cAssayManager.h"
#include "cTemplateManager.h"
#include "cPopulationManager.h"
#include "cFormulaRatio.h"
#include "cCaseCompareReport.h"

cCaseCompareReport *oCaseCompareReport;

FXbool cCaseCompareReport::loadReadings(cSortList &prCases)
{
    FXDialogBox msg(oWinMain,oLanguage->getText("str_report_wtitle"),DECOR_TITLE|DECOR_BORDER);
    new FXLabel(&msg,oLanguage->getText("str_report_ltext"),NULL,LAYOUT_FILL|JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
    msg.create();
    msg.show(PLACEMENT_OWNER);
    msg.setFocus();
    msg.raise();
    oApplicationManager->beginWaitCursor();
    while(oApplicationManager->peekEvent())
        oApplicationManager->runOneEvent(false);

    FXString criteria="";
    int j=0;
    for(int i=0;i<prCases.getNumItems();i++)
        if(prCases.isItemSelected(i))
        {
            if(j>0)
                criteria=criteria+" OR ";
            criteria=criteria+"(assay_oid='"+prCases.getItemText(i).before('\t')+"' AND "+"id='"+prCases.getItemText(i).after('\t').before('\t')+"')";
            j++;
        }
    if(!j)
        return false;
    cDataResult *resr=oDataLink->execute("SELECT DISTINCT reading_oid FROM t_gncases WHERE "+criteria+" ORDER BY assay_oid ASC;");
    tSetsCount=resr->getRowCount();
    if(!tSetsCount)
        return false;
    cDataResult *resc,*resp;
    casesSets=(sCasesSet*)malloc(tSetsCount*sizeof(sCasesSet));
    if(!casesSets)
    {
        tSetsCount=0;
        tCasesCount=0;
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return false;
    }

    tCasesCount=0;
    areBins=false;
    areTiters=false;
    areSecondary=false;
    areOnlyTs=false;
    areSamples=false;
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        FXString __reading_oid(resr->getCellString(setsi,0));
        resc=oDataLink->execute("SELECT * FROM t_gncases WHERE ("+criteria+") AND reading_oid='"+__reading_oid+"' ORDER BY (startPlate*100+startCell) ASC;");
        casesSets[setsi].casesCount=resc->getRowCount();
        tCasesCount+=casesSets[setsi].casesCount;
        casesSets[setsi].cases=(sCaseEntry*)malloc(casesSets[setsi].casesCount*sizeof(sCaseEntry));
        if(!casesSets[setsi].cases)
        {
            tSetsCount=0;
            tCasesCount=0;
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
            return false;
        }

        FXString __assay(resc->getCellString(0,6));
        FXString __template(resc->getCellString(0,16));
        casesSets[setsi].assayRes=oDataLink->execute("SELECT * FROM t_assays WHERE id='"+__assay+"';");
        casesSets[setsi].templateRes=oDataLink->execute("SELECT * FROM t_templates WHERE id='"+__template+"';");

        casesSets[setsi].orientation=resc->getCellInt(0,17);
        {
            int k=resc->getCellInt(0,18);
            casesSets[setsi].alelisa=k==2?true:false;
        }
        int __alelisa=casesSets[setsi].alelisa;
        int __alelisa_multiplier=__alelisa?2:1;

        FXString __lot(resc->getCellString(0,7));
        FXString __expDate(resc->getCellString(0,8));

        FXString calculations_defs=casesSets[setsi].assayRes->getCellString(0,6);
        casesSets[setsi].useSecond=FXIntVal(calculations_defs.before('\n'))==2?true:false;
        if(casesSets[setsi].useSecond)
            areSecondary=true;
        calculations_defs=calculations_defs.after('\n');

        casesSets[setsi].cala=new FXString(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        casesSets[setsi].calapco=new FXString(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        casesSets[setsi].calaop=new FXString(calculations_defs.before('\t'));
        calculations_defs=calculations_defs.after('\t');
        casesSets[setsi].calasco=new FXString(calculations_defs.before('\n'));
        calculations_defs=calculations_defs.after('\n');

        if(casesSets[0].useSecond)
        {
            casesSets[setsi].calb=new FXString(calculations_defs.before('\t'));
            calculations_defs=calculations_defs.after('\t');
            casesSets[setsi].calbpco=new FXString(calculations_defs.before('\t'));
            calculations_defs=calculations_defs.after('\t');
            casesSets[setsi].calbop=new FXString(calculations_defs.before('\t'));
            calculations_defs=calculations_defs.after('\t');
            casesSets[setsi].calbsco=new FXString(calculations_defs.before('\n'));
            calculations_defs=calculations_defs.after('\n');
        }
        else
        {
            casesSets[setsi].calb=NULL;
            casesSets[setsi].calbop=NULL;
            casesSets[setsi].calbpco=new FXString("0");
            casesSets[setsi].calbsco=new FXString("0");
        }

        if(*casesSets[setsi].calasco=="0")
            *casesSets[setsi].calasco=*casesSets[setsi].calapco;
        if(*casesSets[setsi].calbsco=="0")
            *casesSets[setsi].calbsco=*casesSets[setsi].calbpco;

        FXString factors_defs=casesSets[setsi].assayRes->getCellString(0,5);
        for(int i=0;i<4;i++)
        {
            casesSets[setsi].factors[i]=FXIntVal(factors_defs.before('\t'));
            factors_defs=factors_defs.after('\t');
        }

        FXString titers_defs=casesSets[setsi].assayRes->getCellString(0,7);
        casesSets[setsi].useTiters=FXIntVal(titers_defs.before('\t'))!=0?true:false;
        if(casesSets[setsi].useTiters)
            areTiters=true;
        titers_defs=titers_defs.after('\t');
        casesSets[setsi].slope=FXFloatVal(titers_defs.before('\t'));
        titers_defs=titers_defs.after('\t');
        casesSets[setsi].intercept=FXFloatVal(titers_defs.before('\t'));
        titers_defs=titers_defs.after('\t');
        casesSets[setsi].calt=new FXString(titers_defs.before('\n'));
        titers_defs=titers_defs.after('\n');
        bool idz=false;
        for(int i=0;i<30;i++)
        {
            casesSets[setsi].titerGroups[i]=FXIntVal(titers_defs.before('\t'));
            titers_defs=titers_defs.after('\t');
            if(casesSets[setsi].titerGroups[i]>0)
                idz=true;
        }
        if(casesSets[setsi].useTiters && !idz)
            areOnlyTs=true;

        FXString bins_defs=casesSets[setsi].assayRes->getCellString(0,8);
        bool ib=false;
        for(int i=0;i<30;i++)
        {
            casesSets[setsi].ratioGroups[i]=FXFloatVal(bins_defs.before('\t'));
            bins_defs=bins_defs.after('\t');
            if(casesSets[setsi].ratioGroups[i]>0)
            {
                ib=true;
                areBins=true;
            }
        }
        if(!casesSets[setsi].useTiters && !ib)
            areSamples=true;

        FXString __controls_defs(resc->getCellString(0,22));
        __controls_defs=__controls_defs.after('\n');
        casesSets[setsi].controlsPerPlate=0;
        int value;
        do{
            value=FXIntVal(__controls_defs.before('\t'));
            __controls_defs=__controls_defs.after('\t');
            if(value!=0 && value!=-100)
                casesSets[setsi].controlsLayout[casesSets[setsi].controlsPerPlate++]=value;
            else
                break;
        }while(true);
        cMDIReadings::sortControls(casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate);

        casesSets[setsi].platesCount=0;
        FXDict *pl=new FXDict();
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++)
        {
            casesSets[setsi].cases[casei].startCell=resc->getCellInt(casei,20);
            casesSets[setsi].cases[casei].startPlate=resc->getCellInt(casei,19);
            casesSets[setsi].cases[casei].plateSpanCount=resc->getCellInt(casei,21);

            int ps=casesSets[setsi].cases[casei].startPlate,pe=ps+casesSets[setsi].cases[casei].plateSpanCount-1;
            FXString key;
            for(int i=ps;i<=pe;i++)
                pl->insert(FXStringFormat("%c%c",i/256+1,i%256+1).text(),NULL);

            casesSets[setsi].cases[casei].id=new FXString(resc->getCellString(casei,0));
            casesSets[setsi].cases[casei].reading_oid=new FXString(__reading_oid);
            casesSets[setsi].cases[casei].count=resc->getCellInt(casei,15);
            casesSets[setsi].cases[casei].replicates=resc->getCellInt(casei,11);
            casesSets[setsi].cases[casei].factor=resc->getCellInt(casei,12);
            casesSets[setsi].cases[casei].alelisa=__alelisa;

            casesSets[setsi].sampleCount+=casesSets[0].cases[casei].count*__alelisa_multiplier;

            casesSets[setsi].cases[casei].age=new FXString(resc->getCellString(casei,14));
            casesSets[setsi].cases[casei].tpl=new FXString(__template);
            casesSets[setsi].cases[casei].veterinarian=new FXString(resc->getCellString(casei,5));
            casesSets[setsi].cases[casei].controls_defs=new FXString(resc->getCellString(casei,22));
            casesSets[setsi].cases[casei].readingDate=new FXString(resc->getCellString(casei,2));
            casesSets[setsi].cases[casei].technician=new FXString(resc->getCellString(casei,3));
            casesSets[setsi].cases[casei].reason=new FXString(resc->getCellString(casei,4));
            casesSets[setsi].cases[casei].lot=new FXString(__lot);
            casesSets[setsi].cases[casei].expirationDate=new FXString(__expDate);
            casesSets[setsi].cases[casei].spType=new FXString(resc->getCellString(casei,9));
            casesSets[setsi].cases[casei].bleedDate=new FXString(resc->getCellString(casei,10));
            casesSets[setsi].cases[casei].comments=new FXString(resc->getCellString(casei,24));
            casesSets[setsi].cases[casei].population=new FXString(resc->getCellString(casei,13));
            casesSets[setsi].cases[casei].assay=new FXString(__assay);

            resp=oDataLink->execute("SELECT * FROM t_gnpopulations WHERE id='"+*casesSets[setsi].cases[casei].population+"';");
            casesSets[setsi].cases[casei].breed1=new FXString(resp->getCellString(casei,3));
            casesSets[setsi].cases[casei].breed2=new FXString(resp->getCellString(casei,4));
            resp->free();

            casesSets[setsi].cases[casei].parentSet=setsi;

            FXString __data(resc->getCellString(casei,23));
            FXString __controls=__data.before('\n');
            __data=__data.after('\n');

            casesSets[setsi].cases[casei].controlsCount=casesSets[setsi].cases[casei].plateSpanCount*__alelisa_multiplier*casesSets[setsi].controlsPerPlate;
            casesSets[setsi].cases[casei].controlsData=(double*)malloc(casesSets[setsi].cases[casei].controlsCount*sizeof(double));
            if(!casesSets[setsi].cases[casei].controlsData)
            {
                tSetsCount=0;
                tCasesCount=0;
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                return false;
            }
            for(int i=0;i<casesSets[setsi].cases[casei].controlsCount;i++)
            {
                casesSets[setsi].cases[casei].controlsData[i]=FXFloatVal(__controls.before(' '));
                __controls=__controls.after(' ');
            }

            int ct=casesSets[setsi].cases[casei].count*__alelisa_multiplier*(casesSets[setsi].cases[casei].replicates+1);
            casesSets[setsi].cases[casei].data=(double*)malloc(ct*sizeof(double));
            if(!casesSets[setsi].cases[casei].data)
            {
                tSetsCount=0;
                tCasesCount=0;
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                return false;
            }
            for(int i=0;i<ct;i++)
            {
                casesSets[setsi].cases[casei].data[i]=FXFloatVal(__data.before(' '));
                __data=__data.after(' ');
            }
        }
        casesSets[setsi].platesCount=pl->no();
        delete pl;
        resc->free();
    }
    resr->free();

    bool incompatible=false,start=casesSets[0].alelisa;
    for(int setsi=0;setsi<tSetsCount;setsi++)
        if(start!=casesSets[setsi].alelisa)
        {
            incompatible=true;
            break;
        }

    mcSingle=false;
    mcCala=true;
    mcCalb=false;
    mcEU=false;
    mcResult=true;
    mcResult2=false;
    mcAge=false;
    mcBreed1=false;
    mcBreed2=false;
    mcLog2=false;
    //mcBins2=false;
    if(areTiters)
    {
        mcTiter=true;
        mcTiters=true;
        mcBins=false;
    }
    else
    {
        mcTiter=false;
        mcTiters=false;
        mcBins=true;
    }

    oApplicationManager->endWaitCursor();
    msg.hide();

    if(incompatible)
    {
        tSetsCount=0;
        tCasesCount=0;
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_reading_incompatcse").text());
        return false;
    }

    return true;
}

FXbool cCaseCompareReport::processReadings(void)
{
    if(!tCasesCount)
        return false;
    int nc=2;
    if(mcCala)nc++;
    if(mcCalb)nc++;
    if(mcEU)nc++;
    if(mcTiter)nc++;
    if(mcLog2)nc++;
    if(mcTiters)nc++;
    if(mcAge)nc++;
    if(mcBreed1)nc++;
    if(mcBreed2)nc++;
    if(mcBins)nc++;
    //if(mcBins2)nc++;
    if(mcResult)nc++;
    if(mcResult2)nc++;

    cColorsManager::generateDeterministicSeed(4321);
    cDataResult *res;
    char replicates[10];
    bool __alelisa=false;
    int __alelisa_multiplier=1;
    int k,l,offset,m;
    int platesi=0;
    int platesci=0;
    int datai=0;
    int samplei=0;
    int casesgi=0;
    int controlsi=0;
    int casepi=0;
    int p2_datai=1;
    int deltasi=0;
    int replicatesi=0;
    int result;
    FXdouble statsBase[5][5];
    double *statsValues[5];
    double ndata,aldata=0;
    FXint statsCount[5]={0,0,0,0,0};
    int scPos=-1,kc;
    FXbool foundSc=false;
    double *ctData=NULL,n,val;
    FXString invalidPlates="";
    int dp,rp,pd=0,diff=0,spi=-1,cplatei,splatei;

    // baseline initialization
    int cat=cSpeciesManager::getAgeFormat(oDataLink->getSpecies());
    int bslval[BASELINE_PARCOUNT][4];
    FXColor bslcl[3];
    FXColor bslclpal[3];
    for(int i=0;i<BASELINE_PARCOUNT;i++)
    {
        bslval[i][0]=bslval[i][1]=0;
        bslval[i][2]=bslval[i][3]=-1;
    }
    if(baseline)
    {
        if(bslss)
        {
            bslcl[0]=FXRGB(170,170,170);
            bslcl[1]=FXRGB(120,120,0);
            bslcl[2]=FXRGB(0,0,0);
            bslclpal[0]=FXRGB(220,220,220);
            bslclpal[1]=FXRGB(210,210,100);
            bslclpal[2]=FXRGB(0,0,0);
        }
        else
        {
            bslcl[0]=FXRGB(220,220,0);
            bslcl[1]=FXRGB(10,150,0);
            bslcl[2]=FXRGB(180,0,0);
            bslclpal[0]=FXRGB(240,240,100);
            bslclpal[1]=FXRGB(110,250,100);
            bslclpal[2]=FXRGB(250,100,100);
        }
        cDataResult *resb=cBaselineManager::getBaseline(bslid);
        if(resb && resb->getRowCount())
        {
            FXString params=resb->getCellString(0,2);
            int r=0;
            while(!params.empty())
            {
                if(BASELINE_PARCOUNT<=r)
                    break;
                FXString psrow=params.before('\n');
                params=params.after('\n');

                FXString a1=psrow.before('\t');
                psrow=psrow.after('\t');
                FXString a2=psrow.before('\t');
                psrow=psrow.after('\t');
                FXString lo=psrow.before('\t');
                psrow=psrow.after('\t');
                FXString hi=psrow.before('\t');
                psrow=psrow.after('\t');
                FXString st=psrow.before('\t');

                bslval[r][0]=FXIntVal(a1.before('-'))*cat+FXIntVal(a1.after('-'));
                bslval[r][1]=FXIntVal(a2.before('-'))*cat+FXIntVal(a2.after('-'));

                if(bslss)
                {
                    bslval[r][2]=FXIntVal(st);
                }
                else
                {
                    bslval[r][2]=FXIntVal(lo);
                    bslval[r][3]=FXIntVal(hi);
                }
                r++;
            }
            delete resb;
        }
    }
    // baseline initialization

    cVChart *rchart=NULL;
    FXDialogBox msg(oWinMain,oLanguage->getText("str_report_wtitle"),DECOR_TITLE|DECOR_BORDER);
    new FXLabel(&msg,oLanguage->getText("str_report_wtext"),NULL,LAYOUT_FILL|JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
    msg.create();
    msg.show(PLACEMENT_OWNER);
    msg.setFocus();
    msg.raise();
    oApplicationManager->beginWaitCursor();
    while(oApplicationManager->peekEvent())
        oApplicationManager->runOneEvent(false);
    FXDict *stdict=new FXDict();
    FXDict *singled=new FXDict();
    sCCRObject *sdentry;
    cVChart *chart=NULL;
    FXStringDict *resdict=new FXStringDict();
    FXStringDict *singlek=new FXStringDict();
    cColorTable *statsTable;
    if(useGraphs && !mcSingle)
        repWin->addTitle(oLanguage->getText("str_report_graphs")+(baseline?(" - "+bslid):""),0,true);
    for(int setsi=0;setsi<tSetsCount;setsi++)
    {
        sdentry=(sCCRObject*)singled->find(casesSets[setsi].cases[0].assay->text());
        if(!sdentry)
        {
            sdentry=new sCCRObject;
            sdentry->chart=new cVChart(repWin,cVChart::CHART_BAR,LAYOUT_FILL_X|LAYOUT_FIX_HEIGHT,0,0,0,350);
            chart=sdentry->chart;
            sdentry->cases=new FXDict();
            chart->setTitle((*casesSets[setsi].cases[0].assay)+" - "+casesSets[setsi].assayRes->getCellString(0,1));
            chart->create();
            chart->recalc();
            singlek->insert(FXStringFormat("%d",singlek->no()).text(),casesSets[setsi].cases[0].assay->text());
            singled->insert(casesSets[setsi].cases[0].assay->text(),sdentry);
        }
        spi=-1;
        splatei=-1;
        diff=0;
        __alelisa=casesSets[setsi].alelisa && casesSets[setsi].orientation==1?true:false;
        __alelisa_multiplier=__alelisa?2:1;
        for(int casei=0;casei<casesSets[setsi].casesCount;casei++,casesgi++)
        {
            statsTable=new cColorTable(repWin);
            statsTable->create();

            /*cColorTable *plateTable2=new cColorTable(repWin);
            plateTable2->setTableSize(1,nc);
            plateTable2->create();

            for(int i=0;i<plateTable2->getNumRows();i++)
                for(int j=0;j<plateTable2->getNumColumns();j++)
                    plateTable2->setItemText(i,j,(char*)NULL);

            kc=0;
            plateTable2->setItemText(0,kc++,oLanguage->getText("str_rdgmdi_cllayout"));
            plateTable2->setItemText(0,kc++,oLanguage->getText("str_rdgmdi_od"));
            if(mcCala)plateTable2->setItemText(0,kc++,casesSets[setsi].cala->text());
            if(mcCalb)plateTable2->setItemText(0,kc++,casesSets[setsi].useSecond?casesSets[setsi].calb->text():oLanguage->getText("str_rdgmdi_calb"));
            if(mcEU)plateTable2->setItemText(0,kc++,oLanguage->getText("str_rdgmdi_eu"));
            if(mcTiter)plateTable2->setItemText(0,kc++,oLanguage->getText("str_asymdi_titer"));
            if(mcLog2)plateTable2->setItemText(0,kc++,oLanguage->getText("str_rdgmdi_log2"));
            if(mcTiters)plateTable2->setItemText(0,kc++,oLanguage->getText("str_asymdi_titers"));
            if(mcBins)plateTable2->setItemText(0,kc++,oLanguage->getText("str_asymdi_bins"));
            //if(mcBins2)plateTable2->setItemText(0,kc++,oLanguage->getText("str_asymdi_bins2"));
            if(mcAge)plateTable2->setItemText(0,kc++,oLanguage->getText("str_rdgmdi_clage"));
            if(mcBreed1)plateTable2->setItemText(0,kc++,oLanguage->getText("str_dlg_pop_breed1"));
            if(mcBreed2)plateTable2->setItemText(0,kc++,oLanguage->getText("str_dlg_pop_breed2"));
            if(mcResult)plateTable2->setItemText(0,kc++,oLanguage->getText("str_rdgmdi_result"));
            if(mcResult2)plateTable2->setItemText(0,kc++,oLanguage->getText("str_rdgmdi_result2"));*/

            int stp=0,sts=0,stn=0,ste=0;
            for(int i=0;i<5;i++)
                for(int j=0;j<5;j++)
                    statsBase[i][j]=0;

            int ctui=casesSets[setsi].cases[casei].count*(!mcAm?(casesSets[setsi].cases[casei].replicates+1):1);
            for(int ui=0;ui<5;ui++)
            {
                statsValues[ui]=(double*)malloc(ctui*sizeof(double));
                if(!statsValues[ui])
                {
                    tSetsCount=0;
                    tCasesCount=0;
                    FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                    repWin->close();
                    return false;
                }
                statsBase[ui][0]=999999;
                statsBase[ui][4]=ctui;
            }
            if(mcSingle && baseline && (chart->legendStrings()->no()==0))
            {
                int bp=0;
                chart->legendStrings()->insert(FXStringFormat("%d",chart->legendStrings()->no()).text(),FXStringFormat("%s\n%d\t%d\t%d",oLanguage->getText("str_report_bsllo").text(),FXREDVAL(bslclpal[bp]),FXGREENVAL(bslclpal[bp]),FXBLUEVAL(bslclpal[bp])).text());
                bp++;
                if(!bslss)
                {
                    chart->legendStrings()->insert(FXStringFormat("%d",chart->legendStrings()->no()).text(),FXStringFormat("%s\n%d\t%d\t%d",oLanguage->getText("str_report_bslmd").text(),FXREDVAL(bslclpal[bp]),FXGREENVAL(bslclpal[bp]),FXBLUEVAL(bslclpal[bp])).text());
                    bp++;
                }
                chart->legendStrings()->insert(FXStringFormat("%d",chart->legendStrings()->no()).text(),FXStringFormat("%s\n%d\t%d\t%d",oLanguage->getText("str_report_bslhi").text(),FXREDVAL(bslclpal[bp]),FXGREENVAL(bslclpal[bp]),FXBLUEVAL(bslclpal[bp])).text());
            }
            if(!mcSingle)
            {
                chart=new cVChart(repWin,cVChart::CHART_BAR,LAYOUT_FIX_WIDTH|LAYOUT_FIX_HEIGHT,0,0,200,250);
                chart->setTitle((*casesSets[setsi].cases[casei].id)+" - "+(*casesSets[setsi].cases[casei].population)+" - "+(*casesSets[setsi].cases[casei].assay)+" - "+(*casesSets[setsi].cases[casei].readingDate));
                chart->create();
                chart->recalc();
            }
            FXDict *cv=chart->dataSet();
            cplatei=0;
            int bct[31],tct[31],sct=0;
            int chtype=0;
            for(int h=0;h<31;h++)
                bct[h]=tct[h]=0;
            samplei=0;
            int rdi=datai;
            for(int g=spi+1;g<casesSets[setsi].cases[casei].startPlate;g++,diff++,splatei++);
            spi=casesSets[setsi].cases[casei].startPlate;
            while(samplei<casesSets[setsi].cases[casei].count*__alelisa_multiplier)
            {
                deltasi=0;
                replicatesi=0;
                replicates[0]=0;
                while(replicatesi<=casesSets[setsi].cases[casei].replicates)
                {
                    platesi=datai/(96/__alelisa_multiplier);
                    if(platesci==platesi || datai==rdi)
                    {
                        splatei++;
                        if(samplei)cplatei++;
                        ctData=casesSets[setsi].cases[casei].controlsData+cplatei*casesSets[setsi].controlsPerPlate*__alelisa_multiplier;

                        double na=0,pa=0,nha=0,pha=0,pc=0,nc=0;
                        for(int cswitchi=0;cswitchi<2;cswitchi++)
                        {
                            controlsi=0;
                            for(int i=0;i<casesSets[setsi].controlsPerPlate;i++)
                            {
                                if(((!cswitchi) && (casesSets[setsi].controlsLayout[i]>=0)) ||
                                   (cswitchi && (casesSets[setsi].controlsLayout[i]<0)))
                                {
                                    controlsi+=__alelisa?2:1;
                                    continue;
                                }

                                if(casesSets[setsi].controlsLayout[i]>=0)
                                    {pa+=ctData[controlsi];pc++;}
                                if(casesSets[setsi].controlsLayout[i]<0)
                                    {na+=ctData[controlsi];nc++;}

                                m=cMDIReadings::platePosition(abs(casesSets[setsi].controlsLayout[i])==1000?0:abs(casesSets[setsi].controlsLayout[i]),false,casesSets[setsi].orientation);
                                l=m%12;
                                //plateTable2->insertRows(p2_datai);
                                //plateTable2->setItemText(p2_datai,0,FXStringFormat("(%s) %c%d",casesSets[setsi].controlsLayout[i]>=0?oLanguage->getText("str_pos").text():oLanguage->getText("str_neg").text(),
                                //                                                   'A'+m/12,l+1));
                                //plateTable2->setItemText(p2_datai,1,cMDIReadings::calculVal(ctData[controlsi]));
                                p2_datai++;
                                controlsi++;
                                if(__alelisa)
                                {
                                    if(casesSets[setsi].controlsLayout[i]>=0)
                                        pha+=ctData[controlsi];
                                    if(casesSets[setsi].controlsLayout[i]<0)
                                        nha+=ctData[controlsi];

                                    m=cMDIReadings::platePosition(abs(casesSets[setsi].controlsLayout[i])==1000?0:abs(casesSets[setsi].controlsLayout[i]),false,casesSets[setsi].orientation)+1;
                                    l=m%12;
                                    //plateTable2->insertRows(p2_datai);
                                    //plateTable2->setItemText(p2_datai,0,FXStringFormat("%c%d",'A'+m/12,l+1));
                                    //plateTable2->setItemText(p2_datai,1,cMDIReadings::calculVal(ctData[controlsi]));
                                    p2_datai++;
                                    controlsi++;
                                }
                            }
                        }

                        platesci++;
                        FXString rule,op,r2,rules_defs=casesSets[setsi].assayRes->getCellString(0,10);
                        r2=rules_defs;
                        na/=nc;
                        pa/=pc;
                        nha/=nc;
                        pha/=pc;
                    }

                    casepi=datai%(96/__alelisa_multiplier);
                    dp=datai/(96/__alelisa_multiplier);
                    rp=pd+casesSets[setsi].cases[casei].startPlate-diff;
                    if(!cMDIReadings::isControlPosition(casepi,casesSets[setsi].controlsLayout,
                                          casesSets[setsi].controlsPerPlate,__alelisa) &&
                                          ((casepi>=casesSets[setsi].cases[casei].startCell && dp==rp) || (dp>rp)))
                    {
                        m=cMDIReadings::platePosition(datai%(96/__alelisa_multiplier),__alelisa,casesSets[setsi].orientation);
                        l=m%12;
                        k=(datai/(96/__alelisa_multiplier))*8+m/12;
                        offset=(casesSets[setsi].cases[casei].replicates+1)*samplei+__alelisa_multiplier*replicatesi;
                        //plateTable2->insertRows(p2_datai);
                        //plateTable2->setItemText(p2_datai,0,FXStringFormat("%c%d",'A'+m/12,l+1));
                        //plateTable2->setItemText(p2_datai,1,cMDIReadings::calculVal(casesSets[setsi].cases[casei].data[offset]));
                        if(mcAm && replicatesi==0)
                        {
                            val=n=0;
                            for(int xi=0;xi<=casesSets[setsi].cases[casei].replicates;xi++,n++)
                                val+=casesSets[setsi].cases[casei].data[(casesSets[setsi].cases[casei].replicates+1)*samplei+__alelisa_multiplier*xi];
                            ndata=val/n;
                        }
                        else if(!mcAm)
                            ndata=casesSets[setsi].cases[casei].data[offset];
                        else
                        {
                            ndata=-999;
                            //for(int xi=2;xi<nc;xi++)
                                //plateTable2->setItemText(p2_datai,xi,replicates);
                        }
                        deltasi=1;
                        if(__alelisa)
                        {
                            m=cMDIReadings::platePosition(datai%(96/__alelisa_multiplier),__alelisa,casesSets[setsi].orientation,true);
                            l=m%12;
                            k=(datai/(96/__alelisa_multiplier))*8+m/12;
                            offset=(casesSets[setsi].cases[casei].replicates+1)*samplei+__alelisa_multiplier*replicatesi+1;
                            //plateTable2->insertRows(p2_datai+1);
                            //plateTable2->setItemText(p2_datai+1,0,FXStringFormat("%c%d",'A'+m/12,l+1));
                            //plateTable2->setItemText(p2_datai+1,1,cMDIReadings::calculVal(casesSets[setsi].cases[casei].data[offset]));
                            if(mcAm && replicatesi==0)
                            {
                                val=n=0;
                                for(int xi=0;xi<=casesSets[setsi].cases[casei].replicates;xi++,n++)
                                    val+=casesSets[setsi].cases[casei].data[(casesSets[setsi].cases[casei].replicates+1)*samplei+__alelisa_multiplier*xi+1];
                                aldata=val/n;
                            }
                            else if(!mcAm)
                                aldata=casesSets[setsi].cases[casei].data[offset];
                            else
                                aldata=-999;
                            deltasi=2;
                        }

                        if(ndata!=-999)
                        {
                            double calt,titer=0,titerg,bin,cala,calb=0,caleu,titerl2;
                            int k=2,st=0;

                            if(!foundSc)
                            {
                                foundSc=true;
                                scPos=p2_datai;
                            }

                            cala=cFormulaRatio::calculateRatio(casesSets[setsi].cala,
                                                               ndata,
                                                               __alelisa?aldata:0,
                                                               ctData,casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate,
                                                               casesSets[setsi].cases[casei].factor,casesSets[setsi].factors,__alelisa);
                            if(mcCala)
                            {
                                //plateTable2->setItemText(p2_datai,k++,cMDIReadings::calculVal(cala));
                                    if(cala<statsBase[st][0])
                                        statsBase[st][0]=cala;
                                    if(cala>statsBase[st][1])
                                        statsBase[st][1]=cala;
                                    statsBase[st][2]+=cala;
                                    statsBase[st][3]+=cala?log10(cala):0;
                                    statsValues[st][statsCount[st]++]=cala;
                                    st++;
                            }

                            if(casesSets[setsi].useSecond)
                                calb=cFormulaRatio::calculateRatio(casesSets[setsi].calb,
                                                                   ndata,
                                                                   __alelisa?aldata:0,
                                                                   ctData,casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate,
                                                                   casesSets[setsi].cases[casei].factor,casesSets[setsi].factors,__alelisa);
                            if(mcCalb)
                                if(!casesSets[setsi].useSecond)
                                    ;//plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_na"));
                                else
                                {
                                    //plateTable2->setItemText(p2_datai,k++,cMDIReadings::calculVal(calb));
                                        if(calb<statsBase[st][0])
                                            statsBase[st][0]=calb;
                                        if(calb>statsBase[st][1])
                                            statsBase[st][1]=calb;
                                        statsBase[st][2]+=calb;
                                        statsBase[st][3]+=calb?log10(calb):0;
                                        statsValues[st][statsCount[st]++]=calb;
                                        st++;
                                }
                            if(mcEU)
                            {
                                caleu=cFormulaRatio::calculateEU(ndata,
                                                                 ctData,casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate,
                                                                 casesSets[setsi].cases[casei].factor,casesSets[setsi].factors,__alelisa);
                                    if(caleu<statsBase[st][0])
                                        statsBase[st][0]=caleu;
                                    if(caleu>statsBase[st][1])
                                        statsBase[st][1]=caleu;
                                    statsBase[st][2]+=caleu;
                                    statsBase[st][3]+=caleu?log10(caleu):0;
                                    statsValues[st][statsCount[st]++]=caleu;
                                    st++;

                                //plateTable2->setItemText(p2_datai,k++,cMDIReadings::calculVal(caleu));
                            }

                            if(!casesSets[setsi].useTiters)
                            {
                                //if(mcTiter)
                                    //plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_na"));
                            }
                            else
                            {
                                calt=cFormulaRatio::calculateRatio(casesSets[setsi].calt,
                                                                   ndata,
                                                                   __alelisa?aldata:0,
                                                                   ctData,casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate,
                                                                   casesSets[setsi].cases[casei].factor,casesSets[setsi].factors,__alelisa);
                                titer=cFormulaRatio::calculateTiter(calt,casesSets[setsi].slope,casesSets[setsi].intercept,casesSets[setsi].cases[casei].factor,casesSets[setsi].factors);
                                //if(mcTiter)
                                    //plateTable2->setItemText(p2_datai,k++,cMDIReadings::calculVal((int)titer));
                                    if(titer<statsBase[st][0])
                                        statsBase[st][0]=titer;
                                    if(titer>statsBase[st][1])
                                        statsBase[st][1]=titer;
                                    statsBase[st][2]+=titer;
                                    statsBase[st][3]+=titer?log10(titer):0;
                                    statsValues[st][statsCount[st]++]=titer;
                                    st++;
                            }

                            if(mcLog2)
                            {
                                if(!casesSets[setsi].useTiters)
                                    ;//plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_na"));
                                else
                                {
                                    titerl2=cFormulaRatio::calculateTiterLog2(titer);
                                    //plateTable2->setItemText(p2_datai,k++,cMDIReadings::calculVal(titerl2));
                                        if(titerl2<statsBase[st][0])
                                            statsBase[st][0]=titerl2;
                                        if(titerl2>statsBase[st][1])
                                            statsBase[st][1]=titerl2;
                                        statsBase[st][2]+=titerl2;
                                        statsBase[st][3]+=titerl2?log10(titerl2):0;
                                        statsValues[st][statsCount[st]++]=titerl2;
                                        st++;
                                }
                            }

                            bool is=false;
                            for(int we=0;we<30;we++)
                                if(casesSets[setsi].titerGroups[we]!=0)
                                {
                                    is=true;
                                    break;
                                }
                            if(is)
                                titerg=cFormulaRatio::calculateTiterGroup(titer,casesSets[setsi].titerGroups);
                            else
                                titerg=-1;
                            /*if(mcTiters)
                            {
                                (!casesSets[setsi].useTiters || !is)
                                    plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_na"));
                                else
                                    plateTable2->setItemText(p2_datai,k++,cMDIReadings::calculVal((int)titerg));
                            }*/

                            is=false;
                            for(int we=0;we<30;we++)
                                if(casesSets[setsi].ratioGroups[we]!=0)
                                {
                                    is=true;
                                    break;
                                }
                            if(is)
                                bin=cFormulaRatio::calculateRatioGroup(cala,casesSets[setsi].ratioGroups);
                            else
                                bin=-1;
                            if(mcBins/* || mcBins2*/)
                            {

                                /*if(mcBins)
                                {
                                    if(!is)
                                        plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_na"));
                                    else
                                        plateTable2->setItemText(p2_datai,k++,cMDIReadings::calculVal((int)bin));
                                }*/
                                /*if(mcBins2)
                                {
                                    if(!is || !casesSets[setsi].useSecond)
                                        plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_na"));
                                    else
                                        plateTable2->setItemText(p2_datai,k++,cMDIReadings::calculVal((int)cFormulaRatio::calculateRatioGroup(calb,casesSets[setsi].ratioGroups)));
                                }*/
                            }

                            /*if(mcAge)
                                plateTable2->setItemText(p2_datai,k++,*casesSets[setsi].cases[casei].age);

                            if(mcBreed1 || mcBreed2)
                            {
                                if(mcBreed1)
                                    plateTable2->setItemText(p2_datai,k++,*casesSets[setsi].cases[casei].breed1);
                                if(mcBreed2)
                                    plateTable2->setItemText(p2_datai,k++,*casesSets[setsi].cases[casei].breed2);
                            }*/

                            if(mcResult)
                            {
                                double __calapco,__calasco;
                                int r,a;
                                sControlAms cams=cFormulaRatio::calculateControlAms(ctData,casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate,__alelisa);
                                ClearAllVars();
                                SetValue("pa",&cams.posam);
                                SetValue("na",&cams.negam);
                                r=Evaluate((char*)casesSets[setsi].calapco->text(),&__calapco,&a);
                                r=Evaluate((char*)casesSets[setsi].calasco->text(),&__calasco,&a);
                                ClearAllVars();
                                result=0;
                                if(*casesSets[setsi].calaop==">=")
                                    result=cala==-9999?-9999:cala<__calasco?0:cala<__calapco?1:2;
                                else
                                    result=cala==-9999?-9999:cala>__calasco?0:cala>__calapco?1:2;

                                switch(result)
                                {
                                    case 0:
                                        //plateTable2->setItemText(p2_datai,k,oLanguage->getText("str_rdgmdi_neg"));
                                        //plateTable2->setCellTextColor(p2_datai,k,FXRGB(180,180,180));
                                        //plateTable2->useCellTextColor(p2_datai,k,true);
                                        k++;
                                        stn++;
                                        break;
                                    case 1:
                                        //plateTable2->setItemText(p2_datai,k,oLanguage->getText("str_rdgmdi_sus"));
                                        //plateTable2->setCellTextColor(p2_datai,k,FXRGB(210,120,30));
                                        //plateTable2->useCellTextColor(p2_datai,k,true);
                                        k++;
                                        sts++;
                                        break;
                                    case 2:
                                        //plateTable2->setItemText(p2_datai,k,oLanguage->getText("str_rdgmdi_pos"));
                                        //plateTable2->setCellTextColor(p2_datai,k,FXRGB(160,0,0));
                                        //plateTable2->useCellTextColor(p2_datai,k,true);
                                        k++;
                                        stp++;
                                        break;
                                    default:
                                        //plateTable2->setItemText(p2_datai,k,oLanguage->getText("str_invalid"));
                                        //plateTable2->useCellTextColor(p2_datai,k,false);
                                        k++;
                                        ste++;
                                        break;
                                }
                            }

                            if(mcResult2)
                            {
                                if(!casesSets[setsi].useSecond)
                                    ;//plateTable2->setItemText(p2_datai,k++,oLanguage->getText("str_na"));
                                else
                                {
                                    double __calbpco,__calbsco;
                                    int r,a;
                                    sControlAms cams=cFormulaRatio::calculateControlAms(ctData,casesSets[setsi].controlsLayout,casesSets[setsi].controlsPerPlate,__alelisa);
                                    ClearAllVars();
                                    SetValue("pa",&cams.posam);
                                    SetValue("na",&cams.negam);
                                    r=Evaluate((char*)casesSets[setsi].calbpco->text(),&__calbpco,&a);
                                    r=Evaluate((char*)casesSets[setsi].calbsco->text(),&__calbsco,&a);
                                    ClearAllVars();
                                    result=0;
                                    if(*casesSets[setsi].calbop==">=")
                                        result=calb==-9999?-9999:calb<__calbsco?0:cala<__calbpco?1:2;
                                    else
                                        result=calb==-9999?-9999:calb>__calbsco?0:cala>__calbpco?1:2;
                                    switch(result)
                                    {
                                        case 0:
                                            //plateTable2->setItemText(p2_datai,k,oLanguage->getText("str_rdgmdi_neg"));
                                            //plateTable2->setCellTextColor(p2_datai,k,FXRGB(180,180,180));
                                            //plateTable2->useCellTextColor(p2_datai,k,true);
                                            k++;
                                            break;
                                        case 1:
                                            //plateTable2->setItemText(p2_datai,k,oLanguage->getText("str_rdgmdi_sus"));
                                            //plateTable2->setCellTextColor(p2_datai,k,FXRGB(230,200,50));
                                            //plateTable2->useCellTextColor(p2_datai,k,true);
                                            k++;
                                            break;
                                        case 2:
                                            //plateTable2->setItemText(p2_datai,k,oLanguage->getText("str_rdgmdi_pos"));
                                            //plateTable2->setCellTextColor(p2_datai,k,FXRGB(160,0,0));
                                            //plateTable2->useCellTextColor(p2_datai,k,true);
                                            k++;
                                            break;
                                        default:
                                            //plateTable2->setItemText(p2_datai,k,oLanguage->getText("str_invalid"));
                                            //plateTable2->useCellTextColor(p2_datai,k,false);
                                            k++;
                                            break;
                                    }
                                }
                            }


                            if(graphType==0 && titerg!=-1 && titerg!=-9999 && casesSets[setsi].useTiters)
                            {
                                tct[(int)titerg]++;
                                chtype=0;
                            }
                            else if(graphType<2 && bin!=-1 && bin!=-9999)
                            {
                                bct[(int)bin]++;
                                chtype=1;
                            }
                            else if((graphType<2 || graphType==3) && casesSets[setsi].useTiters)
                            {
                                chtype=3;
                                if(titer>-9999)
                                {
                                    sVChartData *cval=new sVChartData;
                                    cval->value=titer<0?0:titer;
                                    cval->name=FXStringFormat("%d",sct+1);
                                    cv->insert(FXStringFormat("%d",sct).text(),cval);
                                    sct++;
                                }
                            }
                            else
                            {
                                chtype=2;
                                if(cala>-9999)
                                {
                                    sVChartData *cval=new sVChartData;
                                    cval->value=cala<0?0:cala;
                                    cval->name=FXStringFormat("%d",sct+1);
                                    cv->insert(FXStringFormat("%d",cv->no()).text(),cval);
                                    sct++;
                                }
                            }
                        }

                        p2_datai+=deltasi;
                        replicates[replicatesi]='\'';
                        replicates[replicatesi+1]=0;
                        replicatesi++;
                    }
                    datai++;
                }
                samplei+=deltasi;
            }

            int bslpos=-1;
            if(baseline)
            {
                int age=FXIntVal(casesSets[setsi].cases[casei].age->before('-'))*cat+FXIntVal(casesSets[setsi].cases[casei].age->after('-'));
                for(int i=0;i<BASELINE_PARCOUNT;i++)
                {
                    if((bslval[i][2] || bslval[i][3])&&bslval[i][1]&&(bslval[i][0]<=age)&&(bslval[i][1]>=age))
                    {
                        bslpos=i;
                        break;
                    }
                }
            }

            int ccnt;
            FXColor col=cColorsManager::generateDeterministicColor(50,180);
            double cl,dc;
            switch(chtype)
            {
                case 0:
                    cl=130;
                    for(ccnt=0;(ccnt<30) && (casesSets[setsi].titerGroups[ccnt]!=0);ccnt++);
                    ccnt++;
                    dc=50.0/(double)ccnt;
                    if((!mcSingle) && baseline && (bslpos!=-1))
                    {
                        int bp=0;
                        chart->legendStrings()->insert(FXStringFormat("%d",chart->legendStrings()->no()).text(),FXStringFormat("%s\n%d\t%d\t%d",oLanguage->getText("str_report_bsllo").text(),FXREDVAL(bslcl[bp]),FXGREENVAL(bslcl[bp]),FXBLUEVAL(bslcl[bp])).text());
                        bp++;
                        if(!bslss)
                        {
                            chart->legendStrings()->insert(FXStringFormat("%d",chart->legendStrings()->no()).text(),FXStringFormat("%s\n%d\t%d\t%d",oLanguage->getText("str_report_bslmd").text(),FXREDVAL(bslcl[bp]),FXGREENVAL(bslcl[bp]),FXBLUEVAL(bslcl[bp])).text());
                            bp++;
                        }
                        chart->legendStrings()->insert(FXStringFormat("%d",chart->legendStrings()->no()).text(),FXStringFormat("%s\n%d\t%d\t%d",oLanguage->getText("str_report_bslhi").text(),FXREDVAL(bslcl[bp]),FXGREENVAL(bslcl[bp]),FXBLUEVAL(bslcl[bp])).text());
                    }

                    for(int i=0;i<ccnt;i++/*,cl-=dc*/)
                    {
                        sVChartData *cval=new sVChartData;
                        cval->value=tct[i];
                        int bp=0;
                        if(baseline && bslpos!=-1)
                        {
                            int thi=casesSets[setsi].titerGroups[i]?casesSets[setsi].titerGroups[i]:999999;
                            bp=0;
                            if(thi>=bslval[bslpos][2])
                            {
                                bp++;
                                if((bslval[bslpos][3]!=-1) && (thi>=bslval[bslpos][3]))
                                    bp++;
                            }
                        }
                        if(mcSingle)
                            cval->color=col;
                        else
                        {
                            if(baseline && bslpos!=-1 && casesSets[setsi].titerGroups[i])
                                cval->color=bslcl[bp];
                            else
                                cval->color=FXRGB(((int)cl)>>1,(int)cl,0);
                        }
                        if(mcSingle && baseline && bslpos!=-1)
                            cval->sValue=FXStringFormat("\n%d\t%d\t%d",
                                                      FXREDVAL(bslclpal[bp]),FXGREENVAL(bslclpal[bp]),FXBLUEVAL(bslclpal[bp]));
                        cval->name=(i==ccnt-1)?FXStringFormat("%d+",i):FXStringFormat("%d",i);
                        cv->insert(FXStringFormat("%d",cv->no()).text(),cval);
                    }
                    chart->setHAxisTitle(oLanguage->getText("str_asymdi_titers"));
                    chart->setVAxisTitle(oLanguage->getText("str_rdgmdi_chcount"));
                    break;
                case 1:
                    cl=180;
                    for(ccnt=0;(ccnt<30) && (casesSets[setsi].ratioGroups[ccnt]!=0);ccnt++);
                    ccnt++;
                    dc=50.0/(double)ccnt;
                    for(int i=0;i<ccnt;i++/*,cl-=dc*/)
                    {
                        sVChartData *cval=new sVChartData;
                        cval->value=bct[i];
                        if(mcSingle)
                            cval->color=col;
                        else
                            cval->color=FXRGB((int)cl,((int)cl)>>1,0);
                        cval->name=(i==ccnt-1)?FXStringFormat("%d+",i):FXStringFormat("%d",i);
                        cv->insert(FXStringFormat("%d",cv->no()).text(),cval);
                    }
                    chart->setHAxisTitle(oLanguage->getText("str_asymdi_bins"));
                    chart->setVAxisTitle(oLanguage->getText("str_rdgmdi_chcount"));
                    break;
                case 3:
                    cl=150;
                    ccnt=cv->no();
                    if(ccnt)
                        dc=30.0/(double)ccnt;
                    /*for(int i=0;i<ccnt;i++,cl-=dc)
                            ((sVChartData*)cv->find(FXStringFormat("%d",i).text()))->color=FXRGB(((int)cl)>>1,0,(int)cl>>1);*/

                    if(baseline && bslpos!=-1)
                    {
                        int bp=0;
                        chart->legendStrings()->insert(FXStringFormat("%d",chart->legendStrings()->no()).text(),FXStringFormat("%s\n%d\t%d\t%d",oLanguage->getText("str_report_bsllo").text(),FXREDVAL(bslcl[bp]),FXGREENVAL(bslcl[bp]),FXBLUEVAL(bslcl[bp])).text());
                        bp++;
                        if(!bslss)
                        {
                            chart->legendStrings()->insert(FXStringFormat("%d",chart->legendStrings()->no()).text(),FXStringFormat("%s\n%d\t%d\t%d",oLanguage->getText("str_report_bslmd").text(),FXREDVAL(bslcl[bp]),FXGREENVAL(bslcl[bp]),FXBLUEVAL(bslcl[bp])).text());
                            bp++;
                        }
                        chart->legendStrings()->insert(FXStringFormat("%d",chart->legendStrings()->no()).text(),FXStringFormat("%s\n%d\t%d\t%d",oLanguage->getText("str_report_bslhi").text(),FXREDVAL(bslcl[bp]),FXGREENVAL(bslcl[bp]),FXBLUEVAL(bslcl[bp])).text());
                    }
                    for(int i=0;i<ccnt;i++/*,cl-=dc*/)
                    {
                        sVChartData *cval=((sVChartData*)cv->find(FXStringFormat("%d",i).text()));
                        if(baseline && bslpos!=-1)
                        {
                            int thi=(int)round(cval->value);
                            int bp=0;
                            if(thi>=bslval[bslpos][2])
                            {
                                bp++;
                                if((bslval[bslpos][3]!=-1) && (thi>=bslval[bslpos][3]))
                                    bp++;
                            }

                            cval->color=bslcl[bp];
                        }
                        else
                            cval->color=FXRGB(((int)cl)>>1,0,(int)cl>>1);
                    }

                    chart->setHAxisTitle(oLanguage->getText("str_rdgmdi_chsamples"));
                    chart->setVAxisTitle(oLanguage->getText("str_asymdi_titer"));
                    break;
                default:
                    cl=150;
                    ccnt=cv->no();
                    if(ccnt)
                        dc=30.0/(double)ccnt;
                    for(int i=0;i<ccnt;i++/*,cl-=dc*/)
                            ((sVChartData*)cv->find(FXStringFormat("%d",i).text()))->color=FXRGB(0,((int)cl)>>2,(int)cl);
                    chart->setHAxisTitle(oLanguage->getText("str_rdgmdi_chsamples"));
                    chart->setVAxisTitle(*casesSets[setsi].cala);
                    break;
            }

            if(mcSingle)
                chart->legendStrings()->insert(FXStringFormat("%d",chart->legendStrings()->no()).text(),casesSets[setsi].cases[casei].id->text());

            int ps=setsi;

            statsTable=new cColorTable(repWin);
            statsTable->setTableSize(7,1);
            statsTable->setItemText(0,0,oLanguage->getText("str_report_statvars"));
            statsTable->setItemText(1,0,oLanguage->getText("str_rdgmdi_min"));
            statsTable->setItemText(2,0,oLanguage->getText("str_rdgmdi_max"));
            statsTable->setItemText(3,0,oLanguage->getText("str_rdgmdi_amean"));
            statsTable->setItemText(4,0,oLanguage->getText("str_rdgmdi_gmean"));
            statsTable->setItemText(5,0,oLanguage->getText("str_rdgmdi_sd"));
            statsTable->setItemText(6,0,oLanguage->getText("str_rdgmdi_cv"));
            statsTable->create();
            int snc=mcCala?2:1;
            kc=0;
            if(mcCalb && casesSets[ps].useSecond)snc++;
            if(mcEU)snc++;
            if(mcTiter && casesSets[ps].useTiters)snc++;
            if(mcLog2 && casesSets[ps].useTiters)snc++;
            statsTable->insertColumns(1,snc);
            double stdev,armean;
            if(mcCala)
            {
                statsTable->setItemText(0,kc+1,*casesSets[ps].cala);
                if(statsBase[kc][0]==-9999)
                {
                    for(int ct=0;ct<6;ct++)
                        statsTable->setItemText(ct+1,kc+1,cMDIReadings::calculVal(-9999));
                }
                else
                {
                    statsTable->setItemText(1,kc+1,cMDIReadings::calculVal(statsBase[kc][0]));
                    statsTable->setItemText(2,kc+1,cMDIReadings::calculVal(statsBase[kc][1]));
                    armean=statsBase[kc][2]/statsBase[kc][4];
                    statsTable->setItemText(3,kc+1,cMDIReadings::calculVal(armean));
                    statsTable->setItemText(4,kc+1,cMDIReadings::calculVal(pow(10,statsBase[kc][3]/statsBase[kc][4])));
                    stdev=0;
                    for(int ct=0;ct<statsBase[kc][4];ct++)
                    {
                        stdev+=pow(statsValues[kc][ct]-armean,2);
                    }
                    stdev/=statsBase[kc][4];
                    stdev=sqrt(stdev);
                    statsTable->setItemText(5,kc+1,cMDIReadings::calculVal(stdev));
                    statsTable->setItemText(6,kc+1,cMDIReadings::calculVal(!armean?0:round(10*(100*stdev/armean))/10)+"%");
                }
                kc++;
            }

            if(mcCalb && casesSets[ps].useSecond)
            {
                statsTable->setItemText(0,kc+1,*casesSets[ps].calb);
                if(statsBase[kc][0]==-9999)
                {
                    for(int ct=0;ct<6;ct++)
                        statsTable->setItemText(ct+1,kc+1,cMDIReadings::calculVal(-9999));
                }
                else
                {
                    statsTable->setItemText(1,kc+1,cMDIReadings::calculVal(statsBase[kc][0]));
                    statsTable->setItemText(2,kc+1,cMDIReadings::calculVal(statsBase[kc][1]));
                    armean=statsBase[kc][2]/statsBase[kc][4];
                    statsTable->setItemText(3,kc+1,cMDIReadings::calculVal(armean));
                    statsTable->setItemText(4,kc+1,cMDIReadings::calculVal(pow(10,statsBase[kc][3]/statsBase[kc][4])));
                    stdev=0;
                    for(int ct=0;ct<statsBase[kc][4];ct++)
                        stdev+=pow(statsValues[kc][ct]-armean,2);
                    stdev/=statsBase[kc][4];
                    stdev=sqrt(stdev);
                    statsTable->setItemText(5,kc+1,cMDIReadings::calculVal(stdev));
                    statsTable->setItemText(6,kc+1,cMDIReadings::calculVal(!armean?0:round(10*(100*stdev/armean))/10)+"%");
                }
                kc++;
            }
            if(mcEU)
            {
                statsTable->setItemText(0,kc+1,oLanguage->getText("str_rdgmdi_eu"));
                if(statsBase[kc][0]==-9999)
                {
                    for(int ct=0;ct<6;ct++)
                        statsTable->setItemText(ct+1,kc+1,cMDIReadings::calculVal(-9999));
                }
                else
                {
                    statsTable->setItemText(1,kc+1,cMDIReadings::calculVal(statsBase[kc][0]));
                    statsTable->setItemText(2,kc+1,cMDIReadings::calculVal(statsBase[kc][1]));
                    armean=statsBase[kc][2]/statsBase[kc][4];
                    statsTable->setItemText(3,kc+1,cMDIReadings::calculVal(round(armean)));
                    statsTable->setItemText(4,kc+1,cMDIReadings::calculVal(round(pow(10,statsBase[kc][3]/statsBase[kc][4]))));
                    stdev=0;
                    for(int ct=0;ct<statsBase[kc][4];ct++)
                    {
                        stdev+=pow(statsValues[kc][ct]-armean,2);
                    }
                    stdev/=statsBase[kc][4];
                    stdev=sqrt(stdev);
                    statsTable->setItemText(5,kc+1,cMDIReadings::calculVal(round(stdev)));
                    statsTable->setItemText(6,kc+1,cMDIReadings::calculVal(round(!armean?0:round(10*(100*stdev/armean))/10))+"%");
                }
                kc++;
            }
            if(mcTiter && casesSets[ps].useTiters)
            {
                statsTable->setItemText(0,kc+1,oLanguage->getText("str_asymdi_titer"));
                if(statsBase[kc][0]==-9999)
                {
                    for(int ct=0;ct<6;ct++)
                        statsTable->setItemText(ct+1,kc+1,cMDIReadings::calculVal(-9999));
                }
                else
                {
                    statsTable->setItemText(1,kc+1,cMDIReadings::calculVal(statsBase[kc][0]));
                    statsTable->setItemText(2,kc+1,cMDIReadings::calculVal(statsBase[kc][1]));
                    armean=statsBase[kc][2]/statsBase[kc][4];
                    statsTable->setItemText(3,kc+1,cMDIReadings::calculVal(round(armean)));
                    statsTable->setItemText(4,kc+1,cMDIReadings::calculVal(round(pow(10,statsBase[kc][3]/statsBase[kc][4]))));
                    stdev=0;
                    for(int ct=0;ct<statsBase[kc][4];ct++)
                    {
                        stdev+=pow(statsValues[kc][ct]-armean,2);
                    }
                    stdev/=statsBase[kc][4];
                    stdev=sqrt(stdev);
                    statsTable->setItemText(5,kc+1,cMDIReadings::calculVal(round(stdev)));
                    statsTable->setItemText(6,kc+1,cMDIReadings::calculVal(round(!armean?0:round(10*(100*stdev/armean))/10))+"%");
                }
                kc++;
            }
            if(mcLog2 && casesSets[ps].useTiters)
            {
                statsTable->setItemText(0,kc+1,oLanguage->getText("str_rdgmdi_log2"));
                if(statsBase[kc][0]==-9999)
                {
                    for(int ct=0;ct<6;ct++)
                        statsTable->setItemText(ct+1,kc+1,cMDIReadings::calculVal(-9999));
                }
                else
                {
                    statsTable->setItemText(1,kc+1,cMDIReadings::calculVal(statsBase[kc][0]));
                    statsTable->setItemText(2,kc+1,cMDIReadings::calculVal(statsBase[kc][1]));
                    armean=statsBase[kc][2]/statsBase[kc][4];
                    statsTable->setItemText(3,kc+1,cMDIReadings::calculVal(armean));
                    statsTable->setItemText(4,kc+1,cMDIReadings::calculVal(pow(10,statsBase[kc][3]/statsBase[kc][4])));
                    stdev=0;
                    for(int ct=0;ct<statsBase[kc][4];ct++)
                    {
                        stdev+=pow(statsValues[kc][ct]-armean,2);
                    }
                    stdev/=statsBase[kc][4];
                    stdev=sqrt(stdev);
                    statsTable->setItemText(5,kc+1,cMDIReadings::calculVal(stdev));
                    statsTable->setItemText(6,kc+1,cMDIReadings::calculVal(!armean?0:round(10*(100*stdev/armean))/10)+"%");
                }
                kc++;
            }
            kc++;
            if(kc<statsTable->getNumColumns())
                statsTable->removeColumns(kc,statsTable->getNumColumns()-kc);

            if(useGraphs)
            {
                if(!mcSingle)
                {
                    if(!rchart && (casesgi<tCasesCount-1))
                        rchart=chart;
                    else
                    {
                        if(!rchart && (casesgi>=tCasesCount-1))
                            repWin->addDoubleCharts(chart,NULL);
                        else
                            repWin->addDoubleCharts(rchart,chart);
                        rchart=NULL;
                    }
                }
                if(casesgi>=tCasesCount-1 && (!mcSingle))
                {
                    repWin->cancelTitleContinuum();
                    repWin->nextPage();
                }
            }
            else
                delete chart;

            sCCRSubObject *ccrso=NULL;
            if(mcSingle)
            {
                ccrso=new sCCRSubObject;
                ccrso->results=new FXString();
                ccrso->setsi=setsi;
                ccrso->casei=casei;
            }

            if(useData)
            {
                // platTable2

                stdict->insert(FXStringFormat("%d",casesgi).text(),statsTable);
                if(mcSingle)
                {
                    ccrso->stdicti=casesgi;
                    (*ccrso->results)=oLanguage->getText("str_rdgmdi_posct")+FXStringFormat(" %d, ",stp)+
                                   oLanguage->getText("str_rdgmdi_susct")+FXStringFormat(" %d, ",sts)+
                                   oLanguage->getText("str_rdgmdi_negct")+FXStringFormat(" %d, ",stn)+
                                   oLanguage->getText("str_rdgmdi_errct")+FXStringFormat(" %d",ste);
                }
                else
                {
                    resdict->insert(FXStringFormat("%d",casesgi).text(),
                                   (oLanguage->getText("str_rdgmdi_posct")+FXStringFormat(" %d, ",stp)+
                                   oLanguage->getText("str_rdgmdi_susct")+FXStringFormat(" %d, ",sts)+
                                   oLanguage->getText("str_rdgmdi_negct")+FXStringFormat(" %d, ",stn)+
                                   oLanguage->getText("str_rdgmdi_errct")+FXStringFormat(" %d",ste)).text());
                }
            }
            else
            {
                //delete plateTable2;
                delete statsTable;
            }
            if(mcSingle)
                sdentry->cases->insert(FXStringFormat("%d",sdentry->cases->no()).text(),ccrso);

            for(int ui=0;ui<5;ui++)
            {
                statsCount[ui]=0;
                free(statsValues[ui]);
            }
            p2_datai=1;
        }
        pd+=casesSets[setsi].platesCount;
        int mdi=datai%(96/__alelisa_multiplier);
        if(mdi)
            datai+=(96/__alelisa_multiplier)-mdi;
    }
    if(mcSingle && singled->no())
    {
        for(int asi=0;asi<singled->no();asi++)
        {
            sCCRObject *csd=(sCCRObject*)singled->find(singlek->find(FXStringFormat("%d",asi).text()));
            if(!csd)
                continue;
            if(useGraphs)
            {
                repWin->addTitle(oLanguage->getText("str_report_singleGr")+(baseline?(" - "+bslid):""));
                csd->chart->setClusterCount(csd->cases->no());
                repWin->addChart(csd->chart);
            }
            else
                delete csd->chart;
            if(useInfo || useData)
                for(int ci=0;ci<csd->cases->no();ci++)
                {
                    sCCRSubObject *c=(sCCRSubObject*)csd->cases->find(FXStringFormat("%d",ci).text());
                    if(!c)
                        continue;
                    int setsi=c->setsi;
                    int casei=c->casei;
                    int stdicti=c->stdicti;
                    if(ci==0)
                    {
                        if(useInfo)
                        {
                            repWin->addSubTitle(oLanguage->getText("str_report_kitInfo"));
                            repWin->addText(FXStringFormat("%s - %s, %s: %s, %s: %s,\n%s, %s: \"%s\", %s: \"%s\"",
                                                           casesSets[setsi].cases[casei].assay->text(),casesSets[setsi].assayRes->getCellString(0,1).text(),
                                                           oLanguage->getText("str_report_primary").text(),casesSets[setsi].cala->text(),
                                                           oLanguage->getText("str_report_secondary").text(),casesSets[setsi].useSecond?casesSets[setsi].calb->text():
                                                           oLanguage->getText("str_na").text(),
                                                           casesSets[setsi].useTiters?oLanguage->getText("str_report_useTiters").text():
                                                           oLanguage->getText("str_report_noTiters").text(),
                                                           oLanguage->getText("str_rdgmdi_assayLot").text(),casesSets[setsi].cases[casei].lot->text(),
                                                           oLanguage->getText("str_rdgmdi_assayExp").text(),casesSets[setsi].cases[casei].expirationDate->text()),
                                                           JUSTIFY_LEFT,true);
                        }
                        repWin->nextPage();
                        repWin->addTitle(oLanguage->getText("str_report_details")+(baseline?(" - "+bslid):""),0,true);
                    }
                    repWin->addText(" ");
                    repWin->addSubTitle("\n"+(*casesSets[setsi].cases[casei].id)+" - "+(*casesSets[setsi].cases[casei].population)+" - "+(*casesSets[setsi].cases[casei].assay)+" - "+(*casesSets[setsi].cases[casei].readingDate));
                    if(useInfo)
                    {
                        repWin->addTextBold(oLanguage->getText("str_report_caseInfo"));
                        repWin->addText(FXStringFormat("%d %s (%d %s), %s %d, %s \"%s\", %s %s, %s: %s, %s: %s,\n%s \"%s\", %s %s, %s \"%s\"%s",
                                         casesSets[setsi].cases[casei].count,oLanguage->getText("str_rdgmdi_samples").text(),
                                         casesSets[setsi].cases[casei].replicates,oLanguage->getText("str_rdgmdi_replicates").text(),
                                         oLanguage->getText("str_rdgmdi_factor").text(),casesSets[setsi].cases[casei].factor,
                                         oLanguage->getText("str_rdgmdi_age").text(),casesSets[setsi].cases[casei].age->text(),
                                         oLanguage->getText("str_rdgmdi_bldate").text(),casesSets[setsi].cases[casei].bleedDate->text(),
                                         oLanguage->getText("str_rdgmdi_reason").text(),casesSets[setsi].cases[casei].reason->text(),
                                         oLanguage->getText("str_rdgmdi_sptype").text(),casesSets[setsi].cases[casei].spType->text(),
                                         oLanguage->getText("str_rdgmdi_vet").text(),casesSets[setsi].cases[casei].veterinarian->text(),
                                         oLanguage->getText("str_rdgmdi_readingDate").text(),casesSets[setsi].cases[casei].readingDate->text(),
                                         oLanguage->getText("str_rdgmdi_tech").text(),casesSets[setsi].cases[casei].technician->text(),
                                         (casesSets[setsi].cases[casei].comments->empty()?"":
                                          FXStringFormat("\n%s: %s",oLanguage->getText("str_rdgmdi_comments").text(),
                                                         casesSets[setsi].cases[casei].comments->text()).text()))
                                                   ,JUSTIFY_LEFT,true);

                        repWin->addTextBold(oLanguage->getText("str_report_popInfo"));
                        res=cPopulationManager::getPopulation(*casesSets[setsi].cases[casei].population);
                        repWin->addText(FXStringFormat("%s %s, %s %s, %s \"%s\", %s \"%s\", %s \"%s\",\n%s \"%s\", %s \"%s\",%s \"%s\", %s \"%s\", %s \"%s\"",
                                                       oLanguage->getText("str_report_pop").text(),casesSets[setsi].cases[casei].population->text(),
                                                       oLanguage->getText("str_dlg_pop_brdate").lower().text(),res->getCellString(0,2).text(),
                                                       oLanguage->getText("str_dlg_pop_breed1").lower().text(),res->getCellString(0,3).text(),
                                                       oLanguage->getText("str_dlg_pop_breed2").lower().text(),res->getCellString(0,4).text(),
                                                       oLanguage->getText("str_dlg_pop_vaccine").lower().text(),res->getCellString(0,7).text(),
                                                       oLanguage->getText("str_dlg_pop_owner").lower().text(),res->getCellString(0,5).text(),
                                                       oLanguage->getText("str_dlg_pop_grower").lower().text(),res->getCellString(0,6).text(),
                                                       oLanguage->getText("str_dlg_pop_unit").lower().text(),res->getCellString(0,8).text(),
                                                       oLanguage->getText("str_dlg_pop_location").lower().text(),res->getCellString(0,9).text(),
                                                       oLanguage->getText("str_dlg_pop_sublocation").lower().text(),res->getCellString(0,10).text()),
                                                       JUSTIFY_LEFT,true);
                        delete res;

                    }

                    if(useData && stdict->no()>stdicti)
                    {
                        //repWin->addSubTitle(oLanguage->getText("str_report_caseData"));
                        //repWin->addTable(plateTable2,true);

                        statsTable=(cColorTable*)stdict->find(FXStringFormat("%d",stdicti).text());
                        if(statsTable && statsTable->getNumColumns()>1)
                        {
                            repWin->addTextBold(oLanguage->getText("str_report_caseStats"));
                            repWin->addTable(statsTable,true);
                        }
                        repWin->addTextBold(oLanguage->getText("str_report_caseResults"));
                        repWin->addText(c->results->text(),JUSTIFY_LEFT,true);
                    }
                    delete c->results;
                }
                delete csd->cases;
                repWin->cancelTitleContinuum();
                repWin->nextPage();
        }
    }
    else
    {
        if(useInfo || useData)
        {
            repWin->addTitle(oLanguage->getText("str_report_details")+(baseline?(" - "+bslid):""),0,true);
            casesgi=0;
            for(int setsi=0;setsi<tSetsCount;setsi++)
            {
                for(int casei=0;casei<casesSets[setsi].casesCount;casei++,casesgi++)
                {
                    repWin->addText(" ");
                    repWin->addSubTitle((*casesSets[setsi].cases[casei].id)+" - "+(*casesSets[setsi].cases[casei].population)+" - "+(*casesSets[setsi].cases[casei].assay)+" - "+(*casesSets[setsi].cases[casei].readingDate));
                    if(useInfo)
                    {
                        repWin->addTextBold(oLanguage->getText("str_report_caseInfo"));
                        repWin->addText(FXStringFormat("%d %s (%d %s), %s %d, %s \"%s\", %s %s, %s: %s, %s: %s,\n%s \"%s\", %s %s, %s \"%s\"%s",
                                         casesSets[setsi].cases[casei].count,oLanguage->getText("str_rdgmdi_samples").text(),
                                         casesSets[setsi].cases[casei].replicates,oLanguage->getText("str_rdgmdi_replicates").text(),
                                         oLanguage->getText("str_rdgmdi_factor").text(),casesSets[setsi].cases[casei].factor,
                                         oLanguage->getText("str_rdgmdi_age").text(),casesSets[setsi].cases[casei].age->text(),
                                         oLanguage->getText("str_rdgmdi_bldate").text(),casesSets[setsi].cases[casei].bleedDate->text(),
                                         oLanguage->getText("str_rdgmdi_reason").text(),casesSets[setsi].cases[casei].reason->text(),
                                         oLanguage->getText("str_rdgmdi_sptype").text(),casesSets[setsi].cases[casei].spType->text(),
                                         oLanguage->getText("str_rdgmdi_vet").text(),casesSets[setsi].cases[casei].veterinarian->text(),
                                         oLanguage->getText("str_rdgmdi_readingDate").text(),casesSets[setsi].cases[casei].readingDate->text(),
                                         oLanguage->getText("str_rdgmdi_tech").text(),casesSets[setsi].cases[casei].technician->text(),
                                         (casesSets[setsi].cases[casei].comments->empty()?"":
                                          FXStringFormat("\n%s: %s",oLanguage->getText("str_rdgmdi_comments").text(),
                                                         casesSets[setsi].cases[casei].comments->text()).text()))
                                                   ,JUSTIFY_LEFT,true);

                        repWin->addTextBold(oLanguage->getText("str_report_popInfo"));
                        res=cPopulationManager::getPopulation(*casesSets[setsi].cases[casei].population);
                        repWin->addText(FXStringFormat("%s %s, %s %s, %s \"%s\", %s \"%s\", %s \"%s\",\n%s \"%s\", %s \"%s\",%s \"%s\", %s \"%s\", %s \"%s\"",
                                                       oLanguage->getText("str_report_pop").text(),casesSets[setsi].cases[casei].population->text(),
                                                       oLanguage->getText("str_dlg_pop_brdate").lower().text(),res->getCellString(0,2).text(),
                                                       oLanguage->getText("str_dlg_pop_breed1").lower().text(),res->getCellString(0,3).text(),
                                                       oLanguage->getText("str_dlg_pop_breed2").lower().text(),res->getCellString(0,4).text(),
                                                       oLanguage->getText("str_dlg_pop_vaccine").lower().text(),res->getCellString(0,7).text(),
                                                       oLanguage->getText("str_dlg_pop_owner").lower().text(),res->getCellString(0,5).text(),
                                                       oLanguage->getText("str_dlg_pop_grower").lower().text(),res->getCellString(0,6).text(),
                                                       oLanguage->getText("str_dlg_pop_unit").lower().text(),res->getCellString(0,8).text(),
                                                       oLanguage->getText("str_dlg_pop_location").lower().text(),res->getCellString(0,9).text(),
                                                       oLanguage->getText("str_dlg_pop_sublocation").lower().text(),res->getCellString(0,10).text()),
                                                       JUSTIFY_LEFT,true);
                        delete res;

                        repWin->addTextBold(oLanguage->getText("str_report_kitInfo"));
                        repWin->addText(FXStringFormat("%s - %s, %s: %s, %s: %s,\n%s, %s: \"%s\", %s: \"%s\"",
                                                       casesSets[setsi].cases[casei].assay->text(),casesSets[setsi].assayRes->getCellString(0,1).text(),
                                                       oLanguage->getText("str_report_primary").text(),casesSets[setsi].cala->text(),
                                                       oLanguage->getText("str_report_secondary").text(),casesSets[setsi].useSecond?casesSets[setsi].calb->text():
                                                       oLanguage->getText("str_na").text(),
                                                       casesSets[setsi].useTiters?oLanguage->getText("str_report_useTiters").text():
                                                       oLanguage->getText("str_report_noTiters").text(),
                                                       oLanguage->getText("str_rdgmdi_assayLot").text(),casesSets[setsi].cases[casei].lot->text(),
                                                       oLanguage->getText("str_rdgmdi_assayExp").text(),casesSets[setsi].cases[casei].expirationDate->text()),
                                                       JUSTIFY_LEFT,true);
                    }

                    if(useData && stdict->no()>casesgi)
                    {
                        //repWin->addSubTitle(oLanguage->getText("str_report_caseData"));
                        //repWin->addTable(plateTable2,true);

                        statsTable=(cColorTable*)stdict->find(FXStringFormat("%d",casesgi).text());
                        if(statsTable && statsTable->getNumColumns()>1)
                        {
                            repWin->addTextBold(oLanguage->getText("str_report_caseStats"));
                            repWin->addTable(statsTable,true);
                        }
                        repWin->addTextBold(oLanguage->getText("str_report_caseResults"));
                        repWin->addText(resdict->find(FXStringFormat("%d",casesgi).text()),JUSTIFY_LEFT,true);
                    }
                }
            }
        }
        if(tCasesCount>1)
            repWin->nextPage();
        else
            repWin->addText(" ");
    }
    repWin->addComments();
    delete stdict;
    delete resdict;
    delete singled;
    delete singlek;
    oApplicationManager->endWaitCursor();
    msg.hide();
    return true;
}

FXbool cCaseCompareReport::askPrefs(void)
{
    dlg=new cDLGReportCC(oWinMain,areTiters,areSecondary,areBins,areSamples,areOnlyTs);
    if(!dlg->execute(PLACEMENT_OWNER))
        return false;

    mcCala=dlg->bcCala->getCheck();
    mcCalb=areSecondary?dlg->bcCalb->getCheck():false;
    mcTiter=areTiters?dlg->bcTiter->getCheck():false;
    //mcTiters=areTiters?dlg->bcTiters->getCheck():false;
    //mcBins=areBins?dlg->bcBins->getCheck():false;
    //mcResult=dlg->bcResult->getCheck();
    //mcResult2=areSecondary?dlg->bcResult2->getCheck():false;
    //mcBins2=(areBins && areSecondary)?dlg->bcBins2->getCheck():false;
    mcAm=dlg->bcAm->getCheck();
    //mcAge=dlg->bcAge->getCheck();
    mcSingle=dlg->bcSingle->getCheck();
    mcLog2=areTiters?dlg->bcLog2->getCheck():false;
    mcEU=dlg->bcEU->getCheck();
    //mcBreed1=dlg->bcBreed1->getCheck();
    //mcBreed2=dlg->bcBreed2->getCheck();
    useGraphs=dlg->bUseGraphs->getCheck();
    useData=dlg->bUseData->getCheck();
    useInfo=dlg->bUseInfo->getCheck();

    baseline=useGraphs && dlg->bsl && (dlg->bsl->getCurrentItem()>0);
    bslss=false;
    bslid="";
    if(baseline)
    {
        bslss=dlg->bSS->getCheck();
        bslid=dlg->bsl->getItemText(dlg->bsl->getCurrentItem());
    }

    graphType=dlg->choice-1;

    return true;
}

void cCaseCompareReport::openCases(FXMDIClient *prP, FXPopup *prPup,cSortList &prCases)
{
    oCaseCompareReport=new cCaseCompareReport();

    oCaseCompareReport->repWin=new cMDIReport(prP,oLanguage->getText("str_report_cc"),new FXGIFIcon(oApplicationManager,data_reports_cca),prPup,MDI_NORMAL,0,0,400,300);
    oCaseCompareReport->repWin->setWidth(770);
    oCaseCompareReport->repWin->setHeight(520);
    //oCaseCompareReport->repWin->maximize();
    oCaseCompareReport->repWin->hide();
    oCaseCompareReport->repWin->create();
    oCaseCompareReport->repWin->setFocus();
    oCaseCompareReport->repWin->setTitle(oLanguage->getText("str_report_cc"));
    oCaseCompareReport->repWin->hide();

    oCaseCompareReport->graphType=0;
    oCaseCompareReport->useGraphs=true;
    oCaseCompareReport->useData=true;
    oCaseCompareReport->useInfo=true;

    if(!oCaseCompareReport->loadReadings(prCases) || !oCaseCompareReport->askPrefs() || !oCaseCompareReport->processReadings())
    {
        delete oCaseCompareReport->repWin;
        return;
    }

    oCaseCompareReport->repWin->recalc();
    oCaseCompareReport->repWin->show();
}


