#include "engine.h"
#include "cDataLink.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cMDIGrower.h"
#include "cGrowerManager.h"

FXbool cGrowerManager::growerSolid(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_growers WHERE id='"+ress.trim().substitute("'","")+"' AND solid=1;");
    return (res->getRowCount()>0) || (prId=="---");
}

FXbool cGrowerManager::growerExists(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_growers WHERE id='"+ress.trim().substitute("'","")+"';");
    return (res->getRowCount()>0) || (prId=="---");
}

FXbool cGrowerManager::growerNeeded(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_gnpopulations WHERE grower_oid='"+ress.trim().substitute("'","")+"';");
    return res->getRowCount()>0;
}

FXString cGrowerManager::getNewID(void)
{
    static int grwNr=oApplicationManager->reg().readIntEntry("oids","growers",1);
    FXString res=oLanguage->getText("str_grwmdi_grwtitle");
    FXString ret=res+FXStringFormat(" #%d",++grwNr);
    while(growerExists(ret))
        ret=res+FXStringFormat(" #%d",++grwNr);
    oApplicationManager->reg().writeIntEntry("oids","growers",grwNr);
    return ret;
}

int cGrowerManager::getGrowerCount(void)
{
    if(!oDataLink->isOpened())
        return 0;
    cDataResult *res=oDataLink->execute("SELECT COUNT(id) FROM t_growers;");
    return res->getCellInt(0,0);
}

sGrowerObject *cGrowerManager::listGrowers()
{
    if(!oDataLink->isOpened())
        return NULL;

    sGrowerObject *ret=(sGrowerObject*)malloc(getGrowerCount()*sizeof(sGrowerObject));
    if(!ret)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }
    cDataResult *res=oDataLink->execute(("SELECT id,ctperson,solid FROM t_growers;"));
    for(int i=0;i<res->getRowCount();i++)
    {
        ret[i].id=new FXString(res->getCellString(i,0));
        ret[i].title=new FXString(res->getCellString(i,1));
        ret[i].solid=res->getCellInt(i,2);
    }
    return ret;
}

cDataResult *cGrowerManager::getGrower(const FXString &prId)
{
    FXString res=prId;
    return oDataLink->execute("SELECT * FROM t_growers WHERE id='"+res.trim().substitute("'","")+"';");
}

FXString cGrowerManager::newGrower(FXMDIClient *prP, FXPopup *prPup)
{
    FXString ret=getNewID();
    cMDIGrower *grwWin=new cMDIGrower(prP,ret,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,300,570);
    grwWin->create();
    grwWin->setFocus();
    return ret;
}

FXString cGrowerManager::editGrower(FXMDIClient *prP, FXPopup *prPup,const FXString &prId)
{
    cMDIGrower *grwWin=new cMDIGrower(prP,prId,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,300,570);
    grwWin->create();
    grwWin->loadGrower(prId);
    grwWin->setFocus();
    return prId;
}

FXString cGrowerManager::duplicateGrower(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return "";
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT * FROM t_growers WHERE id='"+ress.trim().substitute("'","")+"';");
    if(res->getRowCount()<=0)
        return "";
    int i=1;
    FXString ret=res->getCellString(0,0);
    while(growerExists(ret))
        ret=res->getCellString(0,0)+FXStringFormat(" #%d",i++);
    FXString res2="INSERT INTO t_growers VALUES('"+ret.trim().substitute("'","")+"'";
    for(i=1;i<res->getFieldCount();i++)
        res2=res2+",'"+res->getCellString(0,i).trim().substitute("'","")+"'";
    res2=res2+");";
    res=oDataLink->execute(res2);
    return ret;
}

FXbool cGrowerManager::addGrower(FXString prId,FXString prCtp,FXString prAdd,FXString prCity,FXString prState,FXString prZip,FXString prPhone,FXString prFax,FXString prEmail,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("INSERT INTO t_growers VALUES('"+
                                                    prId.trim().substitute("'","")+"','"+
                                                    prCtp.trim().substitute("'","")+"','"+
                                                    "0','"+
                                                    prAdd.trim().substitute("'","")+"','"+
                                                    prCity.trim().substitute("'","")+"','"+
                                                    prState.trim().substitute("'","")+"','"+
                                                    prZip.trim().substitute("'","")+"','"+
                                                    prPhone.trim().substitute("'","")+"','"+
                                                    prFax.trim().substitute("'","")+"','"+
                                                    prEmail.trim().substitute("'","")+"','"+
                                                    prComments.trim().substitute("'","")+"');");
    return (oDataLink->getAffectedRowCount()==1);
}

FXbool cGrowerManager::setGrower(FXString prOldId,FXString prId,FXString prCtp,FXString prAdd,FXString prCity,FXString prState,FXString prZip,FXString prPhone,FXString prFax,FXString prEmail,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("UPDATE t_growers SET id='"+prId.trim().substitute("'","")+
                       "', ctperson='"+prCtp.trim().substitute("'","")+
                       "', solid='0"+
                       "', address='"+prAdd.trim().substitute("'","")+
                       "', city='"+prCity.trim().substitute("'","")+
                       "', state='"+prState.trim().substitute("'","")+
                       "', zip='"+prZip.trim().substitute("'","")+
                       "', phone='"+prPhone.trim().substitute("'","")+
                       "', fax='"+prFax.trim().substitute("'","")+
                       "', email='"+prEmail.trim().substitute("'","")+
                       "', comments='"+prComments.trim().substitute("'","")+
                       "' WHERE id='"+prOldId.trim().substitute("'","")+"' AND solid='0';");
    int ret=oDataLink->getAffectedRowCount();
    oDataLink->execute("UPDATE t_gnpopulations SET grower_oid='"+prId.trim().substitute("'","")+"' WHERE grower_oid='"+prOldId+"';");
    return (ret==1);
}

FXbool cGrowerManager::removeGrower(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT oid FROM t_growers WHERE id='"+ress.trim().substitute("'","")+"' AND solid='0';");
    if(!res || res->getRowCount()==0)
        return false;
    res=oDataLink->execute("DELETE FROM t_growers WHERE id='"+ress.trim().substitute("'","")+"' AND solid='0';");
    return (res!=NULL && oDataLink->getAffectedRowCount()>0);
}


