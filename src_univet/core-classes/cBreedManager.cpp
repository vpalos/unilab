#include "engine.h"
#include "cDataLink.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cMDIBreed.h"
#include "cBreedManager.h"

FXbool cBreedManager::breedSolid(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_breeds WHERE id='"+ress.trim().substitute("'","")+"' AND solid=1;");
    return (res->getRowCount()>0) || (prId=="---");
}

FXbool cBreedManager::breedExists(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_breeds WHERE id='"+ress.trim().substitute("'","")+"';");
    return (res->getRowCount()>0) || (prId=="---");
}

FXbool cBreedManager::breedNeeded(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_gnpopulations WHERE breed1='"+ress.trim().substitute("'","")+"' OR breed2='"+ress+"';");
    return res->getRowCount()>0;
}

FXString cBreedManager::getNewID(void)
{
    static int breNr=oApplicationManager->reg().readIntEntry("oids","breeds",1);
    FXString res=oLanguage->getText("str_bremdi_bretitle");
    FXString ret=res+FXStringFormat(" #%d",++breNr);
    while(breedExists(ret))
        ret=res+FXStringFormat(" #%d",++breNr);
    oApplicationManager->reg().writeIntEntry("oids","breeds",breNr);
    return ret;
}

int cBreedManager::getBreedCount(void)
{
    if(!oDataLink->isOpened())
        return 0;
    cDataResult *res=oDataLink->execute("SELECT COUNT(id) FROM t_breeds;");
    return res->getCellInt(0,0);
}

sBreedObject *cBreedManager::listBreeds()
{
    if(!oDataLink->isOpened())
        return NULL;

    sBreedObject *ret=(sBreedObject*)malloc(getBreedCount()*sizeof(sBreedObject));
    if(!ret)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }
    cDataResult *res=oDataLink->execute(("SELECT id,title,solid FROM t_breeds;"));
    for(int i=0;i<res->getRowCount();i++)
    {
        ret[i].id=new FXString(res->getCellString(i,0));
        ret[i].title=new FXString(res->getCellString(i,1));
        ret[i].solid=res->getCellInt(i,2);
    }
    return ret;
}

cDataResult *cBreedManager::getBreed(const FXString &prId)
{
    FXString res=prId;
    return oDataLink->execute("SELECT * FROM t_breeds WHERE id='"+res.trim().substitute("'","")+"';");
}

FXString cBreedManager::newBreed(FXMDIClient *prP, FXPopup *prPup)
{
    FXString ret=getNewID();
    cMDIBreed *breWin=new cMDIBreed(prP,ret,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,300,240);
    breWin->create();
    breWin->setFocus();
    return ret;
}

FXString cBreedManager::editBreed(FXMDIClient *prP, FXPopup *prPup,const FXString &prId)
{
    cMDIBreed *breWin=new cMDIBreed(prP,prId,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,300,240);
    breWin->create();
    breWin->loadBreed(prId);
    breWin->setFocus();
    return prId;
}

FXString cBreedManager::duplicateBreed(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return "";
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT * FROM t_breeds WHERE id='"+ress.trim().substitute("'","")+"';");
    if(res->getRowCount()<=0)
        return "";
    int i=1;
    FXString ret=res->getCellString(0,0);
    while(breedExists(ret))
        ret=res->getCellString(0,0)+FXStringFormat(" #%d",i++);
    FXString res2="INSERT INTO t_breeds VALUES('"+ret.trim().substitute("'","")+"'";
    for(i=1;i<res->getFieldCount();i++)
        res2=res2+",'"+res->getCellString(0,i).trim().substitute("'","")+"'";
    res2=res2+");";
    res=oDataLink->execute(res2);
    return ret;
}

FXbool cBreedManager::addBreed(FXString prId,FXString prTitle,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("INSERT INTO t_breeds VALUES('"+
                                                    prId.trim().substitute("'","")+"','"+
                                                    prTitle.trim().substitute("'","")+"','"+
                                                    "0','"+
                                                    prComments.trim().substitute("'","")+"');");
    return (oDataLink->getAffectedRowCount()==1);
}

FXbool cBreedManager::setBreed(FXString prOldId,FXString prId,FXString prTitle,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("UPDATE t_breeds SET id='"+prId.trim().substitute("'","")+
                       "', title='"+prTitle.trim().substitute("'","")+
                       "', solid='0"+
                       "', comments='"+prComments.trim().substitute("'","")+
                       "' WHERE id='"+prOldId.trim().substitute("'","")+"' AND solid='0';");
    int ret=oDataLink->getAffectedRowCount();
    oDataLink->execute("UPDATE t_gnpopulations SET breed1='"+prId.trim().substitute("'","")+"' WHERE breed1='"+prOldId+"';");
    oDataLink->execute("UPDATE t_gnpopulations SET breed2='"+prId.trim().substitute("'","")+"' WHERE breed2='"+prOldId+"';");
    return (ret==1);
}

FXbool cBreedManager::removeBreed(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT oid FROM t_breeds WHERE id='"+ress.trim().substitute("'","")+"' AND solid='0';");
    if(!res || res->getRowCount()==0)
        return false;
    res=oDataLink->execute("DELETE FROM t_breeds WHERE id='"+ress.trim().substitute("'","")+"' AND solid='0';");
    return (res!=NULL && oDataLink->getAffectedRowCount()>0);
}


