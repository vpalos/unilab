#include <ctype.h>
#include <stdio.h>
#include <strings.h>

#include "engine.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cDataLink.h"
#include "cTemplateManager.h"
#include "cMDITemplate.h"
#include "cMDIReadings.h"
#include "cMDIDatabaseManager.h"

int cTemplateManager::getTemplateCount(void)
{
    if(!oDataLink->isOpened())
        return 0;
    cDataResult *res=oDataLink->execute("SELECT COUNT(id) FROM t_templates;");
    return res->getCellInt(0,0);
}

sTemplateObject *cTemplateManager::listTemplates(void)
{
    if(!oDataLink->isOpened())
        return NULL;

    sTemplateObject *ret=(sTemplateObject*)malloc(getTemplateCount()*sizeof(sTemplateObject));
    if(!ret)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }
    cDataResult *res=oDataLink->execute("SELECT id,date FROM t_templates;");
    for(int i=0;i<res->getRowCount();i++)
    {
        ret[i].title=new FXString(res->getCellString(i,0));
        ret[i].date=new FXString(res->getCellString(i,1));
    }
    return ret;
}

FXbool cTemplateManager::templateExists(const FXString &prTitle)
{
    if(!oDataLink->isOpened())
        return false;
    cDataResult *res=oDataLink->execute("SELECT date FROM t_templates WHERE id='"+prTitle+"';");
    return res->getRowCount()>0;
}

FXString cTemplateManager::getNewID(void)
{
    static int tplNr=oApplicationManager->reg().readIntEntry("oids","templates",1);
    FXString res=oLanguage->getText("str_tplmdi_title");
    FXString ret=res+FXStringFormat(" #%d",++tplNr);
    while(templateExists(ret))
        ret=res+FXStringFormat(" #%d",++tplNr);
    oApplicationManager->reg().writeIntEntry("oids","templates",tplNr);
    return ret;
}

FXString cTemplateManager::newTemplate(FXMDIClient *prP, FXPopup *prPup)
{
    if(!oDataLink->isOpened())
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_nodb").text());
        cMDIDatabaseManager::load(prP,prPup);
        return "";
    }
    
    FXString ret=getNewID();
    cMDITemplate *tplWin=new cMDITemplate(prP,ret,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,678,507);
    tplWin->create();
    tplWin->setFocus();
    return ret;
}

FXbool cTemplateManager::addTemplate(FXString prId,FXString prDate,FXint prOrientation,FXint prAlelisa,FXString prControlsDefs,FXString prCaseDefs)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("INSERT INTO t_templates VALUES('"+prId.trim().substitute("'","")+"','"+prDate.trim().substitute("'","")+"',"+FXStringVal(prOrientation)+","+FXStringVal(prAlelisa)+",'"+prControlsDefs.trim().substitute("'","")+"','"+prCaseDefs.trim().substitute("'","")+"');");
    return (oDataLink->getAffectedRowCount()==1);
}

FXbool cTemplateManager::setTemplate(FXString prOldId,FXString prId,FXString prDate,FXint prOrientation,FXint prAlelisa,FXString prControlsDefs,FXString prCaseDefs)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("UPDATE t_templates SET id='"+prId.trim().substitute("'","")+"', date='"+prDate.trim().substitute("'","")+"', orientation="+FXStringVal(prOrientation)+", alelisa="+FXStringVal(prAlelisa)+", controls_defs='"+prControlsDefs.trim().substitute("'","")+"', cases_defs='"+prCaseDefs.trim().substitute("'","")+"' WHERE id='"+prOldId.trim().substitute("'","")+"';");
    int ret=oDataLink->getAffectedRowCount();
    oDataLink->execute("UPDATE t_gncases SET template_oid='"+prId.trim().substitute("'","")+"' WHERE template_oid='"+prOldId+"';");
    sCDLGO *dt=new sCDLGO;
    dt->id=new FXString(prOldId);
    dt->id2=new FXString(prId);

    for(int i=oWinMain->getReadings()->first();i<=oWinMain->getReadings()->last();i=oWinMain->getReadings()->next(i))
    {
        cMDIReadings *w=(cMDIReadings*)oWinMain->getReadings()->data(i);
        if(w)
        {
            w->onRefreshTpl(NULL,0,dt);
        }
    }
    delete dt->id;
    delete dt->id2;
    delete dt;
    return (ret==1);
}

cDataResult *cTemplateManager::getTemplate(FXString prId)
{
    return oDataLink->execute("SELECT * FROM t_templates WHERE id='"+prId.trim().substitute("'","")+"';");
}

FXString cTemplateManager::editTemplate(FXMDIClient *prP, FXPopup *prPup,const FXString &prTemplate)
{
    cMDITemplate *tplWin=new cMDITemplate(prP,prTemplate,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,678,507);
    tplWin->create();
    tplWin->loadTemplate(prTemplate);
    tplWin->setFocus();
    return prTemplate;
}

FXString cTemplateManager::duplicateTemplate(const FXString &prTemplate)
{
    if(!oDataLink->isOpened())
        return "";
    cDataResult *res=oDataLink->execute("SELECT * FROM t_templates WHERE id='"+prTemplate+"';");
    if(res->getRowCount()<=0)
        return "";
    int i=1;
    FXString ret=res->getCellString(0,0);
    while(templateExists(ret))
        ret=res->getCellString(0,0)+FXStringFormat(" #%d",i++);
    FXString res2="INSERT INTO t_templates VALUES('"+ret.trim().substitute("'","")+"'";
    for(i=1;i<res->getFieldCount();i++)
        res2=res2+",'"+res->getCellString(0,i).trim().substitute("'","")+"'";
    res2=res2+");";
    res=oDataLink->execute(res2);
    return ret;
}

FXbool cTemplateManager::removeTemplate(const FXString &prTemplate)
{
    if(!oDataLink->isOpened())
        return false;
    return (oDataLink->execute("DELETE FROM t_templates WHERE id='"+prTemplate+"';")!=NULL);
}

FXint cTemplateManager::getWellCount(const FXString &prTemplate)
{
    cDataResult *res=getTemplate(prTemplate);
    if(res==NULL)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_opentpl").text());
        return 0;
    }
    
    int ret=0,c,r,alelisa=res->getCellInt(0,3);
    FXString cases_defs=res->getCellString(0,5);
    int limita=FXIntVal(cases_defs.before('\n'));
    for(int i=0;i<limita;i++)
    {
        cases_defs=cases_defs.after('\n');
        
        cases_defs.before('\t');
        cases_defs=cases_defs.after('\t');
        c=FXIntVal(cases_defs.before('\t'));
        cases_defs=cases_defs.after('\t');
        cases_defs.before('\t');
        cases_defs=cases_defs.after('\t');
        cases_defs.before('\t');
        cases_defs=cases_defs.after('\t');
        cases_defs.before('\t');
        cases_defs=cases_defs.after('\t');
        r=FXIntVal(cases_defs.before('\t'));
        cases_defs=cases_defs.after('\t');
        ret+=(1+r)*c;
    }
    res->free();
    ret*=alelisa;
    return ret;
}

FXint cTemplateManager::getControlCount(const FXString &prTemplate)
{
    FXint ret=0;

    cDataResult *res=getTemplate(prTemplate);
    if(res==NULL)
    {
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_opentpl").text());
        return 0;
    }
    
    int c,r,alelisa=res->getCellInt(0,3);
    FXString controls_defs=res->getCellString(0,4);
    
    c=FXIntVal(controls_defs.before('\n'));
    switch(c)
    {
        case 1:
            ret=4;
            break;
        case 2:
            ret=6;
            break;
        case 3:
            controls_defs=controls_defs.after('\n');
            r=0;
            do{
                r=FXIntVal(controls_defs.before('\t'));
                controls_defs=controls_defs.after('\t');
                if(r!=0 && r!=-100)
                    ret++;
            }while(r!=0 && r!=-100);
            break;
        default:
            break;
    }
    ret*=alelisa;
    return ret;
}

FXint cTemplateManager::getPlateCount(const FXString &prTemplate)
{
    int cc=getControlCount(prTemplate),wc=getWellCount(prTemplate);
    return (wc/(96-cc)+(wc%(96-cc)>0?1:0));
}


