#include <strings.h>

#include "engine.h"
#include "graphics.h"
#include "cSpeciesManager.h"
#include "cLanguage.h"
#include "cWinMain.h"

#define SPECIES_COUNT 6

int cSpeciesManager::getSpeciesCount(void)
{
    return SPECIES_COUNT;
}

sSpeciesObject *cSpeciesManager::getSpeciesList(void)
{
    sSpeciesObject *ret=(sSpeciesObject*)malloc(SPECIES_COUNT*sizeof(sSpeciesObject));
    if(!ret)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }

    int i=0;
    /*ret[i].name=new FXString("Plants");
    ret[i].image_data=(unsigned char*)data_species_plants;
    ret[i].age_format=7;

    i++;*/
    ret[i].name=new FXString("Poultry");
    ret[i].image_data=(unsigned char*)data_species_poultry;
    ret[i].age_format=7;

    i++;
    ret[i].name=new FXString("Bovine");
    ret[i].image_data=(unsigned char*)data_species_bovine;
    ret[i].age_format=5;

    i++;
    ret[i].name=new FXString("Swine");
    ret[i].image_data=(unsigned char*)data_species_swine;
    ret[i].age_format=5;

    i++;
    ret[i].name=new FXString("Equine");
    ret[i].image_data=(unsigned char*)data_species_equine;
    ret[i].age_format=5;

    i++;
    ret[i].name=new FXString("Ovine");
    ret[i].image_data=(unsigned char*)data_species_ovine;
    ret[i].age_format=5;

    i++;
    ret[i].name=new FXString("Others");
    ret[i].image_data=(unsigned char*)data_species_others;
    ret[i].age_format=5;

    return ret;
}

FXString cSpeciesManager::getSpeciesSqlBase(const FXString &prSpecies)
{
    sSpeciesObject *res=getSpeciesList();
    FXString ret="";
    int i,pos=0;
    if(res)
    {

        for(i=0;i<getSpeciesCount();i++)
            if(comparecase(*(res[i].name),prSpecies)==0)
            {
                pos=i;
                break;
            }
        free(res);

        switch(pos)
        {
            case 0: // poultry
                ret="";
                break;
            case 1: // bovine
                ret="";
                break;
            case 2: // swine
                ret="";
                break;
            case 3: // equine
                ret="";
                break;
            case 4: // ovine
                ret="";
                break;
            case 5: // others
                ret="";
                break;
        }
    }
    return ret;
}

int cSpeciesManager::getAgeFormat(const FXString &prTitle)
{
    sSpeciesObject *res=getSpeciesList();
    FXint ret=7;
    if(res)
    {

        for(int i=0;i<getSpeciesCount();i++)
            if(comparecase(*(res[i].name),prTitle)==0)
            {
                ret=res[i].age_format;
                break;
            }
        free(res);
    }
    return ret;
}

