#ifndef CCASEANALYSISREPORT_H
#define CCASEANALYSISREPORT_H

#include <fx.h>
#include "cDLGReportCA.h"
#include "cMDIReadings.h"
#include "cMDIReport.h"
#include "cSortList.h"

class cCaseAnalysisReport
{
    private:
        cDLGReportCA *dlg;
        cMDIReport *repWin;
        cColorTable *plateTable2;
        cColorTable *statsTable;
    
        FXbool mcCala;
        FXbool mcCalb;
        FXbool mcTiter;
        FXbool mcTiters;
        FXbool mcBins;
        FXbool mcResult;
        FXbool mcResult2;
        //FXbool mcBins2;
        FXbool mcAm;
        FXbool mcAge;
        FXbool mcLog2;
        FXbool mcEU;
        FXbool mcBreed1;
        FXbool mcBreed2;
        
        FXint graphType;
        FXbool useGraphs;
        FXbool useData;
        FXbool usePlate;
        FXbool useInfo;

        FXbool mcPCala;
        
        sCasesSet *casesSets;
        int tSetsCount;
        int tCasesCount;
        
        bool areTiters;
        bool areOnlyTs;
        bool areBins;
        bool areSecondary;
        
        FXbool baseline;
        FXString bslid;
        FXbool bslss;
        
    protected:
        FXbool loadReadings(cSortList &prCases);
        FXbool processReadings(void);
        FXbool askPrefs(void);
        
    public:
        static void openCases(FXMDIClient *prP, FXPopup *prPup,cSortList &prCases);
};

#endif
