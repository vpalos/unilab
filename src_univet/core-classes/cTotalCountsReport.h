#ifndef CTOTALCOUNTSREPORT_H
#define CTOTALCOUNTSREPORT_H

#include <fx.h>
#include "cVChart.h"
#include "cColorTable.h"
#include "cDLGReportTC.h"
#include "cMDIReadings.h"
#include "cMDIReport.h"
#include "cSortList.h"

class cTotalCountsReport
{
    private:
        cDLGReportTC *dlg;
        cMDIReport *repWin;
        cColorTable *plateTable2;
        cColorTable *statsTable;
    
        FXbool mcAm;
        FXint sType;
        
        sCasesSet *casesSets;
        int tSetsCount;
        int tCasesCount;
        
    protected:
        FXbool loadReadings(cSortList &prCases);
        FXbool processReadings(void);
        FXbool askPrefs(void);
        
    public:
        static void openCases(FXMDIClient *prP, FXPopup *prPup,cSortList &prCases);
};

#endif
