#ifndef CBREEDMANAGER_H
#define CBREEDMANAGER_H

#include <fx.h>

typedef struct sBreedObject
{
    FXString *id;
    FXString *title;
    int solid;
};

class cBreedManager
{
    private:
    protected:
    public:
        static FXbool breedSolid(const FXString &prId);
        static FXbool breedExists(const FXString &prId);
        static FXbool breedNeeded(const FXString &prId);
        static FXString getNewID(void);
        static int getBreedCount(void);
        
        static sBreedObject *listBreeds(void);
        static cDataResult *getBreed(const FXString &prId);
        
        static FXString newBreed(FXMDIClient *prP, FXPopup *prPup);
        static FXString editBreed(FXMDIClient *prP, FXPopup *prPup,const FXString &prId);
        static FXString duplicateBreed(const FXString &prId);
        
        static FXbool addBreed(FXString prId,FXString prTitle,FXString prComments);
        static FXbool setBreed(FXString prOldId,FXString prId,FXString prTitle,FXString prComments);
        static FXbool removeBreed(const FXString &prId);
};

#endif
