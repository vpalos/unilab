#ifndef CCOLORSMANAGER_H
#define CCOLORSMANAGER_H

#include <fx.h>

class cColorsManager
{
    private:
    protected:
    public:
        static int getColorsCount(void);
        //static FXStipplePattern color2Stipple(FXColor prColor);
        static FXColor getColor(FXint prIndex, FXint prOffset=0);

        static FXColor generateDeterministicColor(FXint prMin, FXint prMax);
        static void generateDeterministicSeed(FXint prSeed);
};

#endif
