#ifndef CVETERINARIANMANAGER_H
#define CVETERINARIANMANAGER_H

#include <fx.h>

typedef struct sVeterinarianObject
{
    FXString *id;
    FXString *title;
    int solid;
};

class cVeterinarianManager
{
    private:
    protected:
    public:
        static FXbool veterinarianSolid(const FXString &prId);
        static FXbool veterinarianExists(const FXString &prId);
        static FXbool veterinarianNeeded(const FXString &prId);
        static FXString getNewID(void);
        static int getVeterinarianCount(void);
        
        static sVeterinarianObject *listVeterinarians(void);
        static cDataResult *getVeterinarian(const FXString &prId);
        
        static FXString newVeterinarian(FXMDIClient *prP, FXPopup *prPup);
        static FXString editVeterinarian(FXMDIClient *prP, FXPopup *prPup,const FXString &prId);
        static FXString duplicateVeterinarian(const FXString &prId);
        
        static FXbool addVeterinarian(FXString prId,FXString prLab,FXString prAdd,FXString prCity,FXString prState,FXString prZip,FXString prPhone,FXString prFax,FXString prEmail,FXString prComments);
        static FXbool setVeterinarian(FXString prOldId,FXString prId,FXString prLab,FXString prAdd,FXString prCity,FXString prState,FXString prZip,FXString prPhone,FXString prFax,FXString prEmail,FXString prComments);
        static FXbool removeVeterinarian(const FXString &prId);
};

#endif
