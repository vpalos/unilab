#include <strings.h>

#include "engine.h"
#include "graphics.h"
#include "cWinMain.h"
#include "cLanguage.h"
#include "cDataResult.h"
#include "cAssayManager.h"
#include "cTemplateManager.h"
#include "cReadersManager.h"
#include "cReadingsManager.h"
#include "cMDIReadings.h"
#include "cFormulaRatio.h"

FXbool cReadingsManager::makeReading(FXMDIClient *prP, FXPopup *prPup,FXString prAssay,FXString prTemplate,FXbool prManual,FXString prLot,FXString prExp)
{
    if(prAssay.empty() || prTemplate.empty())
        return false;
    
    int plateCount=cTemplateManager::getPlateCount(prTemplate);
    cDataResult *resa=cAssayManager::getAssay(prAssay);
    cDataResult *rest=cTemplateManager::getTemplate(prTemplate);
    
    if(!cFormulaRatio::testCompatible(resa,rest))
        return false;
    
    double **resd=(double**)malloc(plateCount*sizeof(double*));
    if(!resd)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return false;
    }
    /*int ows=0;
    if(oWinMain->isMinimized())
        ows=1;
    else if(oWinMain->isMaximized())
        ows=2;
    if(!prManual)
        oWinMain->minimize();*/
    int ok=true;
    for(int i=0;i<plateCount;i++)
    {
        if(prManual)
        {
            resd[i]=(double*)malloc(96*sizeof(double));
            if(!resd[i])
            {
                FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
                ok=false;
                break;
            }
            memset(resd[i],0,96*sizeof(double));
        }
        else
        {
            resd[i]=cReadersManager::acquire(resa->getCellString(0,3),resa->getCellString(0,4),i,plateCount);
            if(resd[i]==NULL)
            {
                FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_serialerr_rsessabort").text());
                ok=false;
                break;
            }
            
            /* TEMPORARY FOR TEST PURPOSES ONLY
            FXString res=FXStringFormat("\n\nPlate %d:\n",i);
            for(int y=0;y<96;y++)
                res=res+(resd[i][y]>10000?FXStringFormat("__OVER%c",(y+1)%12?' ':'\n'):resd[i][y]<0?FXStringFormat("_UNDER%c",(y+1)%12?' ':'\n'):
                    FXStringFormat("%+1.3f%c",resd[i][y],(y+1)%12?' ':'\n'));
            FXMessageBox::warning(oWinMain,MBOX_OK,"DEBUGWN",res.text());
            // TEMPORARY */
        }
    }
    /*switch(ows)
    {
        case 1:
            break;
        case 2:
            oWinMain->maximize();
            break;
        default:
            oWinMain->restore();
            break;
    }*/
    
    if(ok)
    {
        cMDIReadings *rdsWin=new cMDIReadings(prP,oLanguage->getText("str_reading"),new FXGIFIcon(oApplicationManager,data_browse_readings),prPup,MDI_NORMAL,0,0,657,560);
        rdsWin->create();
        if(rdsWin->newReading(resa,rest,plateCount,resd,prLot,prExp))
            rdsWin->setFocus();
        else return false;
    }
    //resa->free();
    //rest->free();
    return ok;
}

double **cReadingsManager::remakeReading(const FXString &prFilterA,const FXString &prFilterB,FXint prPlateCount,FXint prShownPlateStart,FXint prShownPlateCount)
{
    double **resd=(double**)malloc(prPlateCount*sizeof(double*));
    if(!resd)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }
    int ok=true;
    for(int i=0;i<prPlateCount;i++)
    {
        resd[i]=cReadersManager::acquire(prFilterA,prFilterB,i+prShownPlateStart,prShownPlateCount);
        if(resd[i]==NULL)
        {
            FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_warning").text(),oLanguage->getText("str_serialerr_rsessabort").text());
            ok=false;
            break;
        }
    }
    return ok?resd:NULL;
}

void cReadingsManager::openReading(FXMDIClient *prP, FXPopup *prPup,cSortList &prCases)
{
    cMDIReadings *rdsWin=new cMDIReadings(prP,oLanguage->getText("str_reading"),new FXGIFIcon(oApplicationManager,data_browse_readings),prPup,MDI_NORMAL,0,0,657,560);
    rdsWin->create();
    if(rdsWin->loadReadings(prCases))
        rdsWin->setFocus();
    else {delete rdsWin;return;}
}
