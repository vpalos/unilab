#ifndef CLANGUAGE_H
#define CLANGUAGE_H

#include <fx.h>

class cLanguage
{
    private:
    protected:
        FXStringDict *dictionary;
        FXString storedLanguage;
        FXbool loaded;
        FXSettings *help;

    public:
        cLanguage();
        ~cLanguage();

        FXSettings *getHelp(){return help;};

        FXbool loadDesignated(bool prChoose=false);
        FXbool loadDesignated(const FXString &prLanguage);
        FXbool saveDesignated(void);
        FXbool saveDesignated(const FXString &prLanguage);

        FXString getLanguage(void);
        FXString getText(const FXString &prKey);

        int canQuit(void);
        void saveSettings(void);
};

extern cLanguage *oLanguage;

#endif
