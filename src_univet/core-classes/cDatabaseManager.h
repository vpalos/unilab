#ifndef CDATABASEMANAGER_H
#define CDATABASEMANAGER_H

#include <fx.h>

typedef struct sDatabaseObject
{
    FXString *filename;
    FXString *specie;
    FXString *title;
    FXString *owner;
    FXString *comments;
};

class cDatabaseManager
{
    private:
    protected:
    public:
        static int getDatabaseCount(void);
        static int getDatabaseCount(const FXString &prSpecies);

        static sDatabaseObject *listDatabases(void);
        static sDatabaseObject *listDatabases(const FXString &prSpecies);

        static FXbool databaseExists(const FXString &prTitle);
        static FXbool databaseExists(const FXString &prTitle,const FXString &prSpecies);

        static FXString newDatabase(int prSpecies);
        static FXString editDatabase(const FXString &prFilename);
        static FXString duplicateDatabase(const FXString &prFilename);

        static FXbool removeDatabase(const FXString &prFilename);
};

#endif
