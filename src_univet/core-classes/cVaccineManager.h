#ifndef CVACCINEMANAGER_H
#define CVACCINEMANAGER_H

#include <fx.h>

typedef struct sVaccineObject
{
    FXString *id;
    FXString *title;
    int solid;
};

class cVaccineManager
{
    private:
    protected:
    public:
        static FXbool vaccineSolid(const FXString &prId);
        static FXbool vaccineExists(const FXString &prId);
        static FXbool vaccineNeeded(const FXString &prId);
        static FXString getNewID(void);
        static int getVaccineCount(void);
        
        static sVaccineObject *listVaccines(void);
        static cDataResult *getVaccine(const FXString &prId);
        
        static FXString newVaccine(FXMDIClient *prP, FXPopup *prPup);
        static FXString editVaccine(FXMDIClient *prP, FXPopup *prPup,const FXString &prId);
        static FXString duplicateVaccine(const FXString &prId);
        
        static FXbool addVaccine(FXString prId,FXString prTitle,FXString prAssays_defs,FXString prComments);
        static FXbool setVaccine(FXString prOldId,FXString prId,FXString prTitle,FXString prAssays_defs,FXString prComments);
        static FXbool removeVaccine(const FXString &prId);
};

#endif
