#include <strings.h>
#include <time.h>
#include "engine.h"
#include "graphics.h"
#include "cWinMain.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cDataResult.h"
#include "cCaseExport.h"
#include "cCaseManager.h"
#include "cPopulationManager.h"
#include "cVaccineManager.h"
#include "cVeterinarianManager.h"
#include "cMDIDatabaseManager.h"

FXString cCaseExport::getFileName(void)
{
    FXString ret,dir=oApplicationManager->reg().readStringEntry("settings","exportdir",(FXSystem::getHomeDirectory()+PATHSEP).text());
    bool found=false;
    while(!found)
    {
        ret=FXFileDialog::getSaveFilename(oWinMain,oLanguage->getText("str_export_dlgtitle"),dir,oLanguage->getText("str_export_dlgpatt1")+" (*)\n"+oLanguage->getText("str_export_dlgpatt2")+" (*.uv"+oDataLink->getSpecies()+")",1);
        if(!ret.empty())
        {
            dir=FXPath::directory(ret)+PATHSEP;
            oApplicationManager->reg().writeStringEntry("settings","exportdir",dir.text());
        }
        if(ret=="")
            return ret;
        if(FXPath::extension(ret).lower()!=("uv"+oDataLink->getSpecies()))
            ret=ret+".uv"+oDataLink->getSpecies();
        if(FXStat::exists(ret))
        {
            if(MBOX_CLICKED_YES==FXMessageBox::question(oWinMain,MBOX_YES_NO,oLanguage->getText("str_error").text(),oLanguage->getText("str_fileovr").text()))
                found=true;
        }
        else
            found=true;
    }
    return ret;
}

FXbool cCaseExport::processData(cSortList &prCases,FXString prFile)
{
    if(prFile.empty())
        return true;

    FXStringDict *assays=new FXStringDict();
    FXStringDict *vets=new FXStringDict();
    FXStringDict *vaccs=new FXStringDict();
    FXStringDict *breeds=new FXStringDict();
    FXStringDict *tpls=new FXStringDict();
    FXStringDict *grws=new FXStringDict();
    FXStringDict *owns=new FXStringDict();
    FXStringDict *pops=new FXStringDict();

    FXStringDict *cids=new FXStringDict();
    FXStringDict *casy=new FXStringDict();

    int cc=0;

    int j=0;
    for(int i=0;i<prCases.getNumItems();i++)
        if(prCases.isItemSelected(i))
        {
            cDataResult *cse,*pop,*vac;
            cse=cCaseManager::getCase(prCases.getItemText(i).after('\t').before('\t'),prCases.getItemText(i).before('\t'));
            if(!cse || !cse->getRowCount())continue;
            pop=cPopulationManager::getPopulation(cse->getCellString(0,13));
            if(!pop || !pop->getRowCount())continue;
            vac=cVaccineManager::getVaccine(pop->getCellString(0,7));
            if((!vac || !vac->getRowCount()) && (pop->getCellString(0,7)!="---"))continue;

            cids->insert(FXStringFormat("%d",cc).text(),cse->getCellString(0,0).text());
            casy->insert(FXStringFormat("%d",cc++).text(),cse->getCellString(0,6).text());

            assays->insert(cse->getCellString(0,6).text(),"-");
            pops->insert(cse->getCellString(0,13).text(),"-");
            vets->insert(cse->getCellString(0,5).text(),"-");
            tpls->insert(cse->getCellString(0,16).text(),"-");

            breeds->insert(pop->getCellString(0,3).text(),"-");
            breeds->insert(pop->getCellString(0,4).text(),"-");
            owns->insert(pop->getCellString(0,5).text(),"-");
            grws->insert(pop->getCellString(0,6).text(),"-");
            vaccs->insert(pop->getCellString(0,7).text(),"-");

            delete cse;
            delete pop;
            delete vac;

            j++;
        }
    if(!j)
        return false;

    cDataResult *rr;
    cDataLink *link=new cDataLink();
    if(FXStat::exists(prFile))
        FXFile::remove(prFile.text());
    if(!link->open(prFile,true))
        return false;
    for(int i=0;i<cids->no();i++)
    {
        rr=oDataLink->execute("SELECT * FROM t_gncases WHERE id='"+
                              FXStringFormat("%s",cids->find(FXStringFormat("%d",i).text()))+"' AND assay_oid=='"+
                              FXStringFormat("%s",casy->find(FXStringFormat("%d",i).text()))+"';");
        if(rr && rr->getRowCount())
        {
            link->execute(FXStringFormat("INSERT INTO t_gncases VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',%s,%s,'%s','%s',%s,'%s',%s,%s,%s,%s,%s,'%s','%s','%s');",
                                    rr->getCellString(0,0).text(),
                                    rr->getCellString(0,1).text(),
                                    rr->getCellString(0,2).text(),
                                    rr->getCellString(0,3).text(),
                                    rr->getCellString(0,4).text(),
                                    rr->getCellString(0,5).text(),
                                    rr->getCellString(0,6).text(),
                                    rr->getCellString(0,7).text(),
                                    rr->getCellString(0,8).text(),
                                    rr->getCellString(0,9).text(),
                                    rr->getCellString(0,10).text(),
                                    rr->getCellString(0,11).text(),
                                    rr->getCellString(0,12).text(),
                                    rr->getCellString(0,13).text(),
                                    rr->getCellString(0,14).text(),
                                    rr->getCellString(0,15).text(),
                                    rr->getCellString(0,16).text(),
                                    rr->getCellString(0,17).text(),
                                    rr->getCellString(0,18).text(),
                                    rr->getCellString(0,19).text(),
                                    rr->getCellString(0,20).text(),
                                    rr->getCellString(0,21).text(),
                                    rr->getCellString(0,22).text(),
                                    rr->getCellString(0,23).text(),
                                    rr->getCellString(0,24).text()
                                    ).text());
        }
        delete rr;
    }

    rr=oDataLink->execute("SELECT * FROM t_assays;");
    for(int i=0;i<rr->getRowCount();i++)
    {
        if(assays->find(rr->getCellString(i,0).text()))
        {
            link->execute(FXStringFormat("INSERT INTO t_assays VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",
                                    rr->getCellString(i,0).text(),
                                    rr->getCellString(i,1).text(),
                                    rr->getCellString(i,2).text(),
                                    rr->getCellString(i,3).text(),
                                    rr->getCellString(i,4).text(),
                                    rr->getCellString(i,5).text(),
                                    rr->getCellString(i,6).text(),
                                    rr->getCellString(i,7).text(),
                                    rr->getCellString(i,8).text(),
                                    rr->getCellString(i,9).text(),
                                    rr->getCellString(i,10).text()
                                    ).text());
        }
    }
    delete rr;

    rr=oDataLink->execute("SELECT * FROM t_gnpopulations;");
    for(int i=0;i<rr->getRowCount();i++)
    {
        if(pops->find(rr->getCellString(i,0).text()))
        {
            link->execute(FXStringFormat("INSERT INTO t_gnpopulations VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",
                                    rr->getCellString(i,0).text(),
                                    rr->getCellString(i,1).text(),
                                    rr->getCellString(i,2).text(),
                                    rr->getCellString(i,3).text(),
                                    rr->getCellString(i,4).text(),
                                    rr->getCellString(i,5).text(),
                                    rr->getCellString(i,6).text(),
                                    rr->getCellString(i,7).text(),
                                    rr->getCellString(i,8).text(),
                                    rr->getCellString(i,9).text(),
                                    rr->getCellString(i,10).text(),
                                    rr->getCellString(i,11).text()
                                    ).text());
        }
    }
    delete rr;

    rr=oDataLink->execute("SELECT * FROM t_vaccines;");
    for(int i=0;i<rr->getRowCount();i++)
    {
        if(vaccs->find(rr->getCellString(i,0).text()))
        {
            FXString id=rr->getCellString(i,0);
            FXString defs=rr->getCellString(i,3);
            FXString ndefs="";
            FXint na=0;

            defs=defs.after('\r');
            while(!defs.empty())
            {
                if(assays->find(defs.before('\n').text()))
                {
                    na++;
                    ndefs=ndefs+defs.before('\r')+"\r";
                }
                defs=defs.after('\r');
            }
            ndefs=FXStringFormat("%d\r",na)+ndefs;
            link->execute(FXStringFormat("INSERT INTO t_vaccines VALUES('%s','%s','%s','%s','%s');",
                                    rr->getCellString(i,0).text(),
                                    rr->getCellString(i,1).text(),
                                    rr->getCellString(i,2).text(),
                                    ndefs.text(),
                                    rr->getCellString(i,4).text()
                                    ).text());
        }
    }
    delete rr;

    rr=oDataLink->execute("SELECT * FROM t_veterinarians;");
    for(int i=0;i<rr->getRowCount();i++)
    {
        if(vets->find(rr->getCellString(i,0).text()))
        {
            link->execute(FXStringFormat("INSERT INTO t_veterinarians VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",
                                    rr->getCellString(i,0).text(),
                                    rr->getCellString(i,1).text(),
                                    rr->getCellString(i,2).text(),
                                    rr->getCellString(i,3).text(),
                                    rr->getCellString(i,4).text(),
                                    rr->getCellString(i,5).text(),
                                    rr->getCellString(i,6).text(),
                                    rr->getCellString(i,7).text(),
                                    rr->getCellString(i,8).text(),
                                    rr->getCellString(i,9).text(),
                                    rr->getCellString(i,10).text()
                                    ).text());
        }
    }
    delete rr;

    rr=oDataLink->execute("SELECT * FROM t_owners;");
    for(int i=0;i<rr->getRowCount();i++)
    {
        if(owns->find(rr->getCellString(i,0).text()))
        {
            link->execute(FXStringFormat("INSERT INTO t_owners VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",
                                    rr->getCellString(i,0).text(),
                                    rr->getCellString(i,1).text(),
                                    rr->getCellString(i,2).text(),
                                    rr->getCellString(i,3).text(),
                                    rr->getCellString(i,4).text(),
                                    rr->getCellString(i,5).text(),
                                    rr->getCellString(i,6).text(),
                                    rr->getCellString(i,7).text(),
                                    rr->getCellString(i,8).text(),
                                    rr->getCellString(i,9).text(),
                                    rr->getCellString(i,10).text()
                                    ).text());
        }
    }
    delete rr;

    rr=oDataLink->execute("SELECT * FROM t_growers;");
    for(int i=0;i<rr->getRowCount();i++)
    {
        if(grws->find(rr->getCellString(i,0).text()))
        {
            link->execute(FXStringFormat("INSERT INTO t_growers VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",
                                    rr->getCellString(i,0).text(),
                                    rr->getCellString(i,1).text(),
                                    rr->getCellString(i,2).text(),
                                    rr->getCellString(i,3).text(),
                                    rr->getCellString(i,4).text(),
                                    rr->getCellString(i,5).text(),
                                    rr->getCellString(i,6).text(),
                                    rr->getCellString(i,7).text(),
                                    rr->getCellString(i,8).text(),
                                    rr->getCellString(i,9).text(),
                                    rr->getCellString(i,10).text()
                                    ).text());
        }
    }
    delete rr;

    rr=oDataLink->execute("SELECT * FROM t_breeds;");
    for(int i=0;i<rr->getRowCount();i++)
    {
        if(breeds->find(rr->getCellString(i,0).text()))
        {
            link->execute(FXStringFormat("INSERT INTO t_breeds VALUES('%s','%s','%s','%s');",
                                    rr->getCellString(i,0).text(),
                                    rr->getCellString(i,1).text(),
                                    rr->getCellString(i,2).text(),
                                    rr->getCellString(i,3).text()
                                    ).text());
        }
    }
    delete rr;

    rr=oDataLink->execute("SELECT * FROM t_templates;");
    for(int i=0;i<rr->getRowCount();i++)
    {
        if(tpls->find(rr->getCellString(i,0).text()))
        {
            link->execute(FXStringFormat("INSERT INTO t_templates VALUES('%s','%s',%s,%s,'%s','%s');",
                                    rr->getCellString(i,0).text(),
                                    rr->getCellString(i,1).text(),
                                    rr->getCellString(i,2).text(),
                                    rr->getCellString(i,3).text(),
                                    rr->getCellString(i,4).text(),
                                    rr->getCellString(i,5).text()
                                    ).text());
        }
    }
    delete rr;

    FXchar ftime[100];
    time_t curtime;
    struct tm *loctime;
    curtime=time(NULL);
    loctime=localtime(&curtime);
    strftime(ftime,100,"%Y-%m-%d %H:%M:%S",loctime);
    FXString nowDate(ftime);
    link->setProperty("database_name",oLanguage->getText("str_export_dbname")+FXStringFormat(" \"%s\"",oDataLink->getName().text())+" - "+nowDate);
    link->setProperty("database_species",oDataLink->getSpecies());
    link->setProperty("database_comments","");
    link->setProperty("database_owner",FXPath::title(prFile));

    link->close();

    delete assays;
    delete vets;
    delete vaccs;
    delete breeds;
    delete tpls;
    delete grws;
    delete owns;
    delete pops;
    delete cids;
    delete casy;

    FXMessageBox::information(oWinMain,MBOX_OK,oLanguage->getText("str_information").text(),oLanguage->getText("str_success").text());
    return true;
}

void cCaseExport::openCases(FXMDIClient *prP, FXPopup *prPup,cSortList &prCases)
{
    if(MBOX_CLICKED_NO==FXMessageBox::warning(oWinMain,MBOX_YES_NO,oLanguage->getText("str_question").text(),oLanguage->getText("str_export_confirm").text()))
        return;
    if(!processData(prCases,getFileName()))
        FXMessageBox::warning(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_exporterror").text());
    else
        cMDIDatabaseManager::cmdRefresh();
}


