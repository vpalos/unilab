#ifndef CFORMULARATIO_H
#define CFORMULARATIO_H

#include <fx.h>
#include "cDataResult.h"

#define FORMULA_PRICAL 1
#define FORMULA_SECCAL 2
#define FORMULA_TITERS 4

#define FORMULA_DEFAULT 128

#define FORMULA_ALL 7

typedef struct sFormulaRatioObject
{
    FXString *id;
    FXString *title;
    int flags;
};

typedef struct sControlAms
{
    FXdouble posam,negam;
};

class cFormulaRatio
{
    private:
    protected:
    public:
        static int getCount(void);

        static sFormulaRatioObject *listFormulaRatio(void);

        static sControlAms calculateControlAms(FXdouble *prControlsData,FXint *prControlsLayout,FXint prControlsCount,FXbool prAlelisa);

        static FXdouble calculateRatio(FXString *prFormula,FXdouble prValue,FXdouble prNhc,FXdouble *prControlsData,FXint *prControlsLayout,FXint prControlsCount,FXint prFactor,FXint prFactors[4],FXbool prAlelisa);
        static FXdouble calculateEU(FXdouble prValue,FXdouble *prControlsData,FXint *prControlsLayout,FXint prControlsCount,FXint prFactor,FXint prFactors[4],FXbool prAlelisa);
        static FXdouble calculateRatioGroup(FXdouble prValue,FXdouble prRatioGroups[30]);
        static FXdouble calculateTiter(FXdouble prValue,FXdouble prSlope,FXdouble prIntercept,FXint prFactor,FXint prFactors[4]);
        static FXdouble calculateTiterLog2(FXdouble prValue);
        static FXdouble calculateTiterGroup(FXdouble prValue,FXint prTiterGroups[30]);

        static FXbool testCompatible(cDataResult *prAssayRes,cDataResult *prTemplateRes);
        static FXbool testChangeCompatible(FXString prCal_defs,FXString prOldCal_defs,FXString prRul_defs,FXString prOldRul_defs);
};

#endif
