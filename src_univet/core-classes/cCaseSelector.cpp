#include <fx.h>
#include <strings.h>
#include <math.h>
#include "engine.h"
#include "cCaseSelector.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cFormulaRatio.h"
#include "cDataResult.h"
#include "cDataLink.h"

int cCaseSelector::getCriteriaCount(void)
{
    return 26;
}

sCriteriaObject *cCaseSelector::listCriteria(void)
{
    sCriteriaObject *ret;

    ret=(sCriteriaObject*)malloc(getCriteriaCount()*sizeof(sCriteriaObject));
    if(!ret)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }

    FXString attrs="id,population_oid,assay_oid,template_oid,age,bleedDate,readingDate,count,replicates,factor,lot,expirationDate,veterinarian_oid,technician,reason,spType,comments";

    for(int i=0;i<17;i++)
    {
        ret[i].name=new FXString(oLanguage->getText(FXStringFormat("str_csmdi_c%d",i)));
        ret[i].attribute=new FXString(attrs.before(','));
        attrs=attrs.after(',');
        ret[i].flag=CRITERIA_CASE;
        ret[i].type=CRITERIA_TEXT;
        ret[i].values1=NULL;
        ret[i].values2=NULL;
    }
    ret[5].type=CRITERIA_DATE;
    ret[6].type=CRITERIA_DATE;
    ret[7].type=CRITERIA_NUMBER;
    ret[8].type=CRITERIA_NUMBER;
    ret[9].type=CRITERIA_NUMBER;

    attrs="birthDate,breed1,breed2,owner_oid,grower_oid,vaccine_oid,unit,location,sublocation";
    for(int i=0;i<9;i++)
    {
        ret[17+i].name=new FXString(oLanguage->getText(FXStringFormat("str_csmdi_p%d",i)));
        ret[17+i].attribute=new FXString(attrs.before(','));
        attrs=attrs.after(',');
        ret[17+i].flag=CRITERIA_POPULATION;
        ret[17+i].type=CRITERIA_TEXT;
        ret[17+i].values1=NULL;
        ret[17+i].values2=NULL;
    }
    ret[17].type=CRITERIA_DATE;

    return ret;
}

sCaseObject *cCaseSelector::makeQuickSelection(FXString prCasea,FXString prCaseb,FXString prPopa,FXString prPopb,FXString prDatea,FXString prDateb,FXIconList *prAssays,FXint *prCount)
{
    sCaseObject *ret;
    FXString criteria="";
    prCasea=prCasea.trim().substitute("'","");
    prCaseb=prCaseb.trim().substitute("'","");
    prPopa=prPopa.trim().substitute("'","");
    prPopb=prPopb.trim().substitute("'","");
    prDatea=prDatea.trim().substitute("'","");
    prDateb=prDateb.trim().substitute("'","");
    if(prCasea.empty() && !prCaseb.empty())
        prCasea=prCaseb;
    if(!prCasea.empty())
    {
        criteria="lower(id)";
        criteria=criteria+(prCaseb.empty()?" LIKE ":">=")+(prCaseb.empty()?"'%":"'")+prCasea.lower()+(prCaseb.empty()?"%'":"'")+(prCaseb.empty()?"":" AND lower(id)<='"+prCaseb.lower()+"'");
    }
    if(prPopa.empty() && !prPopb.empty())
        prPopa=prPopb;
    if(!prPopa.empty())
    {
        criteria="lower(population_oid)";
        criteria=criteria+(prPopb.empty()?" LIKE ":">=")+(prPopb.empty()?"'%":"'")+prPopa.lower()+(prPopb.empty()?"%'":"'")+(prPopb.empty()?"":" AND lower(population_oid)<='"+prPopb.lower()+"'");
    }
    if(prDatea.empty() && !prDateb.empty())
        prDatea=prDateb;
    if(!prDatea.empty())
        criteria=criteria+(criteria.empty()?"":" AND ")+"readingDate"+(prDateb.empty()?" LIKE ":">=")+(prDateb.empty()?"'%":"'")+prDatea+(prDateb.empty()?"%'":"'")+(prDateb.empty()?"":" AND readingDate<='"+prDateb+"'");
    int selcount=0;
    for(int i=0;i<prAssays->getNumItems();i++)
        if(prAssays->isItemSelected(i))
            selcount++;
    if(selcount)
    {
        criteria=criteria+(criteria.empty()?"":" AND ")+"(";
        int ti=0;
        for(int i=0;i<prAssays->getNumItems();i++)
            if(prAssays->isItemSelected(i))
                criteria=criteria+((ti++)>0?" OR ":"")+"assay_oid='"+prAssays->getItemText(i).before('\t')+"'";
        criteria=criteria+")";
    }
    ret=cCaseManager::listCases(criteria,prCount);
    return ret;
}

sCaseObject *cCaseSelector::makeAllSelection(FXint *prCount)
{
    sCaseObject *ret;
    ret=cCaseManager::listCases("",prCount);
    return ret;
}

sCaseObject *cCaseSelector::makeSelection(FXIconList *prCriteriaList,FXint *prCount)
{
    sCaseObject *ret;
    sCriteriaObject *co;

    FXString sc="",ssc="";
    int sct=0,ssct=0;
    bool age=false;
    sCriteriaObject *pco=NULL;

    for(int i=0;i<prCriteriaList->getNumItems();i++)
    {
        co=(sCriteriaObject*)prCriteriaList->getItemData(i);
        if(!co || !co->values1)
            continue;
        if(*co->attribute=="age")
        {
            age=true;
            pco=co;
            continue;
        }
        ssc=(co->flag==CRITERIA_CASE)?"":"lower(population_oid) in (SELECT lower(id) FROM t_gnpopulations WHERE ";
        ssct=0;
        for(int j=0;j<co->values1->no();j++)
        {
            bool isd=false;
            if((co->values2) && co->values2->data(j) && (FXString(co->values2->data(j)).trim().substitute("'","").length()>0))
                isd=true;

            bool isn=co->type==CRITERIA_NUMBER;

            ssc=ssc+(j>0?" OR (":"(");
            ssct=1;
            if(isn)
                ssc=ssc+*co->attribute+(isd?">=":"=");
            else
                ssc=ssc+"lower("+*co->attribute+(isd?")>=":") LIKE ");
            ssc=ssc+(isn?"'":"'%")+FXString(co->values1->data(j)).trim().lower().substitute("'","")+(isn?"'":"%'");
            if(isd)
            {
                ssc=ssc+" AND ";
                if(isn)
                    ssc=ssc+*co->attribute+"<=";
                else
                    ssc=ssc+"lower("+*co->attribute+")<=";
                ssc=ssc+"'"+FXString(isn?FXStringVal(FXIntVal(co->values2->data(j))):co->values2->data(j)).trim().lower().substitute("'","")+"'";
            }
            ssc=ssc+")";
        }
        if(co->flag!=CRITERIA_CASE)
            ssc=ssc+")";
        sc=sc+(sct?" AND (":"(")+ssc+")";
        sct=1;
    }

    ret=cCaseManager::listCases(sc,prCount);

    if(age)
    {
        sCaseObject *ret2;
        FXDict *cases=new FXDict();
        int j=0;
        for(int i=0;i<*prCount;i++)
        {
            sCaseObject *c=&ret[i];
            FXString sage=*c->age;
            sage=sage.substitute("-","");
            bool found=false;

            for(int k=0;k<pco->values1->no();k++)
            {
                bool isd=false;
                if((pco->values2) && pco->values2->data(k) && (FXString(pco->values2->data(k)).trim().length()>0))
                    isd=true;

                FXString p1="",p2="";
                p1=FXString(pco->values1->data(k)).trim();
                if(isd)
                    p2=FXString(pco->values2->data(k)).trim();
                p1=p1.substitute("-","");
                p2=p2.substitute("-","");

                FXint agv=FXIntVal(sage),p1v=FXIntVal(p1),p2v=FXIntVal(p2);

                if(isd)
                {
                    if(agv>=p1v && agv<=p2v)
                        found=true;
                }
                else
                {
                    if(agv==p1v)
                        found=true;
                }
            }

            if(found)
                cases->insert(FXStringFormat("%d",j++).text(),c);
        }

        ret2=(sCaseObject*)malloc(j*sizeof(sCaseObject));
        if(!ret2)
        {
            FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
            return NULL;
        }
        for(int i=0;i<j;i++)
        {
            sCaseObject *c=(sCaseObject*)cases->data(i);
            ret2[i].id=c->id;
            ret2[i].readingDate=c->readingDate;
            ret2[i].assay_oid=c->assay_oid;
            ret2[i].population_oid=c->population_oid;
            ret2[i].population=c->population;
            ret2[i].age=c->age;
            ret2[i].count=c->count;
            ret2[i].tpl=c->tpl;
        }

        free(ret);
        *prCount=j;
        delete cases;
        return ret2;
    }

    return ret;
}

