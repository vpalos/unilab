#ifndef CBASELINEMANAGER_H
#define CBASELINEMANAGER_H

#include <fx.h>
#include "cDataResult.h"

typedef struct sBaselineObject
{
    FXString *id;
    int solid;
};

class cBaselineManager
{
    private:
    protected:
        static FXbool idBaselineId(const FXString &prId);
    public:
        static FXbool baselineSolid(const FXString &prId);
        static FXbool baselineExists(const FXString &prId);
        static FXString getNewID(void);
        static int getBaselineCount(void);
        
        static sBaselineObject *listBaselines(void);
        static cDataResult *getBaseline(const FXString &prId);
        
        static FXString newBaseline(FXMDIClient *prP, FXPopup *prPup);
        static FXString editBaseline(FXMDIClient *prP, FXPopup *prPup,const FXString &prId);
        static FXString duplicateBaseline(const FXString &prId);
        
        static FXbool addBaseline(FXString prId,FXString prParams,FXString prComments);
        static FXbool setBaseline(FXString prOldId,FXString prId,FXString prParams,FXString prComments);
        static FXbool removeBaseline(const FXString &prId);
};

#endif
