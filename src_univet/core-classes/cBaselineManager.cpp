#include "engine.h"
#include "cDataLink.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cMDIBaseline.h"
#include "cBaselineManager.h"

FXbool cBaselineManager::baselineSolid(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_baselines WHERE id='"+ress.trim().substitute("'","")+"' AND solid='1';");
    return res->getRowCount()>0;
}

FXbool cBaselineManager::baselineExists(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_baselines WHERE id='"+ress.trim().substitute("'","")+"';");
    return res->getRowCount()>0;
}

FXbool cBaselineManager::idBaselineId(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT id FROM t_baselines WHERE id='"+ress.trim().substitute("'","")+"';");
    return res->getRowCount()>0;
}

FXString cBaselineManager::getNewID(void)
{
    static int basNr=oApplicationManager->reg().readIntEntry("oids","baselines",1);
    FXString res=oLanguage->getText("str_bslmdi_vactitle");
    FXString ret=res+FXStringFormat(" #%d",++basNr);
    while(idBaselineId(ret))
        ret=res+FXStringFormat(" #%d",++basNr);
    oApplicationManager->reg().writeIntEntry("oids","baselines",basNr);
    return ret;
}

int cBaselineManager::getBaselineCount(void)
{
    if(!oDataLink->isOpened())
        return 0;
    cDataResult *res=oDataLink->execute("SELECT COUNT(id) FROM t_baselines;");
    return res->getCellInt(0,0);
}

sBaselineObject *cBaselineManager::listBaselines()
{
    if(!oDataLink->isOpened())
        return NULL;

    sBaselineObject *ret=(sBaselineObject*)malloc(getBaselineCount()*sizeof(sBaselineObject));
    if(!ret)
    {
        FXMessageBox::error(oWinMain,MBOX_OK,oLanguage->getText("str_error").text(),oLanguage->getText("str_memalloc").text());
        return NULL;
    }
    cDataResult *res=oDataLink->execute(("SELECT id,solid FROM t_baselines;"));
    for(int i=0;i<res->getRowCount();i++)
    {
        ret[i].id=new FXString(res->getCellString(i,0));
        ret[i].solid=res->getCellInt(i,1);
    }
    return ret;
}

cDataResult *cBaselineManager::getBaseline(const FXString &prId)
{
    FXString res=prId;
    return oDataLink->execute("SELECT * FROM t_baselines WHERE id='"+res.substitute("'","")+"';");
}

FXString cBaselineManager::newBaseline(FXMDIClient *prP, FXPopup *prPup)
{
    FXString ret=getNewID();
    cMDIBaseline *basWin=new cMDIBaseline(prP,ret,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,508,388);
    basWin->create();
    basWin->setFocus();
    return ret;
}

FXString cBaselineManager::editBaseline(FXMDIClient *prP, FXPopup *prPup,const FXString &prId)
{
    cMDIBaseline *basWin=new cMDIBaseline(prP,prId,NULL,prPup,MDI_NORMAL|MDI_TRACKING,0,0,508,388);
    basWin->create();
    basWin->loadBaseline(prId);
    basWin->setFocus();
    return prId;
}

FXString cBaselineManager::duplicateBaseline(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return "";
    FXString ress=prId;
    cDataResult *res=oDataLink->execute("SELECT * FROM t_baselines WHERE id='"+ress.trim().substitute("'","")+"';");
    if(res->getRowCount()<=0)
        return "";
    int i=1;
    FXString ret=res->getCellString(0,0);
    while(idBaselineId(ret))
        ret=res->getCellString(0,0)+FXStringFormat(" #%d",i++);
    FXString res2="INSERT INTO t_baselines VALUES('"+ret.trim().substitute("'","")+"'";
    for(i=1;i<res->getFieldCount();i++)
        res2=res2+",'"+res->getCellString(0,i).trim().substitute("'","")+"'";
    res2=res2+");";
    res=oDataLink->execute(res2);
    return ret;
}

FXbool cBaselineManager::addBaseline(FXString prId,FXString prParams,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("INSERT INTO t_baselines VALUES('"+
                                                    prId.trim().substitute("'","")+"','"+
                                                    "0','"+
                                                    prParams.trim().substitute("'","")+"','"+
                                                    prComments.trim().substitute("'","")+"');");
    return (oDataLink->getAffectedRowCount()==1);
}

FXbool cBaselineManager::setBaseline(FXString prOldId,FXString prId,FXString prParams,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("UPDATE t_baselines SET id='"+prId.trim().substitute("'","")+
                       "', solid='0"+
                       "', params_defs='"+prParams.trim().substitute("'","")+
                       "', comments='"+prComments.trim().substitute("'","")+
                       "' WHERE id='"+prOldId.trim().substitute("'","")+"' AND solid='0';");
    return (oDataLink->getAffectedRowCount()==1);
}

FXbool cBaselineManager::removeBaseline(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    FXString ress=prId;
    return (oDataLink->execute("DELETE FROM t_baselines WHERE id='"+ress.trim().substitute("'","")+"' AND solid='0';")!=NULL);
}


