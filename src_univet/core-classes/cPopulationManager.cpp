#include <ctype.h>
#include <stdio.h>
#include <strings.h>
#include <time.h>

#include "engine.h"
#include "cLanguage.h"
#include "cWinMain.h"
#include "cDataResult.h"
#include "cPopulationManager.h"
#include "cSpeciesManager.h"
#include "cMDITemplate.h"

FXbool cPopulationManager::populationExists(const FXString &prTitle)
{
    if(!oDataLink->isOpened())
        return false;
    cDataResult *res=oDataLink->execute("SELECT birthDate FROM t_gnpopulations WHERE id='"+prTitle+"';");
    return (res->getRowCount()>0) || (prTitle=="---");
}

FXString cPopulationManager::getNewID(void)
{
    static int poNr=oApplicationManager->reg().readIntEntry("oids","populations",1);
    FXString res=oLanguage->getText("str_pomdi_id");
    FXString ret=res+FXStringFormat(" #%d",++poNr);
    while(populationExists(ret))
        ret=res+FXStringFormat(" #%d",++poNr);
    oApplicationManager->reg().writeIntEntry("oids","populations",poNr);
    return ret;
}

cDataResult *cPopulationManager::getPopulation(FXString prId)
{
    FXString res=prId;
    return oDataLink->execute("SELECT * FROM t_gnpopulations WHERE id='"+res.trim().substitute("'","")+"';");
}

FXString cPopulationManager::getPopulationAge(const FXString &prTitle,const FXString &prDate)
{
    FXString ret="";
    struct tm prstime,prsit;
    if(!oDataLink->isOpened())
        return ret;
    cDataResult *res=oDataLink->execute("SELECT birthDate FROM t_gnpopulations WHERE id='"+prTitle+"';");
    memset(&prstime,0,sizeof(prstime));
    memset(&prsit,0,sizeof(prsit));
    if((res->getRowCount()<=0) || (sscanf(res->getCellString(0,0).text(),"%d-%d-%d",&prstime.tm_year,&prstime.tm_mon,&prstime.tm_mday)!=3) ||
        (sscanf(prDate.text(),"%d-%d-%d",&prsit.tm_year,&prsit.tm_mon,&prsit.tm_mday)!=3))
        return ret;
    prstime.tm_year-=prstime.tm_year<1900?0:1900;
    prstime.tm_mon-=1;
    prsit.tm_year-=prsit.tm_year<1900?0:1900;
    prsit.tm_mon-=1;
    int res2=(int)(difftime(mktime(&prsit),mktime(&prstime))/86400);
    if(res2<0)
        return ret;
    int cat=cSpeciesManager::getAgeFormat(oDataLink->getSpecies());
    if(cat==5)
        res2/=7;
    ret=FXStringFormat("%d-%d",res2/cat,res2%cat);
    return ret;
}

FXbool cPopulationManager::addPopulation(FXString prId,FXString prPredId,FXString prBirthDate,FXString prBreed1,FXString prBreed2,FXString prOwner_oid,FXString prGrower_oid,FXString prVaccine_oid,FXString prUnit,FXString prLocation,FXString prSublocation,FXString prComments)
{
    return addPopulation(oDataLink,prId,prPredId,prBirthDate,prBreed1,prBreed2,prOwner_oid,prGrower_oid,prVaccine_oid,prUnit,prLocation,prSublocation,prComments);
}

FXbool cPopulationManager::addPopulation(cDataLink *prLink,FXString prId,FXString prPredId,FXString prBirthDate,FXString prBreed1,FXString prBreed2,FXString prOwner_oid,FXString prGrower_oid,FXString prVaccine_oid,FXString prUnit,FXString prLocation,FXString prSublocation,FXString prComments)
{
    if(!prLink->isOpened())
        return false;
    prLink->execute("INSERT INTO t_gnpopulations VALUES('"+
                                                    prId.trim().substitute("'","")+"','"+
                                                    prPredId.trim().substitute("'","")+"','"+
                                                    prBirthDate.trim().substitute("'","")+"','"+
                                                    prBreed1.trim().substitute("'","")+"','"+
                                                    prBreed2.trim().substitute("'","")+"','"+
                                                    prOwner_oid.trim().substitute("'","")+"','"+
                                                    prGrower_oid.trim().substitute("'","")+"','"+
                                                    prVaccine_oid.trim().substitute("'","")+"','"+
                                                    prUnit.trim().substitute("'","")+"','"+
                                                    prLocation.trim().substitute("'","")+"','"+
                                                    prSublocation.trim().substitute("'","")+"','"+
                                                    prComments.trim().substitute("'","")+"');");
    return (prLink->getAffectedRowCount()==1);
}

FXbool cPopulationManager::setPopulation(FXString prOldId,FXString prPredId,FXString prId,FXString prBirthDate,FXString prBreed1,FXString prBreed2,FXString prOwner_oid,FXString prGrower_oid,FXString prVaccine_oid,FXString prUnit,FXString prLocation,FXString prSublocation,FXString prComments)
{
    if(!oDataLink->isOpened())
        return false;
    oDataLink->execute("UPDATE t_gnpopulations SET id='"+prId.trim().substitute("'","")+
                       "', predId_defs='"+prPredId.trim().substitute("'","")+
                       "', birthDate='"+prBirthDate.trim().substitute("'","")+
                       "', breed1='"+prBreed1.trim().substitute("'","")+
                       "', breed2='"+prBreed2.trim().substitute("'","")+
                       "', owner_oid='"+prOwner_oid.trim().substitute("'","")+
                       "', grower_oid='"+prGrower_oid.trim().substitute("'","")+
                       "', vaccine_oid='"+prVaccine_oid.trim().substitute("'","")+
                       "', unit='"+prUnit.trim().substitute("'","")+
                       "', location='"+prLocation.trim().substitute("'","")+
                       "', sublocation='"+prSublocation.trim().substitute("'","")+
                       "', comments='"+prComments.trim().substitute("'","")+
                       "' WHERE id='"+prOldId+"';");
    int ret=oDataLink->getAffectedRowCount();
    oDataLink->execute("UPDATE t_gncases SET population_oid='"+prId.trim().substitute("'","")+"' WHERE population_oid='"+prOldId.trim().substitute("'","")+"';");

    /*
    // Update predecessors when population name changed
    cDataResult *ppr=oDataLink->execute("SELECT id,predId_defs FROM t_gnpopulations;");
    if(ppr)
    {
        bool found;
        int o;
        FXString id,defs,p,ndefs;
        for(int i=0;i<ppr->getRowCount();i++)
        {
            o=0;
            found=false;
            ndefs="";
            id=ppr->getCellString(i,0);
            defs=ppr->getCellString(i,1);
            while(!defs.empty())
            {
                p=defs.before('\t');
                if(p==prOldId)
                {
                    ndefs=ndefs+(o>0?"\t":"")+prId;
                    found=true;
                }
                else
                    ndefs=ndefs+(o>0?"\t":"")+p;
                defs=defs.after('\t');
                o++;
            }
            if(found)
                oDataLink->execute("UPDATE t_gnpopulations SET predId_defs='"+ndefs+"' WHERE id='"+id+"';");    
        }
    }*/
    
    return (ret==1);
}

FXbool cPopulationManager::removePopulation(const FXString &prId)
{
    if(!oDataLink->isOpened())
        return false;
    return (oDataLink->execute("DELETE FROM t_gnpopulation WHERE id='"+prId+"';")!=NULL);
}

