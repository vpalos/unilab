#ifndef CCASECOMPAREREPORT_H
#define CCASECOMPAREREPORT_H

#include <fx.h>
#include "cVChart.h"
#include "cColorTable.h"
#include "cDLGReportCC.h"
#include "cMDIReadings.h"
#include "cMDIReport.h"
#include "cSortList.h"

typedef struct sCCRSubObject
{
    FXString *results;
    FXint setsi,casei,stdicti;
};

typedef struct sCCRObject
{
    cVChart *chart;
    FXDict *cases;
};

class cCaseCompareReport
{
    private:
        cDLGReportCC *dlg;
        cMDIReport *repWin;
        cColorTable *plateTable2;
        cColorTable *statsTable;
    
        FXbool mcCala;
        FXbool mcCalb;
        FXbool mcTiter;
        FXbool mcTiters;
        FXbool mcBins;
        FXbool mcResult;
        FXbool mcResult2;
        //FXbool mcBins2;
        FXbool mcAm;
        FXbool mcAge;
        FXbool mcSingle;
        FXbool mcLog2;
        FXbool mcEU;
        FXbool mcBreed1;
        FXbool mcBreed2;
        
        FXint graphType;
        FXbool useGraphs;
        FXbool useData;
        FXbool useInfo;
        
        sCasesSet *casesSets;
        int tSetsCount;
        int tCasesCount;
        
        bool areTiters;
        bool areOnlyTs;
        bool areBins;
        bool areSamples;
        bool areSecondary;
        
        FXbool baseline;
        FXString bslid;
        FXbool bslss;
    protected:
        FXbool loadReadings(cSortList &prCases);
        FXbool processReadings(void);
        FXbool askPrefs(void);
        
        
    public:
        static void openCases(FXMDIClient *prP, FXPopup *prPup,cSortList &prCases);
};

#endif
