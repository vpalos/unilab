#include <signal.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <fx.h>
#include <sys/stat.h>
#include <FXPNGIcon.h>
#include "../index.h"
#include "engine.h"
#include "cLabel.h"
#include "graphics.h"
#include "cLanguage.h"
#include "cDataLink.h"
#include "cWinMain.h"
#include "cWindowInstall.h"
#include "cMDIDatabaseManager.h"

#ifdef WIN32
#include "windows.h"
#include "shlobj.h"
#endif

#define KEYTOLERANCE 30

FXString APP_PATH=FXSystem::getCurrentDirectory()+PATHSEP;
FXString PACKAGES_PATH=FXPath::absolute("core-packages")+PATHSEP;
FXString DATABASES_PATH=FXPath::absolute("data-databases")+PATHSEP;
FXApp *oApplicationManager;
FXbool APP_CREATED,APP_ITERATE;

int initiateCore(int prArgc,char **prArgv)
{
    APP_CREATED=false;
    APP_ITERATE=false;
    oApplicationManager=new FXApp(APP_TITLE,APP_VENDOR);
    oApplicationManager->init(prArgc,prArgv);

    FXString rdb=oApplicationManager->reg().readStringEntry("settings","recent_db","");
    FXString rules_defs,k=oApplicationManager->reg().readStringEntry("settings","install_version","");
    rules_defs=k;
    FXuint version=0,v1=0,v2=0,v3=0;
    v1=FXIntVal(rules_defs.before('.'));
    rules_defs=rules_defs.after('.');
    v2=FXIntVal(rules_defs.before('.'));
    rules_defs=rules_defs.after('.');
    v3=FXIntVal(rules_defs);
    version=FXIntVal(FXStringFormat("1%03d%03d%03d",v1,v2,v3));


    if(version>APP_VERSION_NO)
    {
        fatalError("A newer version is already installed!");
        return false;
    }

//#ifndef WIN32
    /*
    if((oApplicationManager->reg().readStringEntry("settings","install_path","")=="") || (!FXStat::isWritable(DATABASES_PATH)) || (k.empty()) ||
        ((oApplicationManager->reg().readStringEntry("settings","install_path","")=="") && (!rdb.empty()) && (!FXStat::isWritable(rdb))))
    {
        if(!installSoftware())
            return 0;
    }
    else if(version<APP_VERSION_NO)
    {
        if(!APP_CREATED)
        {
            APP_CREATED=true;
            oApplicationManager->create();
        }
        if(MBOX_CLICKED_YES==FXMessageBox::information(oApplicationManager,MBOX_YES_NO,"Install","A older version is already installed!\n\nProceeding to install new version."))
        {
            if(!installSoftware())
                return 0;
        }
    }//*/

//#else
//    if(!FXFile::isWritable(DATABASES_PATH))
//        fatalError("Access denies when trying to access the UniVet databases location!");
//#endif

    if(!securityCheck())
    {
        oApplicationManager->exit();
        return 0;
    }

    oLanguage=new cLanguage();
    oLanguage->loadDesignated();

    if(!APP_CREATED)
    {
        APP_CREATED=true;
        oApplicationManager->create();
    }

    oWinMain=new cWinMain(oApplicationManager,APP_TITLE);
    oWinMain->create();

#ifndef DEBUG
    oApplicationManager->addSignal(SIGINT,oWinMain,cWinMain::CMD_QUITPROGRAM);
#ifndef WIN32
    oApplicationManager->addSignal(SIGQUIT,oWinMain,cWinMain::CMD_QUITPROGRAM);
    oApplicationManager->addSignal(SIGHUP,oWinMain,cWinMain::CMD_QUITPROGRAM);
    oApplicationManager->addSignal(SIGPIPE,oWinMain,cWinMain::CMD_QUITPROGRAM);
#endif
#endif
    oDataLink=new cDataLink();
    oDataLink->openDesignated();

    oWinMain->inputTechnician();

    APP_ITERATE=true;
    return oApplicationManager->run();
}

void fatalError(const FXString &prMessage)
{
    if(!APP_CREATED)
    {
        APP_CREATED=true;
        oApplicationManager->create();
    }
    FXMessageBox::error(oApplicationManager,MBOX_OK,"Fatal Error Occured",prMessage.text());
    oApplicationManager->exit(0);
    exit(0);
}

int mainCanQuit(void)
{
    return (oLanguage->canQuit() &&
            oWinMain->canQuit());
}

void mainSaveSettings(void)
{
    oLanguage->saveSettings();
    oWinMain->saveSettings();
}

void quitProgram(void)
{
    oApplicationManager->exit(0);
    exit(0);
}

FXString val2key(FXString prVal)
{
    char dic[32]="N2YK3I1H9A6BEV74GWQPXD5J80OLZMT";
    FXString ret="";
    int y=FXIntVal(prVal),k=(y%7)+1;
    prVal=FXStringVal(y*k);
    for(int i=prVal.length()-1;i>=0;i--)
    {
        int n=FXIntVal(prVal.mid(i,1))*89;
        int c=n/29,r=n%29;
        ret=ret+FXStringFormat("%c%c",dic[r],dic[c]);
    }
    ret=FXStringFormat("%c",dic[k+3])+ret;
    return ret.upper();
}

FXString key2val(FXString prKey)
{
    prKey=prKey.upper();
    char dic[32]="N2YK3I1H9A6BEV74GWQPXD5J80OLZMT";
    FXString ret="";
    int k;
    char ch=prKey[0];
    for(k=0;(dic[k]!=ch) && (k<32);k++);
    k-=3;
    prKey=prKey.mid(1,prKey.length()-1);
    while(!prKey.empty())
    {
        FXString cs=prKey.mid(prKey.length()-2,2);
        prKey=prKey.mid(0,prKey.length()-2);
        int c,r;
        char ch=cs[0];
        for(r=0;(dic[r]!=ch) && (r<32);r++);
        ch=cs[1];
        for(c=0;(dic[c]!=ch) && (c<32);c++);

        ret=ret+FXStringVal((c*29+r)/89);
    }
    ret=FXStringVal(FXIntVal(ret)/k);
    return ret;
}

bool securityCheck(void)
{
    FXString key=oApplicationManager->reg().readStringEntry("oids","residue","");
    FXString rfp=oApplicationManager->reg().readStringEntry("oids","reference","");
    FXString dte=oApplicationManager->reg().readStringEntry("oids","blocking","");
    if((time(NULL)/86400)<FXIntVal(dte))
    {
        if(!APP_CREATED)
        {
            APP_CREATED=true;
            oApplicationManager->create();
        }
        FXMessageBox::error(oApplicationManager,MBOX_OK,"Fatal Error Occured","An illegal time change was detected!\nThe time on this computer was turned backwards!\nYou must now re-enter the UniVet acces code!");
        oApplicationManager->reg().writeStringEntry("oids","residue","");
        oApplicationManager->reg().writeStringEntry("oids","reference","");
        key="";
        rfp="";
    }
    oApplicationManager->reg().writeStringEntry("oids","blocking",FXStringFormat("%d",(int)(time(NULL)/86400)).text());
    oApplicationManager->reg().write();
    if(key.empty() || rfp.empty() || dte.empty())
    {
        key=getLegal();
        if(key.empty())
        {
            fatalError("You cannot use this software without agreeing to it's terms first!");
            return false;
        }
        if(key.length()<5)
        {
            fatalError("Invalid acces code!");
            return false;
        }
        rfp=val2key(FXStringVal((FXulong)((time(NULL)/86400))));
    }
    int kval=FXIntVal(key2val(key));
    int rval=FXIntVal(key2val(rfp));
    if((kval+KEYTOLERANCE-1)<rval)
    {
        fatalError("Invalid acces code!");
        return false;
    }
    oApplicationManager->reg().writeStringEntry("oids","residue",key.text());
    oApplicationManager->reg().writeStringEntry("oids","reference",rfp.text());
    return true;
}

FXString getLegal(void)
{
    if(!APP_CREATED)
    {
        APP_CREATED=true;
        oApplicationManager->create();
    }
    FXDialogBox dlg(oApplicationManager,"UniVet Access Code",DECOR_TITLE|DECOR_BORDER);
    FXVerticalFrame *_vframe0=new FXVerticalFrame(&dlg,LAYOUT_FILL);
    FXLabel *_l0;

    FXHorizontalFrame *_hframe8=new FXHorizontalFrame(_vframe0,0,0,0,0,0,0,0,0,0,0,0);
    FXFont *ft1=new FXFont(oApplicationManager,"Times New Roman",26,FXFont::Normal);
    FXFont *ft2=new FXFont(oApplicationManager,"Times New Roman",18,FXFont::Normal);
    _l0=new cLabel(_hframe8,"U",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,0,0,0,0);
        _l0->setFont(ft1);
        _l0->setTextColor(FXRGB(160,0,0));
    _l0=new cLabel(_hframe8,"n",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,2,3,0,2);
        _l0->setFont(ft2);
        _l0->setTextColor(FXRGB(160,0,0));
    _l0=new cLabel(_hframe8,"i",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,0,0,0,2);
        _l0->setFont(ft2);
        _l0->setTextColor(FXRGB(160,0,0));
    _l0=new cLabel(_hframe8,"V",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,0,0,0,0);
        _l0->setFont(ft1);
        _l0->setTextColor(FXRGB(160,0,0));
    _l0=new cLabel(_hframe8,"e",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,0,3,0,2);
        _l0->setFont(ft2);
        _l0->setTextColor(FXRGB(160,0,0));
    _l0=new cLabel(_hframe8,"t",NULL,LAYOUT_LEFT|LAYOUT_BOTTOM,0,0,0,0,0,0,0,2);
        _l0->setFont(ft2);
        _l0->setTextColor(FXRGB(160,0,0));

    new FXLabel(_vframe0,FXStringFormat("  version %s",APP_VERSION));
    new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_WIDTH,0,450);
    new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_WIDTH|SEPARATOR_GROOVE,0,0,350);
    new FXLabel(_vframe0,"Copyright Pasteur Animal Health.");
    new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_WIDTH|SEPARATOR_GROOVE,0,0,350);
    new FXLabel(_vframe0,"Terms and Conditions");
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X|FRAME_SUNKEN|LAYOUT_FIX_HEIGHT,0,0,0,160,0,0,0,0);
    FXText *tx=new FXText(_hframe0,NULL,0,TEXT_WORDWRAP|TEXT_READONLY|LAYOUT_FILL);
    tx->setScrollStyle(HSCROLLER_NEVER|HSCROLLING_OFF|VSCROLLER_ALWAYS|VSCROLLING_ON);
    tx->setBackColor(FXRGB(242,240,240));
    tx->setText(APP_LEGAL);

    _l0=new FXLabel(_vframe0,FXStringFormat("Developed by Palos && Sons.\nCheck out the http://www.palos.ro website!\nEmail us at office@palos.ro!"),NULL,LAYOUT_CENTER_X);
    _l0->setTextColor(FXRGB(180,80,80));
    _l0->setFont(new FXFont(oApplicationManager,"helvetica,80,bold"));
    new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_WIDTH|SEPARATOR_GROOVE,0,0,350);
    new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_WIDTH,0,450);
    new FXLabel(_vframe0,"Access code:");
    FXTextField *sn=new FXTextField(_vframe0,50,NULL,0,TEXTFIELD_NORMAL|LAYOUT_FILL_X);
    new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_WIDTH,0,450);
    new FXHorizontalSeparator(_vframe0,LAYOUT_FIX_WIDTH|SEPARATOR_GROOVE,0,0,350);
    FXHorizontalFrame *_hframe10=new FXHorizontalFrame(_vframe0,LAYOUT_FILL_X,0,0,0,0,0,0,0,0);
    new FXButton(_hframe10,"I Decline!",NULL,&dlg,FXDialogBox::ID_CANCEL,BUTTON_INITIAL|BUTTON_DEFAULT|FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH,0,0,80);
    new FXVerticalSeparator(_hframe10,LAYOUT_FIX_WIDTH|SEPARATOR_NONE,0,0,50);
    new FXButton(_hframe10,"I Accept!",NULL,&dlg,FXDialogBox::ID_ACCEPT,BUTTON_DEFAULT|FRAME_RAISED|LAYOUT_CENTER_X|LAYOUT_FIX_WIDTH,0,0,80);
    FXString ret="";
    sn->setFocus();
    if(dlg.execute(PLACEMENT_SCREEN))
    {
        ret=sn->getText();
        if(ret.empty())
            ret="!";
    }

    return ret;
}

/*
// Old install stuff!

#ifdef WIN32

HRESULT CreateLink(LPCSTR lpszPathObj, LPCSTR lpszPathLink, LPCSTR lpszDesc, LPCSTR lpszIcon, LPCSTR lpszWDir)
{
    HRESULT hres;
    IShellLink* psl;

    hres = CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER,
                            IID_IShellLink, (LPVOID*)&psl);
    if (SUCCEEDED(hres))
    {
        IPersistFile* ppf;

        psl->SetPath(lpszPathObj);
        psl->SetDescription(lpszDesc);
        psl->SetIconLocation(lpszIcon,0);
        psl->SetWorkingDirectory(lpszWDir);

        hres = psl->QueryInterface(IID_IPersistFile, (LPVOID*)&ppf);

        if (SUCCEEDED(hres))
        {
            WCHAR wsz[MAX_PATH];

            MultiByteToWideChar(CP_ACP, 0, lpszPathLink, -1, wsz, MAX_PATH);

            hres = ppf->Save(wsz, TRUE);
            ppf->Release();
        }
        psl->Release();
    }
    return hres;
}

bool changeMode(FXString prPath,FXuint prMode)
{
    FXString *dl;
    prPath=FXFile::absolute(prPath);
    if(_chmod(prPath.text(),prMode)==-1)
        return false;
    if(FXFile::isDirectory(prPath))
    {
        FXString olddir=FXFile::getCurrentDirectory();

        if(!FXFile::setCurrentDirectory(prPath))
            return false;

        int nof=FXFile::listFiles(dl,prPath,"*",LIST_ALL_FILES|LIST_ALL_DIRS|LIST_NO_PARENT);
        for(int i=0;i<nof;i++)
            changeMode(dl[i],prMode);

        if(!FXFile::setCurrentDirectory(olddir))
            return false;
    }
    return true;
}

#else

bool changeMode(FXString prPath,FXuint prMode)
{
    FXString *dl;
    prPath=FXPath::absolute(prPath);
    if(chmod(prPath.text(),prMode))
        return false;
    if(FXStat::isDirectory(prPath))
    {
        FXString olddir=FXSystem::getCurrentDirectory();

        if(!FXSystem::setCurrentDirectory(prPath))
            return false;

        int nof=FXDir::listFiles(dl,prPath,"*",FXDir::AllFiles|FXDir::AllDirs|FXDir::NoParent);
        for(int i=0;i<nof;i++)
            changeMode(dl[i],prMode);

        if(!FXSystem::setCurrentDirectory(olddir))
            return false;
    }
    return true;
}

#endif

bool installSoftware(void)
{
    if(!APP_CREATED)
    {
        APP_CREATED=true;
        oApplicationManager->create();
    }

    cWindowInstall dlg(oApplicationManager,"UniVet Install",DECOR_TITLE|DECOR_BORDER);
    if(!dlg.execute(PLACEMENT_SCREEN))
        return false;

    FXString path=dlg.getPath();

    if(!FXStat::exists(path))
        if(!FXDir::create(path,S_IRWXU))
        {
            fatalError("Could not create installation destination!");
            return false;
        }
    //path=FXFile::absolute(path);

    FXDialogBox msg(oApplicationManager,"Installing...",DECOR_TITLE|DECOR_BORDER);
    new FXLabel(&msg,"Installing UniVet Software!\n\nPlease wait...",NULL,LAYOUT_FILL|JUSTIFY_CENTER_X|JUSTIFY_CENTER_Y);
    msg.create();
    msg.show(PLACEMENT_SCREEN);
    msg.setFocus();
    msg.raise();
    oApplicationManager->beginWaitCursor();
    while(oApplicationManager->peekEvent())
        oApplicationManager->runOneEvent(false);
    if(!FXFile::copy(APP_PATH,FXPath::absolute(path),true))
    {
        oApplicationManager->endWaitCursor();
        msg.hide();
        fatalError("File copying failed!");
        return false;
    }

#ifdef WIN32
    if(changeMode(path,_S_IREAD|_S_IWRITE)==-1)
    {
        oApplicationManager->endWaitCursor();
        msg.hide();
        fatalError("File permission setting failed!");
        return false;
    }
#else
    if(!changeMode(path,S_IRWXU))
    {
        oApplicationManager->endWaitCursor();
        msg.hide();
        fatalError("File permission setting failed!");
        return false;
    }
#endif

    APP_PATH=path+PATHSEP;
    FXSystem::setCurrentDirectory(path);
    PACKAGES_PATH=FXPath::absolute("core-packages")+PATHSEP;
    DATABASES_PATH=FXPath::absolute("data-databases")+PATHSEP;
    oApplicationManager->reg().writeStringEntry("settings","install_path",APP_PATH.text());
    oApplicationManager->reg().writeStringEntry("settings","install_version",APP_VERSION);
#ifdef WIN32
    if(dlg.getDesk())
    {
        FXString src(APP_PATH+"univet.exe");
        FXString dir;
        bool is=true;

        dir=FXSystem::getHomeDirectory()+PATHSEP+FXStringFormat("Desktop");
        if(!FXStat::exists(dir))
        {
            dir="C:\\Windows\\Desktop";
            if(!FXFile::exists(dir))
                is=false;
        }

        if(is)
        {
            FXString tgt(dir+FXStringFormat("\\%s %s.lnk",APP_TITLE));
            FXString desc(FXStringFormat("%s",APP_TITLE));
            FXString icon(FXStringFormat("%s%s",APP_PATH.text(),"data-graphics\\appicon32.ico"));
            FXString wdir(APP_PATH);

            CoInitialize(NULL);
            CreateLink(src.text(),tgt.text(),desc.text(),icon.text(),wdir.text());
        }
    }
#endif
    oApplicationManager->endWaitCursor();
    msg.hide();

    FXMessageBox::information(oApplicationManager,MBOX_OK,"Success!","UniVet has been successfully installed on your computer!\n\nThank you!");
    return true;
}
*/

