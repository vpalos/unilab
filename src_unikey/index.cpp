#include <fx.h>
#include "index.h"
#include "engine.h"

int main(int prArgc,char **prArgv)
{
    initiateCore(prArgc,prArgv);
    return iterateCore();
}
