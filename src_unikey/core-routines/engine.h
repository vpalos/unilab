#ifndef ENGINE_H
#define ENGINE_H

#include <fx.h>
#include "../index.h"


extern FXString APP_PATH;
extern FXbool APP_CREATED;
extern FXApp *oApplicationManager;

void initiateCore(int prArgc,char **prArgv);
int  iterateCore(void);

void fatalError(const FXString &prMessage);

int  mainCanQuit(void);
void mainSaveSettings(void);

void quitProgram(void);

#endif
