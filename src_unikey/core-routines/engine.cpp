#include <fx.h>
#include <signal.h>
#include <stdlib.h>
#include "engine.h"
#include "cWinMain.h"

FXString APP_PATH=FXSystem::getCurrentDirectory()+PATHSEP;
FXbool APP_CREATED;
FXApp *oApplicationManager;

void initiateCore(int prArgc,char **prArgv)
{
    oApplicationManager=new FXApp(APP_TITLE,APP_VENDOR);
    oApplicationManager->init(prArgc,prArgv);
    oWinMain=new cWinMain(oApplicationManager,APP_TITLE);
    oApplicationManager->create();
}

int iterateCore(void)
{
    return oApplicationManager->run();
}

void quitProgram(void)
{
    oApplicationManager->exit(0);
    exit(0);
}
