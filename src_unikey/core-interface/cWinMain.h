#ifndef CWINMAIN_H
#define CWINMAIN_H

#include <fx.h>

class cWinMain : public FXMainWindow
{
    FXDECLARE(cWinMain);
    
    private:
        FXTextField *sn;
        
    protected:
        cWinMain();
    
    public:
        cWinMain(FXApp *prApp,const FXString &prCaption);
        virtual ~cWinMain();

        virtual void create();
        
        enum
        {
            ID_GENERATE=FXMainWindow::ID_LAST,
            
            CMD_GENERATE,
       
            ID_LAST
        };

        long onCmdGenerate(FXObject *prSender,FXSelector prSelector,void *prData);
};

extern cWinMain *oWinMain;

#endif

