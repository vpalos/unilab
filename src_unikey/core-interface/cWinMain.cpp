#include <time.h>
#include "engine.h"
#include "cWinMain.h"

cWinMain *oWinMain;

#define WINMAIN_MIN_W 400
#define WINMAIN_MIN_H 100

FXDEFMAP(cWinMain) mapWinMain[]=
{
    FXMAPFUNC(SEL_COMMAND,cWinMain::CMD_GENERATE,cWinMain::onCmdGenerate),
};

FXIMPLEMENT(cWinMain,FXMainWindow,mapWinMain,ARRAYNUMBER(mapWinMain))

cWinMain::cWinMain()
{
}

cWinMain::cWinMain(FXApp *prApp,const FXString &prCaption) 
    : FXMainWindow(prApp,prCaption,NULL,NULL,DECOR_BORDER|DECOR_TITLE,0,0,WINMAIN_MIN_W,WINMAIN_MIN_H,0,0,0,0)
{
    FXHorizontalFrame *_hframe0=new FXHorizontalFrame(this,LAYOUT_FIX_WIDTH|LAYOUT_FIX_HEIGHT,0,0,400,100);
    sn=new FXTextField(_hframe0,15,NULL,0,TEXTFIELD_NORMAL|TEXTFIELD_READONLY|LAYOUT_CENTER_Y|LAYOUT_FILL_X);
    sn->setFont(new FXFont(getApp(),"courier new,150,bold"));
    sn->setText("...");
    sn->setJustify(JUSTIFY_CENTER_X);
    FXVerticalFrame *_vframe0=new FXVerticalFrame(_hframe0,LAYOUT_CENTER_Y);
    FXButton *b=new FXButton(_vframe0,"Generate!",NULL,this,CMD_GENERATE,BUTTON_DEFAULT|BUTTON_INITIAL|FRAME_RAISED|LAYOUT_FIX_WIDTH|LAYOUT_FIX_HEIGHT,0,0,100,50);
    b->setBackColor(FXRGB(164,161,160));
    b->setTextColor(FXRGB(255,255,255));
    new FXButton(_vframe0,"Exit",NULL,this,ID_CLOSE,FRAME_RAISED|LAYOUT_FIX_WIDTH,0,0,100);
}

cWinMain::~cWinMain()
{
}

void cWinMain::create()
{
    FXMainWindow::create();
    show(PLACEMENT_SCREEN);
}

long cWinMain::onCmdGenerate(FXObject *prSender,FXSelector prSelector,void *prData)
{
    FXString prVal=FXStringVal((FXulong)(time(NULL)/86400));
    char dic[32]="N2YK3I1H9A6BEV74GWQPXD5J80OLZMT";
    FXString ret="";
    int y=FXIntVal(prVal),k=(y%7)+1;
    prVal=FXStringVal(y*k);
    for(int i=prVal.length()-1;i>=0;i--)
    {
        int n=FXIntVal(prVal.mid(i,1))*89;
        int c=n/29,r=n%29;
        ret=ret+FXStringFormat("%c%c",dic[r],dic[c]);
    }
    ret=FXStringFormat("%c",dic[k+3])+ret;
    sn->setText(ret.upper());
    sn->setFocus();
    sn->selectAll();
    return 1;
}

