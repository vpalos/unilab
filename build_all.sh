#!/bin/bash

if [ ! $1 ]; then
    CPUS=1
    echo
    echo
    echo ======================================
    echo Number of CPU cores was not specified!
    echo Reverted to the default of 1 CPU code!
    echo ======================================
    echo
    echo
else
    CPUS=$1
fi

# Clear current installers
rm -f Uni*-Installer.exe

# Build UniKey
pushd src_unikey
make clean
make -j$CPUS
popd

# Build UniVet
pushd src_univet
make clean
make -j$CPUS
popd

# Build UniTox
pushd src_unitox
make clean
make -j$CPUS
popd
